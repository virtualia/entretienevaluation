﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Datas
    Public Class ObjetUtiAnonyme
        Private WsNom As String
        '** Exécution
        Private WsCheminServeur As String
        Private WsNomFichierCourant As String
        Private WsAdresseSite As String
        Private WsUniteDisk As String
        Private WsNomEvaluateur As String
        Private WsIdeManager As Integer
        Private WsSiSelf As Boolean = False
        Private WsSiManager As Boolean = False
        Private WsSiN2 As Boolean = False
        Private WsContexte As Virtualia.Net.Datas.ObjetNavigation
        Private WsCompteur As Integer = 0
        Private WsSiDRH As Boolean = False
        Private WsIDSession As String = ""

        Public Property IDSession As String
            Get
                Return WsIDSession
            End Get
            Set(value As String)
                WsIDSession = value
            End Set
        End Property

        'Public Property NomFichierBase As String
        '    Get
        '        Return WsNomFichierCourant
        '    End Get
        '    Set(ByVal value As String)
        '        WsNomFichierCourant = value
        '    End Set
        'End Property

        Public Property Compteur As Integer
            Get
                Return WsCompteur
            End Get
            Set(ByVal value As Integer)
                WsCompteur = value
            End Set
        End Property

        Public ReadOnly Property PointeurContexte() As Virtualia.Net.Datas.ObjetNavigation
            Get
                If WsContexte Is Nothing Then
                    WsContexte = New Virtualia.Net.Datas.ObjetNavigation(Me)
                End If
                Return WsContexte
            End Get
        End Property

        Public Property Nom As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                WsNom = value
            End Set
        End Property

        Public Property CheminServeur As String
            Get
                Return WsCheminServeur
            End Get
            Set(ByVal value As String)
                WsCheminServeur = value
            End Set
        End Property

        Public Property AdresseduSite As String
            Get
                Return WsAdresseSite
            End Get
            Set(ByVal value As String)
                WsAdresseSite = value
            End Set
        End Property

        Public Property UniteDisk As String
            Get
                Return WsUniteDisk
            End Get
            Set(ByVal value As String)
                WsUniteDisk = value
            End Set
        End Property

        Public Property IdentifiantManager As Integer
            Get
                Return WsIdeManager
            End Get
            Set(ByVal value As Integer)
                WsIdeManager = value
            End Set
        End Property

        Public Property SiSelf As Boolean
            Get
                Return WsSiSelf
            End Get
            Set(ByVal value As Boolean)
                WsSiSelf = value
            End Set
        End Property
        Public Property SiManager As Boolean
            Get
                Return WsSiManager
            End Get
            Set(ByVal value As Boolean)
                WsSiManager = value
            End Set
        End Property
        Public Property SiN2 As Boolean
            Get
                Return WsSiN2
            End Get
            Set(ByVal value As Boolean)
                WsSiN2 = value
            End Set
        End Property

        Public Property SiDRH As Boolean
            Get
                Return WsSiDRH
            End Get
            Set(ByVal value As Boolean)
                WsSiDRH = value
            End Set
        End Property

        Public Sub New(ByVal NoSession As String, ByVal NomUtilisateur As String)
            WsIDSession = NoSession
            WsNom = NomUtilisateur
        End Sub
    End Class
End Namespace