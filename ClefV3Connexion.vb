﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class ClefV3Connexion
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
        Private WsRhFct As Virtualia.Systeme.Fonctions.Generales
        Private WsChainePostee As String
        Private WsSiAccesManager As Boolean = False
        Private WsSiAccesGRH As Boolean = False
        Private WsAppliAccedee As String = ""
        Private WsUtilisateurV3 As Virtualia.Version3.Utilisateur.UtilisateurV3
        Private WsNomCnxV3 As String
        Private WsIdentifiant As Integer = 0
        Private WsIdentifiantManager As Integer = 0
        Private WsIdentifiantManagerN2 As Integer = 0
        Private WsSiAccess_ManagerEtN2 As Boolean = False
        Private WsDetailErr As String = ""

        Public ReadOnly Property SiAcces_Manager As Boolean
            Get
                Return WsSiAccesManager
            End Get
        End Property

        Public ReadOnly Property SiAcces_DRH As Boolean
            Get
                Return WsSiAccesGRH
            End Get
        End Property

        Public ReadOnly Property SiAcces_ManagerEtN2 As Boolean
            Get
                Return WsSiAccess_ManagerEtN2
            End Get
        End Property

        Public ReadOnly Property Appli_Accedee As String
            Get
                Return WsAppliAccedee
            End Get
        End Property

        Public ReadOnly Property Nom_CnxV3 As String
            Get
                Return WsNomCnxV3
            End Get
        End Property

        Public ReadOnly Property Detail_ErreurCnx As String
            Get
                Return WsDetailErr
            End Get
        End Property

        Public ReadOnly Property ObjetUti_CnxV3 As Virtualia.Version3.Utilisateur.UtilisateurV3
            Get
                Return WsUtilisateurV3
            End Get
        End Property

        Public ReadOnly Property Identifiant As Integer
            Get
                Return WsIdentifiant
            End Get
        End Property

        Public Property IdentifiantManager As Integer
            Get
                Return WsIdentifiantManager
            End Get
            Set(value As Integer)
                WsIdentifiantManager = value
            End Set
        End Property

        Public Function SiAcces_ManagerN2(ByVal AppGlobal As Virtualia.Net.WebAppli.ObjetGlobal, ByVal ChaineEval As String) As Boolean
            If IsNumeric(ChaineEval) Then
                Dim IdeManager As Integer
                IdeManager = CInt(ChaineEval)

                Dim StructureAff As String


                For IndiceI = 4 To 1 Step -1
                    StructureAff = NiveauAffectation(AppGlobal, WsRhDates.DateduJour, IndiceI)
                    If StructureAff <> "" Then
                        WsIdentifiantManager = AppGlobal.IdentifiantManager(CStr(IndiceI), StructureAff)
                        If WsIdentifiantManager > 0 And WsIdentifiantManager <> WsIdentifiant Then
                            Exit For
                        End If
                    End If
                Next IndiceI



                For IndiceI = 4 To 1 Step -1
                    StructureAff = NiveauAffectation(AppGlobal, WsRhDates.DateduJour, IndiceI)
                    If StructureAff <> "" Then
                        WsIdentifiantManagerN2 = AppGlobal.IdentifiantManager(CStr(IndiceI), StructureAff)
                        If WsIdentifiantManagerN2 > 0 And WsIdentifiantManagerN2 <> WsIdentifiant And WsIdentifiantManagerN2 <> WsIdentifiantManager Then
                            Exit For
                        End If
                    End If
                Next IndiceI
                If WsIdentifiantManagerN2 = 0 Then
                    WsIdentifiantManagerN2 = WsIdentifiantManager
                End If

                If WsIdentifiantManagerN2 = WsIdentifiantManager Then
                    WsSiAccess_ManagerEtN2 = True
                Else
                    WsSiAccess_ManagerEtN2 = False
                End If

                If WsIdentifiantManagerN2 = IdeManager Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        End Function

        Public Function SiAccesOK(ByVal AppGlobal As Virtualia.Net.WebAppli.ObjetGlobal) As Boolean
            Dim Chaine As String
            Dim Separateur As String
            Dim VerifV4 As Integer
            Dim TableauData(0) As String

            WsDetailErr = ""
            Chaine = WsRhFct.HashSHA(Now.Hour & WsRhDates.DateduJour)
            If Chaine.Length > 15 Then
                Separateur = Strings.Left(Chaine, 15).ToLower
            Else
                Separateur = Chaine.ToLower
            End If

            WsNomCnxV3 = ""
            WsAppliAccedee = ""
            VerifV4 = Now.Hour + Now.Day + Now.Month + Now.Year - 1951
            WsSiAccesManager = False
            WsSiAccesGRH = False

            If WsChainePostee.Contains(Separateur) = False Then
                Return False
            End If
            TableauData = Strings.Split(WsChainePostee, Separateur, -1)
            '** 1ère vérification Heure et jour de l'appel
            If IsNumeric(TableauData(1)) = False Then
                WsDetailErr = "Heure non numérique"
                Return False
            End If
            If VerifV4 = CInt(TableauData(1)) Then
                WsAppliAccedee = TableauData(2)
                If WsAppliAccedee = "" Then
                    WsDetailErr = "Application erronée"
                    Return False
                End If
            Else
                WsDetailErr = "Heure erronée"
                Return False
            End If
            If IsNumeric(TableauData(0)) Then
                WsIdentifiant = CInt(CInt(TableauData(0)) / 17)
                If WsRhFct.HashSHA(TableauData(3).ToLower & CStr(WsIdentifiant)) = TableauData(4) Then
                    WsNomCnxV3 = TableauData(3)
                Else
                    WsDetailErr = "Paramètres de connexion invalide"
                    Return False
                End If
            Else
                WsDetailErr = "Paramètres de connexion invalide"
                Return False
            End If
            If WsAppliAccedee.StartsWith("Self") = False Then
                If WsAppliAccedee.StartsWith("Manager") = True Then
                    WsSiAccesManager = True
                Else
                    WsSiAccesGRH = VerificationUtiVersion3()
                    If WsSiAccesGRH = False Then
                        WsDetailErr = "Accés GRH invalide"
                        Return False
                    End If
                End If
            End If

            Return True
        End Function

        Public Function Chaine_Test(ByVal NomUser As String, ByVal Appli As String, ByVal Ide As Integer) As String
            Dim Chaine As String
            Dim Separateur As String
            Dim VerifV4 As Integer

            Chaine = WsRhFct.HashSHA(Now.Hour & WsRhDates.DateduJour)
            If Chaine.Length > 15 Then
                Separateur = Strings.Left(Chaine, 15).ToLower
            Else
                Separateur = Chaine.ToLower
            End If
            VerifV4 = Now.Hour + Now.Day + Now.Month + Now.Year - 1951
            Chaine = CStr(CInt(Ide * 17)) & Separateur & VerifV4 & Separateur & Appli & Separateur
            Chaine &= NomUser & Separateur & WsRhFct.HashSHA(NomUser.ToLower & CStr(Ide))
            Return Chaine
        End Function

        Private Function VerificationUtiVersion3() As Boolean
            If WsNomCnxV3 = "" Then
                Return False
            End If
            WsUtilisateurV3 = New Virtualia.Version3.Utilisateur.UtilisateurV3(System.Configuration.ConfigurationManager.AppSettings("RepertoireUtilisateurV3"), WsNomCnxV3)
            If WsUtilisateurV3 IsNot Nothing AndAlso WsUtilisateurV3.Nom.ToUpper = WsNomCnxV3.ToUpper Then
                Return True
            End If
            Return False
        End Function

        ''******* AKR AFB
        Public ReadOnly Property NiveauAffectation(ByVal AppGlobal As Virtualia.Net.WebAppli.ObjetGlobal, ByVal DateEffet As String, ByVal Index As Integer) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim ListeFonds As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim ListeObjets As List(Of Integer) = New List(Of Integer)
                ListeObjets.Add(VI.ObjetPer.ObaCivil)
                ListeObjets.Add(VI.ObjetPer.ObaSociete)
                ListeObjets.Add(VI.ObjetPer.ObaStatut)
                ListeObjets.Add(VI.ObjetPer.ObaOrganigramme)
                ListeObjets.Add(VI.ObjetPer.ObaAdrPro)
                ListeObjets.Add(VI.ObjetPer.ObaActivite)
                ListeObjets.Add(VI.ObjetPer.ObaGrade)
                ListeObjets.Add(VI.ObjetPer.ObaFormation)
                ListeObjets.Add(VI.ObjetPer.ObaDIF)
                ListeObjets.Add(VI.ObjetPer.ObaSpecialite)
                ListeObjets.Add(VI.ObjetPer.ObaExterne)
                If WsIdentifiant <= 0 Then
                    Return ""
                End If
                Try
                    ListeFonds = AppGlobal.VirServiceServeur.LectureDossier_ToFiches(AppGlobal.PointeurObjetUtiGlobal.Nom,
                                                                                  VI.PointdeVue.PVueApplicatif, WsIdentifiant, False, ListeObjets)
                    LstFiches = (From instance In ListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaCivil).ToList
                Catch ex As Exception
                    Return ""
                End Try
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Return ""
                End If
                Dim DateValeur As Date = AppGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

                LstFiches = (From instance In ListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In ListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    Select Case Index
                        Case 1
                            Return Fiche.Structure_de_rattachement
                        Case 2
                            Return Fiche.Structure_d_affectation
                        Case 3
                            Return Fiche.Structure_de_3e_niveau
                        Case 4
                            Return Fiche.Structure_de_4e_niveau
                    End Select
                End If
                Return ""
            End Get
        End Property

        ''************************

        Public Sub New(ByVal ChainePOST As String)
            WsChainePostee = ChainePOST
            WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
            WsRhFct = New Virtualia.Systeme.Fonctions.Generales
        End Sub
    End Class
End Namespace