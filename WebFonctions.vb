﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Public Class WebFonctions
    Inherits System.Web.UI.UserControl
    Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales
    Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
    Private WsCharte As Virtualia.Systeme.Fonctions.CharteGraphique
    Private WsNomUti As String
    Private WsTypeControle As Integer = 0 'UserControl, 1 Page, 2 MasterPage
    Private WsHostUC As System.Web.UI.UserControl
    Private WsHostP As System.Web.UI.Page
    Private WsHostMP As System.Web.UI.MasterPage
    Private Const TypeUC As Integer = 0
    Private Const TypeP As Integer = 1
    Private Const TypeMP As Integer = 2
    Private AppObjetGlobal As Virtualia.Net.WebAppli.ObjetGlobal
    '
    Private WsListeCtl(0) As Control
    Private WsCpt As Integer

    Public ReadOnly Property PointeurGlobal() As Virtualia.Net.WebAppli.ObjetGlobal
        Get
            Return AppObjetGlobal
        End Get
    End Property

    Public ReadOnly Property PointeurUtilisateur() As Virtualia.Net.Datas.ObjetUtilisateur
        Get
            Return AppObjetGlobal.PointeurObjetUtiGlobal
        End Get
    End Property
    '******************
    Public ReadOnly Property SessionVirtualia(ByVal NoSession As String) As Virtualia.Net.Datas.ObjetUtiAnonyme
        Get
            Return AppObjetGlobal.ItemSession(NoSession)
        End Get
    End Property

    Public ReadOnly Property InfoExperte(ByVal Ptdevue As Integer, ByVal NumObjet As Integer, ByVal IdeExperte As Integer, ByVal Ide As Integer, ByVal DateEffet As String) As String
        Get

            If PointeurUtilisateur Is Nothing Then
                Return ""
            End If
            Dim Result As String
            Select Case IdeExperte
                Case 916
                    DateEffet = "31/12/" & Strings.Format(CInt(Strings.Right(DateEffet, 4)), "0000")
            End Select

            PointeurUtilisateur.PointeurDllExpert.ClasseExperte(Ptdevue, NumObjet) = VI.OptionInfo.DicoRecherche
            Try
                Result = PointeurUtilisateur.PointeurDllExpert.Donnee(Ptdevue, NumObjet, IdeExperte, Ide, "01/01/1950", DateEffet)
            Catch Ex As Exception
                Result = ""
            End Try
            Return Result
        End Get
    End Property

    Public ReadOnly Property ContexteSession(ByVal NoSession As String) As Virtualia.Net.Datas.ObjetNavigation
        Get
            Dim Uti As Virtualia.Net.Datas.ObjetUtiAnonyme = SessionVirtualia(NoSession)
            If Uti IsNot Nothing Then
                Return Uti.PointeurContexte
            Else
                Return PointeurUtilisateur.PointeurContexte
            End If
        End Get
    End Property

    Public ReadOnly Property ViRhDates() As Virtualia.Systeme.Fonctions.CalculDates
        Get
            Return WsRhDates
        End Get
    End Property

    Public ReadOnly Property ViRhFonction() As Virtualia.Systeme.Fonctions.Generales
        Get
            Return WsRhFonction
        End Get
    End Property

    Public Function VirSourceHtml(ByVal Ctrl As System.Web.UI.Control) As String
        Dim ChaineWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim RenduHtml As System.Web.UI.HtmlTextWriter
        RenduHtml = New System.Web.UI.HtmlTextWriter(ChaineWriter)
        Try
            Ctrl.RenderControl(RenduHtml)
        Catch ex As Exception
            Return ""
        End Try
        Return RenduHtml.InnerWriter.ToString
    End Function

    Public ReadOnly Property Evt_MessageInformatif(ByVal NumObjet As Integer, ByVal TitreMsg As String, ByVal MsgInfos As ArrayList, Optional ByVal Ide As Integer = 0) As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Get
            If MsgInfos Is Nothing Then
                Return Nothing
            End If
            Dim Msg As String = ""
            Dim I As Integer

            For I = 0 To MsgInfos.Count - 1
                Msg &= MsgInfos(I).ToString & Strings.Chr(13) & Strings.Chr(10)
            Next I

            Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("MajDossier", NumObjet, Ide.ToString, "OK", TitreMsg, Msg)
            Return Evenement
        End Get
    End Property

    Public ReadOnly Property Evt_MessageBloquant(ByVal NumObjet As Integer, ByVal TitreMsg As String, ByVal MsgErreurs As ArrayList) As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Get
            If MsgErreurs Is Nothing Then
                Return Nothing
            End If
            Dim Msg As String = ""
            Dim I As Integer

            For I = 0 To MsgErreurs.Count - 1
                Msg &= " Anomalie bloquante : " & MsgErreurs(I).ToString & Strings.Chr(13) & Strings.Chr(10)
            Next I

            Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("MajDossier", NumObjet, "", "KO", TitreMsg, Msg)
            Return Evenement
        End Get
    End Property

    Public ReadOnly Property AppliquerRegle(ByVal NomPrenom As String, ByVal CacheIde As ArrayList, ByVal CacheMaj As ArrayList, ByVal Chainelue As String) As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Get
            '** Provisoire
            Return Nothing
            Exit Property
            '**
        End Get
    End Property

    Public ReadOnly Property ConfirmerSuppressionFiche(ByVal NumObjet As Integer, ByVal Occurence As String) As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Get
            Dim TitreMsg As String = "Suppression de la fiche " & Occurence
            Dim Msg As String = "Confirmez-vous la suppression de cette fiche ?"
            Dim NatureCmd As String = "Oui;Non"

            Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("SuppFiche", NumObjet, Occurence, NatureCmd, TitreMsg, Msg)
            Return Evenement
        End Get
    End Property

    Public ReadOnly Property SiFicheASupprimer(ByVal CacheMaj As ArrayList) As Boolean
        Get
            Dim IndiceI As Integer
            For IndiceI = 0 To CacheMaj.Capacity - 1
                If CacheMaj(IndiceI) IsNot Nothing Then
                    If CacheMaj(IndiceI).ToString <> "" Then
                        Return False
                        Exit Property
                    End If
                End If
            Next IndiceI
            Return True
        End Get
    End Property

    Public ReadOnly Property ListeCtl(ByVal Index As Integer) As Control
        Get
            Select Case Index
                Case 0 To WsListeCtl.Count - 1
                    Return WsListeCtl(Index)
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property

    Public Function Controle_KeyPressStd(ByVal PtdeVue As Integer, ByVal NumObjet As Integer, ByVal NumInfo As Integer, ByVal KeyAscii As String) As String
        Select Case KeyAscii
            Case Is = "8"
                Return KeyAscii
                Exit Function
        End Select
        Select Case KeyAscii
            Case Is = "ToucheSuppression"
                Return "0"
                Exit Function
        End Select
        Select Case KeyAscii
            Case Is = VI.Tild
                Return "0"
                Exit Function
        End Select
        Select Case KeyAscii
            Case Is = VI.SigneBarre
                Return "0"
                Exit Function
        End Select
        Select Case PointeurDicoInfo(PtdeVue, NumObjet, NumInfo).VNature
            Case VI.NatureDonnee.DonneeNumerique
                Select Case PointeurDicoInfo(PtdeVue, NumObjet, NumInfo).Format
                    Case VI.FormatDonnee.Monetaire, VI.FormatDonnee.Pourcentage, VI.FormatDonnee.Standard
                        Select Case KeyAscii
                            Case Is = "."
                                Return KeyAscii
                                Exit Function
                            Case Is = ","
                                Return Strings.Chr(46)
                                Exit Function
                        End Select
                    Case VI.FormatDonnee.Decimal1 To VI.FormatDonnee.Decimal6
                        Select Case KeyAscii
                            Case Is = "."
                                Return KeyAscii
                                Exit Function
                            Case Is = ","
                                Return Strings.Chr(46)
                                Exit Function
                        End Select
                    Case VI.FormatDonnee.Fixe, VI.FormatDonnee.Duree
                        If KeyAscii = "." Or KeyAscii = "," Then
                            Return "0"
                            Exit Function
                        End If
                End Select
                If KeyAscii < "0" Or KeyAscii > "9" Then Return "0"
            Case VI.NatureDonnee.DonneeHeure
                Select Case PointeurDicoInfo(PtdeVue, NumObjet, NumInfo).Format
                    Case VI.FormatDonnee.DureeHeure
                        Select Case KeyAscii
                            Case Is = "h"
                                Return KeyAscii
                                Exit Function
                        End Select
                    Case Else
                        Select Case KeyAscii
                            Case Is = ":"
                                Return KeyAscii
                                Exit Function
                        End Select
                End Select
                If KeyAscii < "0" Or KeyAscii > "9" Then Return "0"
            Case VI.NatureDonnee.DonneeTable, VI.NatureDonnee.DonneeTableMemo
                Return "0"
        End Select
        Return KeyAscii
    End Function

    Public Function Controle_LostFocusStd(ByVal NatureDonnee As Integer, ByVal FormatDonnee As Integer, ByVal Valeur As String) As String
        Dim Chaine As String = Valeur
        Select Case NatureDonnee
            Case VI.NatureDonnee.DonneeExperte
                Return Valeur
                Exit Function
            Case VI.NatureDonnee.DonneeTexte
                Select Case FormatDonnee
                    Case VI.FormatDonnee.Majuscule
                        Chaine = Valeur.ToUpper
                    Case VI.FormatDonnee.Minuscule
                        Chaine = WsRhFonction.Lettre1Capi(Valeur, 0)
                    Case VI.FormatDonnee.L1Capi
                        Chaine = WsRhFonction.Lettre1Capi(Valeur, 1)
                End Select
            Case VI.NatureDonnee.DonneeDate
                Select Case FormatDonnee
                    Case VI.FormatDonnee.Annee, VI.FormatDonnee.FinAnnee
                        Select Case Valeur
                            Case Is <> ""
                                Select Case Len(Valeur)
                                    Case Is > 4
                                        Chaine = Right(Valeur, 4)
                                    Case Is = 4
                                        Chaine = Valeur
                                End Select
                        End Select
                        Select Case Chaine
                            Case Is <> ""
                                Select Case FormatDonnee
                                    Case VI.FormatDonnee.Annee
                                        Chaine = "01/01/" & Chaine
                                    Case VI.FormatDonnee.FinAnnee
                                        Chaine = "31/12/" & Chaine
                                End Select
                                Chaine = WsRhDates.DateSaisieVerifiee(Chaine)
                                Chaine = Strings.Right(Chaine, 4)
                        End Select
                    Case Else
                        Chaine = WsRhDates.DateSaisieVerifiee(Valeur)
                End Select
            Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeCalculee
                Select Case Valeur
                    Case Is <> ""
                        Select Case FormatDonnee
                            Case VI.FormatDonnee.Standard
                                If Strings.InStr(Valeur, ".") > 0 Or InStr(Valeur, ",") > 0 Then
                                    Chaine = Strings.Format(Val(WsRhFonction.VirgulePoint(Valeur)), "###0.0")
                                Else
                                    Chaine = Format(Val(Valeur), "###0")
                                End If
                            Case VI.FormatDonnee.Fixe
                                Chaine = Strings.Format(Val(Valeur), "###0")
                            Case VI.FormatDonnee.Pourcentage
                                Chaine = Strings.Format(Val(WsRhFonction.VirgulePoint(Valeur)), "##0.00#")
                            Case VI.FormatDonnee.Monetaire, VI.FormatDonnee.Decimal2
                                Chaine = Strings.Format(Val(WsRhFonction.VirgulePoint(Valeur)), "#,##0.00")
                            Case VI.FormatDonnee.Decimal1
                                Select Case Right(Valeur, 2)
                                    Case ".0", ",0"
                                        Chaine = Strings.Format(Val(WsRhFonction.VirgulePoint(Valeur)), "#,##0")
                                    Case Else
                                        Chaine = Strings.Format(Val(WsRhFonction.VirgulePoint(Valeur)), "#,##0.0")
                                End Select
                            Case VI.FormatDonnee.Decimal3
                                Chaine = Strings.Format(Val(WsRhFonction.VirgulePoint(Valeur)), "#,##0.000")
                            Case VI.FormatDonnee.Decimal4
                                Chaine = Strings.Format(Val(WsRhFonction.VirgulePoint(Valeur)), "#,##0.0000")
                            Case VI.FormatDonnee.Decimal5
                                Chaine = Strings.Format(Val(WsRhFonction.VirgulePoint(Valeur)), "#,##0.00000")
                            Case VI.FormatDonnee.Duree
                                Chaine = Strings.Mid(Valeur, 3, 2) & " A. " & Strings.Mid(Valeur, 5, 2) & " M. " & Strings.Mid(Valeur, 7, 2) & " J. "
                            Case VI.FormatDonnee.DureeHeure
                                Chaine = WsRhDates.VerifHeure(Valeur, False)
                            Case Else
                                Chaine = Valeur.ToUpper
                        End Select
                    Case Else
                        Chaine = Valeur
                End Select
            Case VI.NatureDonnee.DonneeHeure
                Select Case Valeur
                    Case Is <> ""
                        Select Case FormatDonnee
                            Case VI.FormatDonnee.DureeHeure
                                Chaine = WsRhDates.VerifHeure(Valeur, False)
                            Case Else
                                Chaine = WsRhDates.VerifHeure(Valeur, True)
                        End Select
                End Select
        End Select
        Return Chaine
    End Function

    Public ReadOnly Property Old_VirWebControle(ByVal CadreInfo As Control, ByVal Prefixe As String, ByVal Index As Integer) As Control
        Get
            Dim Cpt As Integer = -1
            Dim IndiceI As Integer
            Dim IndiceN1 As Integer
            Dim IndiceN2 As Integer
            Dim IndiceN3 As Integer
            Dim IndiceN4 As Integer
            Dim IndiceN5 As Integer
            Dim IndiceN6 As Integer
            Dim IndiceN7 As Integer
            Dim IndiceN8 As Integer
            Dim N0 As Control
            Dim N1 As Control
            Dim N2 As Control
            Dim N3 As Control
            Dim N4 As Control
            Dim N5 As Control
            Dim N6 As Control
            Dim N7 As Control
            Dim N8 As Control
            Dim Lg As Integer = Prefixe.Length

            For IndiceI = 0 To CadreInfo.Controls.Count - 1
                N0 = CadreInfo.Controls.Item(IndiceI)
                Select Case Strings.Left(N0.ID, Lg)
                    Case Is = Prefixe
                        Cpt += 1
                        If Cpt = Index Then
                            Return N0
                        End If
                    Case Else
                        For IndiceN1 = 0 To N0.Controls.Count - 1
                            N1 = N0.Controls.Item(IndiceN1)
                            Select Case Strings.Left(N1.ID, Lg)
                                Case Is = Prefixe
                                    Cpt += 1
                                    If Cpt = Index Then
                                        Return N1
                                    End If
                                Case Else
                                    For IndiceN2 = 0 To N1.Controls.Count - 1
                                        N2 = N1.Controls.Item(IndiceN2)
                                        Select Case Strings.Left(N2.ID, Lg)
                                            Case Is = Prefixe
                                                Cpt += 1
                                                If Cpt = Index Then
                                                    Return N2
                                                End If
                                            Case Else
                                                For IndiceN3 = 0 To N2.Controls.Count - 1
                                                    N3 = N2.Controls.Item(IndiceN3)
                                                    Select Case Strings.Left(N3.ID, Lg)
                                                        Case Is = Prefixe
                                                            Cpt += 1
                                                            If Cpt = Index Then
                                                                Return N3
                                                            End If
                                                        Case Else
                                                            For IndiceN4 = 0 To N3.Controls.Count - 1
                                                                N4 = N3.Controls.Item(IndiceN4)
                                                                Select Case Strings.Left(N4.ID, Lg)
                                                                    Case Is = Prefixe
                                                                        Cpt += 1
                                                                        If Cpt = Index Then
                                                                            Return N4
                                                                        End If
                                                                    Case Else
                                                                        For IndiceN5 = 0 To N4.Controls.Count - 1
                                                                            N5 = N4.Controls.Item(IndiceN5)
                                                                            Select Case Strings.Left(N5.ID, Lg)
                                                                                Case Is = Prefixe
                                                                                    Cpt += 1
                                                                                    If Cpt = Index Then
                                                                                        Return N5
                                                                                    End If
                                                                                Case Else
                                                                                    For IndiceN6 = 0 To N5.Controls.Count - 1
                                                                                        N6 = N5.Controls.Item(IndiceN6)
                                                                                        Select Case Strings.Left(N6.ID, Lg)
                                                                                            Case Is = Prefixe
                                                                                                Cpt += 1
                                                                                                If Cpt = Index Then
                                                                                                    Return N6
                                                                                                End If
                                                                                            Case Else
                                                                                                For IndiceN7 = 0 To N6.Controls.Count - 1
                                                                                                    N7 = N6.Controls.Item(IndiceN7)
                                                                                                    Select Case Strings.Left(N7.ID, Lg)
                                                                                                        Case Is = Prefixe
                                                                                                            Cpt += 1
                                                                                                            If Cpt = Index Then
                                                                                                                Return N7
                                                                                                            End If
                                                                                                        Case Else
                                                                                                            For IndiceN8 = 0 To N7.Controls.Count - 1
                                                                                                                N8 = N7.Controls.Item(IndiceN8)
                                                                                                                Select Case Strings.Left(N8.ID, Lg)
                                                                                                                    Case Is = Prefixe
                                                                                                                        Cpt += 1
                                                                                                                        If Cpt = Index Then
                                                                                                                            Return N8
                                                                                                                        End If
                                                                                                                End Select
                                                                                                            Next IndiceN8
                                                                                                    End Select
                                                                                                Next IndiceN7
                                                                                        End Select
                                                                                    Next IndiceN6
                                                                            End Select
                                                                        Next IndiceN5
                                                                End Select
                                                            Next IndiceN4
                                                    End Select
                                                Next IndiceN3
                                        End Select
                                    Next IndiceN2
                            End Select
                        Next IndiceN1
                End Select
            Next IndiceI
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property VirWebControle(ByVal CadreInfo As Control, ByVal Prefixe As String, ByVal Index As Integer) As Control
        Get
            Dim IndiceI As Integer
            Dim IndiceN1 As Integer
            Dim IndiceN2 As Integer
            Dim IndiceN3 As Integer
            Dim IndiceN4 As Integer
            Dim IndiceN5 As Integer
            Dim IndiceN6 As Integer
            Dim IndiceN7 As Integer
            Dim IndiceN8 As Integer
            Dim IndiceN9 As Integer
            Dim IndiceN10 As Integer
            Dim VirCtl As Control
            Dim N0 As Control
            Dim N1 As Control
            Dim N2 As Control
            Dim N3 As Control
            Dim N4 As Control
            Dim N5 As Control
            Dim N6 As Control
            Dim N7 As Control
            Dim N8 As Control
            Dim N9 As Control
            Dim N10 As Control
            Dim Lg As Integer = Prefixe.Length

            WsCpt = -1
            For IndiceI = 0 To CadreInfo.Controls.Count - 1
                N0 = CadreInfo.Controls.Item(IndiceI)
                VirCtl = Recherche_WebControle(N0, Prefixe, Index)
                If VirCtl IsNot Nothing Then
                    Return VirCtl
                End If
                For IndiceN1 = 0 To N0.Controls.Count - 1
                    N1 = N0.Controls.Item(IndiceN1)
                    VirCtl = Recherche_WebControle(N1, Prefixe, Index)
                    If VirCtl IsNot Nothing Then
                        Return VirCtl
                    End If
                    For IndiceN2 = 0 To N1.Controls.Count - 1
                        N2 = N1.Controls.Item(IndiceN2)
                        VirCtl = Recherche_WebControle(N2, Prefixe, Index)
                        If VirCtl IsNot Nothing Then
                            Return VirCtl
                        End If
                        For IndiceN3 = 0 To N2.Controls.Count - 1
                            N3 = N2.Controls.Item(IndiceN3)
                            VirCtl = Recherche_WebControle(N3, Prefixe, Index)
                            If VirCtl IsNot Nothing Then
                                Return VirCtl
                            End If
                            For IndiceN4 = 0 To N3.Controls.Count - 1
                                N4 = N3.Controls.Item(IndiceN4)
                                VirCtl = Recherche_WebControle(N4, Prefixe, Index)
                                If VirCtl IsNot Nothing Then
                                    Return VirCtl
                                End If
                                For IndiceN5 = 0 To N4.Controls.Count - 1
                                    N5 = N4.Controls.Item(IndiceN5)
                                    VirCtl = Recherche_WebControle(N5, Prefixe, Index)
                                    If VirCtl IsNot Nothing Then
                                        Return VirCtl
                                    End If
                                    For IndiceN6 = 0 To N5.Controls.Count - 1
                                        N6 = N5.Controls.Item(IndiceN6)
                                        VirCtl = Recherche_WebControle(N6, Prefixe, Index)
                                        If VirCtl IsNot Nothing Then
                                            Return VirCtl
                                        End If
                                        For IndiceN7 = 0 To N6.Controls.Count - 1
                                            N7 = N6.Controls.Item(IndiceN7)
                                            VirCtl = Recherche_WebControle(N7, Prefixe, Index)
                                            If VirCtl IsNot Nothing Then
                                                Return VirCtl
                                            End If
                                            For IndiceN8 = 0 To N7.Controls.Count - 1
                                                N8 = N7.Controls.Item(IndiceN8)
                                                VirCtl = Recherche_WebControle(N8, Prefixe, Index)
                                                If VirCtl IsNot Nothing Then
                                                    Return VirCtl
                                                End If
                                                For IndiceN9 = 0 To N8.Controls.Count - 1
                                                    N9 = N8.Controls.Item(IndiceN9)
                                                    VirCtl = Recherche_WebControle(N9, Prefixe, Index)
                                                    If VirCtl IsNot Nothing Then
                                                        Return VirCtl
                                                    End If
                                                    For IndiceN10 = 0 To N9.Controls.Count - 1
                                                        N10 = N9.Controls.Item(IndiceN10)
                                                        VirCtl = Recherche_WebControle(N10, Prefixe, Index)
                                                        If VirCtl IsNot Nothing Then
                                                            Return VirCtl
                                                        End If
                                                    Next IndiceN10
                                                Next IndiceN9
                                            Next IndiceN8
                                        Next IndiceN7
                                    Next IndiceN6
                                Next IndiceN5
                            Next IndiceN4
                        Next IndiceN3
                    Next IndiceN2
                Next IndiceN1
            Next IndiceI

            Return Nothing
        End Get
    End Property

    Private ReadOnly Property Recherche_WebControle(ByVal Conteneur As Control, ByVal Prefixe As String, ByVal Index As Integer) As Control
        Get
            Dim IndiceI As Integer
            Dim Ctl As Control
            Dim Lg As Integer = Prefixe.Length
            For IndiceI = 0 To Conteneur.Controls.Count - 1
                Ctl = Conteneur.Controls.Item(IndiceI)
                Select Case Strings.Left(Ctl.ID, Lg)
                    Case Is = Prefixe
                        WsCpt += 1
                        If WsCpt = Index Then
                            Return Ctl
                        End If
                End Select
            Next IndiceI
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property PointeurDicoObjet(ByVal PointdeVue As Integer, ByVal Numobjet As Integer) As Virtualia.Systeme.MetaModele.Donnees.Objet
        Get
            If PointdeVue = 0 Or Numobjet = 0 Then
                Return Nothing
                Exit Property
            End If
            Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet)
            Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire)
            Dico = AppObjetGlobal.VirListeObjetsDico.FindAll(AddressOf Predicat.InformationParIdeObjet)
            If Dico.Count = 0 Then
                Return Nothing
                Exit Property
            End If
            Return Dico.Item(0).PointeurModele
        End Get
    End Property

    Public ReadOnly Property PointeurDicoInfo(ByVal PointdeVue As Integer, ByVal Numobjet As Integer, ByVal NumInfo As Integer) As Virtualia.Systeme.MetaModele.Donnees.Information
        Get
            If PointdeVue = 0 Or Numobjet = 0 Then
                Return Nothing
                Exit Property
            End If
            Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet, NumInfo)
            Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
            Dico = AppObjetGlobal.VirListeInfosDico.FindAll(AddressOf Predicat.InformationParIdeInfo)
            If Dico.Count = 0 Then
                Return Nothing
                Exit Property
            End If
            Return Dico.Item(0).PointeurModele
        End Get
    End Property

    Public Function RechercherFenetresObjet(ByVal SiFenVirtualia As Boolean, ByVal PrefixePvue As String) As String
        '----- Recherche sur 11 niveaux des Objets "PER", "GRD" etc --------

        Select Case SiFenVirtualia
            Case True
                ReDim WsListeCtl(20)
            Case False
                ReDim WsListeCtl(200)
        End Select
        Dim ICtl As Integer = 0

        Dim IndiceA As Integer
        Dim IndiceB As Integer
        Dim IndiceC As Integer
        Dim IndiceD As Integer
        Dim IndiceE As Integer
        Dim IndiceF As Integer
        Dim IndiceG As Integer
        Dim IndiceH As Integer
        Dim IndiceI As Integer
        Dim IndiceJ As Integer
        Dim IndiceK As Integer
        Dim N0 As Control
        Dim N1 As Control
        Dim N2 As Control
        Dim N3 As Control
        Dim N4 As Control
        Dim N5 As Control
        Dim N6 As Control
        Dim N7 As Control
        Dim N8 As Control
        Dim N9 As Control
        Dim N10 As Control
        Dim Chaine As String = ""

        If WsHostP Is Nothing Then
            Return ""
            Exit Function
        End If

        Dim FrmConteneur As Control = WsHostP.Page.Form.FindControl("CorpsMaster")
        Dim TableSaisie As Control = Nothing

        If FrmConteneur IsNot Nothing Then
            Select Case SiFenVirtualia
                Case True
                    TableSaisie = FrmConteneur.FindControl("CadreSaisie")
                Case False
                    TableSaisie = WsHostP.Page.Form
            End Select
            If TableSaisie IsNot Nothing Then
                For IndiceA = 0 To TableSaisie.Controls.Count - 1
                    N0 = TableSaisie.Controls.Item(IndiceA)
                    Select Case SiFenVirtualia
                        Case False
                            WsListeCtl(ICtl) = N0
                            Chaine &= N0.ID & " -N0-" & IndiceA.ToString & "- "
                        Case True
                            If SiTrouveControle(N0.ID, PrefixePvue) Then
                                WsListeCtl(ICtl) = N0
                                Chaine &= N0.ID & " -N0-" & IndiceA.ToString & "- "
                                ICtl += 1
                            End If
                    End Select
                    For IndiceB = 0 To N0.Controls.Count - 1
                        N1 = N0.Controls.Item(IndiceB)
                        Select Case SiFenVirtualia
                            Case False
                                WsListeCtl(ICtl) = N1
                                Chaine &= N1.ID & " -N1-" & IndiceB.ToString & "- "
                            Case True
                                If SiTrouveControle(N1.ID, PrefixePvue) Then
                                    WsListeCtl(ICtl) = N1
                                    Chaine &= N1.ID & " -N1-" & IndiceB.ToString & "- "
                                    ICtl += 1
                                End If
                        End Select
                        For IndiceC = 0 To N1.Controls.Count - 1
                            N2 = N1.Controls.Item(IndiceC)
                            Select Case SiFenVirtualia
                                Case False
                                    WsListeCtl(ICtl) = N2
                                    Chaine &= N2.ID & " -N2-" & IndiceC.ToString & "- "
                                Case True
                                    If SiTrouveControle(N2.ID, PrefixePvue) Then
                                        WsListeCtl(ICtl) = N2
                                        Chaine &= N2.ID & " -N2-" & IndiceC.ToString & "- "
                                        ICtl += 1
                                    End If
                            End Select
                            For IndiceD = 0 To N2.Controls.Count - 1
                                N3 = N2.Controls.Item(IndiceD)
                                Select Case SiFenVirtualia
                                    Case False
                                        WsListeCtl(ICtl) = N3
                                        Chaine &= N3.ID & " -N3-" & IndiceD.ToString & "- "
                                    Case True
                                        If SiTrouveControle(N3.ID, PrefixePvue) Then
                                            WsListeCtl(ICtl) = N3
                                            Chaine &= N3.ID & " -N3-" & IndiceD.ToString & "- "
                                            ICtl += 1
                                        End If
                                End Select
                                For IndiceE = 0 To N3.Controls.Count - 1
                                    N4 = N3.Controls.Item(IndiceE)
                                    Select Case SiFenVirtualia
                                        Case False
                                            WsListeCtl(ICtl) = N4
                                            Chaine &= N4.ID & " -N4-" & IndiceE.ToString & "- "
                                        Case True
                                            If SiTrouveControle(N4.ID, PrefixePvue) Then
                                                WsListeCtl(ICtl) = N4
                                                Chaine &= N4.ID & " -N4-" & IndiceE.ToString & "- "
                                                ICtl += 1
                                            End If
                                    End Select
                                    For IndiceF = 0 To N4.Controls.Count - 1
                                        N5 = N4.Controls.Item(IndiceF)
                                        Select Case SiFenVirtualia
                                            Case False
                                                WsListeCtl(ICtl) = N5
                                                Chaine &= N5.ID & " -N5-" & IndiceF.ToString & "- "
                                            Case True
                                                If SiTrouveControle(N5.ID, PrefixePvue) Then
                                                    WsListeCtl(ICtl) = N5
                                                    Chaine &= N5.ID & " -N5-" & IndiceF.ToString & "- "
                                                    ICtl += 1
                                                End If
                                        End Select
                                        For IndiceG = 0 To N5.Controls.Count - 1
                                            N6 = N5.Controls.Item(IndiceG)
                                            Select Case SiFenVirtualia
                                                Case False
                                                    WsListeCtl(ICtl) = N6
                                                    Chaine &= N6.ID & " -N6-" & IndiceG.ToString & "- "
                                                Case True
                                                    If SiTrouveControle(N6.ID, PrefixePvue) Then
                                                        WsListeCtl(ICtl) = N6
                                                        Chaine &= N6.ID & " -N6-" & IndiceG.ToString & "- "
                                                        ICtl += 1
                                                    End If
                                            End Select
                                            For IndiceH = 0 To N6.Controls.Count - 1
                                                N7 = N6.Controls.Item(IndiceH)
                                                Select Case SiFenVirtualia
                                                    Case False
                                                        WsListeCtl(ICtl) = N7
                                                        Chaine &= N7.ID & " -N7-" & IndiceH.ToString & "- "
                                                    Case True
                                                        If SiTrouveControle(N7.ID, PrefixePvue) Then
                                                            WsListeCtl(ICtl) = N7
                                                            Chaine &= N7.ID & " -N7-" & IndiceH.ToString & "- "
                                                            ICtl += 1
                                                        End If
                                                End Select
                                                For IndiceI = 0 To N7.Controls.Count - 1
                                                    N8 = N7.Controls.Item(IndiceI)
                                                    Select Case SiFenVirtualia
                                                        Case False
                                                            WsListeCtl(ICtl) = N8
                                                            Chaine &= N8.ID & " -N8-" & IndiceI.ToString & "- "
                                                        Case True
                                                            If SiTrouveControle(N8.ID, PrefixePvue) Then
                                                                WsListeCtl(ICtl) = N8
                                                                Chaine &= N8.ID & " -N8-" & IndiceI.ToString & "- "
                                                                ICtl += 1
                                                            End If
                                                    End Select
                                                    For IndiceJ = 0 To N8.Controls.Count - 1
                                                        N9 = N8.Controls.Item(IndiceJ)
                                                        Select Case SiFenVirtualia
                                                            Case False
                                                                WsListeCtl(ICtl) = N9
                                                                Chaine &= N9.ID & " -N9-" & IndiceJ.ToString & "- "
                                                            Case True
                                                                If SiTrouveControle(N9.ID, PrefixePvue) Then
                                                                    WsListeCtl(ICtl) = N9
                                                                    Chaine &= N9.ID & " -N9-" & IndiceJ.ToString & "- "
                                                                    ICtl += 1
                                                                End If
                                                        End Select
                                                        For IndiceK = 0 To N9.Controls.Count - 1
                                                            N10 = N9.Controls.Item(IndiceK)
                                                            Select Case SiFenVirtualia
                                                                Case False
                                                                    WsListeCtl(ICtl) = N10
                                                                    Chaine &= N10.ID & " -N10-" & IndiceK.ToString & "- "
                                                                Case True
                                                                    If SiTrouveControle(N10.ID, PrefixePvue) Then
                                                                        WsListeCtl(ICtl) = N10
                                                                        Chaine &= N10.ID & " -N10-" & IndiceK.ToString & "- "
                                                                        ICtl += 1
                                                                    End If
                                                            End Select
                                                        Next IndiceK
                                                    Next IndiceJ
                                                Next IndiceI
                                            Next IndiceH
                                        Next IndiceG
                                    Next IndiceF
                                Next IndiceE
                            Next IndiceD
                        Next IndiceC
                    Next IndiceB
                Next IndiceA
            End If
        End If
        '---------------Fin Recherche Objets PER---------------------
        Return Chaine
    End Function

    Private Function SiTrouveControle(ByVal IDControle As String, ByVal Prefixe As String) As Boolean
        If IDControle Is Nothing Then
            Return False
            Exit Function
        End If
        If IDControle.Length > 4 Then
            If Strings.Left(IDControle, 4) = Prefixe & "_" Then
                Return True
                Exit Function
            End If
        End If
        Return False
    End Function

    Public Function VerifierCookie(ByVal CtlPage As System.Web.UI.Page) As Boolean
        Dim CacheIdent As CacheIdentification = LireCookie(CtlPage)
        If CacheIdent Is Nothing Then
            Return False
        End If
        If CacheIdent.NumeroSession = CtlPage.Session.SessionID Then
            Return True
        End If
        Return False
    End Function

    Public Sub EcrireCookie(ByVal CtlPage As System.Web.UI.Page, ByVal Valeur As CacheIdentification)
        Dim Vcookie As System.Web.HttpCookie

        Vcookie = CtlPage.Request.Cookies.Get("MyEntretienPro")
        If Vcookie Is Nothing Then
            Vcookie = New System.Web.HttpCookie("MyEntretienPro")
        End If
        Vcookie.Value = EncodageCookie(Valeur.Chaine_Cookie)
        Vcookie.Expires = DateTime.Now.AddDays(7)
        CtlPage.Response.Cookies.Add(Vcookie)
    End Sub

    Public Function LireCookie(ByVal CtlPage As System.Web.UI.Page) As CacheIdentification
        Dim Vcookie As System.Web.HttpCookie
        Dim CacheIdent As New CacheIdentification

        Vcookie = CtlPage.Request.Cookies.Get("MyEntretienPro")
        If Vcookie Is Nothing OrElse Vcookie.Value Is Nothing Then
            Return Nothing
        End If
        CacheIdent.Chaine_Cookie = DecodageCookie(Vcookie.Value)
        Return CacheIdent
    End Function

    Private ReadOnly Property EncodageCookie(ByVal Valeur As String) As String
        Get
            Dim TableauByte() As Byte
            TableauByte = System.Text.Encoding.UTF8.GetBytes(Valeur)
            Return BitConverter.ToString(TableauByte)
        End Get
    End Property

    Private ReadOnly Property DecodageCookie(ByVal Valeur As String) As String
        Get
            Dim I As Integer
            Dim Tableaudata() As String = Strings.Split(Valeur, "-")
            Dim TableauByte() As Byte
            ReDim TableauByte(Tableaudata.Length - 1)
            Try
                For I = 0 To TableauByte.Count - 1
                    TableauByte(I) = Convert.ToByte(Tableaudata(I), 16)
                Next I
                Return System.Text.Encoding.UTF8.GetString(TableauByte)
            Catch ex As Exception
                Return Valeur
            End Try
        End Get
    End Property

    Private Sub InitialiserSession()
        Dim Init As Integer = 0
        Select Case WsTypeControle
            Case TypeUC
                AppObjetGlobal = CType(WsHostUC.Application.Item("VirGlobales"), Virtualia.Net.WebAppli.ObjetGlobal)
            Case TypeP
                AppObjetGlobal = CType(WsHostP.Application.Item("VirGlobales"), Virtualia.Net.WebAppli.ObjetGlobal)
            Case TypeMP
                AppObjetGlobal = CType(WsHostMP.Application.Item("VirGlobales"), Virtualia.Net.WebAppli.ObjetGlobal)
        End Select
        If AppObjetGlobal Is Nothing Then
            Dim VirObjetGlobal As Virtualia.Net.WebAppli.ObjetGlobal
            Dim NoBd As Integer = CShort(System.Configuration.ConfigurationManager.AppSettings("NumeroDatabase"))
            Try
                VirObjetGlobal = New Virtualia.Net.WebAppli.ObjetGlobal("Virtualia", NoBd)
                Application.Add("VirGlobales", VirObjetGlobal)
            Catch ex As Exception
                Me.Dispose()
                Exit Sub
            End Try
            Exit Sub
        End If

    End Sub

    Public Sub New(ByVal Host As Object, ByVal TypeControle As Integer)
        WsNomUti = "Virtualia"
        WsTypeControle = TypeControle
        Select Case WsTypeControle
            Case TypeUC
                WsHostUC = CType(Host, System.Web.UI.UserControl)
            Case TypeP
                WsHostP = CType(Host, System.Web.UI.Page)
            Case TypeMP
                WsHostMP = CType(Host, System.Web.UI.MasterPage)
        End Select
        Call InitialiserSession()
        WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
    End Sub

    Public ReadOnly Property PoliceCharte() As String
        Get
            Return "Trebuchet MS"
        End Get
    End Property

    Public ReadOnly Property CouleurCharte(ByVal PointdeVue As Integer, ByVal NomCouleur As String) As System.Drawing.Color
        Get
            Dim NumCharte As Integer
            If WsCharte Is Nothing Then
                WsCharte = New Virtualia.Systeme.Fonctions.CharteGraphique
            End If
            Select Case PointdeVue
                Case 0 ' Fenetre PER BIS
                    NumCharte = 2
                Case VI.PointdeVue.PVueApplicatif
                    NumCharte = 0
                Case Else
                    NumCharte = 1
            End Select
            Return WsCharte.CouleurParSonNom(NumCharte, NomCouleur)
        End Get
    End Property

    Public ReadOnly Property CouleurMaj() As System.Drawing.Color
        Get
            Return ConvertCouleur("DEFAD7")
        End Get
    End Property

    Public Function ConvertCouleur(ByVal Valeur As String) As System.Drawing.Color
        Dim R As Integer
        Dim G As Integer
        Dim B As Integer
        Select Case Valeur.Length
            Case Is = 6
                R = CInt(Val("&H" & Strings.Left(Valeur, 2) & "&"))
                G = CInt(Val("&H" & Strings.Mid(Valeur, 3, 2) & "&"))
                B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                Return System.Drawing.Color.FromArgb(R, G, B)
            Case Is = 7
                R = CInt(Val("&H" & Strings.Mid(Valeur, 2, 2) & "&"))
                G = CInt(Val("&H" & Strings.Mid(Valeur, 4, 2) & "&"))
                B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                Return System.Drawing.Color.FromArgb(R, G, B)
            Case Else
                Return Drawing.Color.White
        End Select
    End Function

    Public Function CouleurHexadecimale(ByVal Valeur As System.Drawing.Color) As String
        Dim R As Byte = Valeur.R
        Dim G As Byte = Valeur.G
        Dim B As Byte = Valeur.B
        Dim Chaine As String = Hex(R) & Hex(G) & Hex(B)
        Return "#" & Chaine
    End Function

    '--- CallBack -------
    'Méthode de rappel de serveur Exemple testé et sauvegardé

    '<%@ Implements Interface="System.Web.UI.ICallbackEventHandler" %>'

    'Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
    '    Return WsResultCallBack
    'End Function

    'Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
    'Dim argValeurs() As String = eventArgument.Split("|"c)
    '   If argValeurs Is Nothing Then
    '        WsResultCallBack = "0"
    '        Return
    '    End If
    'Dim Ide As String = argValeurs(0)
    'Dim ValidAction As String = argValeurs(1)
    '    Select Case ValidAction
    '        Case "NouvelIdentifiant"
    '            Page.ClientScript.ValidateEvent("BoutonValid", ValidAction)
    '            WsResultCallBack = Ide
    '    End Select
    '
    'End Sub

    'Création de fonctions de script client
    'Private Sub CreationScriptDynamique()
    '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NouvelIdentifiant", _
    '                                                 "function NouvelIdentifiant() { " & _
    '                                                 "var ide = document.forms(0).ctl00_CorpsMaster_ResCallBack.value; " & _
    '                                                 "CallServer(ide, ""NouvelIdentifiant"");} ", True)
    '
    'Dim RefManager As ClientScriptManager = Page.ClientScript
    'Dim RefCallBack As String = "var param = arg + '|' + context;"
    '    RefCallBack &= RefManager.GetCallbackEventReference(Me, "param", "ReceiveServerData", "context")
    'Dim RefScriptCallBack As String = ""
    '    RefScriptCallBack &= "function CallServer (arg, context)" & "{" & RefCallBack & "; }"
    '    RefManager.RegisterClientScriptBlock(Me.GetType, "CallServer", RefScriptCallBack, True)
    '
    'End Sub

    'Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
    '    Page.ClientScript.RegisterForEventValidation("BoutonValid", "NouvelIdentifiant")
    '    MyBase.Render(writer)
    'End Sub
    '
End Class
