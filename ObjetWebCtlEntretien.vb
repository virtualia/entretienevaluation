﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Microsoft.VisualBasic
Namespace Controles
    Public Class ObjetWebCtlEntretien
        Inherits System.Web.UI.UserControl
        Public Delegate Sub AppelTableEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
        Public Event AppelTable As AppelTableEventHandler
        Public Delegate Sub MessageEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
        Public Event MessageSaisie As MessageEventHandler
        Public Delegate Sub MsgDialog_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
        Public Event MessageDialogue As MsgDialog_MsgEventHandler
        '
        Private WsNomStateUti As String = "Utilisateur"
        Private WsCacheUti As ArrayList
        Private WebFct As Virtualia.Net.WebFonctions
        '
        Private WsPvue As Integer = VI.PointdeVue.PVueApplicatif

        Private WsLibelListe As String
        Private WsLibelColonne As String
        Private WsTabColonnes(9) As Integer
        '
        Protected Overridable Sub V_MessageSaisie(ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
            RaiseEvent MessageSaisie(Me, e)
        End Sub

        Protected Overridable Sub V_MessageDialog(ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
            RaiseEvent MessageDialogue(Me, e)
        End Sub

        Protected Overridable Sub V_AppelTable(ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
            RaiseEvent AppelTable(Me, e)
        End Sub

        Protected Overridable Sub V_DonTab_AppelTable(ByVal NumInfo As Integer, ByVal IDAppelant As String, ByVal ObjetAppelant As Integer, ByVal PvueInverse As Integer, ByVal Nomdelatable As String)
            Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
            Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(IDAppelant, ObjetAppelant, PvueInverse, Nomdelatable)
            V_AppelTable(Evenement)
        End Sub

        Public Property V_NomUtilisateur As String
            Get
                If Me.ViewState(WsNomStateUti) Is Nothing Then
                    Return ""
                End If
                WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
                If WsCacheUti IsNot Nothing Then
                    Return WsCacheUti(0).ToString
                Else
                    Return ""
                End If
            End Get
            Set(ByVal value As String)
                If Me.ViewState(WsNomStateUti) IsNot Nothing Then
                    Me.ViewState.Remove(WsNomStateUti)
                End If
                WsCacheUti = New ArrayList
                WsCacheUti.Add(value)
                Me.ViewState.Add(WsNomStateUti, WsCacheUti)
            End Set
        End Property

        Public ReadOnly Property V_ListeFiches() As String
            Get
                Dim DossierPER As Virtualia.Net.Entretien.DossierEntretien
                DossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If DossierPER Is Nothing Then
                    Return Nothing
                End If
                Return DossierPER.ListeDesEntretiens
            End Get
        End Property

        Public ReadOnly Property V_WebFonction() As Virtualia.Net.WebFonctions
            Get
                If WebFct Is Nothing Then
                    WebFct = New Virtualia.Net.WebFonctions(Me, 0)
                End If
                Return WebFct
            End Get
        End Property

        Public Property V_Identifiant() As Integer
            Get
                Dim DossierPER As Virtualia.Net.Entretien.DossierEntretien
                DossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If DossierPER Is Nothing Then
                    Return 0
                Else
                    Return DossierPER.V_Identifiant
                End If
            End Get
            Set(ByVal value As Integer)
                If value = 0 Then
                    Exit Property
                End If
                Dim DossierPER As Virtualia.Net.Entretien.DossierEntretien
                DossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If DossierPER Is Nothing Then
                    DossierPER = New Virtualia.Net.Entretien.DossierEntretien(V_WebFonction.PointeurGlobal, value, V_WebFonction.PointeurUtilisateur.Nom)
                Else
                    If DossierPER.V_Identifiant <> value Then
                        DossierPER = New Virtualia.Net.Entretien.DossierEntretien(V_WebFonction.PointeurGlobal, value, V_WebFonction.PointeurUtilisateur.Nom)
                    End If
                End If
                V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation = DossierPER
            End Set
        End Property

        Public WriteOnly Property V_Occurence() As String
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim DossierPER As Virtualia.Net.Entretien.DossierEntretien
                DossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If DossierPER Is Nothing Then
                    Exit Property
                End If
                DossierPER.Annee = Strings.Right(value, 4)
            End Set
        End Property

        Public Property V_LibelListe() As String
            Get
                Return WsLibelListe
            End Get
            Set(ByVal value As String)
                WsLibelListe = value
            End Set
        End Property

        Public Property V_LibelColonne() As String
            Get
                Return WsLibelColonne
            End Get
            Set(ByVal value As String)
                WsLibelColonne = value
            End Set
        End Property

        Public Overridable Sub V_MajFiche()
            Dim ChaineLue As String = ""
            Dim Cretour As Boolean
            Dim MajOK As Virtualia.Systeme.Evenements.MessageSaisieEventArgs

            Dim DossierPER As Virtualia.Net.Entretien.DossierEntretien
            DossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
            If DossierPER Is Nothing Then
                Exit Sub
            End If
            Cretour = DossierPER.MettreAJour
            MajOK = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(Me.ID, 150, DossierPER.V_Identifiant.ToString, "OK", "Mise à jour effectuée", "")
            V_MessageSaisie(MajOK)
        End Sub

        Protected Overridable Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            WsLibelListe = ""
        End Sub

        Protected Overridable Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            WsLibelListe = ""
        End Sub

        Public Sub New()
            Dim I As Integer
            For I = 0 To WsTabColonnes.Count - 1
                WsTabColonnes(I) = -1
            Next I
        End Sub
    End Class
End Namespace

