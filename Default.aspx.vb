﻿Option Explicit On
Option Compare Text
Option Strict On
''**** AKR LOG
Imports VI = Virtualia.Systeme.Constantes

Public Class _Default
    Inherits System.Web.UI.Page
    Private VirObjetGlobal As Virtualia.Net.WebAppli.ObjetGlobal
    Private WebFct As Virtualia.Net.WebFonctions
    Private SiV3_7 As Boolean = False
    Private SiTypeAccesManager As Boolean = False
    Private SiTypeAccesManagerN2 As Boolean = False
    Private SiTypeAccesManagerEtN2 As Boolean = False
    Private SiTypeAccesGRH As Boolean = False
    Private WsAppliAccedee As String
    Private WsNomUser As String = ""

    ''**** AKR LOG
    Private Const SysFicLog As String = "WebSelfSalarie.log"
    Private SysCodeIso As System.Text.Encoding = System.Text.Encoding.UTF8

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ChaineIntranetV3 As String = ""
        Dim ChaineClient As String = ""
        Dim TabPOST(0) As String
        Dim Identifiant As String = ""
        Dim NomEvaluateur As String = ""
        Dim AdresseDuSite As String = ""
        Dim TabAdresseDuSite() As String
        Dim Unite As String = ""
        Dim IndiceUnite As Integer = 0
        Dim ObjetUtilisateur As Virtualia.Net.Datas.ObjetUtiAnonyme = Nothing
        Dim Msg As String = "Erreur au lancement de l'application."

        Dim ObjetClefV3 As Virtualia.Net.Session.ClefV3Connexion = Nothing

        WebFct = New Virtualia.Net.WebFonctions(Me, 1)

        '************* Version 3.7 **********************************************************************
        ChaineIntranetV3 = Request.Form("ClefV4") 'Issu du POST - Intranet RH
        If ChaineIntranetV3 Is Nothing OrElse ChaineIntranetV3 = "" Then
            ChaineIntranetV3 = Request.QueryString("ClefV4") 'Issu du GET - VirtualiaRHV2.exe
        End If

        ''*** AKR uniquement pour AFB 
        '' Récupération de l'ide Evaluateur
        Dim ChaineEval = Request.Form("IdentifiantManager") 'Issu du POST - Intranet RH
        If ChaineEval Is Nothing OrElse ChaineEval = "" Then
            ChaineEval = Request.QueryString("IdentifiantManager") 'Issu du GET - VirtualiaRHV2.exe
        End If
        ''*****

        '** Uniquement pour test
        '' AFB (utiliser la BD 10 (CNED) matricule 1080)
        'BD ENM (10) dossier 1076
        Dim ObjetTest As New Virtualia.Net.Session.ClefV3Connexion("")
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("Virtualia", "ExtractionEntretien", 0)
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("", "ManagerEntretien", 10950)


        ChaineIntranetV3 = ObjetTest.Chaine_Test("", "ManagerEntretien", 104365)
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("", "SelfEntretien", 104365)

        'ChaineIntranetV3 = ObjetTest.Chaine_Test("Virtualia", "DRH", 477)
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("", "SelfEntretien", 1080)
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("", "ManagerEntretien", 459)
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("", "ManagerEntretien", 6)
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("Virtualia", "DRH", 1080)

        'BD CNM (15) dossier 53
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("", "SelfEntretien", 53)
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("", "ManagerEntretien", 53)
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("Virtualia", "DRH", 24)
        '**

        If ChaineIntranetV3 IsNot Nothing AndAlso ChaineIntranetV3 <> "" Then
            SiV3_7 = True
            ObjetClefV3 = New Virtualia.Net.Session.ClefV3Connexion(ChaineIntranetV3)
        End If
        '************************************************************************************************
        If SiV3_7 = False Then
            ChaineIntranetV3 = Request.Form("ChainePOST") 'Issu du POST - Intranet RH
            If ChaineIntranetV3 = "" Then
                ChaineIntranetV3 = Request.QueryString("ChainePOST") 'Issu du GET - VirtualiaRHV2.exe
                If ChaineIntranetV3 <> "" Then
                    SiTypeAccesGRH = True
                End If
            End If
            ChaineClient = Request.QueryString("CLEF")
            '** Uniquement pour tests
            'ChaineIntranetV3 = "IntranetRhVTAPOSTSelfSalarieVTAPOSTVTAPOSTCEVTAPOST12665VTAPOSTPAULHEVTAPOST72" 'Self Salarie Public
            ''***** AKR décomenter manager public
            'ChaineIntranetV3 = "IntranetRhVTAPOSTemanagementVTAPOSTVTAPOSTCEVTAPOST12665VTAPOSTPAULHEVTAPOST19143" 'Manager Public
            'ChaineClient = Strings.Replace(WebFct.PointeurGlobal.VirModele.InstanceProduit.ClefProduit, "-", "")
            '**
            Select Case SiTypeAccesGRH
                Case False
                    If (ChaineIntranetV3 Is Nothing OrElse ChaineIntranetV3 = "") AndAlso (ChaineClient Is Nothing OrElse ChaineClient = "") Then
                        Msg &= " Demande non valide"
                        Response.Redirect("~/ErreurApplication.aspx?Msg=" & Msg)
                        Exit Sub
                    End If
            End Select
        End If
        '*************************************************************************************************

        If ChaineIntranetV3 = "" And ChaineClient = "" Then
            Response.Redirect("~/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If

        VirObjetGlobal = CType(Application("VirGlobales"), Virtualia.Net.WebAppli.ObjetGlobal)

        Select Case SiV3_7
            Case True
                If ObjetClefV3.SiAccesOK(VirObjetGlobal) = False Then
                    Msg &= " Accés invalide. " & ObjetClefV3.Detail_ErreurCnx
                    Response.Redirect("ErreurApplication.aspx?Msg=" & Msg)
                    Exit Sub
                End If
                SiTypeAccesManager = ObjetClefV3.SiAcces_Manager
                SiTypeAccesGRH = ObjetClefV3.SiAcces_DRH
                WsAppliAccedee = ObjetClefV3.Appli_Accedee
                Identifiant = CStr(ObjetClefV3.Identifiant)
                ''**** AKR AFB
                ' ChaineEval = "169"
                If ChaineEval <> "" And SiTypeAccesManager = True Then
                    SiTypeAccesManagerN2 = ObjetClefV3.SiAcces_ManagerN2(VirObjetGlobal, ChaineEval)
                End If
                SiTypeAccesManagerEtN2 = ObjetClefV3.SiAcces_ManagerEtN2
                ''**** AKR

                If WsAppliAccedee.StartsWith("Extraction") = True And SiTypeAccesGRH = True Then
                    If WebFct.PointeurGlobal.VirModele.InstanceProduit.NumeroLicence = "2209158" Then 'HONDA
                        Response.Redirect("Extractions/FrmExtractHonda.aspx")
                    ElseIf WebFct.PointeurGlobal.PointeurEnsemble Is Nothing Then
                        Response.Redirect("Extractions/FrmInitialisation.aspx")
                    Else
                        Response.Redirect("Extractions/FrmExtractions.aspx")
                    End If
                    Exit Sub
                End If
                WsNomUser = Identifiant & "_" & Strings.Replace(Format(TimeOfDay, "t"), ":", "0")
                ObjetUtilisateur = VirObjetGlobal.AjouterSessionVirtualia(Session.SessionID, WsNomUser)
                ObjetUtilisateur.SiDRH = SiTypeAccesGRH
                ''****** AKR 24/10/2017
                ''If SiTypeAccesManager = False Then
                If SiTypeAccesManager = False And SiTypeAccesGRH = False Then
                    ObjetUtilisateur.SiSelf = True
                    ObjetUtilisateur.SiManager = False
                Else
                    ObjetUtilisateur.SiSelf = False
                    ObjetUtilisateur.SiManager = True
                End If
                ''****** AKR AFB 07/05/2018
                If SiTypeAccesManagerN2 = True Then
                    ObjetUtilisateur.SiN2 = True
                    If SiTypeAccesManagerEtN2 = True Then
                        ObjetUtilisateur.SiManager = True
                    Else
                        ObjetUtilisateur.SiManager = False
                    End If
                Else
                    ObjetUtilisateur.SiN2 = False
                End If

            Case False
                If ChaineClient <> "" Then
                    If ChaineClient <> Strings.Replace(WebFct.PointeurGlobal.VirModele.InstanceProduit.ClefProduit, "-", "") Then
                        Msg &= " Clef invalide"
                        Response.Redirect("ErreurApplication.aspx?Msg=" & Msg)
                        Exit Sub
                    End If
                    If WebFct.PointeurGlobal.VirModele.InstanceProduit.NumeroLicence = "2209158" Then 'HONDA
                        Response.Redirect("Extractions/FrmExtractHonda.aspx")
                    Else
                        If WebFct.PointeurGlobal.PointeurEnsemble Is Nothing Then
                            Response.Redirect("Extractions/FrmInitialisation.aspx")
                        Else
                            Response.Redirect("Extractions/FrmExtractions.aspx")
                        End If
                    End If
                    Exit Sub
                End If
                TabPOST = Strings.Split(ChaineIntranetV3, "VTAPOST")
                Identifiant = TabPOST(4)
                WsNomUser = Identifiant & "_" & Strings.Replace(Format(TimeOfDay, "t"), ":", "0")
                ObjetUtilisateur = VirObjetGlobal.AjouterSessionVirtualia(Session.SessionID, WsNomUser)
                ObjetUtilisateur.SiDRH = SiTypeAccesGRH
                If TabPOST(1) = "SelfSalarie" Then
                    ObjetUtilisateur.SiSelf = True
                Else
                    ObjetUtilisateur.SiSelf = False
                End If
        End Select
        If VirObjetGlobal.VirTypeEntretien = "CE" AndAlso ObjetUtilisateur.SiDRH = True Then
            ObjetUtilisateur.SiSelf = False
        End If
        '****************************************************************************************************

        ObjetUtilisateur.CheminServeur = Request.PhysicalApplicationPath
        TabAdresseDuSite = Split(Request.Url.AbsoluteUri, "/", -1)
        For IndiceI = 0 To UBound(TabAdresseDuSite) - 1
            AdresseDuSite = AdresseDuSite & TabAdresseDuSite(IndiceI) & "/"
        Next
        ObjetUtilisateur.AdresseduSite = Strings.Left(AdresseDuSite, Len(AdresseDuSite) - 1)

        Unite = ObjetUtilisateur.CheminServeur
        IndiceUnite = 1
        Do While Mid(Unite, IndiceUnite, 1) <> "\"
            IndiceUnite = IndiceUnite + 1
        Loop
        Unite = Left(Unite, IndiceUnite)
        ObjetUtilisateur.UniteDisk = Unite

        WebFct.ContexteSession(Session.SessionID).Identifiant_Courant = CInt(Identifiant)
        WebFct.ContexteSession(Session.SessionID).Fenetre_VueActive(8) = 0

        ObjetUtilisateur.IDSession = Session.SessionID
        Dim CacheCookie As CacheIdentification = WebFct.LireCookie(Me)
        If CacheCookie Is Nothing Then
            CacheCookie = New CacheIdentification
        End If
        CacheCookie.Nom_Identification = WsNomUser
        CacheCookie.NumeroSession = Session.SessionID
        WebFct.EcrireCookie(Me, CacheCookie)

        'Nettoyer Répertoire téléchargement
        Dim Repertoire As String = ObjetUtilisateur.CheminServeur
        If Strings.Right(WebFct.SessionVirtualia(Session.SessionID).CheminServeur, 1) <> "\" Then
            Repertoire &= "\"
        End If
        Repertoire &= "Telechargement\" & Identifiant
        If My.Computer.FileSystem.DirectoryExists(Repertoire) = True Then
            Try
                My.Computer.FileSystem.DeleteDirectory(Repertoire, FileIO.DeleteDirectoryOption.DeleteAllContents)
            Catch ex As Exception
                Exit Try
            End Try
        End If
        If WsNomUser <> "" Then
            Response.Redirect(VirObjetGlobal.Adresse_FrmEntretien & "?Nomuser=" & WsNomUser)
        End If
    End Sub

    '*** AKR LOG
    Private Shared ReadOnly LogSync As New Object()
    Private Sub EcrireLog(ByVal Msg As String)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim NomLog As String
        SyncLock LogSync
            NomLog = VI.DossierVirtualiaService("Logs") & SysFicLog
            FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
            FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
            Try
                FicWriter.WriteLine(Format(System.DateTime.Now, "g") & Space(1) & Msg)
                FicWriter.Flush()
            Finally
                FicWriter.Close()
            End Try
        End SyncLock
    End Sub

    ''*** FIN AKR LOG

End Class