﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P0_ONISEP
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
            End If
        End Set
    End Property

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim Tableaudata(0) As String
        Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)

        O_CadreLogoMEN.BackImageUrl = Tableaudata(0) & "/Images/General/LogoMEN.jpg"
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim IndiceI As Integer
        Dim IndiceK As Integer
        Dim Chaine As String
        Dim Chaine1 As String = ""
        Dim Chaine2 As String = ""
        Dim ChaineManager As List(Of String)

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        O_LabelAnneeEntretien.Text = "ANNEE " & WsDossierPER.Annee
        O_LabelIdentite.Text = WsDossierPER.Nom & " " & WsDossierPER.Prenom
        O_LabelPatronyme.Text = WsDossierPER.NomPatronymique
        O_LabelNaissance.Text = WsDossierPER.LibelleNaissance
        O_LabelStatut.Text = WsDossierPER.Statut(DatedEffet)
        O_LabelGrade.Text = WsDossierPER.Grade(DatedEffet)
        O_LabelEchelon.Text = WsDossierPER.Echelon(DatedEffet)
        O_LabelDateEchelon.Text = V_WebFonction.InfoExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, 678, WsDossierPER.V_Identifiant, DatedEffet)

        IndiceK = 0
        For IndiceI = 4 To 1 Step -1
            Chaine = WsDossierPER.NiveauAffectation(DatedEffet, IndiceI)
            If Chaine <> "" Then
                IndiceK += 1
                Select Case IndiceK
                    Case 1
                        Chaine2 = Chaine
                    Case 2
                        Chaine1 = Chaine
                        Exit For
                End Select
            End If
        Next IndiceI
        If Chaine1 = "" Then
            O_LabelAffectationNiveau1.Text = Chaine2
            O_LabelAffectationNiveau2.Text = ""
        Else
            O_LabelAffectationNiveau1.Text = Chaine1
            O_LabelAffectationNiveau2.Text = Chaine2
        End If

        O_LabelEtablissement.Text = WsDossierPER.Etablissement(DatedEffet)
        O_LabelEntreeEtablissement.Text = WsDossierPER.Date_Entree_Etablissement(DatedEffet)

        Chaine = WsDossierPER.FonctionExercee(DatedEffet, True)
        ChaineManager = WsDossierPER.Grade_Fonction_Evaluateur
        If ChaineManager IsNot Nothing Then
            O_LabelGradeManager.Text = ChaineManager(0)
            O_LabelFonctionManager.Text = ChaineManager(1)
        End If
        Call LireLaFiche()

    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreInfo, "O_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 8, 3))
            VirLabel.Text = ValeurLue(NumObjet, NumInfo)
            IndiceI += 1
        Loop

        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreEmploi, "O_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 8, 3))
            VirLabel.Text = ValeurLue(NumObjet, NumInfo)
            IndiceI += 1
        Loop

        IndiceI = 0
        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreEmploi, "O_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        IndiceI = 0
        Dim VirRadio As VirtualiaControle_VTrioHorizontalRadio
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreEmploi, "O_RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirRadio = CType(Ctl, VirtualiaControle_VTrioHorizontalRadio)
            NumObjet = VirRadio.V_Objet
            If ValeurLue(NumObjet, NumInfo) <> "" Then
                Select Case ValeurLue(NumObjet, NumInfo)
                    Case Is = "Oui"
                        VirRadio.RadioGaucheCheck = True
                    Case Else
                        VirRadio.RadioCentreCheck = True
                End Select
            End If
            VirRadio.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
            End Select
            Return ""
        End Get
    End Property

End Class