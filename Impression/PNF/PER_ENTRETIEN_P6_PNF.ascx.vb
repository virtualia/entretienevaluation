﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P6_PNF
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim Tableaudata(0) As String
        Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)

        VI_CadreLogoPNF.BackImageUrl = Tableaudata(0) & "/Images/General/LogoPNF.jpg"
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim Chaine As String

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If
        VI_LabelAnneeEntretien.Text = "ANNEE " & WsDossierPER.Annee
        VI_LabelIdentite.Text = WsDossierPER.Qualite & " " & WsDossierPER.Nom & " " & WsDossierPER.Prenom

        Chaine = WsDossierPER.FonctionExercee(DatedEffet, True)
        If Chaine <> "" Then
            VI_LabelPoste.Text = WsDossierPER.FonctionExercee(DatedEffet, False)
            VI_LabelDatePoste.Text = Chaine
        End If

        VI_LabelTitreNumero11.Text = "BILAN DE L'ANNEE " & WsDossierPER.AnneePrecedente
        VI_LabelTitreNumero2.Text = "PERSPECTIVES DE L'ANNEE " & WsDossierPER.Annee

        V_LibelListe = "Aucune formation" & VI.Tild & "Une formation" & VI.Tild & "formations"
        V_LibelColonne = "date" & VI.Tild & "formation suivie" & VI.Tild & "Durée" & VI.Tild & "Si DIF" & VI.Tild & "Clef"
        VI_ListeFormationsSuivies.V_Liste(V_LibelColonne, V_LibelListe) = WsDossierPER.ListeDesFormationsSuivies(WsDossierPER.Annee, 1)

        Call LireLaFiche()
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label

        Do
            Ctl = V_WebFonction.VirWebControle(Me.VI_CadreInfo, "VI_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 10, 3))
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1))
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.VI_CadreInfo, "VI_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1))
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VTrioVerticalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.VI_CadreInfo, "VI_RadioV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VTrioVerticalRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 10, 1))
            Select Case NumInfo
                Case 2
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "T1"
                            VirRadio.RadioGaucheCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "T2"
                            VirRadio.RadioCentreCheck = True
                            VirRadio.RadioGaucheText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "T3"
                            VirRadio.RadioDroiteCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioGaucheText = ""
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
                Case 8
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "P1"
                            VirRadio.RadioGaucheCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "P2"
                            VirRadio.RadioCentreCheck = True
                            VirRadio.RadioGaucheText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "P3"
                            VirRadio.RadioDroiteCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioGaucheText = ""
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
                Case 12
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "Sous DIF - TS"
                            VirRadio.RadioGaucheCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "Sous DIF - HTS"
                            VirRadio.RadioCentreCheck = True
                            VirRadio.RadioGaucheText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "Hors DIF"
                            VirRadio.RadioDroiteCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioGaucheText = ""
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
            End Select
            VirRadio.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 155
                    If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_155(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class