﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P6_PNF

    '''<summary>
    '''Contrôle VI_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_CadreTitreFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreTitreFormation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_CadreTitreEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreTitreEntretien As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_CadreLogoPNF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreLogoPNF As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_Etiquette As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelAnneeEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelAnneeEntretien As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreEnteteEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreEnteteEntretien As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_EtiLabelIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_EtiLabelIdentite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelIdentite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_EtiLabelPoste.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_EtiLabelPoste As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelPoste.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelPoste As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_EtiLabelDatePoste.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_EtiLabelDatePoste As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelDatePoste.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelDatePoste As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_EtiLabelManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_EtiLabelManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoH015001.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoH015001 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreNumero1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelTitreNumero11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_ListeFormationsSuivies.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_ListeFormationsSuivies As Global.Virtualia.Net.VirtualiaControle_VListeGrid

    '''<summary>
    '''Contrôle VI_LabelTitreNumero12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVT03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVT03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle NumeroPage9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelTitreNumero14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero14 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVU03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVU03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelTitreNumero15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero15 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVY03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVY03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle NumeroPage10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreNumero2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelTitreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelTitreNumero21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT31 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT32 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT41.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT41 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT42.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT42 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeDIF11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeDIF11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeDIF12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeDIF12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT51.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT51 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT52.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT52 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT61.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT61 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT62.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT62 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreEnteteDemandes1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreEnteteDemandes1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelEnteteExpressionDemande1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteExpressionDemande1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEntetePriorite1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEntetePriorite1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEnteteTypologie1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteTypologie1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEnteteModalite1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteModalite1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEnteteDureeFormation1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteDureeFormation1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreDemandeN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreDemandeN1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_TableDemandeN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDemandeN1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelActionFormationN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelActionFormationN1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoHA15501.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHA15501 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObjectifsN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObjectifsN1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVA03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVA03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelCompetencesN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelCompetencesN1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVA05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVA05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioVA08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVA08 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVA02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVA02 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVA12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVA12 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_InfoHA15511.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHA15511 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSeparationDemandeN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSeparationDemandeN1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreDemandeN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreDemandeN2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_TableDemandeN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDemandeN2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelActionFormationN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelActionFormationN2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoHB15501.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHB15501 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObjectifsN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObjectifsN2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVB03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVB03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelCompetencesN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelCompetencesN2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVB05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVB05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioVB08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVB08 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVB02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVB02 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVB12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVB12 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_InfoHB15511.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHB15511 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSeparationDemandeN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSeparationDemandeN2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreDemandeN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreDemandeN3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_TableDemandeN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDemandeN3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelActionFormationN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelActionFormationN3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoHC15501.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHC15501 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObjectifsN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObjectifsN3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVC03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVC03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelCompetencesN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelCompetencesN3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVC05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVC05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioVC08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVC08 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVC02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVC02 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVC12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVC12 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_InfoHC15511.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHC15511 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSeparationDemandeN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSeparationDemandeN3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreDemandeN4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreDemandeN4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_TableDemandeN4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDemandeN4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelActionFormationN4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelActionFormationN4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoHD15501.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHD15501 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObjectifsN4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObjectifsN4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVD03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVD03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelCompetencesN4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelCompetencesN4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVD05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVD05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioVD08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVD08 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVD02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVD02 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVD12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVD12 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_InfoHD15511.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHD15511 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NumeroPage11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSeparationDemandeN4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSeparationDemandeN4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreDemandeN5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreDemandeN5 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_TableDemandeN5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDemandeN5 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelActionFormationN5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelActionFormationN5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoHE15501.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHE15501 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObjectifsN5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObjectifsN5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVE03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVE03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelCompetencesN5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelCompetencesN5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVE05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVE05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioVE08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVE08 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVE02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVE02 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVE12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVE12 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_InfoHE15511.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHE15511 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSeparationDemandeN5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSeparationDemandeN5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreDemandeN6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreDemandeN6 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_TableDemandeN6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDemandeN6 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelActionFormationN6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelActionFormationN6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoHF15501.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHF15501 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObjectifsN6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObjectifsN6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVF03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVF03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelCompetencesN6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelCompetencesN6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVF05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVF05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioVF08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVF08 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVF02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVF02 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVF12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVF12 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_InfoHF15511.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHF15511 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSeparationDemandeN6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSeparationDemandeN6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreDemandeN7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreDemandeN7 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_TableDemandeN7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDemandeN7 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelActionFormationN7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelActionFormationN7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoHG15501.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHG15501 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObjectifsN7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObjectifsN7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVG03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVG03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelCompetencesN7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelCompetencesN7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVG05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVG05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioVG08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVG08 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVG02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVG02 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVG12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVG12 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_InfoHG15511.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHG15511 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSeparationDemandeN7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSeparationDemandeN7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreDemandeN8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreDemandeN8 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_TableDemandeN8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDemandeN8 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelActionFormationN8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelActionFormationN8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoHH15501.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHH15501 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObjectifsN8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObjectifsN8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVH03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVH03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelCompetencesN8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelCompetencesN8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVH05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVH05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioVH08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVH08 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVH02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVH02 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_RadioVH12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVH12 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_InfoHH15511.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHH15511 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NumeroPage12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSeparationDemandeN8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSeparationDemandeN8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreNumero3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreNumero3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelTitreNumero31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero31 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeDIF31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeDIF31 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeDIF32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeDIF32 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT71.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT71 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT72.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT72 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT81.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT81 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelLegendeT82.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelLegendeT82 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreEnteteDemandes2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreEnteteDemandes2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelEnteteExpressionDemande2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteExpressionDemande2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEnteteTypologie2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteTypologie2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEnteteModalite2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteModalite2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEnteteDureeFormation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteDureeFormation2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreDemandeN9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreDemandeN9 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_TableDemandeN9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDemandeN9 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelActionFormationN9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelActionFormationN9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoHI15501.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHI15501 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObjectifsN9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObjectifsN9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVI03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVI03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelCompetencesN9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelCompetencesN9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVI05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVI05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelTypologieDemandeN9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTypologieDemandeN9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_RadioVI12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVI12 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_InfoHI15511.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHI15511 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSeparationDemandeN9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSeparationDemandeN9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreDemandeN10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreDemandeN10 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_TableDemandeN10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDemandeN10 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelActionFormationN10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelActionFormationN10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoHJ15501.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHJ15501 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObjectifsN10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObjectifsN10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVJ03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVJ03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelCompetencesN10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelCompetencesN10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVJ05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVJ05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelTypologieDemandeN10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTypologieDemandeN10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_RadioVJ12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVJ12 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_InfoHJ15511.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHJ15511 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSeparationDemandeN10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSeparationDemandeN10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreDemandeN11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreDemandeN11 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_TableDemandeN11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDemandeN11 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelActionFormationN11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelActionFormationN11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoHK15501.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHK15501 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObjectifsN11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObjectifsN11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVK03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVK03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelCompetencesN11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelCompetencesN11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVK05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVK05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelTypologieDemandeN11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTypologieDemandeN11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_RadioVK12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVK12 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_InfoHK15511.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHK15511 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSeparationDemandeN11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSeparationDemandeN11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreDemandeN12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreDemandeN12 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_TableDemandeN12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDemandeN12 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelActionFormationN12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelActionFormationN12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoHL15501.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHL15501 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObjectifsN12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObjectifsN12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVL03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVL03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelCompetencesN12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelCompetencesN12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVL05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVL05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelTypologieDemandeN12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTypologieDemandeN12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_RadioVL12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioVL12 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle VI_InfoHL15511.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoHL15511 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NumeroPage13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage13 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSeparationDemandeN12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSeparationDemandeN12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreNumero4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreNumero4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelTitreNumero4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreNumero5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreNumero5 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelObservationsAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObservationsAgent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVO03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVO03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelDateSignatureAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelDateSignatureAgent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelAgentSignature.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelAgentSignature As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoH015020.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoH015020 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSignatureAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSignatureAgent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelObservationsEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelObservationsEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVP03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVP03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_LabelDateSignatureManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelDateSignatureManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelManagerSignature.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelManagerSignature As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoH015021.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoH015021 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSignatureManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSignatureManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NumeroPage14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage14 As Global.System.Web.UI.WebControls.Label
End Class
