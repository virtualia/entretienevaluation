﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P1_PNF.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P1_PNF" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixVerticalRadio.ascx" tagname="VSixVerticalRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="I_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="I_CadreTitreResultats" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVolet" runat="server" Text="BILAN DE L'ANNEE" Height="25px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            style="margin-top: 1px; margin-left: 1px; margin-bottom: 10px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVoletBis" runat="server" Height="25px" Width="746px"
                            Text="Atteinte des objectifs et actions conduites"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            style="margin-top: 1px; margin-left: 1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelNumerobjectif1" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°1"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif1" runat="server" Height="32px" Width="301px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs1" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteIndicateurs1" runat="server" Height="32px" Width="260px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Indicateurs <br/> de réussite prévus"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation1" runat="server" Height="32px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None" 
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="297px" DonHeight="210px" DonTabIndex="1"
                                   Donstyle="margin-left: 1px;"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                   DonBorderWidth="1px" DonBorderColor="Transparent"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur1" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="256px" DonHeight="210px" DonTabIndex="2"
                                   EtiBorderStyle="None" EtiBackColor="Transparent"
                                   Donstyle="margin-left: 1px;"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                   DonBorderWidth="1px" DonBorderColor="Transparent"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="I_RadioHC04" runat="server" V_Groupe="Realisation1"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="34px" VRadioN2Height="34px" VRadioN3Height="34px"
                                            VRadioN4Height="34px" VRadioN5Height="34px" VRadioN6Height="34px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="insuffisant" VRadioN2Text="partiellement atteint" VRadioN3Text="atteint"
                                            VRadioN4Text="dépassé" VRadioN5Text="exceptionnel" VRadioN6Text=""
                                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteCommentaire1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteCommentaire1" runat="server" Height="18px" Width="748px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Commentaires"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableCommentaire1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="3"
                                   Donstyle="margin-left: 1px;"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                   DonBorderWidth="1px" DonBorderColor="Transparent"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="I_CadreObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelNumerobjectif2" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°2"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif2" runat="server" Height="32px" Width="301px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs2" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteIndicateurs2" runat="server" Height="32px" Width="260px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Indicateurs <br/> de réussite prévus"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation2" runat="server" Height="32px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None" 
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="297px" DonHeight="210px" DonTabIndex="4"
                                   Donstyle="margin-left: 1px;"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                   DonBorderWidth="1px" DonBorderColor="Transparent"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur2" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="256px" DonHeight="210px" DonTabIndex="5"
                                   Donstyle="margin-left: 1px;"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                   DonBorderWidth="1px" DonBorderColor="Transparent"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="I_RadioHD04" runat="server" V_Groupe="Realisation2"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="34px" VRadioN2Height="34px" VRadioN3Height="34px"
                                            VRadioN4Height="34px" VRadioN5Height="34px" VRadioN6Height="34px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="insuffisant" VRadioN2Text="partiellement atteint" VRadioN3Text="atteint"
                                            VRadioN4Text="dépassé" VRadioN5Text="exceptionnel" VRadioN6Text=""
                                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteCommentaire2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteCommentaire2" runat="server" Height="18px" Width="748px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Commentaires"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableCommentaire2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="6"
                                   Donstyle="margin-left: 1px;"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                   DonBorderWidth="1px" DonBorderColor="Transparent"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage2" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="2 / 14" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
             <div style="page-break-before:always;"></div>
        </asp:TableCell>
     </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="I_CadreObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelNumerobjectif3" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°3"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif3" runat="server" Height="32px" Width="301px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs3" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteIndicateurs3" runat="server" Height="32px" Width="260px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Indicateurs <br/> de réussite prévus"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation3" runat="server" Height="32px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None" 
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="297px" DonHeight="210px" DonTabIndex="7"
                                   Donstyle="margin-left: 1px;"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                   DonBorderWidth="1px" DonBorderColor="Transparent"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur3" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="256px" DonHeight="210px" DonTabIndex="8"
                                   Donstyle="margin-left: 1px;"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                   DonBorderWidth="1px" DonBorderColor="Transparent"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="I_RadioHE04" runat="server" V_Groupe="Realisation3"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="34px" VRadioN2Height="34px" VRadioN3Height="34px"
                                            VRadioN4Height="34px" VRadioN5Height="34px" VRadioN6Height="34px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="insuffisant" VRadioN2Text="partiellement atteint" VRadioN3Text="atteint"
                                            VRadioN4Text="dépassé" VRadioN5Text="exceptionnel" VRadioN6Text=""
                                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteCommentaire3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteCommentaire3" runat="server" Height="18px" Width="748px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Commentaires"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableCommentaire3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="9"
                                   Donstyle="margin-left: 1px;"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                   DonBorderWidth="1px" DonBorderColor="Transparent"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="I_CadreObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelNumerobjectif4" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°4"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif4" runat="server" Height="32px" Width="301px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs4" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteIndicateurs4" runat="server" Height="32px" Width="260px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Indicateurs <br/> de réussite prévus"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation4" runat="server" Height="32px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None" 
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="297px" DonHeight="210px" DonTabIndex="10"
                                   Donstyle="margin-left: 1px;"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                   DonBorderWidth="1px" DonBorderColor="Transparent"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur4" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="256px" DonHeight="210px" DonTabIndex="11"
                                   Donstyle="margin-left: 1px;"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                   DonBorderWidth="1px" DonBorderColor="Transparent"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="I_RadioHF04" runat="server" V_Groupe="Realisation4"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="34px" VRadioN2Height="34px" VRadioN3Height="34px"
                                            VRadioN4Height="34px" VRadioN5Height="34px" VRadioN6Height="34px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="insuffisant" VRadioN2Text="partiellement atteint" VRadioN3Text="atteint"
                                            VRadioN4Text="dépassé" VRadioN5Text="exceptionnel" VRadioN6Text=""
                                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteCommentaire4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteCommentaire4" runat="server" Height="18px" Width="748px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Commentaires"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableCommentaire4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="11"
                                   Donstyle="margin-left: 1px;"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                   DonBorderWidth="1px" DonBorderColor="Transparent"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage3" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="3 / 14" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>