﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P6_PNF.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P6_PNF" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioVerticalRadio.ascx" tagname="VTrioVerticalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="VI_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreTitreFormation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="VI_CadreTitreEntretien" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="Black">
                        <asp:TableRow>
                            <asp:TableCell Height="2px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Table ID="VI_CadreLogoPNF" runat="server" BackImageUrl="~/Images/General/LogoPNF.jpg" Width="166px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true">
                                    <asp:TableRow>
                                        <asp:TableCell Height="75px" BackColor="Transparent" Width="166px"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="10px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="VI_Etiquette" runat="server" Text="COMPTE RENDU D'ENTRETIEN DE FORMATION" Height="25px" Width="748px"
                                    BackColor="Transparent"  BorderColor="Transparent" BorderStyle="None"
                                    BorderWidth="1px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                    style="margin-top: 1px; margin-left: 1px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_LabelAnneeEntretien" runat="server" Text="" Height="25px" Width="748px"
                                    BackColor="Transparent"  BorderColor="Black" BorderStyle="None"
                                    BorderWidth="2px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                    style="margin-top: 10px; margin-left: 1px; margin-bottom: 10px;
                                    font-style:  normal; text-indent: 2px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="VI_CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left"
                        BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">                        
                        <asp:TableRow>
                            <asp:TableCell Height="7px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelIdentite" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Nom et prénom : "
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 4px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_LabelIdentite" runat="server" Height="30px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 0px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelPoste" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Intitulé du poste ooste occupé :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 4px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_LabelPoste" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelDatePoste" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Date de prise de fonction du poste actuel : "
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 4px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_LabelDatePoste" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelManager" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Supérieur hiérarchique ayant conduit l'entretien : "
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 4px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_InfoH015001" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>                       
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreNumero11" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="BILAN DE L'ANNEE"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="VI_ListeFormationsSuivies" runat="server" CadreWidth="746px" SiColonneSelect="false"
                            SiAppliquerCharte="false" BackColorCaption="White" ForeColorCaption="Black" BackColorRow="White" ForeColorRow="Black"/>
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero12" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="a) Actions de formation continue suivies"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVT03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="200px" EtiHeight="20px" DonTabIndex="2" 
                           EtiText=""
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>  
                <asp:TableFooterRow>
                    <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
                        <asp:Label ID="NumeroPage9" runat="server" Height="15px" Width="150px"
                             BackColor="Transparent" BorderStyle="None"
                             Text="9 / 14" ForeColor="Black" Font-Italic="True"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                             style="text-align:left" >
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell >
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero14" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="b) Actions prévues lors du précédent entretien de formation et non suivies"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVU03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="4" 
                           EtiText="" Etivisible="false"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero15" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="c) Actions conduites en tant que formateur"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVY03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="5" 
                           EtiText="(intitulé des actions, nombre de jours, public bénéficiaire)" 
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
       <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage10" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="10 / 14" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:left" >
            </asp:Label>   
       </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
       <asp:TableCell >
            <div style="page-break-before:always;"></div>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="VI_LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" Text="PERSPECTIVES DE L'ANNEE A VENIR"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="VI_LabelTitreNumero21" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="a) Actions de formation continue sollicitées"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT11" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(*)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT12" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Typologie basée sur l'objectif visé, dans le cas spécifique de l'agent, c'est à dire :"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT21" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT22" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T1 : adaptation immédiate au poste de travail"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT31" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT32" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T2 : adaptation à l'évolution prévisible des métiers"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT41" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT42" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T3 : développement de ses qualifications ou acquisition de nouvelles qualifications"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeDIF11" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(**)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeDIF12" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Sous réserve de remplir les conditions d'accès au DIF (vérification par le service formation)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT51" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT52" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="TS : dans le temps de service"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT61" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT62" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="HTS : hors du temps de service"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreEnteteDemandes1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelEnteteExpressionDemande1" runat="server" Height="50px" Width="427px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                            Text="Liaison formation / réalisation missions, évolution de carrière"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelEntetePriorite1" runat="server" Height="50px" Width="55px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Priorité <br/>&nbsp"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelEnteteTypologie1" runat="server" Height="50px" Width="62px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Typologie (*)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelEnteteModalite1" runat="server" Height="50px" Width="140px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Modalité de formation (**)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelEnteteDureeFormation1" runat="server" Height="50px" Width="60px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Nombre d'heures"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreDemandeN1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_TableDemandeN1" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelActionFormationN1" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_InfoHA15501" runat="server" Height="20px" Width="415px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelObjectifsN1" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVA03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="7" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelCompetencesN1" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVA05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="8" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVA08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123A"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVA02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123A"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVA12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="false"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteA"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoHA15511" runat="server" Height="20px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="VI_LabelSeparationDemandeN1" runat="server" Height="6px" Width="748px"
              BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreDemandeN2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_TableDemandeN2" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelActionFormationN2" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_InfoHB15501" runat="server" Height="20px" Width="415px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelObjectifsN2" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVB03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="11" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelCompetencesN2" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVB05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="12" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVB08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123B"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVB02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123B"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVB12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="false"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteB"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoHB15511" runat="server" Height="20px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="VI_LabelSeparationDemandeN2" runat="server" Height="6px" Width="748px"
              BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreDemandeN3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_TableDemandeN3" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelActionFormationN3" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_InfoHC15501" runat="server" Height="20px" Width="415px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelObjectifsN3" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVC03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="15" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelCompetencesN3" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVC05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="16" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVC08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123C"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVC02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123C"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVC12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="false"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteC"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoHC15511" runat="server" Height="20px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="VI_LabelSeparationDemandeN3" runat="server" Height="6px" Width="748px"
              BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreDemandeN4" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_TableDemandeN4" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelActionFormationN4" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_InfoHD15501" runat="server" Height="20px" Width="415px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelObjectifsN4" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVD03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="19" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelCompetencesN4" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVD05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="20" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVD08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123D"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVD02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123D"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVD12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="false"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteD"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoHD15511" runat="server" Height="20px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
       <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage11" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="11 / 14" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:left">
            </asp:Label>   
       </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
       <asp:TableCell >
            <div style="page-break-before:always;"></div>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="VI_LabelSeparationDemandeN4" runat="server" Height="1px" Width="748px"
              BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreDemandeN5" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_TableDemandeN5" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelActionFormationN5" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_InfoHE15501" runat="server" Height="20px" Width="415px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelObjectifsN5" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVE03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="23" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelCompetencesN5" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVE05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="24" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVE08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123E"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVE02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123E"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVE12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="false"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteE"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoHE15511" runat="server" Height="20px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="VI_LabelSeparationDemandeN5" runat="server" Height="6px" Width="748px"
              BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreDemandeN6" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_TableDemandeN6" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelActionFormationN6" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_InfoHF15501" runat="server" Height="20px" Width="415px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelObjectifsN6" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVF03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="27" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelCompetencesN6" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVF05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="28" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVF08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123F"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVF02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123F"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVF12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="false"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteF"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoHF15511" runat="server" Height="20px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="VI_LabelSeparationDemandeN6" runat="server" Height="6px" Width="748px"
              BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreDemandeN7" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_TableDemandeN7" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelActionFormationN7" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_InfoHG15501" runat="server" Height="20px" Width="415px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelObjectifsN7" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVG03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="31" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelCompetencesN7" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVG05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="32" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVG08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123G"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVG02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123G"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVG12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="false"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteG"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoHG15511" runat="server" Height="20px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="VI_LabelSeparationDemandeN7" runat="server" Height="6px" Width="748px"
              BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreDemandeN8" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_TableDemandeN8" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelActionFormationN8" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_InfoHH15501" runat="server" Height="20px" Width="415px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelObjectifsN8" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVH03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="35" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelCompetencesN8" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVH05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="36" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVH08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123H"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVH02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123H"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVH12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="false"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteH"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoHH15511" runat="server" Height="20px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
       <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage12" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="12 / 14" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:left">
            </asp:Label>   
       </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
       <asp:TableCell >
            <div style="page-break-before:always;"></div>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="VI_LabelSeparationDemandeN8" runat="server" Height="1px" Width="748px"
              BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
           <asp:Table ID="VI_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="25px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="VI_LabelTitreNumero31" runat="server" Height="45px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="b) Autres actions sollicitées, dont celles mobilisant le DIF"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeDIF31" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(**)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeDIF32" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Sous réserve de remplir les conditions d'accès au DIF (vérification par le service formation)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT71" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT72" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="TS : dans le temps de service"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT81" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelLegendeT82" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="HTS : hors du temps de service"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>            
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreEnteteDemandes2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelEnteteExpressionDemande2" runat="server" Height="50px" Width="482px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                            Text="Préparation examens et concours, VAE, bilan de compétences, période de professionnalisation"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: -1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelEnteteTypologie2" runat="server" Height="50px" Width="62px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Typologie (*)"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelEnteteModalite2" runat="server" Height="50px" Width="140px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Modalité de formation (**)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelEnteteDureeFormation2" runat="server" Height="50px" Width="60px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Nombre d'heures"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreDemandeN9" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_TableDemandeN9" runat="server" CellPadding="0" CellSpacing="0" Width="480px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="2px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelActionFormationN9" runat="server" Height="22px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Besoin ou action"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_InfoHI15501" runat="server" Height="20px" Width="470px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelObjectifsN9" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs poursuivis"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVI03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="39" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelCompetencesN9" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Compétences à développer"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVI05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="40" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTypologieDemandeN9" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVI12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="false"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteI"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoHI15511" runat="server" Height="20px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="VI_LabelSeparationDemandeN9" runat="server" Height="6px" Width="748px"
              BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreDemandeN10" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_TableDemandeN10" runat="server" CellPadding="0" CellSpacing="0" Width="480px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelActionFormationN10" runat="server" Height="22px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Besoin ou action"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_InfoHJ15501" runat="server" Height="20px" Width="470px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelObjectifsN10" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs poursuivis"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVJ03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="43" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelCompetencesN10" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Compétences à développer"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVJ05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="44" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTypologieDemandeN10" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVJ12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="false"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteJ"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoHJ15511" runat="server" Height="20px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="VI_LabelSeparationDemandeN10" runat="server" Height="6px" Width="748px"
              BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreDemandeN11" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_TableDemandeN11" runat="server" CellPadding="0" CellSpacing="0" Width="480px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelActionFormationN11" runat="server" Height="22px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Besoin ou action"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_InfoHK15501" runat="server" Height="20px" Width="470px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelObjectifsN11" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs poursuivis"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVK03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="47" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelCompetencesN11" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Compétences à développer"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVK05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="48" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTypologieDemandeN11" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVK12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="false"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteK"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoHK15511" runat="server" Height="20px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="VI_LabelSeparationDemandeN11" runat="server" Height="6px" Width="748px"
              BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreDemandeN12" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_TableDemandeN12" runat="server" CellPadding="0" CellSpacing="0" Width="480px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelActionFormationN12" runat="server" Height="22px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Besoin ou action"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_InfoHL15501" runat="server" Height="20px" Width="470px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelObjectifsN12" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs poursuivis"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVL03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="51" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="VI_LabelCompetencesN12" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Compétences à développer"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVL05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="52" 
                                        Etivisible="false" Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTypologieDemandeN12" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="VI_RadioVL12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="false"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteL"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoHL15511" runat="server" Height="20px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
       <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage13" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="13 / 14" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:left">
            </asp:Label>   
       </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
       <asp:TableCell >
            <div style="page-break-before:always;"></div>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="VI_LabelSeparationDemandeN12" runat="server" Height="1px" Width="748px"
              BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="25px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="VI_LabelTitreNumero4" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" Text="OBSERVATIONS - COMPLEMENTS"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>          
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="VI_LabelObservationsAgent" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text="L'agent"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVO03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="220px" EtiHeight="20px" DonTabIndex="54" 
                           EtiText="" Etivisible="false"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
          	    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelDateSignatureAgent" runat="server" Height="30px" Width="454px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="Date de signature de l'agent" 
		                    ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelAgentSignature" runat="server" Height="30px" Width="294px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="Signature" 
		                    ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_InfoH015020" runat="server" Height="30px" Width="454px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                   <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelSignatureAgent" runat="server" Height="30px" Width="294px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" 
		                    ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell Height="40px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="VI_LabelObservationsEvaluateur" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text="Le responsable hiérarchique"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVP03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="200px" EtiHeight="20px" DonTabIndex="56" 
                           EtiText="" Etivisible="false"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
          	    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelDateSignatureManager" runat="server" Height="30px" Width="454px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="Date de signature du responsable hiérarchique" 
		                    ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelManagerSignature" runat="server" Height="30px" Width="294px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="Signature" 
		                    ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_InfoH015021" runat="server" Height="30px" Width="454px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                   <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelSignatureManager" runat="server" Height="30px" Width="294px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" 
		                    ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Left" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage14" runat="server" Height="15px" Width="650px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="14 / 14" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:right" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>