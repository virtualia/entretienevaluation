﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P1_PNF

    '''<summary>
    '''Contrôle I_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_CadreTitreResultats.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreTitreResultats As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelVolet As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_LabelVoletBis.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelVoletBis As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_CadreObjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreObjectif1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelNumerobjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelNumerobjectif1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableEnteteObjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteObjectif1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteObjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteObjectif1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableEnteteIndicateurs1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteIndicateurs1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteIndicateurs1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteIndicateurs1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableEnteteRealisation1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteRealisation1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteRealisation1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteRealisation1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableObjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableObjectif1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_InfoVC02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVC02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle I_TableIndicateur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableIndicateur1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_InfoVC03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVC03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle I_TableRealisation1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableRealisation1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_RadioHC04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_RadioHC04 As Global.Virtualia.Net.VirtualiaControle_VSixVerticalRadio

    '''<summary>
    '''Contrôle I_TableEnteteCommentaire1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteCommentaire1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteCommentaire1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteCommentaire1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableCommentaire1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableCommentaire1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_InfoVC07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVC07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle I_CadreObjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreObjectif2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelNumerobjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelNumerobjectif2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableEnteteObjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteObjectif2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteObjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteObjectif2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableEnteteIndicateurs2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteIndicateurs2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteIndicateurs2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteIndicateurs2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableEnteteRealisation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteRealisation2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteRealisation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteRealisation2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableObjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableObjectif2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_InfoVD02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVD02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle I_TableIndicateur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableIndicateur2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_InfoVD03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVD03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle I_TableRealisation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableRealisation2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_RadioHD04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_RadioHD04 As Global.Virtualia.Net.VirtualiaControle_VSixVerticalRadio

    '''<summary>
    '''Contrôle I_TableEnteteCommentaire2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteCommentaire2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteCommentaire2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteCommentaire2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableCommentaire2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableCommentaire2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_InfoVD07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVD07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle NumeroPage2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_CadreObjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreObjectif3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelNumerobjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelNumerobjectif3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableEnteteObjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteObjectif3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteObjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteObjectif3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableEnteteIndicateurs3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteIndicateurs3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteIndicateurs3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteIndicateurs3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableEnteteRealisation3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteRealisation3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteRealisation3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteRealisation3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableObjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableObjectif3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_InfoVE02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVE02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle I_TableIndicateur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableIndicateur3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_InfoVE03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVE03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle I_TableRealisation3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableRealisation3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_RadioHE04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_RadioHE04 As Global.Virtualia.Net.VirtualiaControle_VSixVerticalRadio

    '''<summary>
    '''Contrôle I_TableEnteteCommentaire3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteCommentaire3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteCommentaire3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteCommentaire3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableCommentaire3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableCommentaire3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_InfoVE07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVE07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle I_CadreObjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreObjectif4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelNumerobjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelNumerobjectif4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableEnteteObjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteObjectif4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteObjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteObjectif4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableEnteteIndicateurs4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteIndicateurs4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteIndicateurs4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteIndicateurs4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableEnteteRealisation4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteRealisation4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteRealisation4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteRealisation4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableObjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableObjectif4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_InfoVF02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVF02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle I_TableIndicateur4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableIndicateur4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_InfoVF03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVF03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle I_TableRealisation4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableRealisation4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_RadioHF04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_RadioHF04 As Global.Virtualia.Net.VirtualiaControle_VSixVerticalRadio

    '''<summary>
    '''Contrôle I_TableEnteteCommentaire4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteCommentaire4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_LabelEnteteCommentaire4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteCommentaire4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle I_TableCommentaire4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableCommentaire4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle I_InfoVF07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVF07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle NumeroPage3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage3 As Global.System.Web.UI.WebControls.Label
End Class
