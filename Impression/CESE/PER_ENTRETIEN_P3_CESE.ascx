﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P3_CESE.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P3_CESE" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="III_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreTitreCompetence" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelVolet" runat="server" Text="EVALUATION" Height="20px" Width="748px"
                            BackColor= "Transparent" BorderStyle="None" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelVoletSuite" runat="server" Height="30px" Width="748px"
                            Text="Pour chacun des thèmes, cocher le niveau estimé."
                            BackColor= "Transparent" BorderStyle="None" ForeColor="Black"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: oblique; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_EnteteCompetences1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BorderStyle="Notset" BorderWidth="1px" BorderColor="Transparent" BackColor="#E7E7E7">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelFamille1" runat="server" Height="25px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" 
                                       Text="CONNAISSANCE DU DOMAINE D'EXERCICE DE LA FONCTION"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                                       font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelSousFamille1" runat="server" Height="20px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" 
                                       Text="A compléter en fonction des compétences attendues par la fiche de poste actualisée"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="true"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 0px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreCompetences1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreCompetence1" runat="server" Height="32px" Width="246px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Compétence"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels1" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote11" runat="server" Height="33px" Width="55px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Expertise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote12" runat="server" Height="33px" Width="70px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote13" runat="server" Height="35px" Width="70px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="A <br/> développer"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote14" runat="server" Height="35px" Width="55px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="A <br/> acquérir"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreObserv1" runat="server" Height="32px" Width="236px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Observations éventuelles"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>       
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVA01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="1" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHA07" runat="server" V_Groupe="CritereAgent1"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVA11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="2" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVB01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="3" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHB07" runat="server" V_Groupe="CritereAgent2"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVB11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="4" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVC01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="5" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHC07" runat="server" V_Groupe="CritereAgent3"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVC11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="6" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>  
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_EnteteCompetences2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                            BorderStyle="Notset" BorderWidth="1px" BorderColor="Transparent" BackColor="#E7E7E7"> 
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelFamille2" runat="server" Height="30px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" 
                                       Text="QUALITE DU TRAVAIL ET QUALITES RELATIONNELLES"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                                       font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreCompetences2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreCompetence2" runat="server" Height="32px" Width="246px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Qualité"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote21" runat="server" Height="33px" Width="55px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Excellent"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote22" runat="server" Height="33px" Width="70px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Satisfaisant"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote23" runat="server" Height="35px" Width="70px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="A <br/> développer"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote24" runat="server" Height="35px" Width="55px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="A <br/> acquérir"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreObserv2" runat="server" Height="32px" Width="236px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Observations éventuelles"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>       
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVH01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="7" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHH07" runat="server" V_Groupe="CritereAgent4"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVH11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="8" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVI01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="9" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHI07" runat="server" V_Groupe="CritereAgent5"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVI11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="10" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVJ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="11" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHJ07" runat="server" V_Groupe="CritereAgent6"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVJ11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="12" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVK01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="13" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHK07" runat="server" V_Groupe="CritereAgent7"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVK11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="14" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVL01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="15" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHL07" runat="server" V_Groupe="CritereAgent8"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVL11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="16" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVM01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="17" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHM07" runat="server" V_Groupe="CritereAgent9"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"  
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVM11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="18" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVN01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="19" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHN07" runat="server" V_Groupe="CritereAgent10"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"  
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVN11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="20" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVO01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="21" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHO07" runat="server" V_Groupe="CritereAgent11"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"  
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVO11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="22" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVP01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="23" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHP07" runat="server" V_Groupe="CritereAgent12"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"  
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVP11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="24" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVQ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="25" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHQ07" runat="server" V_Groupe="CritereAgent13"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVQ11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="26" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="4px"></asp:TableCell>
    </asp:TableRow>
       <asp:TableFooterRow>
         <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage5" runat="server" Height="15px" Width="250px"
               BackColor="Transparent" BorderStyle="None"
               Text="5 / 12" ForeColor="Black" Font-Italic="True"
               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
               style="text-align:right" >
            </asp:Label>   
         </asp:TableCell>
       </asp:TableFooterRow>
    <asp:TableRow>
       <asp:TableCell>
         <div style="page-break-before:always;"></div>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_EnteteCompetences3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                            BorderStyle="Notset" BorderWidth="1px" BorderColor="Transparent" BackColor="#E7E7E7">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelFamille3" runat="server" Height="25px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" 
                                       Text="APTITUDES AU MANAGEMENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                                       font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelSousFamille3" runat="server" Height="20px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" 
                                       Text="(uniquement pour les agents en situation d'encadrement)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="true"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                                       font-style: oblique; text-indent: 0px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreCompetences3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreCompetence3" runat="server" Height="32px" Width="246px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Aptitude"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels3" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote31" runat="server" Height="33px" Width="55px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Excellent"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote32" runat="server" Height="33px" Width="70px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Satisfaisant"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote33" runat="server" Height="35px" Width="70px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="A <br/> développer"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote34" runat="server" Height="35px" Width="55px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="A <br/> acquérir"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreObserv3" runat="server" Height="32px" Width="236px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Observations éventuelles"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>       
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVU01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="27" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHU07" runat="server" V_Groupe="CritereAgent14"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVU11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="28" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVV01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="29" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHV07" runat="server" V_Groupe="CritereAgent15"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVV11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="30" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVW01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="31" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHW07" runat="server" V_Groupe="CritereAgent16"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVW11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="32" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVX01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="33" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="III_RadioHX07" runat="server" V_Groupe="CritereAgent17"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVX11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonBorderColor="Black" DonTabIndex="34" 
                                   EtiVisible="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage6" runat="server" Height="15px" Width="250px"
               BackColor="Transparent" BorderStyle="None"
               Text="6 / 12" ForeColor="Black" Font-Italic="True"
               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
               style="text-align:right" >
            </asp:Label>   
         </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>