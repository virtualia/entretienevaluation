﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P0_CESE.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P0_CESE" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<asp:Table ID="PAGE_0" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="O_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreTitreEntretien" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#124545">
                        <asp:TableRow>
                            <asp:TableCell Height="2px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Table ID="O_CadreLogoCESE" runat="server" BackImageUrl="~/Images/General/Logo_CESE.jpg" Width="290px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true" HorizontalAlign="Left">
                                    <asp:TableRow>
                                        <asp:TableCell Height="180px" BackColor="Transparent" Width="290px" HorizontalAlign="Left"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Label ID="O_EtiquetteEntete" runat="server" Height="21px" Width="746px"
                                    Text="SECRETARIAT GENERAL - Direction des ressources humaines"
                                    BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None"
                                    BorderWidth="1px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 1px; margin-left: 1px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: right;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="10px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="O_Etiquette" runat="server" Text="FICHE D'ENTRETIEN PROFESSIONNEL DES AGENTS" Height="21px" Width="746px"
                                    BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="1px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 1px; margin-left: 1px; margin-bottom: 5px;
                                    font-style: normal; text-indent: 0px; text-align: center;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="O_EtiquetteSuite" runat="server" Text="DU CONSEIL ECONOMIQUE, SOCIAL ET ENVIRONNEMENTAL" Height="21px" Width="746px"
                                    BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="1px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 1px; margin-left: 1px; margin-bottom: 10px;
                                    font-style: normal; text-indent: 0px; text-align: center;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="O_LabelAnneeEntretien" runat="server" Text="" Height="20px" Width="746px"
                                    BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="2px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 5px; margin-left: 1px; margin-bottom: 20px;
                                    font-style:  normal; text-indent: 0px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelIdentite" runat="server" Height="20px" Width="150px"
                                    BackColor="Transparent" BorderStyle="None" Text="NOM :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 30px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelIdentite" runat="server" Height="20px" Width="550px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelCorps" runat="server" Height="20px" Width="150px"
                                    BackColor="Transparent" BorderStyle="None" Text="CORPS :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 30px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelCorps" runat="server" Height="20px" Width="550px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGrade" runat="server" Height="20px" Width="150px"
                                    BackColor="Transparent" BorderStyle="None" Text="GRADE :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 30px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGrade" runat="server" Height="20px" Width="550px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelEchelon" runat="server" Height="20px" Width="150px"
                                    BackColor="Transparent" BorderStyle="None" Text="ECHELON :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 30px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelEchelon" runat="server" Height="20px" Width="550px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelTempsTravail" runat="server" Height="20px" Width="150px"
                                    BackColor="Transparent" BorderStyle="None" Text="TEMPS DE TRAVAIL :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 30px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelTempsTravail" runat="server" Height="20px" Width="550px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelAffectation" runat="server" Height="20px" Width="150px"
                                    BackColor="Transparent" BorderStyle="None" Text="AFFECTATION :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 30px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="O_LabelAffectationNiveau1" runat="server" Height="20px" Width="550px"
                                    BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                    BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left; ">
                                </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="O_LabelAffectationSuite" runat="server" Height="20px" Width="150px"
                                    BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                    BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 30px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align:  left; ">
                                </asp:Label>          
                            </asp:TableCell>  
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="O_LabelAffectationNiveau2" runat="server" Height="20px" Width="550px"
                                    BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                    BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left; ">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreFonctions" runat="server" Height="20px" Width="746px"
                                    BackColor= "Transparent" BorderStyle="None" Text="FONCTIONS EXERCEES"
                                    ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 10px; margin-left: 0px; margin-bottom: 5px;
                                    font-style:  normal; text-indent: 0px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreFonctionsSuite" runat="server" Height="20px" Width="746px"
                                    BackColor= "Transparent" BorderStyle="None" Text="(à remplir par l’évaluateur)"
                                    ForeColor="Black"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                                    font-style:  normal; text-indent: 0px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreFonctions" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="O_LabelTitreIntituleFonction" runat="server" Height="20px" Width="180px"
                                    BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                    Text="INTITULE DE LA FONCTION :"
                                    BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left; ">
                                </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="O_InfoH15005" runat="server" Height="20px" Width="550px"
                                    BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                    </asp:Label>     
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="O_LabelTitreDateFonction" runat="server" Height="20px" Width="180px"
                                    BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                    Text="Depuis le :"
                                    BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left; ">
                                </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="O_InfoHA15403" runat="server" Height="20px" Width="550px"
                                    BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                    </asp:Label>     
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                <asp:Label ID="O_LabelCommentaire1" runat="server" Height="22px" Width="748px"
                                   BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                   Text="La fiche de poste sera à l'occasion actualisée si nécessaire et jointe au compte-rendu d'entretien."
                                   BorderWidth="1px" ForeColor="Black" Font-Italic="True"
                                   Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px;
                                   font-style: oblique; text-indent: 1px; text-align: left;">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                <asp:Label ID="O_LabelCommentaire2" runat="server" Height="22px" Width="748px"
                                   BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                   Text="Précisez les points sur lesquels la fiche de poste est actualisée :"
                                   BorderWidth="1px" ForeColor="Black" Font-Italic="True"
                                   Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px;
                                   font-style: oblique; text-indent: 1px; text-align: left;">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV16" runat="server" DonTextMode="true"
                                    V_PointdeVue="1" V_Objet="150" V_Information="16" V_SiDonneeDico="false"
                                    EtiVisible="False"
                                    DonWidth="740px" DonHeight="160px" DonTabIndex="1"
                                    Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreEntretienPro" runat="server" Height="20px" Width="746px"
                                    BackColor= "Transparent" BorderStyle="None" Text="ENTRETIEN PROFESSIONNEL"
                                    ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 10px; margin-left: 0px; margin-bottom: 2px;
                                    font-style:  normal; text-indent: 0px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDateEntretien" runat="server" Height="20px" Width="100px"
                                    BackColor="Transparent" BorderStyle="None" Text="Tenu le :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15000" runat="server" Height="20px" Width="640px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreAvec" runat="server" Height="20px" Width="380px"
                                    BackColor= "Transparent" BorderStyle="None" Text="avec"
                                    ForeColor="Black"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 10px; margin-left: 0px; margin-bottom: 12px;
                                    font-style: normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelManager" runat="server" Height="20px" Width="80px"
                                    BackColor="Transparent" BorderStyle="None" Text="NOM :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15001" runat="server" Height="20px" Width="620px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                       <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelFonctionManager" runat="server" Height="20px" Width="80px"
                                    BackColor="Transparent" BorderStyle="None" Text="FONCTION :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelFonctionManager" runat="server" Height="20px" Width="620px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                <asp:Label ID="O_LabelTitreMotif" runat="server" Height="22px" Width="746px"
                                   BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Si l'entretien n'a pas lieu, motif :"
                                   BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                                   font-style: normal; text-indent: 1px; text-align: left;">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoVB03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                   DonWidth="740px" DonHeight="60px" DonTabIndex="2"
                                   EtiVisible="false"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Left" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage1" runat="server" Height="15px" Width="746px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="1 / 12" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:right; text-indent: 0px" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>