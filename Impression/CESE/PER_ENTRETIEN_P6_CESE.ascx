﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P6_CESE.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P6_CESE" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="VI_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreTitreAppreciation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelVolet" runat="server" Text="APPRECIATION GENERALE DE L'EVALUATEUR" Height="20px" Width="748px"
                            BackColor= "Transparent" BorderStyle="None" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 10px;
                            font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelVoletSuite" runat="server" Height="20px" Width="748px"
                            Text="Préciser les qualités et aptitudes personnelles de l'agent"
                            BackColor= "Transparent" BorderStyle="None" ForeColor="Black"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: oblique; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="250px" EtiHeight="80px" DonTabIndex="1" 
                           Etitext="(Sens des responsabilités, fiabilité, conscience professionnelle, aptitudes managériales, 
                           aptitudes organisationnelles, esprit d'initiative, capacité d'adaptation à l'évolution du poste ou 
                           de son environnement, aptitude à l'acquisition de nouvelles connaissances, présentation générale, 
                           ponctualité et assiduité…)" 
                           Etivisible="True" EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderStyle="None"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Left" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage11" runat="server" Height="15px" Width="740px"
                BackColor="Transparent" BorderStyle="None"
                Text="11 / 12" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:right">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelVoletAgent" runat="server" Text="EXPRESSION DE L'AGENT" Height="20px" Width="748px"
                            BackColor= "Transparent" BorderStyle="None" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelVoletSuiteAgent" runat="server" Height="40px" Width="748px"
                            Text="La signature du document atteste que l'agent en a pris connaissance, pas qu'il en approuve le sens. 
                            Dans un premier temps, si vous ne partagez pas l'appréciation portée par votre évaluateur, 
                            vous pouvez l'indiquer sur cette page."
                            BackColor= "Transparent" BorderStyle="None" ForeColor="Black"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: oblique; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero2" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Souhaitez-vous apporter des précisions à la description qui a été faite de vos fonctions ? Si oui lesquelles ?"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVQ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="135px" EtiHeight="20px" DonTabIndex="2" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero3" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Partagez-vous l'évaluation générale vous concernant ? Sinon pourquoi ?"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVR03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="135px" EtiHeight="20px" DonTabIndex="3" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero4" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Vous sentez-vous à l'aise dans votre service ? Si non pourquoi ?"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVS03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="135px" EtiHeight="20px" DonTabIndex="4" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero5" runat="server" Height="35px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Quels sont vos souhaits d'évolution professionnelle ? Evolution sur le poste, 
                           prises de responsabilités nouvelles, projet professionnel, avancement."
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVT03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="135px" EtiHeight="20px" DonTabIndex="5" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero6" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Autres observations"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVU03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="130px" EtiHeight="20px" DonTabIndex="6" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreSignatures" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignature1" runat="server" Height="16px" Width="500px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="A"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align: right;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignature2" runat="server" Height="16px" Width="248px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="le"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignature3" runat="server" Height="35px" Width="500px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Signature de l'évaluateur <br/> ayant conduit l'entretien"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignature4" runat="server" Height="35px" Width="248px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Signature de l'agent"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoH015003" runat="server" Height="17px" Width="500px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>       
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoH015004" runat="server" Height="17px" Width="248px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Left" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage12" runat="server" Height="15px" Width="740px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="12 / 12" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:right" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>