﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P0_MEN

    '''<summary>
    '''Contrôle PAGE_0.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PAGE_0 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_CadreTitreEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreTitreEntretien As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_CadreLogoMEN.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreLogoMEN As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_Etiquette As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelAnneeEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelAnneeEntretien As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_CadreEnteteEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreEnteteEntretien As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_LabelTitreAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreAgent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_EtiLabelIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelIdentite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelIdentite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_EtiLabelPatronyme.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelPatronyme As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelPatronyme.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelPatronyme As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_EtiLabelNaissance.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelNaissance As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelNaissance.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelNaissance As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_EtiLabelStatut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelStatut As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelStatut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelStatut As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_EtiLabelGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelGrade As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelGrade As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_EtiLabelEchelon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelEchelon As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelEchelon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelEchelon As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_EtiLabelDateEchelon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelDateEchelon As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelDateEchelon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelDateEchelon As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelTitreManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_EtiLabelManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_InfoH15001.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15001 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_EtiLabelGradeManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelGradeManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelGradeManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelGradeManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_EtiLabelFonctionManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelFonctionManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelFonctionManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelFonctionManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_EtiLabelDirectionManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelDirectionManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_InfoH15002.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15002 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_CadreEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreEntretien As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_EtiLabelDateEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelDateEntretien As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_InfoH15000.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15000 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_CadreEmploi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreEmploi As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_CadreTitreEmploi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreTitreEmploi As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_LabelVoletEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelVoletEntete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelVolet As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelVoletSuite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelVoletSuite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_CadreTitreStructure.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreTitreStructure As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_EtiLabelStructure.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelStructure As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelEtablissement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelEtablissement As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreNumero1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_LabelTitreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreNumero1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_InfoH15005.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15005 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelTitreEntreeEtablissement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreEntreeEtablissement As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelEntreeEtablissement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelEntreeEtablissement As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreNumero2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_LabelTitreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreNumero2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_InfoH15006.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15006 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_CadreNumero3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreNumero3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_LabelTitreNumero31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreNumero31 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelAffectationNiveau1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelAffectationNiveau1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelTitreNumero32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreNumero32 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelAffectationNiveau2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelAffectationNiveau2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_CadreNumero4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreNumero4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_LabelTitreNumero41.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreNumero41 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelTitreNumero42.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreNumero42 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_CadreNumero5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreNumero5 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_LabelTitreNumero5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreNumero5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_InfoV07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoV07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle O_CadreNumero6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreNumero6 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_LabelTitreNumero6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreNumero6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableSiConduiteProjet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableSiConduiteProjet As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_LabelSiConduiteProjet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelSiConduiteProjet As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_RadioH25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_RadioH25 As Global.Virtualia.Net.VirtualiaControle_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle O_LabelCadrage2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelCadrage2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableSiEncadrement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableSiEncadrement As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_LabelSiEncadrement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelSiEncadrement As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_RadioH08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_RadioH08 As Global.Virtualia.Net.VirtualiaControle_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle O_LabelCadrage1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelCadrage1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableAgentsEncadres.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableAgentsEncadres As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle O_LabelNbEncadres.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelNbEncadres As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_InfoH15009.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15009 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelRepartition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelRepartition As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_InfoH15026.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15026 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelCategorieA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelCategorieA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_InfoH15027.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15027 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelCategorieB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelCategorieB As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_InfoH15028.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15028 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelCategorieC.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelCategorieC As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle O_LabelCadrage3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelCadrage3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NumeroPage1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage1 As Global.System.Web.UI.WebControls.Label
End Class
