﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P0_AFB
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
            End If
        End Set
    End Property

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim Tableaudata(0) As String
        Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)

        CadreLogoAFB.BackImageUrl = Tableaudata(0) & "/Images/General/Logo_AFB.jpg"
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim IndiceI As Integer
        Dim Chaine As String

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If
        O_LabelAnneeEntretien.Text = "ANNEE " & WsDossierPER.Annee
        O_EtiPeriodeEntretien.Text = WsDossierPER.AnneePrecedente
        O_LabelIdentite.Text = WsDossierPER.Qualite & " " & WsDossierPER.Nom & " " & WsDossierPER.Prenom
        O_LabelDateNaissance.Text = WsDossierPER.DateDeNaissance
        O_LabelStatut.Text = WsDossierPER.Statut(DatedEffet)
        O_LabelCorps.Text = WsDossierPER.Corps(DatedEffet)
        O_LabelGrade.Text = WsDossierPER.Grade(DatedEffet)
        O_LabelEchelon.Text = WsDossierPER.Echelon(DatedEffet)
        ' O_LabelDateEchelon.Text = V_WebFonction.InfoExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, 678, WsDossierPER.V_Identifiant, DatedEffet)

        ''Manager 
        O_LabelManagerCorps.Text = WsDossierPER.CorpsManager
        O_LabelManagerGrade.Text = WsDossierPER.GradeManager
        O_LabelManagerFonction.Text = WsDossierPER.FonctionManager

        For IndiceI = 4 To 1 Step -1
            Chaine = WsDossierPER.NiveauAffectation(DatedEffet, IndiceI)
            If Chaine <> "" Then
                O_LabelAffectation.Text = Chaine
                Exit For
            End If
        Next IndiceI

        Chaine = WsDossierPER.FonctionExercee(DatedEffet, True)
        If Chaine <> "" Then
            O_LabelPoste.Text = WsDossierPER.FonctionExercee(DatedEffet, False)
            O_LabelDatePoste.Text = Chaine
        End If

        Call LireLaFiche()
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreInfo, "O_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 8, 3))
            VirLabel.Text = ValeurLue(NumObjet, NumInfo)
            IndiceI += 1
        Loop

    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
            End Select
            Return ""
        End Get
    End Property
End Class