﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P5_AFB.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P5_AFB" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioVerticalRadio.ascx" tagname="VTrioVerticalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="V_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreTitreFormation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="V_CadreTitreEntretien" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="Black">
                        <asp:TableRow>
                            <asp:TableCell Height="2px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Table ID="V_CadreLogoAFB" runat="server" BackImageUrl="~/Images/General/Logo_AFB.jpg" Width="166px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true">
                                    <asp:TableRow>
                                        <asp:TableCell Height="75px" BackColor="Transparent" Width="166px"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="10px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="V_Etiquette" runat="server" Text="5 - COMPTE RENDU D'ENTRETIEN DE FORMATION" Height="25px" Width="748px"
                                    BackColor="Transparent"  BorderColor="Transparent" BorderStyle="None"
                                    BorderWidth="1px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                    style="margin-top: 1px; margin-left: 1px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="V_LabelAnneeEntretien" runat="server" Text="" Height="25px" Width="748px"
                                    BackColor="Transparent"  BorderColor="Black" BorderStyle="None"
                                    BorderWidth="2px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                    style="margin-top: 10px; margin-left: 1px; margin-bottom: 10px;
                                    font-style:  normal; text-indent: 2px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="V_CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left"
                        BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">                        
                        <asp:TableRow>
                            <asp:TableCell Height="7px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="V_EtiLabelIdentite" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Nom et prénom : "
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 4px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="V_LabelIdentite" runat="server" Height="30px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 0px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="V_EtiLabelPoste" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Intitulé du poste ooste occupé :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 4px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="V_LabelPoste" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="V_EtiLabelDatePoste" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Date de prise de fonction du poste actuel : "
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 4px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="V_LabelDatePoste" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="V_EtiLabelManager" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Supérieur hiérarchique ayant conduit l'entretien : "
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 4px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="V_InfoH015001" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>                       
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreNumero11" runat="server" Height="30px" Width="748px"
                           BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Solid" Text="BILAN DE L'ANNEE"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
<%--                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="V_ListeFormationsSuivies" runat="server" CadreWidth="746px" SiColonneSelect="false"
                            SiAppliquerCharte="false" BackColorCaption="White" ForeColorCaption="Black" BackColorRow="White" ForeColorRow="Black"/>
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow> --%>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero12" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="a) Actions de formation suivies au titre de la formation continue : thématique et durée"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVFA03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="200px" EtiHeight="20px" DonTabIndex="2" 
                           EtiText="" EtiVisible ="false"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" >
                        <asp:Label ID="V_LabelTitreNumero14" runat="server" Height="50px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="b) Actions de formation suivies au titre de la PEC (préparation aux examens concours) : thématique et durée"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" >
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVFI03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="4" 
                           EtiText="" Etivisible="false"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableFooterRow>
                    <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
                        <asp:Label ID="NumeroPage15" runat="server" Height="15px" Width="150px"
                             BackColor="Transparent" BorderStyle="None"
                             Text="13 / 18" ForeColor="Black" Font-Italic="True"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                             style="text-align:left" >
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell >
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" >
                        <asp:Label ID="V_LabelTitreNumero15" runat="server" Height="50px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="c) Autres actions suivies (VAE-validation des acquis de l'expérience, CEP-congé de formation professionnelle, bilan de carrière, etc.) : thématiques et durée"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" >
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVFJ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="5" 
                           EtiText=""  EtiVisible ="false"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>   
                                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" >
                        <asp:Label ID="Label1" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="d) Actions de formation conduites en tant que formateur interne : thématique et durée "
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVFK03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                            EtiWidth="746px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="5"
                            EtiText="" EtiVisible="false"
                            EtiBorderStyle="None" EtiBackColor="Transparent"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
<%--                                                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label2" runat="server" Height="40px" Width="690px"
                                        BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                                        Text="Nombre d'heures de DIF (droit individuel à la formation) mobilisées au cours de l'année"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                        Style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; font-style: normal; text-indent: 0px; text-align: Left;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="V_InfoHI15511" runat="server" Height="40px" Width="58px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px"
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 0px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
        </asp:TableRow> --%>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
       <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage16" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="14 / 18" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:left" >
            </asp:Label>   
       </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
       <asp:TableCell >
            <div style="page-break-before:always;"></div>
       </asp:TableCell>
    </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                           BackColor="#DFF2FF" BorderColor="Black" BorderStyle="solid" Text="PERSPECTIVES DE L'ANNEE A VENIR"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero21" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="a) Actions de formation continue sollicitées au regard des Compétences attendues sur le poste"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>

                    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" ColumnSpan ="2">
            <asp:Table ID="Table2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="Transparent" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow  BackColor="Transparent">
                    <asp:TableCell HorizontalAlign="Center" >
                        <asp:Label ID="Label3" runat="server" Height="20px" Width="230px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Actions de formation continue*"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Label4" runat="server" Height="20px" Width="500px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Compétences** individuelles concernées"
                            ForeColor="Black" Font-Italic="False"
                          Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVA01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="230px" DonHeight="130px" DonTabIndex="16"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVA03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="130px" DonTabIndex="17"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVB01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="230px" DonHeight="130px" DonTabIndex="18"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVB03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="130px" DonTabIndex="19"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVC01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="230px" DonHeight="130px" DonTabIndex="20"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVC03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="130px" DonTabIndex="21"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVD01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="230px" DonHeight="130px" DonTabIndex="22"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVD03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="130px" DonTabIndex="23"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVE01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="230px" DonHeight="130px" DonTabIndex="22"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVE03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="130px" DonTabIndex="23"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVF01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="230px" DonHeight="130px" DonTabIndex="22"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVF03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="130px" DonTabIndex="23"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelLegendeT11" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="* "
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelLegendeT12" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="action de formation, inscription dans un parcours de professionnalisation thématique, tutorat, compagnonnage, etc"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelLegendeT21" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="** "
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelLegendeT22" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="la compétence individuelle doit être appréciee dans toutes ses composantes : savoir être, savoir faire, conaissance métiers"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelLegendeT31" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelLegendeT32" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Cette compétence individuelle s'apprécie au regard de compétences collectives présentes dans le service"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

        <asp:TableFooterRow>
       <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="Label7" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="15 / 18" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:left" >
            </asp:Label>   
       </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
       <asp:TableCell >
            <div style="page-break-before:always;"></div>
       </asp:TableCell>
    </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>

      <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" ColumnSpan ="2">
            <asp:Table ID="Table3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="Transparent" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow  BackColor="Transparent">
                    <asp:TableCell HorizontalAlign="Center" >
                        <asp:Label ID="Label5" runat="server" Height="20px" Width="230px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Concours / examens visés"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Label6" runat="server" Height="20px" Width="500px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Actions de formation PEC (préparation aux examens et coucours)"
                            ForeColor="Black" Font-Italic="False"
                          Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVG01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="230px" DonHeight="130px" DonTabIndex="16"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVG03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="130px" DonTabIndex="17"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVH01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="230px" DonHeight="130px" DonTabIndex="18"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVH03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="130px" DonTabIndex="19"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                            </asp:Table>
                       </asp:TableCell> 
    </asp:TableRow>
            <asp:TableRow>
          <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
                    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Label8" runat="server" Height="50px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Autres actions sollicitées <small> (validation des acquis de l'expérience, bilan de compétences, période de professionnalisation, etc...) Préciser le(s) motif(s) </small>"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVFL03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="200px" EtiHeight="20px" DonTabIndex="2" 
                           EtiText="" EtiVisible ="false"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>


    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
       <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage18" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="16 / 18" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:left">
            </asp:Label>   
       </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>