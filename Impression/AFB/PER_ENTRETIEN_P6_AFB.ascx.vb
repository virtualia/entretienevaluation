﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P6_AFB
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        ''V_LabelTitreNumero10.Text = "Bilan de l'année " & WsDossierPER.AnneePrecedente
        'V_LabelTitreNumero13.Text = "Engagement sur l'année " & WsDossierPER.Annee
        'V_LabelTitreNumero14.Text = "Commentaires de l'agent sur l'engagement année " & WsDossierPER.Annee
        'V_LabelTitreNumero15.Text = "Commentaires du manager sur l'engagement année " & WsDossierPER.Annee

        Call LireLaFiche()

    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label

        Do
            Ctl = V_WebFonction.VirWebControle(Me.VI_CadreInfo, "VI_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            'NumObjet = CShort(Strings.Mid(Ctl.ID, 10, 3))
            'Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1))
            If Ctl.ID.Length > 14 Then
                NumObjet = CShort(Strings.Mid(Ctl.ID, 11, 3))
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 2))
            Else
                NumObjet = CShort(Strings.Mid(Ctl.ID, 10, 3))
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1))
            End If

            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.VI_CadreInfo, "VI_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            'Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1))
            If Ctl.ID.Length > 11 Then
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 2))
            Else
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1))
            End If
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirCoche As VirtualiaControle_VCocheSimple
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.VI_CadreInfo, "VI_Coche", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirCoche = CType(Ctl, VirtualiaControle_VCocheSimple)
            NumObjet = VirCoche.V_Objet
            VirCoche.V_Check = False
            VirCoche.V_ForeColor = Drawing.Color.LightGray
            Select Case Strings.Mid(Ctl.ID, 8, 1)
                Case "A"
                    If ValeurLue(NumObjet, NumInfo, "") = "1" Then
                        VirCoche.V_Check = True
                        VirCoche.V_ForeColor = Drawing.Color.Black
                        VirCoche.V_Text = "  Insuffisante"
                    End If
                Case "B"
                    If ValeurLue(NumObjet, NumInfo, "") = "2" Then
                        VirCoche.V_Check = True
                        VirCoche.V_ForeColor = Drawing.Color.Black
                        VirCoche.V_Text = "  Partielle"
                    End If
                Case "C"
                    If ValeurLue(NumObjet, NumInfo, "") = "3" Then
                        VirCoche.V_Check = True
                        VirCoche.V_ForeColor = Drawing.Color.Black
                        VirCoche.V_Text = "  Satisfaisante"
                    End If
                Case "D"
                    If ValeurLue(NumObjet, NumInfo, "") = "4" Then
                        VirCoche.V_Check = True
                        VirCoche.V_ForeColor = Drawing.Color.Black
                        VirCoche.V_Text = "  Très satisfaisante"
                    End If
                Case "E"
                    If ValeurLue(NumObjet, NumInfo, "") = "5" Then
                        VirCoche.V_Check = True
                        VirCoche.V_ForeColor = Drawing.Color.Black
                        VirCoche.V_Text = "  Exceptionnelle"
                    End If
            End Select
            VirCoche.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 157
                    If WsDossierPER.Objet_157(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_157(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class