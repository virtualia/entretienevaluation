﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P5_AFB
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim Tableaudata(0) As String
        Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)

        V_CadreLogoAFB.BackImageUrl = Tableaudata(0) & "/Images/General/Logo_AFB.jpg"
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim Chaine As String

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If
        V_LabelAnneeEntretien.Text = "ANNEE " & WsDossierPER.Annee
        V_LabelIdentite.Text = WsDossierPER.Qualite & " " & WsDossierPER.Nom & " " & WsDossierPER.Prenom

        Chaine = WsDossierPER.FonctionExercee(DatedEffet, True)
        If Chaine <> "" Then
            V_LabelPoste.Text = WsDossierPER.FonctionExercee(DatedEffet, False)
            V_LabelDatePoste.Text = Chaine
        End If

        V_LabelTitreNumero11.Text = "BILAN DE L'ANNEE " & WsDossierPER.AnneePrecedente
        V_LabelTitreNumero2.Text = "PERSPECTIVES DE L'ANNEE " & WsDossierPER.Annee

        V_LibelListe = "Aucune formation" & VI.Tild & "Une formation" & VI.Tild & "formations"
        V_LibelColonne = "date" & VI.Tild & "formation suivie" & VI.Tild & "Durée" & VI.Tild & "Si DIF" & VI.Tild & "Clef"
        'V_ListeFormationsSuivies.V_Liste(V_LibelColonne, V_LibelListe) = WsDossierPER.ListeDesFormationsSuivies(WsDossierPER.Annee, 1)

        Call LireLaFiche()
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label

        Do
            Ctl = V_WebFonction.VirWebControle(Me.V_CadreInfo, "V_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 9, 3))
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1))
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.V_CadreInfo, "V_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            'Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1))
            If Ctl.ID.Length > 10 Then
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 2))
            Else
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1))
            End If
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            If Rang = "7.1.1" Then
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.1.2")
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.1.3")
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.1.4")
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.1.5")
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.1.6")
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.1.7")
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.1.8")
            End If
            If Rang = "7.5.1" Then
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.5.2")
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.5.3")
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.5.4")
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.5.5")
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.5.6")
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.5.7")
                VirVertical.DonText &= Chr(13) & ValeurLue(NumObjet, NumInfo, "7.5.8")
            End If
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VTrioVerticalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.V_CadreInfo, "V_RadioV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VTrioVerticalRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 10, 1))
            Select Case NumInfo
                Case 2
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "T1"
                            VirRadio.RadioGaucheCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "T2"
                            VirRadio.RadioCentreCheck = True
                            VirRadio.RadioGaucheText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "T3"
                            VirRadio.RadioDroiteCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioGaucheText = ""
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
                Case 8
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "P1"
                            VirRadio.RadioGaucheCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "P2"
                            VirRadio.RadioCentreCheck = True
                            VirRadio.RadioGaucheText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "P3"
                            VirRadio.RadioDroiteCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioGaucheText = ""
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
                Case 12
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "Sous DIF - TS"
                            VirRadio.RadioGaucheCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "Sous DIF - HTS"
                            VirRadio.RadioCentreCheck = True
                            VirRadio.RadioGaucheText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "Hors DIF"
                            VirRadio.RadioDroiteCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioGaucheText = ""
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
            End Select
            VirRadio.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 155
                    If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_155(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class