﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P1_AFB.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P1_AFB" %>

<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VTrioVerticalRadio.ascx" TagName="VTrioVerticalRadio" TagPrefix="Virtualia" %>

<div style="page-break-before: always;"></div>

<asp:Table ID="I_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreTitreResultats" runat="server" Height="30px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVolet" runat="server" Text="BILAN DE L'ANNEE" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            Style="margin-top: 1px; margin-left: 1px; margin-bottom: 10px; font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVoletBis" runat="server" Height="25px" Width="746px"
                            Text="1-Atteinte des objectifs et actions conduites"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            Style="margin-top: 1px; margin-left: 1px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="Label1" runat="server" Height="18px" Width="748px"
                                        BackColor="#DFF2FF" BorderStyle="None"
                                        Text="CONTEXTE"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                    <asp:Label ID="Label2" runat="server" Height="18px" Width="748px"
                                        BackColor="#DFF2FF" BorderStyle="None"
                                        Text="(Politique, environnement, réorganisation, moyens, coopération interne ou externe, ... objectifs du service)"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="50%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVAA03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="1"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelNumerobjectif1" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°1"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteObjectif1" runat="server" Height="32px" Width="301px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Rappel de l'objectif <br/> fixé pour l'année"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs1" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteIndicateurs1" runat="server" Height="32px" Width="260px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Indicateurs <br/> de réussite prévus"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation1" runat="server" Height="32px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="297px" DonHeight="210px" DonTabIndex="1"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur1" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="256px" DonHeight="210px" DonTabIndex="2"
                                        EtiBorderStyle="None" EtiBackColor="Transparent"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" BorderColor="Black" BorderStyle="solid" BorderWidth="1px">
                        <asp:Table ID="I_TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VTrioVerticalRadio ID="I_RadioHC04" runat="server" V_Groupe="Realisation1"
                                        V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                        RadioGaucheWidth="173px" RadioCentreWidth="173px" RadioDroiteWidth="173px"
                                        RadioGaucheHeight="34px" RadioCentreHeight="34px" RadioDroiteHeight="34px"
                                        RadioGaucheStyle="margin-left: 0px; margin-top: 9px; text-align:  left;" RadioCentreStyle="margin-left: 0px; margin-top: 1px; text-align:  left;" RadioDroiteStyle="margin-left: 0px; margin-top: 1px; text-align:  left"
                                        RadioGaucheText="Atteint" RadioCentreText="Partiellement atteint" RadioDroiteText="Non atteint"
                                        RadioGaucheBackColor="Transparent" RadioCentreBackColor="Transparent" RadioDroiteBackColor="Transparent"
                                        RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteCommentaire1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteCommentaire1" runat="server" Height="18px" Width="748px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Commentaires"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableCommentaire1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC07" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelNumerobjectif2" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°2"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteObjectif2" runat="server" Height="32px" Width="301px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Rappel de l'objectif <br/> fixé pour l'année"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs2" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteIndicateurs2" runat="server" Height="32px" Width="260px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Indicateurs <br/> de réussite prévus"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation2" runat="server" Height="32px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="297px" DonHeight="210px" DonTabIndex="4"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur2" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="256px" DonHeight="210px" DonTabIndex="5"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" BorderColor="Black" BorderStyle="solid" BorderWidth="1px">
                        <asp:Table ID="I_TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VTrioVerticalRadio ID="I_RadioHD04" runat="server" V_Groupe="Realisation2"
                                        V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                        RadioGaucheWidth="173px" RadioCentreWidth="173px" RadioDroiteWidth="173px"
                                        RadioGaucheHeight="34px" RadioCentreHeight="34px" RadioDroiteHeight="34px"
                                        RadioGaucheStyle="margin-left: 0px; margin-top: 9px; text-align:  left" RadioCentreStyle="margin-left: 0px; margin-top: 1px; text-align:  left" RadioDroiteStyle="margin-left: 0px; margin-top: 1px; text-align:  left"
                                        RadioGaucheText="Atteint" RadioCentreText="Partiellement atteint" RadioDroiteText="Non atteint"
                                        RadioGaucheBackColor="Transparent" RadioCentreBackColor="Transparent" RadioDroiteBackColor="Transparent"
                                        RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteCommentaire2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteCommentaire2" runat="server" Height="18px" Width="748px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Commentaires"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableCommentaire2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD07" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="6"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage2" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="2 / 18" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                Style="text-align: left">
            </asp:Label>
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell>
             <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelNumerobjectif3" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°3"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteObjectif3" runat="server" Height="32px" Width="301px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Rappel de l'objectif <br/> fixé pour l'année"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs3" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteIndicateurs3" runat="server" Height="32px" Width="260px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Indicateurs <br/> de réussite prévus"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation3" runat="server" Height="32px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="297px" DonHeight="210px" DonTabIndex="7"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur3" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="256px" DonHeight="210px" DonTabIndex="8"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" BorderColor="Black" BorderStyle="solid" BorderWidth="1px">
                        <asp:Table ID="I_TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VTrioVerticalRadio ID="I_RadioHE04" runat="server" V_Groupe="Realisation3"
                                        V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                        RadioGaucheWidth="173px" RadioCentreWidth="173px" RadioDroiteWidth="173px"
                                        RadioGaucheHeight="34px" RadioCentreHeight="34px" RadioDroiteHeight="34px"
                                        RadioGaucheStyle="margin-left: 0px; margin-top: 9px; text-align:  left" RadioCentreStyle="margin-left: 0px; margin-top: 1px; text-align:  left" RadioDroiteStyle="margin-left: 0px; margin-top: 1px; text-align:  left"
                                        RadioGaucheText="Atteint" RadioCentreText="Partiellement atteint" RadioDroiteText="Non atteint"
                                        RadioGaucheBackColor="Transparent" RadioCentreBackColor="Transparent" RadioDroiteBackColor="Transparent"
                                        RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteCommentaire3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteCommentaire3" runat="server" Height="18px" Width="748px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Commentaires"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableCommentaire3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE07" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="9"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelNumerobjectif4" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°4"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteObjectif4" runat="server" Height="32px" Width="301px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Rappel de l'objectif <br/> fixé pour l'année"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs4" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteIndicateurs4" runat="server" Height="32px" Width="260px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Indicateurs <br/> de réussite prévus"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation4" runat="server" Height="32px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="297px" DonHeight="210px" DonTabIndex="10"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur4" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="256px" DonHeight="210px" DonTabIndex="11"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" BorderColor="Black" BorderStyle="solid" BorderWidth="1px">
                        <asp:Table ID="I_TableRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VTrioVerticalRadio ID="I_RadioHF04" runat="server" V_Groupe="Realisation4"
                                        V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                        RadioGaucheWidth="173px" RadioCentreWidth="173px" RadioDroiteWidth="173px"
                                        RadioGaucheHeight="34px" RadioCentreHeight="34px" RadioDroiteHeight="34px"
                                        RadioGaucheStyle="margin-left: 0px; margin-top: 9px; text-align:  left" RadioCentreStyle="margin-left: 0px; margin-top: 1px; text-align:  left" RadioDroiteStyle="margin-left: 0px; margin-top: 1px; text-align:  left"
                                        RadioGaucheText="Atteint" RadioCentreText="Partiellement atteint" RadioDroiteText="Non atteint"
                                        RadioGaucheBackColor="Transparent" RadioCentreBackColor="Transparent" RadioDroiteBackColor="Transparent"
                                        RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteCommentaire4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteCommentaire4" runat="server" Height="18px" Width="748px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Commentaires"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableCommentaire4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF07" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="12"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
        <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage3" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="3 / 18" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                Style="text-align: left">
            </asp:Label>
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell>
             <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelNumerobjectif5" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°5"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteObjectif5" runat="server" Height="32px" Width="301px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Rappel de l'objectif <br/> fixé pour l'année"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs5" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteIndicateurs5" runat="server" Height="32px" Width="260px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Indicateurs <br/> de réussite prévus"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation5" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation5" runat="server" Height="32px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="297px" DonHeight="170px" DonTabIndex="13"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur5" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="256px" DonHeight="170px" DonTabIndex="14"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" BorderColor="Black" BorderStyle="solid" BorderWidth="1px">
                        <asp:Table ID="I_TableRealisation5" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VTrioVerticalRadio ID="I_RadioHG04" runat="server" V_Groupe="Realisation5"
                                        V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                        RadioGaucheWidth="173px" RadioCentreWidth="173px" RadioDroiteWidth="173px"
                                        RadioGaucheHeight="34px" RadioCentreHeight="34px" RadioDroiteHeight="34px"
                                        RadioGaucheStyle="margin-left: 0px; margin-top: 9px; text-align:  left" RadioCentreStyle="margin-left: 0px; margin-top: 1px; text-align:  left" RadioDroiteStyle="margin-left: 0px; margin-top: 1px; text-align:  left"
                                        RadioGaucheText="Atteint" RadioCentreText="Partiellement atteint" RadioDroiteText="Non atteint"
                                        RadioGaucheBackColor="Transparent" RadioCentreBackColor="Transparent" RadioDroiteBackColor="Transparent"
                                        RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteCommentaire5" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteCommentaire5" runat="server" Height="18px" Width="748px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Commentaires"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableCommentaire5" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG07" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="30px" DonTabIndex="15"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif6" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelNumerobjectif6" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°6"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif6" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteObjectif6" runat="server" Height="32px" Width="301px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Rappel de l'objectif <br/> fixé pour l'année"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs6" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteIndicateurs6" runat="server" Height="32px" Width="260px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Indicateurs <br/> de réussite prévus"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation6" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation6" runat="server" Height="32px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif6" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="297px" DonHeight="170px" DonTabIndex="16"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur6" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="256px" DonHeight="170px" DonTabIndex="17"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" BorderColor="Black" BorderStyle="solid" BorderWidth="1px">
                        <asp:Table ID="I_TableRealisation6" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VTrioVerticalRadio ID="I_RadioHH04" runat="server" V_Groupe="Realisation6"
                                        V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                        RadioGaucheWidth="173px" RadioCentreWidth="173px" RadioDroiteWidth="173px"
                                        RadioGaucheHeight="34px" RadioCentreHeight="34px" RadioDroiteHeight="34px"
                                        RadioGaucheStyle="margin-left: 0px; margin-top: 9px; text-align:  left" RadioCentreStyle="margin-left: 0px; margin-top: 1px; text-align:  left" RadioDroiteStyle="margin-left: 0px; margin-top: 1px; text-align:  left"
                                        RadioGaucheText="Atteint" RadioCentreText="Partiellement atteint" RadioDroiteText="Non atteint"
                                        RadioGaucheBackColor="Transparent" RadioCentreBackColor="Transparent" RadioDroiteBackColor="Transparent"
                                        RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteCommentaire6" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteCommentaire6" runat="server" Height="18px" Width="748px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Commentaires"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableCommentaire6" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH07" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="30px" DonTabIndex="18"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif7" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelNumerobjectif7" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°7"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif7" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteObjectif7" runat="server" Height="32px" Width="301px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Rappel de l'objectif <br/> fixé pour l'année"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs7" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteIndicateurs7" runat="server" Height="32px" Width="260px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Indicateurs <br/> de réussite prévus"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation7" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation7" runat="server" Height="32px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif7" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVI02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="297px" DonHeight="150px" DonTabIndex="19"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur7" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVI03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="256px" DonHeight="150px" DonTabIndex="20"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" BorderColor="Black" BorderStyle="solid" BorderWidth="1px">
                        <asp:Table ID="I_TableRealisation7" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VTrioVerticalRadio ID="I_RadioHI04" runat="server" V_Groupe="Realisation7"
                                        V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                        RadioGaucheWidth="173px" RadioCentreWidth="173px" RadioDroiteWidth="173px"
                                        RadioGaucheHeight="34px" RadioCentreHeight="34px" RadioDroiteHeight="34px"
                                        RadioGaucheStyle="margin-left: 0px; margin-top: 9px; text-align:  left" RadioCentreStyle="margin-left: 0px; margin-top: 1px; text-align:  left" RadioDroiteStyle="margin-left: 0px; margin-top: 1px; text-align:  left"
                                        RadioGaucheText="Atteint" RadioCentreText="Partiellement atteint" RadioDroiteText="Non atteint"
                                        RadioGaucheBackColor="Transparent" RadioCentreBackColor="Transparent" RadioDroiteBackColor="Transparent"
                                        RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteCommentaire7" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteCommentaire7" runat="server" Height="18px" Width="748px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Commentaires"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableCommentaire7" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVI07" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="30px" DonTabIndex="21"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage4" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="4 / 18" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                Style="text-align: left">
            </asp:Label>
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell>
             <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="25px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObservations" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelObservations" runat="server" Height="18px" Width="748px"
                            BackColor="#DFF2FF" BorderStyle="None"
                            Text="OBSERVATIONS ET COMMENATIRES"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                        <asp:Label ID="I_LabelObservations_2" runat="server" Height="18px" Width="748px"
                            BackColor="#DFF2FF" BorderStyle="None"
                            Text="(travaux, missions réalisés non prévus dans les objectifs de l'année précédente)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="50%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVAB03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="743px" DonHeight="100px" DonTabIndex="22"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Transparent" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
                    <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreParticipation" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">

                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelParticipation" runat="server" Height="18px" Width="748px"
                            BackColor="#DFF2FF" BorderStyle="None"
                            Text="PARTICIPATION A LA VIE COLLECTIVE"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                        <asp:Label ID="I_LabelParticipation_2" runat="server" Height="18px" Width="748px"
                            BackColor="#DFF2FF" BorderStyle="None"
                            Text="(formateur interne, membre de jury, animation/participation à un groupe de travail, un réseau, etc.)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="50%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVAC03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="743px" DonHeight="100px" DonTabIndex="23"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Transparent" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage5" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="5 / 18" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                Style="text-align: left">
            </asp:Label>
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>
