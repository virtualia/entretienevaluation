﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P6_CNED

    '''<summary>
    '''Contrôle VI_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_CadreEnteteDePage1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreEnteteDePage1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelEnteteLigne11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteLigne11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEnteteLigne12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteLigne12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreTitreFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreTitreFormation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelVoletEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelVoletEntete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelVolet As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelVoletSuite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelVoletSuite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelTitreCommentaires.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreCommentaires As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreNumero1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelTitreNumero11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_ListeFormationsSuivies.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_ListeFormationsSuivies As Global.Virtualia.Net.VirtualiaControle_VListeGrid

    '''<summary>
    '''Contrôle VI_LabelTitreNumero12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVW03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVW03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle NumeroPage11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreEnteteDePage2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreEnteteDePage2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelEnteteLigne21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteLigne21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEnteteLigne22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteLigne22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreNumero2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelTitreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_TableBesoin102.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableBesoin102 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelEnteteStage102.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteStage102 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEntetePeriode102.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEntetePeriode102 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVA01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVA01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioHA08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHA08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle VI_InfoVB01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVB01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioHB08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHB08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle VI_InfoVC01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVC01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioHC08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHC08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle VI_InfoVD01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVD01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioHD08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHD08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle VI_TableBesoin103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableBesoin103 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelTypeBesoin103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTypeBesoin103 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelTypeBesoin103Bis.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTypeBesoin103Bis As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEnteteStage103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteStage103 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEntetePeriode103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEntetePeriode103 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVE01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVE01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioHE08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHE08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle VI_InfoVF01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVF01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioHF08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHF08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle VI_InfoVG01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVG01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioHG08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHG08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle VI_InfoVH01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVH01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioHH08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHH08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle NumeroPage12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_CadreEnteteDePage3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreEnteteDePage3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelEnteteLigne31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteLigne31 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEnteteLigne32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteLigne32 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_TableBesoin104.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableBesoin104 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelTypeBesoin104.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTypeBesoin104 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEnteteStage104.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEnteteStage104 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelEntetePeriode104.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelEntetePeriode104 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_InfoVI01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVI01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioHI08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHI08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle VI_InfoVJ01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVJ01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioHJ08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHJ08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle VI_InfoVK01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVK01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioHK08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHK08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle VI_InfoVL01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVL01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle VI_RadioHL08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHL08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle VI_TableDIF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_TableDIF As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VI_LabelTitre105.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitre105 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelTitreDIF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreDIF As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSoldeDIF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSoldeDIF As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_LabelSiUtilisationDIF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSiUtilisationDIF As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VI_RadioHM19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_RadioHM19 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle NumeroPage14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage14 As Global.System.Web.UI.WebControls.Label
End Class
