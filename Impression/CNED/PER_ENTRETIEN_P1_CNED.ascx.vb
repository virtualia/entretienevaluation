﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P1_CNED
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim Chaine As String = ""

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        Chaine = WsDossierPER.LibelleIdentite
        Chaine += " - " & "Grade : " & WsDossierPER.Grade(DatedEffet)
        I_LabelEnteteLigne11.Text = Chaine
        I_LabelEnteteLigne21.Text = Chaine

        Chaine = ""
        Chaine += "Site géographique : " & WsDossierPER.SiteGeographique
        Chaine += " - " & "Direction : " & WsDossierPER.NiveauAffectation(DatedEffet, 1)
        I_LabelEnteteLigne12.Text = Chaine
        I_LabelEnteteLigne22.Text = Chaine

        Call LireLaFiche()
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label

        Do
            Ctl = V_WebFonction.VirWebControle(Me.I_CadreInfo, "I_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 9, 3))
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1), "CNED")
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.I_CadreInfo, "I_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1), "CNED")
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VSixVerticalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.I_CadreInfo, "I_RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VSixVerticalRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1), "CNED")
            If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                Select Case ValeurLue(NumObjet, NumInfo, Rang)
                    Case "4"
                        VirRadio.VRadioN1Check = True
                        VirRadio.VRadioN2Text = ""
                        VirRadio.VRadioN3Text = ""
                        VirRadio.VRadioN4Text = ""
                        VirRadio.VRadioN5Text = ""
                    Case "3"
                        VirRadio.VRadioN2Check = True
                        VirRadio.VRadioN3Text = ""
                        VirRadio.VRadioN4Text = ""
                        VirRadio.VRadioN5Text = ""
                        VirRadio.VRadioN1Text = ""
                    Case "2"
                        VirRadio.VRadioN3Check = True
                        VirRadio.VRadioN2Text = ""
                        VirRadio.VRadioN1Text = ""
                        VirRadio.VRadioN4Text = ""
                        VirRadio.VRadioN5Text = ""
                    Case "1"
                        VirRadio.VRadioN4Check = True
                        VirRadio.VRadioN2Text = ""
                        VirRadio.VRadioN3Text = ""
                        VirRadio.VRadioN1Text = ""
                        VirRadio.VRadioN5Text = ""
                    Case "0"
                        VirRadio.VRadioN5Check = True
                        VirRadio.VRadioN2Text = ""
                        VirRadio.VRadioN3Text = ""
                        VirRadio.VRadioN4Text = ""
                        VirRadio.VRadioN1Text = ""
                    Case Else
                        VirRadio.VRadioN1Check = False
                        VirRadio.VRadioN2Check = False
                        VirRadio.VRadioN3Check = False
                        VirRadio.VRadioN4Check = False
                        VirRadio.VRadioN5Check = False
                End Select
            End If
            VirRadio.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 151
                    If WsDossierPER.Objet_151(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_151(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class