﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P6_FAM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P6_FAM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="VI_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreTitreAppreciation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVolet" runat="server" Text="VI. Appréciations générales sur la valeur professionnelle de l'agent"
                            Height="20px" Width="746px"
                            BackColor="Transparent"  BorderStyle="None"
                            ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero11" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Evaluation globale des résultats obtenus par l'agent au regard"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero11Suite" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="des objectifs fixés initialement ou révisés, le cas échéant, en cours d'année"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero12" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Evaluation de la manière de servir de l'agent"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="VI_CadreCompetences" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="VI_LabelTitreCompetence" runat="server" Height="30px" Width="316px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="VI_CadreEnteteLabels" runat="server" CellPadding="0" CellSpacing="0">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="VI_LabelTitreNote1" runat="server" Height="30px" Width="107px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="EXCELLENT"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: normal; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="VI_LabelTitreNote2" runat="server" Height="30px" Width="105px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="TRES BON"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: normal; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="VI_LabelTitreNote3" runat="server" Height="30px" Width="105px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="SATISFAISANT"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: normal; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="VI_LabelTitreNote4" runat="server" Height="30px" Width="107px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="A DEVELOPPER"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: normal; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="VI_InfoHA15601" runat="server" Height="20px" Width="270px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="Qualité du travail" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="VI_RadioHA03" runat="server" V_Groupe="Critere1"
                                     V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                     VRadioN1Width="99px" VRadioN2Width="99px" VRadioN3Width="99px"
                                     VRadioN4Width="97px" VRadioN5Width="0px" VRadioN6Width="0px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="VI_InfoHB15601" runat="server" Height="20px" Width="270px"
                                    BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                    Text="Qualités relationnelles" ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="VI_RadioHB03" runat="server" V_Groupe="Critere2"
                                     V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                     VRadioN1Width="99px" VRadioN2Width="99px" VRadioN3Width="99px"
                                     VRadioN4Width="97px" VRadioN5Width="0px" VRadioN6Width="0px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="VI_InfoHC15601" runat="server" Height="20px" Width="270px"
                                    BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                    Text="Implication personnelle" ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="VI_RadioHC03" runat="server" V_Groupe="Critere3"
                                     V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                     VRadioN1Width="99px" VRadioN2Width="99px" VRadioN3Width="99px"
                                     VRadioN4Width="97px" VRadioN5Width="0px" VRadioN6Width="0px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="VI_InfoHD15601" runat="server" Height="20px" Width="270px"
                                    BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                    Text="Sens du service public" ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="VI_RadioHD03" runat="server" V_Groupe="Critere4"
                                     V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                     VRadioN1Width="99px" VRadioN2Width="99px" VRadioN3Width="99px"
                                     VRadioN4Width="97px" VRadioN5Width="0px" VRadioN6Width="0px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero2" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Aptitude à exercer des fonctions supérieures"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoV18" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="18" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="5" 
                           Etitext="(rempli par le supérieur hiérarchique direct)" Etivisible="True"
                           EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  center; text-indent: 1px; font-size: small; font-style: italic"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero3" runat="server" Height="25px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" Text="Appréciations littérales du supérieur hiérarchique direct"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="6" 
                           Etitext="" Etivisible="False"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="18px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreSignatureSuperieur" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="VI_LabelTitreSignatureSuperieur" runat="server" Height="25px" Width="738px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Signature du supérieur hiérarchique direct ayant conduit l'entretien"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="14px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelManager" runat="server" Height="20px" Width="120px"
                            BackColor="Transparent" BorderStyle="None" Text="Nom et prénom :"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 10px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_InfoH015001" runat="server" Height="20px" Width="580px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  normal; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelDateSignatureManager" runat="server" Height="20px" Width="120px"
                            BackColor="Transparent" BorderStyle="None" Text="Date :"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 10px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_InfoH015003" runat="server" Height="20px" Width="580px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  normal; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="VI_LabelSignatureSuperieur" runat="server" Height="90px" Width="738px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Signature"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 10px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage17" runat="server" Height="15px" Width="150px"
                    BackColor="Transparent" BorderStyle="None"
                    Text="17 / 18" ForeColor="Black" Font-Italic="True"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                    style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreTitreNotification" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="Transparent">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="VI_LabelVoletNotif" runat="server" Text="VII. Notification à l'agent du compte-rendu de l'entretien professionnel" 
                            Height="20px" Width="746px"
                            BackColor="Transparent"  BorderStyle="None"
                            ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelIdentite" runat="server" Height="20px" Width="120px"
                            BackColor="Transparent" BorderStyle="None" Text="Nom et prénom :"
                            ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelIdentite" runat="server" Height="20px" Width="580px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelAffectation" runat="server" Height="20px" Width="200px"
                            BackColor="Transparent" BorderStyle="None" Text="Direction - service - unité :"
                            ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelAffectation" runat="server" Height="20px" Width="500px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelAffectationUnite" runat="server" Height="20px" Width="200px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelAffectationUnite" runat="server" Height="20px" Width="500px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>     
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"  ColumnSpan="2">
                        <asp:Label ID="VI_LabelTitreNumero40" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Observations éventuelles de l'agent"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero41" runat="server" Height="39px" Width="370px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Sur la conduite de l'entretien et les thèmes abordés"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero42" runat="server" Height="39px" Width="370px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Sur les appréciations portées"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVV03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="370px" DonWidth="368px" DonHeight="200px" EtiHeight="20px" DonTabIndex="7" 
                           Etitext="" Etivisible="False"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:small"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVW03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="370px" DonWidth="368px" DonHeight="200px" EtiHeight="20px" DonTabIndex="8" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:small"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"  ColumnSpan="2">
                        <asp:Label ID="VI_LabelTitreNumero43" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Visa et observations éventuelles de l'autorité hiérarchique"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero44" runat="server" Height="39px" Width="370px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Sur la conduite de l'entretien et les thèmes abordés"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero45" runat="server" Height="39px" Width="370px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Sur les appréciations portées"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVX03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="370px" DonWidth="368px" DonHeight="200px" EtiHeight="20px" DonTabIndex="9" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:small"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVY03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="370px" DonWidth="368px" DonHeight="200px" EtiHeight="20px" DonTabIndex="10" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:small"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="7px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelNomAutorite" runat="server" Height="39px" Width="370px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Nom :"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 3px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero46" runat="server" Height="39px" Width="370px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Observations éventuelles"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelFonctionAutorite" runat="server" Height="152px" Width="370px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="<br/> &nbsp;Fonction exercée : <br/>  <br/>  <br/> &nbsp;Date : <br/>  <br/>  <br/> &nbsp;Visa du responsable hiérarchique :"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 3px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVZ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="370px" DonWidth="368px" DonHeight="150px" EtiHeight="20px" DonTabIndex="11" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:small"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreSignatureAgent" runat="server" CellPadding="0" CellSpacing="0" Width="740px">
                <asp:TableRow>
                    <asp:TableCell Height="14px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelDateSignatureAgent" runat="server" Height="20px" Width="120px"
                            BackColor="Transparent" BorderStyle="None" Text="Date :"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 0px; margin-left: 50px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_InfoH015004" runat="server" Height="20px" Width="500px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  normal; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="VI_LabelSignatureAgent" runat="server" Height="40px" Width="738px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Signature de l'agent"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 50px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreInformationAgent" runat="server" CellPadding="0" CellSpacing="0" Width="740px">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelInformationLigne1" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="La signature de l'agent vaut notification du présent document (et non approbation de son contenu)."
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelInformationLigne2" runat="server" Height="38px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Destinataire : Unité Formation, Evaluation et Dialogue Social (FEDS) pour versement au dossier individuel de l'agent."
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelInformationLigne3" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Copies : agent, supérieur hiérarchique direct et chef de service."
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelInformationLigne4" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Recours éventuels de l'agent"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: italic; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelInformationLigne5" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="L'agent peut saisir l'autorité hiérarchique d'une demande de révision de tout ou partie du compte-rendu de l'entretien
                            professionnel, dans un délai de quinze jours francs suivant la notification du présent document."
                            ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: italic; text-indent: 0px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelInformationLigne6" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="L'autorité hiérarchique dispose d'un délai de quinze jours à compter de la saisine pour lui répondre."
                            ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: italic; text-indent: 0px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_EtiLabelInformationLigne7" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="L'exercice de ce recours hiérarchique est un préalable obligatoire à la saisine de la CAP ou CCP compétente,
                            dans un délai d'un mois à compter de la réponse de l'autorité hiérarchique. L'agent dispose également des voies et
                            délais de recours de droit commun pour contester son compte-rendu de l'entretien professionnel."
                            ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: italic; text-indent: 0px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>  
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage18" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="18 / 18" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>