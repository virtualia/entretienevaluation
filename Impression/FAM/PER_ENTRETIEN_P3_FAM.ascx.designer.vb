﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P3_FAM

    '''<summary>
    '''Contrôle III_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_CadreTitreCompetence.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreTitreCompetence As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelVolet As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelVoletTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelVoletTitre As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelInformations.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelInformations As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelNiveau4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelNiveau4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelNiveau3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelNiveau3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelNiveau2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelNiveau2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelNiveau1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelNiveau1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelNiveau0.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelNiveau0 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreNumero1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelFamille1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelFamille1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSousFamille1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSousFamille1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreCompetences1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreCompetences1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreCompetence1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreCompetence1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote13 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote14 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote15 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHA15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHA15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHA09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHA09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHA07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHA07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace13 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHA15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHA15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreCompetences2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreCompetences2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreCompetence2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreCompetence2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote23 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote24 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote25 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHB15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHB15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHB09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHB09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHB07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHB07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace23 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHB15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHB15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreCompetences3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreCompetences3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreCompetence3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreCompetence3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote31 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote32 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote33 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote34 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote35.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote35 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHC15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHC15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace31 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHC09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHC09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace32 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHC07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHC07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace33 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHC15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHC15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreCompetences4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreCompetences4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreCompetence4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreCompetence4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote41.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote41 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote42.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote42 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote43.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote43 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote44.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote44 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote45.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote45 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHD15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHD15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace41.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace41 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHD09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHD09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace42.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace42 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHD07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHD07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace43.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace43 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHD15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHD15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreCompetences5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreCompetences5 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreCompetence5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreCompetence5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels5 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote51.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote51 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote52.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote52 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote53.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote53 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote54.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote54 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote55.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote55 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHE15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHE15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace51.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace51 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHE09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHE09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace52.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace52 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHE07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHE07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace53.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace53 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHE15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHE15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NumeroPage6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreNumero2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelFamille2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelFamille2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreSavoirFaire6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreSavoirFaire6 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreSavoirFaire6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreSavoirFaire6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels6 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote61.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote61 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote62.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote62 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote63.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote63 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote64.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote64 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote65.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote65 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHF15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHF15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace61.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace61 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHF09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHF09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace62.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace62 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHF07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHF07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace63.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace63 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHF15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHF15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreSavoirFaire7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreSavoirFaire7 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreSavoirFaire7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreSavoirFaire7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels7 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote71.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote71 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote72.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote72 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote73.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote73 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote74.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote74 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote75.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote75 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHG15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHG15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace71.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace71 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHG09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHG09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace72.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace72 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHG07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHG07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace73.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace73 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHG15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHG15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreSavoirFaire8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreSavoirFaire8 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreSavoirFaire8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreSavoirFaire8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels8 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote81.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote81 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote82.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote82 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote83.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote83 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote84.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote84 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote85.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote85 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHH15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHH15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace81.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace81 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHH09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHH09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace82.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace82 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHH07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHH07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace83.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace83 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHH15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHH15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreSavoirFaire9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreSavoirFaire9 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreSavoirFaire9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreSavoirFaire9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels9 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote91.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote91 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote92.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote92 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote93.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote93 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote94.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote94 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote95.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote95 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHI15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHI15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace91.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace91 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHI09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHI09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace92.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace92 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHI07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHI07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace93.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace93 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHI15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHI15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreSavoirFaire10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreSavoirFaire10 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreSavoirFaire10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreSavoirFaire10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels10 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote101.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote101 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote102.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote102 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote103 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote104.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote104 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote105.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote105 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHJ15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHJ15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace101.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace101 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHJ09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHJ09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace102.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace102 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHJ07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHJ07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace103 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHJ15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHJ15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreSavoirFaire11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreSavoirFaire11 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreSavoirFaire11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreSavoirFaire11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels11 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote111.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote111 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote112.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote112 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote113.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote113 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote114.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote114 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote115.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote115 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHK15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHK15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace111.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace111 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHK09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHK09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace112.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace112 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHK07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHK07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace113.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace113 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHK15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHK15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreSavoirFaire12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreSavoirFaire12 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreSavoirFaire12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreSavoirFaire12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels12 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote121.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote121 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote122.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote122 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote123.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote123 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote124.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote124 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote125.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote125 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHL15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHL15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace121.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace121 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHL09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHL09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace122.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace122 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHL07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHL07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace123.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace123 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHL15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHL15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreSavoirFaire13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreSavoirFaire13 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreSavoirFaire13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreSavoirFaire13 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau13 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels13 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote131.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote131 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote132.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote132 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote133.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote133 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote134.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote134 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote135.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote135 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHM15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHM15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace131.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace131 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHM09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHM09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace132.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace132 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent13 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHM07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHM07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace133.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace133 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation13 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHM15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHM15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NumeroPage7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreNumero3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreNumero3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelFamille3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelFamille3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreQualites14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreQualites14 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreQualites14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreQualites14 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau14 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels14 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote141.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote141 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote142.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote142 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote143.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote143 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote144.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote144 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote145.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote145 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHN15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHN15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace141.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace141 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHN09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHN09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace142.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace142 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent14 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHN07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHN07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace143.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace143 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation14 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHN15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHN15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur14 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreQualites15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreQualites15 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreQualites15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreQualites15 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau15 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels15 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote151.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote151 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote152.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote152 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote153.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote153 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote154.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote154 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote155.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote155 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHO15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHO15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace151.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace151 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHO09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHO09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace152.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace152 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent15 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHO07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHO07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace153.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace153 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation15 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHO15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHO15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur15 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreQualites16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreQualites16 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreQualites16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreQualites16 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau16 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels16 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote161.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote161 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote162.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote162 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote163.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote163 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote164.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote164 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote165.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote165 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHP15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHP15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace161.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace161 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHP09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHP09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace162.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace162 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent16 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHP07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHP07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace163.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace163 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation16 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHP15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHP15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur16 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreQualites17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreQualites17 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreQualites17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreQualites17 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau17 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels17 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote171.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote171 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote172.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote172 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote173.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote173 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote174.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote174 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote175.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote175 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHQ15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHQ15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace171.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace171 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHQ09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHQ09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace172.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace172 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent17 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHQ07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHQ07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace173.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace173 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation17 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHQ15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHQ15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur17 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreQualites18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreQualites18 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreQualites18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreQualites18 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau18 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels18 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote181.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote181 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote182.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote182 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote183.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote183 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote184.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote184 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote185.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote185 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHR15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHR15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace181.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace181 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHR09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHR09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace182.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace182 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent18 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHR07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHR07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace183.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace183 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation18 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHR15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHR15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur18 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreQualites19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreQualites19 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreQualites19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreQualites19 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau19 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels19 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote191.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote191 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote192.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote192 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote193.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote193 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote194.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote194 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote195.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote195 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHS15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHS15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace191.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace191 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHS09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHS09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace192.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace192 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent19 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHS07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHS07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace193.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace193 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation19 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHS15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHS15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur19 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreQualites20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreQualites20 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreQualites20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreQualites20 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau20 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels20 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote201.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote201 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote202.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote202 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote203.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote203 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote204.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote204 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote205.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote205 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHT15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHT15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreEspace201.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace201 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHT09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHT09 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace202.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace202 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent20 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHT07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHT07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace203.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace203 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation20 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHT15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHT15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NumeroPage8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreNumero4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreNumero4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelFamille4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelFamille4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSousFamille41.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSousFamille41 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEncadrement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEncadrement As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_EtiNbrAgentsEncadres.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_EtiNbrAgentsEncadres As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoH015009.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoH015009 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoVG03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoVG03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle III_CadreManagement21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreManagement21 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreManagement21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreManagement21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels21 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote211.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote211 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote212.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote212 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote213.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote213 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote214.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote214 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHV15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHV15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHV07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHV07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace213.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace213 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHV15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHV15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreManagement22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreManagement22 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreManagement22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreManagement22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels22 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote221.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote221 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote222.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote222 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote223.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote223 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote224.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote224 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHW15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHW15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHW07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHW07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace223.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace223 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHW15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHW15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreManagement23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreManagement23 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreManagement23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreManagement23 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau23 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels23 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote231.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote231 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote232.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote232 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote233.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote233 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote234.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote234 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHX15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHX15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent23 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHX07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHX07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace233.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace233 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation23 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHX15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHX15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur23 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreManagement24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreManagement24 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreManagement24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreManagement24 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau24 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels24 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote241.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote241 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote242.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote242 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote243.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote243 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote244.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote244 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHY15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHY15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent24 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHY07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHY07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace243.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace243 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation24 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHY15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHY15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur24 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreManagement25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreManagement25 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreManagement25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreManagement25 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau25 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels25 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote251.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote251 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote252.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote252 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote253.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote253 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote254.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote254 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHZ15301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHZ15301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent25 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioHZ07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioHZ07 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace253.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace253 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation25 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoHZ15311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoHZ15311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur25 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreManagement26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreManagement26 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreManagement26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreManagement26 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau26 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels26 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote261.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote261 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote262.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote262 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote263.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote263 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote264.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote264 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoH115301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoH115301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent26 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioH107.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioH107 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace263.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace263 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation26 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoH115311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoH115311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelSeparateur26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelSeparateur26 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreManagement27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreManagement27 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreManagement27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreManagement27 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveau27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveau27 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_CadreEnteteLabels27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_CadreEnteteLabels27 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle III_LabelTitreNote271.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote271 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote272.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote272 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote273.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote273 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNote274.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNote274 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoH215301.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoH215301 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreNiveauAgent27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreNiveauAgent27 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_RadioH207.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_RadioH207 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle III_LabelTitreEspace273.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreEspace273 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_LabelTitreAppreciation27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_LabelTitreAppreciation27 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle III_InfoH215311.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents III_InfoH215311 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NumeroPage9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage9 As Global.System.Web.UI.WebControls.Label
End Class
