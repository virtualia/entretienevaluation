﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P6_FAM
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim Chaine As String = ""
        Dim Chaine1 As String = ""
        Dim Chaine2 As String = ""
        Dim Chaine3 As String = ""

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        VI_LabelIdentite.Text = WsDossierPER.LibelleIdentite

        Chaine1 = WsDossierPER.NiveauAffectation(DatedEffet, 1)
        If WsDossierPER.NiveauAffectation(DatedEffet, 2) <> "" Then
            Chaine2 = " - " & WsDossierPER.NiveauAffectation(DatedEffet, 2)
        End If
        VI_LabelAffectation.Text = Chaine1 & Chaine2
        If WsDossierPER.NiveauAffectation(DatedEffet, 3) <> "" Then
            VI_LabelAffectationUnite.Text = WsDossierPER.NiveauAffectation(DatedEffet, 3)
        End If

        Call LireLaFiche()

        If WsDossierPER.Date_de_Signature_Manager <> "" Then
            VI_LabelSignatureSuperieur.Text = "Signature - Validé par " & WsDossierPER.Evaluateur(DatedEffet)
            VI_LabelSignatureSuperieur.Text &= " via son mot de passe Virtualia"
        End If
        If WsDossierPER.Date_de_Signature_Evalue <> "" Then
            VI_LabelSignatureAgent.Text = "Signature - Validé par " & WsDossierPER.Qualite & Strings.Space(1) & WsDossierPER.Nom & Strings.Space(1) & WsDossierPER.Prenom
            VI_LabelSignatureAgent.Text &= " via son mot de passe Virtualia"
        End If
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label

        Do
            Ctl = V_WebFonction.VirWebControle(Me.VI_CadreInfo, "VI_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 10, 3))
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1))
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.VI_CadreInfo, "VI_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1))
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VSixBoutonRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.VI_CadreInfo, "VI_RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VSixBoutonRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 10, 1))
            If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                Select Case ValeurLue(NumObjet, NumInfo, Rang)
                    Case Is = "4"
                        VirRadio.VRadioN1Check = True
                        VirRadio.VRadioN1Text = "excellent"
                    Case Is = "3"
                        VirRadio.VRadioN2Check = True
                        VirRadio.VRadioN2Text = "très bon"
                    Case Is = "2"
                        VirRadio.VRadioN3Check = True
                        VirRadio.VRadioN3Text = "satisfaisant"
                    Case Is = "1"
                        VirRadio.VRadioN4Check = True
                        VirRadio.VRadioN4Text = "à développer"
                    Case Else
                        VirRadio.VRadioN1Check = False
                        VirRadio.VRadioN2Check = False
                        VirRadio.VRadioN3Check = False
                        VirRadio.VRadioN4Check = False
                End Select
            End If
            VirRadio.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 156
                    If WsDossierPER.Objet_156(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_156(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class