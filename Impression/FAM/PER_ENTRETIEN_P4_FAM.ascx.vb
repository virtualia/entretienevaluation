﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P4_FAM
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim Chaine As String = ""
        Dim Chaine1 As String = ""
        Dim Chaine2 As String = ""
        Dim Chaine3 As String = ""

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        IV_LabelIdentite.Text = WsDossierPER.LibelleIdentite

        Chaine1 = WsDossierPER.NiveauAffectation(DatedEffet, 1)
        If WsDossierPER.NiveauAffectation(DatedEffet, 2) <> "" Then
            Chaine2 = " - " & WsDossierPER.NiveauAffectation(DatedEffet, 2)
        End If
        IV_LabelAffectation.Text = Chaine1 & Chaine2
        If WsDossierPER.NiveauAffectation(DatedEffet, 3) <> "" Then
            IV_LabelAffectationUnite.Text = WsDossierPER.NiveauAffectation(DatedEffet, 3)
        End If

        V_LibelListe = "Aucune formation" & VI.Tild & "Une formation" & VI.Tild & "formations"
        V_LibelColonne = "date" & VI.Tild & "formation suivie" & VI.Tild & "Clef"
        IV_ListeFormationsSuivies.V_Liste(V_LibelColonne, V_LibelListe) = WsDossierPER.ListeDesFormationsSuivies(WsDossierPER.Annee, 1)

        Call LireLaFiche()
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label

        Do
            Ctl = V_WebFonction.VirWebControle(Me.IV_CadreInfo, "IV_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 10, 3))
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1))
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.IV_CadreInfo, "IV_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1))
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VTrioVerticalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.IV_CadreInfo, "IV_RadioV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VTrioVerticalRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 10, 1))
            Select Case NumInfo
                Case 2
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "T1"
                            VirRadio.RadioGaucheCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "T2"
                            VirRadio.RadioCentreCheck = True
                            VirRadio.RadioGaucheText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "T3"
                            VirRadio.RadioDroiteCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioGaucheText = ""
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
                Case 6
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "Favorable"
                            VirRadio.RadioGaucheCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "Refusé"
                            VirRadio.RadioCentreCheck = True
                            VirRadio.RadioGaucheText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "Reporté"
                            VirRadio.RadioDroiteCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioGaucheText = ""
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
                Case 10
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "Agent"
                            VirRadio.RadioGaucheCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "Responsable"
                            VirRadio.RadioCentreCheck = True
                            VirRadio.RadioGaucheText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
                Case 11
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "Oui"
                            VirRadio.RadioGaucheCheck = True
                            VirRadio.RadioCentreText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Is = "Non"
                            VirRadio.RadioCentreCheck = True
                            VirRadio.RadioGaucheText = ""
                            VirRadio.RadioDroiteText = ""
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
            End Select
            VirRadio.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 155
                    If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_155(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class