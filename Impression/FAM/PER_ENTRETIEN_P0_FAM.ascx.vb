﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P0_FAM
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
            End If
        End Set
    End Property

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim Tableaudata(0) As String
        Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)
        CadreLogoFAM.BackImageUrl = Tableaudata(0) & "/Images/General/LogoFAM.jpg"
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim Chaine As String = ""
        Dim Chaine1 As String = ""
        Dim Chaine2 As String = ""
        Dim Chaine3 As String = ""

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If
        O_LabelAnneeEntretien.Text = "au titre de l'année " & WsDossierPER.Annee
        O_LabelIdentite.Text = WsDossierPER.LibelleIdentite
        O_LabelNaissance.Text = WsDossierPER.LibelleNaissance
        O_LabelStatut.Text = WsDossierPER.Statut(DatedEffet)
        O_LabelGrade.Text = WsDossierPER.Grade(DatedEffet)
        O_LabelEchelon.Text = WsDossierPER.Echelon(DatedEffet)
        O_LabelDateEchelon.Text = V_WebFonction.InfoExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, 678, WsDossierPER.V_Identifiant, DatedEffet)

        Chaine1 = WsDossierPER.NiveauAffectation(DatedEffet, 1)
        If WsDossierPER.NiveauAffectation(DatedEffet, 2) <> "" Then
            Chaine2 = " - " & WsDossierPER.NiveauAffectation(DatedEffet, 2)
        End If
        O_LabelAffectation.Text = Chaine1 & Chaine2
        If WsDossierPER.NiveauAffectation(DatedEffet, 3) <> "" Then
            O_LabelAffectationUnite.Text = WsDossierPER.NiveauAffectation(DatedEffet, 3)
        End If

        Chaine = WsDossierPER.FonctionExercee(DatedEffet, True)
        If Chaine <> "" Then
            O_LabelPoste.Text = WsDossierPER.FonctionExercee(DatedEffet, False)
            O_LabelDatePoste.Text = Chaine
            O_FonctionsExercees.Text = WsDossierPER.FonctionExercee(DatedEffet, False)
        End If

        O_CotationPoste.Text = ""
        O_QuotiteTravail.Text = WsDossierPER.Quotite_Activite(DatedEffet)
        '** ODEADOM
        If WsDossierPER.Etablissement(DatedEffet).Contains("ODEADOM") Then
            Dim Tableaudata(0) As String
            Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)
            CadreLogoFAM.BackImageUrl = Tableaudata(0) & "/Images/General/LogoOdeadom.jpg"
        End If
        '**
        Call LireLaFiche()
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim Rang As String

        Dim VirLabel As System.Web.UI.WebControls.Label
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreInfo, "O_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 8, 3))
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, "")
            IndiceI += 1
        Loop

        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreEmploi, "O_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 8, 3))
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, "")
            IndiceI += 1
        Loop

        IndiceI = 0
        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreEmploi, "O_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, "")
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        IndiceI = 0
        Dim VirRadio As VirtualiaControle_VTrioHorizontalRadio
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreEmploi, "O_RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirRadio = CType(Ctl, VirtualiaControle_VTrioHorizontalRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1))
            If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                Select Case ValeurLue(NumObjet, NumInfo, Rang)
                    Case Is = "Oui"
                        VirRadio.RadioCentreCheck = True
                        VirRadio.RadioGaucheText = ""
                    Case Else
                        VirRadio.RadioGaucheCheck = True
                        VirRadio.RadioCentreText = ""
                End Select
            End If
            VirRadio.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 157
                    If WsDossierPER.Objet_157(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_157(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class