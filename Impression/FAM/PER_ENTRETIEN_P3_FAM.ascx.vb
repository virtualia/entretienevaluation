﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P3_FAM
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        Call LireLaFiche()
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label

        Do
            Ctl = V_WebFonction.VirWebControle(Me.III_CadreInfo, "III_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            Rang = ""
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 11, 3))
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 10, 1))
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.III_CadreInfo, "III_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 10, 1))
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VSixBoutonRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.III_CadreInfo, "III_RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VSixBoutonRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 11, 1))
            If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                Select Case ValeurLue(NumObjet, NumInfo, Rang)
                    Case Is = "1"
                        VirRadio.VRadioN2Check = True
                        VirRadio.VRadioN2BorderColor = System.Drawing.Color.Black
                        VirRadio.VRadioN2BackColor = System.Drawing.Color.LightGray
                        VirRadio.VRadioN1Text = ""
                        VirRadio.VRadioN3Text = ""
                        VirRadio.VRadioN4Text = ""
                        VirRadio.VRadioN5Text = ""
                        VirRadio.VRadioN6Text = ""
                    Case Is = "2"
                        VirRadio.VRadioN3Check = True
                        VirRadio.VRadioN3BorderColor = System.Drawing.Color.Black
                        VirRadio.VRadioN3BackColor = System.Drawing.Color.LightGray
                        VirRadio.VRadioN1Text = ""
                        VirRadio.VRadioN2Text = ""
                        VirRadio.VRadioN4Text = ""
                        VirRadio.VRadioN5Text = ""
                        VirRadio.VRadioN6Text = ""
                    Case Is = "3"
                        VirRadio.VRadioN4Check = True
                        VirRadio.VRadioN4BorderColor = System.Drawing.Color.Black
                        VirRadio.VRadioN4BackColor = System.Drawing.Color.LightGray
                        VirRadio.VRadioN1Text = ""
                        VirRadio.VRadioN2Text = ""
                        VirRadio.VRadioN3Text = ""
                        VirRadio.VRadioN5Text = ""
                        VirRadio.VRadioN6Text = ""
                    Case Is = "4"
                        VirRadio.VRadioN5Check = True
                        VirRadio.VRadioN5BorderColor = System.Drawing.Color.Black
                        VirRadio.VRadioN5BackColor = System.Drawing.Color.LightGray
                        VirRadio.VRadioN1Text = ""
                        VirRadio.VRadioN2Text = ""
                        VirRadio.VRadioN3Text = ""
                        VirRadio.VRadioN4Text = ""
                        VirRadio.VRadioN6Text = ""
                    Case Is = "5"
                        VirRadio.VRadioN6Check = True
                        VirRadio.VRadioN6BorderColor = System.Drawing.Color.Black
                        VirRadio.VRadioN6BackColor = System.Drawing.Color.LightGray
                        VirRadio.VRadioN1Text = ""
                        VirRadio.VRadioN2Text = ""
                        VirRadio.VRadioN3Text = ""
                        VirRadio.VRadioN4Text = ""
                        VirRadio.VRadioN5Text = ""
                    Case Else
                        VirRadio.VRadioN1Check = True
                        VirRadio.VRadioN1BorderColor = System.Drawing.Color.Black
                        VirRadio.VRadioN1BackColor = System.Drawing.Color.LightGray
                        VirRadio.VRadioN2Text = ""
                        VirRadio.VRadioN3Text = ""
                        VirRadio.VRadioN4Text = ""
                        VirRadio.VRadioN5Text = ""
                        VirRadio.VRadioN6Text = ""
                End Select
            End If
            VirRadio.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    If WsDossierPER.Objet_150 IsNot Nothing Then
                        Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                    End If
                Case 153
                    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_153(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class