﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P4_ISMEP
    
    '''<summary>
    '''Contrôle IV_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_CadreEnteteDePage1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_CadreEnteteDePage1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_LabelEnteteLigne11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelEnteteLigne11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_LabelEnteteLigne12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelEnteteLigne12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_CadreTitreObjectifs.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_CadreTitreObjectifs As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_LabelVoletEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelVoletEntete As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelVolet As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_LabelVoletSuite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelVoletSuite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_LabelTitreObjectifs.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelTitreObjectifs As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_CadreNumero1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_LabelNumerobjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelNumerobjectif1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_TableObjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_TableObjectif1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_InfoVA02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_InfoVA02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle IV_LabelNumerobjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelNumerobjectif2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_TableObjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_TableObjectif2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_InfoVB02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_InfoVB02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle IV_LabelNumerobjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelNumerobjectif3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_TableObjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_TableObjectif3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_InfoVC02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_InfoVC02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle IV_LabelNumerobjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelNumerobjectif4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_TableObjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_TableObjectif4 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_InfoVD02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_InfoVD02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle IV_LabelNumerobjectif5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelNumerobjectif5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_TableObjectif5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_TableObjectif5 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_InfoVE02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_InfoVE02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle IV_LabelNumerobjectif6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelNumerobjectif6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_TableObjectif6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_TableObjectif6 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_InfoVF02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_InfoVF02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle IV_LabelNumerobjectif7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelNumerobjectif7 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_TableObjectif7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_TableObjectif7 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_InfoVG02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_InfoVG02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle NumeroPage7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage7 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_CadreEnteteDePage2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_CadreEnteteDePage2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_LabelEnteteLigne21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelEnteteLigne21 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_LabelEnteteLigne22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelEnteteLigne22 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_CadreNumero2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_LabeldemarcheAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabeldemarcheAgent As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_InfoVM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_InfoVM03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle IV_CadreTitrePerspectives.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_CadreTitrePerspectives As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_LabelVoletEntetePerspectives.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelVoletEntetePerspectives As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_LabelVoletPerspectives.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelVoletPerspectives As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_LabelVoletSuitePerspectives.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelVoletSuitePerspectives As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_LabelTitrePerspectives.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelTitrePerspectives As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_CadreNumero3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_CadreNumero3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle IV_InfoVN03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_InfoVN03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle IV_LabelTitreNumero32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_LabelTitreNumero32 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle IV_InfoVO03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IV_InfoVO03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle NumeroPage8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage8 As Global.System.Web.UI.WebControls.Label
End Class
