﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P5_ISMEP.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P5_ISMEP" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="V_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="V_CadreEnteteDePage1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelEnteteLigne11" runat="server" Height="20px" Width="748px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelEnteteLigne12" runat="server" Height="20px" Width="748px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreTitreSignatureSuperieur" runat="server" Height="30px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletEnteteSignatureSuperieur" runat="server" Height="20px" Width="30px" Text="7 - " 
                            CssClass="EP_Imp_LargerBold" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletSignatureSuperieur" runat="server" Height="20px" Width="556px" 
                            Text="Signature du supérieur hiérarchique direct" 
                            CssClass="EP_Imp_LargerBoldSouligne" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletSuiteSignatureSuperieur" runat="server" Height="20px" Width="160px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="8px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreSignatureSuperieur" runat="server" Height="30px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_EtiLabelDateEntretien" runat="server" Height="20px" Width="350px"
                            Text="Date de l'entretien : "
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_InfoH15000" runat="server" Height="20px" Width="396px" Text=""
                            CssClass="EP_Imp_SmallNormal">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_EtiLabelDateTransmissionFiller" runat="server" Height="20px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallNormal">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_EtiLabelDateTransmission" runat="server" Height="20px" Width="398px"
                            Text="Date de transmission du compte-rendu :"
                            CssClass="EP_Imp_SmallNormal">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_EtiLabelManager" runat="server" Height="20px" Width="350px"
                            Text="Nom, qualité et signature du responsable hiérarchique : "
                            CssClass="EP_Imp_SmallNormal" style="margin-bottom: 40px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_InfoH15001" runat="server" Height="20px" Width="398px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="margin-bottom: 40px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreTitreAppreciation" runat="server" Height="30px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletEntete" runat="server" Height="20px" Width="30px" Text="8 - " 
                            CssClass="EP_Imp_LargerBold" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVolet" runat="server" Height="20px" Width="666px"
                            Text="Observations de l'agent sur son évaluation" 
                            CssClass="EP_Imp_LargerBoldSouligne" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletSuite" runat="server" Height="20px" Width="50px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Label ID="V_LabelTitreCommentaires" runat="server" Height="20px" Width="746px"
                           Text="(dans un délai d'une semaine à compter de la date de transmission du compte rendu)"
                           CssClass="EP_Imp_SmallBold" style="margin-bottom: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreNumero1" runat="server" Height="20px" Width="748px"
                           Text="Sur l'entretien :"
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 3px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVP03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="744px" DonHeight="200px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 1px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreNumero2" runat="server" Height="20px" Width="748px"
                           Text="Sur les perspectives de carrière et de mobilité :"
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 3px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVQ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="744px" DonHeight="200px" EtiHeight="20px" DonTabIndex="2" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 1px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="8px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreTitreSignatureAutorite" runat="server" Height="30px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletEnteteSignatureAutorite" runat="server" Height="20px" Width="30px" Text="9 - " 
                            CssClass="EP_Imp_LargerBold" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletSignatureAutorite" runat="server" Height="20px" Width="556px" 
                            Text="Signature de l'autorité hiérarchique" 
                            CssClass="EP_Imp_LargerBoldSouligne" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletSuiteSignatureAutorite" runat="server" Height="20px" Width="160px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="1px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreSignatureAutorite" runat="server" Height="30px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelEnteteSignatureAutorite" runat="server" Height="20px" Width="350px" Text="Date :"
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_EtiLabelAutorite" runat="server" Height="20px" Width="350px"
                            Text="Nom, qualité et signature de l'autorité hiérarchique : "
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="40px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage9" runat="server" Height="15px" Width="250px"
                BackColor="Transparent" BorderStyle="None"
                Text="9 / 13" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:right" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell Height="15px">
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="V_CadreEnteteDePage2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelEnteteLigne21" runat="server" Height="20px" Width="748px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelEnteteLigne22" runat="server" Height="20px" Width="748px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreTitreSignatureAgent" runat="server" Height="30px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletEnteteSignatureAgent" runat="server" Height="20px" Width="45px" Text="10 - " 
                            CssClass="EP_Imp_LargerBold" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletSignatureAgent" runat="server" Height="20px" Width="666px"
                            Text="Signature de l'agent" 
                            CssClass="EP_Imp_LargerBoldSouligne" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletSuiteSignatureAgent" runat="server" Height="20px" Width="35px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="8px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreSignatureAgent" runat="server" Height="30px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelEnteteSignatureAgent" runat="server" Height="20px" Width="350px" Text="Date :"
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_InfoH15004" runat="server" Height="20px" Width="358px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="V_EtiLabelSignatureAgent" runat="server" Height="20px" Width="350px"
                            Text="Signature : "
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="V_EtiCommentaireAgent" runat="server" Height="20px" Width="700px"
                            Text="La date et la signature ont pour seul objet de témoigner de la tenue de l'entretien"
                            CssClass="EP_Imp_SmallItalic" Font-Size="80%" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>              
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreModalitesRecours" runat="server" Height="30px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreModalites" runat="server" Height="20px" Width="748px"
                            Text="Modalités de recours :"
                            CssClass="EP_Imp_SmallBold" style="margin-left: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreRecoursSpecifique" runat="server" Height="22px" Width="748px"
                            Text="- recours spécifique (Article 6 du décret n°2010-888 du 28 juillet 2010) :"
                            CssClass="EP_Imp_SmallBold" Font-Size="80%" style="margin-left: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTexteRecoursSpecifique1" runat="server" Height="40px" Width="748px"
                            Text="L'agent peut saisir l'autorité hiérarchique d'une demande de révision de son compte rendu d'entretien professionnel.
			                Ce recours hiérarchique doit être exercé dans le délai de 15 jours francs suivant la notification
                            du compte rendu d'entretien professionnel."
                            CssClass="EP_Imp_SmallNormal" Font-Size="80%" style="margin-left: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTexteRecoursSpecifique2" runat="server" Height="40px" Width="748px"
                            Text="La réponse de l'autorité hiérarchique doit être notifiée dans un délai de 15 jours francs
                            à compter de la date de réception de la demande de révision du compte rendu de l'entretien professionnel."
                            CssClass="EP_Imp_SmallNormal" Font-Size="80%" style="margin-left: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTexteRecoursSpecifique3" runat="server" Height="40px" Width="748px"
                            Text="A compter de la date de la notification de cette réponse, l'agent peut saisir la commission administrative paritaire
			                dans un délai d'un mois. Le recours hiérarchique est le préalable obligatoire à la saisine de la CAP."
                            CssClass="EP_Imp_SmallNormal" Font-Size="80%" style="margin-left: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreRecoursDroitCommun" runat="server" Height="20px" Width="748px"
                            Text="- recours de droit commun :"
                            CssClass="EP_Imp_SmallBold" Font-Size="80%" style="margin-left: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTexteRecoursDroitCommun1" runat="server" Height="70px" Width="748px"
                            Text="L'agent qui souhaite contester son compte rendu d'entretien professionnel peut exercer un recours de droit commun
			                devant le juge administratif dans les 2 mois suivant la notification du compte rendu de l'entretien professionnel,
			                sans exercer de recours gracieux ou hiérarchique (et sans saisir la CAP) ou après avoir exercé un recours administratif
			                de droit commun (gracieux ou hiérarchique)."
                            CssClass="EP_Imp_SmallNormal" Font-Size="80%" style="margin-left: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTexteRecoursDroitCommun2" runat="server" Height="60px" Width="748px"
                            Text="Il peut enfin saisir le juge administratif à l'issue de la procédure spécifique définie par l'article 6 précité.
			                Le délai de recours contentieux, suspendu durant cette procédure, repart à compter de la notification de la décision finale de l'administration
			                faisant suite à l'avis rendu par la CAP."
                            CssClass="EP_Imp_SmallNormal" Font-Size="80%" style="margin-left: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>               
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage10" runat="server" Height="15px" Width="250px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="10 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:right" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>