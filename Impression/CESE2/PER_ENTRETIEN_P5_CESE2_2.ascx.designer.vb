﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P5_CESE2_2
    
    '''<summary>
    '''Contrôle V_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_CadreTitreAppreciation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreTitreAppreciation As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVolet As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreNumero2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelTitreNumero21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero21 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelTitreNumero22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero22 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelTitreNumero23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero23 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelTitreNumero24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero24 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_InfoHA15601.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHA15601 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CocheA04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CocheA04 As Global.Virtualia.Net.VirtualiaControle_VCocheSimple
    
    '''<summary>
    '''Contrôle V_LabelTitreNumero26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero26 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_InfoVM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVM03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle V_CadreNumero3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreNumero3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelTitreNumero31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero31 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_InfoHB15601.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHB15601 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CocheB04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CocheB04 As Global.Virtualia.Net.VirtualiaControle_VCocheSimple
    
    '''<summary>
    '''Contrôle V_InfoHM15601.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHM15601 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CocheM04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CocheM04 As Global.Virtualia.Net.VirtualiaControle_VCocheSimple
    
    '''<summary>
    '''Contrôle V_InfoHC15601.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHC15601 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CocheC04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CocheC04 As Global.Virtualia.Net.VirtualiaControle_VCocheSimple
    
    '''<summary>
    '''Contrôle V_InfoHD15601.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHD15601 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CocheD04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CocheD04 As Global.Virtualia.Net.VirtualiaControle_VCocheSimple
    
    '''<summary>
    '''Contrôle V_InfoHE15601.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHE15601 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CocheE04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CocheE04 As Global.Virtualia.Net.VirtualiaControle_VCocheSimple
    
    '''<summary>
    '''Contrôle V_CadreNumero4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreNumero4 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelTitreNumero41.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero41 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_InfoHF15601.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHF15601 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CocheF04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CocheF04 As Global.Virtualia.Net.VirtualiaControle_VCocheSimple
    
    '''<summary>
    '''Contrôle V_InfoHG15601.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHG15601 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CocheG04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CocheG04 As Global.Virtualia.Net.VirtualiaControle_VCocheSimple
    
    '''<summary>
    '''Contrôle V_InfoHH15601.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHH15601 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CocheH04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CocheH04 As Global.Virtualia.Net.VirtualiaControle_VCocheSimple
    
    '''<summary>
    '''Contrôle V_LabelDatePromoGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelDatePromoGrade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelModeAccesGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelModeAccesGrade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellSautPage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSautPage As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle NumeroPage9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage9 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellPageBreak.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellPageBreak As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle V_CadreNumero5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreNumero5 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelTitreNumero51.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero51 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_InfoHI15601.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHI15601 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CocheI04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CocheI04 As Global.Virtualia.Net.VirtualiaControle_VCocheSimple
    
    '''<summary>
    '''Contrôle V_InfoHJ15601.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHJ15601 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CocheJ04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CocheJ04 As Global.Virtualia.Net.VirtualiaControle_VCocheSimple
    
    '''<summary>
    '''Contrôle V_InfoHK15601.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHK15601 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CocheK04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CocheK04 As Global.Virtualia.Net.VirtualiaControle_VCocheSimple
    
    '''<summary>
    '''Contrôle V_LabelDatePromoCorps.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelDatePromoCorps As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelModeAccesCorps.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelModeAccesCorps As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CadreNumero6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreNumero6 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelTitrePerspectiveSalariale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitrePerspectiveSalariale As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelTitreDateRevalo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreDateRevalo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_InfoHZ15403.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHZ15403 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelTitreMontantRevalo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreMontantRevalo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_InfoHD15403.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoHD15403 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelTitrePerspective.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitrePerspective As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_InfoVE03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVE03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle NumeroPage10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage10 As Global.System.Web.UI.WebControls.Label
End Class
