﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P0_CESE2
    
    '''<summary>
    '''Contrôle PAGE_0.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PAGE_0 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_CadreTitreEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreTitreEntretien As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_CadreLogoCESE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreLogoCESE As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_EtiquetteEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiquetteEntete As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_Etiquette As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiquetteSuite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiquetteSuite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelAnneeEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelAnneeEntretien As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_CadreEnteteEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreEnteteEntretien As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_EtiLabelIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelIdentite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelIdentite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelEntreeCESE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelEntreeCESE As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelEntreeCESE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelEntreeCESE As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelCorps.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelCorps As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelCorps.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelCorps As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelGrade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelGrade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelEchelon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelEchelon As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelEchelon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelEchelon As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelTempsTravail.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelTempsTravail As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelTempsTravail.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTempsTravail As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelAffectation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelAffectation As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelAffectationNiveau1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelAffectationNiveau1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelAffectationSuite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelAffectationSuite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelAffectationNiveau2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelAffectationNiveau2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelTitreFonctions.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreFonctions As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelTitreFonctionsSuite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreFonctionsSuite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_CadreFonctions.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreFonctions As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_LabelTitreIntituleFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreIntituleFonction As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoH15005.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15005 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelTitreDateFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreDateFonction As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoHA15403.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoHA15403 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelCommentaire1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelCommentaire1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelCommentaire2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelCommentaire2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoV16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoV16 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle O_CadreEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreEntretien As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_LabelTitreEntretienPro.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreEntretienPro As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelDateEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelDateEntretien As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoH15000.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15000 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelTitreAvec.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreAvec As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelManager As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoH15001.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15001 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelFonctionManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelFonctionManager As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoVC03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoVC03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle O_LabelTitreMotif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreMotif As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoVB03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoVB03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle NumeroPage1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage1 As Global.System.Web.UI.WebControls.Label
End Class
