﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P3_CESE2
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim Si_Titulaire As Boolean = WsDossierPER.SiTitulaire("")
        If Not Si_Titulaire Then
            NumeroPage5.Text = "5 / 12"
            NumeroPage6.Text = "6 / 12"
        End If

        Call LireLaFiche()

    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.III_CadreInfo, "III_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 10, 1), "CESE2")
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VSixBoutonRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.III_CadreInfo, "III_RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VSixBoutonRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 11, 1), "CESE2")
            If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                Select Case ValeurLue(NumObjet, NumInfo, Rang)
                    Case Is = "4"
                        VirRadio.VRadioN1Check = True
                        VirRadio.VRadioN1BackColor = System.Drawing.Color.LightGray
                    Case Is = "3"
                        VirRadio.VRadioN2Check = True
                        VirRadio.VRadioN2BackColor = System.Drawing.Color.LightGray
                    Case Is = "2"
                        VirRadio.VRadioN3Check = True
                        VirRadio.VRadioN3BackColor = System.Drawing.Color.LightGray
                    Case Is = "1"
                        VirRadio.VRadioN4Check = True
                        VirRadio.VRadioN4BackColor = System.Drawing.Color.LightGray
                    Case Is = "0"
                        VirRadio.VRadioN5Check = True
                        VirRadio.VRadioN5BackColor = System.Drawing.Color.LightGray
                    Case Else
                        VirRadio.VRadioN5Check = True
                        VirRadio.VRadioN5BackColor = System.Drawing.Color.LightGray
                End Select
            End If
            VirRadio.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 153
                    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_153(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class