﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P5_CESE2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P5_CESE2" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="V_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreTitreAppreciation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelVolet" runat="server" Text="EVOLUTION PROFESSIONNELLE DE L'AGENT" Height="20px" Width="748px"
                            CssClass="EP_Imp_MediumBold">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero11" runat="server" Height="34px" Width="748px"
                           Text="1. Proposition d'attribution d'une bonification d'ancienneté"
                           CssClass="EP_Imp_MediumNormal"
                           BackColor="#E7E7E7" style="padding-top: 6px; text-indent: 20px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero12" runat="server" Height="18px" Width="748px"
                           Text="Pour rappel :"
                           CssClass="EP_Imp_SmallBold"
                           style="text-indent: 5px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero13" runat="server" Height="110px" Width="748px"
                           Text="Tout agent ayant obtenu une promotion par examen professionnel ou au choix ne peut obtenir
                           une réduction d'ancienneté au titre de la même année. Les agents ayant obtenu une réduction de 3 mois
                           en année N ne peuvent obtenir en N+1 une réduction identique. L'absence de réduction d'ancienneté pendant 
                           plusieurs années consécutives doit être justifiée par l'administration et par le supérieur hiérarchique 
                           auprès de son collaborateur, notamment si les appréciations dans le cadre des entretiens d'évaluations 
                           ne font pas apparaître de difficulté particulière."
                           CssClass="EP_Imp_SmallItalic"
                           Font-Size="80%">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero14" runat="server" Height="45px" Width="748px"
                           Text="Concernant les administrateurs, conformément aux dispositions réglementaires, 
                           aucune réduction d'ancienneté ne peut être attribuée (article 10 du décret du 16 novembre 1999 et 
                           articles 7 à 11 du décret n° 2010-888 du 28 juillet 2010)."
                           CssClass="EP_Imp_SmallItalic"
                           Font-Size="80%">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero15" runat="server" Height="20px" Width="748px"
                           Text="Evolution conduisant à un avancement normal"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 1px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVJ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="744px" DonHeight="60px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero16" runat="server" Height="20px" Width="748px"
                           Text="Evolution conduisant à un avancement accéléré"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero17" runat="server" Height="40px" Width="740px"
                           Text="L'évaluateur peut indiquer l'amplitude de la réduction d'ancienneté souhaitée, 
                           soit un mois, soit trois mois (sous réserve de l'harmonisation)"
                           CssClass="EP_Imp_SmallItalic"
                           Font-Size="80%"
                           style="margin-left: 8px; padding-top: 4px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVK03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="744px" DonHeight="60px" EtiHeight="20px" DonTabIndex="2" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero18" runat="server" Height="20px" Width="748px"
                           Text="Dernières réductions d'ancienneté et promotions obtenues"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="V_CadreReductionsAnciennete" runat="server" CellPadding="0" CellSpacing="0" Width="600px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="V_LabelCadrageReducAnneeN3" runat="server" Height="20px" Width="100px" Text=""
                                       CssClass="EP_Imp_SmallBold"
                                       style="text-align: right">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="V_LabelEtiReducAnneeN3" runat="server" Height="20px" Width="90px" Text="Année N-3 : "
                                       CssClass="EP_Imp_SmallBold"
                                       style="text-align: center">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="V_LabelReducAnneeN3" runat="server" Height="20px" Width="100px" Text=""
                                       CssClass="EP_Imp_SmallBold">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="V_LabelCadrageReducAnneeN2" runat="server" Height="20px" Width="100px" Text=""
                                       CssClass="EP_Imp_SmallBold"
                                       style="text-align: right">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="V_LabelEtiReducAnneeN2" runat="server" Height="20px" Width="90px" Text="Année N-2 : "
                                       CssClass="EP_Imp_SmallBold"
                                       style="text-align: center">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="V_LabelReducAnneeN2" runat="server" Height="20px" Width="100px" Text=""
                                       CssClass="EP_Imp_SmallBold">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="V_LabelCadrageReducAnneeN1" runat="server" Height="20px" Width="100px" Text=""
                                       CssClass="EP_Imp_SmallBold"
                                       style="text-align: right">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="V_LabelEtiReducAnneeN1" runat="server" Height="20px" Width="90px" Text="Année N-1 : "
                                       CssClass="EP_Imp_SmallBold"
                                       style="text-align: center">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="V_LabelReducAnneeN1" runat="server" Height="20px" Width="100px" Text=""
                                       CssClass="EP_Imp_SmallBold">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCocheSimple ID="V_CocheL04" runat="server"
                                       V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                                       V_Height="25px" V_Width="300px"
                                       V_Text="  Sans objet pour les Administrateurs" V_SiModeCaractere="true"
                                       V_BorderStyle="None" V_BackColor="Transparent"
                                       V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>               
                                </asp:TableCell>
                                <asp:TableCell Width="100px" HorizontalAlign="Center"></asp:TableCell>
                                <asp:TableCell Width="100px" HorizontalAlign="Center"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell>  
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero21" runat="server" Height="34px" Width="748px"
                           Text="2. Proposition d'attribution d'une augmentation de la part variable de la prime de rendement"
                           CssClass="EP_Imp_MediumNormal"
                           BackColor="#E7E7E7" style="padding-top: 6px; text-indent: 20px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero22" runat="server" Height="18px" Width="748px"
                           Text="Pour rappel :"
                           CssClass="EP_Imp_SmallBold"
                           style="text-indent: 5px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero23" runat="server" Height="75px" Width="748px"
                           Text="«le montant individualisé de la part variable lié à l'engagement professionnel de l'agent et à l'atteinte 
			               des objectifs annuels est déterminée au regard notamment du compte-rendu de l'entretien d'évaluation.
			               Par rapport à l'année N-1, la part variable ne peut diminuer ou augmenter de plus de 10 % sauf en cas de changement de corps et / ou de fonction.»"
                           CssClass="EP_Imp_SmallItalic"
                           Font-Size="80%">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero24" runat="server" Height="20px" Width="748px"
                           Text="(Référentiel de gestion des ressources humaines, arrêté n°14-06 du 5 février 2014)"
                           CssClass="EP_Imp_SmallItalic"
                           Font-Size="80%" style="text-indent: 5px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHA15601" runat="server" Height="20px" Width="368px"
                           Text="Proposition d'une augmentation de la part variable"
                           CssClass="EP_Imp_SmallNormal"
                           BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="V_CocheA04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>     
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero26" runat="server" Height="40px" Width="370px"
                           Text="Dernières augmentations de la part variable obtenues"
                           CssClass="EP_Imp_SmallBold"
                           style="padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVM03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="361px" DonHeight="40px" EtiHeight="20px" DonTabIndex="4" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell ID="CellSautPage" Height="25px" HorizontalAlign="Center" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage9" runat="server" Height="15px" Width="748px"
                BackColor="Transparent" BorderStyle="None"
                Text="9 / 13" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:right">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell ID="CellPageBreak">
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="V_LabelTitreNumero31" runat="server" Height="34px" Width="748px"
                           Text="3. Un changement d’affectation est-il :"
                           CssClass="EP_Imp_MediumNormal"
                           BackColor="#E7E7E7" style="padding-top: 6px; text-indent: 20px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHB15601" runat="server" Height="20px" Width="368px"
                           Text="Sans objet pour le moment"
                           CssClass="EP_Imp_SmallNormal"
                           BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="V_CocheB04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHM15601" runat="server" Height="20px" Width="368px"
                           Text="Possible dans l'année ou à court terme"
                           CssClass="EP_Imp_SmallNormal"
                           BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="V_CocheM04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHC15601" runat="server" Height="20px" Width="368px"
                           Text="Souhaitable à moyen terme"
                           CssClass="EP_Imp_SmallNormal"
                           BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="V_CocheC04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHD15601" runat="server" Height="20px" Width="368px"
                           Text="A envisager"
                           CssClass="EP_Imp_SmallNormal"
                           BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="V_CocheD04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHE15601" runat="server" Height="20px" Width="368px"
                           Text="A été réalisé dans l'année"
                           CssClass="EP_Imp_SmallNormal"
                           BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="V_CocheE04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="V_LabelTitreNumero41" runat="server" Height="34px" Width="748px"
                           Text="4. Une promotion de grade au choix, sous réserve des conditions statutaires, est-elle :"
                           CssClass="EP_Imp_MediumNormal"
                           BackColor="#E7E7E7" style="padding-top: 6px; text-indent: 20px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHF15601" runat="server" Height="20px" Width="368px"
                           Text="Pas encore à l'ordre du jour"
                           CssClass="EP_Imp_SmallNormal"
                           BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="V_CocheF04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHG15601" runat="server" Height="20px" Width="368px"
                           Text="Envisageable à moyen terme"
                           CssClass="EP_Imp_SmallNormal"
                           BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="V_CocheG04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHH15601" runat="server" Height="20px" Width="368px"
                           Text="Recommandée fortement par l'évaluateur"
                           CssClass="EP_Imp_SmallNormal"
                           BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="V_CocheH04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="V_LabelDatePromoGrade" runat="server" Height="20px" Width="748px"
                           Text="DERNIERE PROMOTION DE CORPS le :"
                           CssClass="EP_Imp_SmallNormal"
                           style="padding-top: 2px; text-indent: 5px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="V_LabelModeAccesGrade" runat="server" Height="20px" Width="748px"
                           Text="Mode d'accès :"
                           CssClass="EP_Imp_SmallNormal"
                           style="padding-top: 2px; text-indent: 5px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="V_LabelTitreNumero51" runat="server" Height="34px" Width="748px"
                           Text="5. Une promotion de corps au choix, sous réserve des conditions statutaires, est-elle :"
                           CssClass="EP_Imp_MediumNormal"
                           BackColor="#E7E7E7" style="padding-top: 6px; text-indent: 20px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHI15601" runat="server" Height="20px" Width="368px"
                           Text="Pas encore à l'ordre du jour"
                           CssClass="EP_Imp_SmallNormal"
                           BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="V_CocheI04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHJ15601" runat="server" Height="20px" Width="368px"
                           Text="Envisageable à moyen terme"
                           CssClass="EP_Imp_SmallNormal"
                           BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="V_CocheJ04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHK15601" runat="server" Height="20px" Width="368px"
                           Text="Recommandée fortement par l'évaluateur"
                           CssClass="EP_Imp_SmallNormal"
                           BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="V_CocheK04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 2px; text-align: left"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="V_LabelDatePromoCorps" runat="server" Height="20px" Width="748px"
                           Text="DERNIERE PROMOTION DE CORPS le :"
                           CssClass="EP_Imp_SmallNormal"
                           style="padding-top: 2px; text-indent: 5px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="V_LabelModeAccesCorps" runat="server" Height="20px" Width="748px"
                           Text="Mode d'accès :"
                           CssClass="EP_Imp_SmallNormal"
                           Style="padding-top: 2px; text-indent: 5px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero6" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitrePerspectiveSalariale" runat="server" Height="34px" Width="748px"
                           Text="PERSPECTIVE SALARIALE"
                           CssClass="EP_Imp_MediumNormal"
                           BackColor="#E7E7E7" style="padding-top: 6px; text-align: center">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreDateRevalo" runat="server" Height="20px" Width="300px"
                            Text="Date de la dernière revalorisation salariale :"
                            CssClass="EP_Imp_SmallNormal"
                            style="text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_InfoHZ15403" runat="server" Height="20px" Width="440px" Text=""
                            CssClass="EP_Imp_SmallNormal">
                        </asp:Label>     
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreMontantRevalo" runat="server" Height="20px" Width="300px"
                            Text="Montant de la dernière revalorisation salariale :"
                            CssClass="EP_Imp_SmallNormal"
                            style="text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_InfoHD15403" runat="server" Height="20px" Width="440px" Text=""
                            CssClass="EP_Imp_SmallNormal">
                            </asp:Label>     
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitrePerspective" runat="server" Height="30px" Width="748px"
                           Text="Perspective pour l'entretien tri-annuel de revalorisation (année) en cohérence avec la durée du contrat de l'agent "
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVE03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="200px" EtiHeight="20px" DonTabIndex="17" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Center" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage10" runat="server" Height="15px" Width="748px"
                BackColor="Transparent" BorderStyle="None"
                Text="10 / 13" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:right; text-indent: 0px">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>