﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P0_CESE2
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
            End If
        End Set
    End Property

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim Tableaudata(0) As String
        Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)

        O_CadreLogoCESE.BackImageUrl = Tableaudata(0) & "/Images/General/Logo_CESE.jpg"
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim IndiceI As Integer
        Dim IndiceK As Integer
        Dim Chaine As String
        Dim Chaine1 As String = ""
        Dim Chaine2 As String = ""

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If
        Dim Si_Titulaire As Boolean = WsDossierPER.SiTitulaire(DatedEffet)

        O_LabelAnneeEntretien.Text = "ANNEE " & WsDossierPER.Annee
        O_LabelIdentite.Text = WsDossierPER.Nom & " " & WsDossierPER.Prenom
        O_LabelEntreeCESE.Text = WsDossierPER.Date_Entree_Etablissement(DatedEffet)

        If Si_Titulaire Then
            Chaine = V_WebFonction.InfoExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, 574, WsDossierPER.V_Identifiant, DatedEffet) 'Date entrée dans le corps
            O_LabelCorps.Text = WsDossierPER.Statut(DatedEffet) & " - " & WsDossierPER.Corps(DatedEffet) & Space(2) & "depuis le" & Space(1) & Chaine
            Chaine = ""
            Chaine = V_WebFonction.InfoExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, 553, WsDossierPER.V_Identifiant, DatedEffet) 'Date entrée dans le grade
            O_LabelGrade.Text = WsDossierPER.Grade(DatedEffet) & Space(2) & "depuis le" & Space(1) & Chaine
        Else
            O_EtiLabelCorps.Text = ""
            O_LabelCorps.Text = ""
            O_LabelGrade.Text = WsDossierPER.Grade(DatedEffet)
            O_Etiquette.Text = "FICHE D'ENTRETIEN PROFESSIONNEL DES AGENTS NON TITULAIRES"
            NumeroPage1.Text = "1 / 12"
        End If

        O_LabelEchelon.Text = WsDossierPER.Echelon(DatedEffet)
        O_LabelTempsTravail.Text = WsDossierPER.Quotite_Activite(DatedEffet)

        IndiceK = 0
        Chaine = ""
        For IndiceI = 4 To 1 Step -1
            Chaine = WsDossierPER.NiveauAffectation(DatedEffet, IndiceI)
            If Chaine <> "" Then
                IndiceK += 1
                Select Case IndiceK
                    Case 1
                        Chaine2 = Chaine
                    Case 2
                        Chaine1 = Chaine
                        Exit For
                End Select
            End If
        Next IndiceI
        If Chaine1 = "" Then
            O_LabelAffectationNiveau1.Text = Chaine2
            O_LabelAffectationNiveau2.Text = ""
        Else
            O_LabelAffectationNiveau1.Text = Chaine1
            O_LabelAffectationNiveau2.Text = Chaine2
        End If

        Chaine = WsDossierPER.FonctionExercee(DatedEffet, True)

        Call LireLaFiche()

    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label

        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreInfo, "O_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            Rang = ""
            If Strings.Mid(Ctl.ID, 8, 1) = "A" Then
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1), "CESE2")
                NumObjet = CShort(Strings.Mid(Ctl.ID, 9, 3))
            Else
                NumObjet = CShort(Strings.Mid(Ctl.ID, 8, 3))
            End If
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreInfo, "O_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1), "CESE2")
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property

End Class