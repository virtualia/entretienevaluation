﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P5_CESE2
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Dim Chaine As String
        Dim DatedEffet As String

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        Dim Si_Titulaire As Boolean = WsDossierPER.SiTitulaire(DatedEffet)
        Chaine = ""
        If Si_Titulaire Then
            Chaine = V_WebFonction.InfoExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, 574, WsDossierPER.V_Identifiant, DatedEffet) 'Date entrée dans le corps
            V_LabelDatePromoCorps.Text = "DERNIERE PROMOTION DE CORPS le :  " & Chaine & " (" & WsDossierPER.Corps(Chaine) & ")"
            V_LabelModeAccesCorps.Text = WsDossierPER.ModedAcces(Chaine)
            Chaine = ""
            Chaine = V_WebFonction.InfoExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, 553, WsDossierPER.V_Identifiant, DatedEffet) 'Date entrée dans le grade
            V_LabelDatePromoGrade.Text = "DERNIERE PROMOTION DE GRADE le :  " & Chaine & " (" & WsDossierPER.Grade(Chaine) & ")"
            V_LabelModeAccesGrade.Text = WsDossierPER.ModedAcces(Chaine)
            V_CadreNumero6.Visible = False
            ''**** AKR
            'If CDate(DatedEffet) >= CDate("01/01/2018") Then
            '    V_CadreNumero1.Visible = False
            '    V_LabelTitreNumero21.Text = "1. Proposition d'attribution d'une augmentation de la part variable de la prime de rendement"
            '    V_LabelTitreNumero31.Text = "2. Un changement d’affectation est-il :"
            '    V_LabelTitreNumero41.Text = "3. Une promotion de grade au choix, sous réserve des conditions statutaires, est-elle :"
            '    V_LabelTitreNumero51.Text = "4. Une promotion de corps au choix, sous réserve des conditions statutaires, est-elle :"
            'End If

            ''*** FIN AKR


        Else
            V_LabelDatePromoCorps.Text = ""
            V_LabelDatePromoGrade.Text = ""
            V_LabelModeAccesCorps.Text = ""
            V_LabelModeAccesGrade.Text = ""
            V_CadreNumero6.Visible = True
            V_CadreNumero2.Visible = False
            V_CadreNumero3.Visible = False
            V_CadreNumero4.Visible = False
            V_CadreNumero5.Visible = False
            CellSautPage.Visible = False
            CellPageBreak.Visible = False
            NumeroPage10.Text = "9 / 12"
            NumeroPage9.Text = ""
        End If

        Call LireLaFiche()

    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label

        Do
            Ctl = V_WebFonction.VirWebControle(Me.V_CadreInfo, "V_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 9, 3))
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1), "CESE2")
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.V_CadreInfo, "V_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1), "CESE2")
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirCoche As VirtualiaControle_VCocheSimple
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.V_CadreInfo, "V_Coche", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirCoche = CType(Ctl, VirtualiaControle_VCocheSimple)
            NumObjet = VirCoche.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1), "CESE2")
            VirCoche.V_SiEnLectureSeule = True
            If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                Select Case ValeurLue(NumObjet, NumInfo, Rang)
                    Case Is = "Oui"
                        VirCoche.V_Check = True
                    Case Is = "Non"
                        VirCoche.V_Check = False
                    Case Else
                        VirCoche.V_Check = False
                End Select
            End If
            IndiceI += 1
        Loop

        ' A FAIRE
        'Test d'encadrement si case cochée
        'Select Case V_CocheB04.V_Check
        'Case True
        'V_InfoHB15601.BorderColor = Drawing.Color.Black
        'Case Else
        'InfoHA01.DonBackColor = Drawing.Color.Transparent
        'End Select

    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 156
                    If WsDossierPER.Objet_156(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_156(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class