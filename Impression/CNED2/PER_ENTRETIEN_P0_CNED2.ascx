﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P0_CNED2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P0_CNED2"%>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia"%>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia"%>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<asp:Table ID="PAGE_0" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="O_CadreInfo" runat="server" BorderStyle="None" Visible="true" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreTitreEntretien" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Table ID="O_CadreLogoMEN" runat="server" BackImageUrl="~/Images/General/LogoMEN.jpg" Width="667px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true" HorizontalAlign="Left">
                                    <asp:TableRow>
                                        <asp:TableCell Height="132px" BackColor="Transparent" Width="667px" HorizontalAlign="Left"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="O_Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                                    CssClass="EP_Imp_MediumBold">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="4px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="O_LabelAnneeEntretien" runat="server" Text="" Height="20px" Width="100px"
                                    CssClass="EP_Imp_MediumNormal"
                                    style="text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell Height="4px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreAgent" runat="server" Text="Entre l'agent" Height="20px" Width="150px"
                                    CssClass="EP_Imp_MediumNormal">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelIdentite" runat="server" Text="Nom d'usage :" Height="20px" Width="150px"
                                    CssClass="EP_Imp_SmallItalic">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelIdentite" runat="server" Text="" Height="20px" Width="550px"
                                    CssClass="EP_Imp_MediumNormal">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelPatronyme" runat="server" Text="Nom de famille :" Height="20px" Width="150px"
                                    CssClass="EP_Imp_SmallItalic">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelPatronyme" runat="server" Text="" Height="20px" Width="550px"
                                    CssClass="EP_Imp_MediumNormal">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelNaissance" runat="server" Text="Date de naissance :" Height="20px" Width="150px"
                                    CssClass="EP_Imp_SmallItalic">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelNaissance" runat="server" Text="" Height="20px" Width="550px"
                                    CssClass="EP_Imp_SmallNormal">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelStatut" runat="server" Text="Statut :" Height="20px" Width="150px"
                                    CssClass="EP_Imp_SmallItalic">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelStatut" runat="server" Text="" Height="20px" Width="550px"
                                    CssClass="EP_Imp_SmallNormal">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGrade" runat="server" Text="Corps - grade :" Height="20px" Width="150px"
                                    CssClass="EP_Imp_SmallItalic">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGrade" runat="server" Text="" Height="20px" Width="550px"
                                    CssClass="EP_Imp_SmallNormal">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelEchelon" runat="server" Text="Echelon :" Height="20px" Width="150px"
                                    CssClass="EP_Imp_SmallItalic">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelEchelon" runat="server" Text="" Height="20px" Width="550px"
                                    CssClass="EP_Imp_SmallNormal">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDateEchelon" runat="server" Text="Date de promotion dans l'échelon :" Height="20px" Width="250px"
                                    CssClass="EP_Imp_SmallItalic">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelDateEchelon" runat="server" Text="" Height="20px" Width="450px"
                                    CssClass="EP_Imp_SmallNormal">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreManager" runat="server" Text="Et son supérieur hiérarchique direct" Height="20px" Width="280px"
                                    CssClass="EP_Imp_MediumNormal">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelManager" runat="server" Text="Nom et prénom :" Height="20px" Width="150px"
                                    CssClass="EP_Imp_SmallItalic">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15001" runat="server" Text="" Height="20px" Width="550px"
                                    CssClass="EP_Imp_SmallNormal">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGradeManager" runat="server" Text="Corps - grade :" Height="20px" Width="150px"
                                    CssClass="EP_Imp_SmallItalic">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGradeManager" runat="server" Text="" Height="20px" Width="550px"
                                    CssClass="EP_Imp_SmallNormal">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelFonctionManager" runat="server" Text="Intitulé de la fonction :" Height="20px" Width="150px"
                                    CssClass="EP_Imp_SmallItalic">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelFonctionManager" runat="server" Text="" Height="20px" Width="550px"
                                    CssClass="EP_Imp_SmallNormal">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelEtablissement1" runat="server" Text="Structure :" Height="20px" Width="150px"
                                    CssClass="EP_Imp_SmallItalic">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelEtablissement1" runat="server" Text="" Height="20px" Width="550px"
                                    CssClass="EP_Imp_SmallNormal">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>  
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="O_EtiLabelDateEntretien" runat="server" Text="Date de l'entretien professionnel :" Height="20px" Width="375px"
                                    CssClass="EP_Imp_MediumNormal"
                                    style="text-align: right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="O_InfoH15000" runat="server" Text="" Height="20px" Width="375px"
                                    CssClass="EP_Imp_MediumNormal"
                                    style="text-indent: 5px">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:Table ID="O_CadreEmploi" runat="server" BorderStyle="None" Visible="true"
                Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreTitreEmploi" runat="server" Height="25px" CellPadding="0" Width="750px" 
                            CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVoletEntete" runat="server" Text="1 - " Height="20px" Width="30px"
                                        CssClass="EP_Imp_LargerBold">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVolet" runat="server" Text="Description du poste occupé par l'agent" Height="20px" Width="460px"
                                        CssClass="EP_Imp_LargerBoldSouligne">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVoletSuite" runat="server" Text="" Height="20px" Width="256px"
                                        CssClass="EP_Imp_LargerBold">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px" ColumnSpan="3"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreTitreStructure" runat="server" Height="20px" CellPadding="0" Width="750px" 
                            CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" Visible="true">
			                <asp:TableRow>
                                <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                     <asp:Label ID="O_EtiLabelStructure" runat="server" Text="- structure : " Height="20px" Width="80px"
                                        CssClass="EP_Imp_SmallBold">
                                     </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                     <asp:Label ID="O_LabelEtablissement2" runat="server" Text="" Height="20px" Width="600px"
                                        CssClass="EP_Imp_SmallNormal">
                                     </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreIntitulePoste" runat="server" Text="- intitulé du poste : " Height="20px" Width="125px"
                                       CssClass="EP_Imp_SmallBold">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelIntitulePoste" runat="server" Text="" Height="20px" Width="610px"
                                        CssClass="EP_Imp_SmallNormal">
                                     </asp:Label>     
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreDateAffectation" runat="server" Text="- date d'affectation : " Height="20px" Width="135px"
                                       CssClass="EP_Imp_SmallBold">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                     <asp:Label ID="O_LabelDateAffectation" runat="server" Text="" Height="20px" Width="600px"
                                        CssClass="EP_Imp_SmallNormal">
                                     </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>     
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreEmploiType" runat="server" Text="- emploi type (cf. REME ou REFERENS) : " Height="20px" Width="250px"
                                       CssClass="EP_Imp_SmallBold">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelEmploiType" runat="server" Text="" Height="20px" Width="498px"
                                        CssClass="EP_Imp_SmallNormal">
                                     </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>    
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitrePositionPoste" runat="server" Text="- positionnement du poste dans la structure : " Height="20px" Width="300px"
                                       CssClass="EP_Imp_SmallBold">
                                    </asp:Label>          
                                </asp:TableCell> 
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_InfoH15005" runat="server" Text="" Height="20px" Width="498px"
                                       CssClass="EP_Imp_SmallNormal"
                                       style="text-indent: 3px;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreQuotite" runat="server" Text="- quotité d'affectation : " Height="20px" Width="200px"
                                       CssClass="EP_Imp_SmallBold">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_InfoH15704" runat="server" Text="" Height="20px" Width="548px"
                                       CssClass="EP_Imp_SmallNormal"
                                       style="text-indent: 3px;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreCotationPoste" runat="server" Text="- cotation du poste (part F) : " Height="20px" Width="200px"
                                       CssClass="EP_Imp_SmallBold">
                                    </asp:Label>  
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelCotationPoste" runat="server" Text="" Height="20px" Width="548px"
                                       CssClass="EP_Imp_SmallNormal"                                       
                                       style="text-indent: 3px;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>      
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreMissions" runat="server" Text="Missions du poste :" Height="20px" Width="748px"
                                       CssClass="EP_Imp_SmallBold">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV07" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="7" V_SiDonneeDico="false"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="4"
                                       Etitext="" EtiVisible="false"
                                       EtiBorderStyle="None" EtiBackColor="Transparent"
		                               Donstyle="margin-left: 1px;"
		                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:Smaller"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>      
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero6" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero6" runat="server" Height="30px" Width="748px"
                                       Text="le cas échéant, fonctions d'encadrement ou de conduite de projet :"
                                       CssClass="EP_Imp_SmallBold">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="TableSiConduiteProjet" runat="server" CellPadding="0" CellSpacing="0" Width="748px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                 <asp:Label ID="O_LabelSiConduiteProjet" runat="server" Height="20px" Width="350px"
                                                    Text="- l'agent assume-t-il des fonctions de conduite de projet ?"
                                                    CssClass="EP_Imp_SmallNormal">
                                                 </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">        
                                                <Virtualia:VTrioHorizontalRadio ID="O_RadioH25" runat="server" V_Groupe="SiConduiteProjet"
                                                    V_PointdeVue="1" V_Objet="150" V_Information="25" V_SiDonneeDico="false"
                                                    RadioGaucheWidth="60px" RadioCentreWidth="60px" RadioDroiteWidth="0px"  
                                                    RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteVisible="false"
                                                    RadioGaucheHeight="20px" RadioCentreHeight="20px"
                                                    RadioCentreStyle="text-align:  center;"  
                                                    RadioGaucheStyle="text-align:  center;" Visible="true"
			                                        RadioGaucheBackColor="Transparent" RadioCentreBackColor="Transparent"
			                                        RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent"/>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelCadrage2" runat="server" Height="20px" Width="248px"
                                                    BackColor="Transparent" BorderStyle="none" Text="">
                                                </asp:Label>          
                                            </asp:TableCell>
                                        </asp:TableRow>      
                                    </asp:Table>
                               </asp:TableCell>  
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="TableSiEncadrement" runat="server" CellPadding="0" CellSpacing="0" Width="748px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                 <asp:Label ID="O_LabelSiEncadrement" runat="server" Height="20px" Width="350px"
                                                    Text="- l'agent assume-t-il des fonctions d'encadrement ?"
                                                    CssClass="EP_Imp_SmallNormal">
                                                 </asp:Label>          
                                           </asp:TableCell>
                                           <asp:TableCell HorizontalAlign="Left">        
                                               <Virtualia:VTrioHorizontalRadio ID="O_RadioH08" runat="server" V_Groupe="SiEncadrement"
                                                   V_PointdeVue="1" V_Objet="150" V_Information="8" V_SiDonneeDico="false"
                                                   RadioGaucheWidth="60px" RadioCentreWidth="60px" RadioDroiteWidth="0px"  
                                                   RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteVisible="false"
                                                   RadioGaucheHeight="20px" RadioCentreHeight="20px"
                                                   RadioCentreStyle="text-align:  center;"  
                                                   RadioGaucheStyle="text-align:  center;" Visible="true"
			                                       RadioGaucheBackColor="Transparent" RadioCentreBackColor="Transparent"
			                                       RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent"/>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelCadrage1" runat="server" Height="20px" Width="248px"
                                                   BackColor="Transparent" BorderStyle="none" Text="">
                                                </asp:Label>          
                                            </asp:TableCell>
                                        </asp:TableRow>      
                                    </asp:Table>
                                </asp:TableCell>     
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="TableAgentsEncadres" runat="server" CellPadding="0" CellSpacing="0" Width="748px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                 <asp:Label ID="O_LabelNbEncadres" runat="server" Height="20px" Width="225px"
                                                    Text="Si oui, préciser le nombre d'agents : "
                                                    CssClass="EP_Imp_SmallNormal">
                                                 </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="O_InfoH15009" runat="server" Text="" Height="20px" Width="35px" 
                                                   CssClass="EP_Imp_SmallNormal">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                 <asp:Label ID="O_LabelRepartition" runat="server" Height="20px" Width="210px"
                                                    Text="et leur répartition par catégorie : "
                                                    CssClass="EP_Imp_SmallNormal">
                                                 </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                               <asp:Label ID="O_InfoH15026" runat="server" Text="" Height="20px" Width="35px" 
                                                   CssClass="EP_Imp_SmallNormal"
			                                       style="text-indent: 2px; text-align: right;">
                                               </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelCategorieA" runat="server" Text="A" Height="24px" Width="20px"
                                                   CssClass="EP_Imp_MediumNormal"
                                                   style="text-align: center;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="O_InfoH15027" runat="server" Text="" Height="20px" Width="35px" 
                                                   CssClass="EP_Imp_SmallNormal"
 			                                       style="text-indent: 1px; text-align: right;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelCategorieB" runat="server" Text="B" Height="24px" Width="20px"
                                                   CssClass="EP_Imp_MediumNormal"
                                                   style="text-align: center;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                               <asp:Label ID="O_InfoH15028" runat="server" Text="" Height="20px" Width="35px"
                                                   CssClass="EP_Imp_SmallNormal"
			                                       style="text-indent: 1px; text-align: right;">
                                               </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelCategorieC" runat="server" Text="C" Height="24px" Width="20px"
                                                   CssClass="EP_Imp_MediumNormal"
                                                   style="text-align: center;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelCadrage3" runat="server" Height="20px" Width="313px"
                                                   BackColor="Transparent" BorderStyle="none" Text="">
                                                </asp:Label>          
                                            </asp:TableCell>
                                        </asp:TableRow>      
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
             </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="20px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage1" runat="server" Height="15px" Width="250px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="1 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left; text-indent: 2px" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>