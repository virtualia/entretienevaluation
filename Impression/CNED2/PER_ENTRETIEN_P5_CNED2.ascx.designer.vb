﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P5_CNED2

    '''<summary>
    '''Contrôle V_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_CadreEnteteDePage1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreEnteteDePage1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_LabelEnteteLigne11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelEnteteLigne11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelEnteteLigne12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelEnteteLigne12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_CadreTitreSignatureSuperieur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreTitreSignatureSuperieur As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_LabelVoletEnteteSignatureSuperieur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletEnteteSignatureSuperieur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelVoletSignatureSuperieur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletSignatureSuperieur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelVoletSuiteSignatureSuperieur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletSuiteSignatureSuperieur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_CadreSignatureSuperieur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreSignatureSuperieur As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_TableSuperieurHierarchique.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_TableSuperieurHierarchique As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_EtiLabelDateEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_EtiLabelDateEntretien As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_InfoH15000.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoH15000 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_EtiLabelDateTransmission.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_EtiLabelDateTransmission As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_InfoH15003.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoH15003 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_EtiLabelManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_EtiLabelManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_InfoH15001.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoH15001 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_EtiLabelFonctionManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_EtiLabelFonctionManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelFonctionManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelFonctionManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_CadreTitreAppreciation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreTitreAppreciation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_LabelVoletEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletEntete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVolet As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelVoletSuite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletSuite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelTitreCommentaires.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreCommentaires As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreNumero1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_LabelTitreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_InfoVP03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVP03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle V_LabelTitreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_InfoVQ03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVQ03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle V_CadreTitreSignatureAutorite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreTitreSignatureAutorite As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_LabelVoletEnteteSignatureAutorite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletEnteteSignatureAutorite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelVoletSignatureAutorite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletSignatureAutorite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelVoletSuiteSignatureAutorite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletSuiteSignatureAutorite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_CadreSignatureAutorite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreSignatureAutorite As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_LabelEnteteSignatureAutorite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelEnteteSignatureAutorite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_EtiLabelAutorite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_EtiLabelAutorite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NumeroPage9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_CadreEnteteDePage2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreEnteteDePage2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_LabelEnteteLigne21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelEnteteLigne21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelEnteteLigne22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelEnteteLigne22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_CadreTitreSignatureAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreTitreSignatureAgent As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_LabelVoletEnteteSignatureAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletEnteteSignatureAgent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelVoletSignatureAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletSignatureAgent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelVoletIdentiteAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletIdentiteAgent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_CadreSignatureAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreSignatureAgent As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_LabelEnteteSignatureAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelEnteteSignatureAgent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_InfoH15004.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoH15004 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_EtiLabelSignatureAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_EtiLabelSignatureAgent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_EtiCommentaireAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_EtiCommentaireAgent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_CadreModalitesRecours.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreModalitesRecours As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle V_LabelTitreModalites.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreModalites As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelTitreRecoursSpecifique.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreRecoursSpecifique As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelTexteRecoursSpecifique1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTexteRecoursSpecifique1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelTexteRecoursSpecifique2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTexteRecoursSpecifique2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelTexteRecoursSpecifique3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTexteRecoursSpecifique3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelTitreRecoursDroitCommun.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreRecoursDroitCommun As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelTexteRecoursDroitCommun1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTexteRecoursDroitCommun1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle V_LabelTexteRecoursDroitCommun2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTexteRecoursDroitCommun2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NumeroPage10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage10 As Global.System.Web.UI.WebControls.Label
End Class
