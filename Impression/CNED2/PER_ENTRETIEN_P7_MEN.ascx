﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P7_MEN.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P7_MEN"%>

<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia"%>
<%@ Register src="~/Controles/Saisies/VSixVerticalRadio.ascx" tagname="VSixVerticalRadio" tagprefix="Virtualia"%>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<asp:Table ID="PAGE_VII" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VII_CadreInfo" runat="server" BorderStyle="None" Visible="true"
                Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="VII_CadreTitreAvancement" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="none" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Table ID="VII_CadreLogoMEN" runat="server" BackImageUrl="~/Images/General/LogoMEN.jpg" Width="667px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true" HorizontalAlign="center">
                                    <asp:TableRow>
                                        <asp:TableCell Height="132px" BackColor="Transparent" Width="667px" HorizontalAlign="center"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="2px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="VII_Etiquette_Entete" runat="server" Text="FICHE DE" Height="25px" Width="746px"
                                    CssClass="EP_Imp_MediumBold"
                                    Font-Underline="true">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="6px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="VII_Etiquette" runat="server" Text="PROPOSITION D’UNE REDUCTION OU MAJORATION D’ANCIENNETE D’ECHELON" Height="25px" Width="746px"
                                    CssClass="EP_Imp_MediumBold"
                                    Font-Underline="true">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_LabelAnneeEntretien" runat="server" Text="" Height="20px" Width="746px"
                                    CssClass="EP_Imp_MediumNormal"
                                    style="text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Left">
                      <asp:Label ID="VII_LabelInfosAvancement" runat="server" Height="54px" Width="748px"
                        Text="<B>Rappel :</B> Il est demandé aux supérieurs hiérarchiques directs d'émettre un avis, favorable ou non, à l'octroi d’une réduction d'ancienneté d'échelon (1 ou 2 mois), sans toutefois préciser de souhait particulier quant au nombre de mois qu'il conviendrait d'octroyer à l'agent ayant bénéficié d'un entretien professionnel."
                        CssClass="EP_Imp_SmallNormal">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="VII_CadreEnteteAvancement" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                        BorderStyle="Solid" BorderWidth="1px" BorderColor="Black">
                        <asp:TableRow>
                            <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_EtiLabelIdentite" runat="server" Text="Nom d'usage de l'agent :" Height="20px" Width="250px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 8px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_LabelIdentite" runat="server" Text="" Height="20px" Width="496px"
                                    CssClass="EP_Imp_MediumNormal"
                                    style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_EtiLabelPrenom" runat="server" Text="Prénom :" Height="20px" Width="250px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 8px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_LabelPrenom" runat="server" Text="" Height="20px" Width="496px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>                      
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_EtiLabelNaissance" runat="server" Text="Date de naissance :" Height="20px" Width="250px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 8px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_LabelNaissance" runat="server" Text="" Height="20px" Width="496px"
                                    BackColor="Transparent" BorderStyle="None" 
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_EtiLabelTelephone" runat="server" Text="Téléphone :" Height="20px" Width="250px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 8px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_LabelTelephone" runat="server" Text="" Height="20px" Width="496px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_EtiLabelCourriel" runat="server" Text="Courriel :" Height="20px" Width="250px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 8px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_LabelCourriel" runat="server" Text="" Height="20px" Width="496px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_EtiLabelGrade" runat="server" Text="Corps - grade :" Height="20px" Width="250px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 8px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_LabelGrade" runat="server" Text="" Height="20px" Width="496px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_EtiLabelEchelon" runat="server" Text="Echelon :" Height="20px" Width="250px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 8px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_LabelEchelon" runat="server" Text="" Height="20px" Width="496px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_EtiLabelDateEchelon" runat="server" Text="Date de promotion dans l'échelon :" Height="20px" Width="250px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 8px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VII_LabelDateEchelon" runat="server" Text="" Height="20px" Width="496px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="VII_EtiLabelDateEntretien" runat="server" Text="Date de l'entretien professionnel :" Height="20px" Width="250px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 8px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="VII_InfoH15000" runat="server" Text="" Height="20px" Width="496px"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="VII_LabelAvisHierarchique" runat="server" Height="20px" Width="748px"
                        Text="Avis du supérieur hiérarchique direct ayant conduit l’entretien professionnel"
                        CssClass="EP_Imp_MediumBold"
                        Font-Underline="true">
                     </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VII_LabelInfoAvis" runat="server" Height="20px" Width="748px"
                           Text="<B>Rappel :</B> la formulation de l'avis doit être en cohérence avec le compte rendu de l'entretien professionnel."
                           CssClass="EP_Imp_SmallNormal"
                           style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VSixVerticalRadio ID="VII_RadioVA04" runat="server" V_Groupe="AvisHierarchique"
                            V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="false"
                            VRadioN1Width="550px" VRadioN2Width="550px" VRadioN3Width="550px"
                            VRadioN4Width="550px" VRadioN5Width="550px" VRadioN6Width="550px"
                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px" 
                            VRadioN1BackColor="Transparent" VRadioN2BackColor="Transparent" 
                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" 
                            VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: Small; Font-italic: false; Font-style: normal"
                            VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: Small; Font-italic: false; Font-style: normal" 
                            VRadioN3Style="margin-left: 0px; margin-top: 0px;" VRadioN4Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="<FONT SIZE=MEDIUM>Favorable</FONT> à l'attribution d'une réduction d'ancienneté" 
					        VRadioN2Text="<FONT SIZE=MEDIUM>Défavorable</FONT> à l'attribution d'une réduction d'ancienneté" 
					        VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                            VRadioN3Visible="false" VRadioN4Visible="false"
                            VRadioN5Visible="false" VRadioN6Visible="false"  
                            Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="VII_CadreAvisDefavorable" runat="server" CellPadding="0" CellSpacing="0" Width="750px" BorderStyle="none">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VII_LabelAvisDefavorable" runat="server" Height="20px" Width="570px"
                                        Text="Si votre avis est défavorable, souhaitez-vous que soit attribuée une majoration d'ancienneté :"
                                        CssClass="EP_Imp_SmallNormal"
                                        style="text-indent: 1px;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VTrioHorizontalRadio ID="VII_RadioHB04" runat="server" V_Groupe="SiAvisDefavorable"
                                        V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="false"
                                        RadioGaucheWidth="70px" RadioCentreWidth="70px" RadioDroiteWidth="30px"
                                        RadioGaucheHeight="20px" RadioCentreHeight="20px" RadioDroiteHeight="20px"
                                        RadioGaucheBackColor="Transparent" RadioCentreBackColor="Transparent" RadioDroiteBackColor="Transparent" 
                                        RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                                        RadioGaucheStyle="margin-left: 0px; margin-top: 0px; Font-italic: false; Font-style: normal" 
                                        RadioCentreStyle="margin-left: 0px; margin-top: 0px; Font-italic: false; Font-style: normal" RadioDroiteStyle="margin-left: 0px; margin-top: 0px;"
                                        RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
					                    RadioDroiteVisible="false" Visible="true"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>              
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VII_LabelAvisMotivationRefus" runat="server" Height="40px" Width="748px"
                           Text="(Rappel : en cas de demande de majoration, il conviendra d'une part de motiver cette demande et d'autre part d'en informer préalablement l'agent)."
                           CssClass="EP_Imp_SmallNormal">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="VII_CadreSignature" runat="server" CellPadding="0" CellSpacing="0" Width="741px"
                            BorderStyle="none" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VII_EtilNomManager" runat="server" Text="NOM du supérieur hiérarchique" Height="40px" Width="271px"
                                        CssClass="EP_Imp_MediumBold"
                                        BorderStyle="solid" BorderWidth="1px" BorderColor="Black"
                                        style="padding-top: 10px;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VII_EtiFonctionManager" runat="server" Text="Fonction" Height="40px" Width="211px"
                                        CssClass="EP_Imp_MediumBold"
                                        BorderStyle="solid" BorderWidth="1px" BorderColor="Black"
                                        style="margin-left: -1px; padding-top: 10px;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VII_EtiSignature" runat="server" Text="Signature" Height="40px" Width="159px"
                                        CssClass="EP_Imp_MediumBold"
                                        BorderStyle="solid" BorderWidth="1px" BorderColor="Black"
                                        style="margin-left: -1px; padding-top: 10px;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VII_EtiDateSignature" runat="server" Text="Date" Height="40px" Width="100px"
                                        CssClass="EP_Imp_MediumBold"
                                        BorderStyle="solid" BorderWidth="1px" BorderColor="Black"
                                        style="margin-left: -1px; padding-top: 10px;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VII_LabelNomManager" runat="server" Text="" Height="40px" Width="271px"
                                        CssClass="EP_Imp_SmallNormal"
                                        style="padding-top: 10px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VII_LabelFonctionManager" runat="server" Text="" Height="40px" Width="211px"
                                        CssClass="EP_Imp_SmallNormal"
                                        style="margin-left: -1px; padding-top: 10px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VII_LabelSignature" runat="server" Text="" Height="40px" Width="159px"
                                        CssClass="EP_Imp_SmallNormal"
                                        style="margin-left: -1px; padding-top: 10px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VII_LabelDateSignature" runat="server" Text="" Height="40px" Width="100px"
                                        CssClass="EP_Imp_SmallNormal"
                                        style="margin-left: -1px; padding-top: 10px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>              
                </asp:TableRow>
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>