﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P4_CNED2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P4_CNED2"%>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia"%>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="IV_CadreInfo" runat="server" BorderStyle="None" Visible="true"
     Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="IV_CadreEnteteDePage1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelEnteteLigne11" runat="server" Text="" Height="20px" Width="748px"
                            CssClass="EP_Imp_SmallNormal"
                            style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelEnteteLigne12" runat="server" Text="" Height="20px" Width="748px"
                            CssClass="EP_Imp_SmallNormal"
                            style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreTitreObjectifs" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelVoletEntete" runat="server" Text="5 - " Height="20px" Width="30px"
                            CssClass="EP_Imp_LargerBold">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelVolet" runat="server" Text="Objectifs fixés pour la nouvelle année" Height="20px" Width="556px"
                           CssClass="EP_Imp_LargerBoldSouligne">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelVoletSuite" runat="server" Text="" Height="20px" Width="160px"
                            CssClass="EP_Imp_LargerBold">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="7px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Label ID="IV_LabelTitreObjectifs" runat="server" Text="5.1 Objectifs d'activités attendus" Height="20px" Width="746px"
                           CssClass="EP_Imp_MediumBold"
                           style="text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="IV_LabelNumerobjectif1" runat="server" Text="Objectif n°1" Height="20px" Width="508px"
                            CssClass="EP_Imp_MediumNormal"                            
                            style="padding-top: 4px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVA02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="500px" DonHeight="110px" DonTabIndex="1"
                                   Donstyle="margin-left: 1px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="IV_LabelNumerobjectif2" runat="server" Text="Objectif n°2" Height="20px" Width="508px"
                            CssClass="EP_Imp_MediumNormal"                            
                            style="padding-top: 4px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="500px" DonHeight="110px" DonTabIndex="2"
                                   Donstyle="margin-left: 1px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="IV_LabelNumerobjectif3" runat="server" Text="Objectif n°3" Height="20px" Width="508px"
                            CssClass="EP_Imp_MediumNormal"                            
                            style="padding-top: 4px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="500px" DonHeight="110px" DonTabIndex="3"
                                   Donstyle="margin-left: 1px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="IV_LabelNumerobjectif4" runat="server" Text="Objectif n°4" Height="20px" Width="508px"
                            CssClass="EP_Imp_MediumNormal"                            
                            style="padding-top: 4px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="500px" DonHeight="110px" DonTabIndex="4"
                                   Donstyle="margin-left: 1px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="IV_LabelNumerobjectif5" runat="server" Text="Objectif n°5" Height="20px" Width="508px"
                            CssClass="EP_Imp_MediumNormal"                            
                            style="padding-top: 4px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="500px" DonHeight="110px" DonTabIndex="5"
                                   Donstyle="margin-left: 1px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="IV_LabelNumerobjectif6" runat="server" Text="Objectif n°6" Height="20px" Width="508px"
                            CssClass="EP_Imp_MediumNormal"                            
                            style="padding-top: 4px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableObjectif6" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVF02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="500px" DonHeight="110px" DonTabIndex="2"
                                   Donstyle="margin-left: 1px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
       <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage7" runat="server" Height="15px" Width="250px"
                BackColor="Transparent" BorderStyle="None"
                Text="7 / 13" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:right" >
            </asp:Label>   
       </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell>
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="1px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="IV_CadreEnteteDePage2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelEnteteLigne21" runat="server" Text="" Height="20px" Width="748px"
                            CssClass="EP_Imp_SmallNormal"
                            style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelEnteteLigne22" runat="server" Text="" Height="20px" Width="748px"
                            CssClass="EP_Imp_SmallNormal"
                            style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>   
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabeldemarcheAgent" runat="server" Height="45px" Width="748px"
                           Text="5.2 Démarche envisagée, et moyens à prévoir dont la formation, pour faciliter l'atteinte des objectifs"
                           CssClass="EP_Imp_MediumBold"
                           style="text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVM03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="744px" DonHeight="200px" DonTabIndex="6" 
                           EtiWidth="744px" EtiHeight="20px"  
                           Etitext="(à préciser pour chaque objectif)" Etivisible="true"
                           EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                           Etistyle="margin-left: 1px; text-align: left; text-indent: 2px;" 
                           Donstyle="margin-left: 1px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreTitrePerspectives" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelVoletEntetePerspectives" runat="server" Text="6 - " Height="20px" Width="30px"
                            CssClass="EP_Imp_LargerBold">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelVoletPerspectives" runat="server" Text="Perspectives d'évolution professionnelle" Height="20px" Width="556px"
                            CssClass="EP_Imp_LargerBoldSouligne">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelVoletSuitePerspectives" runat="server" Text="" Height="20px" Width="160px"
                            CssClass="EP_Imp_LargerBold">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="7px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Label ID="IV_LabelTitrePerspectives" runat="server" Text="6.1 Evolution des activités" Height="20px" Width="746px"
                           CssClass="EP_Imp_MediumBold"
                           style="text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVN03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="744px" DonHeight="200px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="(préciser l'échéance envisagée)" Etivisible="true"
                           EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 0px; font-style: normal"
                           Donstyle="margin-left: 1px;"                           
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero32" runat="server" Text="6.2 Evolution de carrière" Height="20px" Width="748px"
                           CssClass="EP_Imp_MediumBold"
                           style="text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVO03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="744px" DonHeight="200px" EtiHeight="20px" DonTabIndex="2" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 1px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage8" runat="server" Height="15px" Width="250px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="8 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:right" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>