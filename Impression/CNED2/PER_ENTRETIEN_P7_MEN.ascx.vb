﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P7_MEN
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
            End If
        End Set
    End Property

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim Tableaudata(0) As String
        Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)

        VII_CadreLogoMEN.BackImageUrl = Tableaudata(0) & "/Images/General/LogoMEN.jpg"
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim Chaine1 As String = ""
        Dim Chaine2 As String = ""
        Dim ChaineManager As List(Of String)

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        VII_LabelAnneeEntretien.Text = "ANNEE " & WsDossierPER.Annee
        VII_LabelIdentite.Text = WsDossierPER.Nom
        VII_LabelPrenom.Text = WsDossierPER.Prenom
        VII_LabelNaissance.Text = WsDossierPER.LibelleNaissance
        VII_LabelTelephone.Text = WsDossierPER.TelephonePro
        VII_LabelCourriel.Text = WsDossierPER.EmailPro
        VII_LabelGrade.Text = WsDossierPER.Grade(DatedEffet)
        VII_LabelEchelon.Text = WsDossierPER.Echelon(DatedEffet)
        VII_LabelDateEchelon.Text = V_WebFonction.InfoExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, 678, WsDossierPER.V_Identifiant, DatedEffet)

        VII_LabelNomManager.Text = WsDossierPER.Evaluateur(DatedEffet)
        ChaineManager = WsDossierPER.Grade_Fonction_Evaluateur
        If ChaineManager IsNot Nothing Then
            VII_LabelFonctionManager.Text = ChaineManager(1)
        End If

        Call LireLaFiche()

    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim Rang As String

        Dim VirLabel As System.Web.UI.WebControls.Label
        Do
            Ctl = V_WebFonction.VirWebControle(Me.VII_CadreInfo, "VII_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 10, 3))
            Rang = ""
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VSixVerticalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.VII_CadreInfo, "VII_RadioV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VSixVerticalRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 11, 1))
            Select Case NumObjet
                Case 157
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "Favorable à l'attribution d'une réduction d'ancienneté"
                            VirRadio.VRadioN1Check = True
                            VirRadio.VRadioN2Text = ""
                        Case Is = "Défavorable à l'attribution d'une réduction d'ancienneté"
                            VirRadio.VRadioN2Check = True
                            VirRadio.VRadioN1Text = ""
                        Case Else
                            VirRadio.VRadioN3Check = False
                            VirRadio.VRadioN4Check = False
                            VirRadio.VRadioN5Check = False
                            VirRadio.VRadioN6Check = False
                    End Select
            End Select
            VirRadio.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop

        Dim VirRadioH As VirtualiaControle_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.VII_CadreInfo, "VII_RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadioH = CType(Ctl, VirtualiaControle_VTrioHorizontalRadio)
            NumObjet = VirRadioH.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 11, 1))
            Select Case NumObjet
                Case 157
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "Oui"
                            VirRadioH.RadioGaucheCheck = True
                            VirRadioH.RadioCentreText = ""
                        Case Is = "Non"
                            VirRadioH.RadioCentreCheck = True
                            VirRadioH.RadioGaucheText = ""
                        Case Else
                            VirRadioH.RadioDroiteCheck = False
                    End Select
            End Select
            VirRadioH.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop

    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 157
                    If WsDossierPER.Objet_157(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_157(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property

End Class