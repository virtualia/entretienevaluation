﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P5_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P5_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="V_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="V_CadreTitreFormation" runat="server" Height="50px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black" BackColor="LightGray" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletEntete" runat="server" Text="V - " Height="20px" Width="30px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVolet" runat="server" Text="FORMATION" Height="20px" Width="600px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="V_CadreTitreFormationSuite" runat="server" Height="20px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletInformations" runat="server" Height="20px" Width="746px"
                            Text="A renseigner lors de l'entretien."
                            CssClass="EP_Imp_SmallItalic" style="text-indent: 1px; font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero11" runat="server" Text="1° Formation(s) suivie(s) durant l'année écoulée :" Height="25px" Width="750px"
                           CssClass="EP_Imp_SmallBold"
                           style="padding-top: 4px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="V_ListeFormationsSuivies" runat="server" CadreWidth="746px" SiColonneSelect="false" SiAppliquerCharte="false"
                             BackColorCaption="White" ForeColorCaption="Black" BackColorRow="White" ForeColorRow="Black"/>
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="V_SautPage" runat="server">
                <asp:TableFooterRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="V_CadreBasDePage11" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="V_LabelBasdePageNom11" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell> 
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="V_LabelBasdePagePrenom11" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="V_LabelBasdePageCorps11" runat="server" Height="25px" Width="350px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="V_LabelBasdePageAnnee11" runat="server" Height="25px" Width="30px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="NumeroPage11" runat="server" Height="25px" Width="50px"
                                        Text="p.11/16" 
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                                    </asp:Label>   
                                </asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero2" runat="server" Text="2° Formation(s) sollicitée(s) et non suivie(s) :" Height="38px" Width="750px"
                           CssClass="EP_Imp_SmallBold"
                           style="padding-top: 4px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="V_TableBesoin102" runat="server" CellPadding="0" CellSpacing="0" Width="746px" BorderStyle="None">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="V_LabelEnteteStage102" runat="server" Text="Intitulé" Height="25px" Width="325px"
                               CssClass="EP_Imp_SmallNormal"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="V_LabelEnteteInitiative102" runat="server" Text="Formation proposée ou demandée" Height="25px" Width="410px"
                               CssClass="EP_Imp_SmallNormal"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVA01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               DonWidth="320px" DonHeight="56px" DonTabIndex="1" 
                               DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                               DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="V_RadioHA10" runat="server" V_Groupe="InitiativeA"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVB01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               DonWidth="320px" DonHeight="56px" DonTabIndex="2" 
                               DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                               DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="V_RadioHB10" runat="server" V_Groupe="InitiativeB"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVC01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               DonWidth="320px" DonHeight="56px" DonTabIndex="3" 
                               DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                               DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="V_RadioHC10" runat="server" V_Groupe="InitiativeC"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVD01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               DonWidth="320px" DonHeight="56px" DonTabIndex="4" 
                               DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                               DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="V_RadioHD10" runat="server" V_Groupe="InitiativeD"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVE01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               DonWidth="320px" DonHeight="56px" DonTabIndex="5" 
                               DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                               DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="V_RadioHE10" runat="server" V_Groupe="InitiativeE"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="V_TableBesoin103" runat="server" CellPadding="0" CellSpacing="0" Width="746px" BorderStyle="None">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="V_LabelTypeBesoin103" runat="server" Text="3° Formation(s) envisagée(s) :" Height="38px" Width="750px"
                               CssClass="EP_Imp_SmallBold"
                               style="padding-top: 4px;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="V_LabelEnteteStage103" runat="server" Text="Intitulé" Height="25px" Width="325px"
                               CssClass="EP_Imp_SmallNormal"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="V_LabelEnteteInitiative103" runat="server" Text="Formation proposée ou demandée" Height="25px" Width="410px"
                               CssClass="EP_Imp_SmallNormal"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVH01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               DonWidth="320px" DonHeight="56px" DonTabIndex="6" 
                               DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                               DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="V_RadioHH10" runat="server" V_Groupe="InitiativeH"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false" 
                                Visible="true"/>
                          </asp:TableCell>                              
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVI01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               DonWidth="320px" DonHeight="56px" DonTabIndex="7" 
                               DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                               DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="V_RadioHI10" runat="server" V_Groupe="InitiativeI"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVJ01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               DonWidth="320px" DonHeight="56px" DonTabIndex="8" 
                               DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                               DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="V_RadioHJ10" runat="server" V_Groupe="InitiativeJ"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVK01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               DonWidth="320px" DonHeight="56px" DonTabIndex="9" 
                               DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                               DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="V_RadioHK10" runat="server" V_Groupe="InitiativeK"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVL01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               DonWidth="320px" DonHeight="56px" DonTabIndex="10" 
                               DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                               DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="V_RadioHL10" runat="server" V_Groupe="InitiativeL"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero31" runat="server" Height="30px" Width="748px"
                           Text="Observations :"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 2px; padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVP03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="744px" DonHeight="190px" DonTabIndex="11" 
                           DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="V_CadreBasDePage12" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelBasdePageNom12" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelBasdePagePrenom12" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelBasdePageCorps12" runat="server" Height="25px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelBasdePageAnnee12" runat="server" Height="25px" Width="30px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage12" runat="server" Height="25px" Width="50px"
                            Text="p.12/16" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>