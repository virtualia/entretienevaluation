﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P0_ENM
    
    '''<summary>
    '''Contrôle PAGE_0.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PAGE_0 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_CadreTitreEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreTitreEntretien As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_Etiquette As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelAnneeEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelAnneeEntretien As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelAnneeEntretien_bis.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelAnneeEntretien_bis As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_CadreEnteteEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreEnteteEntretien As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_EtiLabelCorps.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelCorps As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelCorps.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelCorps As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelGrade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelGrade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelEchelon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelEchelon As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelEchelon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelEchelon As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_CadreDateEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreDateEntretien As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_EtiLabelPeriodeEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelPeriodeEntretien As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelPeriodeEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelPeriodeEntretien As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelDateEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelDateEntretien As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoH15000.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15000 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelIdentite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelIdentite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelPatronyme.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelPatronyme As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelPatronyme.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelPatronyme As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelPrenom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelPrenom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelPrenom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelPrenom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelNaissance.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelNaissance As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelNaissance.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelNaissance As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelAdresseAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelAdresseAgent As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelAdresseAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelAdresseAgent As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelFonctionAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelFonctionAgent As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelFonctionAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelFonctionAgent As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelDirection.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelDirection As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoH15002.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15002 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelEtablissement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelEtablissement As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelEtablissement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelEtablissement As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelTitreManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreManager As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelManager As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoH15001.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15001 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_EtiLabelFonctionManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_EtiLabelFonctionManager As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelFonctionManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelFonctionManager As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_CadreEmploi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreEmploi As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreNumero1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_LabelTitreSuite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreSuite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelAnciennetePoste.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelAnciennetePoste As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoH15016.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoH15016 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreNumero2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_LabelTitreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreNumero2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoV07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoV07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle O_CadreNumero3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreNumero3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_LabelTitreNumero3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelTitreNumero3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_InfoVA03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_InfoVA03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle O_CadreBasDePage1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_CadreBasDePage1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle O_LabelBasdePageNom1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelBasdePageNom1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelBasdePagePrenom1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelBasdePagePrenom1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelBasdePageCorps1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelBasdePageCorps1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle O_LabelBasdePageAnnee1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents O_LabelBasdePageAnnee1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle NumeroPage1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage1 As Global.System.Web.UI.WebControls.Label
End Class
