﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P8_ENM
    
    '''<summary>
    '''Contrôle VIII_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VIII_CadreTitreRecours.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_CadreTitreRecours As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VIII_LabelVoletEnteteRecours.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelVoletEnteteRecours As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_LabelVoletRecours.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelVoletRecours As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_CadreNumero1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VIII_LabelTitreNumero11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelTitreNumero11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_RadioHB04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_RadioHB04 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle VIII_CadreInformationsRecours.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_CadreInformationsRecours As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VIII_LabelDateRecours.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelDateRecours As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_InfoH15704.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_InfoH15704 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_LabelTitreNumero12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelTitreNumero12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_InfoVZ03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_InfoVZ03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle VIII_LabelTitreNumero13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelTitreNumero13 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_CadreModalitesRecours.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_CadreModalitesRecours As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VIII_LabelTexteRecours1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelTexteRecours1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_LabelTexteRecours2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelTexteRecours2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_LabelTexteRecours3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelTexteRecours3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_LabelTexteRecours4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelTexteRecours4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_LabelTexteRecours5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelTexteRecours5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_CadreVoiesRecours.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_CadreVoiesRecours As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VIII_LabelVoletVoiesRecours1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelVoletVoiesRecours1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_LabelVoletVoiesRecours2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelVoletVoiesRecours2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_LabelTexteRecours7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelTexteRecours7 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_CadreBasDePage16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_CadreBasDePage16 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VIII_LabelBasdePageNom16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelBasdePageNom16 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_LabelBasdePagePrenom16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelBasdePagePrenom16 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_LabelBasdePageCorps16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelBasdePageCorps16 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VIII_LabelBasdePageAnnee16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIII_LabelBasdePageAnnee16 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle NumeroPage16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage16 As Global.System.Web.UI.WebControls.Label
End Class
