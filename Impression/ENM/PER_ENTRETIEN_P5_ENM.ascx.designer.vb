﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P5_ENM
    
    '''<summary>
    '''Contrôle V_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_CadreTitreFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreTitreFormation As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelVoletEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletEntete As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVolet As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CadreTitreFormationSuite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreTitreFormationSuite As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelVoletInformations.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelVoletInformations As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreNumero1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelTitreNumero11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_ListeFormationsSuivies.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_ListeFormationsSuivies As Global.Virtualia.Net.VirtualiaControle_VListeGrid
    
    '''<summary>
    '''Contrôle V_SautPage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_SautPage As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_CadreBasDePage11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreBasDePage11 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelBasdePageNom11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelBasdePageNom11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelBasdePagePrenom11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelBasdePagePrenom11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelBasdePageCorps11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelBasdePageCorps11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelBasdePageAnnee11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelBasdePageAnnee11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle NumeroPage11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreNumero2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelTitreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_TableBesoin102.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_TableBesoin102 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelEnteteStage102.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelEnteteStage102 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelEnteteInitiative102.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelEnteteInitiative102 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_InfoVA01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVA01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle V_RadioHA10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_RadioHA10 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle V_InfoVB01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVB01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle V_RadioHB10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_RadioHB10 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle V_InfoVC01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVC01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle V_RadioHC10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_RadioHC10 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle V_InfoVD01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVD01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle V_RadioHD10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_RadioHD10 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle V_InfoVE01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVE01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle V_RadioHE10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_RadioHE10 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle V_TableBesoin103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_TableBesoin103 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelTypeBesoin103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTypeBesoin103 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelEnteteStage103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelEnteteStage103 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelEnteteInitiative103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelEnteteInitiative103 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_InfoVH01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVH01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle V_RadioHH10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_RadioHH10 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle V_InfoVI01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVI01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle V_RadioHI10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_RadioHI10 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle V_InfoVJ01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVJ01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle V_RadioHJ10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_RadioHJ10 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle V_InfoVK01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVK01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle V_RadioHK10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_RadioHK10 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle V_InfoVL01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVL01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle V_RadioHL10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_RadioHL10 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle V_CadreNumero3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreNumero3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelTitreNumero31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelTitreNumero31 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_InfoVP03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_InfoVP03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle V_CadreBasDePage12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_CadreBasDePage12 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle V_LabelBasdePageNom12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelBasdePageNom12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelBasdePagePrenom12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelBasdePagePrenom12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelBasdePageCorps12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelBasdePageCorps12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle V_LabelBasdePageAnnee12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents V_LabelBasdePageAnnee12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle NumeroPage12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage12 As Global.System.Web.UI.WebControls.Label
End Class
