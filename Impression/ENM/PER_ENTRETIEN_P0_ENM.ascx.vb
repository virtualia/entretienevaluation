﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P0_ENM
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
            End If
        End Set
    End Property

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim Tableaudata(0) As String
        Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim ChaineManager As List(Of String)

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        O_LabelAnneeEntretien.Text = "Au titre de l'année : " & WsDossierPER.AnneePrecedente
        Select Case WsDossierPER.Annee
            Case "2016"
                O_LabelPeriodeEntretien.Text = "1er juillet " & CStr(CInt(WsDossierPER.AnneePrecedente) - 1) & " au 30 juin " & WsDossierPER.AnneePrecedente
            Case "2017"
                O_LabelPeriodeEntretien.Text = "1er juillet " & CStr(CInt(WsDossierPER.AnneePrecedente) - 1) & " au 31 décembre " & WsDossierPER.AnneePrecedente
            Case Else
                O_LabelPeriodeEntretien.Text = "1er janvier " & WsDossierPER.AnneePrecedente & " au 31 décembre " & WsDossierPER.AnneePrecedente
        End Select

        O_LabelCorps.Text = WsDossierPER.Corps(DatedEffet)
        O_LabelGrade.Text = WsDossierPER.Grade(DatedEffet)
        O_LabelEchelon.Text = WsDossierPER.Echelon(DatedEffet)

        O_LabelIdentite.Text = WsDossierPER.Nom
        O_LabelPatronyme.Text = WsDossierPER.NomPatronymique
        O_LabelPrenom.Text = WsDossierPER.Prenom
        O_LabelNaissance.Text = WsDossierPER.LibelleNaissance
        O_LabelAdresseAgent.Text = ""

        ''******** AKR 12/09/2017
        O_LabelFonctionAgent.Text = WsDossierPER.FonctionExercee(DatedEffet, False)
        'O_LabelFonctionAgent.Text = WsDossierPER.FonctionExercee(DatedEffet, True)
        O_LabelEtablissement.Text = WsDossierPER.Etablissement(DatedEffet)
        'O_LabelEtablissement.Text = WsDossierPER.Etablissement(DatedEffet) & " depuis le " & WsDossierPER.Date_Entree_Etablissement(DatedEffet)
        ''**********

        ChaineManager = WsDossierPER.Grade_Fonction_Evaluateur
        If ChaineManager IsNot Nothing Then
            O_LabelFonctionManager.Text = ChaineManager(1)
        End If

        O_LabelBasdePageNom1.Text = "Nom : " & WsDossierPER.Nom
        O_LabelBasdePagePrenom1.Text = "Prénom : " & WsDossierPER.Prenom
        O_LabelBasdePageCorps1.Text = "Corps : " & WsDossierPER.Corps(DatedEffet)
        O_LabelBasdePageAnnee1.Text = WsDossierPER.Annee

        Call LireLaFiche()
        ''******** AKR 12/09/2017
        O_InfoH15002.Text = WsDossierPER.ServiceEtDirection(DatedEffet)
        If O_InfoV07.DonText = "" And WsDossierPER.PosteFonctionnel(DatedEffet) <> "" Then
            O_InfoV07.DonText = WsDossierPER.PosteFonctionnel(DatedEffet) & " (voir fiche de poste en pièce jointe)"
        End If
        ''**********
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Ctl As Control
        Dim Rang As String
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreInfo, "O_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 8, 3))
            Rang = ""
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreEmploi, "O_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 8, 3))
            Rang = ""
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        IndiceI = 0
        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreEmploi, "O_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = ""
            If Strings.Mid(Ctl.ID, 8, 1) = "A" Then
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1))
            End If
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop


    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property

End Class