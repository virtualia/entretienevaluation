﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P4_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P4_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="IV_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="IV_CadreTitreObjectifs" runat="server" Height="50px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black" BackColor="LightGray" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelVoletEntete" runat="server" Text="IV - " Height="20px" Width="35px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelVolet" runat="server" Text="LES OBJECTIFS ASSIGNES POUR L'ANNEE A VENIR" Height="20px" Width="600px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero11" runat="server" Height="20px" Width="748px"
                           Text="1° Principaux objectifs du service pour l'année à venir :"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 2px; padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVM03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="744px" DonHeight="190px" DonTabIndex="1" 
                           DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelContexteAgent" runat="server" Height="20px" Width="748px"
                           Text="2° Déclinaison des objectifs du service pour l'agent :"
                           CssClass="EP_Imp_SmallBold"
                           style="padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableEnteteObjectifs" runat="server" CellPadding="0" CellSpacing="0" Width="373px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="IV_LabelEnteteObjectif1" runat="server" Height="16px" Width="363px"
                                   Text="Description des objectifs"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center; padding-top: 4px;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="IV_LabelEnteteObjectif2" runat="server" Height="28px" Width="363px" Text=""
                                   CssClass="EP_Imp_SmallBold">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableEnteteConditions" runat="server" CellPadding="0" CellSpacing="0" Width="375px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="IV_LabelEnteteConditions1" runat="server" Height="16px" Width="365px"
                                   Text="Conditions de mise en oeuvre"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center; padding-top: 4px;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="IV_LabelEnteteConditions2" runat="server" Height="23px" Width="365px"
                                   Text="(délais et moyens nécessaires)"
                                   CssClass="EP_Imp_SmallItalic" style="text-align: center; padding-top: 5px;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="373px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVA02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="368px" DonHeight="110px" DonTabIndex="2"
                                   DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableConditions1" runat="server" CellPadding="0" CellSpacing="0" Width="375px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVA04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="false"
                                   DonWidth="370px" DonHeight="110px" DonTabIndex="3"
                                   DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="373px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="368px" DonHeight="110px" DonTabIndex="4"
                                   DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableConditions2" runat="server" CellPadding="0" CellSpacing="0" Width="375px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVB04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="false"
                                   DonWidth="370px" DonHeight="110px" DonTabIndex="5"
                                   DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="373px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="368px" DonHeight="110px" DonTabIndex="6"
                                   DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableConditions3" runat="server" CellPadding="0" CellSpacing="0" Width="375px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVC04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="false"
                                   DonWidth="370px" DonHeight="110px" DonTabIndex="7"
                                   DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="373px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="368px" DonHeight="110px" DonTabIndex="8"
                                   DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableConditions4" runat="server" CellPadding="0" CellSpacing="0" Width="375px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVD04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="false"
                                   DonWidth="370px" DonHeight="110px" DonTabIndex="9"
                                   DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="373px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="368px" DonHeight="110px" DonTabIndex="10"
                                   DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="IV_TableConditions5" runat="server" CellPadding="0" CellSpacing="0" Width="375px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVE04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="false"
                                   DonWidth="370px" DonHeight="110px" DonTabIndex="11"
                                   DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>             
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="IV_SautPage" runat="server">
                <asp:TableFooterRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="IV_CadreBasDePage9" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="IV_LabelBasdePageNom9" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell> 
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="IV_LabelBasdePagePrenom9" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="IV_LabelBasdePageCorps9" runat="server" Height="25px" Width="350px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="IV_LabelBasdePageAnnee9" runat="server" Height="25px" Width="30px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="NumeroPage9" runat="server" Height="25px" Width="50px"
                                        Text="p.9/16" 
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                                    </asp:Label>   
                                </asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero31" runat="server" Height="20px" Width="748px"
                           Text="3° Perspectives d'évolution des conditions d'organisation et de fonctionnement du service :"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 2px; padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVN03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="744px" DonHeight="190px" DonTabIndex="12" 
                           DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero32" runat="server" Height="20px" Width="748px"
                           Text="4° Evolution envisagée des fonctions exercées :"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 2px; padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVO03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="744px" DonHeight="190px" DonTabIndex="13" 
                           DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="IV_CadreBasDePage10" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelBasdePageNom10" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelBasdePagePrenom10" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelBasdePageCorps10" runat="server" Height="25px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelBasdePageAnnee10" runat="server" Height="25px" Width="30px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage10" runat="server" Height="25px" Width="50px"
                            Text="p.10/16" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>