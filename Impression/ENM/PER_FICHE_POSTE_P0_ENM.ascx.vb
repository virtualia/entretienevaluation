﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_FICHE_POSTE_P0_ENM
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
            End If
        End Set
    End Property

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim Tableaudata(0) As String
        Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)

        CadreLogoENM.BackImageUrl = Tableaudata(0) & "/Images/General/Logo_ENM.jpg"
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        LabelSousFamille.Text = "Sous-famille : " & WsDossierPER.SousFamille(DatedEffet)
        LabelIntituleRefEmploi.Text = WsDossierPER.IntituleEmploi(DatedEffet) & "<br/>" & WsDossierPER.ReferenceEmploi(WsDossierPER.IntituleEmploi(DatedEffet))

        LabelIdentite.Text = "FICHE DE POSTE DE " & WsDossierPER.LibelleIdentite & "<br/>" & "N° DE POSTE : " & WsDossierPER.NumeroDePoste(DatedEffet) & "<br/>" & WsDossierPER.PosteFonctionnel(DatedEffet)

        '' LabelPoste.Text = "Poste : " & WsDossierPER.PosteFonctionnel(DatedEffet) & " (" & WsDossierPER.NumeroDePoste(DatedEffet) & ")"

        ' LabelCategorie.Text = "Catégorie : " & WsDossierPER.Categorie(DatedEffet)
        LabelServiceAffectation.Text = "SERVICE D'AFFECTATION : " & WsDossierPER.ServiceOuDirection(DatedEffet)

        'InfoVB03.DonText = WsDossierPER.ActivitesPrincipales(WsDossierPER.IntituleEmploi(DatedEffet))
        'InfoVC03.DonText = WsDossierPER.Aptitudes(WsDossierPER.IntituleEmploi(DatedEffet))

        Call LireLaFiche()
        If I_InfoVB15403.DonText = "" Then
            I_InfoVB15403.DonText = WsDossierPER.ActivitesPrincipales(WsDossierPER.IntituleEmploi(DatedEffet))
        End If
        If I_InfoVC15403.DonText = "" Then
            I_InfoVC15403.DonText = WsDossierPER.Aptitudes(WsDossierPER.IntituleEmploi(DatedEffet))
        End If
        I_InfoV_00.DonText = WsDossierPER.DefinitionSynthetique(WsDossierPER.IntituleEmploi(DatedEffet))
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Ctl As Control
        Dim Rang As String
        Dim IndiceI As Integer = 0

        Dim VirLabel As System.Web.UI.WebControls.Label
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreInfo, "I_InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirLabel = CType(Ctl, System.Web.UI.WebControls.Label)
            NumObjet = CShort(Strings.Mid(Ctl.ID, 9, 3))
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1))
            VirLabel.Text = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.O_CadreInfo, "I_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 8, 1))
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 157
                    If WsDossierPER.Objet_157(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_157(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property


End Class