﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P8_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P8_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="VIII_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VIII_CadreTitreRecours" runat="server" Height="50px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black" BackColor="LightGray" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VIII_LabelVoletEnteteRecours" runat="server" Text="IX - " Height="35px" Width="35px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VIII_LabelVoletRecours" runat="server" Text="RECOURS HIERARCHIQUE FORME CONTRE LE COMPTE-RENDU D'ENTRETIEN" Height="35px" Width="705px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px; text-align:left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VIII_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VIII_LabelTitreNumero11" runat="server" Height="25px" Width="746px"
                            Text="Je souhaite former une demande de révision du compte-rendu de l'entretien professionnel :" 
                            CssClass="EP_Imp_SmallBold" style="text-indent: 3px; text-align: left; font-style:italic; text-decoration:underline;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VSixBoutonRadio ID="VIII_RadioHB04" runat="server" V_Groupe="SiRecours"
                            V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="false"
                            VRadioN1Width="80px" VRadioN2Width="80px" VRadioN3Width="80px"
                            VRadioN4Width="80px" VRadioN5Width="80px" VRadioN6Width="80px"
                            VRadioN1Height="26px" VRadioN2Height="26px" VRadioN3Height="26px"
                            VRadioN4Height="26px" VRadioN5Height="26px" VRadioN6Height="26px"
                            VRadioN1Style="margin-left: 0px; margin-top: 1px; Font-size: Medium; padding-top: 4px" VRadioN2Style="margin-left: 0px; margin-top: 1px; Font-size: Medium; padding-top: 4px" 
                            VRadioN3Style="margin-left: 0px; margin-top: 1px;" VRadioN4Style="margin-left: 0px; margin-top: 1px;" 
                            VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"
                            VRadioN1Text="Oui" 
					        VRadioN2Text="Non" 
					        VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                            VRadioN3Visible="false" VRadioN4Visible="false"
                            VRadioN5Visible="false" VRadioN6Visible="false"  
                            Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="VIII_CadreInformationsRecours" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                            BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black" BackColor="Transparent">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VIII_LabelDateRecours" runat="server" Height="20px" Width="200px" Text="Date du recours :"
                                            CssClass="EP_Imp_SmallNormal" style="text-indent: 10px;">
                                    </asp:Label>
                                </asp:TableCell>       
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VIII_InfoH15704" runat="server" Height="20px" Width="100px" Text=""
                                        CssClass="EP_Imp_SmallNormal" style="text-indent: 10px;">
                                    </asp:Label>     
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VIII_LabelTitreNumero12" runat="server" Height="20px" Width="748px"
                                       Text="Motif(s) invoqué(s) par l'agent :"
                                       CssClass="EP_Imp_SmallNormal" style="text-indent: 10px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="VIII_InfoVZ03" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="730px" DonHeight="120px" DonTabIndex="11" 
                                       DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                       DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VIII_LabelTitreNumero13" runat="server" Height="35px" Width="748px"
                                       Text="Signature de l'agent évalué :"
                                       CssClass="EP_Imp_SmallNormal" style="text-indent: 10px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VIII_CadreModalitesRecours" runat="server" Height="30px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VIII_LabelTexteRecours1" runat="server" Height="50px" Width="748px"
                            Text="L'autorité hiérarchique peut être saisie par le fonctionnaire d'une demande de révision
                            du compte-rendu de l'entretien professionnel. Ce recours est exercé dans un délai de 15 jours francs
                            à compter de la date de notification à l'agent du compte-rendu de l'entretien."
                            CssClass="EP_Imp_SmallNormal" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VIII_LabelTexteRecours2" runat="server" Height="80px" Width="748px"
                            Text="L'autorité hiérarchique notifie sa réponse, acceptation ou refus, dans un délai de 15 jours francs
                            à compter de la date de réception de la demande de révision du compte-rendu de l'entretien professionnel.
                            (Si l'autorité hiérarchique ne répond pas, le silence gardé par l'administration pendant 2 mois vaut décision
                            implicite de rejet. Le délai d'un mois pour saisir la CAP court à compter de la date de formation
                            de la décision implicite de rejet)."
                            CssClass="EP_Imp_SmallNormal" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VIII_LabelTexteRecours3" runat="server" Height="50px" Width="748px"
                            Text="L'exercice de ce recours est une condition préalable à la saisine du président
                            de la commission administrative paritaire (CAP) concernée, dans un délai d'un mois 
                            à compter de la date de notification de la réponse formulée par l'autorité hiérarchique
                            dans le cadre du recours."
                            CssClass="EP_Imp_SmallNormal" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VIII_LabelTexteRecours4" runat="server" Height="40px" Width="748px"
                            Text="L'agent saisit, par écrit et de façon motivée, le président de la commission
                            administrative paritaire (CAP) dont il relève sous couvert de la voie hiérarchique."
                            CssClass="EP_Imp_SmallNormal" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VIII_LabelTexteRecours5" runat="server" Height="30px" Width="748px"
                            Text="La CAP peut, après examen de la requête de l'intéressé, demander la révision
                            du compte-rendu d'entretien professionnel."
                            CssClass="EP_Imp_SmallNormal" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="VIII_CadreVoiesRecours" runat="server" Height="50px" CellPadding="0" Width="750px" 
                            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black" BackColor="LightGray" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VIII_LabelVoletVoiesRecours1" runat="server" Text="X - " Height="35px" Width="35px"
                                        CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 5px;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VIII_LabelVoletVoiesRecours2" runat="server" Text="AUTRES VOIES DE RECOURS" Height="35px" Width="710px"
                                        CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px; text-align:left">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="3px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VIII_LabelTexteRecours7" runat="server" Height="70px" Width="748px"
                            Text="L'agent est en outre avisé qu'en application des articles R 421-1 et R 421-2 du code de justice administrative,
                            il peut présenter un recours contentieux devant le tribunal administratif dans les deux mois qui
                            suivent la communication du compte-rendu. Si l'agent forme un recours hiérarchique dans ce délai,
                            il disposera à nouveau d'un délai de deux mois pour saisir le tribunal administratif à compter 
                            de la date de réponse de l'administration implicite ou expresse."
                            CssClass="EP_Imp_SmallNormal" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>                
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="VIII_CadreBasDePage16" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VIII_LabelBasdePageNom16" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VIII_LabelBasdePagePrenom16" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VIII_LabelBasdePageCorps16" runat="server" Height="25px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VIII_LabelBasdePageAnnee16" runat="server" Height="25px" Width="30px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage16" runat="server" Height="25px" Width="50px"
                            Text="p.16/16" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>