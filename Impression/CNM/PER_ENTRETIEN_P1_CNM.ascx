﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P1_CNM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P1_CNM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="I_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreTitreResultats1" runat="server" Height="30px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Left" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVolet1" runat="server" Text="BILAN DE L'ANNEE ECOULEE EXPRIME PAR L'AGENT" Height="20px" Width="500px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVoletCollectivite1" runat="server" Text="" Height="20px" Width="216px"
                            CssClass="EP_Imp_SmallBold" style="margin-top: 5px; text-align: right;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" Width="748px" HorizontalAlign="Left" ColumnSpan="2" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" Visible="true" HorizontalAlign="Left" 
                BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="I_LabelTitreNumero11" runat="server" Height="20px" Width="748px"
                           Text=". Bilan de l'exercice des missions dans le poste au cours de l'année écoulée"
                           CssClass="EP_Imp_MediumBold"
                           style="margin-bottom: 3px; padding-top: 7px; text-align:left; text-indent:5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVA03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="740px" DonHeight="200px" DonTabIndex="1"
                           Donstyle="margin-left: 2px; Font-size: 11px;" 
                           DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                           Etitext="(effectué par l'évalué) - Ex : Actions réalisées; faits marquants; difficultés rencontrées"
                           EtiWidth="740px" EtiHeight="20px" 
                           EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                           EtiVisible="True" Etistyle="margin-left: 0px; text-indent:5px;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="I_LabelTitreNumero12" runat="server" Height="20px" Width="748px"
                           Text=". Points forts"
                           CssClass="EP_Imp_MediumBold"
                           style=" margin-bottom: 3px; padding-top: 7px; text-align:left; text-indent:5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVB03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="740px" DonHeight="150px" DonTabIndex="2" 
                           Donstyle="margin-left: 2px; Font-size: 11px;"
                           DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                           Etitext="(éléments portés ou exprimés par l'évalué)"
                           EtiWidth="740px" EtiHeight="20px"
                           EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                           EtiVisible="True" Etistyle="margin-left: 0px; text-indent:5px;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="I_LabelTitreNumero13" runat="server" Height="20px" Width="748px"
                           Text=". Points à améliorer"
                           CssClass="EP_Imp_MediumBold"
                           style=" margin-bottom: 3px; padding-top: 7px; text-align:left; text-indent:5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="740px" DonHeight="150px" DonTabIndex="3"
                           Donstyle="margin-left: 2px; Font-size: 11px;"
                           DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"  
                           Etitext="(éléments portés ou exprimés par l'évalué)"
                           EtiWidth="740px" EtiHeight="20px"
                           EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                           EtiVisible="True" Etistyle="margin-left: 0px; text-indent:5px;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_SautPage1" runat="server">
                <asp:TableFooterRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="I_CadreBasDePage2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelBasdePageNom2" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell> 
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelBasdePagePrenom2" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelBasdePageGrade2" runat="server" Height="25px" Width="350px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelBasdePageAnnee2" runat="server" Height="25px" Width="30px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="NumeroPage2" runat="server" Height="25px" Width="50px"
                                        Text="p.2/12" 
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                                    </asp:Label>   
                                </asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreTitreResultats2" runat="server" Height="50px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVolet2" runat="server" Text="BILAN DE L'ANNEE ECOULEE - OBJECTIFS EFFECTUES PAR L'EVALUATEUR" Height="50px" Width="500px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVoletCollectivite2" runat="server" Text="" Height="20px" Width="216px"
                            CssClass="EP_Imp_SmallBold" style="margin-top: 5px; text-align: right;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="7px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" Width="748px" HorizontalAlign="Left" ColumnSpan="2" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="7px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Label ID="I_LabelTitreNumero21" runat="server" Height="20px" Width="250px"
                           Text="Rappel des objectifs de service :"
                           CssClass="EP_Imp_MediumBold"
                           style="margin-bottom: 3px; padding-top: 7px; text-decoration:underline; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="742px" DonHeight="150px" DonTabIndex="4" 
                           EtiVisible="false"
                           Donstyle="margin-left: 1px; Font-size: 11px;"
                           DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="I_LabelTitreNumero22" runat="server" Height="20px" Width="748px"
                           Text=". Rappel des objectifs de la période écoulée - Résultats obtenus"
                           CssClass="EP_Imp_MediumBold"
                           style="padding-top: 4px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="I_LabelTitreNumero222" runat="server" Height="20px" Width="748px"
                           Text="(résultats individuels et contribution aux objectifs du service)"
                           CssClass="EP_Imp_MediumBold"
                           style="padding-top: 4px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObjectif" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteObjectif1" runat="server" Height="55px" Width="241px"
                                   Text="Objectifs"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteRealisation" runat="server" CellPadding="0" CellSpacing="0" Width="169px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                <asp:Label ID="I_LabelEnteteObjectif" runat="server" Height="24px" Width="167px"
                                   Text="Résultats"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteNonAtteint" runat="server" Height="30px" Width="49px"
                                   Text="Non atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEntetePartielle" runat="server" Height="30px" Width="66px"
                                   Text="partiellement atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteAtteint" runat="server" Height="30px" Width="49px"
                                   Text="Atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObservations" runat="server" CellPadding="0" CellSpacing="0" Width="298px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteObservations" runat="server" Height="55px" Width="288px"
                                   Text="Observations"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="273px" DonHeight="200px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px; Font-size: 11px;"
                                   DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                                   EtiText="Objectif n°1"
                                   Etiwidth="275px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                                   EtiVisible="True"  
                                   Etistyle="margin-left: 0px; Font-size: 12px; text-indent:2px; font-weight: bold;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="169px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHE04" runat="server" V_Groupe="Realisation1"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHE15105" runat="server" Height="20px" Width="145px" Text=""
                                    CssClass="EP_Imp_MediumNormal"
                                    style="margin-top: 0px; margin-left: 8px; margin-bottom: 0px; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes1" runat="server" CellPadding="0" CellSpacing="0" Width="298px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="false"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px; Font-size: 11px;"
                                   DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                                   EtiText="Observations de l'agent" 
                                   Etiwidth="292px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                                   EtiVisible="True" 
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="8"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" 
                                   DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                                   EtiText="Commentaires (justification par des faits précis)"
                                   Etiwidth="292px" EtiHeight="18px" 
                                   EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"  
                                   EtiVisible="True"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" Width="748px" HorizontalAlign="Left" ColumnSpan="3" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="273px" DonHeight="200px" DonTabIndex="9"
                                   Donstyle="margin-left: 0px; Font-size: 11px;"
                                   DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                                   EtiText="Objectif n°2"
                                   Etiwidth="275px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                                   EtiVisible="True"  
                                   Etistyle="margin-left: 0px; Font-size: 12px; text-indent:2px; font-weight: bold;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="169px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHF04" runat="server" V_Groupe="Realisation2"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHF15105" runat="server" Height="20px" Width="145px" Text=""
                                    CssClass="EP_Imp_MediumNormal"
                                    style="margin-top: 0px; margin-left: 8px; margin-bottom: 0px; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes2" runat="server" CellPadding="0" CellSpacing="0" Width="298px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="false"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="11"
                                   Donstyle="margin-left: 0px; Font-size: 11px;"
                                   DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                                   EtiText="Observations de l'agent" 
                                   Etiwidth="292px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                                   EtiVisible="True" 
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="12"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" 
                                   DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                                   EtiText="Commentaires (justification par des faits précis)"
                                   Etiwidth="292px" EtiHeight="18px" 
                                   EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"  
                                   EtiVisible="True"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" Width="748px" HorizontalAlign="Left" ColumnSpan="3" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="I_SautPage2" runat="server">
                            <asp:TableFooterRow>
                                <asp:TableCell VerticalAlign="Bottom">
                                    <asp:Table ID="I_CadreBasDePage3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="I_LabelBasdePageNom3" runat="server" Height="25px" Width="150px" Text=""
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                                </asp:Label>          
                                            </asp:TableCell> 
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="I_LabelBasdePagePrenom3" runat="server" Height="25px" Width="150px" Text=""
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="I_LabelBasdePageGrade3" runat="server" Height="25px" Width="350px" Text=""
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="I_LabelBasdePageAnnee3" runat="server" Height="25px" Width="30px" Text=""
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="NumeroPage3" runat="server" Height="25px" Width="50px"
                                                    Text="p.3/12" 
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                                                </asp:Label>   
                                            </asp:TableCell>
                                        </asp:TableRow>   
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableFooterRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <div style="page-break-before:always;"></div>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="I_CadreTitreResultats3" runat="server" Height="50px" CellPadding="0" Width="750px" 
                            CellSpacing="0" HorizontalAlign="Left" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelVolet3" runat="server" Text="BILAN DE L'ANNEE ECOULEE - OBJECTIFS EFFECTUES PAR L'EVALUATEUR" Height="50px" Width="500px"
                                        CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelVoletCollectivite3" runat="server" Text="" Height="20px" Width="216px"
                                        CssClass="EP_Imp_SmallBold" style="margin-top: 5px; text-align: right;">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="7px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="1px" Width="748px" HorizontalAlign="Left" ColumnSpan="2" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteObjectif12" runat="server" Height="55px" Width="241px"
                                   Text="Objectifs"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="169px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                <asp:Label ID="I_LabelEnteteObjectif2" runat="server" Height="24px" Width="167px"
                                   Text="Résultats"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteNonAtteint2" runat="server" Height="30px" Width="49px"
                                   Text="Non atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEntetePartielle2" runat="server" Height="30px" Width="66px"
                                   Text="partiellement atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteAtteint2" runat="server" Height="30px" Width="49px"
                                   Text="Atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObservations2" runat="server" CellPadding="0" CellSpacing="0" Width="298px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteObservations2" runat="server" Height="55px" Width="288px"
                                   Text="Observations"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="273px" DonHeight="200px" DonTabIndex="13"
                                   Donstyle="margin-left: 0px; Font-size: 11px;"
                                   DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                                   EtiText="Objectif n°3"
                                   Etiwidth="275px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                                   EtiVisible="True"  
                                   Etistyle="margin-left: 0px; Font-size: 12px; text-indent:2px; font-weight: bold;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="169px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHG04" runat="server" V_Groupe="Realisation3"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHG15105" runat="server" Height="20px" Width="145px" Text=""
                                    CssClass="EP_Imp_MediumNormal"
                                    style="margin-top: 0px; margin-left: 8px; margin-bottom: 0px; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes3" runat="server" CellPadding="0" CellSpacing="0" Width="298px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="false"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="15"
                                   Donstyle="margin-left: 0px; Font-size: 11px;"
                                   DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                                   EtiText="Observations de l'agent" 
                                   Etiwidth="292px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                                   EtiVisible="True" 
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="16"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" 
                                   DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                                   EtiText="Commentaires (justification par des faits précis)"
                                   Etiwidth="292px" EtiHeight="18px" 
                                   EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"  
                                   EtiVisible="True"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" Width="748px" HorizontalAlign="Left" ColumnSpan="3" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="273px" DonHeight="200px" DonTabIndex="17"
                                   Donstyle="margin-left: 0px; Font-size: 11px;"
                                   DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                                   EtiText="Objectif n°4"
                                   Etiwidth="275px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                                   EtiVisible="True"  
                                   Etistyle="margin-left: 0px; Font-size: 12px; text-indent:2px; font-weight: bold;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="169px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHH04" runat="server" V_Groupe="Realisation4"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHH15105" runat="server" Height="20px" Width="145px" Text=""
                                    CssClass="EP_Imp_MediumNormal"
                                    style="margin-top: 0px; margin-left: 8px; margin-bottom: 0px; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes4" runat="server" CellPadding="0" CellSpacing="0" Width="298px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="false"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="19"
                                   Donstyle="margin-left: 0px; Font-size: 11px;"
                                   DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                                   EtiText="Observations de l'agent" 
                                   Etiwidth="292px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                                   EtiVisible="True" 
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="20"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" 
                                   DonBorderColor="Black" DonBackColor="Transparent" DonBorderWidth="1px"
                                   EtiText="Commentaires (justification par des faits précis)"
                                   Etiwidth="292px" EtiHeight="18px" 
                                   EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"  
                                   EtiVisible="True"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" Width="746px" BackColor="Transparent" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="I_CadreBasDePage34" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelBasdePageNom4" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelBasdePagePrenom4" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelBasdePageGrade4" runat="server" Height="25px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelBasdePageAnnee4" runat="server" Height="25px" Width="30px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage4" runat="server" Height="25px" Width="50px"
                            Text="p.4/12" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>