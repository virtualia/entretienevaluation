﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P0_CNM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P0_CNM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia"%>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<asp:Table ID="PAGE_0" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="O_CadreInfo" runat="server" BorderStyle="None" BorderWidth="1px" Visible="true"
                BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreTitreEntretien" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Left">
                                            <asp:Table ID="O_CadreLogoCollectivite" runat="server" BackImageUrl="~/Images/General/LogoCNM.jpg" Width="137px"
                                                BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true">
                                                <asp:TableRow>
                                                    <asp:TableCell Height="108px" BackColor="Transparent" Width="137px"></asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>    
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                                            <asp:Label ID="O_Etiquette" runat="server" Text="COMPTE-RENDU ENTRETIEN ANNUEL D'EVALUATION" Height="35px" Width="600px"
                                                CssClass="EP_Imp_LargerBold" style="text-align: center;">
                                            </asp:Label>          
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="2px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Table runat="server" HorizontalAlign="Left">
                                    <asp:TableRow Width="140px" BackColor="LightGray">
                                        <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="O_LabelTitreAnneeEntretien" runat="server" Text="Année : " Height="25px" Width="60px"
                                                CssClass="EP_Imp_MediumNormal" style="text-indent: 3px;">
                                                </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="O_LabelAnneeEntretien" runat="server" Text="" Height="25px" Width="80px"
                                                CssClass="EP_Imp_MediumBold" style="text-align:left;">
                                                </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Table runat="server" HorizontalAlign="Left">
                                    <asp:TableRow width="360px" BackColor="LightGray">
                                        <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="O_EtiLabelDateEntretien" runat="server" Height="25px" Width="260px" Text="Date de réalisation de l'entretien :"
                                                CssClass="EP_Imp_MediumNormal" style="text-indent: 3px;">
                                                </asp:Label>   
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="O_InfoH15000" runat="server" Height="25px" Width="100px" Text=""
                                                CssClass="EP_Imp_MediumBold" style="text-align:left;">
                                                </asp:Label>          
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Table runat="server" HorizontalAlign="Left">
                                    <asp:TableRow width="690px" BackColor="LightGray">
                                        <asp:TableCell HorizontalAlign="Left">
                                            <asp:Label ID="O_EtiLabelEtablissement" runat="server" Height="25px" Width="340px" Text="Nom de la collectivité ou de l'établissement :"
                                                CssClass="EP_Imp_MediumNormal" style="text-indent: 3px;">
                                             </asp:Label>     
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                             <asp:Label ID="O_LabelEtablissement" runat="server" Height="25px" Width="350px" Text=""
                                                CssClass="EP_Imp_MediumBold" style="text-align:left;">
                                             </asp:Label>          
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEnteteEntretien" HorizontalAlign="Left" runat="server" CellPadding="0" CellSpacing="0" Width="742px">
                        <asp:TableRow>
                            <asp:TableCell Height="1px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Table runat="server" HorizontalAlign="Left" Width="742px">
                                    <asp:TableRow>
                                        <asp:TableCell BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                            <asp:Table runat="server" HorizontalAlign="Left" Width="370px">
                                                <asp:TableRow BackColor="LightGray">
                                                     <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                                                         <asp:Label ID="O_LabelTitreAgent1" runat="server" Height="25px" Width="370px" Text="Agent"
                                                            CssClass="EP_Imp_MediumBold">
                                                         </asp:Label>   
                                                     </asp:TableCell>
                                                 </asp:TableRow>
                                                 <asp:TableRow BackColor="LightGray">
                                                     <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                                                         <asp:Label ID="O_LabelTitreAgent2" runat="server" Height="20px" Width="370px" Text=""
                                                            CssClass="EP_Imp_MediumNormal" style="text-align: center;">
                                                         </asp:Label>  
                                                     </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                                    <asp:TableCell HorizontalAlign="Left">
                                                         <asp:Label ID="O_EtiLabelNom" runat="server" Height="33px" Width="60px" Text="Nom :"
                                                             CssClass="EP_Imp_SmallNormal" style="text-indent: 3px; padding-top:7px;">
                                                         </asp:Label>          
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Left">
                                                         <asp:Label ID="O_LabelNom" runat="server" Height="33px" Width="305px" Text=""
                                                             CssClass="EP_Imp_MediumNormal" style="text-indent: 1px; padding-top:7px;">
                                                         </asp:Label>          
                                                    </asp:TableCell> 
                                                </asp:TableRow>
                                                <asp:TableRow BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                                    <asp:TableCell HorizontalAlign="Left">
                                                         <asp:Label ID="O_EtiLabelPrenom" runat="server" Height="28px" Width="60px" Text="Prénom :"
                                                             CssClass="EP_Imp_SmallNormal" style="text-indent: 3px; padding-top:7px;">
                                                         </asp:Label>          
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Left">
                                                         <asp:Label ID="O_LabelPrenom" runat="server" Height="28px" Width="305px" Text=""
                                                             CssClass="EP_Imp_MediumNormal" style="text-indent: 1px; padding-top:7px;">
                                                         </asp:Label>          
                                                    </asp:TableCell> 
                                                </asp:TableRow>
                                                 <asp:TableRow BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                                    <asp:TableCell HorizontalAlign="Left">
                                                         <asp:Label ID="O_EtiLabelGrade" runat="server" Height="33px" Width="60px" Text="Grade :"
                                                            CssClass="EP_Imp_SmallNormal" style="text-indent: 3px; padding-top:7px;">
                                                         </asp:Label>          
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Left">
                                                         <asp:Label ID="O_LabelGrade" runat="server" Height="33px" Width="305px" Text=""
                                                            CssClass="EP_Imp_SmallNormal" style="text-indent: 1px; padding-top:7px;">
                                                         </asp:Label>          
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                        <asp:TableCell BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                            <asp:Table runat="server" HorizontalAlign="Left" Width="362px">
                                                <asp:TableRow BackColor="LightGray">
                                                     <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                                                         <asp:Label ID="O_LabelTitreManager1" runat="server" Height="25px" Width="362px" Text="Evaluateur"
                                                            CssClass="EP_Imp_MediumBold">
                                                         </asp:Label>   
                                                     </asp:TableCell>
                                                 </asp:TableRow>
                                                 <asp:TableRow BackColor="LightGray">
                                                     <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                                                         <asp:Label ID="O_LabelTitreManager2" runat="server" Height="20px" Width="362px" Text="Supérieur hiérarchique direct"
                                                            CssClass="EP_Imp_SmallItalic" style="text-align: center;">
                                                         </asp:Label>  
                                                     </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                                    <asp:TableCell HorizontalAlign="Left">
                                                         <asp:Label ID="O_EtiLabelManager1" runat="server" Height="33px" Width="70px" Text="Nom :"
                                                            CssClass="EP_Imp_SmallNormal" style="text-indent: 3px; padding-top:7px;">
                                                         </asp:Label>          
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Left">
                                                         <asp:Label ID="O_InfoH15001" runat="server" Height="33px" Width="292px" Text=""
                                                            CssClass="EP_Imp_SmallNormal" style="text-indent: 1px; padding-top:7px;">
                                                         </asp:Label>          
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                                    <asp:TableCell HorizontalAlign="Left">
                                                         <asp:Label ID="O_EtiLabelPrenomManager" runat="server" Height="28px" Width="70px" Text=""
                                                             CssClass="EP_Imp_SmallNormal" style="text-indent: 3px; padding-top:7px;">
                                                         </asp:Label>          
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Left">
                                                         <asp:Label ID="O_LabelPrenomManager" runat="server" Height="28px" Width="292px" Text=""
                                                             CssClass="EP_Imp_SmallNormal" style="text-indent: 1px; padding-top:7px;">
                                                         </asp:Label>          
                                                    </asp:TableCell> 
                                                </asp:TableRow>
                                                <asp:TableRow BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                                    <asp:TableCell HorizontalAlign="Left">
                                                         <asp:Label ID="O_EtiLabelFonctionManager" runat="server" Height="33px" Width="70px" Text="Fonction :"
                                                            CssClass="EP_Imp_SmallNormal" style="text-indent: 3px; padding-top:7px;">
                                                         </asp:Label>          
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Left">
                                                         <asp:Label ID="O_LabelFonctionManager" runat="server" Height="33px" Width="292px" Text=""
                                                            CssClass="EP_Imp_SmallNormal" style="text-indent: 1px; padding-top:7px;">
                                                         </asp:Label>          
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:Table ID="O_CadreEmploi" runat="server" Visible="true" Width="740px" HorizontalAlign="Left"
                BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="740px">
                            <asp:TableRow>
                                <asp:TableCell Height="3px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="O_LabelTitreSuite" runat="server" Height="20px" Width="740px"
                                       Text="Informations relatives à l'agent et à son poste"
                                       CssClass="EP_Imp_MediumBold" style="margin-bottom: 2px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table runat="server" HorizontalAlign="Left">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                 <asp:Label ID="O_EtiLabelDirection" runat="server" Height="20px" Width="90px" Text="- Affectation :"
                                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                                 </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                 <asp:Label ID="O_InfoH15002" runat="server" Height="20px" Width="600px" Text=""
                                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                                 </asp:Label>          
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table runat="server" HorizontalAlign="Left">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelIntitulePoste" runat="server" Height="20px" Width="120px"
                                                   Text="- Intitulé du poste :"
                                                   CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; font-style: normal; text-indent: 1px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="O_InfoH15005" runat="server" Height="20px" Width="600px" Text=""
                                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                                 </asp:Label>     
                                            </asp:TableCell>
                                        </asp:TableRow> 
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table runat="server" HorizontalAlign="Left">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelAnciennetePoste" runat="server" Height="20px" Width="410px"
                                                   Text="- Ancienneté dans le poste au 31 décembre de l'année en cours : "
                                                   CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; font-style: normal; text-indent: 1px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="O_InfoH15704" runat="server" Height="20px" Width="300px" Text=""
                                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                                 </asp:Label>     
                                            </asp:TableCell>
                                        </asp:TableRow>
                                     </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                              <asp:TableCell Height="3px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero2" runat="server" Height="20px" Width="700px"
                                       Text="- Descriptif du poste occupé : (cf fiche de poste)"
                                       CssClass="EP_Imp_SmallNormal">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV07" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="7" V_SiDonneeDico="false"
                                       DonWidth="736px" DonHeight="190px" DonTabIndex="3"
                                       EtiVisible="false"
                                       Donstyle="margin-left: 1px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero31" runat="server" Height="35px" Width="735px"
                                       Text="- Bilan de l'exercice des missions dans le poste au cours de l'année écoulée : y a-t-il eu des modifications dans le poste au cours de l'année écoulée ?"
                                       CssClass="EP_Imp_SmallNormal">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">        
                                   <Virtualia:VTrioHorizontalRadio ID="O_RadioH15" runat="server" V_Groupe="SiModificationsPoste"
                                       V_PointdeVue="1" V_Objet="150" V_Information="15" V_SiDonneeDico="false"
                                       RadioGaucheWidth="60px" RadioCentreWidth="60px" RadioDroiteWidth="0px"  
                                       RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteVisible="false"
                                       RadioGaucheHeight="20px" RadioCentreHeight="20px"
                                       RadioCentreBackcolor="Transparent" RadioGaucheBackcolor="Transparent" 
                                       RadioCentreBorderColor="Transparent" RadioGaucheBorderColor="Transparent" 
                                       RadioCentreStyle="margin-left: 0px; margin-top: 0px; text-align: center;"  
                                       RadioGaucheStyle="margin-left: 0px; margin-top: 0px; text-align: center;"
                                       Visible="true"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero32" runat="server" Height="20px" Width="700px"
                                       Text="- Si oui, lesquelles : (ex : changement de mission, de grade, d'affectation ... )"
                                       CssClass="EP_Imp_SmallNormal">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV16" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="16" V_SiDonneeDico="false"
                                       DonWidth="736px" DonHeight="90px" DonTabIndex="4"
                                       EtiVisible="false"
                                       Donstyle="margin-left: 1px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero33" runat="server" Height="20px" Width="700px"
                                       Text="- Commentaires et précisions :"
                                       CssClass="EP_Imp_SmallNormal">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV17" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="17" V_SiDonneeDico="false"
                                       DonWidth="736px" DonHeight="90px" DonTabIndex="4"
                                       EtiVisible="false"
                                       Donstyle="margin-left: 1px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>  
             </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="O_CadreBasDePage1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="O_LabelBasdePageNom1" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="O_LabelBasdePagePrenom1" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="O_LabelBasdePageGrade1" runat="server" Height="25px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="O_LabelBasdePageAnnee1" runat="server" Height="25px" Width="30px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage1" runat="server" Height="25px" Width="50px"
                            Text="p.1/12" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>