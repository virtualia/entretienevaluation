﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_P2_CNM
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        II_LabelVoletCollectivite1.Text = WsDossierPER.Etablissement(DatedEffet)
        II_LabelVoletGrilleCriteres1.Text = "Grille de critères / CATEGORIE " & WsDossierPER.Categorie(DatedEffet)
        II_LabelVoletAnnee1.Text = "Année " & WsDossierPER.Annee

        II_LabelVoletCollectivite2.Text = WsDossierPER.Etablissement(DatedEffet)
        II_LabelVoletGrilleCriteres2.Text = "Grille de critères / CATEGORIE " & WsDossierPER.Categorie(DatedEffet)
        II_LabelVoletAnnee2.Text = "Année " & WsDossierPER.Annee

        II_LabelBasdePageNom5.Text = "Nom : " & WsDossierPER.Nom
        II_LabelBasdePagePrenom5.Text = "Prénom : " & WsDossierPER.Prenom
        II_LabelBasdePageGrade5.Text = "Grade : " & WsDossierPER.Grade(DatedEffet)
        II_LabelBasdePageAnnee5.Text = WsDossierPER.Annee

        II_LabelBasdePageNom6.Text = "Nom : " & WsDossierPER.Nom
        II_LabelBasdePagePrenom6.Text = "Prénom : " & WsDossierPER.Prenom
        II_LabelBasdePageGrade6.Text = "Grade : " & WsDossierPER.Grade(DatedEffet)
        II_LabelBasdePageAnnee6.Text = WsDossierPER.Annee

        Call LireLaFiche()

    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String = ""
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim Categorie As String
        Dim DatedEffet As String

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.II_CadreInfo, "II_InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Select Case NumObjet
                Case 153
                    Select Case WsDossierPER.Categorie(DatedEffet)
                        Case "A", "B", "C"
                            Categorie = WsDossierPER.Categorie(DatedEffet) & Strings.Mid(Ctl.ID, 9, 1)
                        Case Else
                            Categorie = "C" & Strings.Mid(Ctl.ID, 9, 1)
                    End Select
                    Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Categorie)
                Case 154
                    Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 9, 1))
            End Select
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VSixBoutonRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.II_CadreInfo, "II_RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VSixBoutonRadio)
            NumObjet = VirRadio.V_Objet
            Select Case NumObjet
                Case 153
                    Select Case WsDossierPER.Categorie(DatedEffet)
                        Case "A", "B", "C"
                            Categorie = WsDossierPER.Categorie(DatedEffet) & Strings.Mid(Ctl.ID, 10, 1)
                        Case Else
                            Categorie = "C" & Strings.Mid(Ctl.ID, 10, 1)
                    End Select
                    Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Categorie)
                Case 154
                    Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 10, 1))
            End Select
            Select Case NumObjet
                Case 153
                    If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                        Select Case ValeurLue(NumObjet, NumInfo, Rang)
                            Case Is = "4"
                                VirRadio.VRadioN1Check = True
                            Case Is = "3"
                                VirRadio.VRadioN2Check = True
                            Case Is = "2"
                                VirRadio.VRadioN3Check = True
                            Case Is = "1"
                                VirRadio.VRadioN4Check = True
                            Case Is = "0"
                                VirRadio.VRadioN5Check = True
                            Case Else
                                VirRadio.VRadioN5Check = True
                        End Select
                    End If
            End Select
            IndiceI += 1
        Loop

    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 153
                    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_153(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class