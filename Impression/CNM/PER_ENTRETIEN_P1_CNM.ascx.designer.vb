﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P1_CNM
    
    '''<summary>
    '''Contrôle I_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_CadreTitreResultats1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreTitreResultats1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelVolet1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelVolet1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelVoletCollectivite1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelVoletCollectivite1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreNumero1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelTitreNumero11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelTitreNumero11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_InfoVA03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVA03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_LabelTitreNumero12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelTitreNumero12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_InfoVB03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVB03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_LabelTitreNumero13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelTitreNumero13 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_InfoVC03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVC03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_SautPage1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_SautPage1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_CadreBasDePage2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreBasDePage2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelBasdePageNom2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelBasdePageNom2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelBasdePagePrenom2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelBasdePagePrenom2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelBasdePageGrade2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelBasdePageGrade2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelBasdePageAnnee2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelBasdePageAnnee2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle NumeroPage2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_CadreTitreResultats2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreTitreResultats2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelVolet2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelVolet2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelVoletCollectivite2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelVoletCollectivite2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreNumero2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelTitreNumero21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelTitreNumero21 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_InfoVD03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVD03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_LabelTitreNumero22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelTitreNumero22 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelTitreNumero222.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelTitreNumero222 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_TableEnteteObjectif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteObjectif As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelEnteteObjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteObjectif1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_TableEnteteRealisation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteRealisation As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelEnteteObjectif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteObjectif As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelEnteteNonAtteint.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteNonAtteint As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelEntetePartielle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEntetePartielle As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelEnteteAtteint.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteAtteint As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_TableEnteteObservations.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteObservations As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelEnteteObservations.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteObservations As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_TableObjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableObjectif1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_InfoVE02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVE02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_TableRealisation1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableRealisation1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_RadioHE04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_RadioHE04 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle I_InfoHE15105.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoHE15105 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_TableDifficultes1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableDifficultes1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_InfoVE06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVE06 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_InfoVE07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVE07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_TableObjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableObjectif2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_InfoVF02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVF02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_TableRealisation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableRealisation2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_RadioHF04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_RadioHF04 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle I_InfoHF15105.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoHF15105 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_TableDifficultes2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableDifficultes2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_InfoVF06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVF06 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_InfoVF07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVF07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_SautPage2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_SautPage2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_CadreBasDePage3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreBasDePage3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelBasdePageNom3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelBasdePageNom3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelBasdePagePrenom3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelBasdePagePrenom3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelBasdePageGrade3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelBasdePageGrade3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelBasdePageAnnee3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelBasdePageAnnee3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle NumeroPage3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_CadreTitreResultats3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreTitreResultats3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelVolet3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelVolet3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelVoletCollectivite3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelVoletCollectivite3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_TableEnteteObjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteObjectif2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelEnteteObjectif12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteObjectif12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_TableEnteteRealisation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteRealisation2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelEnteteObjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteObjectif2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelEnteteNonAtteint2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteNonAtteint2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelEntetePartielle2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEntetePartielle2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelEnteteAtteint2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteAtteint2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_TableEnteteObservations2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableEnteteObservations2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelEnteteObservations2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelEnteteObservations2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_TableObjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableObjectif3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_InfoVG02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVG02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_TableRealisation3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableRealisation3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_RadioHG04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_RadioHG04 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle I_InfoHG15105.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoHG15105 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_TableDifficultes3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableDifficultes3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_InfoVG06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVG06 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_InfoVG07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVG07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_TableObjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableObjectif4 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_InfoVH02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVH02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_TableRealisation4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableRealisation4 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_RadioHH04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_RadioHH04 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle I_InfoHH15105.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoHH15105 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_TableDifficultes4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_TableDifficultes4 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_InfoVH06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVH06 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_InfoVH07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_InfoVH07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle I_CadreBasDePage34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_CadreBasDePage34 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle I_LabelBasdePageNom4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelBasdePageNom4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelBasdePagePrenom4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelBasdePagePrenom4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelBasdePageGrade4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelBasdePageGrade4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle I_LabelBasdePageAnnee4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents I_LabelBasdePageAnnee4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle NumeroPage4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage4 As Global.System.Web.UI.WebControls.Label
End Class
