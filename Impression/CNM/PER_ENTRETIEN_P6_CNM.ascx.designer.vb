﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_P6_CNM
    
    '''<summary>
    '''Contrôle VI_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VI_CadreTitreConclusion.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreTitreConclusion As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VI_LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelVolet As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelVoletCollectivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelVoletCollectivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreNumero1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VI_LabelTitreNumero11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_InfoV13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoV13 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle VI_LabelTitreNumero12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTitreNumero12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_InfoVS03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoVS03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle VI_CadreSignatures.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreSignatures As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VI_CadreIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreIdentite As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VI_LabelSignatureIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSignatureIdentite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelSignatureFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSignatureFonction As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelSignatureDate.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelSignatureDate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_CadreIdentiteAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreIdentiteAgent As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VI_LabelIdentiteAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelIdentiteAgent As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_IdentiteAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_IdentiteAgent As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelFonctionAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelFonctionAgent As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_InfoH15004.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoH15004 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_CadreIdentiteEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreIdentiteEvaluateur As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VI_LabelIdentiteEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelIdentiteEvaluateur As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_InfoH15001.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoH15001 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelFonctionManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelFonctionManager As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_InfoH15003.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoH15003 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_CadreIdentiteAutorite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreIdentiteAutorite As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VI_LabelIdentiteAutorite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelIdentiteAutorite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_IdentiteAutorite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_IdentiteAutorite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelFonctionAutorite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelFonctionAutorite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_InfoH15023.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_InfoH15023 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_CadreModalitesRecours.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreModalitesRecours As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VI_LabelTexteRecours1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTexteRecours1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelTexteRecours2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTexteRecours2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelTexteRecours3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTexteRecours3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelTexteRecours4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTexteRecours4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelTexteRecours5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTexteRecours5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelTexteRecours6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelTexteRecours6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_CadreBasDePage12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_CadreBasDePage12 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VI_LabelBasdePageNom12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelBasdePageNom12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelBasdePagePrenom12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelBasdePagePrenom12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelBasdePageGrade12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelBasdePageGrade12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VI_LabelBasdePageAnnee12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VI_LabelBasdePageAnnee12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle NumeroPage12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroPage12 As Global.System.Web.UI.WebControls.Label
End Class
