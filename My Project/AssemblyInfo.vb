﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Passez en revue les valeurs des attributs de l'assembly
<Assembly: AssemblyTitle("EntrtetienEvaluation")> 
<Assembly: AssemblyDescription("Gestion des formulaires d'évaluation")> 
<Assembly: AssemblyCompany("Virtualia")> 
<Assembly: AssemblyProduct("Virtualia - Version 4")>
<Assembly: AssemblyCopyright("Copyright ©  Virtualia 1992-2015")>
<Assembly: AssemblyTrademark("Virtualia.Net")> 

<Assembly: ComVisible(False)>

'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("ddbc4760-fa33-4342-855f-a097e1bdf967")>

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("4.0.2015.9")>
<Assembly: AssemblyFileVersion("4.0.2015.9")>
