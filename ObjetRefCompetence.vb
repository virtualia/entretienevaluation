﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Namespace Entretien
    Public Class ObjetRefCompetence
        Private WsNatureEntretien As String

        Private WsObjetCNM As Virtualia.Net.Entretien.VEntretienCNM = Nothing
        Private WsObjetCE As Virtualia.Net.Entretien.VEntretienCE = Nothing
        Private WsObjetCESE As Virtualia.Net.Entretien.VEntretienCESE = Nothing
        Private WsObjetCESE2 As Virtualia.Net.Entretien.VEntretienCESE2 = Nothing
        Private WsObjetCNED As Virtualia.Net.Entretien.VEntretienCNED = Nothing
        Private WsObjetCNED2 As Virtualia.Net.Entretien.VEntretienCNED2 = Nothing
        Private WsObjetENM As Virtualia.Net.Entretien.VEntretienENM = Nothing
        Private WsObjetFAM As Virtualia.Net.Entretien.VEntretienFAM = Nothing
        Private WsObjetGIP As Virtualia.Net.Entretien.VEntretienGIP = Nothing
        Private WsObjetMEN As Virtualia.Net.Entretien.VEntretienMEN = Nothing
        Private WsObjetPNF As Virtualia.Net.Entretien.VEntretienPNF = Nothing
        Private WsObjetAFB As Virtualia.Net.Entretien.VEntretienAFB = Nothing
        Private WsObjetONISEP As Virtualia.Net.Entretien.VEntretienONISEP = Nothing
        Private WsObjetISMEP As Virtualia.Net.Entretien.VEntretienISMEP = Nothing

        Public ReadOnly Property VirObjetRef As Virtualia.Net.Entretien.IGenericEntretien
            Get
                Select Case WsNatureEntretien
                    Case "CNM"
                        WsObjetCNM = New Virtualia.Net.Entretien.VEntretienCNM
                        Return WsObjetCNM
                    Case "CE"
                        WsObjetCE = New Virtualia.Net.Entretien.VEntretienCE
                        Return WsObjetCE
                    Case "CESE"
                        WsObjetCESE = New Virtualia.Net.Entretien.VEntretienCESE
                        Return WsObjetCESE
                    Case "CESE2"
                        WsObjetCESE2 = New Virtualia.Net.Entretien.VEntretienCESE2
                        Return WsObjetCESE2
                    Case "CNED"
                        WsObjetCNED = New Virtualia.Net.Entretien.VEntretienCNED
                        Return WsObjetCNED
                    Case "CNED2"
                        WsObjetCNED2 = New Virtualia.Net.Entretien.VEntretienCNED2
                        Return WsObjetCNED2
                    Case "ENM"
                        WsObjetENM = New Virtualia.Net.Entretien.VEntretienENM
                        Return WsObjetENM
                    Case "FAM"
                        WsObjetFAM = New Virtualia.Net.Entretien.VEntretienFAM
                        Return WsObjetFAM
                    Case "GIP"
                        WsObjetGIP = New Virtualia.Net.Entretien.VEntretienGIP
                        Return WsObjetGIP
                    Case "ISMEP"
                        WsObjetISMEP = New Virtualia.Net.Entretien.VEntretienISMEP
                        Return WsObjetISMEP
                    Case "MEN"
                        WsObjetMEN = New Virtualia.Net.Entretien.VEntretienMEN
                        Return WsObjetMEN
                    Case "ONISEP"
                        WsObjetONISEP = New Virtualia.Net.Entretien.VEntretienONISEP
                        Return WsObjetONISEP
                    Case "PNF"
                        WsObjetPNF = New Virtualia.Net.Entretien.VEntretienPNF
                        Return WsObjetPNF
                    Case "AFB"
                        WsObjetAFB = New Virtualia.Net.Entretien.VEntretienAFB
                        Return WsObjetAFB
                    Case Else
                        Return Nothing
                End Select
            End Get
        End Property

        Public ReadOnly Property TypeEntretien As String
            Get
                Return WsNatureEntretien
            End Get
        End Property

        Public ReadOnly Property NombreMaxi(ByVal NomTable As String) As Integer
            Get
                Select Case NomTable
                    Case "CMP"
                        If VirObjetRef.Table_Competences Is Nothing Then
                            Return 0
                        End If
                        Return VirObjetRef.Table_Competences.Count
                    Case "CPL"
                        If VirObjetRef.Table_Complements Is Nothing Then
                            Return 0
                        End If
                        Return VirObjetRef.Table_Complements.Count
                    Case "FOR"
                        Return VirObjetRef.Table_Formations.Count
                    Case "OBS"
                        Return VirObjetRef.Table_Observations.Count
                    Case "RES" 'Résultats
                        Select Case WsNatureEntretien
                            Case "CE", "ENM", "MEN", "ONISEP", "GIP"
                                Return 5
                            Case "CNED", "CNED2"
                                Return 6
                            Case "FAM"
                                Return 3
                            Case "CNM", "CESE", "CESE2", "PNF"
                                Return 4
                            Case "ISMEP"
                                Return 7
                            Case "AFB"
                                Return 11
                        End Select
                    Case "SYN"
                        If VirObjetRef.Table_Syntheses Is Nothing Then
                            Return 0
                        End If
                        Return VirObjetRef.Table_Syntheses.Count
                End Select
                Return 0
            End Get
        End Property

        Public ReadOnly Property LitteralSynthese(ByVal Critere As Integer) As String
            Get
                Select Case WsNatureEntretien
                    Case "CE", "CESE", "CESE2", "ENM", "FAM", "GIP"
                        Return VirObjetRef.LitteralSynthese(Critere)
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property LitteralResultat(ByVal Critere As Integer) As String
            Get
                Select Case WsNatureEntretien
                    Case "CNM", "CE", "CESE", "CESE2", "CNED", "ENM", "FAM", "PNF", "AFB", "GIP"
                        Return VirObjetRef.LitteralResultat(Critere)
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property LitteralCompetence(ByVal Critere As Integer, Optional ByVal SiPrecision As Boolean = False) As String
            Get
                Select Case WsNatureEntretien
                    Case "CNM", "CE", "CESE", "CESE2", "ENM", "FAM", "PNF", "AFB", "GIP"
                        Return VirObjetRef.LitteralCompetence(Critere, SiPrecision)
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property TableCompetence(ByVal IndexPrimaire As Integer, ByVal IndexSecondaire As Integer) As String
            Get
                Dim ListeRef As List(Of Virtualia.Net.Entretien.ObjetTable) = VirObjetRef.Table_Competences
                If ListeRef Is Nothing Then
                    Return ""
                End If
                Select Case IndexPrimaire
                    Case 0 To ListeRef.Count - 1
                        Select Case IndexSecondaire
                            Case 0 'Intitule
                                Return ListeRef.Item(IndexPrimaire).Intitule
                            Case 1 'Numéro de famille
                                Return ListeRef.Item(IndexPrimaire).Rang_Famille
                            Case 2 'Intitulé de famille
                                Return ListeRef.Item(IndexPrimaire).Libelle_Famille
                            Case 3 'Numéro de sous-famille
                                Return ListeRef.Item(IndexPrimaire).Rang_SousFamille
                            Case 4 'Intitulé de la sous-famille
                                If ListeRef.Item(IndexPrimaire).Libelle_SousFamille = "" Then
                                    Return ListeRef.Item(IndexPrimaire).Intitule
                                Else
                                    Return ListeRef.Item(IndexPrimaire).Libelle_SousFamille
                                End If
                            Case 5 'Numéro de tri
                                Return ListeRef.Item(IndexPrimaire).Clef_Index.ToString
                            Case 6 'Clef ou rang origine
                                Return ListeRef.Item(IndexPrimaire).Clef_Origine
                        End Select
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property VirRangCompetence(ByVal RangOrigine As String, ByVal IndexSecondaire As Integer) As String
            Get
                Dim ListeRef As List(Of Virtualia.Net.Entretien.ObjetTable) = VirObjetRef.Table_Competences
                If ListeRef Is Nothing Then
                    Return ""
                End If
                Dim IndiceA As Integer
                For IndiceA = 0 To ListeRef.Count - 1
                    If ListeRef.Item(IndiceA).Clef_Origine = RangOrigine Then
                        Select Case IndexSecondaire
                            Case 0 'Intitule
                                Return ListeRef.Item(IndiceA).Intitule
                            Case 1 'Numéro de famille
                                Return ListeRef.Item(IndiceA).Rang_Famille
                            Case 2 'Intitulé de famille
                                Return ListeRef.Item(IndiceA).Libelle_Famille
                            Case 3 'Numéro de sous-famille
                                Return ListeRef.Item(IndiceA).Rang_SousFamille
                            Case 4 'Intitulé de la sous-famille
                                If ListeRef.Item(IndiceA).Libelle_SousFamille = "" Then
                                    Return ListeRef.Item(IndiceA).Intitule
                                Else
                                    Return ListeRef.Item(IndiceA).Libelle_SousFamille
                                End If
                            Case 5 'Numéro de tri
                                Return ListeRef.Item(IndiceA).Clef_Index.ToString
                            Case 6 'Clef ou rang origine
                                Return ListeRef.Item(IndiceA).Clef_Origine
                        End Select
                    End If
                Next IndiceA
                Return ""
            End Get
        End Property

        Public ReadOnly Property SiCompetenceManager(ByVal Intitule As String) As Boolean
            Get
                Select Case WsNatureEntretien
                    Case Is <> "FAM"
                        Return False
                End Select
                Dim ListeRef As List(Of Virtualia.Net.Entretien.ObjetTable) = VirObjetRef.Table_Competences
                If ListeRef Is Nothing Then
                    Return False
                End If
                'Pour FAM. Détection des compétences managériales
                Dim IndiceA As Integer
                For IndiceA = 0 To ListeRef.Count - 1
                    If ListeRef.Item(IndiceA).Intitule = Intitule Then
                        If ListeRef.Item(IndiceA).Rang_Famille = "3.4" Then
                            Return True
                        End If
                    End If
                Next IndiceA
                Return False
            End Get
        End Property

        Public ReadOnly Property TableObservation(ByVal IndexPrimaire As Integer, ByVal IndexSecondaire As Integer) As String
            Get
                Dim ListeRef As List(Of Virtualia.Net.Entretien.ObjetTable) = VirObjetRef.Table_Observations
                If ListeRef Is Nothing Then
                    Return ""
                End If
                Select Case IndexPrimaire
                    Case 0 To ListeRef.Count - 1
                        Select Case IndexSecondaire
                            Case 0 'Numero Famille
                                Return ListeRef.Item(IndexPrimaire).Rang_Famille
                            Case 1 'Intitulé
                                Return ListeRef.Item(IndexPrimaire).Intitule
                            Case 2 'Rang ou Clef Origine
                                Return ListeRef.Item(IndexPrimaire).Clef_Origine
                        End Select
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property VirRangObservation(ByVal RangOrigine As String, ByVal IndexSecondaire As Integer) As String
            Get
                Dim ListeRef As List(Of Virtualia.Net.Entretien.ObjetTable) = VirObjetRef.Table_Observations
                If ListeRef Is Nothing Then
                    Return ""
                End If
                Dim IndiceA As Integer
                For IndiceA = 0 To ListeRef.Count - 1
                    If ListeRef.Item(IndiceA).Clef_Origine = RangOrigine Then
                        Select Case IndexSecondaire
                            Case 0 'Famille
                                Return ListeRef.Item(IndiceA).Rang_Famille
                            Case 1 'Intitulé
                                Return ListeRef.Item(IndiceA).Intitule
                            Case 2 'Rang ou Clef Origine
                                Return ListeRef.Item(IndiceA).Clef_Origine
                        End Select
                    End If
                Next IndiceA
                Return ""
            End Get
        End Property

        Public ReadOnly Property TableFormation(ByVal IndexPrimaire As Integer, ByVal IndexSecondaire As Integer) As String
            Get
                Dim ListeRef As List(Of Virtualia.Net.Entretien.ObjetTable) = VirObjetRef.Table_Formations
                If ListeRef Is Nothing Then
                    Return ""
                End If
                Select Case IndexPrimaire
                    Case 0 To ListeRef.Count - 1
                        Select Case IndexSecondaire
                            Case 0 'Intitulé
                                Return ListeRef.Item(IndexPrimaire).Intitule
                            Case 1 'Famille
                                Return ListeRef.Item(IndexPrimaire).Rang_Famille
                            Case 2 'Index
                                Return ListeRef.Item(IndexPrimaire).Clef_Index.ToString
                            Case 3 'Rang ou Clef Origine
                                Return ListeRef.Item(IndexPrimaire).Clef_Origine
                        End Select
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property VirRangFormation(ByVal RangOrigine As String, ByVal IndexSecondaire As Integer) As String
            Get
                Dim ListeRef As List(Of Virtualia.Net.Entretien.ObjetTable) = VirObjetRef.Table_Formations
                If ListeRef Is Nothing Then
                    Return ""
                End If
                Dim IndiceA As Integer
                For IndiceA = 0 To ListeRef.Count - 1
                    If ListeRef.Item(IndiceA).Clef_Origine = RangOrigine Then
                        Select Case IndexSecondaire
                            Case 0 'Intitulé
                                Return ListeRef.Item(IndiceA).Intitule
                            Case 1 'Famille
                                Return ListeRef.Item(IndiceA).Rang_Famille
                            Case 2 'Index
                                Return ListeRef.Item(IndiceA).Clef_Index.ToString
                            Case 3 'Rang ou Clef Origine
                                Return ListeRef.Item(IndiceA).Clef_Origine
                        End Select
                    End If
                Next IndiceA
                Return ""
            End Get
        End Property

        Public ReadOnly Property TableSynthese(ByVal IndexPrimaire As Integer, ByVal IndexSecondaire As Integer) As String
            Get
                Dim ListeRef As List(Of Virtualia.Net.Entretien.ObjetTable) = VirObjetRef.Table_Syntheses
                If ListeRef Is Nothing Then
                    Return ""
                End If
                Select Case IndexPrimaire
                    Case 0 To ListeRef.Count - 1
                        Select Case IndexSecondaire
                            Case 0 'Intitulé
                                Return ListeRef.Item(IndexPrimaire).Intitule
                            Case 1 'Famille
                                Return ListeRef.Item(IndexPrimaire).Rang_Famille
                            Case 2 'Index
                                Return ListeRef.Item(IndexPrimaire).Clef_Index.ToString
                            Case 3 'Rang ou Clef Origine
                                Return ListeRef.Item(IndexPrimaire).Clef_Origine
                        End Select
                End Select
                Return ""
            End Get
        End Property
        Public ReadOnly Property VirRangSynthese(ByVal RangOrigine As String, ByVal IndexSecondaire As Integer) As String
            Get
                Dim ListeRef As List(Of Virtualia.Net.Entretien.ObjetTable) = VirObjetRef.Table_Syntheses
                If ListeRef Is Nothing Then
                    Return ""
                End If
                Dim IndiceA As Integer
                For IndiceA = 0 To ListeRef.Count - 1
                    If ListeRef.Item(IndiceA).Clef_Origine = RangOrigine Then
                        Select Case IndexSecondaire
                            Case 0 'Intitulé
                                Return ListeRef.Item(IndiceA).Intitule
                            Case 1 'Famille
                                Return ListeRef.Item(IndiceA).Rang_Famille
                            Case 2 'Index
                                Return ListeRef.Item(IndiceA).Clef_Index.ToString
                            Case 3 'Rang ou Clef Origine
                                Return ListeRef.Item(IndiceA).Clef_Origine
                        End Select
                    End If
                Next IndiceA
                Return ""
            End Get
        End Property


        Public ReadOnly Property TableComplement(ByVal IndexPrimaire As Integer, ByVal IndexSecondaire As Integer) As String
            Get
                Dim ListeRef As List(Of Virtualia.Net.Entretien.ObjetTable) = VirObjetRef.Table_Complements
                If ListeRef Is Nothing Then
                    Return ""
                End If
                Select Case IndexPrimaire
                    Case 0 To ListeRef.Count - 1
                        Select Case IndexSecondaire
                            Case 0 'Clef ou Rang
                                Return ListeRef.Item(IndexPrimaire).Clef_Index.ToString
                            Case 1 'Intitulé
                                Return ListeRef.Item(IndexPrimaire).Intitule
                            Case 2 'Nature
                                Return ListeRef.Item(IndexPrimaire).Libelle_Famille
                            Case 3 'Clef Origine
                                Return ListeRef.Item(IndexPrimaire).Clef_Origine
                        End Select
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property VirRangComplement(ByVal RangOrigine As String, ByVal IndexSecondaire As Integer) As String
            Get
                Dim ListeRef As List(Of Virtualia.Net.Entretien.ObjetTable) = VirObjetRef.Table_Complements
                If ListeRef Is Nothing Then
                    Return ""
                End If
                Dim IndiceA As Integer
                For IndiceA = 0 To ListeRef.Count - 1
                    If ListeRef.Item(IndiceA).Clef_Origine = RangOrigine Then
                        Select Case IndexSecondaire
                            Case 0 'Clef ou Rang
                                Return ListeRef.Item(IndiceA).Clef_Index.ToString
                            Case 1 'Intitulé
                                Return ListeRef.Item(IndiceA).Intitule
                            Case 2 'Nature
                                Return ListeRef.Item(IndiceA).Libelle_Famille
                            Case 3 'Clef Origine
                                Return ListeRef.Item(IndiceA).Clef_Origine
                        End Select
                    End If
                Next IndiceA
                Return ""
            End Get
        End Property

        Public Sub New(ByVal TypeEntretien As String)
            WsNatureEntretien = TypeEntretien
        End Sub
    End Class
End Namespace