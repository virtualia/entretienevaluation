﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class DossierHonda
        Private WsPointeurGlobal As Virtualia.Net.WebAppli.ObjetGlobal
        Private WsSiEstAussiManager As Boolean 'Pour déterminer le type de formulaire
        Private WsNomUtiSGBD As String
        Private WsNomUtilisateur As String
        
        Private WsIdentifiant As Integer
        Private WsListeFonds As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Private WsListeEntretiens As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Private WsFicheEtatCivil As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
        '
        Private WsAnnee As String = Year(Now).ToString
        Private WsObjet_150 As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL
        Private WsLst_151 As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE)
        Private WsLst_152 As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_COMPORTEMENT)
        Private WsLst_153 As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_FORMATION)
        '
        Private WsSiSelf As Boolean = False
        Private WsDateSignature_Self As String = ""
        Private WsObjet_150_Lu As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL
        '***********************************************
        Private WsIdeManager As Integer = 0
        Private WsNomPrenomManager As String
        Private WsServiceManager As String
        Private WsFonctionManager As String
        Private WsEmailManager As String
        Private WsSiDRH As Boolean = False

        Public ReadOnly Property PeriodeKI(ByVal DateEffet As String) As String
            Get
                Dim IndiceI As Integer
                Dim LstDecision As List(Of Virtualia.TablesObjet.ShemaREF.OUT_DECISION)
                Dim NoKI As Integer = KI(DateEffet)
                Dim DateJour As Date = CDate(WsPointeurGlobal.VirRhDates.DateduJour)
                Dim DateDebut As Date
                Dim DateFin As Date

                LstDecision = WsPointeurGlobal.ListeTablesDecision(NoKI, WsSiDRH)
                If LstDecision Is Nothing OrElse LstDecision.Count = 0 Then
                    LstDecision = WsPointeurGlobal.ListeTablesDecision(NoKI - 1)
                End If
                If LstDecision Is Nothing OrElse LstDecision.Count = 0 Then
                    Return "FIN KI"
                End If
                For IndiceI = 0 To LstDecision.Count - 1
                    If LstDecision.Item(IndiceI).ValeurInterne(1) <> "" Then
                        DateDebut = CDate(LstDecision.Item(IndiceI).ValeurInterne(1))
                    Else
                        DateDebut = DateJour
                    End If
                    DateFin = DateJour
                    If LstDecision.Item(IndiceI).ValeurInterne(2) <> "" Then
                        DateFin = CDate(LstDecision.Item(IndiceI).ValeurInterne(2))
                    ElseIf IndiceI < LstDecision.Count - 1 Then
                        If LstDecision.Item(IndiceI + 1).ValeurInterne(1) <> "" Then
                            DateFin = CDate(LstDecision.Item(IndiceI + 1).ValeurInterne(1))
                        End If
                    End If
                    If DateJour >= DateDebut And DateJour <= DateFin Then
                        Return LstDecision.Item(IndiceI).ValeurInterne(0)
                    End If
                Next IndiceI
                LstDecision = WsPointeurGlobal.ListeTablesDecision(NoKI + 1, WsSiDRH)
                If LstDecision Is Nothing OrElse LstDecision.Count = 0 Then
                    Return "FIN KI"
                End If
                If Objet_150(Year(Now)) Is Nothing Then
                    Annee = Year(Now)
                    Return "DEBUT KI"
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property KI(ByVal ArgumentDate As String) As Integer
            Get
                If CDate(ArgumentDate).Month >= 4 Then
                    Return 91 + (CDate(ArgumentDate).Year - 2014)
                Else
                    Return 90 + (CDate(ArgumentDate).Year - 2014)
                End If
            End Get
        End Property

        Public ReadOnly Property DateValeur_Infos(ByVal ArgumentDate As String) As String
            Get
                Dim KI_ArgumentDate As Integer = KI(ArgumentDate)
                Dim KI_DateJour As Integer = KI(WsPointeurGlobal.VirRhDates.DateduJour)
                If KI_ArgumentDate = KI_DateJour Then
                    Return WsPointeurGlobal.VirRhDates.DateduJour
                End If
                Return "31/03/" & CDate(ArgumentDate).Year + 1
            End Get
        End Property

        Public ReadOnly Property V_Identifiant As Integer
            Get
                Return WsIdentifiant
            End Get
        End Property

        Public ReadOnly Property Qualite As String
            Get
                If WsFicheEtatCivil Is Nothing Then
                    Return ""
                    Exit Property
                End If
                Return WsFicheEtatCivil.Qualite
            End Get
        End Property

        Public ReadOnly Property Nom As String
            Get
                If WsFicheEtatCivil Is Nothing Then
                    Return ""
                    Exit Property
                End If
                Return WsFicheEtatCivil.Nom
            End Get
        End Property

        Public ReadOnly Property Prenom As String
            Get
                If WsFicheEtatCivil Is Nothing Then
                    Return ""
                    Exit Property
                End If
                Return WsFicheEtatCivil.Prenom
            End Get
        End Property

        Public ReadOnly Property Contrat(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaStatut And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaStatut _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_CONTRAT).Type_de_Contrat
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Emploi_Conventionnel(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_EMPLOI).EmploiConventionnel
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property NiveauAffectation(ByVal DateEffet As String, ByVal Index As Integer) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    Select Case Index
                        Case 1
                            Return Fiche.Structure_de_rattachement
                        Case 2
                            Return Fiche.Structure_d_affectation
                        Case 3
                            Return Fiche.Structure_de_3e_niveau
                        Case 4
                            Return Fiche.Structure_de_4e_niveau
                    End Select
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property FonctionExercee(ByVal DateEffet As String, ByVal SiDate As Boolean) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    Select Case SiDate
                        Case True
                            Return Fiche.Date_de_Valeur
                        Case Else
                            Return Fiche.Fonction_exercee
                    End Select
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Adresse_Mail As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaAdrPro).ToList
                If LstFiches Is Nothing Then
                    Return ""
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO).Email
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property FormationSuivie(ByVal DateEffet As String, ByVal Index As Integer) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim IndiceK As Integer = 0
                Dim IndiceCourant As Integer = 0
                Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_SESSION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaFormation _
                                        Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    Return ""
                End If
                For Each VirFiche In LstFiches
                    FichePER = CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_SESSION)
                    If FichePER.Motif_de_non_participation = "" Then
                        Select Case WsPointeurGlobal.VirRhDates.ComparerDates(FichePER.Date_de_Valeur, DateEffet)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                IndiceCourant += 1
                                If IndiceCourant = Index Then
                                    If FichePER.Intitule_de_la_session <> "" Then
                                        Return FichePER.Date_de_Valeur & " - " & FichePER.Intitule_de_la_session
                                    Else
                                        Return FichePER.Date_de_Valeur & " - " & FichePER.Intitule
                                    End If
                                End If
                        End Select
                    End If
                Next
                Return ""
            End Get
        End Property

        Public ReadOnly Property DateEntretienPrecedent(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 1 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL)
                    If Strings.Right(Fiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                        Return CType(LstFiches.Item(1), Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL).Date_de_Valeur
                    End If
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property AffectationSecondaire(ByVal DateEffet As String, ByVal Index As Integer) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION_SECOND

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaAffectation2nd And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaAffectation2nd _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION_SECOND)
                    Select Case Index
                        Case 1
                            Return Fiche.Structure_de_rattachement
                        Case 2
                            Return Fiche.Structure_d_affectation
                        Case 3
                            Return Fiche.Structure_de_3e_niveau
                        Case 4
                            Return Fiche.Structure_de_4e_niveau
                    End Select
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Evaluateur_Nom_Prenom() As String
            Get
                If WsObjet_150 Is Nothing Then
                    Call InitialiserManager(WsPointeurGlobal.VirRhDates.DateduJour)
                    Return WsNomPrenomManager
                End If
                If WsIdeManager = 0 Then
                    Call InitialiserManager(WsPointeurGlobal.VirRhDates.DateduJour)
                End If
                Return WsNomPrenomManager
            End Get
        End Property

        Public ReadOnly Property Evaluateur_Fonction() As String
            Get
                If WsObjet_150 Is Nothing Then
                    Return Nothing
                End If
                If WsIdeManager = 0 Then
                    Call InitialiserManager(WsPointeurGlobal.VirRhDates.DateduJour)
                End If
                Return WsFonctionManager
            End Get
        End Property

        Public ReadOnly Property Evaluateur_Service() As String
            Get
                If WsObjet_150 Is Nothing Then
                    Return Nothing
                End If
                If WsIdeManager = 0 Then
                    Call InitialiserManager(WsPointeurGlobal.VirRhDates.DateduJour)
                End If
                Return WsServiceManager
            End Get
        End Property

        Public ReadOnly Property SiFormulaireSigne_Termine(ByVal DateEffet) As Boolean
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL)
                    If Strings.Right(Fiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                        If Fiche.Date_de_Signature_Evalue_KI3 <> "" Then
                            Return True
                        End If
                    End If
                End If
                Return False
            End Get
        End Property

        Public WriteOnly Property TableauMaj(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
            Set(ByVal value As String)
                Dim TableauData As ArrayList
                Dim IndiceF As Integer

                Select Case NoObjet
                    Case 150
                        TableauData = WsObjet_150.V_TableauData
                        TableauData(NoInfo) = value
                        Objet_150.V_TableauData = TableauData
                    Case 151
                        For IndiceF = 0 To WsLst_151.Count - 1
                            If WsLst_151.Item(IndiceF).Numero.ToString = Rang Then
                                TableauData = WsLst_151.Item(IndiceF).V_TableauData
                                TableauData(NoInfo) = value
                                WsLst_151.Item(IndiceF).V_TableauData = TableauData
                            End If
                        Next IndiceF
                    Case 152
                        For IndiceF = 0 To WsLst_152.Count - 1
                            If WsLst_152.Item(IndiceF).Numero_Tri = Rang Then
                                TableauData = WsLst_152.Item(IndiceF).V_TableauData
                                TableauData(NoInfo) = value
                                WsLst_152.Item(IndiceF).V_TableauData = TableauData
                            End If
                        Next IndiceF
                    Case 153
                        For IndiceF = 0 To WsLst_153.Count - 1
                            If WsLst_153.Item(IndiceF).NumeroTri = Rang Then
                                TableauData = WsLst_153.Item(IndiceF).V_TableauData
                                TableauData(NoInfo) = value
                                WsLst_153.Item(IndiceF).V_TableauData = TableauData
                            End If
                        Next IndiceF
                End Select
            End Set
        End Property

        Public ReadOnly Property SiAnneeExiste(ByVal Param As String) As Boolean
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    For Each Virfiche In LstFiches
                        If Strings.Right(Virfiche.Date_de_Valeur, 4) = Param Then
                            Return True
                        End If
                    Next
                End If
                Return False
            End Get
        End Property

        Public ReadOnly Property SiEst_Aussi_Un_Manager As Boolean
            Get
                Return WsSiEstAussiManager
            End Get
        End Property

        Public Property SiSelf As Boolean
            Get
                Return WsSiSelf
            End Get
            Set(ByVal value As Boolean)
                WsSiSelf = value
            End Set
        End Property

        Public Property SiDRH As Boolean
            Get
                Return WsSiDRH
            End Get
            Set(ByVal value As Boolean)
                WsSiDRH = value
            End Set
        End Property

        Public ReadOnly Property Date_de_Signature_Evalue As String
            Get
                Return WsDateSignature_Self
            End Get
        End Property

        Public ReadOnly Property SiVisible(ByVal NumObjet As Integer, ByVal NumInfo As Integer, ByVal DateEval As String) As Boolean
            Get
                Dim Periode_KI As String = PeriodeKI(DateEval)

                If SiDRH = True Then
                    Return True
                End If
                If NumObjet = 153 Then
                    Return True
                End If
                Select Case Periode_KI
                    Case "DEBUT KI", "MILIEU KI", "FIN KI - SELF"
                        Select Case NumObjet
                            Case 150
                                Select Case NumInfo
                                    Case 4 To 9
                                        Return True
                                    Case 11 To 15
                                        Return True
                                    Case 22, 27, 28, 29
                                        Return True
                                    Case Else
                                        Return False
                                End Select
                            Case 151
                                Select Case NumInfo
                                    Case 2 To 12
                                        Return True
                                    Case Else
                                        Return True
                                End Select
                            Case 152
                                Select Case NumInfo
                                    Case 1, 3, 4
                                        Return True
                                    Case Else
                                        Return False
                                End Select
                        End Select
                    Case "FIN KI - MANAGER"
                        Select Case SiSelf
                            Case True
                                Return False
                            Case False
                                Select Case NumObjet
                                    Case 150
                                        Select Case NumInfo
                                            Case 4 To 9
                                                Return True
                                            Case 11 To 15
                                                Return True
                                            Case 19, 22, 27, 28, 29
                                                Return True
                                            Case Else
                                                Return False
                                        End Select
                                    Case 151
                                        Return True
                                    Case 152
                                        Return True
                                End Select
                        End Select
                    Case "FIN KI - DRH"
                        Return False
                    Case "FIN KI"
                        If NumObjet = 150 And NumInfo = 26 Then
                            Return False
                        Else
                            Return True
                        End If
                End Select

                Return True
            End Get
        End Property

        Public ReadOnly Property SiReadonly(ByVal NumObjet As Integer, ByVal NumInfo As Integer, ByVal DateEval As String) As Boolean
            Get
                If WsObjet_150 Is Nothing Then
                    Return True
                End If
                If WsDateSignature_Self <> "" And SiDRH = False Then
                    Return True
                End If
                If SiDRH = True Then
                    Select Case NumObjet
                        Case 150
                            Select Case NumInfo
                                Case 4 To 9
                                    Return False
                                Case 26
                                    Return False
                                Case Else
                                    Return True
                            End Select
                        Case 153
                            Select Case NumInfo
                                Case 12 To 15
                                    Return False
                                Case Else
                                    Return True
                            End Select
                    End Select
                    Return True
                End If
                If NumObjet = 153 Then
                    Select Case SiSelf
                        Case True
                            Return True
                        Case False
                            Select Case NumInfo
                                Case 14, 15
                                    Return True
                                Case Else
                                    Return False
                            End Select
                    End Select
                End If
                Dim Periode_KI As String = PeriodeKI(DateEval)
                Select Case Periode_KI
                    Case "DEBUT KI"
                        Select Case SiSelf
                            Case True
                                Select Case NumObjet
                                    Case 150
                                        Select Case NumInfo
                                            Case 7, 28
                                                If WsObjet_150.Date_de_Signature_Evalue_KI1 = "" Then
                                                    Return False
                                                Else
                                                    Return True
                                                End If
                                            Case Else
                                                Return True
                                        End Select
                                    Case 151
                                        Return True
                                    Case 152
                                        Return True
                                End Select
                            Case False
                                If WsObjet_150.Date_de_Signature_Manager_KI1 <> "" Then
                                    Return True
                                End If
                                Select Case NumObjet
                                    Case 150
                                        Select Case NumInfo
                                            Case 4, 11, 12, 13, 14, 15, 22, 27, 29
                                                Return False
                                            Case Else
                                                Return True
                                        End Select
                                    Case 151
                                        Select Case NumInfo
                                            Case 2, 3, 5, 9
                                                Return False
                                            Case Else
                                                Return True
                                        End Select
                                    Case 152
                                        Return True
                                End Select
                        End Select
                    Case "MILIEU KI"
                        Select Case SiSelf
                            Case True
                                Select Case NumObjet
                                    Case 150
                                        Select Case NumInfo
                                            Case 8, 28
                                                If WsObjet_150.Date_de_Signature_Evalue_KI2 = "" Then
                                                    Return False
                                                Else
                                                    Return True
                                                End If
                                            Case Else
                                                Return True
                                        End Select
                                    Case 151
                                        Return True
                                    Case 152
                                        Return True
                                End Select
                            Case False
                                If WsObjet_150.Date_de_Signature_Manager_KI2 <> "" Then
                                    Return True
                                End If
                                Select Case NumObjet
                                    Case 150
                                        Select Case NumInfo
                                            Case 5, 11, 12, 13, 14, 15, 22, 27, 29
                                                Return False
                                            Case Else
                                                Return True
                                        End Select
                                    Case 151
                                        Select Case NumInfo
                                            Case 2, 3, 5, 9
                                                Return False
                                            Case Else
                                                Return True
                                        End Select
                                    Case 152
                                        Return True
                                End Select
                        End Select
                    Case "FIN KI - SELF"
                        Select Case SiSelf
                            Case True
                                Select Case NumObjet
                                    Case 150
                                        Return True
                                    Case 151
                                        Return True
                                    Case 152
                                        Select Case NumInfo
                                            Case 4
                                                Return False
                                            Case Else
                                                Return True
                                        End Select
                                End Select
                            Case False
                                Return True
                        End Select
                    Case "FIN KI - MANAGER"
                        Select Case SiSelf
                            Case True
                                Return True
                            Case False
                                Select Case NumObjet
                                    Case 150
                                        Select Case NumInfo
                                            Case 15, 19, 22, 27, 29
                                                Return False
                                            Case Else
                                                Return True
                                        End Select
                                    Case 151
                                        Select Case NumInfo
                                            Case 4, 6, 7, 10, 11
                                                Return False
                                            Case Else
                                                Return True
                                        End Select
                                    Case 152
                                        Select Case NumInfo
                                            Case 3
                                                Return False
                                            Case Else
                                                Return True
                                        End Select
                                End Select
                        End Select
                    Case "FIN KI - DRH"
                        Return True
                    Case "FIN KI"
                        Select Case SiSelf
                            Case True
                                Select Case NumObjet
                                    Case 150
                                        Select Case NumInfo
                                            Case 9, 10, 28, 30
                                                If WsObjet_150.Date_de_Signature_Evalue_KI3 = "" Then
                                                    Return False
                                                Else
                                                    Return True
                                                End If
                                            Case Else
                                                Return True
                                        End Select
                                    Case 151
                                        Return True
                                    Case 152
                                        Return True
                                End Select
                            Case False
                                If WsObjet_150.Date_de_Signature_Manager_KI3 <> "" Then
                                    Return True
                                End If
                                Select Case NumObjet
                                    Case 150
                                        Select Case NumInfo
                                            Case 3, 6, 15, 19, 22, 27, 29, 31
                                                Return False
                                            Case Else
                                                Return True
                                        End Select
                                    Case 151
                                        Select Case NumInfo
                                            Case 4, 6, 7, 10, 11
                                                Return False
                                            Case Else
                                                Return True
                                        End Select
                                    Case 152
                                        Select Case NumInfo
                                            Case 3
                                                Return False
                                            Case Else
                                                Return True
                                        End Select
                                End Select
                        End Select
                End Select

                Return False
            End Get
        End Property

        Public ReadOnly Property SiEntretienAccessible() As Boolean
            Get
                If WsSiDRH = True Then
                    Return True
                End If
                Dim Periode_KI As String = PeriodeKI(WsObjet_150.Date_de_Valeur)
                Select Case Periode_KI
                    Case "DEBUT KI"
                        Select Case SiSelf
                            Case True
                                If WsObjet_150.Date_de_Signature_Manager_KI1 = "" Then
                                    Return False
                                Else
                                    Return True
                                End If
                            Case False
                                Return True
                        End Select
                    Case "MILIEU KI"
                        Select Case SiSelf
                            Case True
                                If WsObjet_150.Date_de_Signature_Manager_KI2 = "" Then
                                    Return False
                                Else
                                    Return True
                                End If
                            Case False
                                Return True
                        End Select
                    Case "FIN KI - MANAGER"
                        Select Case SiSelf
                            Case True
                                Return False
                            Case False
                                Return True
                        End Select
                    Case "FIN KI - DRH"
                        Return False
                    Case "FIN KI"
                        Select Case SiSelf
                            Case True
                                If WsObjet_150.Date_de_Signature_Manager_KI3 = "" Then
                                    Return False
                                Else
                                    Return True
                                End If
                            Case False
                                Return True
                        End Select
                End Select
                Return True
            End Get
        End Property

        Public Property Annee As String
            Get
                Return WsAnnee
            End Get
            Set(ByVal value As String)
                WsAnnee = value
                WsObjet_150 = Objet_150
                If WsObjet_150 Is Nothing Then
                    WsObjet_150 = CreationNouvelEntretien
                End If
                WsDateSignature_Self = WsObjet_150.Date_de_Signature_Evalue_KI3
                WsObjet_150_Lu = New Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL
                WsObjet_150_Lu.ContenuTable = WsObjet_150.Ide_Dossier & VI.Tild & WsObjet_150.ContenuTable
                Call FaireListe_151()
                Call FaireListe_152()
                Call FaireListe_153()
            End Set
        End Property

        Public ReadOnly Property AnneePrecedente As String
            Get
                Return CStr(CInt(WsAnnee) - 1)
            End Get
        End Property

        Private ReadOnly Property CreationNouvelEntretien As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL
            Get
                Dim IndiceI As Integer
                Dim ObjetFiche As Object

                Call InitialiserManager(WsPointeurGlobal.VirRhDates.DateduJour)

                If WsListeEntretiens Is Nothing Then
                    WsListeEntretiens = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                End If

                WsObjet_150 = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(150, 0)
                WsObjet_150.Date_de_Valeur = "01/04/" & Year(Now)
                If SiEst_Aussi_Un_Manager = True Then
                    WsObjet_150.Type_Formulaire = "Manager"
                Else
                    WsObjet_150.Type_Formulaire = "Collaborateur non manager"
                End If
                WsObjet_150.Identifiant_Manager = WsIdeManager
                WsObjet_150.KI = KI("01/04/" & Year(Now)).ToString
                WsListeEntretiens.Add(WsObjet_150)

                If SiEst_Aussi_Un_Manager = True Then
                    For IndiceI = 1 To 9
                        If IndiceI <> 5 Then
                            ObjetFiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(151, 0)
                            CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE).Date_de_Valeur = "01/04/" & Year(Now)
                            CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE).Numero = IndiceI
                            WsListeEntretiens.Add(CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE))
                        End If
                    Next IndiceI
                Else
                    For IndiceI = 1 To 5
                        ObjetFiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(151, 0)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE).Date_de_Valeur = "01/04/" & Year(Now)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE).Numero = IndiceI
                        WsListeEntretiens.Add(CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE))
                    Next IndiceI
                End If

                For IndiceI = 1 To 10
                    ObjetFiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(152, 0)
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_COMPORTEMENT).Date_de_Valeur = "01/04/" & Year(Now)
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_COMPORTEMENT).Numero_Tri = IndiceI
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_COMPORTEMENT).Intitule = Evaluation_Element(IndiceI)
                    WsListeEntretiens.Add(CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_COMPORTEMENT))
                Next IndiceI

                For IndiceI = 1 To 3
                    ObjetFiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(153, 0)
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_FORMATION).Date_de_Valeur = "01/04/" & Year(Now)
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_FORMATION).NumeroTri = IndiceI
                    WsListeEntretiens.Add(CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_FORMATION))
                Next IndiceI

                Return WsObjet_150
            End Get
        End Property

        Public Function MettreAJour() As Boolean
            Dim SiOK As Boolean
            Dim ChaineLue As String = ""
            Dim ChaineMaj As String
            Dim CodeMaj As String = ""
            Dim IndiceF As Integer

            If WsObjet_150 Is Nothing Then
                Return False
            End If
            If WsDateSignature_Self <> "" And WsSiDRH = False Then
                Return False
            End If
            ChaineLue = ""
            CodeMaj = "C"
            If WsObjet_150.Ide_Dossier = WsIdentifiant Then
                ChaineLue = WsObjet_150.FicheLue
                CodeMaj = "M"
            End If

            Call InitialiserManager(WsPointeurGlobal.VirRhDates.DateduJour)
            WsObjet_150.Identifiant_Manager = WsIdeManager

            If WsObjet_150.Type_Formulaire = "" Then
                If SiEst_Aussi_Un_Manager = True Then
                    WsObjet_150.Type_Formulaire = "Manager"
                Else
                    WsObjet_150.Type_Formulaire = "Collaborateur non manager"
                End If
            End If
            If WsObjet_150.KI = "" Then
                WsObjet_150.KI = KI(WsObjet_150.Date_de_Valeur).ToString
            End If
            ChaineMaj = WsObjet_150.ContenuTable
            If ChaineMaj <> ChaineLue Then
                SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 150, WsIdentifiant, CodeMaj,
                                                                          ChaineLue, ChaineMaj)
                If SiOK = False Then
                    Return SiOK
                End If
            End If
            WsObjet_150.ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
            Call Envoyer_Eventuellement_Email()

            For IndiceF = 0 To WsLst_151.Count - 1
                ChaineLue = ""
                CodeMaj = "C"
                If WsLst_151.Item(IndiceF).Intitule_Objectif <> "" Then
                    If WsLst_151.Item(IndiceF).Ide_Dossier = WsIdentifiant Then
                        ChaineLue = WsLst_151.Item(IndiceF).FicheLue
                        CodeMaj = "M"
                    End If
                    WsLst_151.Item(IndiceF).Date_de_Valeur = WsObjet_150.Date_de_Valeur
                    ChaineMaj = WsLst_151.Item(IndiceF).ContenuTable
                    If ChaineMaj <> ChaineLue Then
                        SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 151, WsIdentifiant, CodeMaj,
                                                                                  ChaineLue, ChaineMaj)
                        If SiOK Then
                            WsLst_151.Item(IndiceF).ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
                        End If
                    End If
                ElseIf WsLst_151.Item(IndiceF).Ide_Dossier = WsIdentifiant Then
                    SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 151, WsIdentifiant, "S",
                                                                         WsLst_151.Item(IndiceF).FicheLue, "")
                End If
            Next IndiceF

            For IndiceF = 0 To WsLst_152.Count - 1
                ChaineLue = ""
                CodeMaj = "C"
                If WsLst_152.Item(IndiceF).Intitule <> "" Then
                    If WsLst_152.Item(IndiceF).Ide_Dossier = WsIdentifiant Then
                        ChaineLue = WsLst_152.Item(IndiceF).FicheLue
                        CodeMaj = "M"
                    End If
                    WsLst_152.Item(IndiceF).Date_de_Valeur = WsObjet_150.Date_de_Valeur
                    ChaineMaj = WsLst_152.Item(IndiceF).ContenuTable
                    If ChaineMaj <> ChaineLue Then
                        SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 152, WsIdentifiant, CodeMaj,
                                                                                  ChaineLue, ChaineMaj)
                        If SiOK Then
                            WsLst_152.Item(IndiceF).ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
                        End If
                    End If
                    If WsLst_152.Count > 10 Then
                        If Evaluation_Element(WsLst_152.Item(IndiceF).Numero_Tri) <> WsLst_152.Item(IndiceF).Intitule Then
                            SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 152, WsIdentifiant, "S",
                                                                              WsLst_152.Item(IndiceF).FicheLue, "")
                        End If
                    End If
                ElseIf WsLst_152.Item(IndiceF).Ide_Dossier = WsIdentifiant Then
                    SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 152, WsIdentifiant, "S",
                                                                          WsLst_152.Item(IndiceF).FicheLue, "")
                End If
            Next IndiceF

            For IndiceF = 0 To WsLst_153.Count - 1
                ChaineLue = ""
                CodeMaj = "C"
                If WsLst_153.Item(IndiceF).Intitule_Besoin <> "" Then
                    If WsLst_153.Item(IndiceF).Ide_Dossier = WsIdentifiant Then
                        ChaineLue = WsLst_153.Item(IndiceF).FicheLue
                        CodeMaj = "M"
                    End If
                    WsLst_153.Item(IndiceF).Date_de_Valeur = WsObjet_150.Date_de_Valeur
                    ChaineMaj = WsLst_153.Item(IndiceF).ContenuTable
                    If ChaineMaj <> ChaineLue Then
                        SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 153, WsIdentifiant, CodeMaj,
                                                                                  ChaineLue, ChaineMaj)
                        If SiOK Then
                            WsLst_153.Item(IndiceF).ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
                        End If
                    End If
                ElseIf WsLst_153.Item(IndiceF).Ide_Dossier = WsIdentifiant Then
                    SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 153, WsIdentifiant, "S",
                                                                         WsLst_153.Item(IndiceF).FicheLue, "")
                End If
            Next IndiceF

            Return SiOK
        End Function

        Public ReadOnly Property Objet_150() As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    For Each Virfiche In LstFiches
                        If Strings.Right(Virfiche.Date_de_Valeur, 4) = WsAnnee Then
                            Return CType(Virfiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL)
                        End If
                    Next
                End If
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Objet_150(ByVal ArgumentAnnee As String) As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    For Each Virfiche In LstFiches
                        If Strings.Right(Virfiche.Date_de_Valeur, 4) = ArgumentAnnee Then
                            Return CType(Virfiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL)
                        End If
                    Next
                End If
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Objet_151(ByVal Rang As String) As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE
            Get
                If WsLst_151 Is Nothing Then
                    Return Nothing
                End If
                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE In WsLst_151
                    If FichePER.Numero.ToString = Rang Then
                        Return FichePER
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Objet_152(ByVal Rang As String) As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_COMPORTEMENT
            Get
                If WsLst_152 Is Nothing Then
                    Return Nothing
                End If
                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_COMPORTEMENT In WsLst_152
                    If FichePER.Numero_Tri.ToString = Rang Then
                        Return FichePER
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Objet_153(ByVal Rang As String) As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_FORMATION
            Get
                If WsLst_153 Is Nothing Then
                    Return Nothing
                End If
                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_FORMATION In WsLst_153
                    If FichePER.NumeroTri.ToString = Rang Then
                        Return FichePER
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property ListeObjet151() As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE)
            Get
                Return (From Fiche In WsLst_151 Order By Fiche.Numero).ToList
            End Get
        End Property

        Public ReadOnly Property ListeObjet152() As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_COMPORTEMENT)
            Get
                Return (From Fiche In WsLst_152 Order By Fiche.Numero_Tri).ToList
            End Get
        End Property

        Public ReadOnly Property ListeObjet153() As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_FORMATION)
            Get
                If WsLst_153 Is Nothing Then
                    Return Nothing
                Else
                    Return (From Fiche In WsLst_153 Order By Fiche.NumeroTri).ToList
                End If
            End Get
        End Property

        Private Sub FaireListe_151()
            Dim IndiceF As Integer
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE
            Dim DateEffet As String = WsObjet_150.Date_de_Valeur

            LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 151 Order By instance.Date_Valeur_ToDate Descending).ToList

            WsLst_151 = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE)
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each VirFiche In LstFiches
                    If VirFiche.Date_Valeur_ToDate >= DateValue("01/04/2014") Then
                        If Strings.Right(VirFiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                            WsLst_151.Add(CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE))
                            DateEffet = VirFiche.Date_de_Valeur
                        End If
                    End If
                Next
            End If
            '** Ajout des fiches manquantes
            If SiEst_Aussi_Un_Manager = True Then
                If WsLst_151.Count = 8 Then
                    Exit Sub
                End If
                For IndiceF = 1 To 9
                    If IndiceF <> 5 Then
                        If Objet_151(IndiceF) Is Nothing Then
                            Fiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(151, 0)
                            Fiche.Date_de_Valeur = DateEffet
                            Fiche.Numero = IndiceF
                            WsListeEntretiens.Add(Fiche)
                            WsLst_151.Add(Fiche)
                        End If
                    End If
                Next IndiceF
            Else
                If WsLst_151.Count = 5 Then
                    Exit Sub
                End If
                For IndiceF = 1 To 5
                    If Objet_151(IndiceF) Is Nothing Then
                        Fiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(151, 0)
                        Fiche.Date_de_Valeur = DateEffet
                        Fiche.Numero = IndiceF
                        WsListeEntretiens.Add(Fiche)
                        WsLst_151.Add(Fiche)
                    End If
                Next IndiceF
            End If
        End Sub

        Private Sub Envoyer_Eventuellement_Email()
            Dim SiEmailAfaire As Boolean = False
            Dim Emetteur As String = ""
            Dim Destinataire As String = ""
            Dim DateEditee As String = ""
            Dim MsgContenu As String = ""

            If WsObjet_150_Lu Is Nothing Then
                Exit Sub
            End If

            If WsObjet_150.Date_de_Signature_Evalue_KI1 <> "" AndAlso WsObjet_150.Date_de_Signature_Evalue_KI1 <> WsObjet_150_Lu.Date_de_Signature_Evalue_KI1 Then
                SiEmailAfaire = True
                Emetteur = Adresse_Mail
                Destinataire = WsEmailManager
                DateEditee = WsObjet_150.Date_de_Signature_Evalue_KI1
            End If
            If WsObjet_150.Date_de_Signature_Evalue_KI2 <> "" AndAlso WsObjet_150.Date_de_Signature_Evalue_KI2 <> WsObjet_150_Lu.Date_de_Signature_Evalue_KI2 Then
                SiEmailAfaire = True
                Emetteur = Adresse_Mail
                Destinataire = WsEmailManager
                DateEditee = WsObjet_150.Date_de_Signature_Evalue_KI2
            End If
            If WsObjet_150.Date_de_Signature_Evalue_KI3 <> "" AndAlso WsObjet_150.Date_de_Signature_Evalue_KI3 <> WsObjet_150_Lu.Date_de_Signature_Evalue_KI3 Then
                SiEmailAfaire = True
                Emetteur = Adresse_Mail
                Destinataire = WsEmailManager
                DateEditee = WsObjet_150.Date_de_Signature_Evalue_KI3
            End If
            If WsObjet_150.Date_de_Signature_Manager_KI1 <> "" AndAlso WsObjet_150.Date_de_Signature_Manager_KI1 <> WsObjet_150_Lu.Date_de_Signature_Manager_KI1 Then
                SiEmailAfaire = True
                Emetteur = WsEmailManager
                Destinataire = Adresse_Mail
                DateEditee = WsObjet_150.Date_de_Signature_Manager_KI1
            End If
            If WsObjet_150.Date_de_Signature_Manager_KI2 <> "" AndAlso WsObjet_150.Date_de_Signature_Manager_KI2 <> WsObjet_150_Lu.Date_de_Signature_Manager_KI2 Then
                SiEmailAfaire = True
                Emetteur = WsEmailManager
                Destinataire = Adresse_Mail
                DateEditee = WsObjet_150.Date_de_Signature_Manager_KI2
            End If
            If WsObjet_150.Date_de_Signature_Manager_KI3 <> "" AndAlso WsObjet_150.Date_de_Signature_Manager_KI3 <> WsObjet_150_Lu.Date_de_Signature_Manager_KI3 Then
                SiEmailAfaire = True
                Emetteur = WsEmailManager
                Destinataire = Adresse_Mail
                DateEditee = WsObjet_150.Date_de_Signature_Manager_KI3
            End If
            If WsObjet_150.SiRefus_Signature = "Oui" And WsObjet_150_Lu.SiRefus_Signature <> "Oui" Then
                SiEmailAfaire = True
                Emetteur = WsEmailManager
                Destinataire = Adresse_Mail
                DateEditee = WsPointeurGlobal.VirRhDates.DateduJour
                MsgContenu = "Le " & WsPointeurGlobal.VirRhDates.DateduJour & ", " & Nom & Strings.Space(1) & Prenom & " a refusé de signer l'entretien d'évaluation."
            End If
            If SiEmailAfaire = False Then
                Exit Sub
            End If
            If Destinataire = "" OrElse Emetteur = "" Then
                Exit Sub
            End If
            If MsgContenu = "" Then
                MsgContenu = PeriodeKI(WsPointeurGlobal.VirRhDates.DateduJour) & ". " & vbCrLf
                MsgContenu &= "L'entretien de " & Nom & Strings.Space(1) & Prenom
                MsgContenu &= " a été signé : date de signature le " & DateEditee
            End If
            Dim WseOutil As New Virtualia.Net.WebService.Outils(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaUtils"))
            Try
                SiEmailAfaire = WseOutil.EnvoyerEmail(Emetteur, Destinataire, MsgContenu, False)
            Catch ex As Exception
                Exit Try
            End Try
        End Sub

        Private Sub FaireListe_152()
            Dim IndiceF As Integer
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_COMPORTEMENT
            Dim DateEffet As String = WsObjet_150.Date_de_Valeur

            LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 152 Order By instance.Date_Valeur_ToDate Descending).ToList

            WsLst_152 = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_COMPORTEMENT)
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each VirFiche In LstFiches
                    If VirFiche.Date_Valeur_ToDate >= DateValue("01/04/2014") Then
                        If Strings.Right(VirFiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                            WsLst_152.Add(CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_COMPORTEMENT))
                            DateEffet = VirFiche.Date_de_Valeur
                        End If
                    End If
                Next
            End If
            '** Ajout des fiches manquantes
            If WsLst_152.Count = 10 Then
                Exit Sub
            End If
            For IndiceF = 1 To 10
                If Objet_152(IndiceF) Is Nothing Then
                    Fiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(152, 0)
                    Fiche.Date_de_Valeur = DateEffet
                    Fiche.Numero_Tri = IndiceF
                    Fiche.Intitule = Evaluation_Element(IndiceF)
                    WsListeEntretiens.Add(Fiche)
                    WsLst_152.Add(Fiche)
                End If
            Next IndiceF
        End Sub

        Private Sub FaireListe_153()
            Dim IndiceF As Integer
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_FORMATION
            Dim DateEffet As String = WsObjet_150.Date_de_Valeur

            LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 153 Order By instance.Date_Valeur_ToDate Descending).ToList

            WsLst_153 = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_FORMATION)
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each VirFiche In LstFiches
                    If VirFiche.Date_Valeur_ToDate >= DateValue("01/04/2014") Then
                        If Strings.Right(VirFiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                            WsLst_153.Add(CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_FORMATION))
                            DateEffet = VirFiche.Date_de_Valeur
                        End If
                    End If
                Next
            End If
            '** Ajout des fiches manquantes
            If WsLst_153.Count = 3 Then
                Exit Sub
            End If
            For IndiceF = 1 To 3
                If Objet_153(IndiceF) Is Nothing Then
                    Fiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(153, 0)
                    Fiche.Date_de_Valeur = DateEffet
                    Fiche.NumeroTri = IndiceF
                    Fiche.Intitule_Besoin = ""
                    WsListeEntretiens.Add(Fiche)
                    WsLst_153.Add(Fiche)
                End If
            Next IndiceF
        End Sub

        Public ReadOnly Property ListeDesEntretiens As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL
                Dim Chaine As New System.Text.StringBuilder

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Ascending).ToList
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    For Each Virfiche In LstFiches
                        Fiche = CType(Virfiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL)
                        If Fiche.Date_Valeur_ToDate >= DateValue("01/04/2014") Then
                            Chaine.Append(Strings.Right(Fiche.Date_de_Valeur, 4) & " - " & CInt(Right(Fiche.Date_de_Valeur, 4)) + 1 & VI.Tild)
                            Chaine.Append(Fiche.Type_Formulaire & VI.Tild)
                            Chaine.Append(Fiche.KI & VI.Tild)
                            Chaine.Append(Fiche.Date_de_Valeur & VI.SigneBarre)
                        End If
                    Next
                End If
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property ListeDesFormationsSuivies(ByVal Annee As String, ByVal Profondeur As Integer) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim Chaine As System.Text.StringBuilder
                Dim DateEffet As String = "31/12/" & Annee
                Dim DateProfondeur As String = "01/01/" & Annee - Profondeur
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_SESSION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaFormation _
                                        And instance.Date_Valeur_ToDate >= CDate(DateProfondeur) _
                                        Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    Return ""
                End If
                Chaine = New System.Text.StringBuilder
                For Each VirFiche In LstFiches
                    Fiche = CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_SESSION)
                    Select Case WsPointeurGlobal.VirRhDates.ComparerDates(Fiche.Date_de_Valeur, DateEffet)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                            Select Case WsPointeurGlobal.VirRhDates.ComparerDates(Fiche.Date_de_Valeur, DateProfondeur)
                                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                    Chaine.Append(Fiche.Date_de_Valeur & VI.Tild & Fiche.Intitule & VI.Tild & Fiche.Duree_en_heures & " heures" & VI.Tild)
                                    Chaine.Append(Fiche.Date_de_Valeur & VI.SigneBarre)
                            End Select
                    End Select
                Next
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property LibelleIdentite As String
            Get
                If WsFicheEtatCivil Is Nothing Then
                    Return ""
                    Exit Property
                End If
                Dim Chaine As String
                Chaine = WsFicheEtatCivil.Nom & Strings.Space(1)
                Chaine &= WsFicheEtatCivil.Prenom & Strings.Space(1)
                Return Chaine
            End Get
        End Property

        Public ReadOnly Property Note_Ligne_Performance(ByVal Rang As String, ByVal SiCollaborateur As Boolean) As Integer
            Get
                Dim ZSomme As Double = 0
                Dim Ligne As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_PERFORMANCE = Objet_151(Rang)
                If Ligne Is Nothing Then
                    Return 0
                End If
                If SiCollaborateur = True Then
                    ZSomme = Ligne.Resultat_Atteint_Self
                    If Ligne.SiDifficile_Self = "Y" Then
                        Select Case Objet_150.Type_Formulaire
                            Case "Manager"
                                ZSomme += 1
                            Case Else
                                ZSomme += 15
                        End Select
                    End If
                Else
                    ZSomme = Ligne.Resultat_Atteint_Manager
                    If Ligne.SiDifficile_Manager = "Y" Then
                        Select Case Objet_150.Type_Formulaire
                            Case "Manager"
                                ZSomme += 1
                            Case Else
                                ZSomme += 15
                        End Select
                    End If
                End If
                Select Case Objet_150.Type_Formulaire
                    Case "Manager"
                        If ZSomme > 5 Then
                            ZSomme = 5
                        End If
                    Case Else
                        If ZSomme > 60 Then
                            ZSomme = 60
                        End If
                End Select
                If SiCollaborateur = True Then
                    Ligne.Note_Finale_Self = ZSomme
                Else
                    Ligne.Note_Finale_Manager = ZSomme
                End If
                Return ZSomme
            End Get
        End Property

        Public ReadOnly Property Total_Performance(ByVal SiCollaborateur As Boolean) As Double
            Get
                Dim ZSomme As Double = 0
                Dim ZLigne As Double = 0
                Dim IndiceI As Integer
                For IndiceI = 0 To WsLst_151.Count - 1
                    ZLigne = Note_Ligne_Performance(WsLst_151.Item(IndiceI).Numero, SiCollaborateur)
                    If SiCollaborateur = True Then
                        ZSomme += ZLigne * (WsLst_151.Item(IndiceI).Poids_Self / 100)
                    Else
                        ZSomme += ZLigne * (WsLst_151.Item(IndiceI).Poids_Manager / 100)
                    End If
                Next IndiceI
                Select Case Objet_150.Type_Formulaire
                    Case "Manager"
                        If ZSomme > 5 Then
                            ZSomme = 5
                        End If
                    Case Else
                        If ZSomme > 60 Then
                            ZSomme = 60
                        End If
                End Select
                If SiCollaborateur = True Then
                    Objet_150.Note_Performance_Self = ZSomme
                Else
                    Objet_150.Note_Performance_Manager = ZSomme
                End If
                Return ZSomme
            End Get
        End Property

        Public ReadOnly Property Total_Comportement(ByVal SiCollaborateur As Boolean) As Double
            Get
                Dim ZSomme As Double = 0
                Dim IndiceI As Integer
                For IndiceI = 0 To WsLst_152.Count - 1
                    If SiCollaborateur = True Then
                        ZSomme += WsLst_152.Item(IndiceI).Note_Self
                    Else
                        ZSomme += WsLst_152.Item(IndiceI).Note_Manager
                    End If
                Next IndiceI
                Select Case Objet_150.Type_Formulaire
                    Case "Manager"
                        If SiCollaborateur = True Then
                            Objet_150.Note_Comportement_Self = ZSomme / 10
                        Else
                            Objet_150.Note_Comportement_Manager = ZSomme / 10
                        End If
                        Return ZSomme / 10
                    Case Else
                        If SiCollaborateur = True Then
                            Objet_150.Note_Comportement_Self = ZSomme
                        Else
                            Objet_150.Note_Comportement_Manager = ZSomme
                        End If
                        Return ZSomme
                End Select
                Return 0
            End Get
        End Property

        Public ReadOnly Property Note_Globale(ByVal SiCollaborateur As Boolean) As String
            Get
                Dim ZSomme As Double = 0
                ZSomme = Total_Performance(SiCollaborateur) + Total_Comportement(SiCollaborateur)
                If SiCollaborateur = True Then
                    Objet_150.Note_Globale_Self = ZSomme
                Else
                    Objet_150.Note_Globale_Manager = ZSomme
                End If
                Return Strings.Format(ZSomme, "0.00")
            End Get
        End Property

        Public ReadOnly Property Total_Poids(ByVal SiCollaborateur As Boolean) As Integer
            Get
                Dim ZSomme As Integer = 0
                Dim IndiceI As Integer
                For IndiceI = 0 To WsLst_151.Count - 1
                    If SiCollaborateur = True Then
                        ZSomme += WsLst_151.Item(IndiceI).Poids_Self
                    Else
                        ZSomme += WsLst_151.Item(IndiceI).Poids_Manager
                    End If
                Next IndiceI
                Return ZSomme
            End Get
        End Property

        Public ReadOnly Property Note_Globale_Lettre As String
            Get
                Dim NoteLettre As String = ""
                Select Case Objet_150.Type_Formulaire
                    Case "Manager"
                        Select Case WsPointeurGlobal.VirRhFonction.ConversionDouble(Note_Globale(False))
                            Case Is < 1.99
                                NoteLettre = "C"
                            Case 2 To 2.99
                                NoteLettre = "B-"
                            Case 3 To 3.99
                                NoteLettre = "B"
                            Case 4 To 4.49
                                NoteLettre = "B+"
                            Case 4.5 To 4.99
                                NoteLettre = "A"
                            Case Is > 5
                                NoteLettre = "A*"
                        End Select
                    Case Else
                        Select Case WsPointeurGlobal.VirRhFonction.ConversionDouble(Note_Globale(False))
                            Case 0 To 20
                                NoteLettre = "C"
                            Case 21 To 40
                                NoteLettre = "B-"
                            Case 41 To 60
                                NoteLettre = "B"
                            Case 61 To 80
                                NoteLettre = "B+"
                            Case 81 To 90
                                NoteLettre = "A"
                            Case 91 To 100
                                NoteLettre = "A*"
                        End Select
                End Select
                Objet_150.Note_Lettre_Manager = NoteLettre
                Return NoteLettre
            End Get
        End Property

        Public ReadOnly Property Evaluation_Commentaire(ByVal Rang_NoTri As String) As String
            Get
                If IsNumeric(Rang_NoTri) = False Then
                    Return ""
                End If
                Dim Chaine As New System.Text.StringBuilder
                Select Case CInt(Rang_NoTri)
                    Case 1
                        Chaine.Append("Non satisfait de la situation actuelle, fait face et atteint des objectifs ambitieux.")
                        Chaine.Append(Strings.Space(2))
                        Chaine.Append("Affronte les problèmes avec un haut niveau d'imagination, de conscience et de sensibilité")
                        Return Chaine.ToString
                    Case 2
                        Chaine.Append("Tient compte des 3 principes de réalité dans la prise de décision :")
                        Chaine.Append(Strings.Space(2))
                        Chaine.Append("Genba (se rendre sur place);")
                        Chaine.Append(Strings.Space(2))
                        Chaine.Append("Genbutsu (connaître la situation);")
                        Chaine.Append(Strings.Space(2))
                        Chaine.Append("Genjitsu (être réaliste).")
                        Return Chaine.ToString
                    Case 3
                        Return "La compréhension du client guide la décision."
                    Case 4
                        Chaine.Append("Décide et communique de manière ouverte et transparente, en observant la loi et en respectant un code de conduite.")
                        Return Chaine.ToString
                    Case 5
                        Chaine.Append("Identifie les problèmes réels et potentiels au travers de l'analyse des situations, des données et autres informations.")
                        Chaine.Append(Strings.Space(2))
                        Chaine.Append("Détecte  les causes et définit les contre-mesures à adopter.")
                        Return Chaine.ToString
                    Case 6
                        Select Case Objet_150.Type_Formulaire
                            Case "Manager"
                                Chaine.Append("Encourage les initiatives des collaborateurs tout en respectant leur état d'esprit  et leur enthousiasme.")
                            Case Else
                                Chaine.Append("Coopère avec les autres collaborateurs, partage ses expériences et expertise;")
                                Chaine.Append(Strings.Space(2))
                                Chaine.Append("communique les informations pouvant se révéler utiles à l'équipe.")
                        End Select
                        Return Chaine.ToString
                    Case 7
                        Select Case Objet_150.Type_Formulaire
                            Case "Manager"
                                Return "Offre des chances égales  correspondant aux aptitudes et  dynamisme individuels."
                            Case Else
                                Chaine.Append("Développe ses compétences et ses connaissances sur une base volontaire et")
                                Chaine.Append(Strings.Space(2))
                                Chaine.Append("se prépare à faire face à de nouvelles responsabilités.")
                                Return Chaine.ToString
                        End Select
                    Case 8
                        Select Case Objet_150.Type_Formulaire
                            Case "Manager"
                                Return "Les relations professionnelles sont basées sur la confiance mutuelle."
                            Case Else
                                Return "Effectue son travail avec une vision étendue de la situation."
                        End Select
                    Case 9
                        Select Case Objet_150.Type_Formulaire
                            Case "Manager"
                                Chaine.Append("A une vision / rêve dans son milieu de travail  et vise à l'accomplir.")
                                Chaine.Append(Strings.Space(2))
                                Chaine.Append("Cette vision correspond à celle de la société.")
                                Return Chaine.ToString
                            Case Else
                                Return "Aborde les problèmes avec un esprit créatif et logique, grâce à une analyse pertinente des situations."
                        End Select
                    Case 10
                        Select Case Objet_150.Type_Formulaire
                            Case "Manager"
                                Chaine.Append("Prend les mesures avec simplicité, concentration et rapidité, en tirant le meilleur parti de son temps.")
                            Case Else
                                Chaine.Append("Effectue son travail en fonction des objectifs de la société.")
                                Chaine.Append(Strings.Space(2))
                                Chaine.Append("Vise à rapprocher les points de vue et à améliorer la communication entre les départements.")
                                Chaine.Append(Strings.Space(2))
                                Chaine.Append("N'hésite pas à prendre des responsabilités en dehors de ses strictes attributions.")
                        End Select
                        Return Chaine.ToString
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property Evaluation_Element(ByVal Rang_NoTri As String) As String
            Get
                If IsNumeric(Rang_NoTri) = False Then
                    Return ""
                End If
                Select Case CInt(Rang_NoTri)
                    Case 1
                        Return "Challenge"
                    Case 2
                        Return "Trois principes de réalité"
                    Case 3
                        Return "Orienté client"
                    Case 4
                        Return "Honnêteté"
                    Case 5
                        Return "Identifie les problèmes et définit les solutions"
                    Case 6
                        If Objet_150 Is Nothing Then
                            Return "Travail en équipe"
                        Else
                            Select Case Objet_150.Type_Formulaire
                                Case "Manager"
                                    Return "Respecte les individus (et leurs initiatives)"
                                Case Else
                                    Return "Travail en équipe"
                            End Select
                        End If
                    Case 7
                        If Objet_150 Is Nothing Then
                            Return "Développement personnel"
                        Else
                            Select Objet_150.Type_Formulaire
                                Case "Manager"
                                    Return "Équité"
                                Case Else
                                    Return "Développement personnel"
                            End Select
                        End If
                    Case 8
                        If Objet_150 Is Nothing Then
                            Return "Vision élargie"
                        Else
                            Select Case Objet_150.Type_Formulaire
                                Case "Manager"
                                    Return "Confiance mutuelle"
                                Case Else
                                    Return "Vision élargie"
                            End Select
                        End If
                    Case 9
                        If Objet_150 Is Nothing Then
                            Return "Réflexion logique"
                        Else
                            Select Case Objet_150.Type_Formulaire
                                Case "Manager"
                                    Return "Rêve / Vision"
                                Case Else
                                    Return "Réflexion logique"
                            End Select
                        End If
                    Case 10
                        If Objet_150 Is Nothing Then
                            Return "Coopération"
                        Else
                            Select Case Objet_150.Type_Formulaire
                                Case "Manager"
                                    Return "Simplicité, Concentration et Rapidité"
                                Case Else
                                    Return "Coopération"
                            End Select
                        End If
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property Label_NoteChiffre(ByVal Lettre As String) As String
            Get
                Select Case Objet_150.Type_Formulaire
                    Case "Manager"
                        Select Case Lettre
                            Case "A*"
                                Return "(5.0<)"
                            Case "A"
                                Return "(4.5-4.99)"
                            Case "B+"
                                Return "(4.0-4.49)"
                            Case "B"
                                Return "(3.0-3.99)"
                            Case "B-"
                                Return "(2.0-2.99)"
                            Case "C"
                                Return "(<1.99)"
                            Case Else
                                Return ""
                        End Select
                    Case Else
                        Select Case Lettre
                            Case "A*"
                                Return "(91-100)"
                            Case "A"
                                Return "(81-90)"
                            Case "B+"
                                Return "(61-80)"
                            Case "B"
                                Return "(41-60)"
                            Case "B-"
                                Return "(21-40)"
                            Case "C"
                                Return "(0-20)"
                            Case Else
                                Return ""
                        End Select
                End Select
            End Get
        End Property

        Public ReadOnly Property Tooltip_NoteLettre(ByVal Lettre As String) As String
            Get
                Select Case Lettre
                    Case "A*"
                        Return "Excellent"
                    Case "A"
                        Return "Trés bien"
                    Case "B+"
                        Return "Au delà des attentes"
                    Case "B"
                        Return "En ligne avec les attentes"
                    Case "B-"
                        Return "En deçà des attentes"
                    Case "C"
                        Return "Doit s'améliorer"
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Private Sub InitialiserManager(ByVal DateEffet As String)
            Dim FicheOrga As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION
            Dim PrecisionManager As List(Of String) = Nothing
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim TableauData(0) As String
            Dim IndiceI As Integer
            Dim SiOK As Boolean

            WsIdeManager = 0
            If CDate(DateEffet) < CDate("01/04/" & Year(Now)) Then
                If WsObjet_150 IsNot Nothing Then
                    WsIdeManager = WsObjet_150.Identifiant_Manager
                End If
            End If
            WsNomPrenomManager = ""
            WsServiceManager = ""
            WsFonctionManager = ""
            WsEmailManager = ""
            PrecisionManager = New List(Of String)
            If System.Configuration.ConfigurationManager.AppSettings("PrecisionManager") <> "" Then
                TableauData = Strings.Split(System.Configuration.ConfigurationManager.AppSettings("PrecisionManager"), ";", -1)
                For Each Elt As String In TableauData
                    PrecisionManager.Add(Elt)
                Next
            Else
                PrecisionManager.Add("MANAGER")
            End If

            If WsIdeManager = 0 Then
                Dim StructureAff As String
                For IndiceI = 4 To 1 Step -1
                    StructureAff = NiveauAffectation(DateEffet, IndiceI)
                    If StructureAff <> "" Then
                        WsIdeManager = WsPointeurGlobal.IdentifiantManager(CStr(IndiceI), StructureAff)
                        If WsIdeManager > 0 And WsIdeManager <> WsIdentifiant Then
                            '*** Spécifique Honda - Précision avec la fonction ***************
                            WsFonctionManager = ""
                            LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaOrganigramme, WsIdeManager, True)
                            If LstFiches IsNot Nothing Then
                                For Each FichePER In LstFiches
                                    FicheOrga = New Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION
                                    FicheOrga.ContenuTable = FichePER.Ide_Dossier & VI.Tild & CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION).ContenuTable
                                    Select Case WsPointeurGlobal.VirRhDates.ComparerDates(FicheOrga.Date_de_Valeur, DateEffet)
                                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                            If FicheOrga.Structure_de_2e_niveau <> "" Then
                                                WsServiceManager = FicheOrga.Structure_de_2e_niveau
                                            Else
                                                WsServiceManager = FicheOrga.Structure_de_1er_niveau
                                            End If
                                            WsFonctionManager = FicheOrga.Fonction_exercee
                                            Exit For
                                    End Select
                                Next
                            End If
                            SiOK = False
                            For Each Elt As String In PrecisionManager
                                If Elt = WsFonctionManager Then
                                    SiOK = True
                                    Exit For
                                End If
                            Next
                            If SiOK = True Then
                                Exit For
                            Else
                                WsIdeManager = 0
                            End If
                            '************************************************************************
                        Else
                            If WsIdeManager = WsIdentifiant AndAlso IndiceI = 1 Then
                                WsIdeManager = 0
                                StructureAff = AffectationSecondaire(DateEffet, 1)
                                If StructureAff <> "" Then
                                    WsIdeManager = WsPointeurGlobal.IdentifiantManager(CStr(IndiceI), StructureAff)
                                End If
                            Else
                                WsIdeManager = 0
                            End If
                        End If
                    End If
                Next IndiceI
            End If

            If WsIdeManager = 0 Then
                If WsObjet_150 IsNot Nothing Then
                    WsIdeManager = WsObjet_150.Identifiant_Manager
                End If
            End If
            If WsIdeManager = 0 Then
                WsNomPrenomManager = ""
                WsServiceManager = ""
                WsFonctionManager = ""
                WsEmailManager = ""
                Exit Sub
            End If

            Dim FicheCivil As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
            Dim FicheAdrPro As Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO

            LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaCivil, WsIdeManager, False)
            If LstFiches IsNot Nothing Then
                FicheCivil = New Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
                FicheCivil.ContenuTable = LstFiches.Item(0).Ide_Dossier & CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL).ContenuTable
                WsNomPrenomManager = FicheCivil.V_NomEtPrenom
            End If

            LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAdrPro, WsIdeManager, False)
            If LstFiches IsNot Nothing Then
                FicheAdrPro = New Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO
                FicheAdrPro.ContenuTable = LstFiches.Item(0).Ide_Dossier & CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO).ContenuTable
                WsEmailManager = FicheAdrPro.Email
            End If

            LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaOrganigramme, WsIdeManager, True)
            If LstFiches IsNot Nothing Then
                For Each FichePER In LstFiches
                    FicheOrga = New Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION
                    FicheOrga.ContenuTable = FichePER.Ide_Dossier & VI.Tild & CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION).ContenuTable
                    Select Case WsPointeurGlobal.VirRhDates.ComparerDates(FicheOrga.Date_de_Valeur, DateEffet)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                            If FicheOrga.Structure_de_2e_niveau <> "" Then
                                WsServiceManager = FicheOrga.Structure_de_2e_niveau
                            Else
                                WsServiceManager = FicheOrga.Structure_de_1er_niveau
                            End If
                            WsFonctionManager = FicheOrga.Fonction_exercee
                            Exit For
                    End Select
                Next
            End If

            WsSiEstAussiManager = VerifierSiManager()
        End Sub

        Private Function VerifierSiManager() As Boolean
            Dim PrecisionManager As List(Of String)
            Dim TableauData(0) As String
            Dim SiManagerAbsence As Boolean
            Dim FonctionManagerAbsence As String

            SiManagerAbsence = WsPointeurGlobal.SiEstAussiManager(WsFicheEtatCivil.Ide_Dossier)
            If SiManagerAbsence = False Then
                Return False
            End If
            FonctionManagerAbsence = FonctionExercee(WsPointeurGlobal.VirRhDates.DateduJour, False)
            PrecisionManager = New List(Of String)
            If System.Configuration.ConfigurationManager.AppSettings("PrecisionManager") <> "" Then
                TableauData = Strings.Split(System.Configuration.ConfigurationManager.AppSettings("PrecisionManager"), ";", -1)
                For Each Elt As String In TableauData
                    PrecisionManager.Add(Elt)
                Next
            Else
                PrecisionManager.Add("MANAGER")
            End If
            For Each Elt As String In PrecisionManager
                If Elt = FonctionManagerAbsence Then
                    Return True
                End If
            Next
            Return False
        End Function

        Private Sub LireFondsDossier()
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim ListeObjets As List(Of Integer) = New List(Of Integer)
            ListeObjets.Add(VI.ObjetPer.ObaCivil)
            ListeObjets.Add(VI.ObjetPer.ObaStatut)
            ListeObjets.Add(VI.ObjetPer.ObaOrganigramme)
            ListeObjets.Add(VI.ObjetPer.ObaAdrPro)
            ListeObjets.Add(VI.ObjetPer.ObaActivite)
            ListeObjets.Add(VI.ObjetPer.ObaGrade)
            ListeObjets.Add(VI.ObjetPer.ObaFormation)
            ListeObjets.Add(VI.ObjetPer.ObaDIF)
            ListeObjets.Add(VI.ObjetPer.ObaSpecialite)
            ListeObjets.Add(VI.ObjetPer.ObaAffectation2nd)

            WsListeFonds = WsPointeurGlobal.VirServiceServeur.LectureDossier_ToFiches(WsPointeurGlobal.PointeurObjetUtiGlobal.Nom, _
                                                                                       VI.PointdeVue.PVueApplicatif, WsIdentifiant, False, ListeObjets)
            LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaCivil).ToList
            If LstFiches Is Nothing Then
                Exit Sub
            End If
            WsFicheEtatCivil = CType(LstFiches(0), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)
        End Sub

        Private Sub LireEntretiens()
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim ListeObjets As List(Of Integer) = New List(Of Integer)
            For IndiceI = 150 To 153
                ListeObjets.Add(IndiceI)
            Next
            ListeObjets.Add(VI.ObjetPer.ObaExterne)
            WsListeEntretiens = WsPointeurGlobal.VirServiceServeur.LectureDossier_ToFiches(WsPointeurGlobal.PointeurObjetUtiGlobal.Nom, _
                                                                                       VI.PointdeVue.PVueApplicatif, WsIdentifiant, False, ListeObjets)
            WsAnnee = ""
            LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 Order By instance.Date_Valeur_ToDate Descending).ToList
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each VirFiche In LstFiches
                    WsObjet_150 = CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_HONDA_GENERAL)
                    If WsObjet_150.Date_Valeur_ToDate >= CDate("01/04/2014") Then
                        WsAnnee = Strings.Right(WsObjet_150.Date_de_Valeur, 4)
                        Exit For
                    End If
                Next
            End If
            Annee = WsAnnee
        End Sub

        Public Sub New(ByVal PointeurGlobal As Virtualia.Net.WebAppli.ObjetGlobal, ByVal Ide As Integer, ByVal NomCnx As String)
            WsPointeurGlobal = PointeurGlobal
            WsNomUtiSGBD = WsPointeurGlobal.PointeurObjetUtiGlobal.Nom
            WsNomUtilisateur = NomCnx
            WsIdentifiant = Ide

            Call LireFondsDossier()
            Call LireEntretiens()
        End Sub

        Public Sub New(ByVal PointeurGlobal As Virtualia.Net.WebAppli.ObjetGlobal, ByVal Ide As Integer, ByVal ListeFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE), ByVal NomCnx As String)
            WsPointeurGlobal = PointeurGlobal
            WsNomUtiSGBD = WsPointeurGlobal.PointeurObjetUtiGlobal.Nom
            WsNomUtilisateur = NomCnx
            WsIdentifiant = Ide

            WsListeFonds = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            WsListeEntretiens = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            For Each Virfiche In ListeFiches
                Select Case Virfiche.NumeroObjet
                    Case 150 To 153
                        WsListeEntretiens.Add(Virfiche)
                    Case Else
                        WsListeFonds.Add(Virfiche)
                End Select
            Next
            Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            LstRes = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaCivil).ToList
            If LstRes Is Nothing Then
                Exit Sub
            End If
            WsFicheEtatCivil = CType(LstRes(0), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)
            
            WsSiEstAussiManager = VerifierSiManager()
        End Sub

    End Class
End Namespace


