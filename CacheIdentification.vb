﻿Option Strict On
Option Explicit On
Option Compare Text
<Serializable>
Public Class CacheIdentification
    Private WsTild As String = "~"
    Private WsNomIdentification As String = ""
    Private WsSessionID As String = ""

    Public Property Nom_Identification As String
        Get
            Return WsNomIdentification
        End Get
        Set(value As String)
            WsNomIdentification = value
        End Set
    End Property

    Public Property NumeroSession As String
        Get
            Return WsSessionID
        End Get
        Set(value As String)
            WsSessionID = value
        End Set
    End Property

    Public Property Chaine_Cookie As String
        Get
            Dim Chaine As New System.Text.StringBuilder
            Chaine.Append(Nom_Identification & WsTild)
            Chaine.Append(NumeroSession)
            Return Chaine.ToString
        End Get
        Set(value As String)
            Dim TableauData(0) As String
            TableauData = Strings.Split(value, WsTild, -1)
            If TableauData.Count < 2 Then
                Exit Property
            End If
            Nom_Identification = TableauData(0)
            NumeroSession = TableauData(1)
        End Set
    End Property
End Class
