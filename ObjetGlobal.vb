﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace WebAppli
    Public Class ObjetGlobal
        Inherits System.Collections.CollectionBase
        'Private WsClientServiceWcf As Virtualia.Net.WebService.ServiceWcfServeur = Nothing
        Private WsClientServiceWeb As Virtualia.Net.WebService.ServiceWebServeur = Nothing
        Private ModeFonctionnement As String = System.Configuration.ConfigurationManager.AppSettings("ModeVirtualia")

        Private WsPointeurUtiGlobal As Virtualia.Net.Datas.ObjetUtilisateur
        Private WsPointeurEnsemble As Virtualia.Net.Entretien.EnsembleExtraction
        Private WsRhModele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH
        Private WsInstanceSgbd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd
        '
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates = Nothing
        Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales = Nothing
        Private WsRhShemaPER As Virtualia.TablesObjet.ShemaPER.VConstructeur = Nothing
        Private WsListeObjetsDico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire) = Nothing
        Private WsListeInfosDico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire) = Nothing
        Private WsListeExpertesDico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte) = Nothing
        Private WsUrlImageArmoire(31) As String
        '
        Private WsUrlWebService As String = System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur")
        Private WsTypeEntretien As String = System.Configuration.ConfigurationManager.AppSettings("TypeEntretien")
        '
        Private WsListeUtiAnonymes As List(Of Virtualia.Net.Datas.ObjetUtiAnonyme)
        '
        Private WsRefCompetence As Virtualia.Net.Entretien.ObjetRefCompetence
        Private WsTableThemes As System.Text.StringBuilder = Nothing

        Private WsListeTableDecisions As List(Of Virtualia.TablesObjet.ShemaREF.OUT_DECISION)
        '************************************
        Private WsAppliIE As Object
        Private WsPointeurExtractHonda As Virtualia.Net.Entretien.EnsembleExtractHonda

        Private WsOldCompetence_CESE_CNED As Virtualia.Net.Entretien.ObjetRefCompetence

        Public ReadOnly Property VirServiceServeur As Virtualia.Net.WebService.IServiceServeur
            Get
                Dim ParamSpecifique As Integer = 0
                If WsTypeEntretien = "HONDA" Then
                    ParamSpecifique = 4
                End If
                Select Case ModeFonctionnement
                    Case "WCF"
                        Return Nothing
                    Case Else
                        Dim Url As String = System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur")
                        If WsClientServiceWeb Is Nothing Then
                            If WsRhModele Is Nothing Then
                                WsClientServiceWeb = New Virtualia.Net.WebService.ServiceWebServeur(Url, ParamSpecifique)
                            Else
                                WsClientServiceWeb = New Virtualia.Net.WebService.ServiceWebServeur(Url, WsRhModele.InstanceProduit.ClefModele, ParamSpecifique)
                            End If
                        End If
                        Return WsClientServiceWeb
                End Select
            End Get
        End Property

        Public ReadOnly Property PointeurObjetUtiGlobal As Virtualia.Net.Datas.ObjetUtilisateur
            Get
                Return WsPointeurUtiGlobal
            End Get
        End Property

        Public Property PointeurEnsemble() As Virtualia.Net.Entretien.EnsembleExtraction
            Get
                Return WsPointeurEnsemble
            End Get
            Set(ByVal value As Virtualia.Net.Entretien.EnsembleExtraction)
                WsPointeurEnsemble = value
            End Set
        End Property

        Public Property PointeurExtractionHonda() As Virtualia.Net.Entretien.EnsembleExtractHonda
            Get
                If WsPointeurExtractHonda Is Nothing Then
                    WsPointeurExtractHonda = New Virtualia.Net.Entretien.EnsembleExtractHonda(Me)
                End If
                Return WsPointeurExtractHonda
            End Get
            Set(ByVal value As Virtualia.Net.Entretien.EnsembleExtractHonda)
                WsPointeurExtractHonda = value
            End Set
        End Property

        Public ReadOnly Property VirRefCompetence() As Virtualia.Net.Entretien.ObjetRefCompetence
            Get
                Return WsRefCompetence
            End Get
        End Property

        Public ReadOnly Property Adresse_FrmEntretien As String
            Get
                Select Case WsTypeEntretien
                    Case "CNM"
                        Return "Fenetres/CNM/FrmEntretienCNM.aspx"
                    Case "CE"
                        Return "Fenetres/CE/FrmEntretienAgent.aspx"
                    Case "CESE"
                        Return "Fenetres/CESE/FrmEntretienCESE.aspx"
                    Case "CESE2"
                        Return "Fenetres/CESE/FrmEntretienCESE2.aspx"
                    Case "CNED"
                        Return "Fenetres/CNED/FrmEntretienCNED.aspx"
                    Case "CNED2"
                        Return "Fenetres/CNED/FrmEntretienCNED2.aspx"
                    Case "ENM"
                        Return "Fenetres/ENM/FrmEntretienENM.aspx"
                    Case "FAM"
                        Return "Fenetres/FAM/FrmEntretienFAM.aspx"
                    Case "GIP"
                        Return "Fenetres/GIP/FrmEntretienGIP.aspx"
                    Case "HONDA"
                        Return "Fenetres/HONDA/FrmEntretienHonda.aspx"
                    Case "ISMEP"
                        Return "Fenetres/ISMEP/FrmEntretienISMEP.aspx"
                    Case "MEN"
                        Return "Fenetres/MEN/FrmEntretienMEN.aspx"
                    Case "ONISEP"
                        Return "Fenetres/ONISEP/FrmEntretienONISEP.aspx"
                    Case "PNF"
                        Return "Fenetres/PNF/FrmEntretienPNF.aspx"
                    Case "AFB"
                        Return "Fenetres/AFB/FrmEntretienAFB.aspx"
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property Date_Limite(ByVal DateValeur As String) As String
            Get
                Dim ChaineConfig As String = System.Configuration.ConfigurationManager.AppSettings("DateLimite")
                If DateValeur = "" Then
                    DateValeur = System.DateTime.Now.ToShortDateString
                End If
                If ChaineConfig Is Nothing OrElse ChaineConfig = "" Then
                    Return DateValeur
                End If
                ChaineConfig &= "/" & Now.Year
                If CDate(DateValeur).Year = Now.Year Then
                    If CDate(DateValeur) <= CDate(ChaineConfig) Then
                        Return "31/12/" & (Now.Year - 1)
                    End If
                End If
                Return DateValeur
            End Get
        End Property

        Public ReadOnly Property VirRhFonction As Virtualia.Systeme.Fonctions.Generales
            Get
                If WsRhFonction Is Nothing Then
                    WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
                End If
                Return WsRhFonction
            End Get
        End Property

        Public ReadOnly Property VirRhDates As Virtualia.Systeme.Fonctions.CalculDates
            Get
                If WsRhDates Is Nothing Then
                    WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
                End If
                Return WsRhDates
            End Get
        End Property

        Public ReadOnly Property VirRhShemaPER As Virtualia.TablesObjet.ShemaPER.VConstructeur
            Get
                If WsRhShemaPER Is Nothing Then
                    WsRhShemaPER = New Virtualia.TablesObjet.ShemaPER.VConstructeur(VirModele.InstanceProduit.ClefModele)
                    If WsTypeEntretien = "HONDA" Then
                        WsRhShemaPER.V_Parametre = 4
                    End If
                End If
                Return WsRhShemaPER
            End Get
        End Property

        Public ReadOnly Property UrlFichierTxt() As String
            Get
                Return System.Configuration.ConfigurationManager.AppSettings("NomPartageTelechargement")
            End Get
        End Property

        Public ReadOnly Property PathPhysiqueFichierTxt() As String
            Get
                Return System.Configuration.ConfigurationManager.AppSettings("RepertoireTelechargement")
            End Get
        End Property

        Public ReadOnly Property AjouterSessionVirtualia(ByVal NoSession As String, ByVal NomUti As String) As Virtualia.Net.Datas.ObjetUtiAnonyme
            Get
                Dim NouveauUser As Virtualia.Net.Datas.ObjetUtiAnonyme
                If WsListeUtiAnonymes Is Nothing Then
                    WsListeUtiAnonymes = New List(Of Virtualia.Net.Datas.ObjetUtiAnonyme)
                End If
                If WsListeUtiAnonymes.Count > 100 Then
                    Try
                        Call PurgerListeAnonyme()
                    Catch ex As Exception
                        Exit Try
                    End Try
                End If
                If ItemSession(NoSession) Is Nothing Then
                    NouveauUser = New Virtualia.Net.Datas.ObjetUtiAnonyme(NoSession, NomUti)
                    WsListeUtiAnonymes.Add(NouveauUser)
                    Return NouveauUser
                Else
                    Return ItemSession(NoSession)
                End If
            End Get
        End Property

        Public ReadOnly Property ItemSession(ByVal NoSession As String) As Virtualia.Net.Datas.ObjetUtiAnonyme
            Get
                If WsListeUtiAnonymes Is Nothing Then
                    Return Nothing
                End If
                For Each Uti As Virtualia.Net.Datas.ObjetUtiAnonyme In WsListeUtiAnonymes
                    If Uti.IDSession = NoSession Then
                        Return Uti
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public Sub LibererSession(ByVal NoSession As String)
            Dim Uti As Virtualia.Net.Datas.ObjetUtiAnonyme = Nothing
            If WsListeUtiAnonymes Is Nothing Then
                Exit Sub
            End If
            For Each Uti In WsListeUtiAnonymes
                If Uti.IDSession = NoSession Then
                    Exit For
                End If
            Next
            If Uti Is Nothing Then
                Exit Sub
            End If
            If Uti.IDSession = NoSession Then
                WsListeUtiAnonymes.Remove(Uti)
            End If
        End Sub

        Public Property AppliIE As Object
            Get
                Return WsAppliIE
            End Get
            Set(ByVal value As Object)
                WsAppliIE = value
            End Set
        End Property

        Private Sub PurgerListeAnonyme()
            If WsListeUtiAnonymes Is Nothing Then
                Exit Sub
            End If
            Dim I As Integer
            Dim Heure As Integer
            Dim HeureJour As Integer = 0
            Dim TableauObjet(0) As String
            Dim TableauData(0) As String

            TableauObjet = Strings.Split(Format(TimeOfDay, "t"), VI.Tiret, -1)
            If TableauObjet.Count > 0 Then
                TableauData = Strings.Split(TableauObjet(0), ":", -1)
                If TableauData.Count > 0 Then
                    If IsNumeric(TableauData(0)) Then
                        HeureJour = CType(TableauData(0), Integer)
                    End If
                Else
                    Exit Sub
                End If
            Else
                Exit Sub
            End If
            I = 0
            Do
                If I > WsListeUtiAnonymes.Count - 1 Then
                    Exit Do
                End If
                TableauObjet = Strings.Split(WsListeUtiAnonymes.Item(I).Nom, VI.Tiret, -1)
                If TableauObjet.Count > 0 Then
                    TableauData = Strings.Split(TableauObjet(1), ":", -1)
                    If TableauData.Count > 0 Then
                        If IsNumeric(TableauData(0)) Then
                            Heure = CType(TableauData(0), Integer)
                            If (HeureJour - Heure > 1) Or (Heure - HeureJour > 1) Then
                                WsListeUtiAnonymes.RemoveAt(I)
                                I -= 1
                            Else
                                Exit Do
                            End If
                        End If
                    End If
                End If
                I += 1
            Loop
        End Sub

        '****************************************
        Public ReadOnly Property AjouterUnUtilisateur(ByVal NomUtilisateur As String, ByVal NoBd As Integer) As Virtualia.Net.Datas.ObjetUtilisateur
            Get
                Dim NouveauUser As Virtualia.Net.Datas.ObjetUtilisateur
                NouveauUser = New Virtualia.Net.Datas.ObjetUtilisateur(Me, NomUtilisateur, NoBd)
                Me.List.Add(NouveauUser)
                Return NouveauUser
            End Get
        End Property

        Default Public ReadOnly Property Item(ByVal Index As Integer) As Virtualia.Net.Datas.ObjetUtilisateur
            Get
                Return CType(Me.List.Item(Index), Virtualia.Net.Datas.ObjetUtilisateur)
            End Get
        End Property

        Public ReadOnly Property ItemUti(ByVal Nom As String, ByVal Prenom As String) As Virtualia.Net.Datas.ObjetUtilisateur
            Get
                For Each Uti As Virtualia.Net.Datas.ObjetUtilisateur In Me.List
                    Select Case Uti.Nom
                        Case Is = Nom
                            If Prenom = "" Then
                                Return Uti
                            End If
                            Select Case Uti.Prenom
                                Case Is = Prenom
                                    Return Uti
                            End Select
                    End Select
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property VirUrlWebServeur() As String
            Get
                Return WsUrlWebService
            End Get
        End Property

        Public ReadOnly Property VirTypeEntretien As String
            Get
                Return WsTypeEntretien
            End Get
        End Property

        Public ReadOnly Property VirModele() As Virtualia.Systeme.MetaModele.Donnees.ModeleRH
            Get
                Return WsRhModele
            End Get
        End Property

        Public ReadOnly Property IdentifiantManager(ByVal Niveau As String, ByVal Intitule As String) As Integer
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstRes As List(Of String)
                Dim OrdreSql As String

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, WsPointeurUtiGlobal.InstanceBd)
                'Constructeur.NombredeRequetes(VI.PointdeVue.PVueVueLogique, "", "", VI.Operateurs.ET) = 1
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueDirection, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.NoInfoSelection(0, 2) = 1

                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Intitule
                Constructeur.InfoExtraite(0, 2, 0) = 1
                Constructeur.InfoExtraite(1, 2, 0) = 4

                OrdreSql = Constructeur.OrdreSqlDynamique

                'ChaineLue = Proxy.SelectionSql(WsPointeurUtiGlobal.Nom, VI.PointdeVue.PVueVueLogique, 1, OrdreSql, 1)
                LstRes = VirServiceServeur.RequeteSql_ToListeChar(WsPointeurUtiGlobal.Nom, VI.PointdeVue.PVueDirection, 1, OrdreSql)

                If LstRes Is Nothing Then
                    Return 0
                End If
                Dim TableauData(0) As String
                For Each Resultat In LstRes
                    TableauData = Strings.Split(Resultat, VI.Tild)
                    If Strings.Right(TableauData(0), 1) = Niveau Then
                        If IsNumeric(TableauData(2)) Then
                            Return CInt(TableauData(2))
                        End If
                    End If
                Next
                Return 0
            End Get
        End Property

        Public ReadOnly Property IdentifiantManager(ByVal IdeSalarie As Integer) As Integer
            Get
                Dim WseOutil As New Virtualia.Net.WebService.Outils(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaUtils"))
                Dim Chaine As String = WseOutil.ExtractionAnnuaire_ParIde(IdeSalarie, False)
                WseOutil.Dispose()
                If Chaine = "" Then
                    Return 0
                End If
                Dim TableauObjet(0) As String
                Dim TableauData(0) As String
                Dim IndiceI As Integer
                TableauObjet = Strings.Split(Chaine, VI.SigneBarre, -1)
                For IndiceI = 0 To TableauObjet.Count - 1
                    If TableauObjet(IndiceI) = "" Then
                        Exit For
                    End If
                    TableauData = Strings.Split(TableauObjet(IndiceI), VI.PointVirgule, -1)
                    If TableauData.Count > 18 Then
                        If IsNumeric(TableauData(18)) Then
                            Return CInt(TableauData(18))
                        End If
                    End If
                Next IndiceI
                Return 0
            End Get
        End Property

        Public ReadOnly Property SiEstAussiManager(ByVal Ide As Integer) As Boolean
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstRes As List(Of String)
                Dim OrdreSql As String

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, WsPointeurUtiGlobal.InstanceBd)
                'Constructeur.NombredeRequetes(VI.PointdeVue.PVueVueLogique, "", "", VI.Operateurs.ET) = 1
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueDirection, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.NoInfoSelection(0, 2) = 4

                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = CStr(Ide)
                Constructeur.InfoExtraite(0, 2, 0) = 4

                OrdreSql = Constructeur.OrdreSqlDynamique

                'ChaineLue = Proxy.SelectionSql(WsPointeurUtiGlobal.Nom, VI.PointdeVue.PVueVueLogique, 1, OrdreSql, 1)
                LstRes = VirServiceServeur.RequeteSql_ToListeChar(WsPointeurUtiGlobal.Nom, VI.PointdeVue.PVueDirection, 1, OrdreSql)
                If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                    Return False
                End If
                Return True
            End Get
        End Property

        Public ReadOnly Property IdentifiantInverse(ByVal PointdeVue As Integer, ByVal Valeur As String) As Integer
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstRes As List(Of String)
                Dim OrdreSql As String
                Dim TableauObjet(0) As String
                Dim TableauData(0) As String
                Dim Ide As Integer = 0

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, WsPointeurUtiGlobal.InstanceBd)
                Constructeur.NombredeRequetes(PointdeVue, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Select Case PointdeVue
                    Case VI.PointdeVue.PVuePaie
                        Constructeur.NoInfoSelection(0, 1) = 2
                    Case Else
                        Constructeur.NoInfoSelection(0, 1) = 1
                End Select
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeur
                Constructeur.InfoExtraite(0, 1, 0) = 1

                OrdreSql = Constructeur.OrdreSqlDynamique

                LstRes = VirServiceServeur.RequeteSql_ToListeChar(WsPointeurUtiGlobal.Nom, PointdeVue, 1, OrdreSql)
                If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                    Return 0
                    Exit Property
                End If
                TableauData = Strings.Split(LstRes.Item(0), VI.Tild, -1)
                If IsNumeric(TableauData(0)) Then
                    Ide = CInt(TableauData(0))
                End If
                Return Ide
            End Get
        End Property

        Public ReadOnly Property VirObjet(ByVal PointdeVue As Integer, ByVal NumObjet As Integer) As Virtualia.Systeme.MetaModele.Donnees.Objet
            Get
                Dim IndiceP As Integer
                Dim IndiceO As Integer
                For IndiceP = 0 To WsRhModele.NombredePointdeVue - 1
                    Select Case WsRhModele.Item(IndiceP).Numero
                        Case Is = PointdeVue
                            For IndiceO = 0 To WsRhModele.Item(IndiceP).NombredObjets - 1
                                Select Case WsRhModele.Item(IndiceP).Item(IndiceO).Numero
                                    Case Is = NumObjet
                                        Return WsRhModele.Item(IndiceP).Item(IndiceO)
                                End Select
                            Next IndiceO
                            Exit For
                    End Select
                Next IndiceP
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property VirRangTranslate(ByVal NumObjet As Integer, ByVal RangOrigine As String, Optional ByVal Nature As String = "") As String
            Get
                Dim Referentiel As Virtualia.Net.Entretien.ObjetRefCompetence
                Select Case RangOrigine
                    Case Is = ""
                        Return ""
                End Select
                If Nature = "" Then
                    Nature = WsTypeEntretien
                End If
                Select Case Nature
                    Case "CESE", "CNED"
                        Referentiel = WsOldCompetence_CESE_CNED
                    Case Else
                        Referentiel = WsRefCompetence
                End Select
                Select Case NumObjet
                    Case 151
                        Select Case Nature
                            Case "CNM"
                                Select Case RangOrigine
                                    Case "E"
                                        Return "1"
                                    Case "F"
                                        Return "2"
                                    Case "G"
                                        Return "3"
                                    Case "H"
                                        Return "4"
                                    Case Else
                                        Return "4"
                                End Select
                            Case "CESE", "CESE2"
                                Select Case RangOrigine
                                    Case "A"
                                        Return "1"
                                    Case "B"
                                        Return "2"
                                    Case "C"
                                        Return "3"
                                    Case "D"
                                        Return "4"
                                    Case Else
                                        Return "4"
                                End Select
                            Case "HONDA"
                                Select Case RangOrigine 'de K à N Manager, de O à S Salarié
                                    Case "K"
                                        Return "6"
                                    Case "L"
                                        Return "7"
                                    Case "M"
                                        Return "8"
                                    Case "N"
                                        Return "9"
                                    Case "O"
                                        Return "1"
                                    Case "P"
                                        Return "2"
                                    Case "Q"
                                        Return "3"
                                    Case "R"
                                        Return "4"
                                    Case "S"
                                        Return "5"
                                    Case Else
                                        Return "1"
                                End Select
                            Case "CNED2", "ENM", "ISMEP", "MEN", "ONISEP", "GIP"
                                Select Case RangOrigine
                                    Case "A"
                                        Return "1"
                                    Case "B"
                                        Return "2"
                                    Case "C"
                                        Return "3"
                                    Case "D"
                                        Return "4"
                                    Case "E"
                                        Return "5"
                                    Case "F"
                                        Return "6"
                                    Case "H"
                                        Return "7"
                                    Case Else
                                        Return "6"
                                End Select
                            Case Else
                                Select Case RangOrigine
                                    Case "C"
                                        Return "1"
                                    Case "D"
                                        Return "2"
                                    Case "E"
                                        Return "3"
                                    Case "F"
                                        Return "4"
                                    Case "G"
                                        Return "5"
                                    Case "H"
                                        Return "6"
                                    Case Else
                                        Return "7"
                                End Select
                        End Select
                    Case 152
                        Select Case Nature
                            Case "CNM", "CESE", "CESE2"
                                Select Case RangOrigine
                                    Case "A"
                                        Return "1"
                                    Case "B"
                                        Return "2"
                                    Case "C"
                                        Return "3"
                                    Case "D"
                                        Return "4"
                                    Case Else
                                        Return "4"
                                End Select
                            Case "HONDA"
                                Select Case RangOrigine
                                    Case "K"
                                        Return "1"
                                    Case "L"
                                        Return "2"
                                    Case "M"
                                        Return "3"
                                    Case "N"
                                        Return "4"
                                    Case "O"
                                        Return "5"
                                    Case "P"
                                        Return "6"
                                    Case "Q"
                                        Return "7"
                                    Case "R"
                                        Return "8"
                                    Case "S"
                                        Return "9"
                                    Case "T"
                                        Return "10"
                                    Case Else
                                        Return "1"
                                End Select
                            Case "CNED2", "ISMEP", "ENM", "MEN", "ONISEP", "GIP"
                                Select Case RangOrigine
                                    Case "A"
                                        Return "1"
                                    Case "B"
                                        Return "2"
                                    Case "C"
                                        Return "3"
                                    Case "D"
                                        Return "4"
                                    Case "E"
                                        Return "5"
                                    Case "F"
                                        Return "6"
                                    Case "G"
                                        Return "7"
                                    Case Else
                                        Return "6"
                                End Select
                            Case "AFB"
                                Select Case RangOrigine
                                    Case "B"
                                        Return "1"
                                    Case "C"
                                        Return "2"
                                    Case "D"
                                        Return "3"
                                    Case "E"
                                        Return "4"
                                    Case "F"
                                        Return "5"
                                    Case "G"
                                        Return "6"
                                    Case "H"
                                        Return "7"
                                    Case "I"
                                        Return "8"
                                    Case "J"
                                        Return "9"
                                    Case "K"
                                        Return "10"
                                    Case Else
                                        Return "11"
                                End Select
                            Case Else
                                Select Case RangOrigine
                                    Case "B"
                                        Return "1"
                                    Case "C"
                                        Return "2"
                                    Case "D"
                                        Return "3"
                                    Case "E"
                                        Return "4"
                                    Case "F"
                                        Return "5"
                                    Case "G"
                                        Return "6"
                                    Case Else
                                        Return "7"
                                End Select
                        End Select
                    Case 153
                        Select Case Nature
                            Case "CESE", "CESE2", "FAM"
                                Return Referentiel.VirRangCompetence(RangOrigine, 5) 'Parl'index de tri
                            Case "HONDA" 'Formation
                                Select Case RangOrigine
                                    Case "A"
                                        Return "1"
                                    Case "B"
                                        Return "2"
                                    Case "C"
                                        Return "3"
                                    Case Else
                                        Return "1"
                                End Select
                            Case "CNED2", "ISMEP", "MEN", "ONISEP"
                                Return ""
                            Case "AFB"
                                Select Case RangOrigine
                                    Case "M", "N", "O"
                                        Return Referentiel.VirRangCompetence(RangOrigine, 5) 'Parl'index de tri
                                    Case Else
                                        Return Referentiel.VirRangCompetence(RangOrigine, 0)
                                End Select
                            Case Else
                                Return Referentiel.VirRangCompetence(RangOrigine, 0)
                        End Select
                    Case 154
                        Return Referentiel.VirRangObservation(RangOrigine, 0)
                    Case 155
                        Select Case Nature
                            Case "CE"
                                Return Referentiel.VirRangFormation(RangOrigine, 0)
                            Case Else
                                Return Referentiel.VirRangFormation(RangOrigine, 2) 'Parl'index de tri
                        End Select
                    Case 156
                        Return Referentiel.VirRangSynthese(RangOrigine, 0)
                    Case 157
                        Return Referentiel.VirRangComplement(RangOrigine, 0)
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property VirRepertoireTemporaire As String
            Get
                Dim RepTemp As String = System.Configuration.ConfigurationManager.AppSettings("RepertoireTemporaire")
                If RepTemp Is Nothing OrElse RepTemp = "" Then
                    RepTemp = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia") & "\Temp\" & "Entretien"
                End If
                If My.Computer.FileSystem.DirectoryExists(RepTemp) = False Then
                    Try
                        My.Computer.FileSystem.CreateDirectory(RepTemp)
                    Catch ex As Exception
                        Exit Try
                    End Try
                End If
                Return RepTemp
            End Get
        End Property

        Public ReadOnly Property VirSgbd() As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd
            Get
                Return WsInstanceSgbd
            End Get
        End Property

        Public ReadOnly Property VirUrlImageSgbd(ByVal TypeduSgbd As Integer) As String
            Get
                Select Case TypeduSgbd
                    Case VI.TypeSgbdrNumeric.SgbdrAccess
                        Return "~\Images\Icones\DatabaseJaune.bmp"
                    Case VI.TypeSgbdrNumeric.SgbdrSqlServer, VI.TypeSgbdrNumeric.SgbdrDb2, VI.TypeSgbdrNumeric.SgbdrMySql
                        Return "~\Images\Icones\DatabaseBleu.bmp"
                    Case VI.TypeSgbdrNumeric.SgbdrOracle, VI.TypeSgbdrNumeric.SgbdrProgress
                        Return "~\Images\Icones\DatabaseRouge.bmp"
                    Case Else
                        Return "~\Images\Icones\DatabaseJaune.bmp"
                End Select
            End Get
        End Property

        Public ReadOnly Property VirUrlImageArmoire(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To WsUrlImageArmoire.Count - 1
                        Return WsUrlImageArmoire(Index)
                End Select
                Return WsUrlImageArmoire(0)
            End Get
        End Property

        Public ReadOnly Property VirUrlNavigateImageArmoire(ByVal Valeur As String) As String
            Get
                Dim IndiceI As Integer
                For IndiceI = 0 To 15
                    Select Case WsUrlImageArmoire(IndiceI)
                        Case Is = Valeur
                            Return WsUrlImageArmoire(IndiceI + 16)
                    End Select
                Next IndiceI
                Return ""
            End Get
        End Property

        Public ReadOnly Property VirListeObjetsDico() As List(Of Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire)
            Get
                If WsListeObjetsDico Is Nothing Then
                    WsListeObjetsDico = New List(Of Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire)
                    Dim FicheDico As Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire
                    Dim IndiceP As Integer
                    Dim IndiceO As Integer
                    For IndiceP = 0 To VirModele.NombredePointdeVue - 1
                        For IndiceO = 0 To VirModele.Item(IndiceP).NombredObjets - 1
                            FicheDico = New Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire(VirModele.Item(IndiceP).Item(IndiceO))
                            WsListeObjetsDico.Add(FicheDico)
                            FicheDico.VIndex = WsListeObjetsDico.Count
                        Next IndiceO
                    Next IndiceP
                End If
                Return WsListeObjetsDico
            End Get
        End Property

        Public ReadOnly Property VirListeInfosDico() As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
            Get
                If WsListeInfosDico Is Nothing Then
                    WsListeInfosDico = New List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
                    Dim FicheDico As Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire
                    Dim IndiceP As Integer
                    Dim IndiceO As Integer
                    Dim IndiceI As Integer
                    For IndiceP = 0 To VirModele.NombredePointdeVue - 1
                        For IndiceO = 0 To VirModele.Item(IndiceP).NombredObjets - 1
                            For IndiceI = 0 To VirModele.Item(IndiceP).Item(IndiceO).NombredInformations - 1
                                FicheDico = New Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire(VirModele.Item(IndiceP).Item(IndiceO).Item(IndiceI))
                                WsListeInfosDico.Add(FicheDico)
                                FicheDico.VIndex = WsListeInfosDico.Count
                            Next IndiceI
                        Next IndiceO
                    Next IndiceP
                End If
                Return WsListeInfosDico
            End Get
        End Property

        Public ReadOnly Property ListeTablesDecision(ByVal Ide As Integer, Optional ByVal SiDRH As Boolean = False) As List(Of Virtualia.TablesObjet.ShemaREF.OUT_DECISION)
            Get
                If WsListeTableDecisions Is Nothing Or SiDRH = True Then
                    Call LireTableDecision()
                End If
                Return (From Decision In WsListeTableDecisions Where Decision.Ide_Dossier = Ide Order By Decision.Rang Ascending).ToList
            End Get
        End Property

        Private Sub LireTableDecision()
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            LstFiches = VirServiceServeur.LectureObjet_ToFiches(WsPointeurUtiGlobal.Nom, VI.PointdeVue.PVueInterface, 4, 0, False)
            '*******************************************************************************************
            '90 - Honda
            '********************************************************************************************
            WsListeTableDecisions = VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaREF.OUT_DECISION)(LstFiches)
        End Sub

        Public ReadOnly Property SelectionDistincte(ByVal Pvue As Integer, ByVal NoObjet As Integer, ByVal NomTable As String, ByVal Champ As String) As List(Of String)
            Get
                Dim OrdreSql As String
                Dim LstRes As List(Of String)

                OrdreSql = "SELECT DISTINCT " & Champ & " FROM " & NomTable & " ORDER BY " & Champ
                LstRes = VirServiceServeur.RequeteSql_ToListeChar(WsPointeurUtiGlobal.Nom, Pvue, NoObjet, OrdreSql)
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NouvelIdentifiant(ByVal NomTableSgbd As String) As Integer
            Get
                Return VirServiceServeur.ObtenirUnCompteur(WsPointeurUtiGlobal.Nom, NomTableSgbd, "Max") + 1
            End Get
        End Property

        Public ReadOnly Property TabledesCategories() As String()
            Get
                Dim Tabliste(12) As String
                Dim I As Integer
                For I = 0 To Tabliste.Count - 1
                    Tabliste(I) = "9999" & VI.Tild & LibelleCategorie(I) & VI.Tild & I.ToString
                Next I
                Return Tabliste
            End Get
        End Property

        Public ReadOnly Property CategorieNum(ByVal Valeur As String) As Integer
            Get
                Dim I As Integer
                For I = 0 To 12
                    If LibelleCategorie(I) = Valeur Then
                        Return CShort(I)
                    End If
                Next I
                Return 0
            End Get
        End Property

        Public ReadOnly Property LibelleCategorie(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case VI.CategorieRH.InfosPersonnelles
                        Return "Informations personnelles"
                    Case VI.CategorieRH.Diplomes_Qualification
                        Return "Diplômes et qualificatons"
                    Case VI.CategorieRH.SituationAdministrative
                        Return "Situation administrative"
                    Case VI.CategorieRH.AffectationsFonctionnelles
                        Return "Affectations fonctionnelles"
                    Case VI.CategorieRH.AffectationsBudgetaires
                        Return "Affectations budgétaires"
                    Case VI.CategorieRH.TempsdeTravail
                        Return "Temps de travail, congés et absences"
                    Case VI.CategorieRH.Formation
                        Return "Formation continue"
                    Case VI.CategorieRH.Evaluation_Gpec
                        Return "Entretiens et évaluations "
                    Case VI.CategorieRH.Remuneration
                        Return "Rémunération"
                    Case VI.CategorieRH.Frais_Deplacement
                        Return "Frais de déplacement"
                    Case VI.CategorieRH.Social
                        Return "Informations sociales"
                    Case VI.CategorieRH.Retraite
                        Return "Retraite"
                    Case Else
                        Return "(Non défini)"
                End Select
            End Get
        End Property

        Private Sub InitialiserListeImages()
            WsUrlImageArmoire(0) = "~/Images/Armoire/JauneFermer16.bmp"
            WsUrlImageArmoire(1) = "~/Images/Armoire/OrangeFonceFermer16.bmp"
            WsUrlImageArmoire(2) = "~/Images/Armoire/TurquoiseFermer16.bmp"
            WsUrlImageArmoire(3) = "~/Images/Armoire/BleuFermer16.bmp"
            WsUrlImageArmoire(4) = "~/Images/Armoire/BleuFonceFermer16.bmp"
            WsUrlImageArmoire(5) = "~/Images/Armoire/SaumonFermer16.bmp"
            WsUrlImageArmoire(6) = "~/Images/Armoire/RougeCarminFermer16.bmp"
            WsUrlImageArmoire(7) = "~/Images/Armoire/MarronFermer16.bmp"
            WsUrlImageArmoire(8) = "~/Images/Armoire/GrisClairFermer16.bmp"
            WsUrlImageArmoire(9) = "~/Images/Armoire/GrisFonceFermer16.bmp"
            WsUrlImageArmoire(10) = "~/Images/Armoire/NoirFermer16.bmp"
            WsUrlImageArmoire(11) = "~/Images/Armoire/VerdatreFermer16.bmp"
            WsUrlImageArmoire(12) = "~/Images/Armoire/VertClairFermer16.bmp"
            WsUrlImageArmoire(13) = "~/Images/Armoire/VertFermer16.bmp"
            WsUrlImageArmoire(14) = "~/Images/Armoire/VertFonceFermer16.bmp"
            WsUrlImageArmoire(15) = "~/Images/Armoire/RougeFermer16.bmp"
            WsUrlImageArmoire(16) = "~/Images/Armoire/JauneOuvert16.bmp"
            WsUrlImageArmoire(17) = "~/Images/Armoire/OrangeFonceOuvert16.bmp"
            WsUrlImageArmoire(18) = "~/Images/Armoire/TurquoiseOuvert16.bmp"
            WsUrlImageArmoire(19) = "~/Images/Armoire/BleuOuvert16.bmp"
            WsUrlImageArmoire(20) = "~/Images/Armoire/BleuFonceOuvert16.bmp"
            WsUrlImageArmoire(21) = "~/Images/Armoire/SaumonOuvert16.bmp"
            WsUrlImageArmoire(22) = "~/Images/Armoire/RougeCarminOuvert16.bmp"
            WsUrlImageArmoire(23) = "~/Images/Armoire/OrangeOuvert16.bmp"
            WsUrlImageArmoire(24) = "~/Images/Armoire/GrisClairOuvert16.bmp"
            WsUrlImageArmoire(25) = "~/Images/Armoire/GrisFonceOuvert16.bmp"
            WsUrlImageArmoire(26) = "~/Images/Armoire/NoirOuvert16.bmp"
            WsUrlImageArmoire(27) = "~/Images/Armoire/VerdatreOuvert16.bmp"
            WsUrlImageArmoire(28) = "~/Images/Armoire/VertClairOuvert16.bmp"
            WsUrlImageArmoire(29) = "~/Images/Armoire/VertOuvert16.bmp"
            WsUrlImageArmoire(30) = "~/Images/Armoire/VertFonceOuvert16.bmp"
            WsUrlImageArmoire(31) = "~/Images/Armoire/RougeOuvert16.bmp"
        End Sub

        Public Sub EcrireLogTraitement(ByVal Nature As String, ByVal SiDatee As Boolean, ByVal Msg As String)
            Dim FicStream As System.IO.FileStream
            Dim FicWriter As System.IO.StreamWriter
            Dim NomLog As String
            Dim NomRep As String
            Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
            Dim dateValue As Date

            NomRep = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")
            NomLog = Virtualia.Systeme.Constantes.DossierVirtualiaService("Logs") & Nature & ".log"
            FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
            FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
            dateValue = System.DateTime.Now
            Select Case Msg
                Case Is = ""
                    FicWriter.WriteLine(Strings.StrDup(20, "-") & Strings.Space(1) & dateValue.ToString("dd/MM/yyyy hh:mm:ss.fff tt") & Space(1) & Strings.StrDup(20, "-"))
                Case Else
                    Select Case SiDatee
                        Case True
                            FicWriter.WriteLine(dateValue.ToString("dd/MM/yyyy hh:mm:ss.fff tt") & Space(1) & Msg)
                        Case False
                            FicWriter.WriteLine(Space(5) & Msg)
                    End Select
            End Select
            FicWriter.Flush()
            FicWriter.Close()
        End Sub

        Public Sub EcrireLogErreur(ByVal Origine As String, ByVal OrdreSql As String)
            Dim FicStream As System.IO.FileStream
            Dim FicWriter As System.IO.StreamWriter
            Dim NomLog As String
            Dim NomRep As String
            Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)

            NomRep = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")
            NomLog = Virtualia.Systeme.Constantes.DossierVirtualiaService("Logs") & Origine & ".log"
            FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
            FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
            FicWriter.WriteLine("-------------------------------------")
            FicWriter.WriteLine("Le " & Format(System.DateTime.Now, "G"))
            FicWriter.WriteLine(OrdreSql)
            FicWriter.WriteLine("-------------------------------------")
            FicWriter.Flush()
            FicWriter.Close()
        End Sub

        Private Sub PurgerTemporaire()
            Dim RepTemp As String = VirRepertoireTemporaire
            Try
                My.Computer.FileSystem.DeleteDirectory(RepTemp, FileIO.DeleteDirectoryOption.DeleteAllContents)
            Catch ex As Exception
                Exit Try
            End Try
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal NoBd As Integer)
            Dim CodeRetour As Boolean

            WsInstanceSgbd = VirServiceServeur.Instance_Database
            WsRhModele = VirServiceServeur.Instance_ModeleRH
            WsClientServiceWeb = Nothing
            CodeRetour = VirServiceServeur.ChangerBasedeDonneesCourante(NomUtilisateur, NoBd)
            Call InitialiserListeImages()
            WsPointeurUtiGlobal = AjouterUnUtilisateur(NomUtilisateur, NoBd)

            For Each Db In WsInstanceSgbd
                If Db.Numero = NoBd Then
                    Exit For
                End If
            Next

            WsRefCompetence = New Virtualia.Net.Entretien.ObjetRefCompetence(WsTypeEntretien)

            If WsTypeEntretien = "CNED2" Then
                WsOldCompetence_CESE_CNED = New Virtualia.Net.Entretien.ObjetRefCompetence("CNED")
            End If
            If WsTypeEntretien = "CESE2" Then
                WsOldCompetence_CESE_CNED = New Virtualia.Net.Entretien.ObjetRefCompetence("CESE")
            End If
        End Sub

        Public Sub New()
            Call PurgerTemporaire()
        End Sub
    End Class
End Namespace