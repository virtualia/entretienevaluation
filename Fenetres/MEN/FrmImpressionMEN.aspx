﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmImpressionMEN.aspx.vb" Inherits="Virtualia.Net.FrmImpressionMEN" UICulture="Fr" %>

<%@ Register src="~/Impression/MEN/PER_ENTRETIEN_P0_MEN.ascx" tagname="ENT_P0" tagprefix="Virtualia" %>
<%@ Register src="~/Impression/MEN/PER_ENTRETIEN_P1_MEN.ascx" tagname="ENT_P1" tagprefix="Virtualia" %>
<%@ Register src="~/Impression/MEN/PER_ENTRETIEN_P2_MEN.ascx" tagname="ENT_P2" tagprefix="Virtualia" %>
<%@ Register src="~/Impression/MEN/PER_ENTRETIEN_P3_MEN.ascx" tagname="ENT_P3" tagprefix="Virtualia" %>
<%@ Register src="~/Impression/MEN/PER_ENTRETIEN_P4_MEN.ascx" tagname="ENT_P4" tagprefix="Virtualia" %>
<%@ Register src="~/Impression/MEN/PER_ENTRETIEN_P5_MEN.ascx" tagname="ENT_P5" tagprefix="Virtualia" %>
<%@ Register src="~/Impression/MEN/PER_ENTRETIEN_P6_MEN.ascx" tagname="ENT_P6" tagprefix="Virtualia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Formulaire Entretien</title>
</head>
<body>
    <form id="FrmEntretien" runat="server" style="max-width: 800px">
    <div style="margin-left : 10px">
        <asp:ImageButton ID="CommandePDF" runat="server" ImageUrl="~/Images/General/PDF_on.gif"
                     ToolTip="Editer le formulaire au format PDF" />
    </div>
    <div id="Div1" runat="server" style="width: 750px;">
        <asp:Table runat="server" ID="CadreApercu" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:ENT_P0 ID="ENTRETIEN_0" runat="server"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:ENT_P1 ID="ENTRETIEN_1" runat="server" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:ENT_P2 ID="ENTRETIEN_2" runat="server" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:ENT_P3 ID="ENTRETIEN_3" runat="server" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:ENT_P4 ID="ENTRETIEN_4" runat="server" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:ENT_P5 ID="ENTRETIEN_5" runat="server" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:ENT_P6 ID="ENTRETIEN_6" runat="server" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
    </div>
    </form>
</body>
</html>
