﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmEntretienGIP
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String = "VEntretien"
    Private Const IEntete As Integer = 0
    ''Private Const IEmploi As Integer = 1
    Private Const IResultat As Integer = 1
    Private Const IObjectifs As Integer = 2
    Private Const ICompetences As Integer = 3
    Private Const IFormation As Integer = 4
    Private Const IEvolution As Integer = 5
    Private Const IAppreciation As Integer = 6
    '
    Private WsNomStateUti As String = "Utilisateur"
    Private WsCacheUti As ArrayList
    '
    Private ReadOnly Property V_WebFonction As Virtualia.Net.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.WebFonctions(Me, 1)
            End If
            Return WebFct
        End Get
    End Property

    Public Property V_NomUtilisateur As String
        Get
            If Me.ViewState(WsNomStateUti) Is Nothing Then
                Return ""
            End If
            WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
            If WsCacheUti IsNot Nothing Then
                If WsCacheUti(0) IsNot Nothing Then
                    Return WsCacheUti(0).ToString
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            If Me.ViewState(WsNomStateUti) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateUti)
            End If
            WsCacheUti = New ArrayList
            WsCacheUti.Add(value)
            Me.ViewState.Add(WsNomStateUti, WsCacheUti)
        End Set
    End Property

    Public Property Identifiant() As Integer
        Get
            Return CInt(HSelIde.Value)
        End Get
        Set(ByVal value As Integer)
            If value = CInt(HSelIde.Value) Then
                Exit Property
            End If
            HSelIde.Value = value.ToString
            ENT_ENTETE.Identifiant = value
        End Set
    End Property

    Protected Sub Onglet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BoutonN1.Click, BoutonN2.Click,
    BoutonN3.Click, BoutonN4.Click, BoutonN5.Click, BoutonN6.Click, BoutonN7.Click

        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(950)
        Select Case CType(sender, Button).ID
            Case Is = "BoutonN1"
                MultiOnglets.SetActiveView(VueEntete)
            'Case Is = "BoutonN2"
            '    MultiOnglets.SetActiveView(VueEmploi)
            Case Is = "BoutonN2"
                MultiOnglets.SetActiveView(VueResultats)
            Case Is = "BoutonN3"
                MultiOnglets.SetActiveView(VueObjectifs)
            Case Is = "BoutonN4"
                MultiOnglets.SetActiveView(VueCompetence)
            Case Is = "BoutonN5"
                MultiOnglets.SetActiveView(VueFormation)
            Case Is = "BoutonN6"
                MultiOnglets.SetActiveView(VueEvolution)
            Case Is = "BoutonN7"
                MultiOnglets.SetActiveView(VueAppreciation)
        End Select
        Call Initialiser()
        V_WebFonction.ContexteSession(Session.SessionID).Fenetre_VueActive(VI.CategorieRH.Evaluation_Gpec) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles ENT_APPRECIATION.MessageDialogue, ENT_COMPETENCES.MessageDialogue,
    ENT_ENTETE.MessageDialogue, ENT_EVOLUTION.MessageDialogue, ENT_FORMATION.MessageDialogue,
    ENT_OBJECTIFS.MessageDialogue, ENT_RESULTATS.MessageDialogue

        Select Case e.Emetteur
            Case Is = "Entretien"
                ENT_ENTETE.V_Occurence = e.NatureMessage
                MultiOnglets.ActiveViewIndex = IEntete
                V_WebFonction.ContexteSession(Session.SessionID).Fenetre_VueActive(VI.CategorieRH.Evaluation_Gpec) = IEntete
                Exit Sub
        End Select
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = V_WebFonction.ConvertCouleur("#216B68")
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        PopupMsg.Show()
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim Msg As String = ""
        If V_NomUtilisateur = "" Then
            V_NomUtilisateur = Request.QueryString("Nomuser")
        End If
        If V_NomUtilisateur = "" Then
            Msg = "Erreur lors de l'identification du dossier accédé."
            Response.Redirect("~/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If V_WebFonction.SessionVirtualia(Session.SessionID) Is Nothing Then
            Msg = "Erreur de serveur. Vous avez été déconnecté(e)"
            Response.Redirect("~/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If V_WebFonction.VerifierCookie(Me) = False Then
            Msg = "Erreur lors de l'identification du dossier accédé. Session invalide."
            Response.Redirect("~/ErreurApplication.aspx?Msg=" & Msg)
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        ENT_ENTETE.V_NomUtilisateur = V_NomUtilisateur
        ENT_APPRECIATION.V_NomUtilisateur = V_NomUtilisateur
        ENT_COMPETENCES.V_NomUtilisateur = V_NomUtilisateur
        ENT_EVOLUTION.V_NomUtilisateur = V_NomUtilisateur
        ENT_FORMATION.V_NomUtilisateur = V_NomUtilisateur
        ENT_OBJECTIFS.V_NomUtilisateur = V_NomUtilisateur
        ENT_RESULTATS.V_NomUtilisateur = V_NomUtilisateur

        If V_WebFonction.ContexteSession(Session.SessionID).Identifiant_Courant <> 0 Then
            Identifiant = V_WebFonction.ContexteSession(Session.SessionID).Identifiant_Courant
            If V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation IsNot Nothing Then
                If V_WebFonction.SessionVirtualia(Session.SessionID).SiSelf = False Then
                    V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation.SiSelf = False
                Else 'Cas Self-Salarié
                    V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation.SiSelf = True
                End If
                MultiOnglets.ActiveViewIndex = V_WebFonction.ContexteSession(Session.SessionID).Fenetre_VueActive(VI.CategorieRH.Evaluation_Gpec)
                If V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation.Date_de_Signature_Evalue <> "" Then
                    ''***********AKR 12/09/2017
                    'CadreCmdOK.Visible = True
                    CadreCmdOK.Visible = True
                Else
                    CadreCmdOK.Visible = True
                End If
            End If
        End If
        Call Initialiser()
    End Sub

    Private Sub Initialiser()
        Dim Ctl As Control
        Dim BtnControle As System.Web.UI.WebControls.Button
        Dim IndiceI As Integer = 0
        Dim K As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreOnglets, "Bouton", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            BtnControle = CType(Ctl, System.Web.UI.WebControls.Button)
            K = CInt(Strings.Right(BtnControle.ID, 1)) - 1
            If K = MultiOnglets.ActiveViewIndex Then
                BtnControle.Font.Bold = True
                BtnControle.Height = New Unit(24)
                BtnControle.Width = New Unit(85)
                BtnControle.BackColor = V_WebFonction.ConvertCouleur("#7D9F99")
                BtnControle.ForeColor = System.Drawing.Color.White
            Else
                BtnControle.Font.Bold = False
                BtnControle.Height = New Unit(33)
                BtnControle.Width = New Unit(98)
                BtnControle.BackColor = System.Drawing.Color.Transparent
                BtnControle.ForeColor = V_WebFonction.ConvertCouleur("#142425")
            End If
            IndiceI += 1
        Loop
        ConteneurVues.Width = New Unit(820)
        CadreDateJour.Visible = True
    End Sub

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim Cretour As Boolean
        Dim DossierPER As Virtualia.Net.Entretien.DossierEntretien
        DossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If DossierPER Is Nothing Then
            Exit Sub
        End If
        ''********AKR 12/09/2017
        'If DossierPER.Date_de_Signature_Evalue <> "" Then
        '    Exit Sub
        'End If

        Cretour = DossierPER.MettreAJour
        If Cretour = True And (DossierPER.SiEMailAEnvoyer = True Or DossierPER.SiEMailManagerAEnvoyer = True) Then
            V_WebFonction.ContexteSession(Session.SessionID).SiMailAfaire = True
        End If

        Dim TitreMsg As String = "Enregistrement des entretiens professionnels"
        Dim Msg As String
        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        If Cretour = True Then
            Msg = "L'enregistrement a bien été effectué."
        Else
            Msg = "Incident au moment de l'enregistrement."
        End If
        CadreDateJour.Visible = False
        Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("MajDossier", MultiOnglets.ActiveViewIndex, DossierPER.LibelleIdentite, "OK", TitreMsg, Msg)
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = V_WebFonction.ConvertCouleur("#216B68")
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = Evenement
        PopupMsg.Show()
    End Sub

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(950)
        V_WebFonction.ContexteSession(Session.SessionID).Fenetre_VueActive(VI.CategorieRH.Evaluation_Gpec) = e.NumeroObjet
        '********************************* MAIL ***********************************************
        If V_WebFonction.ContexteSession(Session.SessionID).SiMailAfaire = True Then
            Dim SioK As Boolean
            Dim MsgContenu As String
            Dim DossierPER As Virtualia.Net.Entretien.DossierEntretien
            Dim Sujet As String = ""
            DossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
            If DossierPER Is Nothing Then
                V_WebFonction.ContexteSession(Session.SessionID).SiMailAfaire = False
                Call Initialiser()
                MultiOnglets.ActiveViewIndex = e.NumeroObjet
                Exit Sub
            End If
            If DossierPER.SiEMailManagerAEnvoyer = True Then
                If DossierPER.EmailPro <> "" Then
                    MsgContenu = "Bonjour " & DossierPER.Qualite & Strings.Space(1) & DossierPER.Prenom & Strings.Space(1) & DossierPER.Nom & "," & vbCrLf
                    MsgContenu &= "Votre compte-rendu d'entretien professionnel " & DossierPER.Annee & " vient d'être validé, c'est-à-dire daté et enregistré par "
                    MsgContenu &= DossierPER.QualitePrenomNomManager & "."
                    MsgContenu &= " A présent, il vous appartient de le valider également via Virtualia." & vbCrLf
                    MsgContenu &= "Cordialement" & vbCrLf
                    MsgContenu &= ""
                    Sujet = "Sujet" & "Votre compte-rendu d'entretien professionnel a été validé par votre évaluateur"
                    DossierPER.SiEMailAEnvoyer = False
                    V_WebFonction.ContexteSession(Session.SessionID).SiMailAfaire = False
                    Dim WseOutil As New Virtualia.Net.WebService.Outils(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaUtils"))
                    Try
                        SioK = WseOutil.EnvoyerEmail(Sujet, DossierPER.EmailPro, MsgContenu, False)
                    Catch ex As Exception
                        Exit Try
                    End Try
                End If
                If DossierPER.EmailManager <> "" Then
                    MsgContenu = "Bonjour " & DossierPER.QualitePrenomNomManager & "," & vbCrLf
                    MsgContenu &= "Votre validation du compte-rendu d'entretien professionnel " & DossierPER.Annee & ", de "
                    MsgContenu &= DossierPER.Qualite & Strings.Space(1) & DossierPER.Prenom & Strings.Space(1) & DossierPER.Nom
                    MsgContenu &= ", via Virtualia, a bien été prise en compte. A présent, l'agent a été invité à valider également le document." & vbCrLf
                    MsgContenu &= "Cordialement" & vbCrLf
                    MsgContenu &= ""
                    Sujet = "Sujet" & "Votre validation du compte-rendu d'entretien professionnel a été prise en compte"
                    DossierPER.SiEMailAEnvoyer = False
                    V_WebFonction.ContexteSession(Session.SessionID).SiMailAfaire = False
                    Dim WseOutil As New Virtualia.Net.WebService.Outils(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaUtils"))
                    Try
                        SioK = WseOutil.EnvoyerEmail(Sujet, DossierPER.EmailManager, MsgContenu, False)
                    Catch ex As Exception
                        Exit Try
                    End Try
                End If
                Call Initialiser()
                MultiOnglets.ActiveViewIndex = e.NumeroObjet
            End If
            If DossierPER.SiEMailAEnvoyer = True Then
                If System.Configuration.ConfigurationManager.AppSettings("ChaineEmail") <> "" Then
                    MsgContenu = "L'entretien de " & DossierPER.Prenom & Strings.Space(1) & DossierPER.Nom
                    MsgContenu &= " - " & DossierPER.NiveauAffectation(V_WebFonction.ViRhDates.DateduJour, 1)
                    MsgContenu &= " a été validé : date de signature de l'agent le " & DossierPER.Objet_150.Date_de_Signature_Evalue
                    Sujet = "Sujet" & "L'entretien de " & DossierPER.Prenom & Strings.Space(1) & DossierPER.Nom & " est validé"
                    DossierPER.SiEMailAEnvoyer = False
                    V_WebFonction.ContexteSession(Session.SessionID).SiMailAfaire = False
                    Dim WseOutil As New Virtualia.Net.WebService.Outils(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaUtils"))
                    Try
                        SioK = WseOutil.EnvoyerEmail(Sujet, System.Configuration.ConfigurationManager.AppSettings("ChaineEmail"), MsgContenu, False)
                    Catch ex As Exception
                        Exit Try
                    End Try
                End If
                If DossierPER.EmailPro <> "" Then
                    MsgContenu = "Bonjour " & DossierPER.Qualite & Strings.Space(1) & DossierPER.Prenom & Strings.Space(1) & DossierPER.Nom & "," & vbCrLf
                    MsgContenu &= "Vous venez de dater et d'enregistrer votre compte-rendu d'entretien professionnel " & DossierPER.Annee & ", "
                    MsgContenu &= "via Virtualia. Le document est à présent validé." & vbCrLf
                    MsgContenu &= "Cordialement" & vbCrLf
                    MsgContenu &= ""
                    Sujet = "Sujet" & "Votre compte-rendu d'entretien professionnel est validé"
                    DossierPER.SiEMailAEnvoyer = False
                    V_WebFonction.ContexteSession(Session.SessionID).SiMailAfaire = False
                    Dim WseOutil As New Virtualia.Net.WebService.Outils(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaUtils"))
                    Try
                        SioK = WseOutil.EnvoyerEmail(Sujet, DossierPER.EmailPro, MsgContenu, False)
                    Catch ex As Exception
                        Exit Try
                    End Try
                End If
                If DossierPER.EmailManager <> "" Then
                    MsgContenu = "Bonjour " & DossierPER.QualitePrenomNomManager & "," & vbCrLf
                    MsgContenu &= DossierPER.Qualite & Strings.Space(1) & DossierPER.Prenom & Strings.Space(1) & DossierPER.Nom
                    MsgContenu &= " vient de dater et signer, via Virtualia, son compte-rendu d'entretien professionnel " & DossierPER.Annee & "."
                    MsgContenu &= " Le document est à présent validé." & vbCrLf
                    MsgContenu &= "Cordialement" & vbCrLf
                    MsgContenu &= ""
                    Sujet = "Sujet" & "L'entretien de " & DossierPER.Prenom & Strings.Space(1) & DossierPER.Nom & " est validé"
                    DossierPER.SiEMailAEnvoyer = False
                    V_WebFonction.ContexteSession(Session.SessionID).SiMailAfaire = False
                    Dim WseOutil As New Virtualia.Net.WebService.Outils(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaUtils"))
                    Try
                        SioK = WseOutil.EnvoyerEmail(Sujet, DossierPER.EmailManager, MsgContenu, False)
                    Catch ex As Exception
                        Exit Try
                    End Try
                End If
                Call Initialiser()
                MultiOnglets.ActiveViewIndex = e.NumeroObjet
            End If
        End If
        '************************************************************************************************************
        Call Initialiser()
        MultiOnglets.ActiveViewIndex = e.NumeroObjet
    End Sub

    Protected Sub CommandeImprimer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeImprimer.Click
        Dim DossierPER As Virtualia.Net.Entretien.DossierEntretien
        DossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If DossierPER Is Nothing Then
            Exit Sub
        End If
        If DossierPER.Date_de_Signature_Evalue = "" Then
            Dim Cretour As Boolean
            Cretour = DossierPER.MettreAJour
        End If
        Response.Redirect("~/Fenetres/ENM/FrmImpressionENM.aspx?Nomuser=" & V_NomUtilisateur)

    End Sub


    Protected Sub EtiDateJour_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles EtiDateJour.PreRender
        EtiDateJour.Text = Strings.Format(System.DateTime.Now, "D")
        EtiHeure.Text = Strings.Format(TimeOfDay, "t")
    End Sub


End Class
