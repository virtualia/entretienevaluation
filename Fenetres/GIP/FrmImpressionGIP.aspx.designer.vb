﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmImpressionGIP
    
    '''<summary>
    '''Contrôle Head1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead
    
    '''<summary>
    '''Contrôle FrmEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents FrmEntretien As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''Contrôle CommandePDF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandePDF As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle Div1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Div1 As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''Contrôle CadreApercu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreApercu As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle ENTRETIEN_0.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_0 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P0_ENM
    
    '''<summary>
    '''Contrôle ENTRETIEN_1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_1 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P1_ENM
    
    '''<summary>
    '''Contrôle ENTRETIEN_2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_2 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P2_ENM
    
    '''<summary>
    '''Contrôle ENTRETIEN_3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_3 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P3_ENM
    
    '''<summary>
    '''Contrôle ENTRETIEN_4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_4 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P4_ENM
    
    '''<summary>
    '''Contrôle ENTRETIEN_5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_5 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P5_ENM
    
    '''<summary>
    '''Contrôle ENTRETIEN_6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_6 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P6_ENM
    
    '''<summary>
    '''Contrôle ENTRETIEN_7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_7 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P7_ENM
    
    '''<summary>
    '''Contrôle ENTRETIEN_8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_8 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P8_ENM
    
    '''<summary>
    '''Contrôle HSelIde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelIde As Global.System.Web.UI.WebControls.HiddenField
End Class
