﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmImpressionCESE
    
    '''<summary>
    '''Contrôle Head1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead
    
    '''<summary>
    '''Contrôle FrmEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents FrmEntretien As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''Contrôle CommandePDF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandePDF As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle Div1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Div1 As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''Contrôle CadreApercu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreApercu As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle ENTRETIEN_00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_00 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P0_CESE
    
    '''<summary>
    '''Contrôle ENTRETIEN_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_01 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P1_CESE
    
    '''<summary>
    '''Contrôle ENTRETIEN_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_02 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P2_CESE
    
    '''<summary>
    '''Contrôle ENTRETIEN_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_03 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P3_CESE
    
    '''<summary>
    '''Contrôle ENTRETIEN_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_04 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P4_CESE
    
    '''<summary>
    '''Contrôle ENTRETIEN_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_05 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P5_CESE
    
    '''<summary>
    '''Contrôle ENTRETIEN_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ENTRETIEN_06 As Global.Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P6_CESE
    
    '''<summary>
    '''Contrôle HSelIde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelIde As Global.System.Web.UI.WebControls.HiddenField
End Class
