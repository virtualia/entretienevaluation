﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmEntretienHonda
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private Const IEntete As Integer = 0
    Private Const IPerformance As Integer = 1
    Private Const IComportement As Integer = 2
    Private Const IFormation As Integer = 3
    Private Const ITravail As Integer = 4
    '
    Private WsNomStateUti As String = "VEntretien"
    '
    Private ReadOnly Property V_WebFonction As Virtualia.Net.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.WebFonctions(Me, 1)
            End If
            Return WebFct
        End Get
    End Property

    Public Property V_NomUtilisateur As String
        Get
            If Me.ViewState(WsNomStateUti) Is Nothing Then
                Return ""
            End If
            Dim VCache As List(Of String)
            VCache = CType(Me.ViewState(WsNomStateUti), List(Of String))
            If VCache IsNot Nothing Then
                Return VCache.Item(0)
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            Dim VCache As List(Of String)
            If Me.ViewState(WsNomStateUti) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomStateUti), List(Of String))
                Me.ViewState.Remove(WsNomStateUti)
                VCache.Item(0) = value
            Else
                VCache = New List(Of String)
                VCache.Add(value)
                VCache.Add("")
            End If
            Me.ViewState.Add(WsNomStateUti, VCache)
        End Set
    End Property

    Public Property V_PeriodeKI As String
        Get
            If Me.ViewState(WsNomStateUti) Is Nothing Then
                Return ""
            End If
            Dim VCache As List(Of String)
            VCache = CType(Me.ViewState(WsNomStateUti), List(Of String))
            If VCache IsNot Nothing Then
                Return VCache.Item(1)
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            Dim VCache As List(Of String)
            If Me.ViewState(WsNomStateUti) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomStateUti), List(Of String))
                If VCache.Item(1) <> value Then
                    Me.ViewState.Remove(WsNomStateUti)
                    VCache.Item(1) = value
                    Me.ViewState.Add(WsNomStateUti, VCache)
                End If
            End If
        End Set
    End Property

    Public Property Identifiant() As Integer
        Get
            Return CInt(HSelIde.Value)
        End Get
        Set(ByVal value As Integer)
            If value = CInt(HSelIde.Value) Then
                Exit Property
            End If
            HSelIde.Value = value.ToString
            ENT_ENTETE.Identifiant = value
        End Set
    End Property

    Protected Sub Onglet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BoutonN1.Click, BoutonN2.Click, _
    BoutonN3.Click, BoutonN4.Click, BoutonN5.Click

        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(950)
        Select Case CType(sender, Button).ID
            Case "BoutonN1"
                MultiOnglets.SetActiveView(VueEntete)
            Case "BoutonN2"
                MultiOnglets.SetActiveView(VuePerformance)
            Case "BoutonN3"
                MultiOnglets.SetActiveView(VueComportement)
            Case "BoutonN4"
                MultiOnglets.SetActiveView(VueFormation)
            Case "BoutonN5"
                MultiOnglets.SetActiveView(VueTravail)
        End Select
        Call Initialiser()
        V_WebFonction.ContexteSession(Session.SessionID).Fenetre_VueActive(VI.CategorieRH.Evaluation_Gpec) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles ENT_ENTETE.MessageDialogue, ENT_COMPORTEMENT.MessageDialogue, ENT_FORMATION.MessageDialogue, _
    ENT_PERFORMANCE.MessageDialogue

        Select Case e.Emetteur
            Case Is = "Entretien"
                ENT_ENTETE.V_Occurence = e.NatureMessage
                MultiOnglets.ActiveViewIndex = IEntete
                V_WebFonction.ContexteSession(Session.SessionID).Fenetre_VueActive(VI.CategorieRH.Evaluation_Gpec) = IEntete
                Exit Sub
        End Select
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = V_WebFonction.ConvertCouleur("#216B68")
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        PopupMsg.Show()
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim Msg As String = ""
        If V_NomUtilisateur = "" Then
            V_NomUtilisateur = Request.QueryString("Nomuser")
        End If
        If V_NomUtilisateur = "" Then
            Msg = "Erreur lors de l'identification du dossier accédé."
            Response.Redirect("~/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If V_WebFonction.SessionVirtualia(Session.SessionID) Is Nothing Then
            Msg = "Erreur de serveur. Vous avez été déconnecté(e)"
            Response.Redirect("~/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If V_WebFonction.VerifierCookie(Me) = False Then
            Msg = "Erreur lors de l'identification du dossier accédé. Session invalide."
            Response.Redirect("~/ErreurApplication.aspx?Msg=" & Msg)
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DossierPER As Virtualia.Net.Entretien.DossierHonda = Nothing

        ENT_ENTETE.V_NomUtilisateur = V_NomUtilisateur
        ENT_PERFORMANCE.V_NomUtilisateur = V_NomUtilisateur
        ENT_COMPORTEMENT.V_NomUtilisateur = V_NomUtilisateur
        ENT_FORMATION.V_NomUtilisateur = V_NomUtilisateur
        ENT_TRAVAIL.V_NomUtilisateur = V_NomUtilisateur

        If V_WebFonction.ContexteSession(Session.SessionID).Identifiant_Courant <> 0 Then
            Identifiant = V_WebFonction.ContexteSession(Session.SessionID).Identifiant_Courant
            DossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
            If DossierPER IsNot Nothing Then
                If V_WebFonction.SessionVirtualia(Session.SessionID).SiSelf = True Then
                    DossierPER.SiSelf = True
                End If
                If V_WebFonction.SessionVirtualia(Session.SessionID).SiDRH = True Then
                    DossierPER.SiDRH = True
                End If
                V_PeriodeKI = DossierPER.PeriodeKI(DossierPER.Objet_150.Date_de_Valeur)
                If DossierPER.SiEntretienAccessible = False Then
                    Dim Msg As String = "Le formulaire d'entretien professionnel est en période : "
                    Msg &= V_PeriodeKI
                    Msg &= " . Il n'est pas accessible pendant cette période."
                    Response.Redirect("~/ErreurApplication.aspx?Msg=" & Msg)
                    Exit Sub
                End If
                MultiOnglets.ActiveViewIndex = V_WebFonction.ContexteSession(Session.SessionID).Fenetre_VueActive(VI.CategorieRH.Evaluation_Gpec)
                If DossierPER.Date_de_Signature_Evalue <> "" And DossierPER.SiDRH = False Then
                    CadreCmdOK.Visible = False
                Else
                    CadreCmdOK.Visible = True
                End If
            End If
        End If
        Call Initialiser()
    End Sub

    Private Sub Initialiser()
        Dim Ctl As Control
        Dim BtnControle As System.Web.UI.WebControls.Button
        Dim IndiceI As Integer = 0
        Dim K As Integer = 0

        If V_PeriodeKI = "FIN KI" Then
            CadreOnglets.Width = New Unit(500)
            CelluleN5.Visible = True
        Else
            CadreOnglets.Width = New Unit(400)
            CelluleN5.Visible = False
        End If
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreOnglets, "Bouton", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            BtnControle = CType(Ctl, System.Web.UI.WebControls.Button)
            K = CInt(Strings.Right(BtnControle.ID, 1)) - 1
            If K = MultiOnglets.ActiveViewIndex Then
                BtnControle.Font.Bold = True
                BtnControle.Height = New Unit(24)
                BtnControle.Width = New Unit(85)
                BtnControle.BackColor = V_WebFonction.ConvertCouleur("#7D9F99")
                BtnControle.ForeColor = System.Drawing.Color.White
            Else
                BtnControle.Font.Bold = False
                BtnControle.Height = New Unit(33)
                BtnControle.Width = New Unit(98)
                BtnControle.BackColor = System.Drawing.Color.Transparent
                BtnControle.ForeColor = V_WebFonction.ConvertCouleur("#142425")
            End If
            IndiceI += 1
        Loop
        ConteneurVues.Width = New Unit(820)
        CadreDateJour.Visible = True
    End Sub

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim Cretour As Boolean
        Dim DossierPER As Virtualia.Net.Entretien.DossierHonda
        DossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If DossierPER Is Nothing Then
            Exit Sub
        End If
        If DossierPER.Date_de_Signature_Evalue <> "" And DossierPER.SiDRH = False Then
            Exit Sub
        End If
        Cretour = DossierPER.MettreAJour

        Dim TitreMsg As String = "Enregistrement des entretiens professionnels"
        Dim Msg As String
        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        If Cretour = True Then
            Msg = "L'enregistrement a bien été effectué."
            If DossierPER.Objet_150.SiBesoin_Formation = "Oui" And DossierPER.Objet_153("1").Intitule_Besoin = "" Then
                Msg &= vbCrLf & "Pensez à saisir les besoins en formation."
            End If
        Else
            Msg = "Incident au moment de l'enregistrement."
        End If
        CadreDateJour.Visible = False
        Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("MajDossier", CShort(MultiOnglets.ActiveViewIndex), DossierPER.LibelleIdentite, "OK", TitreMsg, Msg)
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = V_WebFonction.ConvertCouleur("#216B68")
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = Evenement
        PopupMsg.Show()
    End Sub

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(950)
        V_WebFonction.ContexteSession(Session.SessionID).Fenetre_VueActive(VI.CategorieRH.Evaluation_Gpec) = e.NumeroObjet

        Call Initialiser()
        MultiOnglets.ActiveViewIndex = e.NumeroObjet
    End Sub

    Protected Sub CommandeImprimer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeImprimer.Click
        Dim DossierPER As Virtualia.Net.Entretien.DossierHonda
        DossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If DossierPER Is Nothing Then
            Exit Sub
        End If
        If DossierPER.Date_de_Signature_Evalue = "" Then
            Dim Cretour As Boolean
            Cretour = DossierPER.MettreAJour
        End If
        'Response.Redirect("~/Fenetres/HONDA/FrmImpressionHONDA.aspx?Nomuser=" & V_NomUtilisateur)
    End Sub

    Protected Sub EtiDateJour_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles EtiDateJour.PreRender
        EtiDateJour.Text = Strings.Format(System.DateTime.Now, "D")
        EtiHeure.Text = Strings.Format(TimeOfDay, "t")
    End Sub
End Class