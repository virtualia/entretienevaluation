﻿Public Class FrmImpressionCNM
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String = "VImpression"
    Private WsNomStateUti As String = "Utilisateur"
    Private WsCacheUti As ArrayList

    Public Property V_NomUtilisateur As String
        Get
            If Me.ViewState(WsNomStateUti) Is Nothing Then
                Return ""
            End If
            WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
            If WsCacheUti IsNot Nothing Then
                If WsCacheUti(0) IsNot Nothing Then
                    Return WsCacheUti(0).ToString
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            If Me.ViewState(WsNomStateUti) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateUti)
            End If
            WsCacheUti = New ArrayList
            WsCacheUti.Add(value)
            Me.ViewState.Add(WsNomStateUti, WsCacheUti)
        End Set
    End Property

    Public Property Identifiant() As Integer
        Get
            Return CInt(HSelIde.Value)
        End Get
        Set(ByVal value As Integer)
            If value = CInt(HSelIde.Value) Then
                Exit Property
            End If
            HSelIde.Value = value.ToString
            ENTRETIEN_0.Identifiant = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim Msg As String = ""
        If V_NomUtilisateur = "" Then
            V_NomUtilisateur = Request.QueryString("Nomuser")
        End If
        If V_NomUtilisateur = "" Then
            Msg = "Erreur lors de l'identification du dossier accédé."
            Response.Redirect("~/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        If WebFct.SessionVirtualia(Session.SessionID) Is Nothing Then
            Msg = "Erreur de serveur. Vous avez été déconnecté(e)"
            Response.Redirect("~/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If WebFct.VerifierCookie(Me) = False Then
            Msg = "Erreur lors de l'identification du dossier accédé. Session invalide."
            Response.Redirect("~/ErreurApplication.aspx?Msg=" & Msg)
        End If
    End Sub

    Private Sub FrmImpression_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        ENTRETIEN_0.V_NomUtilisateur = V_NomUtilisateur
        ENTRETIEN_1.V_NomUtilisateur = V_NomUtilisateur
        ENTRETIEN_2.V_NomUtilisateur = V_NomUtilisateur
        ENTRETIEN_3.V_NomUtilisateur = V_NomUtilisateur
        ENTRETIEN_4.V_NomUtilisateur = V_NomUtilisateur
        ENTRETIEN_5.V_NomUtilisateur = V_NomUtilisateur
        ENTRETIEN_6.V_NomUtilisateur = V_NomUtilisateur

        If WebFct.ContexteSession(Session.SessionID).Identifiant_Courant <> 0 Then
            Identifiant = WebFct.ContexteSession(Session.SessionID).Identifiant_Courant
            If WebFct.ContexteSession(Session.SessionID).DossierEvaluation IsNot Nothing Then
                If WebFct.SessionVirtualia(Session.SessionID).SiSelf = False Then
                    WebFct.ContexteSession(Session.SessionID).DossierEvaluation.SiSelf = False
                Else 'Cas Self-Salarié
                    WebFct.ContexteSession(Session.SessionID).DossierEvaluation.SiSelf = True
                End If
            End If
        End If
    End Sub

    Private Sub Page_PreRenderComplete(sender As Object, e As EventArgs) Handles Me.PreRenderComplete
        Dim FichierHtml As String
        FichierHtml = WebFct.PointeurGlobal.VirRepertoireTemporaire & "\" & Session.SessionID & ".html"
        'Framework 4.0
        Response.ContentEncoding = System.Text.Encoding.Unicode
        Response.Filter = New Virtualia.Systeme.Outils.FiltreReponseParFileStream(Response.Filter, FichierHtml)
        'Framework 4.5
        'Response.ContentEncoding = System.Text.Encoding.UTF8
        'Response.Filter = New Virtualia.Systeme.Outils.FiltreReponseParMemoryStream(Response.Filter, FichierHtml)
    End Sub

    Private Sub CommandePDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CommandePDF.Click
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        End If
        Dim Util As New Virtualia.Systeme.Outils.Utilitaires
        Dim FichierHtml As String
        Dim FluxHtml As String
        Dim NomPdf As String
        Dim FluxPDF As Byte()
        Dim DossierPER As Virtualia.Net.Entretien.DossierEntretien = WebFct.ContexteSession(Session.SessionID).DossierEvaluation
        If DossierPER IsNot Nothing Then
            NomPdf = WebFct.ViRhFonction.ChaineSansAccent(DossierPER.Nom, False) & "_" & WebFct.ViRhFonction.ChaineSansAccent(DossierPER.Prenom, False) & ".pdf"
        Else
            NomPdf = Session.SessionID & ".pdf"
        End If
        FichierHtml = WebFct.PointeurGlobal.VirRepertoireTemporaire & "\" & Session.SessionID & ".html"
        If My.Computer.FileSystem.FileExists(FichierHtml) = False Then
            Exit Sub
        End If
        Call Util.TransformeFicHtml(FichierHtml)

        Dim WseOutil As New Virtualia.Net.WebService.Outils(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaUtils"))
        FluxHtml = My.Computer.FileSystem.ReadAllText(FichierHtml, System.Text.Encoding.GetEncoding(1252))
        FluxPDF = WseOutil.ConversionPDF(FluxHtml, False)
        WseOutil.Dispose()

        If FluxPDF IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & NomPdf & "; size=" & FluxPDF.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxPDF)
            response.Flush()
            response.End()
        End If

    End Sub
End Class