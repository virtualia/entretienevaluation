﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Datas
    Public Class ObjetUtilisateur
        Private WsRhPartition As Virtualia.Metier.Expertes.Partition = Nothing
        Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
        Private WsParent As Virtualia.Net.WebAppli.ObjetGlobal
        Private WsChaineActivite As System.Text.StringBuilder
        Private WsNomUtilisateur As String = ""
        Private WsPrenomUtilisateur As String = ""
        Private WsSgbdUtilisateur As Integer = 0
        Private WsFiltreEtablissement As String = ""
        '
        Private WsSiCouleurDefaut As Boolean = True
        '
        Private WsCritereObjetArmoire As Integer = 0
        Private WsCritereInfoArmoire As Integer = 0
        Private WsCritereSiObjetDate As Boolean = False
        Private WsCritereCouleur(14) As Integer
        Private WsCritereValeur(14) As String
        '
        Private Const BaseComplete As Integer = 0
        Private Const EnActivite As Integer = 1
        Private Const Personnalisee As Integer = 1000
        Private Const LePanel As Integer = 1001
        '
        'TS - Objet Navigation pour le contexte
        Private WsContexte As Virtualia.Net.Datas.ObjetNavigation

        Public ReadOnly Property VParent() As Virtualia.Net.WebAppli.ObjetGlobal
            Get
                Return WsParent
            End Get
        End Property

        Public ReadOnly Property PointeurDllExpert() As Virtualia.Metier.Expertes.Partition
            Get
                If WsRhPartition Is Nothing Then
                    WsRhPartition = New Virtualia.Metier.Expertes.Partition(WsParent.VirModele, InstanceBd)
                    WsRhPartition.NomUtilisateur = WsNomUtilisateur
                    WsRhPartition.PreselectiondeDossiers = Nothing
                End If
                Return WsRhPartition
            End Get
        End Property

        Public ReadOnly Property PointeurContexte() As Virtualia.Net.Datas.ObjetNavigation
            Get
                If WsContexte Is Nothing Then
                    WsContexte = New Virtualia.Net.Datas.ObjetNavigation(Me)
                End If
                Return WsContexte
            End Get
        End Property

        Public ReadOnly Property Nom() As String
            Get
                Return WsNomUtilisateur
            End Get
        End Property

        Public ReadOnly Property Prenom() As String
            Get
                Return WsPrenomUtilisateur
            End Get
        End Property

        Public Property SiCouleurParDefaut() As Boolean
            Get
                Return WsSiCouleurDefaut
            End Get
            Set(ByVal value As Boolean)
                WsSiCouleurDefaut = value
            End Set
        End Property

        Public ReadOnly Property CritereObjetArmoire() As Integer
            Get
                Return WsCritereObjetArmoire
            End Get
        End Property

        Public ReadOnly Property CritereInfoArmoire() As Integer
            Get
                Return WsCritereInfoArmoire
            End Get
        End Property

        Public ReadOnly Property SiCritereObjetDate() As Boolean
            Get
                Return WsCritereSiObjetDate
            End Get
        End Property

        Public Property CritereCouleurArmoire(ByVal Index As Integer) As Integer
            Get
                Select Case Index
                    Case 0 To 14
                        Return WsCritereCouleur(Index)
                    Case Else
                        Return 15
                End Select
            End Get
            Set(ByVal value As Integer)
                Dim IconeSave As Integer = 0
                Dim IndiceI As Integer
                Select Case Index
                    Case 0 To 14
                        IconeSave = WsCritereCouleur(Index)
                        WsCritereCouleur(Index) = value
                        For IndiceI = 0 To 14
                            If WsCritereCouleur(IndiceI) = value And IndiceI <> Index Then
                                WsCritereCouleur(IndiceI) = IconeSave
                                Exit For
                            End If
                        Next IndiceI
                End Select
            End Set
        End Property

        Public Property CritereValeurArmoire(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 14
                        Return WsCritereValeur(Index)
                    Case Else
                        Return ""
                End Select
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 14
                        WsCritereValeur(Index) = value
                End Select
            End Set
        End Property

        Public ReadOnly Property ChaineActivite() As String
            Get
                Return WsChaineActivite.ToString
            End Get
        End Property

        Public ReadOnly Property TableActivite() As ArrayList
            Get
                Dim I As Integer
                Dim TabActivite As New ArrayList
                Dim TableauData(0) As String
                TableauData = Strings.Split(ChaineActivite, VI.PointVirgule, -1)
                For I = 0 To TableauData.Count - 1
                    TabActivite.Add(TableauData(I))
                Next I
                Return TabActivite
            End Get
        End Property

        Public ReadOnly Property NumeroBaseActive() As Integer
            Get
                Return WsSgbdUtilisateur
            End Get
        End Property

        Private Sub Initialiser()
            WsContexte = Nothing
        End Sub

        Private Sub LireSessionUti()
            Dim LstUti As List(Of Virtualia.TablesObjet.Wcf.UtilisateurType)
            WsFiltreEtablissement = ""
            WsPrenomUtilisateur = ""
            LstUti = WsParent.VirServiceServeur.ListeUtilisateurs("Tous", WsSgbdUtilisateur)
            If LstUti Is Nothing Then
                Exit Sub
            End If
            For Each Uti In LstUti
                If Uti.Nom = WsNomUtilisateur Then
                    WsPrenomUtilisateur = Uti.Prenom
                    If Uti.FiltreEtablissement IsNot Nothing AndAlso Uti.FiltreEtablissement.Count > 0 Then
                        WsFiltreEtablissement = Uti.FiltreEtablissement.Item(0)
                    End If
                    Exit For
                End If
            Next
        End Sub

        Public Function FiltreEtablissement(ByVal NoBd As Integer) As String
            Select Case NoBd
                Case Is = 0
                    Return WsFiltreEtablissement
                Case Else
                    Call LireSessionUti()
                    Return WsFiltreEtablissement
            End Select
        End Function

        Private Sub ChercherPositionsdActivite()
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim ChaineSql As String
            Dim LstRes As List(Of String)
            Dim TableauData(0) As String

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, InstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVuePosition, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.NoInfoSelection(0, 1) = 8
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = "Oui;"
            Constructeur.InfoExtraite(0, 1, 0) = 1
            ChaineSql = Constructeur.OrdreSqlDynamique

            LstRes = WsParent.VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVuePosition, 1, ChaineSql)

            WsChaineActivite = New System.Text.StringBuilder
            For Each Resultat In LstRes
                If Resultat = "" Then
                    Exit For
                End If
                TableauData = Split(Resultat, VI.Tild, -1)
                WsChaineActivite.Append(TableauData(1) & VI.PointVirgule)
            Next
        End Sub

        Public Function LireTableGeneraleSimple(ByVal Intitule As String) As String
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim ChaineSql As String
            Dim LstRes As List(Of String)
            Dim ChaineRes As System.Text.StringBuilder

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, InstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueGeneral, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Intitule
            Constructeur.InfoExtraite(0, 2, 1) = 1
            Constructeur.InfoExtraite(1, 2, 0) = 2
            ChaineSql = Constructeur.OrdreSqlDynamique

            LstRes = WsParent.VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVueGeneral, 2, ChaineSql)
            If LstRes Is Nothing Then
                Return ""
            End If
            ChaineRes = New System.Text.StringBuilder
            For Each Resultat In LstRes
                If Resultat = "" Then
                    Exit For
                End If
                ChaineRes.Append(Resultat & VI.SigneBarre)
            Next
            Return ChaineRes.ToString
        End Function

        Public Function LireTableSysReference(ByVal NoPvue As Integer) As String
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim ChaineSql As String
            Dim LstRes As List(Of String)
            Dim ChaineRes As System.Text.StringBuilder

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, InstanceBd)
            Constructeur.NombredeRequetes(NoPvue, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.InfoExtraite(0, 1, 1) = 1
            ChaineSql = Constructeur.OrdreSqlDynamique

            LstRes = WsParent.VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, NoPvue, 1, ChaineSql)
            If LstRes Is Nothing Then
                Return ""
            End If
            ChaineRes = New System.Text.StringBuilder
            For Each Resultat In LstRes
                If Resultat = "" Then
                    Exit For
                End If
                ChaineRes.Append(Resultat & VI.SigneBarre)
            Next
            Return ChaineRes.ToString
        End Function

        Public ReadOnly Property InstanceBd() As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase
            Get
                Dim IndiceI As Integer
                For IndiceI = 0 To WsParent.VirSgbd.NombredeDatabases - 1
                    Select Case WsParent.VirSgbd.Item(IndiceI).Numero
                        Case Is = WsSgbdUtilisateur
                            Return WsParent.VirSgbd.Item(IndiceI)
                            Exit Property
                    End Select
                Next IndiceI
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property VirRhDates() As Virtualia.Systeme.Fonctions.CalculDates
            Get
                Return WsRhDates
            End Get
        End Property

        Public ReadOnly Property VirRhFonction() As Virtualia.Systeme.Fonctions.Generales
            Get
                Return WsRhFonction
            End Get
        End Property

        Public Sub New(ByVal Host As Virtualia.Net.WebAppli.ObjetGlobal, ByVal NomUtilisateur As String, ByVal NoBd As Integer)
            WsParent = Host
            WsNomUtilisateur = NomUtilisateur
            WsSgbdUtilisateur = NoBd
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
            WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
            Call LireSessionUti()
            Call ChercherPositionsdActivite()
        End Sub

    End Class
End Namespace
