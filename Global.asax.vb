﻿Imports System.Web.SessionState

Public Class Global_asax
    Inherits System.Web.HttpApplication
    Private VirObjetGlobal As Virtualia.Net.WebAppli.ObjetGlobal

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque l'application est démarrée
        Dim Vuser As String
        Dim NoBd As Integer
        Dim Pw As String

        Vuser = System.Configuration.ConfigurationManager.AppSettings("UserVirtualia")
        NoBd = CInt(System.Configuration.ConfigurationManager.AppSettings("NumeroDatabase"))
        Pw = System.Configuration.ConfigurationManager.AppSettings("Password")

        VirObjetGlobal = New Virtualia.Net.WebAppli.ObjetGlobal

        Try
            Select Case VirObjetGlobal.VirServiceServeur.OuvertureSession(Vuser, Pw, NoBd)
                Case Virtualia.Systeme.Constantes.CnxUtilisateur.CnxOK, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxDejaConnecte, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxReprise, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxVirtualia
                    Application.Add("Database", NoBd)
                Case Else
                    Call EcrireLog("---- Erreur à la connexion au ServiceServeur ---" & System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur"))
                    Me.Dispose()
                    Exit Sub
            End Select
        Catch ex As Exception
            Call EcrireLog("---- Erreur ServiceServeur ne répond pas ---" & System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur") & Strings.Space(1) & ex.Message)
            Me.Dispose()
            Exit Sub
        End Try

        Try
            VirObjetGlobal = New Virtualia.Net.WebAppli.ObjetGlobal(Vuser, NoBd)
            Application.Add("VirGlobales", VirObjetGlobal)
        Catch ex As Exception
            Call EcrireLog("---- Erreur au Chargement de l'application ---" & Strings.Space(1) & ex.Message)
            Me.Dispose()
            Exit Sub
        End Try
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque la session est démarrée
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche au début de chaque demande
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lors d'une tentative d'authentification de l'utilisation
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsqu'une erreur se produit
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque la session se termine
        Try
            VirObjetGlobal = CType(Application.Item("VirGlobales"), Virtualia.Net.WebAppli.ObjetGlobal)
            VirObjetGlobal.LibererSession(Session.SessionID)
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque l'application se termine
    End Sub

    Private Shared ReadOnly LogSync As New Object()
    Private Sub EcrireLog(ByVal Msg As String)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim NomLog As String
        Dim NomRep As String
        Dim SysFicLog As String = "WebApplication.log"
        Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
        SyncLock LogSync
            NomRep = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")
            NomLog = Virtualia.Systeme.Constantes.DossierVirtualiaService("Logs") & SysFicLog
            FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
            FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
            Try
                FicWriter.WriteLine(Format(System.DateTime.Now, "g") & Space(1) & Msg)
                FicWriter.Flush()
            Finally
                FicWriter.Close()
            End Try
        End SyncLock
    End Sub

End Class