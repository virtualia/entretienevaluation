﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class DossierEntretien
        Private WsPointeurGlobal As Virtualia.Net.WebAppli.ObjetGlobal
        Private WsRefCompetence As Virtualia.Net.Entretien.ObjetRefCompetence
        Private WsNomUtiSGBD As String
        Private WsNomUtilisateur As String

        Private WsIdentifiant As Integer = 0
        Private WsListeFonds As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Private WsListeEntretiens As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Private WsFicheEtatCivil As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
        '
        Private WsAnnee As String = ""
        Private WsObjet_150 As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL
        Private WsLst_151 As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT)
        Private WsLst_152 As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF)
        Private WsLst_153 As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE)
        Private WsLst_154 As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBSERVATION)
        Private WsLst_155 As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION)
        Private WsLst_156 As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE)
        Private WsLst_157 As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT)
        ''********* AKR
        Private WsLst_METIER As List(Of Virtualia.TablesObjet.ShemaREF.MET_METIER)
        Private WsLst_ACTIVITES As List(Of Virtualia.TablesObjet.ShemaREF.MET_ACTIVITES)
        Private WsLst_APTITUDES As List(Of Virtualia.TablesObjet.ShemaREF.MET_APTITUDES)
        Private WsLst_Environement As List(Of Virtualia.TablesObjet.ShemaREF.MET_ENVIRONNEMENT)
        Private Ws_ideDossierMET As Integer
        ''*********
        '
        Private WsSiSelf As Boolean = False
        Private WsSiManager As Boolean = False
        Private WsSiN2 As Boolean = False
        Private WsDateSignature As String = ""
        Private WsDateSignatureObservations As String = ""
        Private WsDateSignatureN2 As String = ""
        Private WsDateSignatureManager As String = ""
        Private WsSiEMailAEnvoyer As Boolean = False
        Private WsSiEMailManagerAEnvoyer As Boolean = False
        Private WsSiEtape1Valide As Boolean = False
        Private WsSiEtape2Valide As Boolean = False
        Private WsSiEtape3Valide As Boolean = False
        Private WsSiEtape4Valide As Boolean = False

        '***********************************************
        Private WsIdeManager As Integer = 0
        Private WsIdeManagerN2 As Integer = 0
        Private WsNomPrenomManager As String
        Private WsNomPrenomManagerN2 As String
        Private WsQualitePrenomNomManager As String
        Private WsQualitePrenomNomManagerN2 As String
        Private WsGradeManager As String
        Private WsGradeManagerN2 As String
        Private WsCorpsManager As String
        Private WsCorpsManagerN2 As String
        Private WsFonctionManager As String
        Private WsFonctionManagerN2 As String
        Private WsEmailManager As String = ""
        Private WsEmailManagerN2 As String = ""
        Private WsPrenomManager As String = ""
        Private WsNomManager As String = ""
        Private WsStatutManager As String = ""
        Public ReadOnly Property V_Identifiant As Integer
            Get
                Return WsIdentifiant
            End Get
        End Property

        Public ReadOnly Property Qualite As String
            Get
                If WsFicheEtatCivil Is Nothing Then
                    Return ""
                    Exit Property
                End If
                Return WsFicheEtatCivil.Qualite
            End Get
        End Property

        Public ReadOnly Property Nom As String
            Get
                If WsFicheEtatCivil Is Nothing Then
                    Return ""
                    Exit Property
                End If
                Return WsFicheEtatCivil.Nom
            End Get
        End Property

        Public ReadOnly Property Prenom As String
            Get
                If WsFicheEtatCivil Is Nothing Then
                    Return ""
                    Exit Property
                End If
                Return WsFicheEtatCivil.Prenom
            End Get
        End Property

        Public ReadOnly Property NomPatronymique As String
            Get
                If WsFicheEtatCivil Is Nothing Then
                    Return ""
                    Exit Property
                End If
                Return WsFicheEtatCivil.Patronyme
            End Get
        End Property
        Public ReadOnly Property DateDeNaissance As String
            Get
                If WsFicheEtatCivil Is Nothing Then
                    Return ""
                    Exit Property
                End If
                Return WsFicheEtatCivil.Date_de_naissance
            End Get
        End Property

        Public ReadOnly Property Etablissement(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaSociete And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaSociete _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE).Administration
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Date_Entree_Etablissement(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaSociete And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaSociete _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE).Date_de_Valeur
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Statut(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaStatut And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaStatut _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_STATUT).Statut
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property SiTitulaire(ByVal DateEffet As String) As Boolean
            Get
                If DateEffet = "" Then
                    DateEffet = WsPointeurGlobal.VirRhDates.DateduJour
                End If
                Dim StatutValable As String = Statut(DateEffet).ToUpper
                '** Titulaire
                '** Titulaire sur contrat
                '** Stagiaire fonctionnaire
                If StatutValable = "TITULAIRE" Then
                    Return True
                End If
                Select Case WsRefCompetence.TypeEntretien
                    Case "CESE", "CESE2"
                        If StatutValable = "TITULAIRE SUR CONTRAT" Then
                            Return True
                        End If
                        If StatutValable = "STAGIAIRE FONCTIONNAIRE" Then
                            Return True
                        End If
                    Case Else
                        If StatutValable.StartsWith("TITULAIRE") Then
                            Return True
                        End If
                End Select
                Return False
            End Get
        End Property

        Public ReadOnly Property SiDetacheInterne(ByVal DateEffet As String) As Boolean
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Chaine As String = ""

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaActivite And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaActivite
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Chaine = WsPointeurGlobal.VirRhFonction.ChaineSansAccent(CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_POSITION).Position, True)
                End If
                If Chaine.Contains("DETACHEMENT") AndAlso Chaine.Contains("INTERNE") Then
                    Return True
                End If
                Return False
            End Get
        End Property
        Public ReadOnly Property Grade_de_Detachement(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGradeDetache And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGradeDetache
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_GRADE_DETACHE).Grade
                End If
                Return ""
            End Get
        End Property
        Public ReadOnly Property Echelon_de_Detachement(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGradeDetache And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGradeDetache
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_GRADE_DETACHE).Echelon
                End If
                Return ""
            End Get
        End Property
        Public ReadOnly Property Corps(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_GRADE).Corps
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property ModedAcces(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_GRADE).Mode_d_acces
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Grade(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_GRADE).Grade
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Echelon(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_GRADE).Echelon
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Indice(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_GRADE).Indice_brut
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Date_Echelon(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_GRADE).Date_de_Valeur
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Categorie(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaGrade _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_GRADE).Categorie
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property NiveauAffectation(ByVal DateEffet As String, ByVal Index As Integer) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    Select Case Index
                        Case 1
                            Return Fiche.Structure_de_rattachement
                        Case 2
                            Return Fiche.Structure_d_affectation
                        Case 3
                            Return Fiche.Structure_de_3e_niveau
                        Case 4
                            Return Fiche.Structure_de_4e_niveau
                    End Select
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Quotite_Activite(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Chaine As String = ""

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaActivite And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaActivite _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Chaine = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_POSITION).Modaliteservice
                    Chaine &= Strings.Space(1) & CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_POSITION).Taux_d_activite & " %"
                End If
                Return Chaine
            End Get
        End Property

        Public ReadOnly Property EmploiType(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL)
                    If Strings.Right(Fiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                        Return Fiche.Emploi_Type
                    End If
                End If
                Return FonctionExercee(DateEffet, False)
            End Get
        End Property

        Public ReadOnly Property FonctionExercee(ByVal DateEffet As String, ByVal SiDate As Boolean) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If

                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    Select Case SiDate
                        Case True
                            Return Fiche.Date_de_Valeur
                        Case Else
                            Return Fiche.Fonction_exercee
                    End Select
                End If
                Return ""
            End Get
        End Property

        ''************ AKR 30/08/2017
        Public ReadOnly Property SousFamille(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    Return Fiche.Sous_famille_du_metier
                End If
                Return ""
            End Get
        End Property
        Public ReadOnly Property NumeroDePoste(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    Return Fiche.Numero_du_poste
                End If
                Return ""
            End Get
        End Property
        Public ReadOnly Property IntituleEmploi(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    Return Fiche.Fonction_exercee
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property ReferenceEmploi(ByVal Emploi As String) As String
            Get
                'Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.MET_METIER)
                Dim Fiche As Virtualia.TablesObjet.ShemaREF.MET_METIER

                LstFiches = (From instance In WsLst_METIER Select instance Where instance.PointdeVue = VI.PointdeVue.PVueMetier And
                              instance.NumeroObjet = 1 Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    For Each VirFiche In LstFiches
                        Fiche = CType(VirFiche, Virtualia.TablesObjet.ShemaREF.MET_METIER)
                        If Fiche.Intitule = Emploi Then
                            Ws_ideDossierMET = Fiche.Ide_Dossier
                            Return Fiche.Reference
                        End If
                    Next
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property DefinitionSynthetique(ByVal Emploi As String) As String
            Get
                'Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.MET_METIER)
                Dim Fiche As Virtualia.TablesObjet.ShemaREF.MET_METIER

                LstFiches = (From instance In WsLst_METIER Select instance Where instance.PointdeVue = VI.PointdeVue.PVueMetier And
                              instance.NumeroObjet = 1 Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    For Each VirFiche In LstFiches
                        Fiche = CType(VirFiche, Virtualia.TablesObjet.ShemaREF.MET_METIER)
                        If Fiche.Intitule = Emploi Then
                            Return Fiche.Definition
                        End If
                    Next
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property ActivitesPrincipales(ByVal Emploi As String) As String
            Get
                'Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.MET_ACTIVITES)
                Dim Fiche As Virtualia.TablesObjet.ShemaREF.MET_ACTIVITES
                If WsLst_ACTIVITES IsNot Nothing Then
                    LstFiches = (From instance In WsLst_ACTIVITES Select instance Where instance.PointdeVue = VI.PointdeVue.PVueMetier And
                              instance.NumeroObjet = 9 Order By instance.Date_Valeur_ToDate Descending).ToList
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        For Each VirFiche In LstFiches
                            Fiche = CType(VirFiche, Virtualia.TablesObjet.ShemaREF.MET_ACTIVITES)
                            If Fiche.Ide_Dossier = Ws_ideDossierMET Then
                                Return Fiche.Activites
                            End If
                        Next
                    End If
                    Return ""
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Aptitudes(ByVal Emploi As String) As String
            Get
                'Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.MET_APTITUDES)
                Dim Fiche As Virtualia.TablesObjet.ShemaREF.MET_APTITUDES
                If WsLst_APTITUDES IsNot Nothing Then
                    LstFiches = (From instance In WsLst_APTITUDES Select instance Where instance.PointdeVue = VI.PointdeVue.PVueMetier And
                              instance.NumeroObjet = 12 Order By instance.Date_Valeur_ToDate Descending).ToList
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        For Each VirFiche In LstFiches
                            Fiche = CType(VirFiche, Virtualia.TablesObjet.ShemaREF.MET_APTITUDES)
                            If Fiche.Ide_Dossier = Ws_ideDossierMET Then
                                Return Fiche.Aptitudes
                            End If
                        Next
                    End If
                    Return ""
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Conditions_Exercice(ByVal Emploi As String) As String
            Get
                'Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.MET_ENVIRONNEMENT)
                Dim Fiche As Virtualia.TablesObjet.ShemaREF.MET_ENVIRONNEMENT
                If WsLst_Environement IsNot Nothing Then
                    LstFiches = (From instance In WsLst_Environement Select instance Where instance.PointdeVue = VI.PointdeVue.PVueMetier And
                              instance.NumeroObjet = 13 Order By instance.Date_Valeur_ToDate Descending).ToList
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        For Each VirFiche In LstFiches
                            Fiche = CType(VirFiche, Virtualia.TablesObjet.ShemaREF.MET_ENVIRONNEMENT)
                            If Fiche.Ide_Dossier = Ws_ideDossierMET Then
                                Return Fiche.Environnement_travail
                            End If
                        Next
                    End If
                    Return ""
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property ServiceOuDirection(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    If Fiche.Structure_d_affectation IsNot Nothing Then
                        Return Fiche.Structure_d_affectation
                    Else
                        Return Fiche.Structure_de_rattachement
                    End If
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property ServiceEtDirection(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    If Fiche.Structure_d_affectation IsNot Nothing Then
                        Return Fiche.Structure_d_affectation & " / " & Fiche.Structure_de_rattachement
                    Else
                        Return Fiche.Structure_de_rattachement
                    End If
                End If
                Return ""
            End Get
        End Property



        ''************
        Public ReadOnly Property PosteFonctionnel(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    Return Fiche.Poste_fonctionnel
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property ReductionAnciennete(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim NbMois As Integer = 0

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaNotation And
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaNotation
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    NbMois = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_NOTATION).Reliquat / 100
                End If
                Return NbMois & " mois"
            End Get
        End Property

        Public ReadOnly Property ObservationAffectation(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaOrganigramme _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    Return Fiche.Observations
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property FormationSuivie(ByVal DateEffet As String, ByVal Index As Integer) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim IndiceK As Integer = 0
                Dim IndiceCourant As Integer = 0
                Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_SESSION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaFormation _
                                        Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    Return ""
                End If
                For Each VirFiche In LstFiches
                    FichePER = CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_SESSION)
                    If FichePER.Motif_de_non_participation = "" Then
                        Select Case WsPointeurGlobal.VirRhDates.ComparerDates(FichePER.Date_de_Valeur, DateEffet)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                IndiceCourant += 1
                                If IndiceCourant = Index Then
                                    If WsRefCompetence.TypeEntretien = "CE" Then
                                        Return FichePER.Date_de_Valeur & " - " & FichePER.Intitule
                                    Else
                                        If FichePER.Intitule_de_la_session <> "" Then
                                            Return FichePER.Date_de_Valeur & " - " & FichePER.Intitule_de_la_session
                                        Else
                                            Return FichePER.Date_de_Valeur & " - " & FichePER.Intitule
                                        End If
                                    End If
                                End If
                        End Select
                    End If
                Next
                Return ""
            End Get
        End Property

        Public ReadOnly Property SoldeDuDif(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaDIF And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaDIF _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_DIF).Solde_DIF
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property DateEntretienPrecedent(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 1 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL)
                    If Strings.Right(Fiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                        Return CType(LstFiches.Item(1), Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL).Date_de_Valeur
                    End If
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Evaluateur(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL)
                    If Strings.Right(Fiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                        Return Fiche.Evaluateur
                    End If
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property Grade_Fonction_Evaluateur() As List(Of String)
            Get
                If WsObjet_150 Is Nothing Then
                    Return Nothing
                End If
                Call InitialiserManager(WsObjet_150.Date_de_Valeur, WsObjet_150.Identifiant_Manager)
                Dim TabRes As New List(Of String)
                TabRes.Add(WsGradeManager)
                TabRes.Add(WsFonctionManager)
                Return TabRes
            End Get
        End Property

        Public ReadOnly Property ServiceEvaluation(ByVal DateEffet As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL)
                    If Strings.Right(Fiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                        Return Fiche.Entite_Evaluation
                    End If
                End If
                Return NiveauAffectation(DateEffet, 1)
            End Get
        End Property

        Public ReadOnly Property SiFormulaireSigne_Termine(ByVal DateEffet) As Boolean
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL)
                    If Strings.Right(Fiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                        If Fiche.Date_de_Signature_Evalue <> "" Then
                            Return True
                        End If
                    End If
                End If
                Return False
            End Get
        End Property

        Public ReadOnly Property SiFonctionEncadrement(ByVal DateEffet) As Boolean
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL)
                    If Strings.Right(Fiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                        If Fiche.SiEncadrement.ToLower = "oui" And Fiche.Nombre_Encadres > 0 Then
                            Return True
                        End If
                    End If
                End If
                Return False
            End Get
        End Property

        Public ReadOnly Property SiSouhaitEntCarriere(ByVal DateEffet) As Boolean
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim DateValeur As Date = WsPointeurGlobal.VirRhDates.DateTypee(DateEffet, True)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 And _
                                                instance.Date_Valeur_ToDate <= DateValeur Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Fiche = CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL)
                    If Strings.Right(Fiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                        If Fiche.SiSouhait_EntretienCarriere.ToLower = "oui" Then
                            Return True
                        End If
                    End If
                End If
                Return False
            End Get
        End Property

        Public WriteOnly Property TableauMaj(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
            Set(ByVal value As String)
                Dim TableauData As ArrayList
                Dim IndiceF As Integer

                Select Case NoObjet
                    Case 150
                        TableauData = WsObjet_150.V_TableauData
                        If NoInfo = 0 Then
                            TableauData(NoInfo) = WsPointeurGlobal.Date_Limite(value)
                        Else
                            TableauData(NoInfo) = value
                        End If
                        Objet_150.V_TableauData = TableauData
                        If Objet_150.Nombre_Encadres > 0 Then
                            Objet_150.SiEncadrement = "Oui"
                        Else
                            Objet_150.SiEncadrement = "Non"
                        End If
                    Case 151
                        For IndiceF = 0 To WsLst_151.Count - 1
                            If WsLst_151.Item(IndiceF).Numero.ToString = Rang Then
                                TableauData = WsLst_151.Item(IndiceF).V_TableauData
                                TableauData(NoInfo) = value
                                Select Case NoInfo
                                    Case 4 'Critère numérique
                                        If IsNumeric(value) Then
                                            TableauData(NoInfo + 1) = WsRefCompetence.LitteralResultat(value)
                                        End If
                                End Select
                                WsLst_151.Item(IndiceF).V_TableauData = TableauData
                            End If
                        Next IndiceF
                    Case 152
                        For IndiceF = 0 To WsLst_152.Count - 1
                            If WsLst_152.Item(IndiceF).Numero.ToString = Rang Then
                                TableauData = WsLst_152.Item(IndiceF).V_TableauData
                                TableauData(NoInfo) = value
                                WsLst_152.Item(IndiceF).V_TableauData = TableauData
                            End If
                        Next IndiceF
                    Case 153
                        Select Case WsRefCompetence.TypeEntretien
                            Case "CESE", "CESE2", "FAM"
                                If IsNumeric(Rang) Then
                                    For IndiceF = 0 To WsLst_153.Count - 1
                                        If WsLst_153.Item(IndiceF).NumeroTri = Rang Then
                                            TableauData = WsLst_153.Item(IndiceF).V_TableauData
                                            TableauData(NoInfo) = value
                                            Select Case NoInfo
                                                Case 7 'Critère numérique
                                                    If IsNumeric(value) Then
                                                        TableauData(NoInfo + 1) = WsRefCompetence.LitteralCompetence(value, WsRefCompetence.SiCompetenceManager(TableauData(1).ToString))
                                                    End If
                                                Case 9 'Critère numérique non requis
                                                    If IsNumeric(value) Then
                                                        TableauData(NoInfo + 1) = WsRefCompetence.LitteralCompetence(value)
                                                    End If
                                            End Select
                                            WsLst_153.Item(IndiceF).V_TableauData = TableauData
                                        End If
                                    Next IndiceF
                                End If
                            Case "AFB"
                                If IsNumeric(Rang) Then
                                    For IndiceF = 0 To WsLst_153.Count - 1
                                        If WsLst_153.Item(IndiceF).NumeroTri = Rang Then
                                            TableauData = WsLst_153.Item(IndiceF).V_TableauData
                                            TableauData(NoInfo) = value
                                            Select Case NoInfo
                                                Case 7 'Critère numérique
                                                    If IsNumeric(value) Then
                                                        TableauData(NoInfo + 1) = WsRefCompetence.LitteralCompetence(value, WsRefCompetence.SiCompetenceManager(TableauData(1).ToString))
                                                    End If
                                                Case 9 'Critère numérique non requis
                                                    If IsNumeric(value) Then
                                                        TableauData(NoInfo + 1) = WsRefCompetence.LitteralCompetence(value)
                                                    End If
                                            End Select
                                            WsLst_153.Item(IndiceF).V_TableauData = TableauData
                                        End If
                                    Next IndiceF
                                Else
                                    For IndiceF = 0 To WsLst_153.Count - 1
                                        If WsLst_153.Item(IndiceF).Competence = Rang Then
                                            TableauData = WsLst_153.Item(IndiceF).V_TableauData
                                            TableauData(NoInfo) = value
                                            Select Case NoInfo
                                                Case 7 'Critère numérique
                                                    If IsNumeric(value) Then
                                                        TableauData(NoInfo + 1) = WsRefCompetence.LitteralCompetence(value, WsRefCompetence.SiCompetenceManager(TableauData(1).ToString))
                                                    End If
                                                Case 9 'Critère numérique non requis
                                                    If IsNumeric(value) Then
                                                        TableauData(NoInfo + 1) = WsRefCompetence.LitteralCompetence(value)
                                                    End If
                                            End Select
                                            WsLst_153.Item(IndiceF).V_TableauData = TableauData
                                        End If
                                    Next IndiceF
                                End If
                            Case Else
                                For IndiceF = 0 To WsLst_153.Count - 1
                                    If WsLst_153.Item(IndiceF).Competence = Rang Then
                                        TableauData = WsLst_153.Item(IndiceF).V_TableauData
                                        TableauData(NoInfo) = value
                                        Select Case NoInfo
                                            Case 7 'Critère numérique
                                                If IsNumeric(value) Then
                                                    TableauData(NoInfo + 1) = WsRefCompetence.LitteralCompetence(value, WsRefCompetence.SiCompetenceManager(TableauData(1).ToString))
                                                End If
                                            Case 9 'Critère numérique non requis
                                                If IsNumeric(value) Then
                                                    TableauData(NoInfo + 1) = WsRefCompetence.LitteralCompetence(value)
                                                End If
                                        End Select
                                        WsLst_153.Item(IndiceF).V_TableauData = TableauData
                                    End If
                                Next IndiceF
                        End Select
                    Case 154
                        For IndiceF = 0 To WsLst_154.Count - 1
                            If WsLst_154.Item(IndiceF).Numero_Famille = Rang Then
                                TableauData = WsLst_154.Item(IndiceF).V_TableauData
                                TableauData(NoInfo) = value
                                WsLst_154.Item(IndiceF).V_TableauData = TableauData
                            End If
                        Next IndiceF
                    Case 155
                        Select Case WsRefCompetence.TypeEntretien
                            Case "CE"
                                For IndiceF = 0 To WsLst_155.Count - 1
                                    If WsLst_155.Item(IndiceF).Intitule = Rang Then
                                        TableauData = WsLst_155.Item(IndiceF).V_TableauData
                                        TableauData(NoInfo) = value
                                        WsLst_155.Item(IndiceF).V_TableauData = TableauData
                                    End If
                                Next IndiceF
                            Case Else
                                If IsNumeric(Rang) Then
                                    For IndiceF = 0 To WsLst_155.Count - 1
                                        If WsLst_155.Item(IndiceF).NumeroTri = Rang Then
                                            TableauData = WsLst_155.Item(IndiceF).V_TableauData
                                            TableauData(NoInfo) = value
                                            WsLst_155.Item(IndiceF).V_TableauData = TableauData
                                        End If
                                    Next IndiceF
                                End If
                        End Select
                    Case 156
                        For IndiceF = 0 To WsLst_156.Count - 1
                            If WsLst_156.Item(IndiceF).Intitule_Famille = Rang Then
                                TableauData = WsLst_156.Item(IndiceF).V_TableauData
                                TableauData(NoInfo) = value
                                Select Case NoInfo
                                    Case 3 'Critère numérique
                                        If IsNumeric(value) Then
                                            TableauData(NoInfo + 1) = WsRefCompetence.LitteralSynthese(value)
                                        End If
                                End Select
                                WsLst_156.Item(IndiceF).V_TableauData = TableauData
                            End If
                        Next IndiceF
                    Case 157
                        For IndiceF = 0 To WsLst_157.Count - 1
                            If WsLst_157.Item(IndiceF).Rang = Rang Then
                                TableauData = WsLst_157.Item(IndiceF).V_TableauData
                                TableauData(NoInfo) = value
                                WsLst_157.Item(IndiceF).V_TableauData = TableauData
                            End If
                        Next IndiceF
                End Select
            End Set
        End Property

        Public ReadOnly Property SiAnneeExiste(ByVal Param As String) As Boolean
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 _
                                                Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    For Each Virfiche In LstFiches
                        If Strings.Right(Virfiche.Date_de_Valeur, 4) = Param Then
                            Return True
                        End If
                    Next
                End If
                Return False
            End Get
        End Property

        Public Property SiSelf As Boolean
            Get
                Return WsSiSelf
            End Get
            Set(ByVal value As Boolean)
                WsSiSelf = value
            End Set
        End Property

        Public Property SiN2 As Boolean
            Get
                Return WsSiN2
            End Get
            Set(ByVal value As Boolean)
                WsSiN2 = value
            End Set
        End Property

        Public Property SiManager As Boolean
            Get
                Return WsSiManager
            End Get
            Set(ByVal value As Boolean)
                WsSiManager = value
            End Set
        End Property

        Public ReadOnly Property Date_de_Signature_Evalue As String
            Get
                Return WsDateSignature
            End Get
        End Property
        Public ReadOnly Property Date_de_Signature_Observations As String
            Get
                Return WsDateSignatureObservations
            End Get
        End Property


        Public ReadOnly Property Date_de_Signature_N2 As String
            Get
                Return WsDateSignatureN2
            End Get
        End Property

        Public ReadOnly Property Date_de_Signature_Manager As String
            Get
                Return WsDateSignatureManager
            End Get
        End Property

        Public ReadOnly Property SiReadOnly(ByVal NumObjet As Integer, ByVal NumInfo As Integer, Optional ByVal Lettre As String = "") As Boolean
            Get
                If WsObjet_150 Is Nothing Then
                    Return True
                End If

                If WsDateSignature <> "" And WsRefCompetence.TypeEntretien = "ENM" Then
                    Select Case NumObjet
                        Case 154
                            If NumInfo = 3 Then
                                Select Case Lettre
                                    Case "B", "C", "D", "E", "F", "1", "2", "3"
                                        Return WsSiSelf
                                    Case Else
                                        Return True
                                End Select
                            Else
                                Return WsSiSelf
                            End If
                        Case 157
                            If NumInfo = 4 Then
                                Select Case Lettre
                                    Case "C", "D"
                                        Return WsSiSelf
                                    Case Else
                                        Return True
                                End Select
                            End If
                        Case Else
                            Return True
                    End Select

                End If

                If WsDateSignature <> "" Then
                    Return True
                End If
                Select Case WsRefCompetence.TypeEntretien
                    Case "CNM"
                        Select Case NumObjet
                            Case 150
                                Select Case NumInfo
                                    Case 4, 20, 29
                                        Return Not WsSiSelf
                                    Case Else
                                        Return WsSiSelf
                                End Select
                            Case 151
                                Select Case NumInfo
                                    Case 6
                                        Return Not WsSiSelf
                                    Case Else
                                        Return WsSiSelf
                                End Select
                            Case 154
                                If NumInfo = 3 Then
                                    Select Case Lettre
                                        Case "A", "B", "C", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "S"
                                            Return Not WsSiSelf
                                        Case Else
                                            Return WsSiSelf
                                    End Select
                                Else
                                    Return WsSiSelf
                                End If
                            Case 155
                                If NumInfo = 6 Then
                                    Return WsSiSelf
                                Else
                                    Select Case Lettre
                                        Case "A", "B", "C", "D"
                                            Return Not WsSiSelf
                                        Case Else
                                            Return WsSiSelf
                                    End Select
                                End If
                            Case 157
                                If NumInfo = 4 Then
                                    Select Case Lettre
                                        Case "A"
                                            Return Not WsSiSelf
                                        Case Else
                                            Return WsSiSelf
                                    End Select
                                End If
                            Case Else
                                Return WsSiSelf
                        End Select
                    Case "CE"
                        Return WsSiSelf
                    'Select Case NumObjet
                    '    Case 150
                    '        Select Case NumInfo
                    '            Case 4
                    '                Return Not WsSiSelf
                    '            Case Else
                    '                Return WsSiSelf
                    '        End Select
                    '    Case Else
                    '        Return WsSiSelf
                    'End Select
                    Case "CESE", "CESE2", "CNED", "CNED2", "ENM", "ISMEP", "MEN", "ONISEP", "GIP"
                        Select Case NumObjet
                            Case 150
                                Select Case NumInfo
                                    Case 4, 20
                                        Return Not WsSiSelf
                                    Case Else
                                        Return WsSiSelf
                                End Select
                            Case 151
                                Select Case WsRefCompetence.TypeEntretien
                                    Case "CNED2"
                                        Select Case NumInfo
                                            Case 2
                                                Return True
                                            Case Else
                                                Return WsSiSelf
                                        End Select
                                    Case "ENM"
                                        Select Case NumInfo
                                            Case 5
                                                Return True
                                            Case Else
                                                Return WsSiSelf
                                        End Select
                                    Case Else
                                        Return WsSiSelf
                                End Select
                            Case 154
                                If NumInfo = 3 Then
                                    Select Case WsRefCompetence.TypeEntretien
                                        Case "CESE"
                                            Select Case Lettre
                                                Case "Q", "R", "S", "T", "U"
                                                    Return Not WsSiSelf
                                                Case Else
                                                    Return WsSiSelf
                                            End Select
                                        Case "CESE2"
                                            Select Case Lettre
                                                Case "Q", "R", "S", "T", "U", "V", "W", "Y"
                                                    Return Not WsSiSelf
                                                Case Else
                                                    Return WsSiSelf
                                            End Select
                                        Case "CNED"
                                            Select Case Lettre
                                                Case "U", "V"
                                                    Return Not WsSiSelf
                                                Case Else
                                                    Return WsSiSelf
                                            End Select
                                        Case "CNED2", "ISMEP", "MEN", "ONISEP"
                                            Select Case Lettre
                                                Case "P", "Q"
                                                    Return Not WsSiSelf
                                                Case Else
                                                    Return WsSiSelf
                                            End Select
                                        Case "ENM"
                                            Select Case Lettre
                                                Case "W", "X", "Z"
                                                    Return Not WsSiSelf
                                                Case Else
                                                    Return WsSiSelf
                                            End Select
                                    End Select
                                Else
                                    Return WsSiSelf
                                End If
                            Case 157
                                Select Case WsRefCompetence.TypeEntretien
                                    Case "ENM"
                                        If NumInfo = 4 Then
                                            Select Case Lettre
                                                Case "A", "B"
                                                    Return Not WsSiSelf
                                                Case Else
                                                    Return WsSiSelf
                                            End Select
                                        End If
                                    Case Else
                                        Return WsSiSelf
                                End Select
                            Case Else
                                Return WsSiSelf
                        End Select
                    Case "FAM"
                        Select Case NumObjet
                            Case 150
                                Select Case NumInfo
                                    Case 4, 15, 16, 17
                                        Return Not WsSiSelf
                                    Case 9, 22
                                        Return False 'A la fois le salarié et le manager
                                    Case Else
                                        Return WsSiSelf
                                End Select
                            Case 151, 152
                                Return False 'A la fois le salarié et le manager
                            Case 154
                                If NumInfo = 3 Then
                                    Select Case Lettre
                                        Case "N", "U", "V", "W"
                                            Return Not WsSiSelf
                                        Case "E", "F", "G", "I", "J", "K", "Q", "R", "S"
                                            Return False 'A la fois le salarié et le manager
                                        Case Else
                                            Return WsSiSelf
                                    End Select
                                Else
                                    Return WsSiSelf
                                End If
                            Case 155
                                If NumInfo = 6 Then
                                    Return WsSiSelf
                                Else
                                    Select Case Lettre
                                        Case "A", "B", "C", "D", "E", "F", "G", "H", "I", "J"
                                            Return False 'A la fois le salarié et le manager
                                        Case Else
                                            Return WsSiSelf
                                    End Select
                                End If
                            Case 157
                                If NumInfo = 4 Then
                                    Select Case Lettre
                                        Case "A", "B"
                                            Return Not WsSiSelf
                                        Case "K"
                                            Return WsSiSelf
                                    End Select
                                End If
                            Case Else
                                Return WsSiSelf
                        End Select
                    Case "PNF"
                        Select Case NumObjet
                            Case 150
                                Select Case NumInfo
                                    Case 4, 20
                                        Return Not WsSiSelf
                                    Case Else
                                        Return WsSiSelf
                                End Select
                            Case 154
                                If NumInfo = 3 Then
                                    Select Case Lettre
                                        Case "O", "V", "W"
                                            Return Not WsSiSelf
                                        Case Else
                                            Return WsSiSelf
                                    End Select
                                Else
                                    Return WsSiSelf
                                End If
                            Case Else
                                Return WsSiSelf
                        End Select
                    Case "AFB"
                        If WsSiSelf = True Then
                            Select Case NumObjet
                                Case 150
                                    Select Case NumInfo
                                        Case 4
                                            If WsDateSignatureN2 <> "" Then
                                                Return Not WsSiSelf
                                            Else
                                                Return WsSiSelf
                                            End If
                                        Case Else
                                            Return WsSiSelf
                                    End Select
                                Case 154
                                    If NumInfo = 3 Then
                                        Select Case Lettre
                                            Case "BB"
                                                If WsDateSignatureManager <> "" Then
                                                    Return Not WsSiSelf
                                                Else
                                                    Return WsSiSelf
                                                End If
                                            Case Else
                                                Return WsSiSelf
                                        End Select
                                    Else
                                        Return WsSiSelf
                                    End If
                                Case 157
                                    Select Case NumInfo
                                        Case 4
                                            Select Case Lettre
                                                Case "B", "C"
                                                    If WsDateSignatureManager <> "" Then
                                                        Return Not WsSiSelf
                                                    Else
                                                        Return WsSiSelf
                                                    End If
                                                Case "J"
                                                    If WsDateSignatureN2 <> "" Then
                                                        Return Not WsSiSelf
                                                    Else
                                                        Return WsSiSelf
                                                    End If
                                                Case Else
                                                    Return WsSiSelf
                                            End Select
                                        Case Else
                                            Return WsSiSelf
                                    End Select
                                Case Else
                                    Return WsSiSelf
                            End Select
                        Else
                            If SiManager = True And SiN2 = True Then
                                Select Case NumObjet
                                    Case 150
                                        Select Case NumInfo
                                            Case 4
                                                Return Not WsSiSelf
                                            Case 23
                                                If WsDateSignatureObservations <> "" Then
                                                    Return WsSiSelf
                                                Else
                                                    Return Not WsSiSelf
                                                End If
                                            Case Else
                                                Return WsSiSelf
                                        End Select
                                    Case 154
                                        If NumInfo = 3 Then
                                            Select Case Lettre
                                                Case "BB"
                                                    Return Not WsSiSelf
                                                Case "BC"
                                                    If WsDateSignatureObservations <> "" Then
                                                        Return WsSiSelf
                                                    Else
                                                        Return Not WsSiSelf
                                                    End If
                                                Case Else
                                                    Return WsSiSelf
                                            End Select
                                        Else
                                            Return WsSiSelf
                                        End If
                                    Case 157
                                        Select Case NumInfo
                                            Case 4
                                                Select Case Lettre
                                                    Case "B", "C", "J"
                                                        Return Not WsSiSelf
                                                    Case "F", "G", "I"
                                                        If WsDateSignatureObservations <> "" Then
                                                            Return WsSiSelf
                                                        Else
                                                            Return Not WsSiSelf
                                                        End If
                                                    Case Else
                                                        Return WsSiSelf
                                                End Select
                                            Case Else
                                                Return WsSiSelf
                                        End Select
                                    Case Else
                                        Return WsSiSelf
                                End Select
                            Else
                                If SiManager = True And SiN2 = False Then
                                    Select Case NumObjet
                                        Case 150
                                            Select Case NumInfo
                                                Case 4, 23
                                                    Return Not WsSiSelf
                                                Case Else
                                                    Return WsSiSelf
                                            End Select
                                        Case 154
                                            If NumInfo = 3 Then
                                                Select Case Lettre
                                                    Case "BB", "BC"
                                                        Return Not WsSiSelf
                                                    Case Else
                                                        Return WsSiSelf
                                                End Select
                                            Else
                                                Return WsSiSelf
                                            End If
                                        Case 157
                                            Select Case NumInfo
                                                Case 4
                                                    Select Case Lettre
                                                        Case "B", "C", "F", "G", "I", "J"
                                                            Return Not WsSiSelf
                                                        Case Else
                                                            Return WsSiSelf
                                                    End Select
                                                Case Else
                                                    Return WsSiSelf
                                            End Select
                                        Case Else
                                            Return WsSiSelf
                                    End Select
                                End If
                                If SiManager = False And SiN2 = True Then
                                    Select Case NumObjet
                                        Case 150
                                            Select Case NumInfo
                                                Case 23
                                                    If WsDateSignatureObservations <> "" Then
                                                        Return WsSiSelf
                                                    Else
                                                        Return Not WsSiSelf
                                                    End If
                                                Case Else
                                                    Return Not WsSiSelf
                                            End Select
                                        Case 154
                                            If NumInfo = 3 Then
                                                Select Case Lettre
                                                    Case "BC"
                                                        If WsDateSignatureObservations <> "" Then
                                                            Return WsSiSelf
                                                        Else
                                                            Return Not WsSiSelf
                                                        End If
                                                    Case Else
                                                        Return Not WsSiSelf
                                                End Select
                                            Else
                                                Return Not WsSiSelf
                                            End If
                                        Case 157
                                            Select Case NumInfo
                                                Case 4
                                                    Select Case Lettre
                                                        Case "F", "G", "I"
                                                            If WsDateSignatureObservations <> "" Then
                                                                Return WsSiSelf
                                                            Else
                                                                Return Not WsSiSelf
                                                            End If
                                                        Case Else
                                                            Return Not WsSiSelf
                                                    End Select
                                                Case Else
                                                    Return Not WsSiSelf
                                            End Select
                                        Case Else
                                            Return Not WsSiSelf
                                    End Select
                                End If
                            End If

                        End If
                    Case Else
                        Return WsSiSelf
                End Select
                Return WsSiSelf
            End Get
        End Property

        Public Property SiEMailAEnvoyer As Boolean
            Get
                Return WsSiEMailAEnvoyer
            End Get
            Set(ByVal value As Boolean)
                WsSiEMailAEnvoyer = value
            End Set
        End Property
        Public Property SiEtape1Valide As Boolean
            Get
                Return WsSiEtape1Valide
            End Get
            Set(ByVal value As Boolean)
                WsSiEtape1Valide = value
            End Set
        End Property
        Public Property SiEtape2Valide As Boolean
            Get
                Return WsSiEtape2Valide
            End Get
            Set(ByVal value As Boolean)
                WsSiEtape2Valide = value
            End Set
        End Property
        Public Property SiEtape3Valide As Boolean
            Get
                Return WsSiEtape3Valide
            End Get
            Set(ByVal value As Boolean)
                WsSiEtape3Valide = value
            End Set
        End Property
        Public Property SiEtape4Valide As Boolean
            Get
                Return WsSiEtape4Valide
            End Get
            Set(ByVal value As Boolean)
                WsSiEtape4Valide = value
            End Set
        End Property

        Public Property SiEMailManagerAEnvoyer As Boolean
            Get
                Return WsSiEMailManagerAEnvoyer
            End Get
            Set(ByVal value As Boolean)
                WsSiEMailManagerAEnvoyer = value
            End Set
        End Property

        Public Property Annee As String
            Get
                If WsAnnee = "" Then
                    WsAnnee = Strings.Right(WsPointeurGlobal.Date_Limite(WsPointeurGlobal.VirRhDates.DateduJour), 4)
                End If
                Return WsAnnee
            End Get
            Set(ByVal value As String)
                WsAnnee = value
                WsObjet_150 = Objet_150
                If WsObjet_150 Is Nothing Then
                    WsObjet_150 = CreationNouvelEntretien
                End If
                WsDateSignature = WsObjet_150.Date_de_Signature_Evalue
                WsDateSignatureManager = WsObjet_150.Date_de_Signature_Evaluateur
                WsDateSignatureN2 = WsObjet_150.Date_de_Signature_Autorite_Hierarchique

                Call FaireListe_151()
                Call FaireListe_152()
                Call FaireListe_153()
                Call FaireListe_154()
                Call FaireListe_155()
                Call FaireListe_156()
                Call FaireListe_157()
                Select Case WsRefCompetence.TypeEntretien
                    Case "FAM", "ENM", "AFB", "GIP"
                        Call InitialiserManager(WsObjet_150.Date_de_Valeur, WsObjet_150.Identifiant_Manager)
                End Select
            End Set
        End Property

        Public ReadOnly Property AnneePrecedente As String
            Get
                Return CStr(CInt(WsAnnee) - 1)
            End Get
        End Property

        Private ReadOnly Property CreationNouvelEntretien As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL
            Get
                Dim IndiceI As Integer
                Dim ObjetFiche As Object
                Dim ObjetPrecedent As Object
                Dim AnneePrecedente As String

                Call InitialiserManager(WsPointeurGlobal.VirRhDates.DateduJour, 0)

                WsObjet_150 = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(150, 0)
                WsObjet_150.Date_de_Valeur = WsPointeurGlobal.Date_Limite(WsPointeurGlobal.VirRhDates.DateduJour)
                WsObjet_150.Evaluateur = WsNomPrenomManager
                Select Case WsRefCompetence.TypeEntretien
                    Case "CE"
                        WsObjet_150.Entite_Evaluation = NiveauAffectation(WsPointeurGlobal.VirRhDates.DateduJour, 2)
                        If WsObjet_150.Entite_Evaluation = "" Then
                            WsObjet_150.Entite_Evaluation = NiveauAffectation(WsPointeurGlobal.VirRhDates.DateduJour, 1)
                        End If
                    Case Else
                        WsObjet_150.Entite_Evaluation = NiveauAffectation(WsPointeurGlobal.VirRhDates.DateduJour, 1)
                End Select
                WsObjet_150.Nombre_Encadres = 0
                WsObjet_150.Critere_Synthese = 0
                WsObjet_150.SiEncadrement = "Non"
                WsObjet_150.SiSouhait_EntretienCarriere = "Non"
                WsObjet_150.SiFichedePoste_Adaptee = "Oui"
                WsObjet_150.SiUtilisation_du_DIF = "Non"
                WsObjet_150.Identifiant_Manager = WsIdeManager

                '** A alimenter avec entretien précédent
                AnneePrecedente = (CInt(WsAnnee) - 1).ToString
                ObjetPrecedent = Objet_150(AnneePrecedente)
                If ObjetPrecedent IsNot Nothing Then
                    WsObjet_150.Descriptif_Poste = CType(ObjetPrecedent, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL).Descriptif_Poste
                    WsObjet_150.Emploi_Type = CType(ObjetPrecedent, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL).Emploi_Type
                    WsObjet_150.ActivitesPrincipales = CType(ObjetPrecedent, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL).ActivitesPrincipales
                    Select Case WsRefCompetence.TypeEntretien
                        Case "CE"
                            WsObjet_150.AcquisExperience = CType(ObjetPrecedent, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL).AcquisExperience
                    End Select
                End If
                '**
                If WsListeEntretiens Is Nothing Then
                    WsListeEntretiens = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                End If
                WsListeEntretiens.Add(WsObjet_150)
                Call FaireListe_152("31/12/" & (CInt(WsAnnee) - 1).ToString) 'Lecture des fiches précédentes

                For IndiceI = 1 To WsRefCompetence.NombreMaxi("RES")
                    ObjetFiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(151, 0)
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT).Date_de_Valeur = WsObjet_150.Date_de_Valeur
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT).Numero = IndiceI
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT).Critere_Numerique = 0
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT).Critere_Litteral = ""
                    WsListeEntretiens.Add(CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT))

                    '** A alimenter avec entretien précédent
                    ObjetPrecedent = Objet_152(IndiceI.ToString)
                    If ObjetPrecedent IsNot Nothing Then
                        If CType(ObjetPrecedent, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF).Intitule <> "" Then
                            CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT).Intitule = _
                                CType(ObjetPrecedent, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF).Intitule
                            If WsRefCompetence.TypeEntretien = "PNF" Then
                                If CType(ObjetPrecedent, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF).Resultats_Attendus IsNot Nothing Then
                                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT).Difficultes =
                                    CType(ObjetPrecedent, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF).Resultats_Attendus
                                End If
                            End If
                        End If
                    End If
                    '**
                Next IndiceI

                For IndiceI = 1 To WsRefCompetence.NombreMaxi("RES")
                    ObjetFiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(152, 0)
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF).Date_de_Valeur = WsObjet_150.Date_de_Valeur
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF).Numero = IndiceI
                    WsListeEntretiens.Add(CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF))
                Next IndiceI

                If WsRefCompetence.NombreMaxi("CMP") > 0 Then
                    For IndiceI = 0 To WsRefCompetence.NombreMaxi("CMP") - 1
                        ObjetFiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(153, 0)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE).Date_de_Valeur = WsObjet_150.Date_de_Valeur
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE).Competence = WsRefCompetence.TableCompetence(IndiceI, 0)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE).Numero_Famille = WsRefCompetence.TableCompetence(IndiceI, 1)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE).Intitule_Famille = WsRefCompetence.TableCompetence(IndiceI, 2)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE).Numero_SousFamille = WsRefCompetence.TableCompetence(IndiceI, 3)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE).Intitule_SousFamille = WsRefCompetence.TableCompetence(IndiceI, 4)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE).NumeroTri = CInt(WsRefCompetence.TableCompetence(IndiceI, 5))
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE).Critere_Numerique = 0
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE).Critere_Litteral = WsRefCompetence.LitteralCompetence(0)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE).Critere_Numerique_Requis = 0
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE).Critere_Litteral_Requis = WsRefCompetence.LitteralCompetence(0)
                        WsListeEntretiens.Add(CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE))
                    Next IndiceI
                End If

                For IndiceI = 0 To WsRefCompetence.NombreMaxi("OBS") - 1
                    ObjetFiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(154, 0)
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBSERVATION).Date_de_Valeur = WsObjet_150.Date_de_Valeur
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBSERVATION).Numero_Famille = WsRefCompetence.TableObservation(IndiceI, 0)
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBSERVATION).Intitule_Famille = WsRefCompetence.TableObservation(IndiceI, 1)
                    WsListeEntretiens.Add(CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBSERVATION))
                Next IndiceI

                For IndiceI = 0 To WsRefCompetence.NombreMaxi("FOR") - 1
                    ObjetFiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(155, 0)
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION).Date_de_Valeur = WsObjet_150.Date_de_Valeur
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION).Intitule = WsRefCompetence.TableFormation(IndiceI, 0)
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION).Type_du_Besoin = WsRefCompetence.TableFormation(IndiceI, 1)
                    Select Case WsRefCompetence.TypeEntretien
                        Case "FAM"
                            If IndiceI >= 10 Then
                                CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION).Type_du_Besoin = WsRefCompetence.TableFormation(IndiceI, 0)
                            End If
                    End Select
                    CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION).NumeroTri = CInt(WsRefCompetence.TableFormation(IndiceI, 2))
                    WsListeEntretiens.Add(CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION))
                Next IndiceI

                If WsRefCompetence.NombreMaxi("SYN") > 0 Then
                    For IndiceI = 0 To WsRefCompetence.NombreMaxi("SYN") - 1
                        ObjetFiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(156, 0)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE).Date_de_Valeur = WsObjet_150.Date_de_Valeur
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE).Intitule_Famille = WsRefCompetence.TableSynthese(IndiceI, 0)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE).Numero_Famille = WsRefCompetence.TableSynthese(IndiceI, 1)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE).NumeroTri = CInt(WsRefCompetence.TableSynthese(IndiceI, 2))
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE).Critere_Numerique = 0
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE).Critere_Litteral = ""
                        WsListeEntretiens.Add(CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE))
                    Next IndiceI
                End If

                If WsRefCompetence.NombreMaxi("CPL") > 0 Then
                    For IndiceI = 0 To WsRefCompetence.NombreMaxi("CPL") - 1
                        ObjetFiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(157, 0)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT).Date_de_Valeur = WsObjet_150.Date_de_Valeur
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT).Rang = WsRefCompetence.TableComplement(IndiceI, 0)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT).Intitule = WsRefCompetence.TableComplement(IndiceI, 1)
                        CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT).Nature = WsRefCompetence.TableComplement(IndiceI, 2)
                        WsListeEntretiens.Add(CType(ObjetFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT))
                    Next IndiceI
                End If

                Return WsObjet_150
            End Get
        End Property

        Public Function MettreAJour() As Boolean
            Dim SiOK As Boolean
            Dim ChaineLue As String
            Dim ChaineMaj As String
            Dim CodeMaj As String = ""
            Dim SiAfaire As Boolean

            If WsObjet_150 Is Nothing Then
                Return False
            End If

            ''****** AKR 12/09/2017
            If WsDateSignature <> "" And WsRefCompetence.TypeEntretien <> "ENM" Then
                Return False
            End If

            'If WsDateSignature <> "" Then
            '    Return False
            'End If

            ChaineLue = ""
            CodeMaj = "C"
            If WsObjet_150.Ide_Dossier = WsIdentifiant Then
                ChaineLue = WsObjet_150.FicheLue
                CodeMaj = "M"
            End If
            If WsObjet_150.Identifiant_Manager = 0 Then
                Call InitialiserManager(WsObjet_150.Date_de_Valeur, 0)
                WsObjet_150.Identifiant_Manager = WsIdeManager
                If WsObjet_150.Evaluateur = "" Then
                    WsObjet_150.Evaluateur = WsNomPrenomManager
                End If
            End If
            ChaineMaj = WsObjet_150.ContenuTable
            If ChaineMaj <> ChaineLue Then
                ''***** AKR test
                'SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 150, WsIdentifiant, CodeMaj, ChaineLue, ChaineLue)
                SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 150, WsIdentifiant, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK = False Then
                    Return SiOK
                End If
            End If

            WsObjet_150.ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
            If WsDateSignature = "" Then
                If WsObjet_150.Date_de_Signature_Evalue <> "" Then
                    WsSiEMailAEnvoyer = True
                    WsSiEtape4Valide = True
                End If
            End If
            Select Case WsRefCompetence.TypeEntretien
                Case "FAM"
                    If WsDateSignatureManager = "" Then
                        If WsObjet_150.Date_de_Signature_Evaluateur <> "" Then
                            WsSiEMailManagerAEnvoyer = True
                        End If
                    End If
                Case "ENM"
                    If WsDateSignatureManager = "" Then
                        If WsObjet_150.Date_de_Signature_Evaluateur <> "" Then
                            WsSiEMailManagerAEnvoyer = True
                        End If
                    End If
                Case "GIP"
                    If WsDateSignatureManager = "" Then
                        If WsObjet_150.Date_de_Signature_Evaluateur <> "" Then
                            WsSiEMailManagerAEnvoyer = True
                        End If
                    End If
                Case "AFB"
                    If WsDateSignatureManager = "" Then
                        If WsObjet_150.Date_de_Signature_Evaluateur <> "" Then
                            WsSiEtape1Valide = True
                        End If
                    End If
                    If WsDateSignatureN2 = "" Then
                        If WsObjet_150.Date_de_Signature_Autorite_Hierarchique <> "" Then
                            WsSiEtape3Valide = True
                        End If
                    End If
            End Select
            For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT In WsLst_151
                ChaineLue = ""
                CodeMaj = "C"
                If FichePER.Intitule <> "" Then
                    If FichePER.Ide_Dossier = WsIdentifiant Then
                        ChaineLue = FichePER.FicheLue
                        CodeMaj = "M"
                    End If
                    FichePER.Date_de_Valeur = WsObjet_150.Date_de_Valeur
                    ChaineMaj = FichePER.ContenuTable
                    If ChaineMaj <> ChaineLue Then
                        SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 151, WsIdentifiant, CodeMaj, ChaineLue, ChaineMaj)
                        If SiOK Then
                            FichePER.ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
                        End If
                    End If
                ElseIf FichePER.Ide_Dossier = WsIdentifiant Then
                    SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 151, WsIdentifiant, "S", FichePER.FicheLue, "")
                End If
            Next

            For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF In WsLst_152
                ChaineLue = ""
                CodeMaj = "C"
                If FichePER.Intitule <> "" Then
                    If FichePER.Ide_Dossier = WsIdentifiant Then
                        ChaineLue = FichePER.FicheLue
                        CodeMaj = "M"
                    End If
                    FichePER.Date_de_Valeur = WsObjet_150.Date_de_Valeur
                    ChaineMaj = FichePER.ContenuTable
                    If ChaineMaj <> ChaineLue Then
                        SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 152, WsIdentifiant, CodeMaj, ChaineLue, ChaineMaj)
                        If SiOK Then
                            FichePER.ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
                        End If
                    End If
                ElseIf FichePER.Ide_Dossier = WsIdentifiant Then
                    SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 152, WsIdentifiant, "S", FichePER.FicheLue, "")
                End If
            Next

            If WsLst_153 IsNot Nothing Then
                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE In WsLst_153
                    ChaineLue = ""
                    CodeMaj = "C"
                    If (FichePER.Critere_Numerique > 0 Or FichePER.Critere_Numerique_Requis > 0) Or FichePER.Appreciation_Competence <> "" Then
                        If FichePER.Ide_Dossier = WsIdentifiant Then
                            ChaineLue = FichePER.FicheLue
                            CodeMaj = "M"
                        End If
                        FichePER.Date_de_Valeur = WsObjet_150.Date_de_Valeur
                        ChaineMaj = FichePER.ContenuTable
                        If ChaineMaj <> ChaineLue And FichePER.Competence <> "" Then
                            SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 153, WsIdentifiant, CodeMaj, ChaineLue, ChaineMaj)
                            If SiOK Then
                                FichePER.ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
                            End If
                        End If
                    ElseIf FichePER.Ide_Dossier = WsIdentifiant Then
                        SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 153, WsIdentifiant, "S", FichePER.FicheLue, "")
                    End If
                Next
            End If

            For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBSERVATION In WsLst_154
                ChaineLue = ""
                CodeMaj = "C"
                If FichePER.Observation <> "" Then
                    If FichePER.Ide_Dossier = WsIdentifiant Then
                        ChaineLue = FichePER.FicheLue
                        CodeMaj = "M"
                    End If
                    FichePER.Date_de_Valeur = WsObjet_150.Date_de_Valeur
                    ChaineMaj = FichePER.ContenuTable
                    If ChaineMaj <> ChaineLue Then
                        SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 154, WsIdentifiant, CodeMaj, ChaineLue, ChaineMaj)
                        If SiOK Then
                            FichePER.ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
                        End If
                    End If
                ElseIf FichePER.Ide_Dossier = WsIdentifiant Then
                    SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 154, WsIdentifiant, "S", FichePER.FicheLue, "")
                End If
            Next

            For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION In WsLst_155
                ChaineLue = ""
                CodeMaj = "C"
                SiAfaire = False
                Select Case WsRefCompetence.TypeEntretien
                    Case "CE"
                        If FichePER.Expression_Besoin <> "" Or FichePER.Objectif <> "" Then
                            SiAfaire = True
                        End If
                    Case Else
                        If FichePER.Intitule <> "" Then
                            SiAfaire = True
                        End If
                End Select
                If SiAfaire = True Then
                    If FichePER.Ide_Dossier = WsIdentifiant Then
                        ChaineLue = FichePER.FicheLue
                        CodeMaj = "M"
                    End If
                    FichePER.Date_de_Valeur = WsObjet_150.Date_de_Valeur
                    ChaineMaj = FichePER.ContenuTable
                    If ChaineMaj <> ChaineLue Then
                        SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 155, WsIdentifiant, CodeMaj, ChaineLue, ChaineMaj)
                        If SiOK Then
                            FichePER.ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
                        End If
                    End If
                ElseIf FichePER.Ide_Dossier = WsIdentifiant Then
                    SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 155, WsIdentifiant, "S", FichePER.FicheLue, "")
                End If
            Next

            If WsLst_156 IsNot Nothing Then
                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE In WsLst_156
                    ChaineLue = ""
                    CodeMaj = "C"
                    If FichePER.Critere_Numerique > 0 Or FichePER.Critere_Litteral <> "" Then
                        If FichePER.Ide_Dossier = WsIdentifiant Then
                            ChaineLue = FichePER.FicheLue
                            CodeMaj = "M"
                        End If
                        FichePER.Date_de_Valeur = WsObjet_150.Date_de_Valeur
                        ChaineMaj = FichePER.ContenuTable
                        If ChaineMaj <> ChaineLue Then
                            SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 156, WsIdentifiant, CodeMaj, ChaineLue, ChaineMaj)
                            If SiOK Then
                                FichePER.ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
                            End If
                        End If
                    ElseIf FichePER.Ide_Dossier = WsIdentifiant Then
                        SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 156, WsIdentifiant, "S", FichePER.FicheLue, "")
                    End If
                Next
            End If

            If WsLst_157 IsNot Nothing Then
                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT In WsLst_157
                    ChaineLue = ""
                    CodeMaj = "C"
                    If FichePER.Valeur <> "" Then
                        If FichePER.Ide_Dossier = WsIdentifiant Then
                            ChaineLue = FichePER.FicheLue
                            CodeMaj = "M"
                        End If
                        FichePER.Date_de_Valeur = WsObjet_150.Date_de_Valeur
                        ChaineMaj = FichePER.ContenuTable
                        If ChaineMaj <> ChaineLue Then
                            SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 157, WsIdentifiant, CodeMaj, ChaineLue, ChaineMaj)
                            If SiOK Then
                                FichePER.ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
                            End If
                        End If
                    ElseIf FichePER.Ide_Dossier = WsIdentifiant Then
                        SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 157, WsIdentifiant, "S", FichePER.FicheLue, "")
                    End If
                    If WsDateSignatureObservations = "" Then
                        If Objet_157("2").V_TableauData(4).ToString() <> "" Then
                            WsSiEtape2Valide = True
                        End If
                    End If
                Next
            End If

            Return SiOK
        End Function

        Public ReadOnly Property Objet_150() As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    For Each Virfiche In LstFiches
                        If Strings.Right(Virfiche.Date_de_Valeur, 4) = WsAnnee Then
                            Return CType(Virfiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL)
                        End If
                    Next
                End If
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Objet_150(ByVal ArgumentAnnee As String) As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    For Each Virfiche In LstFiches
                        If Strings.Right(Virfiche.Date_de_Valeur, 4) = ArgumentAnnee Then
                            Return CType(Virfiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL)
                        End If
                    Next
                End If
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Objet_151(ByVal Rang As String) As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT
            Get
                If WsLst_151 Is Nothing Then
                    Return Nothing
                End If
                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT In WsLst_151
                    If FichePER.Numero.ToString = Rang Then
                        Return FichePER
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Objet_152(ByVal Rang As String) As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF
            Get
                If WsLst_152 Is Nothing Then
                    Return Nothing
                End If
                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF In WsLst_152
                    If FichePER.Numero.ToString = Rang Then
                        Return FichePER
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Objet_153(ByVal Rang As String) As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE
            Get
                If WsLst_153 Is Nothing Then
                    Return Nothing
                End If
                Dim IndiceF As Integer

                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE In WsLst_153
                    Select Case WsRefCompetence.TypeEntretien
                        Case "CESE", "CESE2", "FAM"
                            If IsNumeric(Rang) Then
                                If FichePER.NumeroTri = Rang Then
                                    Return FichePER
                                End If
                            End If
                        Case "AFB"
                            If IsNumeric(Rang) Then
                                If FichePER.NumeroTri = Rang Then
                                    Return FichePER
                                End If
                            Else
                                If FichePER.Competence = Rang Then
                                    Return FichePER
                                End If
                            End If
                        Case Else
                            If FichePER.Competence = Rang Then
                                Return FichePER
                            End If
                    End Select
                Next
                Select Case WsRefCompetence.TypeEntretien
                    Case "CE"
                        For IndiceF = 0 To WsRefCompetence.NombreMaxi("CMP") - 1
                            Select Case IndiceF
                                Case Is <> 7 'autres
                                    If WsRefCompetence.TableCompetence(IndiceF, 0) = Rang Then
                                        Return Nothing
                                    End If
                            End Select
                        Next IndiceF
                        For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE In WsLst_153
                            If FichePER.NumeroTri = 8 Then 'Autres
                                Return FichePER
                            End If
                        Next
                End Select
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Objet_154(ByVal Rang As String) As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBSERVATION
            Get
                If WsLst_154 Is Nothing Then
                    Return Nothing
                End If
                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBSERVATION In WsLst_154
                    If FichePER.Numero_Famille = Rang Then
                        Return FichePER
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Objet_155(ByVal Rang As String) As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION
            Get
                If WsLst_155 Is Nothing Then
                    Return Nothing
                End If

                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION In WsLst_155
                    Select Case WsRefCompetence.TypeEntretien
                        Case "CE"
                            If FichePER.Intitule = Rang Then
                                Return FichePER
                            End If
                        Case Else
                            If IsNumeric(Rang) Then
                                If FichePER.NumeroTri = CInt(Rang) Then
                                    Return FichePER
                                End If
                            End If
                    End Select
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Objet_156(ByVal Rang As String) As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE
            Get
                If WsLst_156 Is Nothing Then
                    Return Nothing
                End If
                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE In WsLst_156
                    If FichePER.Intitule_Famille = Rang Then
                        Return FichePER
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Objet_157(ByVal Rang As String) As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT
            Get
                If IsNumeric(Rang) = False Then
                    Return Nothing
                End If
                If WsLst_157 Is Nothing Then
                    Return Nothing
                End If
                For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT In WsLst_157
                    If FichePER.Rang = CInt(Rang) Then
                        Return FichePER
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property ListeObjet151() As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT)
            Get
                Return (From Fiche In WsLst_151 Order By Fiche.Numero).ToList
            End Get
        End Property

        Public ReadOnly Property ListeObjet152() As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF)
            Get
                Return (From Fiche In WsLst_152 Order By Fiche.Numero).ToList
            End Get
        End Property

        Public ReadOnly Property ListeObjet153() As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE)
            Get
                If WsLst_153 Is Nothing Then
                    Return Nothing
                Else
                    Return (From Fiche In WsLst_153 Order By Fiche.NumeroTri).ToList
                End If
            End Get
        End Property

        Public ReadOnly Property ListeObjet154() As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBSERVATION)
            Get
                ''AKR 14/11/2017 : modification pour le conseil d'etat probleme de décalage des perspectives
                ''Return WsLst_154
                Return (From Fiche In WsLst_154 Order By Fiche.Numero_Famille).ToList
            End Get
        End Property

        Public ReadOnly Property ListeObjet155() As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION)
            Get
                Return (From Fiche In WsLst_155 Order By Fiche.NumeroTri).ToList
            End Get
        End Property

        Public ReadOnly Property ListeObjet156() As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE)
            Get
                If WsLst_156 Is Nothing Then
                    Return Nothing
                Else
                    Return (From Fiche In WsLst_156 Order By Fiche.NumeroTri).ToList
                End If
            End Get
        End Property

        Public ReadOnly Property ListeObjet157() As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT)
            Get
                If WsLst_157 Is Nothing Then
                    Return Nothing
                Else
                    Return (From Fiche In WsLst_157 Order By Fiche.Rang).ToList
                End If
            End Get
        End Property

        Private Sub FaireListe_151()
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceF As Integer
            Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT
            Dim DateEffet As String = WsObjet_150.Date_de_Valeur

            LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 151 Order By instance.Date_Valeur_ToDate Descending).ToList

            WsLst_151 = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT)
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each VirFiche In LstFiches
                    If VirFiche.Date_Valeur_ToDate > DateValue("31/12/2008") Then
                        If Strings.Right(VirFiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                            WsLst_151.Add(CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_RESULTAT))
                            DateEffet = VirFiche.Date_de_Valeur
                        End If
                    End If
                Next
            End If
            '** Ajout des fiches manquantes
            If WsLst_151.Count = WsRefCompetence.NombreMaxi("RES") Then
                Exit Sub
            End If
            For IndiceF = 1 To WsRefCompetence.NombreMaxi("RES")
                If Objet_151(IndiceF) Is Nothing Then
                    Fiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(151, 0)
                    Fiche.Date_de_Valeur = DateEffet
                    Fiche.Numero = IndiceF
                    Fiche.Critere_Numerique = 0
                    Fiche.Critere_Litteral = ""
                    WsListeEntretiens.Add(Fiche)
                    WsLst_151.Add(Fiche)
                End If
            Next IndiceF
        End Sub

        Private Sub FaireListe_152(Optional ByVal ArgumentDate As String = "")
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceF As Integer
            Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF
            Dim DateEffet As String = WsObjet_150.Date_de_Valeur
            If ArgumentDate <> "" Then
                DateEffet = ArgumentDate
            End If

            LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 152 Order By instance.Date_Valeur_ToDate Descending).ToList

            WsLst_152 = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF)
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each VirFiche In LstFiches
                    If VirFiche.Date_Valeur_ToDate > DateValue("31/12/2008") Then
                        If Strings.Right(VirFiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                            WsLst_152.Add(CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBJECTIF))
                            DateEffet = VirFiche.Date_de_Valeur
                        End If
                    End If
                Next
            End If
            '** Ajout des fiches manquantes
            If WsLst_152.Count = WsRefCompetence.NombreMaxi("RES") Then
                Exit Sub
            End If
            For IndiceF = 1 To WsRefCompetence.NombreMaxi("RES")
                If Objet_152(IndiceF) Is Nothing Then
                    Fiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(152, 0)
                    Fiche.Date_de_Valeur = DateEffet
                    Fiche.Numero = IndiceF
                    Fiche.Intitule = ""
                    WsListeEntretiens.Add(Fiche)
                    WsLst_152.Add(Fiche)
                End If
            Next IndiceF
        End Sub

        Private Sub FaireListe_153()
            If WsRefCompetence.NombreMaxi("CMP") = 0 Then
                Exit Sub
            End If
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceK As Integer = 0
            Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE
            Dim DateEffet As String = WsObjet_150.Date_de_Valeur

            LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 153 Order By instance.Date_Valeur_ToDate Descending).ToList

            WsLst_153 = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE)
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each VirFiche In LstFiches
                    If VirFiche.Date_Valeur_ToDate > DateValue("31/12/2008") Then
                        If Strings.Right(VirFiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                            WsLst_153.Add(CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPETENCE))
                            DateEffet = VirFiche.Date_de_Valeur
                        End If
                    End If
                Next
            End If
            '** Ajout des fiches manquantes
            If WsLst_153.Count = WsRefCompetence.NombreMaxi("CMP") Then
                Exit Sub
            End If
            Select Case WsRefCompetence.TypeEntretien
                Case "CESE", "CESE2", "FAM"
                    IndiceK = 5
                Case Else
                    IndiceK = 0
            End Select
            For IndiceF = 0 To WsRefCompetence.NombreMaxi("CMP") - 1
                Select Case WsRefCompetence.TypeEntretien
                    Case "AFB"
                        IndiceK = 5

                End Select
                If Objet_153(WsRefCompetence.TableCompetence(IndiceF, IndiceK)) Is Nothing Then
                    Fiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(153, 0)
                    Fiche.Date_de_Valeur = DateEffet
                    Fiche.Competence = WsRefCompetence.TableCompetence(IndiceF, 0)
                    Fiche.Numero_Famille = WsRefCompetence.TableCompetence(IndiceF, 1)
                    Fiche.Intitule_Famille = WsRefCompetence.TableCompetence(IndiceF, 2)
                    Fiche.Numero_SousFamille = WsRefCompetence.TableCompetence(IndiceF, 3)
                    Fiche.Intitule_SousFamille = WsRefCompetence.TableCompetence(IndiceF, 4)
                    If IndiceF = 8 Then
                        Dim a = 0
                    End If
                    Fiche.NumeroTri = CInt(WsRefCompetence.TableCompetence(IndiceF, 5))
                    Fiche.Critere_Numerique = 0
                    Fiche.Critere_Litteral = ""
                    WsListeEntretiens.Add(Fiche)
                    WsLst_153.Add(Fiche)
                End If
            Next IndiceF
        End Sub

        Private Sub FaireListe_154()
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceF As Integer
            Dim IndiceK As Integer = 0
            Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBSERVATION
            Dim DateEffet As String = WsObjet_150.Date_de_Valeur

            LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 154 Order By instance.Date_Valeur_ToDate Descending).ToList

            WsLst_154 = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBSERVATION)
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each VirFiche In LstFiches
                    If VirFiche.Date_Valeur_ToDate > DateValue("31/12/2008") Then
                        If Strings.Right(VirFiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                            WsLst_154.Add(CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_OBSERVATION))
                            DateEffet = VirFiche.Date_de_Valeur
                        End If
                    End If
                Next
            End If
            '** Ajout des fiches manquantes
            If WsLst_154.Count = WsRefCompetence.NombreMaxi("OBS") Then
                Exit Sub
            End If
            For IndiceF = 0 To WsRefCompetence.NombreMaxi("OBS") - 1
                If Objet_154(WsRefCompetence.TableObservation(IndiceF, 0)) Is Nothing Then
                    Fiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(154, 0)
                    Fiche.Date_de_Valeur = DateEffet
                    Fiche.Numero_Famille = WsRefCompetence.TableObservation(IndiceF, 0)
                    Fiche.Intitule_Famille = WsRefCompetence.TableObservation(IndiceF, 1)
                    WsListeEntretiens.Add(Fiche)
                    WsLst_154.Add(Fiche)
                End If
            Next IndiceF
        End Sub

        Private Sub FaireListe_155()
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceF As Integer
            Dim IndiceK As Integer = 0
            Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION
            Dim DateEffet As String = WsObjet_150.Date_de_Valeur

            LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 155 Order By instance.Date_Valeur_ToDate Descending).ToList

            WsLst_155 = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION)
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each VirFiche In LstFiches
                    If VirFiche.Date_Valeur_ToDate > DateValue("31/12/2008") Then
                        If Strings.Right(VirFiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                            WsLst_155.Add(CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_FORMATION))
                            DateEffet = VirFiche.Date_de_Valeur
                        End If
                    End If
                Next
            End If
            '** Ajout des fiches manquantes
            If WsLst_155.Count = WsRefCompetence.NombreMaxi("FOR") Then
                Exit Sub
            End If
            Select Case WsRefCompetence.TypeEntretien
                Case "CE"
                    IndiceK = 0
                Case Else
                    IndiceK = 2
            End Select
            For IndiceF = 0 To WsRefCompetence.NombreMaxi("FOR") - 1
                If Objet_155(WsRefCompetence.TableFormation(IndiceF, IndiceK)) Is Nothing Then
                    Fiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(155, 0)
                    Fiche.Date_de_Valeur = DateEffet
                    Fiche.Intitule = WsRefCompetence.TableFormation(IndiceF, 0)
                    Fiche.Type_du_Besoin = WsRefCompetence.TableFormation(IndiceF, 1)
                    Fiche.NumeroTri = CInt(WsRefCompetence.TableFormation(IndiceF, 2))
                    Select Case WsRefCompetence.TypeEntretien
                        Case "FAM"
                            If IndiceF >= 10 Then
                                Fiche.Type_du_Besoin = WsRefCompetence.TableFormation(IndiceF, 0)
                            End If
                    End Select
                    WsListeEntretiens.Add(Fiche)
                    WsLst_155.Add(Fiche)
                End If
            Next IndiceF
        End Sub

        Private Sub FaireListe_156()
            If WsRefCompetence.NombreMaxi("SYN") = 0 Then
                Exit Sub
            End If
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceF As Integer
            Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE
            Dim DateEffet As String = WsObjet_150.Date_de_Valeur

            LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 156 Order By instance.Date_Valeur_ToDate Descending).ToList

            WsLst_156 = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE)
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each VirFiche In LstFiches
                    If VirFiche.Date_Valeur_ToDate > DateValue("31/12/2008") Then
                        If Strings.Right(VirFiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                            WsLst_156.Add(CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_SYNTHESE))
                            DateEffet = VirFiche.Date_de_Valeur
                        End If
                    End If
                Next
            End If
            '** Ajout des fiches manquantes
            If WsLst_156.Count = WsRefCompetence.NombreMaxi("SYN") Then
                Exit Sub
            End If
            For IndiceF = 0 To WsRefCompetence.NombreMaxi("SYN") - 1
                If Objet_156(WsRefCompetence.TableSynthese(IndiceF, 0)) Is Nothing Then
                    Fiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(156, 0)
                    Fiche.Date_de_Valeur = DateEffet
                    Fiche.Intitule_Famille = WsRefCompetence.TableSynthese(IndiceF, 0)
                    Fiche.Numero_Famille = WsRefCompetence.TableSynthese(IndiceF, 1)
                    Fiche.NumeroTri = CInt(WsRefCompetence.TableSynthese(IndiceF, 2))
                    Fiche.Critere_Numerique = 0
                    Fiche.Critere_Litteral = ""
                    WsListeEntretiens.Add(Fiche)
                    WsLst_156.Add(Fiche)
                End If
            Next IndiceF
        End Sub

        Private Sub FaireListe_157()
            If WsRefCompetence.NombreMaxi("CPL") = 0 Then
                Exit Sub
            End If
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceF As Integer
            Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT
            Dim DateEffet As String = WsObjet_150.Date_de_Valeur

            LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 157 Order By instance.Date_Valeur_ToDate Descending).ToList

            WsLst_157 = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT)
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each VirFiche In LstFiches
                    If VirFiche.Date_Valeur_ToDate > DateValue("31/12/2008") Then
                        If Strings.Right(VirFiche.Date_de_Valeur, 4) = Strings.Right(DateEffet, 4) Then
                            WsLst_157.Add(CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_COMPLEMENT))
                            DateEffet = VirFiche.Date_de_Valeur
                        End If
                    End If
                Next
            End If
            '** Ajout des fiches manquantes
            If WsLst_157.Count = WsRefCompetence.NombreMaxi("CPL") Then
                Exit Sub
            End If
            For IndiceF = 0 To WsRefCompetence.NombreMaxi("CPL") - 1
                If Objet_157(WsRefCompetence.TableComplement(IndiceF, 0)) Is Nothing Then
                    Fiche = WsPointeurGlobal.VirRhShemaPER.V_NouvelleFiche(157, 0)
                    Fiche.Date_de_Valeur = DateEffet
                    Fiche.Rang = CInt(WsRefCompetence.TableComplement(IndiceF, 0))
                    Fiche.Intitule = WsRefCompetence.TableComplement(IndiceF, 1)
                    Fiche.Nature = CInt(WsRefCompetence.TableComplement(IndiceF, 2))
                    Fiche.Valeur = ""
                    WsListeEntretiens.Add(Fiche)
                    WsLst_157.Add(Fiche)
                End If
            Next IndiceF

            ''**** AKR
            If Objet_157("2") IsNot Nothing Then
                WsDateSignatureObservations = Objet_157("2").V_TableauData(4).ToString()
            End If
            '***
        End Sub

        Public ReadOnly Property ListeDesEntretiens As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL
                Dim Chaine As New System.Text.StringBuilder

                LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 Order By instance.Date_Valeur_ToDate Ascending).ToList
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    For Each Virfiche In LstFiches
                        Fiche = CType(Virfiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL)
                        If Fiche.Date_Valeur_ToDate > DateValue("31/12/2008") Then
                            Chaine.Append(Strings.Right(Fiche.Date_de_Valeur, 4) & VI.Tild)
                            Chaine.Append(Fiche.Evaluateur & VI.Tild)
                            Chaine.Append(Fiche.Entite_Evaluation & VI.Tild)
                            Chaine.Append(Fiche.Date_de_Signature_Evalue & VI.Tild)
                            Chaine.Append(Fiche.Date_de_Valeur & VI.SigneBarre)
                        End If
                    Next
                End If
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property ListeDesFormationsSuivies(ByVal Annee As String, ByVal Profondeur As Integer) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim Chaine As System.Text.StringBuilder
                Dim DateEffet As String = "31/12/" & Annee
                Dim DateProfondeur As String = "01/01/" & Annee - Profondeur
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_SESSION

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaFormation _
                                        And instance.Date_Valeur_ToDate >= CDate(DateProfondeur) _
                                        Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstFiches Is Nothing Then
                    Return ""
                End If
                Chaine = New System.Text.StringBuilder
                For Each VirFiche In LstFiches
                    Fiche = CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_SESSION)
                    Select Case WsPointeurGlobal.VirRhDates.ComparerDates(Fiche.Date_de_Valeur, DateEffet)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                            Select Case WsPointeurGlobal.VirRhDates.ComparerDates(Fiche.Date_de_Valeur, DateProfondeur)
                                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                    If Fiche.Motif_de_non_participation = "" Then
                                        Chaine.Append(Fiche.Date_de_Valeur & VI.Tild & Fiche.Intitule & VI.Tild)
                                        Select Case WsRefCompetence.TypeEntretien
                                            Case "CNED2"
                                                Chaine.Append(Fiche.Duree & " j." & VI.Tild)
                                            Case "CNM", "ENM", "GIP"
                                                Chaine.Append(Fiche.Duree & " j." & VI.Tild)
                                            Case "ISMEP"
                                                Chaine.Append(Fiche.Duree_en_heures & " h" & VI.Tild)
                                            Case "PNF", "AFB"
                                                Chaine.Append(Fiche.Duree_en_heures & VI.Tild)
                                                Chaine.Append(Fiche.Sidif & VI.Tild)
                                        End Select
                                        Chaine.Append(Fiche.Date_de_Valeur & VI.SigneBarre)
                                    End If
                            End Select
                    End Select
                Next
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property LibelleIdentite As String
            Get
                If WsFicheEtatCivil Is Nothing Then
                    Return ""
                End If
                Dim Chaine As String
                Chaine = WsFicheEtatCivil.Qualite & Strings.Space(1)
                Chaine &= WsFicheEtatCivil.Nom & Strings.Space(1)
                Chaine &= WsFicheEtatCivil.Prenom & Strings.Space(1)
                If WsFicheEtatCivil.Patronyme <> "" And WsFicheEtatCivil.Patronyme <> WsFicheEtatCivil.Nom Then
                    Chaine &= "(" & WsFicheEtatCivil.Patronyme & ")"
                End If
                Return Chaine
            End Get
        End Property

        Public ReadOnly Property LibelleNaissance As String
            Get
                If WsFicheEtatCivil Is Nothing Then
                    Return ""
                End If
                Dim Chaine As String = ""
                Select Case WsFicheEtatCivil.Sexe
                    Case Is = "Féminin"
                        Chaine = "Née le" & Strings.Space(1) & WsFicheEtatCivil.Date_de_naissance & Strings.Space(1)
                    Case Is = "Masculin"
                        Chaine = "Né le" & Strings.Space(1) & WsFicheEtatCivil.Date_de_naissance & Strings.Space(1)
                End Select
                Chaine &= " à " & WsFicheEtatCivil.Lieu_de_naissance & Strings.Space(1)
                Chaine &= WsFicheEtatCivil.Departement_ou_pays_de_naissan
                Return Chaine
            End Get
        End Property

        Public ReadOnly Property SiteGeographique As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaAdrPro).ToList
                If LstFiches Is Nothing Then
                    Return ""
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO).Sitegeo
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property TelephonePro As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaAdrPro).ToList
                If LstFiches Is Nothing Then
                    Return ""
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO).Telephone_professionnel
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property EmailPro As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaAdrPro).ToList
                If LstFiches Is Nothing Then
                    Return ""
                End If
                If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                    Return CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO).Email
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property EmailManager As String
            Get
                Return WsEmailManager
            End Get
        End Property

        Public ReadOnly Property EmailManagerN2 As String
            Get
                Return WsEmailManagerN2
            End Get
        End Property
        Public ReadOnly Property QualitePrenomNomManager As String
            Get
                Return WsQualitePrenomNomManager
            End Get
        End Property
        Public ReadOnly Property PrenomManager As String
            Get
                Return WsPrenomManager
            End Get
        End Property
        Public ReadOnly Property NomManager As String
            Get
                Return WsNomManager
            End Get
        End Property

        Public ReadOnly Property QualitePrenomNomManagerN2 As String
            Get
                Return WsQualitePrenomNomManagerN2
            End Get
        End Property

        Public ReadOnly Property CorpsManager As String
            Get
                Return WsCorpsManager
            End Get
        End Property

        Public ReadOnly Property GradeManager As String
            Get
                Return WsGradeManager
            End Get
        End Property

        Public ReadOnly Property StatutManager As String
            Get
                Return WsStatutManager
            End Get
        End Property
        Public ReadOnly Property FonctionManager As String
            Get
                Return WsFonctionManager
            End Get
        End Property
        Public ReadOnly Property FonctionManagerN2 As String
            Get
                Return WsFonctionManagerN2
            End Get
        End Property



        Public ReadOnly Property Specialite_CotationFAM As String
            Get
                Dim LstTriSpeciale As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE
                Dim IndiceF As Integer
                Dim VDateFin As String

                If WsObjet_150 Is Nothing Then
                    Return ""
                End If
                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaSpecialite).ToList
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Return ""
                End If
                For Each FichePER In LstFiches
                    If IsNumeric(CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_SPECIALITE).Niveau) Then
                        If CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_SPECIALITE).Dateacquisition <> "" Then
                            FichePER.Date_de_Valeur = CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_SPECIALITE).Dateacquisition
                        Else
                            FichePER.Date_de_Valeur = "01/01/" & WsAnnee
                        End If
                    Else
                        FichePER.Date_de_Valeur = ""
                    End If
                Next
                LstTriSpeciale = (From instance In LstFiches Select instance Where instance.Date_de_Valeur <> "" Order By instance.Date_Valeur_ToDate Descending).ToList
                If LstTriSpeciale IsNot Nothing Then
                    For IndiceF = 0 To LstTriSpeciale.Count - 1
                        LstTriSpeciale(IndiceF).DateDeFinCalculee = ""
                        If IndiceF > 0 Then
                            VDateFin = WsPointeurGlobal.VirRhDates.CalcDateMoinsJour(LstTriSpeciale.Item(IndiceF - 1).Date_de_Valeur, "0", "1")
                            LstTriSpeciale.Item(IndiceF).Date_de_Fin = VDateFin
                        End If
                        LstTriSpeciale.Item(IndiceF).DateDeFinCalculee = LstTriSpeciale.Item(IndiceF).Date_de_Fin
                    Next IndiceF
                End If
                FichePER = FichePer_Valable(LstTriSpeciale, WsObjet_150.Date_de_Valeur)
                If FichePER Is Nothing Then
                    Return ""
                End If
                Return CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_SPECIALITE).Niveau
            End Get
        End Property

        Public ReadOnly Property Document_FichePoste_Url(ByVal ServeurAutority As String, ByVal IdeUser As String) As String
            Get
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE
                Dim NomFichier As String = ""
                Dim UrlDestination As String = ""

                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaDocuments).ToList
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Return ""
                End If
                For Each FichePER In LstFiches
                    If CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS).Classement.Contains("poste") Then
                        NomFichier = CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS).Reference
                    End If
                Next
                If My.Computer.FileSystem.FileExists(NomFichier) = False Then
                    Return ""
                End If
                My.Computer.FileSystem.CopyFile(NomFichier, WsPointeurGlobal.PathPhysiqueFichierTxt & "\" & IdeUser & "\" & My.Computer.FileSystem.GetFileInfo(NomFichier).Name, True)

                UrlDestination = "http://" & ServeurAutority & WsPointeurGlobal.UrlFichierTxt & "\" & IdeUser & "\"
                UrlDestination &= My.Computer.FileSystem.GetFileInfo(NomFichier).Name
                Return UrlDestination
            End Get
        End Property

        Private ReadOnly Property FichePer_Valable(ByVal LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE), ByVal ArgumentDate As String) As Virtualia.Systeme.MetaModele.VIR_FICHE
            Get
                Dim IndiceF As Integer

                If LstFiches Is Nothing Then
                    Return Nothing
                End If

                For IndiceF = 0 To LstFiches.Count - 1
                    Select Case WsPointeurGlobal.VirRhDates.ComparerDates(LstFiches.Item(IndiceF).Date_de_Valeur, ArgumentDate)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                            If LstFiches.Item(IndiceF).DateDeFinCalculee = "" Then
                                Return LstFiches.Item(IndiceF)
                            End If
                            Select Case WsPointeurGlobal.VirRhDates.ComparerDates(LstFiches.Item(IndiceF).DateDeFinCalculee, ArgumentDate)
                                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                    Return LstFiches.Item(IndiceF)
                            End Select
                            Exit For
                    End Select
                Next IndiceF
                Return Nothing
            End Get
        End Property

        Private Sub InitialiserManager(ByVal DateEffet As String, ByVal IdentifiantManager As Integer)
            Dim TableauObjet(0) As String
            Dim TableauData(0) As String
            Dim IndiceI As Integer

            WsIdeManager = IdentifiantManager
            WsNomPrenomManager = ""
            WsCorpsManager = ""
            WsGradeManager = ""
            WsFonctionManager = ""

            If WsIdeManager = 0 Then
                Dim StructureAff As String
                For IndiceI = 4 To 1 Step -1
                    StructureAff = NiveauAffectation(DateEffet, IndiceI)
                    If StructureAff <> "" Then
                        WsIdeManager = WsPointeurGlobal.IdentifiantManager(CStr(IndiceI), StructureAff)
                        If WsIdeManager > 0 And WsIdeManager <> WsIdentifiant Then
                            Exit For
                        End If
                    End If
                Next IndiceI
            End If

            ''**** AKR AFB

            Select Case WsRefCompetence.TypeEntretien
                Case "AFB"
                    If WsIdeManagerN2 = 0 Then
                        Dim StructureAff As String
                        For IndiceI = 4 To 1 Step -1
                            StructureAff = NiveauAffectation(DateEffet, IndiceI)
                            If StructureAff <> "" Then
                                WsIdeManagerN2 = WsPointeurGlobal.IdentifiantManager(CStr(IndiceI), StructureAff)
                                If WsIdeManagerN2 > 0 And WsIdeManagerN2 <> WsIdentifiant And WsIdeManagerN2 <> WsIdeManager Then

                                    Exit For
                                    'For IndiceJ = IndiceI - 1 To 1 Step -1
                                    '    StructureAff = NiveauAffectation(DateEffet, IndiceJ)
                                    '    If StructureAff <> "" Then
                                    '        WsIdeManagerN2 = WsPointeurGlobal.IdentifiantManager(CStr(IndiceJ), StructureAff)
                                    '        If WsIdeManagerN2 > 0 And WsIdeManagerN2 <> WsIdentifiant And WsIdeManagerN2 <> WsIdeManager Then
                                    '            Exit For
                                    '        End If
                                    '    End If
                                    'Next IndiceJ
                                End If
                            End If
                        Next IndiceI
                    End If

                    If WsIdeManagerN2 = 0 Then
                        WsIdeManagerN2 = WsIdeManager
                    End If
            End Select
            ''*****

            If WsIdeManager = 0 Then
                Exit Sub
            End If

            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim FicheCivil As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
            Dim FicheGrade As Virtualia.TablesObjet.ShemaPER.PER_GRADE
            Dim FicheStatut As Virtualia.TablesObjet.ShemaPER.PER_STATUT
            Dim FicheOrga As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION

            LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaCivil, WsIdeManager, False)

            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                FicheCivil = New Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
                FicheCivil.ContenuTable = LstFiches.Item(0).Ide_Dossier & CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL).ContenuTable
                WsNomPrenomManager = FicheCivil.V_NomEtPrenom
                WsQualitePrenomNomManager = FicheCivil.Qualite & Strings.Space(1) & FicheCivil.Prenom & Strings.Space(1) & FicheCivil.Nom
                WsPrenomManager = FicheCivil.Prenom
                WsNomManager = FicheCivil.Nom
            End If

            LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, WsIdeManager, True)

            If DateEffet = "" Then
                DateEffet = WsPointeurGlobal.VirRhDates.DateduJour
            End If
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each FichepER In LstFiches
                    FicheGrade = New Virtualia.TablesObjet.ShemaPER.PER_GRADE
                    FicheGrade.ContenuTable = FichepER.Ide_Dossier & VI.Tild & CType(FichepER, Virtualia.TablesObjet.ShemaPER.PER_GRADE).ContenuTable
                    Select Case WsPointeurGlobal.VirRhDates.ComparerDates(FicheGrade.Date_de_Valeur, DateEffet)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                            WsGradeManager = FicheGrade.Grade
                            WsCorpsManager = FicheGrade.Corps
                            Exit For
                    End Select
                Next
            End If

            LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaStatut, WsIdeManager, True)

            If DateEffet = "" Then
                DateEffet = WsPointeurGlobal.VirRhDates.DateduJour
            End If
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each FichepER In LstFiches
                    FicheStatut = New Virtualia.TablesObjet.ShemaPER.PER_STATUT
                    FicheStatut.ContenuTable = FichepER.Ide_Dossier & VI.Tild & CType(FichepER, Virtualia.TablesObjet.ShemaPER.PER_STATUT).ContenuTable
                    Select Case WsPointeurGlobal.VirRhDates.ComparerDates(FicheStatut.Date_de_Valeur, DateEffet)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                            WsStatutManager = FicheStatut.Statut
                            Exit For
                    End Select
                Next
            End If


            LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaOrganigramme, WsIdeManager, True)

            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each FichepER In LstFiches
                    FicheOrga = New Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION
                    FicheOrga.ContenuTable = FichepER.Ide_Dossier & VI.Tild & CType(FichepER, Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION).ContenuTable
                    Select Case WsPointeurGlobal.VirRhDates.ComparerDates(FicheOrga.Date_de_Valeur, DateEffet)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                            WsFonctionManager = FicheOrga.Fonction_exercee
                            Exit For
                    End Select
                Next
            End If

            Select Case WsRefCompetence.TypeEntretien
                Case "FAM", "ENM", "GIP"
                    Dim FicheAdrPro As Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO
                    LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAdrPro, WsIdeManager, False)
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        FicheAdrPro = New Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO
                        FicheAdrPro.ContenuTable = LstFiches.Item(0).Ide_Dossier & CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO).ContenuTable
                        WsEmailManager = FicheAdrPro.Email
                    End If
            End Select

            ''**** AKR AFB
            Select Case WsRefCompetence.TypeEntretien
                Case "AFB"

                    LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaCivil, WsIdeManagerN2, False)

                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        FicheCivil = New Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
                        FicheCivil.ContenuTable = LstFiches.Item(0).Ide_Dossier & CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL).ContenuTable
                        WsNomPrenomManagerN2 = FicheCivil.V_NomEtPrenom
                        WsQualitePrenomNomManagerN2 = FicheCivil.Qualite & Strings.Space(1) & FicheCivil.Prenom & Strings.Space(1) & FicheCivil.Nom
                    End If

                    LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, WsIdeManagerN2, True)

                    If DateEffet = "" Then
                        DateEffet = WsPointeurGlobal.VirRhDates.DateduJour
                    End If
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        For Each FichepER In LstFiches
                            FicheGrade = New Virtualia.TablesObjet.ShemaPER.PER_GRADE
                            FicheGrade.ContenuTable = FichepER.Ide_Dossier & VI.Tild & CType(FichepER, Virtualia.TablesObjet.ShemaPER.PER_GRADE).ContenuTable
                            Select Case WsPointeurGlobal.VirRhDates.ComparerDates(FicheGrade.Date_de_Valeur, DateEffet)
                                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                    WsGradeManagerN2 = FicheGrade.Grade
                                    WsCorpsManagerN2 = FicheGrade.Corps
                                    Exit For
                            End Select
                        Next
                    End If

                    LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaOrganigramme, WsIdeManagerN2, True)

                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        For Each FichepER In LstFiches
                            FicheOrga = New Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION
                            FicheOrga.ContenuTable = FichepER.Ide_Dossier & VI.Tild & CType(FichepER, Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION).ContenuTable
                            Select Case WsPointeurGlobal.VirRhDates.ComparerDates(FicheOrga.Date_de_Valeur, DateEffet)
                                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                    WsFonctionManagerN2 = FicheOrga.Fonction_exercee
                                    Exit For
                            End Select
                        Next
                    End If

                    Select Case WsRefCompetence.TypeEntretien
                        Case "FAM", "ENM", "GIP"
                            Dim FicheAdrPro As Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO
                            LstFiches = WsPointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAdrPro, WsIdeManagerN2, False)
                            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                                FicheAdrPro = New Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO
                                FicheAdrPro.ContenuTable = LstFiches.Item(0).Ide_Dossier & CType(LstFiches.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO).ContenuTable
                                WsEmailManagerN2 = FicheAdrPro.Email
                            End If
                    End Select
            End Select
            ''*****

        End Sub

        Private Sub LireFondsDossier()
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim ListeObjets As List(Of Integer) = New List(Of Integer)
            ListeObjets.Add(VI.ObjetPer.ObaCivil)
            ListeObjets.Add(VI.ObjetPer.ObaSociete)
            ListeObjets.Add(VI.ObjetPer.ObaStatut)
            ListeObjets.Add(VI.ObjetPer.ObaOrganigramme)
            ListeObjets.Add(VI.ObjetPer.ObaAdrPro)
            ListeObjets.Add(VI.ObjetPer.ObaActivite)
            ListeObjets.Add(VI.ObjetPer.ObaGrade)
            ListeObjets.Add(VI.ObjetPer.ObaFormation)
            ListeObjets.Add(VI.ObjetPer.ObaDIF)
            ListeObjets.Add(VI.ObjetPer.ObaSpecialite)
            Select Case WsRefCompetence.TypeEntretien
                Case "CE"
                    ListeObjets.Add(VI.ObjetPer.ObaGradeDetache)
                Case "CESE2", "CNM"
                    ListeObjets.Add(VI.ObjetPer.ObaDocuments)
                    ListeObjets.Add(VI.ObjetPer.ObaNotation)
            End Select
            ListeObjets.Add(VI.ObjetPer.ObaExterne)
            If WsIdentifiant <= 0 Then
                Exit Sub
            End If
            Try
                WsListeFonds = WsPointeurGlobal.VirServiceServeur.LectureDossier_ToFiches(WsPointeurGlobal.PointeurObjetUtiGlobal.Nom, _
                                                                                  VI.PointdeVue.PVueApplicatif, WsIdentifiant, False, ListeObjets)
                LstFiches = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaCivil).ToList
            Catch ex As Exception
                Exit Sub
            End Try
            If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                Exit Sub
            End If
            WsFicheEtatCivil = CType(LstFiches(0), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)

        End Sub

        Public Property Referentiel_TypeEntretien As String
            Get
                Return WsRefCompetence.TypeEntretien
            End Get
            Set(value As String)
                If WsRefCompetence.TypeEntretien <> value Then
                    WsRefCompetence = New Virtualia.Net.Entretien.ObjetRefCompetence(value)
                End If
            End Set
        End Property

        Private Sub LireEntretiens()
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim ListeObjets As List(Of Integer) = New List(Of Integer)
            For IndiceI = 150 To 157
                ListeObjets.Add(IndiceI)
            Next
            ListeObjets.Add(VI.ObjetPer.ObaExterne)
            WsListeEntretiens = WsPointeurGlobal.VirServiceServeur.LectureDossier_ToFiches(WsPointeurGlobal.PointeurObjetUtiGlobal.Nom,
                                                                                       VI.PointdeVue.PVueApplicatif, WsIdentifiant, False, ListeObjets)
            WsAnnee = ""
            LstFiches = (From instance In WsListeEntretiens Select instance Where instance.NumeroObjet = 150 Order By instance.Date_Valeur_ToDate Descending).ToList
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                For Each VirFiche In LstFiches
                    WsObjet_150 = CType(VirFiche, Virtualia.TablesObjet.ShemaPER.PER_ENTRETIEN_GENERAL)
                    If WsObjet_150.Date_Valeur_ToDate > CDate("31/12/2008") Then
                        WsAnnee = Strings.Right(WsObjet_150.Date_de_Valeur, 4)
                        Exit For
                    End If
                Next
            End If
            If WsAnnee = "" OrElse CInt(WsAnnee) < Now.Year Then
                WsAnnee = Strings.Right(WsPointeurGlobal.Date_Limite(WsPointeurGlobal.VirRhDates.DateduJour), 4)
            End If
            Annee = WsAnnee
        End Sub


        ''********** AKR 31/08/2017
        Private Sub Lire_MET_Metier()

            Dim ListeObjets As List(Of Integer) = New List(Of Integer)


            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim TabRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceI As Integer

            'Requete pour sélectionner le referientiel Metier
            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsPointeurGlobal.VirModele, WsPointeurGlobal.PointeurObjetUtiGlobal.InstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueMetier, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = False
            Constructeur.SiForcerClauseDistinct = True

            Constructeur.NoInfoSelection(0, 1) = 1
            For IndiceI = 1 To 10
                Constructeur.InfoExtraite(IndiceI, 1, 0) = CInt(IndiceI)
            Next IndiceI

            TabRes = WsPointeurGlobal.VirServiceServeur.RequeteSql_ToFiches(WsPointeurGlobal.PointeurObjetUtiGlobal.Nom, VI.PointdeVue.PVueMetier, 1, Constructeur.OrdreSqlDynamique)

            Constructeur = Nothing

            If TabRes Is Nothing Then
                Exit Sub
            End If
            WsLst_METIER = New List(Of Virtualia.TablesObjet.ShemaREF.MET_METIER)
            For IndiceI = 0 To TabRes.Count - 1
                WsLst_METIER.Add(DirectCast(TabRes.Item(IndiceI), Virtualia.TablesObjet.ShemaREF.MET_METIER))
            Next IndiceI
        End Sub

        Private Sub Lire_MET_Activites()

            Dim ListeObjets As List(Of Integer) = New List(Of Integer)


            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim TabRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceI As Integer

            'Requete pour sélectionner le referientiel activités
            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsPointeurGlobal.VirModele, WsPointeurGlobal.PointeurObjetUtiGlobal.InstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueMetier, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = False
            Constructeur.SiForcerClauseDistinct = True

            Constructeur.NoInfoSelection(0, 9) = 1
            Constructeur.InfoExtraite(1, 9, 0) = 1

            TabRes = WsPointeurGlobal.VirServiceServeur.RequeteSql_ToFiches(WsPointeurGlobal.PointeurObjetUtiGlobal.Nom, VI.PointdeVue.PVueMetier, 9, Constructeur.OrdreSqlDynamique)

            Constructeur = Nothing

            If TabRes Is Nothing Then
                Exit Sub
            End If
            WsLst_ACTIVITES = New List(Of Virtualia.TablesObjet.ShemaREF.MET_ACTIVITES)
            For IndiceI = 0 To TabRes.Count - 1
                WsLst_ACTIVITES.Add(DirectCast(TabRes.Item(IndiceI), Virtualia.TablesObjet.ShemaREF.MET_ACTIVITES))
            Next IndiceI
        End Sub

        Private Sub Lire_MET_Aptitudes()

            Dim ListeObjets As List(Of Integer) = New List(Of Integer)


            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim TabRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceI As Integer

            'Requete pour sélectionner le referientiel Aptitudes
            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsPointeurGlobal.VirModele, WsPointeurGlobal.PointeurObjetUtiGlobal.InstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueMetier, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = False
            Constructeur.SiForcerClauseDistinct = True

            Constructeur.NoInfoSelection(0, 12) = 1
            Constructeur.InfoExtraite(1, 12, 0) = 1

            TabRes = WsPointeurGlobal.VirServiceServeur.RequeteSql_ToFiches(WsPointeurGlobal.PointeurObjetUtiGlobal.Nom, VI.PointdeVue.PVueMetier, 12, Constructeur.OrdreSqlDynamique)

            Constructeur = Nothing

            If TabRes Is Nothing Then
                Exit Sub
            End If
            WsLst_APTITUDES = New List(Of Virtualia.TablesObjet.ShemaREF.MET_APTITUDES)
            For IndiceI = 0 To TabRes.Count - 1
                WsLst_APTITUDES.Add(DirectCast(TabRes.Item(IndiceI), Virtualia.TablesObjet.ShemaREF.MET_APTITUDES))
            Next IndiceI
        End Sub

        Private Sub Lire_MET_Environnement()

            Dim ListeObjets As List(Of Integer) = New List(Of Integer)


            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim TabRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceI As Integer

            'Requete pour sélectionner le referientiel Aptitudes
            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsPointeurGlobal.VirModele, WsPointeurGlobal.PointeurObjetUtiGlobal.InstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueMetier, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = False
            Constructeur.SiForcerClauseDistinct = True

            Constructeur.NoInfoSelection(0, 13) = 1
            Constructeur.InfoExtraite(1, 13, 0) = 1

            TabRes = WsPointeurGlobal.VirServiceServeur.RequeteSql_ToFiches(WsPointeurGlobal.PointeurObjetUtiGlobal.Nom, VI.PointdeVue.PVueMetier, 13, Constructeur.OrdreSqlDynamique)

            Constructeur = Nothing

            If TabRes Is Nothing Then
                Exit Sub
            End If
            WsLst_Environement = New List(Of Virtualia.TablesObjet.ShemaREF.MET_ENVIRONNEMENT)
            For IndiceI = 0 To TabRes.Count - 1
                WsLst_Environement.Add(DirectCast(TabRes.Item(IndiceI), Virtualia.TablesObjet.ShemaREF.MET_ENVIRONNEMENT))
            Next IndiceI
        End Sub
        ''**********

        Public Sub New(ByVal PointeurGlobal As Virtualia.Net.WebAppli.ObjetGlobal, ByVal Ide As Integer, ByVal NomCnx As String)
            WsPointeurGlobal = PointeurGlobal
            WsNomUtiSGBD = WsPointeurGlobal.PointeurObjetUtiGlobal.Nom
            WsNomUtilisateur = NomCnx

            WsRefCompetence = New Virtualia.Net.Entretien.ObjetRefCompetence(System.Configuration.ConfigurationManager.AppSettings("TypeEntretien"))
            WsIdentifiant = Ide
            Call LireFondsDossier()
            Call LireEntretiens()

            ''***AKR
            Call Lire_MET_Metier()
            Call Lire_MET_Activites()
            Call Lire_MET_Aptitudes()
            Call Lire_MET_Environnement()

        End Sub

        Public Sub New(ByVal PointeurGlobal As Virtualia.Net.WebAppli.ObjetGlobal, ByVal Ide As Integer, ByVal ListeFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE))
            WsPointeurGlobal = PointeurGlobal
            WsNomUtiSGBD = PointeurGlobal.PointeurObjetUtiGlobal.Nom
            WsNomUtilisateur = PointeurGlobal.PointeurObjetUtiGlobal.Nom
            WsRefCompetence = PointeurGlobal.VirRefCompetence
            WsIdentifiant = Ide

            WsListeFonds = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            WsListeEntretiens = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            For Each Virfiche In ListeFiches
                Select Case Virfiche.NumeroObjet
                    Case 150 To 157
                        WsListeEntretiens.Add(Virfiche)
                    Case Else
                        WsListeFonds.Add(Virfiche)
                End Select
            Next

            Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            LstRes = (From instance In WsListeFonds Select instance Where instance.NumeroObjet = VI.ObjetPer.ObaCivil).ToList
            If LstRes Is Nothing Then
                Exit Sub
            End If
            WsFicheEtatCivil = CType(LstRes(0), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)

        End Sub
    End Class
End Namespace

