﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P1_CNED2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P1_CNED2"%>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia"%>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="I_CadreInfo" runat="server" BorderStyle="None" Visible="true" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <asp:Table ID="I_CadreEnteteDePage1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelEnteteLigne11" runat="server" Text="" Height="20px" Width="748px"
                            CssClass="EP_Imp_SmallNormal"
                            style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelEnteteLigne12" runat="server" Text="" Height="20px" Width="748px"
                            CssClass="EP_Imp_SmallNormal"
                            style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreTitreResultats" runat="server" Height="25px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVoletEntete" runat="server" Text="2 - " Height="20px" Width="30px"
                            CssClass="EP_Imp_LargerBold">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVolet" runat="server" Text="Evaluation de l'année écoulée" Height="20px" Width="300px"
                            CssClass="EP_Imp_LargerBoldSouligne">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVoletSuite" runat="server" Text="" Height="20px" Width="416px"
                            CssClass="EP_Imp_LargerBold">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelTitreNumero11" runat="server" Height="20px" Width="748px"
                           Text="2.1 Rappel des objectifs d'activités attendus fixés l'année précédente"
                           CssClass="EP_Imp_MediumBold"
                           style="text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelTitreNumero12" runat="server" Height="20px" Width="748px"
                           Text="(Merci d'indiquer si des démarches ou moyens spécifiques ont été mis en oeuvre pour atteindre ces objectifs)"
                           CssClass="EP_Imp_SmallNormal">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="300px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="I_LabelEnteteObjectif1" runat="server" Text="Rappel de l'objectif" Height="30px" Width="241px"
                                        CssClass="EP_Imp_SmallBold"
                                        Font-Underline="true"
                                        style="padding-top: 8px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteDifficultes1" runat="server" CellPadding="0" CellSpacing="0" Width="440px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="I_LabelEnteteDifficultes1" runat="server" Text="Evaluation des résultats" Height="30px" Width="265px"
                                        CssClass="EP_Imp_SmallBold"
                                        Font-Underline="true"
                                        style="padding-top: 8px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Table ID="I_TableNumerobjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="745px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="left">
                                    <asp:Label ID="I_LabelNumerobjectif1" runat="server" Text="Objectif n°1" Height="22px" Width="745px"
                                        CssClass="EP_Imp_MediumNormal"
                                        style="padding-top: 3px; text-indent: 100px;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="300px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVA02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="260px" DonHeight="140px" DonTabIndex="1"
                                        Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes1" runat="server" CellPadding="0" CellSpacing="0" Width="440px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVA03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="330px" DonHeight="140px" DonTabIndex="2"
                                        Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Table ID="I_TableNumerobjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="745px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="left">
                                    <asp:Label ID="I_LabelNumerobjectif2" runat="server" Text="Objectif n°2" Height="22px" Width="745px"
                                        CssClass="EP_Imp_MediumNormal"
                                        style="padding-top: 3px; text-indent: 100px;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="300px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVB02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="260px" DonHeight="140px" DonTabIndex="3"
                                        Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes2" runat="server" CellPadding="0" CellSpacing="0" Width="440px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVB03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="330px" DonHeight="140px" DonTabIndex="4"
                                        Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Table ID="I_TableNumerobjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="745px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="left">
                                    <asp:Label ID="I_LabelNumerobjectif3" runat="server" Text="Objectif n°3" Height="22px" Width="745px"
                                        CssClass="EP_Imp_MediumNormal"
                                        style="padding-top: 3px; text-indent: 100px;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="300px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="260px" DonHeight="140px" DonTabIndex="5"
                                        Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes3" runat="server" CellPadding="0" CellSpacing="0" Width="440px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="330px" DonHeight="140px" DonTabIndex="6"
                                        Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableFooterRow>
                    <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom" ColumnSpan="2">
                        <asp:Label ID="NumeroPage2" runat="server" Height="15px" Width="250px"
                             BackColor="Transparent" BorderStyle="None"
                             Text="2 / 13" ForeColor="Black" Font-Italic="True"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                             style="text-align:right" >
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <asp:Table ID="I_CadreEnteteDePage2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteLigne21" runat="server" Text="" Height="20px" Width="748px"
                                        CssClass="EP_Imp_SmallNormal"
                                        style="text-indent: 4px;">
                                    </asp:Label>          
                                </asp:TableCell>           
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteLigne22" runat="server" Text="" Height="20px" Width="748px"
                                        CssClass="EP_Imp_SmallNormal"
                                        style="text-indent: 4px;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>   
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="300px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="I_LabelEnteteObjectif2" runat="server" Text="Rappel de l'objectif" Height="30px" Width="241px"
                                        CssClass="EP_Imp_SmallBold"
                                        Font-Underline="true"
                                        style="padding-top: 8px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteDifficultes2" runat="server" CellPadding="0" CellSpacing="0" Width="440px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="I_LabelEnteteDifficultes2" runat="server" Text="Evaluation des résultats" Height="30px" Width="265px"
                                        CssClass="EP_Imp_SmallBold"
                                        Font-Underline="true"
                                        style="padding-top: 8px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Table ID="I_TableNumerobjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="745px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="left">
                                    <asp:Label ID="I_LabelNumerobjectif4" runat="server" Text="Objectif n°4" Height="22px" Width="745px"
                                        CssClass="EP_Imp_MediumNormal"
                                        style="padding-top: 3px; text-indent: 100px;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="300px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="260px" DonHeight="140px" DonTabIndex="7"
                                        Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes4" runat="server" CellPadding="0" CellSpacing="0" Width="440px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="330px" DonHeight="140px" DonTabIndex="8"
                                        Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Table ID="I_TableNumerobjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="745px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="left">
                                    <asp:Label ID="I_LabelNumerobjectif5" runat="server" Text="Objectif n°5" Height="22px" Width="745px"
                                        CssClass="EP_Imp_MediumNormal"
                                        style="padding-top: 3px; text-indent: 100px;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="300px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="260px" DonHeight="140px" DonTabIndex="9"
                                        Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes5" runat="server" CellPadding="0" CellSpacing="0" Width="440px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="330px" DonHeight="140px" DonTabIndex="10"
                                        Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Table ID="I_TableNumerobjectif6" runat="server" CellPadding="0" CellSpacing="0" Width="745px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="left">
                                    <asp:Label ID="I_LabelNumerobjectif6" runat="server" Text="Objectif n°6" Height="22px" Width="745px"
                                        CssClass="EP_Imp_MediumNormal"
                                        style="padding-top: 3px; text-indent: 100px;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif6" runat="server" CellPadding="0" CellSpacing="0" Width="300px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF02" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="260px" DonHeight="140px" DonTabIndex="11"
                                        Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes6" runat="server" CellPadding="0" CellSpacing="0" Width="440px"
                        BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="330px" DonHeight="140px" DonTabIndex="12"
                                        Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>                     
           </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelTitreNumero3" runat="server" Height="20px" Width="748px"
                           Text="2.2 Evènements survenus au cours de la période écoulée ayant entraîné un impact sur l'activité"
                           CssClass="EP_Imp_MediumBold"
                           style="text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="25px" DonTabIndex="13" 
                           Etitext="(nouvelles orientations, réorganisations, nouvelles méthodes, nouveaux outils, etc.)"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align: left; text-indent: 0px; font-size:80%; font-style:normal"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>   
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage3" runat="server" Height="15px" Width="250px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="3 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:right">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>