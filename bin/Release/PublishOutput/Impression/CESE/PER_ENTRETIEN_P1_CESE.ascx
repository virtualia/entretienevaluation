﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P1_CESE.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P1_CESE" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="I_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreTitreResultats" runat="server" Height="25px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="I_LabelVolet" runat="server" Text="BILAN DE L'ANNEE PASSEE" Height="20px" Width="746px"
                            BackColor= "Transparent" BorderStyle="None" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="I_LabelVoletSuite" runat="server" Height="30px" Width="746px"
                            Text="Les trois champs à renseigner sont illustrés par un certain nombre de critères <br/> qui ont pour objet de guider l'évaluation mais l'expression est libre"
                            BackColor= "Transparent" BorderStyle="None" ForeColor="Black"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: oblique; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreEnteteResultats" runat="server" Height="25px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="I_LabelEntete" runat="server" Height="33px" Width="746px"
                            Text="Evaluation des réalisations au regard des objectifs formulés et des contraintes exogènes.<br/>Compétences et aptitudes de l'agent."
                            BackColor= "Transparent" BorderStyle="None" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="I_LabelEnteteSuite1" runat="server" Height="20px" Width="746px"
                            Text="Analyse des résultats et enseignements tirés : ce qui a facilité ou freiné les résultats (difficultés rencontrées par l'agent)."
                            BackColor= "Transparent" BorderStyle="None" ForeColor="Black"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: oblique; text-indent: 2px; text-align: Left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="I_LabelEnteteSuite2" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="OBJECTIFS POUR L'ANNEE ECOULEE"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                           font-style: normal; text-indent: 1px; text-align: center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="130px" EtiHeight="18px" DonTabIndex="1" 
                           Etitext="Bilan global des résultats atteints"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:small; font-style:normal; font-bold:true;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                   <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObjectif" runat="server" CellPadding="0" CellSpacing="0" Width="246px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif1" runat="server" Height="60px" Width="244px"
                                   BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                                   Text="Rappel de l'objectif"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteRealisation" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                                <asp:Label ID="I_LabelEnteteRealisation" runat="server" Height="29px" Width="221px"
                                   BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                                   Text="Réalisation"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteComplete" runat="server" Height="30px" Width="49px"
                                   BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="objectif atteint"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px; 
                                   font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEntetePartielle" runat="server" Height="30px" Width="64px"
                                   BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="partiellement atteint"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px; 
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteInsuffisante" runat="server" Height="30px" Width="49px"
                                   BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="non atteint"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteSansObjet" runat="server" Height="30px" Width="53px"
                                   BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="devenu sans objet"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteDifficultes" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteDifficultes" runat="server" Height="60px" Width="265px"
                                   BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                                   Text="Analyse des résultats et enseignements tirés"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableNumerobjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="I_LabelNumerobjectif1" runat="server" Height="20px" Width="251px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif individuel n°1"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                   style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVA02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="246px" DonHeight="130px" DonTabIndex="2"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHA04" runat="server" V_Groupe="Realisation1"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHA15105" runat="server" Height="25px" Width="170px"
                                    BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                                    Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 5px; margin-left: 8px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes1" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVA03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="270px" DonHeight="130px" DonTabIndex="4"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableNumerobjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="I_LabelNumerobjectif2" runat="server" Height="20px" Width="251px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif individuel n°2"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                   style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="246px" DonHeight="130px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHB04" runat="server" V_Groupe="Realisation2"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHB15105" runat="server" Height="25px" Width="170px"
                                    BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                                    Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 5px; margin-left: 8px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes2" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVB03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="270px" DonHeight="130px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableNumerobjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="I_LabelNumerobjectif3" runat="server" Height="20px" Width="251px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif individuel n°3"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                   style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="246px" DonHeight="130px" DonTabIndex="8"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHC04" runat="server" V_Groupe="Realisation3"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHC15105" runat="server" Height="25px" Width="170px"
                                    BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                                    Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 5px; margin-left: 8px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes3" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="270px" DonHeight="130px" DonTabIndex="10"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableNumerobjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="I_LabelNumerobjectif4" runat="server" Height="20px" Width="251px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif individuel n°4"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                   style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="246px" DonHeight="130px" DonTabIndex="11"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHD04" runat="server" V_Groupe="Realisation4"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHD15105" runat="server" Height="25px" Width="170px"
                                    BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                                    Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 5px; margin-left: 8px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes4" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="270px" DonHeight="130px" DonTabIndex="13"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Label ID="I_LabelSuffixe" runat="server" Height="17px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Transparent" BorderWidth="1px"
                            Text="* Au choix l'objectif sera défini comme étant atteint, partiellement atteint, non atteint, devenu sans objet…"
                            ForeColor="Black" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: oblique; text-indent: 1px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
       <asp:TableFooterRow>
         <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage2" runat="server" Height="15px" Width="250px"
               BackColor="Transparent" BorderStyle="None"
               Text="2 / 12" ForeColor="Black" Font-Italic="True"
               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
               style="text-align:right" >
            </asp:Label>   
         </asp:TableCell>
       </asp:TableFooterRow>
    <asp:TableRow>
       <asp:TableCell>
         <div style="page-break-before:always;"></div>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="I_LabelTitreNumero3" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" 
                           Text="Autres travaux sur lesquels l'agent s'est investi en cours d'année"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 1px; text-align: center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="170px" EtiHeight="30px" DonTabIndex="14" 
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 3px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="150px" EtiHeight="20px" DonTabIndex="15" 
                           Etitext="Analyse des résultats et enseignements tirés"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>    
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage3" runat="server" Height="15px" Width="250px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="3 / 12" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:right">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>