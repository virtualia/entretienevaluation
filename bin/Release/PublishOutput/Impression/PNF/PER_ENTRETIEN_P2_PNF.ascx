﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P2_PNF.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P2_PNF" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="II_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreTitreCompetence" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVolet" runat="server" Text="APPRECIATION DES COMPETENCES DE L'AGENT" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="Transparent" BorderStyle="None"
                            BorderWidth="1px" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            style="margin-top: 1px; margin-left: 1px; margin-bottom: 10px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformationsNiveaux" runat="server" Height="25px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                           Text="Niveaux"
                           BorderWidth="1px" ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformationsNiveau1" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                           Text="Niveau 1 : à développer"
                           BorderWidth="1px" ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformationsNiveau2" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                           Text="Niveau 2 : acquis"
                           BorderWidth="1px" ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformationsNiveau3" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                           Text="Niveau 3 : maîtrise"
                           BorderWidth="1px" ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformationsNiveau4" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                           Text="Niveau 4 : expertise"
                           BorderWidth="1px" ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformationsNiveau5" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                           Text="Niveau 5 : sans objet"
                           BorderWidth="1px" ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelFamille1" runat="server" Height="30px" Width="750px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Compétences professionnelles"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences311" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="II_LabelTitreCompetence1" runat="server" Height="44px" Width="361px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels1" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote11" runat="server" Height="44px" Width="76px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Niveau 1"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote12" runat="server" Height="44px" Width="76px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Niveau 2"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote13" runat="server" Height="44px" Width="76px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Niveau 3"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote14" runat="server" Height="44px" Width="76px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Niveau 4"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote15" runat="server" Height="44px" Width="76px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Niveau 5"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHA15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Connaissances du poste"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHA07" runat="server" V_Groupe="Critere111"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN6visible="false"  
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHB15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Connaissances de l'environnement professionnel"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHB07" runat="server" V_Groupe="Critere112"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN6visible="false"  
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHC15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Qualités rédactionnelles"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHC07" runat="server" V_Groupe="Critere113"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHD15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Qualités relationnelles"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHD07" runat="server" V_Groupe="Critere114"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHE15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Qualités d'expression orale"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHE07" runat="server" V_Groupe="Critere115"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHF15301" runat="server" Height="25px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Capacité d'adaptation aux évolutions techniques et professionnelles"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHF07" runat="server" V_Groupe="Critere116"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelFamille2" runat="server" Height="30px" Width="750px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Agents en situation de management"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences321" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="II_LabelTitreCompetence2" runat="server" Height="44px" Width="361px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote21" runat="server" Height="44px" Width="76px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Niveau 1"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote22" runat="server" Height="44px" Width="76px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Niveau 2"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote23" runat="server" Height="44px" Width="76px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Niveau 3"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote24" runat="server" Height="44px" Width="76px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Niveau 4"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote25" runat="server" Height="44px" Width="76px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Niveau 5"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHG15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Capacité à déléguer"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHG07" runat="server" V_Groupe="Critere117"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN6visible="false"  
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHH15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Capacité à assurer le suivi des dossiers"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHH07" runat="server" V_Groupe="Critere118"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN6visible="false"  
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHI15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Aptitude à la prise de décision"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHI07" runat="server" V_Groupe="Critere119"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHJ15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Sens de l'organisation d'une équipe"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHJ07" runat="server" V_Groupe="Critere120"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage3" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="4 / 14" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>