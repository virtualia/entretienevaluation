﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P0_PNF.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P0_PNF" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="PAGE_0" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="O_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreTitreEntretien" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#124545">
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Table ID="CadreLogoPNF" runat="server" BackImageUrl="~/Images/General/LogoPNF.jpg" Width="166px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true">
                                    <asp:TableRow>
                                        <asp:TableCell Height="75px" BackColor="Transparent" Width="166px"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="O_Etiquette" runat="server" Text="COMPTE RENDU D'ENTRETIEN PROFESSIONNEL" Height="35px" Width="748px"
                                    BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="1px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                    style="margin-top: 1px; margin-left: 1px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                 <asp:Label ID="O_LabelAnneeEntretien" runat="server" Text="" Height="40px" Width="748px"
                                    BackColor="Transparent"  BorderColor="Black" BorderStyle="None"
                                    BorderWidth="2px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                    style="margin-top: 10px; margin-left: 1px; margin-bottom: 10px;
                                    font-style:  normal; text-indent: 2px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDateEntretien" runat="server" Height="20px" Width="514px"
                                    BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px" Text="Date de réalisation de l'entretien :"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15000" runat="server" Height="20px" Width="234px"
                                    BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelPeriodeEntretien" runat="server" Height="20px" Width="514px"
                                    BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px" Text="Période évaluée :"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiPeriodeEntretien" runat="server" Height="20px" Width="234px"
                                    BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreAgent" runat="server" Text="Entre l'agent" Height="20px" Width="350px"
                                    BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="2px" ForeColor="Black"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 10px; margin-left: 1px; margin-bottom: 10px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelIdentite" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Nom et prénom :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelIdentite" runat="server" Height="30px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelStatut" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Statut :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelStatut" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGrade" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Corps / grade :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGrade" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelEchelon" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Echelon :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelEchelon" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDateEchelon" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Date de l'échelon :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelDateEchelon" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelAffectation" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Service :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelAffectation" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelPoste" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Poste occupé :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelPoste" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow><asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDatePoste" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="depuis le :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 200px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelDatePoste" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreManager" runat="server" Text="Et son supérieur hiérarchique" Height="30px" Width="350px"
                                    BackColor="Transparent"  BorderStyle="None"
                                    ForeColor="Black"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 20px; margin-left: 1px; margin-bottom: 10px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelManager" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Nom et prénom :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15001" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>                       
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow> 
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:Table ID="O_CadreEmploi" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="Black" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreTitreEmploi" runat="server" Height="30px" CellPadding="0" Width="750px" 
                            CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVolet" runat="server" Text="DESCRIPTION DU POSTE" Height="20px" Width="746px"
                                        BackColor="Transparent"  BorderStyle="None"
                                        ForeColor="Black"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Table ID="O_CadrePoste" runat="server">
                                      <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="O_LabelTitreNumero1" runat="server" Height="20px" Width="180px"
                                                   BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Intitulé du poste :"
                                                   BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                                                   font-style: normal; text-indent: 1px; text-align:  left; ">
                                                </asp:Label>          
                                            </asp:TableCell> 
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="O_InfoH15006" runat="server" Height="20px" Width="568px"
                                                    BackColor="Transparent" BorderStyle="None" Text=""
                                                    ForeColor="Black" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style:  normal; text-indent: 1px; text-align: left;">
                                             </asp:Label>
                                            </asp:TableCell>
                                      </asp:TableRow>
                                   </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Table ID="O_CadreDatePoste" runat="server">
                                      <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Left">
                                            <asp:Label ID="O_LabelTitreDatePrisePoste1" runat="server" Height="20px" Width="180px"
                                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Date de prise du poste :"
                                                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                                font-style:  normal; text-indent: 1px; text-align: left;">
                                            </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                            <asp:Label ID="O_LabelDatePrisePoste1" runat="server" Height="20px" Width="568px"
                                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                                font-style: normal; text-indent: 1px; text-align: left;">
                                            </asp:Label>          
                                        </asp:TableCell>
                                      </asp:TableRow>
                                   </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow> 
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="15px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Missions et activités principales du poste"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: normal; text-indent: 1px; text-align:  left; ">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV07" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="7" V_SiDonneeDico="false"
                                       EtiWidth="740px" DonWidth="746px" DonHeight="300px" EtiHeight="60px" DonTabIndex="2"
                                       Etitext="" EtiVisible="false"
                                       EtiBorderStyle="None" EtiBackColor="Transparent"
                                       Donstyle="margin-left: 1px;"
                                       Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                       DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>    
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>   
             </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Left" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage1" runat="server" Height="15px" Width="700px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="1 / 14" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:right" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>