﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P3_PNF.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P3_PNF" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="III_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreTitreObjectifs" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelVolet" runat="server" Text="DEFINITION DES OBJECTIFS" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="Transparent" BorderStyle="None"
                            BorderWidth="1px" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            style="margin-top: 1px; margin-left: 1px; margin-bottom: 10px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>   
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="III_LabelNumerobjectif1" runat="server" Height="35px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°1"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteObjectifs1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="III_LabelEnteteObjectif1" runat="server" Height="20px" Width="746px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVB02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="210px" DonTabIndex="1"
                            EtiBorderStyle="None" EtiBackColor="Transparent"
                            Donstyle="margin-left: 1px;"
                            Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>                        
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>     
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteIndicateur1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="III_LabelEnteteIndicateur1" runat="server" Height="20px" Width="746px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Indicateurs de réussite prévus"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVB05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="160px" DonTabIndex="2"
                            Donstyle="margin-left: 1px;"
                            Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>                          
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="III_LabelNumerobjectif2" runat="server" Height="35px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°2"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteObjectifs2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="III_LabelEnteteObjectif2" runat="server" Height="20px" Width="746px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVC02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="210px" DonTabIndex="4"
                            Donstyle="margin-left: 1px;"
                            Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>                        
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>     
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteIndicateur2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="III_LabelEnteteIndicateur2" runat="server" Height="20px" Width="746px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Indicateurs de réussite prévus"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVC05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="160px" DonTabIndex="5"
                            Donstyle="margin-left: 1px;"
                            Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>                          
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="35px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
                        <asp:Label ID="NumeroPage5" runat="server" Height="15px" Width="150px"
                             BackColor="Transparent" BorderStyle="None"
                             Text="5 / 14" ForeColor="Black" Font-Italic="True"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                             style="text-align:left" >
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell >
                         <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="III_LabelNumerobjectif3" runat="server" Height="35px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°3"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteObjectifs3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="III_LabelEnteteObjectif3" runat="server" Height="20px" Width="746px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVD02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="210px" DonTabIndex="7"
                            Donstyle="margin-left: 1px;"
                            Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>                        
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>     
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteIndicateur3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="III_LabelEnteteIndicateur3" runat="server" Height="20px" Width="746px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Indicateurs de réussite prévus"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVD05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="160px" DonTabIndex="8"
                            Donstyle="margin-left: 1px;"
                            Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>                          
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="III_LabelNumerobjectif4" runat="server" Height="35px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°4"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteObjectifs4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="III_LabelEnteteObjectif4" runat="server" Height="20px" Width="746px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVE02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="210px" DonTabIndex="7"
                            Donstyle="margin-left: 1px;"
                            Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>                        
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>     
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteIndicateur4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="III_LabelEnteteIndicateur4" runat="server" Height="20px" Width="746px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Indicateurs de réussite prévus"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVE05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="160px" DonTabIndex="8"
                            Donstyle="margin-left: 1px;"
                            Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>                          
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage6" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="6 / 14" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableRow>
 </asp:Table>