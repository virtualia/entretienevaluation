﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P1_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P1_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="I_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreTitreResultats" runat="server" Height="50px" CellPadding="0" Width="750px" BackColor="LightGray" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVoletEntete" runat="server" Text="I - " Height="20px" Width="30px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVolet" runat="server" Text="BILAN DES OBJECTIFS FIXES POUR L'ANNEE ECOULEE" Height="20px" Width="500px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVoletSuite" runat="server" Text="" Height="20px" Width="216px"
                            CssClass="EP_Imp_SmallItalic" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="I_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObjectif" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteObjectif11" runat="server" Height="90px" Width="241px"
                                   Text="Rappel des objectifs assignés à l'agent"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteRealisation" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                                <asp:Label ID="I_LabelEnteteObjectif12" runat="server" Height="59px" Width="220px"
                                   Text="Degré de réalisation de l'objectif"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteComplete" runat="server" Height="30px" Width="49px"
                                   Text="objectif atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEntetePartielle" runat="server" Height="30px" Width="64px"
                                   Text="partiellement atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteInsuffisante" runat="server" Height="30px" Width="49px"
                                   Text="non atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteSansObjet" runat="server" Height="30px" Width="53px"
                                   Text="devenu sans objet"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObservations" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteObservations" runat="server" Height="90px" Width="265px"
                                   Text="<B>Observations :</B> <br/> (Difficultés rencontrées, nouvelles orientations, réorganisations, nouveaux outils, missions nouvelles venant expliquer un décalage par rapport aux objectifs fixés)"
                                   CssClass="EP_Imp_SmallNormal" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVA02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="False"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="1"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif n°1" Etiwidth="245px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVA03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="False"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="2"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Contexte et moyens mis en oeuvre pour y parvenir" Etiwidth="245px" EtiHeight="30px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black" EtiBorderWidth="1px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHA04" runat="server" V_Groupe="Realisation1"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="False"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHA15105" runat="server" Height="20px" Width="170px" Text=""
                                    CssClass="EP_Imp_MediumNormal"
                                    style="margin-top: 0px; margin-left: 8px; margin-bottom: 0px; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes1" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVA06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="False"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="4"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="De l'agent :" Etiwidth="269px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVA07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="False"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Du supérieur hiérarchique direct :" Etiwidth="269px" EtiHeight="30px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" Width="746px" BackColor="Transparent" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="1px" Width="746px" BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" ColumnSpan="3"></asp:TableCell>            
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" Width="746px" BackColor="Transparent" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="False"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="6"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif n°2" Etiwidth="245px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVB03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="False"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Contexte et moyens mis en oeuvre pour y parvenir" Etiwidth="245px" EtiHeight="30px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHB04" runat="server" V_Groupe="Realisation2"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="False"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHB15105" runat="server" Height="20px" Width="170px" Text=""
                                    CssClass="EP_Imp_MediumNormal"
                                    style="margin-top: 0px; margin-left: 8px; margin-bottom: 0px; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes2" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVB06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="False"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="9"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="De l'agent :" Etiwidth="269px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVB07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="False"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="10"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Du supérieur hiérarchique direct :" Etiwidth="269px" EtiHeight="30px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" Width="746px" BackColor="Transparent" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="1px" Width="746px" BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" ColumnSpan="3"></asp:TableCell>            
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" Width="746px" BackColor="Transparent" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="I_SautPage" runat="server">
                            <asp:TableFooterRow>
                                <asp:TableCell VerticalAlign="Bottom">
                                    <asp:Table ID="I_CadreBasDePage2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="I_LabelBasdePageNom2" runat="server" Height="25px" Width="150px" Text=""
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                                </asp:Label>          
                                            </asp:TableCell> 
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="I_LabelBasdePagePrenom2" runat="server" Height="25px" Width="150px" Text=""
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="I_LabelBasdePageCorps2" runat="server" Height="25px" Width="350px" Text=""
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="I_LabelBasdePageAnnee2" runat="server" Height="25px" Width="30px" Text=""
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="NumeroPage2" runat="server" Height="25px" Width="50px"
                                                    Text="p.2/16" 
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                                                </asp:Label>   
                                            </asp:TableCell>
                                        </asp:TableRow>   
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableFooterRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <div style="page-break-before:always;"></div>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteObjectif21" runat="server" Height="90px" Width="241px"
                                   Text="Rappel des objectifs assignés à l'agent"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                                <asp:Label ID="I_LabelEnteteObjectif22" runat="server" Height="59px" Width="220px"
                                   Text="Degré de réalisation de l'objectif"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteComplete2" runat="server" Height="30px" Width="49px"
                                   Text="objectif atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEntetePartielle2" runat="server" Height="30px" Width="64px"
                                   Text="partiellement atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteInsuffisante2" runat="server" Height="30px" Width="49px"
                                   Text="non atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteSansObjet2" runat="server" Height="30px" Width="53px"
                                   Text="devenu sans objet"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObservations2" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteObservations2" runat="server" Height="90px" Width="265px"
                                   Text="<B>Observations :</B> <br/> (Difficultés rencontrées, nouvelles orientations, réorganisations, nouveaux outils, missions nouvelles venant expliquer un décalage par rapport aux objectifs fixés)"
                                   CssClass="EP_Imp_SmallNormal" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="False"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="11"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif N°3" Etiwidth="245px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="False"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="12"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Contexte et moyens mis en oeuvre pour y parvenir" Etiwidth="245px" EtiHeight="30px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHC04" runat="server" V_Groupe="Realisation3"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="False"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHC15105" runat="server" Height="20px" Width="170px" Text=""
                                    CssClass="EP_Imp_MediumNormal"
                                    style="margin-top: 0px; margin-left: 8px; margin-bottom: 0px; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes3" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="False"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="14"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="De l'agent :" Etiwidth="269px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="False"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="15"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Du supérieur hiérarchique direct :" Etiwidth="269px" EtiHeight="30px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" Width="746px" BackColor="Transparent" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="1px" Width="746px" BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" ColumnSpan="3"></asp:TableCell>            
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" Width="746px" BackColor="Transparent" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="False"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="16"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif n°4" Etiwidth="245px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="False"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="17"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Contexte et moyens mis en oeuvre pour y parvenir" Etiwidth="245px" EtiHeight="30px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHD04" runat="server" V_Groupe="Realisation4"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="False"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHD15105" runat="server" Height="20px" Width="170px" Text=""
                                    CssClass="EP_Imp_MediumNormal"
                                    style="margin-top: 0px; margin-left: 8px; margin-bottom: 0px; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes4" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="False"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="19"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="De l'agent :" Etiwidth="269px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="False"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="20"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Du supérieur hiérarchique direct :" Etiwidth="269px" EtiHeight="30px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" Width="746px" BackColor="Transparent" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="1px" Width="746px" BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" ColumnSpan="3"></asp:TableCell>            
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" Width="746px" BackColor="Transparent" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="I_SautPage2" runat="server">
                            <asp:TableFooterRow>
                                <asp:TableCell VerticalAlign="Bottom">
                                    <asp:Table ID="I_CadreBasDePage3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="I_LabelBasdePageNom3" runat="server" Height="25px" Width="150px" Text=""
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                                </asp:Label>          
                                            </asp:TableCell> 
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="I_LabelBasdePagePrenom3" runat="server" Height="25px" Width="150px" Text=""
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="I_LabelBasdePageCorps3" runat="server" Height="25px" Width="350px" Text=""
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="I_LabelBasdePageAnnee3" runat="server" Height="25px" Width="30px" Text=""
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="NumeroPage3" runat="server" Height="25px" Width="50px"
                                                    Text="p.3/16" 
                                                    CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                                                </asp:Label>   
                                            </asp:TableCell>
                                        </asp:TableRow>   
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableFooterRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <div style="page-break-before:always;"></div>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteObjectif31" runat="server" Height="90px" Width="241px"
                                   Text="Rappel des objectifs assignés à l'agent"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                                <asp:Label ID="I_LabelEnteteObjectif32" runat="server" Height="59px" Width="220px"
                                   Text="Degré de réalisation de l'objectif"
                                   CssClass="EP_Imp_SmallBold" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteComplete3" runat="server" Height="30px" Width="49px"
                                   Text="objectif atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEntetePartielle3" runat="server" Height="30px" Width="64px"
                                   Text="partiellement atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteInsuffisante3" runat="server" Height="30px" Width="49px"
                                   Text="non atteint"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteSansObjet3" runat="server" Height="30px" Width="53px"
                                   Text="devenu sans objet"
                                   CssClass="EP_Imp_SmallNormal"
                                   BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableEnteteObservations3" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="I_LabelEnteteObservations3" runat="server" Height="90px" Width="265px"
                                   Text="<B>Observations :</B> <br/> (Difficultés rencontrées, nouvelles orientations, réorganisations, nouveaux outils, missions nouvelles venant expliquer un décalage par rapport aux objectifs fixés)"
                                   CssClass="EP_Imp_SmallNormal" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="False"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="21"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif n°5" Etiwidth="245px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="False"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="22"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Contexte et moyens mis en oeuvre pour y parvenir" Etiwidth="245px" EtiHeight="30px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableRealisation5" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="I_RadioHE04" runat="server" V_Groupe="Realisation5"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="False"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="I_InfoHE15105" runat="server" Height="20px" Width="170px" Text=""
                                    CssClass="EP_Imp_MediumNormal"
                                    style="margin-top: 0px; margin-left: 8px; margin-bottom: 0px; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="I_TableDifficultes5" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="False"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="24"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="De l'agent :" Etiwidth="269px" EtiHeight="18px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="False"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="25"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Du supérieur hiérarchique direct :" Etiwidth="269px" EtiHeight="30px"
                                   EtiBackColor="Transparent" EtiBorderColor="Black" DonBorderColor="Black"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black" BackColor="LightGray">
                        <asp:Label ID="I_LabelTitreNumero2" runat="server" Height="40px" Width="748px"
                           Text="SYNTHESE GENERALE DU BILAN DES OBJECTIFS"
                           CssClass="EP_Imp_LargerBold" 
                           style="margin-bottom: 3px; padding-top: 7px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="False"
                           EtiWidth="744px" DonWidth="740px" DonHeight="150px" EtiHeight="30px" DonTabIndex="26" 
                           Etitext="- du supérieur hiérarchique direct :"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 4px;"
                           EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px" 
                           DonBorderColor="Black" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="False"
                           EtiWidth="744px" DonWidth="740px" DonHeight="150px" EtiHeight="30px" DonTabIndex="27" 
                           Etitext="- de l'agent :"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 4px;"
                           EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderWidth="1px"
                           DonBorderColor="Black" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="I_CadreBasDePage4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelBasdePageNom4" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelBasdePagePrenom4" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelBasdePageCorps4" runat="server" Height="25px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelBasdePageAnnee4" runat="server" Height="25px" Width="30px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage4" runat="server" Height="25px" Width="50px"
                            Text="p.4/16" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>