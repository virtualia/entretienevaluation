﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_FICHE_POSTE_P0_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_FICHE_POSTE_P0_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<asp:Table ID="PAGE_0" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="O_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreTitreEntretien" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="0px" Visible="true" BorderColor="Black">
                        <asp:TableRow>
                            <asp:TableCell Height="3px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" Width="372px">
                                <asp:Table ID="CadreLogoENM" runat="server" BackImageUrl="~/Images/General/Logo_ENM.jpg"  Width="169px"  
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true">
                                    <asp:TableRow Width="169px">
                                        <asp:TableCell Height="81px" BackColor="Transparent" Width="169px" ></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                             <asp:TableCell HorizontalAlign="left" Width="0px"  >
                                  <asp:Label ID="Label1" runat="server"  Width="0px" >
                                      </asp:Label>    
                                 </asp:TableCell>
                              <asp:TableCell HorizontalAlign="left" >
                                 <asp:Label ID="LabelSousFamille" runat="server"  Width="378px"   BorderWidth="1px" BorderColor="Black" 
                                    Text="<br/> Sous-famille " 
                                    CssClass="EP_Imp_SmallItalic" style=" Width:10cm; text-align: center;  vertical-align: middle;
                                     font-size: 70%; font-size:13.0pt;color:navy;background:#BFBFBF; height:63.7pt"
                                     DonBorderWidth="1px" DonBorderColor="Black" >
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                         <asp:TableRow >
                             <asp:TableCell ColumnSpan="2"  HorizontalAlign="left" Width="372px"  >
                                  <asp:Label ID="LabelIntituleRefEmploi" runat="server"  Width="372px"  BorderWidth="1px" BorderColor="Black" 
                                    Text="Intitulé de l'emploi <br/> Référence de l'emploi "
                                    CssClass="EP_Imp_SmallItalic" style=" Width:10cm; vertical-align: middle; 
                                     font-size: 70%; font-size:10.0pt;color:navy;background:#BFBFBF; height:63.7pt"
                                      DonBorderWidth="1px" DonBorderColor="Black">
                                 </asp:Label>   
                                 </asp:TableCell>
                              <asp:TableCell HorizontalAlign="left" >
                                 <asp:Label ID="LabelIdentite" runat="server"  Width="378px"  BorderWidth="1px" BorderColor="Black" 
                                    Text="FICHE DE POSTE DE « Nom et prénom de l’agent » <br/>  N° de poste : « numéro de poste » du poste fonctionnel <br/> « poste fonctionnel » (figurant dans affectation)"
                                    CssClass="EP_Imp_SmallItalic" style=" Width:10cm; vertical-align: middle; 
                                      font-size: 70%; font-size:10.0pt;color:navy;background:#BFBFBF; height:63.7pt"
                                     DonBorderWidth="1px" DonBorderColor="Black">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
         <asp:TableRow >
            <asp:TableCell ColumnSpan="3"  Width="750px" >
            <asp:Table ID="Table1" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true" 
                BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow> 
                    
                                 <asp:TableCell  HorizontalAlign="left" Width="160px"  >
                                  <asp:Label ID="LabelEti_RIME" runat="server"  Width="160px"  
                                    Text="CORRESPONDANCE RIME : "
                                    CssClass="EP_Imp_SmallItalic" style=" Width:160px; vertical-align: middle;
                                       font-size: 70%; font-size:10.0pt;height:29.4pt">
                                 </asp:Label>   
                                 </asp:TableCell>                 
                             <asp:TableCell  HorizontalAlign="left" Width="205px"  >
                                  <asp:Label ID="I_InfoHC15704" runat="server"  Width="205px"  
                                    CssClass="EP_Imp_SmallItalic" style=" Width:205px; vertical-align: middle;
                                       font-size: 70%; font-size:10.0pt;height:29.4pt">
                                 </asp:Label>   
                                 </asp:TableCell>
                                  <asp:TableCell HorizontalAlign="left" >
                                 <asp:Label ID="LabelEti_Cotation" runat="server"  Width="160px" 
                                    Text="COTATION DU POSTE IFSE: "
                                    CssClass="EP_Imp_SmallItalic" style=" Width:160px; vertical-align: middle;
                                   font-size: 70%; font-size:10.0pt; height:29.4pt">
                                 </asp:Label>          
                            </asp:TableCell>
                              <asp:TableCell HorizontalAlign="left" >
                                 <asp:Label ID="I_InfoHD15704" runat="server"  Width="205px"
                                    CssClass="EP_Imp_SmallItalic" style=" Width:205px; vertical-align: middle;
                                   font-size: 70%; font-size:10.0pt; height:29.4pt">
                                 </asp:Label>          
                            </asp:TableCell>
                  </asp:TableRow>
             </asp:Table>
            </asp:TableCell>
         </asp:TableRow>

                        <asp:TableRow >
                             <asp:TableCell ColumnSpan="3"  HorizontalAlign="left" Width="750px"  >
                                  <asp:Label ID="LabelServiceAffectation" runat="server"  Width="750px"  HorizontalAlign="center"  BorderWidth="1px" BorderColor="Black" 
                                    Text="SERVICE D’AFFECTATION : "
                                    CssClass="EP_Imp_SmallItalic" style="Width:20cm; vertical-align: middle; 
                                     font-size: 70%; font-size:10.0pt; background:#BFBFBF; height:22.95pt ; text-align : left">
                                 </asp:Label>  
                                 </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="10px"></asp:TableCell>
                        </asp:TableRow>
                         <asp:TableRow >
                                 <asp:TableCell ColumnSpan="3"  HorizontalAlign="left" Width="758px" >
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoV_00" runat="server" DonTextMode="true" 
                                      V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="758px" DonHeight="60px" DonTabIndex="1"
                                       Etitext="DEFINITION SYNTHETIQUE" EtiVisible="true" 
		                               Donstyle="margin-left: 1px; font-size:9.0pt"
                                        EtiWidth="754px" EtiBorderColor ="Black"
		                               Etistyle="text-align: center; text-indent: 1px; font-size:small; font-weight: bold; EtiForeColor:#BFBFBF"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                        </asp:TableRow>

                         <asp:TableRow>
                            <asp:TableCell Height="15px"></asp:TableCell>
                        </asp:TableRow>

                         <asp:TableRow >
                                 <asp:TableCell ColumnSpan="3"  HorizontalAlign="left" Width="758px" >
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVB15403" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="758px" DonHeight="60px" DonTabIndex="2"
                                       Etitext="ACTIVITES PRINCIPALES" EtiVisible="true" 
		                               Donstyle="margin-left: 1px; font-size:9.0pt"
                                        EtiWidth="754px" EtiBorderColor ="Black"
		                               Etistyle="text-align: center; text-indent: 1px; font-size:small; font-weight: bold; EtiForeColor:#BFBFBF"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                        </asp:TableRow>
                         <asp:TableRow>
                            <asp:TableCell Height="15px"></asp:TableCell>
                        </asp:TableRow>
                         <asp:TableRow >
                               <asp:TableCell ColumnSpan="3"  HorizontalAlign="left" Width="758px" >
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC15403" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="758px" DonHeight="60px" DonTabIndex="2"
                                       Etitext="COMPETENCES" EtiVisible="true" 
		                               Donstyle="margin-left: 1px; font-size:9.0pt"
                                        EtiWidth="754px" EtiBorderColor ="Black"
		                               Etistyle="text-align: center; text-indent: 1px; font-size:small; font-weight: bold;  EtiForeColor:#BFBFBF"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="O_CadreBasDePage1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage1" runat="server" Height="18px" Width="730px"
                            Text="Page 1/2" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>