﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P2_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P2_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

 <asp:Table ID="II_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">

    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="II_CadreTitreEvaluation" runat="server" Height="50px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black" BackColor="LightGray" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVoletEntete" runat="server" Text="II - " Height="20px" Width="30px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVolet" runat="server" Text="FICHE D'EVALUATION" Height="20px" Width="500px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVoletSuite" runat="server" Text="" Height="20px" Width="216px"
                            CssClass="EP_Imp_SmallItalic" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="II_CadreTitreEvaluationSuite" runat="server" Height="60px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVoletInformations" runat="server" Height="40px" Width="746px"
                            Text="La qualification du niveau global de l'agent tel qu'il apparaît dans la grille des critères d'appréciation et d'évaluation doit être en parfaite harmonie avec l'appréciation littérale de la valeur professionnelle de l'agent."
                            CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVoletGrille" runat="server" Height="20px" Width="746px"
                            Text="GRILLE D'EVALUATION : (Pour chacun des thèmes, cocher le niveau estimé et la marge d'évolution)"
                            CssClass="EP_Imp_SmallItalic" style="text-align: center; text-decoration:underline">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>  
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences2" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                            BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                            <asp:TableRow>
                              <asp:TableCell>
                                   <asp:Label ID="II_LabelCadrage2" runat="server" Height="14px" Width="445px" Text="Compétences professionnelles"
                                       CssClass="EP_Imp_SmallBold" style="text-align: center; padding-top: 3px;">
                                   </asp:Label>  
                              </asp:TableCell>
                              <asp:TableCell BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                   <asp:Label ID="II_LabelTitreNiveau2" runat="server" Height="14px" Width="287px" Text="Niveau de performance"
                                       CssClass="EP_Imp_SmallNormal"
                                       style="margin-left: 3px; margin-top: 2px; text-align: center; padding-top: 3px; font-size:70%;">
                                   </asp:Label>  
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="II_LabelTitreCompetence2" runat="server" Height="24px" Width="442px" Text="(Grille à compléter en intégralité)"
                                       CssClass="EP_Imp_SmallNormal"
                                       style="margin-top: 1px; text-align: center; font-size:70%; padding-top: 4px;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote21" runat="server" Height="29px" Width="56px" Text="Excellent" Tooltip="Excellent"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote22" runat="server" Height="29px" Width="42px" Text="Très bon" Tooltip="Très bon"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote23" runat="server" Height="29px" Width="35px" Text="Bon" Tooltip="Bon"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote24" runat="server" Height="29px" Width="40px" Text="Moyen" Tooltip="Moyen"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote25" runat="server" Height="29px" Width="60px" Text="Insuffisant" Tooltip="Insuffisant"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote26" runat="server" Height="29px" Width="60px" Text="Très insuffisant" Tooltip="Très insuffisant"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label> 
                                        </asp:TableCell>          
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVA01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="1" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHA07" runat="server" V_Groupe="CritereAgent1"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVB01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="2" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHB07" runat="server" V_Groupe="CritereAgent2"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                            <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVC01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="37px" DonBorderWidth="1px" DonTabIndex="3" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHC07" runat="server" V_Groupe="CritereAgent3"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="35px" VRadioN2Height="35px" VRadioN3Height="35px"
                                   VRadioN4Height="35px" VRadioN5Height="35px" VRadioN6Height="35px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVD01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="4" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHD07" runat="server" V_Groupe="CritereAgent4"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVE01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="23px" DonBorderWidth="1px" DonTabIndex="5" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHE07" runat="server" V_Groupe="CritereAgent5"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVF01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="23px" DonBorderWidth="1px" DonTabIndex="6" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHF07" runat="server" V_Groupe="CritereAgent6"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreMarge2" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                                <asp:TableCell Width="200px"></asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BackColor="LightGray" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black">
                                    <asp:Label ID="II_LabelTitreMarge2" runat="server" Height="34px" Width="150px" Text="Marge d'évolution"
                                    CssClass="EP_Imp_SmallNormal"
                                    style="padding-top: 6px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell Width="50px"></asp:TableCell>
                                <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHA03" runat="server" V_Groupe="CritereMarge1"
                                    V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                    VRadioN1Width="120px" VRadioN2Width="120px" VRadioN3Width="120px"
                                    VRadioN4Width="5px" VRadioN5Width="5px" VRadioN6Width="5px"
                                    VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                    VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                    VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                    VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                    VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                    VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"
                                    VRadioN1Text="En progrès" VRadioN2Text="Constant" VRadioN3Text="A améliorer"
                                    VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                    VRadioN4Visible="False" VRadioN5Visible="False" VRadioN6Visible="False"
                                    Visible="true"/>
                                </asp:TableCell>
                                <asp:TableCell Width="200px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="40px"></asp:TableCell>
                </asp:TableRow>       
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences3" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                            BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                            <asp:TableRow>
                              <asp:TableCell>
                                   <asp:Label ID="II_LabelCadrage3" runat="server" Height="14px" Width="445px" Text="Aptitudes professionnelles et efficacité dans l'emploi"
                                       CssClass="EP_Imp_SmallBold" style="text-align: center; padding-top: 3px;">
                                   </asp:Label>  
                              </asp:TableCell>
                              <asp:TableCell BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                   <asp:Label ID="II_LabelTitreNiveau3" runat="server" Height="14px" Width="287px" Text="Niveau de performance"
                                       CssClass="EP_Imp_SmallNormal"
                                       style="margin-left: 3px; margin-top: 2px; text-align: center; padding-top: 3px; font-size:70%;">
                                   </asp:Label>  
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="II_LabelTitreCompetence3" runat="server" Height="24px" Width="442px" Text="(Grille à compléter en intégralité)"
                                       CssClass="EP_Imp_SmallNormal"
                                       style="margin-top: 1px; text-align: center; font-size:70%; padding-top: 4px;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels3" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote31" runat="server" Height="29px" Width="56px" Text="Excellent" Tooltip="Excellent"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote32" runat="server" Height="29px" Width="42px" Text="Très bon" Tooltip="Très bon"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote33" runat="server" Height="29px" Width="35px" Text="Bon" Tooltip="Bon"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote34" runat="server" Height="29px" Width="40px" Text="Moyen" Tooltip="Moyen"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote35" runat="server" Height="29px" Width="60px" Text="Insuffisant" Tooltip="Insuffisant"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote36" runat="server" Height="29px" Width="60px" Text="Très insuffisant" Tooltip="Très insuffisant"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label> 
                                        </asp:TableCell>          
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVI01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="7" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHI07" runat="server" V_Groupe="CritereAgent7"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVJ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="8" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHJ07" runat="server" V_Groupe="CritereAgent8"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                            <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVK01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="9" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHK07" runat="server" V_Groupe="CritereAgent9"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVL01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="10" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHL07" runat="server" V_Groupe="CritereAgent10"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVM01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="11" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHM07" runat="server" V_Groupe="CritereAgent11"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVN01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="12" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHN07" runat="server" V_Groupe="CritereAgent12"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreMarge3" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                                <asp:TableCell Width="200px"></asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BackColor="LightGray" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black">
                                    <asp:Label ID="II_LabelTitreMarge3" runat="server" Height="34px" Width="150px" Text="Marge d'évolution"
                                      CssClass ="EP_Imp_SmallNormal"
                                      style="padding-top: 6px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell Width="50px"></asp:TableCell>
                                <asp:TableCell  HorizontalAlign="Left" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHB03" runat="server" V_Groupe="CritereMarge2"
                                    V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                    VRadioN1Width="120px" VRadioN2Width="120px" VRadioN3Width="120px"
                                    VRadioN4Width="5px" VRadioN5Width="5px" VRadioN6Width="5px"
                                    VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                    VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                    VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                    VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                    VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                    VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                    VRadioN1Text="En progrès" VRadioN2Text="Constant" VRadioN3Text="A améliorer"
                                    VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                    VRadioN4Visible="False" VRadioN5Visible="False" VRadioN6Visible="False"
                                    Visible="true"/>
                                </asp:TableCell>
                                <asp:TableCell Width="200px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>       
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="II_SautPage" runat="server">
                <asp:TableFooterRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="II_CadreBasDePage5" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="II_LabelBasdePageNom5" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell> 
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="II_LabelBasdePagePrenom5" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="II_LabelBasdePageCorps5" runat="server" Height="25px" Width="350px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="II_LabelBasdePageAnnee5" runat="server" Height="25px" Width="30px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="NumeroPage5" runat="server" Height="25px" Width="50px"
                                        Text="p.5/16" 
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                                    </asp:Label>   
                                </asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences4" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
	                    BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                            <asp:TableRow>
                              <asp:TableCell>
                                   <asp:Label ID="II_LabelCadrage4" runat="server" Height="14px" Width="445px" Text="Qualités et capacités relationnelles"
                                       CssClass="EP_Imp_SmallBold" style="text-align: center; padding-top: 3px;">
                                   </asp:Label>  
                              </asp:TableCell>
                              <asp:TableCell BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                   <asp:Label ID="II_LabelTitreNiveau4" runat="server" Height="14px" Width="287px" Text="Niveau de performance"
                                       CssClass="EP_Imp_SmallNormal"
                                       style="margin-left: 3px; margin-top: 2px; text-align: center; padding-top: 3px; font-size:70%;">
                                   </asp:Label>  
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="II_LabelTitreCompetence4" runat="server" Height="24px" Width="442px" Text="(Grille à compléter en intégralité)"
                                       CssClass="EP_Imp_SmallNormal"
                                       style="margin-top: 1px; text-align: center; font-size:70%; padding-top: 4px;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels4" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote41" runat="server" Height="29px" Width="56px" Text="Excellent" Tooltip="Excellent"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote42" runat="server" Height="29px" Width="42px" Text="Très bon" Tooltip="Très bon"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote43" runat="server" Height="29px" Width="35px" Text="Bon" Tooltip="Bon"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote44" runat="server" Height="29px" Width="40px" Text="Moyen" Tooltip="Moyen"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote45" runat="server" Height="29px" Width="60px" Text="Insuffisant" Tooltip="Insuffisant"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote46" runat="server" Height="29px" Width="60px" Text="Très insuffisant" Tooltip="Très insuffisant"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label> 
                                        </asp:TableCell>          
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVQ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="13" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHQ07" runat="server" V_Groupe="CritereAgent13"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVR01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="14" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHR07" runat="server" V_Groupe="CritereAgent14"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                    <asp:Table ID="II_CadreMarge4" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                        <asp:TableRow>
		                    <asp:TableCell Width="200px"></asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="LightGray" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black">
                                <asp:Label ID="II_LabelTitreMarge4" runat="server" Height="34px" Width="150px" Text="Marge d'évolution"
                                CssClass="EP_Imp_SmallNormal"
                                style="padding-top: 6px; text-align: center;">
                                </asp:Label>          
                            </asp:TableCell>
		                    <asp:TableCell Width="50px"></asp:TableCell>
                            <asp:TableCell  HorizontalAlign="Left" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                            <Virtualia:VSixBoutonRadio ID="II_RadioHC03" runat="server" V_Groupe="CritereMarge3"
                                V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                VRadioN1Width="120px" VRadioN2Width="120px" VRadioN3Width="120px"
                                VRadioN4Width="5px" VRadioN5Width="5px" VRadioN6Width="5px"
                                VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                VRadioN1Text="En progrès" VRadioN2Text="Constant" VRadioN3Text="A améliorer"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="False" VRadioN5Visible="False" VRadioN6Visible="False"
                                Visible="true"/>
                            </asp:TableCell>
		                    <asp:TableCell Width="200px"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="40px"></asp:TableCell>
                </asp:TableRow>       
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences5" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
	                        BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                            <asp:TableRow>
                              <asp:TableCell>
                                   <asp:Label ID="II_LabelCadrage5" runat="server" Height="14px" Width="445px" Text="Capacités d'encadrement"
                                       CssClass="EP_Imp_SmallBold" style="text-align: center; padding-top: 3px;">
                                   </asp:Label>  
                              </asp:TableCell>
                              <asp:TableCell BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                   <asp:Label ID="II_LabelTitreNiveau5" runat="server" Height="14px" Width="287px" Text="Niveau de performance"
                                       CssClass="EP_Imp_SmallNormal"
                                       style="margin-left: 3px; margin-top: 2px; text-align: center; padding-top: 3px; font-size:70%;">
                                   </asp:Label>  
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="II_LabelTitreCompetence5" runat="server" Height="24px" Width="442px" Text="(à compléter uniquement lorsque l'agent évalué exerce des fonctions d'encadrement)"
                                       CssClass="EP_Imp_SmallNormal"
                                       style="margin-top: 1px; text-align: center; font-size:70%; padding-top: 4px;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels5" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote51" runat="server" Height="29px" Width="56px" Text="Excellent" Tooltip="Excellent"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote52" runat="server" Height="29px" Width="42px" Text="Très bon" Tooltip="Très bon"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote53" runat="server" Height="29px" Width="35px" Text="Bon" Tooltip="Bon"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote54" runat="server" Height="29px" Width="40px" Text="Moyen" Tooltip="Moyen"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote55" runat="server" Height="29px" Width="60px" Text="Insuffisant" Tooltip="Insuffisant"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">
                                           <asp:Label ID="II_LabelTitreNote56" runat="server" Height="29px" Width="60px" Text="Très insuffisant" Tooltip="Très insuffisant"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label> 
                                        </asp:TableCell>          
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVT01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="15" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHT07" runat="server" V_Groupe="CritereAgent15"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVU01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="16" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHU07" runat="server" V_Groupe="CritereAgent16"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                            <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVV01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="17" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHV07" runat="server" V_Groupe="CritereAgent17"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVW01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="18" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHW07" runat="server" V_Groupe="CritereAgent18"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVX01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="19" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHX07" runat="server" V_Groupe="CritereAgent19"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVY01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="442px" DonHeight="22px" DonBorderWidth="1px" DonTabIndex="20" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHY07" runat="server" V_Groupe="CritereAgent20"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                   VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                    <asp:Table ID="II_CadreMarge5" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                        <asp:TableRow>
                            <asp:TableCell Width="200px"></asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="LightGray" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black">
                                <asp:Label ID="II_LabelTitreMarge5" runat="server" Height="34px" Width="150px" Text="Marge d'évolution"
                                CssClass="EP_Imp_SmallNormal"
                                style="padding-top: 6px; text-align: center;">
                                </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell Width="50px"></asp:TableCell>
                            <asp:TableCell  HorizontalAlign="Left" VerticalAlign="Top" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black">        
                            <Virtualia:VSixBoutonRadio ID="II_RadioHD03" runat="server" V_Groupe="CritereMarge4"
                                V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                VRadioN1Width="120px" VRadioN2Width="120px" VRadioN3Width="120px"
                                VRadioN4Width="5px" VRadioN5Width="5px" VRadioN6Width="5px"
                                VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                VRadioN1Text="En progrès" VRadioN2Text="Constant" VRadioN3Text="A améliorer"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="False" VRadioN5Visible="False" VRadioN6Visible="False"
                                Visible="true"/>
                            </asp:TableCell>
                            <asp:TableCell Width="200px"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="40px"></asp:TableCell>
                </asp:TableRow>       
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero7" runat="server" CellPadding="0" CellSpacing="0" Width="550px"
                HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelFamille6" runat="server" Height="38px" Width="450px" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                           Text="MARGE D'EVOLUTION GLOBALE"
                           CssClass="EP_Imp_MediumNormal"
                           style="margin-left: -2px; padding-top: 7px; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences6" runat="server" CellPadding="0" CellSpacing="0" Width="350px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <asp:Table ID="II_CadreMarge6" runat="server" CellPadding="0" CellSpacing="0" Width="320px">
                                    <asp:TableRow>
                                      <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                        <Virtualia:VSixBoutonRadio ID="II_RadioHE03" runat="server" V_Groupe="CritereMarge5"
                                           V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                           VRadioN1Width="100px" VRadioN2Width="100px" VRadioN3Width="100px"
                                           VRadioN4Width="5px" VRadioN5Width="5px" VRadioN6Width="5px"
                                           VRadioN1Height="30px" VRadioN2Height="30px" VRadioN3Height="30px"
                                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                           VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                           VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                           VRadioN1Text="En progrès" VRadioN2Text="Constant" VRadioN3Text="A améliorer"
                                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                           VRadioN4Visible="False" VRadioN5Visible="False" VRadioN6Visible="False"
                                           Visible="true"/>
                                      </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                              </asp:TableCell> 
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelFamille7" runat="server" Height="38px" Width="450px" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px"
                           Text="NIVEAU GLOBAL DE PERFORMANCE"
                           CssClass="EP_Imp_MediumNormal"
                           style="margin-left: -2px; padding-top: 7px; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences7" runat="server" CellPadding="0" CellSpacing="0" Width="400px">
                            <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" Visible="False">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVZ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="242px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="21" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels7" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="LightGray">
                                           <asp:Label ID="II_LabelTitreNote71" runat="server" Height="29px" Width="60px" Text="Excellent"
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="LightGray">
                                           <asp:Label ID="II_LabelTitreNote72" runat="server" Height="29px" Width="60px" Text="Très bon" 
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="LightGray">
                                           <asp:Label ID="II_LabelTitreNote73" runat="server" Height="29px" Width="60px" Text="Bon" 
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="LightGray">
                                           <asp:Label ID="II_LabelTitreNote74" runat="server" Height="29px" Width="60px" Text="Moyen" 
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="LightGray">
                                           <asp:Label ID="II_LabelTitreNote75" runat="server" Height="29px" Width="60px" Text="Insuffisant" 
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="LightGray">
                                           <asp:Label ID="II_LabelTitreNote76" runat="server" Height="29px" Width="60px" Text="Très insuffisant" 
                                               CssClass="EP_Imp_SmallNormal"
                                               style="margin-top: 1px; padding-top: 5px; font-size:70%; text-align: center;">
                                           </asp:Label> 
                                        </asp:TableCell>          
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHZ07" runat="server" V_Groupe="CritereAgent21"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="57px" VRadioN2Width="56px" VRadioN3Width="56px"
                                   VRadioN4Width="56px" VRadioN5Width="56px" VRadioN6Width="57px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>       
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="II_CadreBasDePage6" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelBasdePageNom6" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelBasdePagePrenom6" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelBasdePageCorps6" runat="server" Height="25px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelBasdePageAnnee6" runat="server" Height="25px" Width="30px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage6" runat="server" Height="25px" Width="50px"
                            Text="p.6/16" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>