﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P4_CESE2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P4_CESE2" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="IV_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreTitreFormation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelVolet" runat="server" Text="FORMATION" Height="20px" Width="748px"
                            CssClass="EP_Imp_MediumBold">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero11" runat="server" Height="20px" Width="748px"
                           Text="Bilan de l'année écoulée"
                           CssClass="EP_Imp_SmallBold">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero12" runat="server" Height="20px" Width="748px"
                           Text="Compétences acquises, formations suivies ou non satisfaites, difficultés éventuelles rencontrées…"
                           CssClass="EP_Imp_SmallItalic">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="IV_ListeFormationsSuivies" runat="server" CadreWidth="746px" SiColonneSelect="false"
                            SiAppliquerCharte="false" BackColorCaption="White" ForeColorCaption="Black" BackColorRow="White" ForeColorRow="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelTitreNumero13" runat="server" Height="20px" Width="748px"
                           Text="Commentaire sur les formations de l'année écoulée"
                           CssClass="EP_Imp_SmallNormal"
                           style="margin-bottom: 1px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVP03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="746px" DonHeight="180px" DonTabIndex="1" 
                           Etivisible="false"
                           Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableFooterRow>
                    <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
                        <asp:Label ID="NumeroPage7" runat="server" Height="15px" Width="250px"
                           BackColor="Transparent" BorderStyle="None"
                           Text="7 / 13" ForeColor="Black" Font-Italic="True"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                           style="text-align:right">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
            <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero21" runat="server" Height="22px" Width="748px"
                           Text="Formations souhaitées par l'agent et propositions de l'évaluateur"
                           CssClass="EP_Imp_SmallBold">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero22" runat="server" Height="18px" Width="748px"
                           Text="(à remplir pendant l'entretien)"
                           CssClass="EP_Imp_SmallNormal">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero23" runat="server" Height="20px" Width="748px" Text="A court terme"
                           CssClass="EP_Imp_SmallBold"
                           style="text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero24" runat="server" Height="33px" Width="748px"
                           Text="Afin de permettre d'assurer au mieux les fonctions confiées et d'atteindre les objectifs 
                           individuels et collectifs – Formation d'adaptation au poste de travail et à son environnement"
                           CssClass="EP_Imp_SmallItalic"
                           Font-Size="80%" style="margin-left: 16px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="IV_TableBesoin102" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="Transparent" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVA01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="740px" DonHeight="40px" DonTabIndex="2" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>                          
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="8px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVB01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="740px" DonHeight="40px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="8px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVC01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="740px" DonHeight="40px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>             
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="IV_TableBesoin103" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="Transparent" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell Height="8px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="IV_LabelTypeBesoin103" runat="server" Height="20px" Width="748px" Text="A moyen et long terme"
                                   CssClass="EP_Imp_SmallBold"
                                   style="padding-top: 4px; text-indent: 10px">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="IV_LabelTypeBesoin103Bis" runat="server" Height="33px" Width="748px"
                                   Text="Pour permettre l'évolution professionnelle de l'agent (évolution du métier, perspective d'un projet de service) – Formation préparant à l'évolution du métier ou de la carrière ?"
                                   CssClass="EP_Imp_SmallItalic"
                                   Font-Size="80%" style="margin-left: 16px">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVG01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="740px" DonHeight="40px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>             
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="8px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVH01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="740px" DonHeight="40px" DonTabIndex="6" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>             
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="8px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVI01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="740px" DonHeight="40px" DonTabIndex="7" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>             
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="IV_TableBesoin104" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="Transparent" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell Height="8px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="IV_LabelTypeBesoin104" runat="server" Height="20px" Width="748px"
                                   Text="Possibilité d'une Validation des Acquis de l'Expérience"
                                   CssClass="EP_Imp_SmallBold"
                                   style="padding-top: 4px; text-indent: 10px">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="IV_LabelTypeBesoin104Bis" runat="server" Height="35px" Width="748px"
                                   Text="Si oui dans quel domaine ?"
                                   CssClass="EP_Imp_SmallItalic"
                                   Font-Size="80%" style="margin-left: 16px; padding-top: 4px">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVM01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="740px" DonHeight="80px" DonTabIndex="8" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>             
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="IV_TableBesoin105" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="Transparent" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell Height="8px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="IV_LabelTypeBesoin105" runat="server" Height="20px" Width="748px" Text="Evolution professionnelle"
                                   CssClass="EP_Imp_SmallBold"
                                   style="padding-top: 4px; text-indent: 10px">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="IV_LabelTypeBesoin105Bis" runat="server" Height="33px" Width="748px"
                                   Text="Préparation d'examen professionnel ou de concours – ou action de formation souhaitée dans la perspective d'une mobilité interne ou externe"
                                   CssClass="EP_Imp_SmallItalic"
                                   Font-Size="80%" style="margin-left: 16px">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVN01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="740px" DonHeight="80px" DonTabIndex="9" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>             
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>        
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Center" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage8" runat="server" Height="15px" Width="748px"
                BackColor="Transparent" BorderStyle="None"
                Text="8 / 13" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:right; text-indent: 0px">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>