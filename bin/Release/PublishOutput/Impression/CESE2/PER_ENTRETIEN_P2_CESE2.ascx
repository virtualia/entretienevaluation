﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P2_CESE2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P2_CESE2" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="II_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreTitreObjectifs" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelVolet" runat="server" Text="OBJECTIFS POUR L'ANNEE A VENIR" Height="20px" Width="746px"
                            CssClass="EP_Imp_MediumBold"
                            style="margin-top: 1px">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelVoletSuite" runat="server" Height="30px" Width="746px"
                            Text="Mentionner les objectifs et priorités de travail ou points sur lesquels doit porter l'effort assignés pour l'année suivante"
                            CssClass="EP_Imp_SmallItalic"
                            style="margin-top: 2px; text-align: center">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelTitreNumero12" runat="server" Height="20px" Width="746px"
                           Text="Rappel des objectifs collectifs assignés au service"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 1px; text-indent: 2px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVI03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="744px" DonHeight="170px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 1px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelContexteAgent" runat="server" Height="22px" Width="746px"
                           Text="Objectifs de l'agent"
                           CssClass="EP_Imp_MediumNormal"
                           style="text-align: center">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableEnteteObjectifs" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="II_LabelEnteteObjectif1" runat="server" Height="40px" Width="313px" Text="Objectif"
                                   CssClass="EP_Imp_SmallBold"
                                   style="margin-left: 10px; text-align: center">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="II_LabelEnteteObjectif2" runat="server" Height="35px" Width="313px" Text="" 
                                   CssClass="EP_Imp_SmallItalic"
                                   Font-Size="90%"  style="margin-left: 10px; text-align: center">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableEnteteSeparateur" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="II_LabelEnteteSeparateur1" runat="server" Height="40px" Width="70px" Text=""
                                   CssClass="EP_Imp_SmallNormal"
                                   style="text-align: center">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="II_LabelEnteteSeparateur2" runat="server" Height="35px" Width="70px" Text=""
                                   CssClass="EP_Imp_SmallItalic"
                                   Font-Size="90%" style="text-align: center">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableEnteteConditions" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="II_LabelEnteteConditions1" runat="server" Height="40px" Width="320px"
                                   Text="Conditions de réussite"
                                   CssClass="EP_Imp_SmallBold"
                                   style="text-align: center">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="II_LabelEnteteConditions2" runat="server" Height="35px" Width="320px"
                                   Text="(moyens nécessaires, actions à conduire, formation, <br/> résultats attendus, délai de réalisation…)"
                                   CssClass="EP_Imp_SmallItalic"
                                   Font-Size="80%" style="text-align: center">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVA02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="2"
                                   DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableSeparateur1" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="II_LabelSeparateur1" runat="server" Height="90px" Width="75px"
                                   Text="Objectif <br/> n°1"
                                   CssClass="EP_Imp_MediumNormal"
                                   style="margin-top: 1px; padding-top: 20px; text-align: center">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableConditions1" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVA05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="3"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="4"
                                   DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableSeparateur2" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="II_LabelSeparateur2" runat="server" Height="90px" Width="75px"
                                   Text="Objectif <br/> n°2"
                                   CssClass="EP_Imp_MediumNormal"
                                   style="margin-top: 1px; padding-top: 20px; text-align: center">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableConditions2" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVB05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="6"
                                   DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableSeparateur3" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="II_LabelSeparateur3" runat="server" Height="90px" Width="75px"
                                   Text="Objectif <br/> n°3"
                                   CssClass="EP_Imp_MediumNormal"
                                   style="margin-top: 1px; padding-top: 20px; text-align: center">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableConditions3" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVC05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="8"
                                   DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableSeparateur4" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="II_LabelSeparateur4" runat="server" Height="90px" Width="75px"
                                   Text="Objectif <br/> n°4"
                                   CssClass="EP_Imp_MediumNormal"
                                   style="margin-top: 1px; padding-top: 20px; text-align: center">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="II_TableConditions4" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVD05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="9"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage4" runat="server" Height="15px" Width="250px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="4 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:right">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>