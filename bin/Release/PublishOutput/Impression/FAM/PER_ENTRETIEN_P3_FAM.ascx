﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P3_FAM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P3_FAM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="III_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreTitreCompetence" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelVolet" runat="server" Text="III. Expérience professionnelle : évaluation des acquis" 
                            Height="20px" Width="746px"
                            BackColor="Transparent"  BorderStyle="None"
                            ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelVoletTitre" runat="server" Text="Les compétences requises sur le poste / Les compétences mises en oeuvre par l'agent" 
                            Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="Transparent" BorderStyle="None"
                            BorderWidth="2px" ForeColor="Black"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 2px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="18px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelInformations" runat="server" Height="40px" Width="746px"
                            BackColor="Transparent"  BorderColor="Transparent" BorderStyle="none"
                            Text="LEGENDE pour compléter les colonnes NIVEAU REQUIS et NIVEAU de l'AGENT sachant
                            que le niveau d'exigence requis doit tenir compte du statut de l'agent :"
                            BorderWidth="1px" ForeColor="Black"  
                            Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "false" Font-Bold="False" 
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                            font-style: normal; text-indent: 0px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelNiveau4" runat="server" Height="50px" Width="746px"
                            BackColor="Transparent"  BorderColor="Transparent" BorderStyle="NotSet" 
                            Text="4 - Expert : domine le sujet, voire est capable de le faire évoluer - capacité à former et/ou à être tuteur.
			                La notion d'expert est ici distincte des certifications ou agréments attribués par les ministères dans
			                l'exercice de certaines fonctions spécifiques"
                            BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 3px;
                            font-style:  normal; text-indent: 0px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelNiveau3" runat="server" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="Transparent" BorderStyle="NotSet"
                            Text="3 - Maîtrise : connaissances approfondies - capacité à traiter de façon autonome les situations complexes ou inhabituelles"
                            BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 3px;
                            font-style:  normal; text-indent: 0px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow><asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelNiveau2" runat="server" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="Transparent" BorderStyle="NotSet" 
                            Text="2 - Pratique : connaissances générales - capacité à traiter de façon autonome les situations courantes"
                            BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                            font-style:  normal; text-indent: 0px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelNiveau1" runat="server" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="Transparent" BorderStyle="NotSet"
                            Text="1 - Initié : connaissances élémentaires, notions - capacité à faire mais en étant tutoré"
                            BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                            font-style:  normal; text-indent: 0px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelNiveau0" runat="server" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="Transparent" BorderStyle="NotSet" 
                            Text="0 - Non requis"
                            BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                            font-style:  normal; text-indent: 0px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelFamille1" runat="server" Height="35px" Width="748px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="COMPETENCES"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelSousFamille1" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" Text="(à compléter à partir de la fiche de poste)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreCompetences1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreCompetence1" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Compétence"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau1" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels1" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote11" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote12" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote13" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote14" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote15" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHA15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text=""
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace11" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHA09" runat="server" V_Groupe="CritereRequis1"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace12" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent1" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHA07" runat="server" V_Groupe="CritereAgent1"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace13" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation1" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHA15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur1" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreCompetences2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreCompetence2" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau2" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote21" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote22" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote23" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote24" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote25" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHB15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace21" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHB09" runat="server" V_Groupe="CritereRequis2"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace22" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent2" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHB07" runat="server" V_Groupe="CritereAgent2"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace23" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation2" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHB15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur2" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreCompetences3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreCompetence3" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau3" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels3" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote31" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote32" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote33" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote34" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote35" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHC15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace31" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHC09" runat="server" V_Groupe="CritereRequis3"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace32" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent3" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHC07" runat="server" V_Groupe="CritereAgent3"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace33" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation3" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHC15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur3" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreCompetences4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreCompetence4" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau4" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels4" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote41" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote42" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote43" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote44" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote45" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHD15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace41" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHD09" runat="server" V_Groupe="CritereRequis4"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace42" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent4" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHD07" runat="server" V_Groupe="CritereAgent4"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace43" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation4" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHD15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur4" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreCompetences5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreCompetence5" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau5" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels5" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote51" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote52" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote53" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote54" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote55" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHE15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace51" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHE09" runat="server" V_Groupe="CritereRequis5"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace52" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent5" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHE07" runat="server" V_Groupe="CritereAgent5"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace53" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation5" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHE15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage6" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="6 / 18" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelFamille2" runat="server" Height="35px" Width="748px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="SAVOIR-FAIRE"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreSavoirFaire6" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreSavoirFaire6" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau6" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels6" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote61" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote62" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote63" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote64" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote65" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHF15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text=""
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace61" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHF09" runat="server" V_Groupe="CritereRequis6"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace62" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent6" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHF07" runat="server" V_Groupe="CritereAgent6"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace63" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation6" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHF15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur6" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreSavoirFaire7" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreSavoirFaire7" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau7" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels7" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote71" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote72" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote73" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote74" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote75" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHG15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace71" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHG09" runat="server" V_Groupe="CritereRequis7"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace72" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent7" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHG07" runat="server" V_Groupe="CritereAgent7"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace73" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation7" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHG15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur7" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreSavoirFaire8" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreSavoirFaire8" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau8" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels8" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote81" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote82" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote83" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote84" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote85" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHH15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace81" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHH09" runat="server" V_Groupe="CritereRequis8"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace82" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent8" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHH07" runat="server" V_Groupe="CritereAgent8"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace83" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation8" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHH15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur8" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreSavoirFaire9" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreSavoirFaire9" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau9" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels9" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote91" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote92" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote93" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote94" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote95" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHI15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace91" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHI09" runat="server" V_Groupe="CritereRequis9"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace92" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent9" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHI07" runat="server" V_Groupe="CritereAgent9"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace93" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation9" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHI15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur9" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreSavoirFaire10" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreSavoirFaire10" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau10" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels10" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote101" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote102" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote103" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote104" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote105" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHJ15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace101" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHJ09" runat="server" V_Groupe="CritereRequis10"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace102" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent10" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHJ07" runat="server" V_Groupe="CritereAgent10"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace103" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation10" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHJ15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur10" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreSavoirFaire11" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreSavoirFaire11" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau11" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels11" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote111" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote112" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote113" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote114" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote115" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHK15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace111" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHK09" runat="server" V_Groupe="CritereRequis11"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace112" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent11" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHK07" runat="server" V_Groupe="CritereAgent11"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace113" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation11" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHK15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur11" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreSavoirFaire12" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreSavoirFaire12" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau12" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels12" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote121" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote122" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote123" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote124" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote125" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHL15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace121" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHL09" runat="server" V_Groupe="CritereRequis12"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace122" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent12" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHL07" runat="server" V_Groupe="CritereAgent12"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace123" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation12" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHL15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur12" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreSavoirFaire13" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreSavoirFaire13" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau13" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels13" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote131" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote132" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote133" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote134" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote135" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHM15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace131" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHM09" runat="server" V_Groupe="CritereRequis13"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace132" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent13" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHM07" runat="server" V_Groupe="CritereAgent13"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace133" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation13" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHM15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>    
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage7" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="7 / 18" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelFamille3" runat="server" Height="35px" Width="748px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="QUALITES RELATIONNELLES"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreQualites14" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreQualites14" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau14" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels14" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote141" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote142" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote143" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote144" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote145" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHN15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text=""
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace141" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHN09" runat="server" V_Groupe="CritereRequis14"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace142" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent14" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHN07" runat="server" V_Groupe="CritereAgent14"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace143" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation14" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHN15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur14" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreQualites15" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreQualites15" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau15" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels15" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote151" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote152" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote153" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote154" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote155" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHO15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace151" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHO09" runat="server" V_Groupe="CritereRequis15"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace152" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent15" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHO07" runat="server" V_Groupe="CritereAgent15"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace153" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation15" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHO15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur15" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreQualites16" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreQualites16" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau16" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels16" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote161" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote162" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote163" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote164" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote165" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHP15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace161" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHP09" runat="server" V_Groupe="CritereRequis16"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace162" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent16" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHP07" runat="server" V_Groupe="CritereAgent16"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace163" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation16" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHP15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur16" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreQualites17" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreQualites17" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau17" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels17" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote171" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote172" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote173" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote174" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote175" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHQ15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace171" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHQ09" runat="server" V_Groupe="CritereRequis17"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace172" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent17" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHQ07" runat="server" V_Groupe="CritereAgent17"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace173" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation17" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHQ15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur17" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreQualites18" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreQualites18" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau18" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels18" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote181" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote182" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote183" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote184" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote185" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHR15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace181" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHR09" runat="server" V_Groupe="CritereRequis18"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace182" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent18" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHR07" runat="server" V_Groupe="CritereAgent18"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace183" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation18" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHR15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur18" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreQualites19" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreQualites19" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau19" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels19" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote191" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote192" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote193" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote194" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote195" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHS15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace191" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHS09" runat="server" V_Groupe="CritereRequis19"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace192" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent19" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHS07" runat="server" V_Groupe="CritereAgent19"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace193" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation19" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHS15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur19" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreQualites20" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreQualites20" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau20" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels20" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote201" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote202" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote203" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote204" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote205" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHT15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace201" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHT09" runat="server" V_Groupe="CritereRequis20"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace202" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent20" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHT07" runat="server" V_Groupe="CritereAgent20"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace203" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation20" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHT15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage8" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="8 / 18" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelFamille4" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="LES APTITUDES AU MANAGEMENT"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="III_LabelSousFamille41" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" Text="(pour les agents en situation d'encadrement)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="III_CadreEncadrement" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left"
                                BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_EtiNbrAgentsEncadres" runat="server" Height="30px" Width="180px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="Nombre d'agents encadrés :"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoH015009" runat="server" Height="30px" Width="560px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="7px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVG03" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="80px" EtiHeight="20px" DonTabIndex="42" 
                                       Etitext="Commentaire sur les effectifs" 
                                       EtiBorderStyle="None" EtiBackColor="Transparent"
                                       Donstyle="margin-left: 1px;"
                                       Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                       DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreManagement21" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreManagement21" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau21" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels21" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote211" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote212" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote213" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote214" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHV15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent21" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHV07" runat="server" V_Groupe="CritereAgent21"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace213" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation21" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHV15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur21" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreManagement22" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreManagement22" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau22" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels22" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote221" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote222" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote223" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote224" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHW15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent22" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHW07" runat="server" V_Groupe="CritereAgent22"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace223" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation22" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHW15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur22" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreManagement23" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreManagement23" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau23" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels23" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote231" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote232" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote233" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote234" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHX15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent23" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHX07" runat="server" V_Groupe="CritereAgent23"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace233" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation23" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHX15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur23" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreManagement24" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreManagement24" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau24" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels24" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote241" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote242" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote243" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote244" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHY15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent24" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHY07" runat="server" V_Groupe="CritereAgent24"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace243" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation24" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHY15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur24" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreManagement25" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreManagement25" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau25" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels25" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote251" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote252" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote253" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote254" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHZ15301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent25" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHZ07" runat="server" V_Groupe="CritereAgent25"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace253" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation25" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHZ15311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur25" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreManagement26" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreManagement26" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau26" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels26" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote261" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote262" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote263" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote264" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoH115301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent26" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioH107" runat="server" V_Groupe="CritereAgent26"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace263" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation26" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoH115311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="III_LabelSeparateur26" runat="server" Height="1px" Width="740px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                     BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                     style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_CadreManagement27" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreManagement27" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveau27" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="III_CadreEnteteLabels27" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote271" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote272" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote273" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="III_LabelTitreNote274" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="65%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoH215301" runat="server" Height="20px" Width="256px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreNiveauAgent27" runat="server" Height="23px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Transparent" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioH207" runat="server" V_Groupe="CritereAgent27"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreEspace273" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="III_LabelTitreAppreciation27" runat="server" Height="28px" Width="117px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="APPRECIATION <br/> (le cas échéant)"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoH215311" runat="server" Height="20px" Width="353px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text=""
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage9" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="9 / 18" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>