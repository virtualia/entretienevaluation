﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P0_FAM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P0_FAM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<asp:Table ID="PAGE_0" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="O_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreTitreEntretien" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="none" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Table ID="CadreLogoFAM" runat="server" BackImageUrl="~/Images/General/LogoFAM.jpg" Width="160px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true">
                                    <asp:TableRow>
                                        <asp:TableCell Height="79px" BackColor="Transparent" Width="160px" HorizontalAlign="Center"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="10px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="O_Etiquette" runat="server" Text="COMPTE-RENDU DE L'ENTRETIEN PROFESSIONNEL" Height="25px" Width="750px"
                                    BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="1px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                    style="margin-top: 1px; margin-left: 1px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreAnneeEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDateEntretien" runat="server" Height="20px" Width="260px"
                                    BackColor="Transparent" BorderStyle="None" Text="réalisé le :"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15000" runat="server" Height="20px" Width="110px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 3px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelAnneeEntretien" runat="server" Text="" Height="20px" Width="380px"
                                    BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="2px" ForeColor="Black" Font-Italic="False"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                    style="margin-top: 0px; margin-left: 1px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 3px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>        
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreCommentaire" runat="server"
		                            Text="" 
		                            Height="20px" Width="730px"
                                    BackColor="Transparent"  BorderColor="Black" BorderStyle="None"
                                    BorderWidth="1px" ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 2px;
                                    font-style:  italic; text-indent: 0px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreAgent" runat="server" Text="Entre l'agent" Height="20px" Width="350px"
                                    BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="2px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                    style="margin-top: 5px; margin-left: 1px; margin-bottom: 2px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelIdentite" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Nom et prénom :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelIdentite" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelNaissance" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelNaissance" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelStatut" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Statut :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelStatut" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGrade" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Corps / grade :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGrade" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelEchelon" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Echelon :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelEchelon" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDateEchelon" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Date de l'échelon :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelDateEchelon" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelAffectation" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Direction, service, unité :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelAffectation" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelAffectationUnite" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelAffectationUnite" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelPoste" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Poste occupé :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelPoste" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow><asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDatePoste" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="depuis le :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 200px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelDatePoste" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreManager" runat="server" 
                                    Text="Et son supérieur hiérarchique direct" Height="30px" Width="350px"
                                    BackColor="Transparent"  BorderStyle="None"
                                    ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                    style="margin-top: 20px; margin-left: 1px; margin-bottom: 10px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelManager" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Nom et prénom :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15001" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGradeManager" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Corps / grade :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGradeManager" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelFonctionManager" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Fonctions exercées :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelFonctionManager" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>   
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow> 
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:Table ID="O_CadreEmploi" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreTitreEmploi" runat="server" Height="30px" CellPadding="0" Width="750px" 
                            CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVolet" runat="server" Text="Le poste occupé" Height="20px" Width="150px"
                                        BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVoletSuite" runat="server" 
                                        Text="" 
                                        Height="20px" Width="576px"
                                        BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreTitreFonctions" runat="server" Height="30px" CellPadding="0" Width="750px" 
                            CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelFonctionsExercees" runat="server" Text="Fonctions exercées :" Height="20px" Width="180px"
                                        BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_FonctionsExercees" runat="server" 
                                        Text="" Height="20px" Width="546px"
                                        BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="6px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelCotationPoste" runat="server" Text="Cotation du poste :" Height="20px" Width="180px"
                                        BackColor="Transparent"  BorderStyle="None"
                                        ForeColor="Black"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_CotationPoste" runat="server" 
                                        Text="" Height="20px" Width="546px"
                                        BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="6px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelQuotiteTravail" runat="server" Text="Quotité de travail :" Height="20px" Width="180px"
                                        BackColor="Transparent"  BorderStyle="None"
                                        ForeColor="Black"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_QuotiteTravail" runat="server" 
                                        Text="" Height="20px" Width="546px"
                                        BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="6px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                   <asp:Label ID="O_LabelTitreNumero5" runat="server" Text="Conflit d'intérêt"  Height="20px" Width="150px"
                                        BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                   </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                     <asp:Label ID="O_LabelSiConflit" runat="server" Height="20px" Width="420px"
                                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Etes-vous en situation potentielle de conflit d'intérêt ?"
                                        BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top:0px; margin-left:0px; margin-bottom:0px;
                                        font-style:normal;text-indent:3px;text-align: left;">
                                     </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell Width="330px" HorizontalAlign="Left">        
                                   <Virtualia:VTrioHorizontalRadio ID="O_RadioHA04" runat="server" V_Groupe="SiConflit"
                                       V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="false"
                                       RadioGaucheWidth="80px" RadioCentreWidth="80px" RadioDroiteWidth="0px"  
                                       RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
                                       RadioGaucheHeight="20px" RadioCentreHeight="20px"
                                       RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent"
                                       RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent"
                                       RadioCentreStyle="text-align:center;"  
                                       RadioGaucheStyle="margin-left:3px; text-align:  center;" Visible="true"/>
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                     <asp:Label ID="O_LabelSiConfli2" runat="server" Height="20px" Width="420px"
                                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Si oui, votre déclaration de conflit d'intérêt est-elle en règle ?"
                                        BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top:0px; margin-left:0px; margin-bottom:0px;
                                        font-style:normal;text-indent:3px;text-align: left;">
                                     </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell Width="330px" HorizontalAlign="Left">        
                                   <Virtualia:VTrioHorizontalRadio ID="O_RadioHB04" runat="server" V_Groupe="SiEnRegle"
                                       V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="false"
                                       RadioGaucheWidth="80px" RadioCentreWidth="80px" RadioDroiteWidth="0px"  
                                       RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
                                       RadioGaucheHeight="20px" RadioCentreHeight="20px"
                                       RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent"
                                       RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent"
                                       RadioCentreStyle="text-align:center;"  
                                       RadioGaucheStyle="margin-left:3px; text-align:  center;" Visible="true"/>
                                </asp:TableCell> 
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                   <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="6px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                   <asp:Label ID="O_LabelTitreNumero4" runat="server" Height="20px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Votre fiche de poste :"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                                       font-style: normal; text-indent: 1px; text-align:  left; ">
                                   </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                     <asp:Label ID="O_LabelSiAdaptation" runat="server" Height="20px" Width="248px"
                                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="- Est-elle adaptée au poste occupé ?"
                                        BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top:0px; margin-left:0px; margin-bottom:0px;
                                        font-style:normal;text-indent:3px;text-align: left;">
                                     </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell Width="500px" HorizontalAlign="Left">        
                                   <Virtualia:VTrioHorizontalRadio ID="O_RadioH15" runat="server" V_Groupe="SiEncadrement"
                                       V_PointdeVue="1" V_Objet="150" V_Information="15" V_SiDonneeDico="false"
                                       RadioGaucheWidth="80px" RadioCentreWidth="80px" RadioDroiteWidth="0px"  
                                       RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
                                       RadioGaucheHeight="20px" RadioCentreHeight="20px"
                                       RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent"
                                       RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent"
                                       RadioCentreStyle="text-align:center;"  
                                       RadioGaucheStyle="margin-left:3px; text-align:  center;" Visible="true"/>
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV16" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="16" V_SiDonneeDico="false"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="110px" EtiHeight="20px" DonTabIndex="1" 
                                       Etitext="- Sinon, sur quels points doit-elle être actualisée ?"
                                       EtiBorderStyle="None" EtiBackColor="Transparent"
                                       Donstyle="margin-left: 1px;"
                                       Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:small; font-style:normal"
                                       DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV17" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="17" V_SiDonneeDico="false"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="110px" EtiHeight="20px" DonTabIndex="2" 
                                       Etitext="Le cas échéant, donnez votre appréciation sur votre poste (attraits et contraintes)"
                                       EtiBorderStyle="None" EtiBackColor="Transparent"
                                       Donstyle="margin-left: 1px;"
                                       Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:small; font-style:normal"
                                       DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>      
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow> 
             </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage1" runat="server" Height="15px" Width="250px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="1 / 18" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>