﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P2_CNM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P2_CNM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

 <asp:Table ID="II_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="II_CadreTitreResultats1" runat="server" Height="50px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVolet1" runat="server" Text="BILAN DE L'ANNEE ECOULEE" Height="20px" Width="500px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVoletCollectivite1" runat="server" Text="" Height="20px" Width="216px"
                            CssClass="EP_Imp_SmallBold" style="margin-top: 5px; text-align: right;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" Width="748px" HorizontalAlign="Left" ColumnSpan="2" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVoletGrilleCriteres1" runat="server" Text="Grille de critères / CATEGORIE" Height="20px" Width="500px"
                            CssClass="EP_Imp_MediumBold" style="text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVoletAnnee1" runat="server" Text="Année" Height="20px" Width="216px"
                            CssClass="EP_Imp_MediumBold" style="text-align: right;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" BackColor="Gray">
                        <asp:Label ID="II_LabelFamille1" runat="server" Height="30px" Width="740px"
                           Text="Appréciation de la manière de servir et des qualités relationnelles de l'agent"
                           CssClass="EP_Imp_MediumBold" ForeColor="White"
                           style="margin-left: 0px; padding-top: 7px; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelLegende1" runat="server" Height="25px" Width="740px"
                           Text="PF : Point fort  -  PM : Point maîtrisé  -  PAM : Point à améliorer  -  PNS : Point non satisfaisant  -  SO : Sans objet"
                           CssClass="EP_Imp_SmallNormal" style="text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences1" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                              <asp:TableCell>
                                   <asp:Label ID="II_LabelCadrage1" runat="server" Height="21px" Width="521px" Text=""
                                       CssClass="EP_Imp_MediumBold">
                                   </asp:Label>  
                              </asp:TableCell>
                              <asp:TableCell BackColor="Gray" Width="180px">
                                   <asp:Label ID="II_LabelTitreNiveau1" runat="server" Height="18px" Width="180px" Text="APPRECIATION"
                                       CssClass="EP_Imp_MediumBold" ForeColor="White"
                                       Font-Size="90%" style="margin-left: 20px; margin-top: 2px; padding-top: 3px;">
                                   </asp:Label>  
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="II_LabelTitreCompetence1" runat="server" Height="27px" Width="513px" Text=""
                                       CssClass="EP_Imp_MediumBold"
                                       Font-Size="90%"
                                       style="margin-top: 1px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels1" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None" BackColor="Gray">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote11" runat="server" Height="22px" Width="45px" Text="PF"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote12" runat="server" Height="22px" Width="42px" Text="PM"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote13" runat="server" Height="22px" Width="42px" Text="PAM"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote14" runat="server" Height="22px" Width="42px" Text="PNS"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote15" runat="server" Height="22px" Width="45px" Text="SO"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>          
                                        </asp:TableCell>   
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVA01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="1" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHA07" runat="server" V_Groupe="CritereAgent1"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"  
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVB01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="2" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHB07" runat="server" V_Groupe="CritereAgent2"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"   
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVC01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="3" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHC07" runat="server" V_Groupe="CritereAgent3"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"    
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVD01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="4" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHD07" runat="server" V_Groupe="CritereAgent4"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVE01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="5" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHE07" runat="server" V_Groupe="CritereAgent5"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVF01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="6" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHF07" runat="server" V_Groupe="CritereAgent6"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVG01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="7" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHG07" runat="server" V_Groupe="CritereAgent7"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVH01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="8" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHH07" runat="server" V_Groupe="CritereAgent8"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVI01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="9" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHI07" runat="server" V_Groupe="CritereAgent9"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVJ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="10" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHJ07" runat="server" V_Groupe="CritereAgent10"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVE03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="735px" DonHeight="80px" DonTabIndex="11"
                           Donstyle="margin-left: 2px; Font-size: 11px;" DonBorderWidth="1px"
                           EtiVisible="true" EtiWidth="735px" EtiHeight="20px"  
                           EtiText="Commentaires du supérieur hiérarchique direct"
                           EtiBackColor="Transparent" EtiBorderColor="Transparent" DonBorderColor="Black" EtiBorderWidth="1px"
                           Etistyle="margin-left: 2px; text-indent:2px; font-weight:bold; font-style: normal;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>       
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" BackColor="Gray">
                        <asp:Label ID="II_LabelFamille2" runat="server" Height="45px" Width="740px"
                           Text="Appréciation des capacités d'encadrement ou le cas échéant à exercer des fonctions d'un niveau supérieur"
                           CssClass="EP_Imp_MediumBold" ForeColor="White"
                           style="margin-left: 0px; padding-top: 7px; text-indent: 0px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelLegende2" runat="server" Height="25px" Width="740px"
                           Text="PF : Point fort  -  PM : Point maîtrisé  -  PAM : Point à améliorer  -  PNS : Point non satisfaisant  -  SO : Sans objet"
                           CssClass="EP_Imp_SmallNormal"
                           style="text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences2" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                              <asp:TableCell>
                                   <asp:Label ID="II_LabelCadrage2" runat="server" Height="21px" Width="521px" Text=""
                                       CssClass="EP_Imp_MediumBold">
                                   </asp:Label>  
                              </asp:TableCell>
                              <asp:TableCell BackColor="Gray" Width="180px">
                                   <asp:Label ID="II_LabelTitreNiveau2" runat="server" Height="18px" Width="180px" Text="APPRECIATION"
                                       CssClass="EP_Imp_MediumBold" ForeColor="White"
                                       Font-Size="90%" style="margin-left: 20px; margin-top: 2px; padding-top: 3px;">
                                   </asp:Label>  
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="II_LabelTitreCompetence2" runat="server" Height="27px" Width="513px" Text=""
                                       CssClass="EP_Imp_MediumBold"
                                       Font-Size="90%"
                                       style="margin-top: 1px;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None" BackColor="Gray">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote21" runat="server" Height="22px" Width="45px" Text="PF"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote22" runat="server" Height="22px" Width="42px" Text="PM"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote23" runat="server" Height="22px" Width="42px" Text="PAM"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote24" runat="server" Height="22px" Width="42px" Text="PNS"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote25" runat="server" Height="22px" Width="45px" Text="SO"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>          
                                        </asp:TableCell>   
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVK01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="12" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHK07" runat="server" V_Groupe="CritereAgent11"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVL01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="13" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHL07" runat="server" V_Groupe="CritereAgent12"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVM01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="14" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHM07" runat="server" V_Groupe="CritereAgent13"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVN01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="15" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHN07" runat="server" V_Groupe="CritereAgent14"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVO01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="16" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHO07" runat="server" V_Groupe="CritereAgent15"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVP01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="17" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHP07" runat="server" V_Groupe="CritereAgent16"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVQ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="18" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHQ07" runat="server" V_Groupe="CritereAgent17"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVR01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="19" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHR07" runat="server" V_Groupe="CritereAgent18"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVF03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="735px" DonHeight="80px" DonTabIndex="20"
                           Donstyle="margin-left: 2px; Font-size: 11px;" DonBorderWidth="1px"
                           EtiVisible="true" EtiWidth="735px" EtiHeight="20px"  
                           EtiText="Commentaires du supérieur hiérarchique direct"
                           EtiBackColor="Transparent" EtiBorderColor="Transparent" DonBorderColor="Black" EtiBorderWidth="1px"
                           Etistyle="margin-left: 2px; text-indent:2px; font-weight:bold; font-style: normal;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>       
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="II_SautPage" runat="server">
                <asp:TableFooterRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="II_CadreBasDePage5" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="II_LabelBasdePageNom5" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell> 
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="II_LabelBasdePagePrenom5" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="II_LabelBasdePageGrade5" runat="server" Height="25px" Width="350px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="II_LabelBasdePageAnnee5" runat="server" Height="25px" Width="30px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="NumeroPage5" runat="server" Height="25px" Width="50px"
                                        Text="p.5/12" 
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                                    </asp:Label>   
                                </asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="II_CadreTitreResultats2" runat="server" Height="50px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVolet2" runat="server" Text="BILAN DE L'ANNEE ECOULEE" Height="20px" Width="500px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVoletCollectivite2" runat="server" Text="" Height="20px" Width="216px"
                            CssClass="EP_Imp_SmallBold" style="margin-top: 5px; text-align: right;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" Width="748px" HorizontalAlign="Left" ColumnSpan="2" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVoletGrilleCriteres2" runat="server" Text="Grille de critères / CATEGORIE" Height="20px" Width="500px"
                            CssClass="EP_Imp_MediumBold" style="text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVoletAnnee2" runat="server" Text="Année" Height="20px" Width="216px"
                            CssClass="EP_Imp_MediumBold" style="text-align: right;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" BackColor="Gray">
                        <asp:Label ID="II_LabelFamille3" runat="server" Height="45px" Width="740px"
                           Text="Appréciation des compétences techniques et professionnelles de l'agent et des acquis de l'expérience professionnelle"
                           CssClass="EP_Imp_MediumBold" ForeColor="White"
                           style="margin-left: 0px; padding-top: 7px; text-indent: 0px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelLegende3" runat="server" Height="25px" Width="740px"
                           Text="PF : Point fort  -  PM : Point maîtrisé  -  PAM : Point à améliorer  -  PNS : Point non satisfaisant  -  SO : Sans objet"
                           CssClass="EP_Imp_SmallNormal"
                           style="text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences3" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                              <asp:TableCell>
                                   <asp:Label ID="II_LabelCadrage3" runat="server" Height="21px" Width="521px" Text=""
                                       CssClass="EP_Imp_MediumBold">
                                   </asp:Label>  
                              </asp:TableCell>
                              <asp:TableCell BackColor="Gray" Width="180px">
                                   <asp:Label ID="II_LabelTitreNiveau3" runat="server" Height="18px" Width="180px" Text="APPRECIATION"
                                       CssClass="EP_Imp_MediumBold" ForeColor="White"
                                       Font-Size="90%" style="margin-left: 20px; margin-top: 2px; padding-top: 3px;">
                                   </asp:Label>  
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="II_LabelTitreCompetence3" runat="server" Height="27px" Width="513px" Text="COMPETENCES TECHNIQUES"
                                       CssClass="EP_Imp_MediumBold" style="margin-top: 1px;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels3" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None" BackColor="Gray">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote31" runat="server" Height="22px" Width="45px" Text="PF"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote32" runat="server" Height="22px" Width="42px" Text="PM"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote33" runat="server" Height="22px" Width="42px" Text="PAM"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote34" runat="server" Height="22px" Width="42px" Text="PNS"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote35" runat="server" Height="22px" Width="45px" Text="SO"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>          
                                        </asp:TableCell>   
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVS01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="21" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHS07" runat="server" V_Groupe="CritereAgent19"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVT01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="22" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHT07" runat="server" V_Groupe="CritereAgent20"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVU01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="23" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHU07" runat="server" V_Groupe="CritereAgent21"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVV01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="24" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHV07" runat="server" V_Groupe="CritereAgent22"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVW01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="25" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHW07" runat="server" V_Groupe="CritereAgent23"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVX01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="26" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHX07" runat="server" V_Groupe="CritereAgent24"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences4" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                              <asp:TableCell>
                                   <asp:Label ID="II_LabelCadrage4" runat="server" Height="21px" Width="521px" Text=""
                                       CssClass="EP_Imp_MediumBold">
                                   </asp:Label>  
                              </asp:TableCell>
                              <asp:TableCell BackColor="Gray" Width="180px">
                                   <asp:Label ID="II_LabelTitreNiveau4" runat="server" Height="18px" Width="180px" Text="APPRECIATION"
                                       CssClass="EP_Imp_MediumBold" ForeColor="White"
                                       Font-Size="90%" style="margin-left: 20px; margin-top: 2px; padding-top: 3px;">
                                   </asp:Label>  
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="II_LabelTitreCompetence4" runat="server" Height="27px" Width="513px" Text="COMPETENCES PROFESSIONNELLES"
                                       CssClass="EP_Imp_MediumBold" style="margin-top: 1px;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels4" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None" BackColor="Gray">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote41" runat="server" Height="22px" Width="45px" Text="PF"
                                               CssClass="EP_Imp_MediumBold"  ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote42" runat="server" Height="22px" Width="42px" Text="PM"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote43" runat="server" Height="22px" Width="42px" Text="PAM"
                                               CssClass="EP_Imp_MediumBold"  ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote44" runat="server" Height="22px" Width="42px" Text="PNS"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote45" runat="server" Height="22px" Width="45px" Text="SO"
                                               CssClass="EP_Imp_MediumBold" ForeColor="White"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px;">
                                           </asp:Label>          
                                        </asp:TableCell>   
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVY01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="27" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHY07" runat="server" V_Groupe="CritereAgent25"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent"                                     
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVZ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="28" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioHZ07" runat="server" V_Groupe="CritereAgent26"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoV001" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="29" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioH007" runat="server" V_Groupe="CritereAgent27"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoV101" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="30" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioH107" runat="server" V_Groupe="CritereAgent28"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoV201" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="31" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioH207" runat="server" V_Groupe="CritereAgent29"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoV301" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="32" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioH307" runat="server" V_Groupe="CritereAgent30"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoV401" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="33" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioH407" runat="server" V_Groupe="CritereAgent31"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoV501" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="34" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioH507" runat="server" V_Groupe="CritereAgent32"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoV601" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="35" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioH607" runat="server" V_Groupe="CritereAgent33"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoV701" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="36" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioH707" runat="server" V_Groupe="CritereAgent34"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoV801" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="false"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="37" 
                                   EtiVisible="false" DonBorderColor="Black"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="II_RadioH807" runat="server" V_Groupe="CritereAgent35"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"  VRadioN5Backcolor="Transparent"  VRadioN6Backcolor="Transparent"
                                   VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" VRadioN6BorderColor="Transparent" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVG03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="735px" DonHeight="180px" DonTabIndex="38"
                           Donstyle="margin-left: 2px; Font-size: 11px;" DonBorderWidth="1px"
                           EtiVisible="true" EtiWidth="735px" EtiHeight="20px"  
                           EtiText="Commentaires du supérieur hiérarchique direct"
                           EtiBackColor="Transparent" EtiBorderColor="Transparent" DonBorderColor="Black" EtiBorderWidth="1px"
                           Etistyle="margin-left: 2px; text-indent:2px; font-weight:bold; font-style: normal;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>       
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="II_CadreBasDePage6" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelBasdePageNom6" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelBasdePagePrenom6" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelBasdePageGrade6" runat="server" Height="25px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelBasdePageAnnee6" runat="server" Height="25px" Width="30px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage6" runat="server" Height="25px" Width="50px"
                            Text="p.6/12" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>