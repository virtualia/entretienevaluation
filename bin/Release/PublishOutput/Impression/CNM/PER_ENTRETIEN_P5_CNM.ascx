﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P5_CNM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P5_CNM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="V_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="V_CadreTitreObjectifs1" runat="server" Height="50px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVolet1" runat="server" Text="VOS OBJECTIFS année n+1" Height="20px" Width="500px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletCollectivite1" runat="server" Text="" Height="20px" Width="216px"
                            CssClass="EP_Imp_SmallBold" style="margin-top: 5px; text-align: right;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" Width="744px" HorizontalAlign="Left" ColumnSpan="2" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreNumero11" runat="server" Height="21px" Width="748px"
                           Text=". Rappel des objectifs de service :"
                           CssClass="EP_Imp_MediumBold" style="padding-top: 4px; text-align:left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVR03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="740px" DonHeight="150px" DonTabIndex="1" 
                           Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                           EtiVisible="false" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreNumero12" runat="server" Height="21px" Width="748px"
                           Text=". Objectifs de la période à venir"
                           CssClass="EP_Imp_MediumBold" style="padding-top: 4px; text-align:left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableEnteteObjectifs" runat="server" CellPadding="0" CellSpacing="0" Width="383px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="V_LabelEnteteObjectifTitre" runat="server" Height="30px" Width="373px"
                                   Text="Objectifs année n+1"
                                   CssClass="EP_Imp_MediumNormal" style="padding-top: 5px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableEnteteIndicateurs" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="V_LabelEnteteIndicateurs" runat="server" Height="30px" Width="230px"
                                   Text="Indicateurs"
                                   CssClass="EP_Imp_MediumNormal" style="padding-top: 5px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableEnteteEcheance" runat="server" CellPadding="0" CellSpacing="0" Width="127px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="V_LabelEnteteEcheance" runat="server" Height="30px" Width="117px"
                                   Text="Echéance"
                                   CssClass="EP_Imp_MediumNormal" style="padding-top: 5px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3" Width="750px"
                        BackColor="Transparent" BorderStyle="None">
                        <asp:Label ID="V_LabeNumeroObjectif1" runat="server" Height="25px" Width="200px" Text="Objectif 1"
                            CssClass="EP_Imp_SmallBold" style="padding-top: 5px; text-align: left; text-indent:2px; font-weight:bold; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="383px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVA02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="376px" DonHeight="100px" DonTabIndex="2"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableIndicateurs1" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVA05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                                   DonWidth="233px" DonHeight="100px" DonTabIndex="3"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableEcheance1" runat="server" CellPadding="0" CellSpacing="0" Width="127px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVA03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="false"
                                   DonWidth="122px" DonHeight="100px" DonTabIndex="4"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3" Width="750px"
                        BackColor="Transparent" BorderStyle="None">
                        <asp:Label ID="V_LabeNumeroObjectif2" runat="server" Height="25px" Width="200px" Text="Objectif 2"
                            CssClass="EP_Imp_SmallBold" style="padding-top: 5px; text-align: left; text-indent:2px; font-weight:bold; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="383px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="376px" DonHeight="100px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableIndicateurs2" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVB05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                                   DonWidth="233px" DonHeight="100px" DonTabIndex="6"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableEcheance2" runat="server" CellPadding="0" CellSpacing="0" Width="127px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVB03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="false"
                                   DonWidth="122px" DonHeight="100px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3" Width="750px"
                        BackColor="Transparent" BorderStyle="None">
                        <asp:Label ID="V_LabeNumeroObjectif3" runat="server" Height="25px" Width="200px" Text="Objectif 3"
                            CssClass="EP_Imp_SmallBold" style="padding-top: 5px; text-align: left; text-indent:2px; font-weight:bold; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="383px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="376px" DonHeight="100px" DonTabIndex="8"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableIndicateurs3" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVC05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                                   DonWidth="233px" DonHeight="100px" DonTabIndex="9"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableEcheance3" runat="server" CellPadding="0" CellSpacing="0" Width="127px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="false"
                                   DonWidth="122px" DonHeight="100px" DonTabIndex="10"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3" Width="750px"
                        BackColor="Transparent" BorderStyle="None">
                        <asp:Label ID="V_LabeNumeroObjectif4" runat="server" Height="25px" Width="200px" Text="Objectif 4"
                            CssClass="EP_Imp_SmallBold" style="padding-top: 5px; text-align: left; text-indent:2px; font-weight:bold; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="383px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   DonWidth="376px" DonHeight="100px" DonTabIndex="11"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableIndicateurs4" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVD05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                                   DonWidth="233px" DonHeight="100px" DonTabIndex="12"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="V_TableEcheance4" runat="server" CellPadding="0" CellSpacing="0" Width="127px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="false"
                                   DonWidth="122px" DonHeight="100px" DonTabIndex="13"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="V_SautPage" runat="server">
                <asp:TableFooterRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="V_CadreBasDePage10" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="V_LabelBasdePageNom10" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell> 
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="V_LabelBasdePagePrenom10" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="V_LabelBasdePageGrade10" runat="server" Height="25px" Width="350px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="V_LabelBasdePageAnnee10" runat="server" Height="25px" Width="30px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="NumeroPage10" runat="server" Height="25px" Width="50px"
                                        Text="p.10/12" 
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                                    </asp:Label>   
                                </asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="V_CadreTitreObjectifs2" runat="server" Height="50px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVolet2" runat="server" Text="VOS OBJECTIFS année n+1" Height="20px" Width="500px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVoletCollectivite2" runat="server" Text="" Height="20px" Width="216px"
                            CssClass="EP_Imp_SmallBold" style="margin-top: 5px; text-align: right;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" Width="744px" HorizontalAlign="Left" ColumnSpan="2" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreNumero31" runat="server" Height="25px" Width="748px"
                           Text="Modalités permettant la réalisation des objectifs (à définir entre l'évaluateur et l'évalué)"
                           CssClass="EP_Imp_MediumBold" style="padding-top: 4px; text-align:left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Width="743px" 
                        BackColor="Transparent" BorderStyle="None">
                        <asp:Label ID="V_LabeNumObjectif1" runat="server" Height="25px" Width="745px" Text="Objectif 1"
                            CssClass="EP_Imp_SmallBold" style="padding-top: 5px; text-indent:2px; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="V_TableModaliteObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVA04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="false"
                                   DonWidth="746px" DonHeight="50px" DonTabIndex="14"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Width="743px" 
                        BackColor="Transparent" BorderStyle="None">
                        <asp:Label ID="V_LabeNumObjectif2" runat="server" Height="25px" Width="745px" Text="Objectif 2"
                            CssClass="EP_Imp_SmallBold" style="padding-top: 5px; text-indent:2px; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="V_TableModaliteObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVB04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="false"
                                   DonWidth="746px" DonHeight="50px" DonTabIndex="15"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Width="743px"
                        BackColor="Transparent" BorderStyle="None">
                        <asp:Label ID="V_LabeNumObjectif3" runat="server" Height="25px" Width="745px" Text="Objectif 3"
                            CssClass="EP_Imp_SmallBold" style="padding-top: 5px; text-indent:2px; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="V_TableModaliteObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVC04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="false"
                                   DonWidth="746px" DonHeight="50px" DonTabIndex="16"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Width="743px"
                        BackColor="Transparent" BorderStyle="None">
                        <asp:Label ID="V_LabeNumObjectif4" runat="server" Height="25px" Width="745px" Text="Objectif 4"
                            CssClass="EP_Imp_SmallBold" style="padding-top: 5px; text-indent:2px; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="V_TableModaliteObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVD04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="false"
                                   DonWidth="746px" DonHeight="50px" DonTabIndex="17"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="false" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>          
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="V_CadreBasDePage11" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelBasdePageNom11" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelBasdePagePrenom11" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelBasdePageGrade11" runat="server" Height="25px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelBasdePageAnnee11" runat="server" Height="25px" Width="30px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage11" runat="server" Height="25px" Width="50px"
                            Text="p.11/12" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>