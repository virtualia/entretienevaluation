﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P6_CNM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P6_CNM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>


<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="VI_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VI_CadreTitreConclusion" runat="server" Height="50px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVolet" runat="server" Text="CONCLUSION DE L'ENTRETIEN" Height="20px" Width="500px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVoletCollectivite" runat="server" Text="" Height="20px" Width="216px"
                            CssClass="EP_Imp_SmallBold" style="margin-top: 5px; text-align: right;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" Width="744px" HorizontalAlign="Left" ColumnSpan="2" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero11" runat="server" Height="20px" Width="748px"
                           Text="Observations générales du supérieur hiérarchique"
                           CssClass="EP_Imp_MediumBold" style="padding-top: 7px; text-indent: 3px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="false"
                           DonWidth="745px" DonHeight="200px" DonTabIndex="1" 
                           Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                           EtiVisible="false" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero12" runat="server" Height="20px" Width="748px"
                           Text="Observations éventuelles de l'agent"
                           CssClass="EP_Imp_MediumBold" style="padding-top: 7px; text-indent: 3px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVS03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="745px" DonHeight="200px" DonTabIndex="2" 
                           Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                           EtiVisible="false" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>  
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreSignatures" runat="server" CellPadding="0" Width="748px" 
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell Width="294px">
                        <asp:Table ID="VI_CadreIdentite" runat="server" Width="294px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_LabelSignatureIdentite" runat="server" Height="40px" Width="294px" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"
                                       Text="Nom et prénom"
                                       CssClass="EP_Imp_MediumBold" style="margin-left:-1px; padding-top: 8px;">
                                    </asp:Label>
                                </asp:TableCell> 
                            </asp:TableRow>
                        </asp:Table> 
                    </asp:TableCell>
                    <asp:TableCell Width="296px">
                        <asp:Label ID="VI_LabelSignatureFonction" runat="server" Height="40px" Width="296px" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"
                           Text="Fonction"
                           CssClass="EP_Imp_MediumBold" style="padding-top: 8px;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell Width="150px">
                        <asp:Label ID="VI_LabelSignatureDate" runat="server" Height="40px" Width="150px" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"
                           Text="Signature et date"
                           CssClass="EP_Imp_MediumBold" style="padding-top: 8px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="294px">
                        <asp:Table ID="VI_CadreIdentiteAgent" runat="server" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px" Visible="true" Width="294px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_LabelIdentiteAgent" runat="server" Height="20px" Width="292px"
                                       Text="Agent :"
                                       CssClass="EP_Imp_SmallNormal" style="text-indent: 3px;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_IdentiteAgent" runat="server" Height="40px" Width="292px" Text=""
                                       CssClass="EP_Imp_SmallBold" style="text-indent: 3px;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell Width="296px">
                        <asp:Label ID="VI_LabelFonctionAgent" runat="server" Height="61px" Width="296px" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px" Text=""
                           CssClass="EP_Imp_SmallNormal" style="padding-top: 10px; text-indent: 3px;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell Width="150px">
                        <asp:Label ID="VI_InfoH15004" runat="server" Height="61px" Width="150px" Text="" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"
                           CssClass="EP_Imp_SmallBold" style="padding-top: 10px; text-align:center;">
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="294px">
                        <asp:Table ID="VI_CadreIdentiteEvaluateur" runat="server" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px" Visible="true" Width="294px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_LabelIdentiteEvaluateur" runat="server" Height="20px" Width="292px"
                                       Text="Supérieur hiérarchique direct :"
                                       CssClass="EP_Imp_SmallNormal" style="text-indent: 3px;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_InfoH15001" runat="server" Height="40px" Width="292px" Text=""
                                       CssClass="EP_Imp_SmallBold" style="text-indent: 3px;">
                                    </asp:Label>            
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell Width="296px">
                        <asp:Label ID="VI_LabelFonctionManager" runat="server" Height="61px" Width="296px" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px" Text=""
                           CssClass="EP_Imp_SmallNormal" style="padding-top: 10px; text-indent: 3px;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell Width="150px">
                        <asp:Label ID="VI_InfoH15003" runat="server" Height="61px" Width="150px" Text="" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"
                           CssClass="EP_Imp_SmallBold" style="padding-top: 10px; text-align:center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="294px">
                        <asp:Table ID="VI_CadreIdentiteAutorite" runat="server" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px" Visible="true" Width="294px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_LabelIdentiteAutorite" runat="server" Height="20px" Width="292px"
                                       Text="Autorité territoriale :"
                                       CssClass="EP_Imp_SmallNormal" style="text-indent: 3px;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_IdentiteAutorite" runat="server" Height="40px" Width="292px" 
			                           Text="Maire ou Président"
                                       CssClass="EP_Imp_SmallNormal" style="text-indent: 3px;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell Width="296px">
                        <asp:Label ID="VI_LabelFonctionAutorite" runat="server" Height="61px" Width="296px" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px" Text=""
                           CssClass="EP_Imp_SmallNormal" style="padding-top: 10px;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell Width="150px">
                        <asp:Label ID="VI_InfoH15023" runat="server" Height="61px" Width="150px" Text="" BorderStyle="NotSet" BorderColor="Black" BorderWidth="1px"
                           CssClass="EP_Imp_SmallBold" style="padding-top: 10px; text-align:center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>   
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreModalitesRecours" runat="server" Height="30px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTexteRecours1" runat="server" Height="30px" Width="748px"
                            Text="Demande de révision du compte-rendu"
                            CssClass="EP_Imp_SmallBold" style="margin-left: 2px; text-indent: 4px; text-align:left; text-decoration:underline">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTexteRecours2" runat="server" Height="23px" Width="748px"
                            Text="Article 7 du décret n°2014-1526 du 16 décembre 2014"
                            CssClass="EP_Imp_SmallNormal" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTexteRecours3" runat="server" Height="23px" Width="748px"
                            Text="- Auprès de l'autorité territoriale (dans un délai de 15 jours après réception après notification)"
                            CssClass="EP_Imp_SmallNormal" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTexteRecours4" runat="server" Height="40px" Width="748px"
                            Text="- Puis saisine des membres de la CAP compétente (à compter de 15 jours après réception de la réponse de l'autorité territoriale)"
                            CssClass="EP_Imp_SmallNormal" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTexteRecours5" runat="server" Height="30px" Width="748px"
                            Text="Recours pour excès de pouvoir"
                            CssClass="EP_Imp_SmallBold" style="margin-left: 2px; text-indent: 4px; text-align:left;text-decoration:underline">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTexteRecours6" runat="server" Height="25px" Width="748px"
                            Text="Auprès du Tribunal Administratif dans un délai de deux mois à compter de la présente notification"
                            CssClass="EP_Imp_SmallNormal" style="margin-left: 2px; text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>                
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>   
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="VI_CadreBasDePage12" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelBasdePageNom12" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelBasdePagePrenom12" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelBasdePageGrade12" runat="server" Height="25px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelBasdePageAnnee12" runat="server" Height="25px" Width="30px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage12" runat="server" Height="25px" Width="50px"
                            Text="p.12/12" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>