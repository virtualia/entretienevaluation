﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P7.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P7" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="VII_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VII_CadreTitreAppreciation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VII_LabelVolet" runat="server" Text="7. APPRECIATIONS GENERALES DE LA VALEUR PROFESSIONNELLE DE L'AGENT" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderStyle="None"
                            ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VII_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VII_LabelTitreNumero1" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="7.1 Synthèse de l'évaluation des résultats"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="VII_CocheA10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="false" V_Height="30px"
                           V_Text="  résultats se situant au-delà des objectifs assignés" V_Width="450px" V_SiModeCaractere="true"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left"/>               
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="VII_CocheB10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="false" V_Height="30px"
                           V_Text="  résultats conformes aux objectifs assignés" V_Width="450px" V_SiModeCaractere="true"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left"/>               
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="VII_CocheC10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="false" V_Height="30px"
                           V_Text="  résultats partiellement conformes aux objectifs assignés" V_Width="450px" V_SiModeCaractere="true"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left"/>               
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="VII_CocheD10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="false" V_Height="30px"
                           V_Text="  résultats insuffisants par rapport aux objectifs assignés" V_Width="450px" V_SiModeCaractere="true"
                           V_BorderStyle="None" V_BackColor="Transparent"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left"/>               
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VII_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VII_LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="7.2 Synthèse de l'évaluation de la manière de servir"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="VII_CadreCompetences" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Left">
                                   <asp:Label ID="VII_LabelTitreCompetence" runat="server" Height="44px" Width="310px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text=""
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False" Visible="false"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 1px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Left">
                                  <asp:Table ID="VII_CadreEnteteLabels" runat="server" CellPadding="0" CellSpacing="0">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Left">
                                           <asp:Label ID="VII_LabelTitreNote1" runat="server" Height="44px" Width="60px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="sans objet"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                           <asp:Label ID="VII_LabelTitreNote2" runat="server" Height="44px" Width="80px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="insuffisant"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                           <asp:Label ID="VII_LabelTitreNote3" runat="server" Height="44px" Width="90px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="en cours d'acquisition"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                           <asp:Label ID="VII_LabelTitreNote4" runat="server" Height="44px" Width="60px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="acquis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                           <asp:Label ID="VII_LabelTitreNote5" runat="server" Height="44px" Width="60px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="maîtrisé"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                           <asp:Label ID="VII_LabelTitreNote6" runat="server" Height="44px" Width="60px"
                                               BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="VII_InfoHA15601" runat="server" Height="20px" Width="316px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="Compétences professionnelles et technicité"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 1px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="VII_RadioHA03" runat="server" V_Groupe="Critere1"
                                     V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                     VRadioN1Width="55px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="55px" VRadioN5Width="55px" VRadioN6Width="54px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 57px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 5px; margin-top: 0px;" VRadioN5Style="margin-left: 5px; margin-top: 0px;" VRadioN6Style="margin-left: 5px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                                     VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                                     VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent" 
                                     Visible="true" VRadioN1visible="false"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="VII_InfoHB15601" runat="server" Height="20px" Width="316px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="Qualités personnelles et relationnelles"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 1px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="VII_RadioHB03" runat="server" V_Groupe="Critere2"
                                     V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                     VRadioN1Width="55px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="55px" VRadioN5Width="55px" VRadioN6Width="54px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 57px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 5px; margin-top: 0px;" VRadioN5Style="margin-left: 5px; margin-top: 0px;" VRadioN6Style="margin-left: 5px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                                     VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                                     VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent" 
                                     Visible="true" VRadioN1visible="false"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="VII_InfoHC15601" runat="server" Height="20px" Width="316px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="Méthode et résultats"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 1px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="VII_RadioHC03" runat="server" V_Groupe="Critere3"
                                     V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                     VRadioN1Width="55px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="55px" VRadioN5Width="55px" VRadioN6Width="54px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 57px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 5px; margin-top: 0px;" VRadioN5Style="margin-left: 5px; margin-top: 0px;" VRadioN6Style="margin-left: 5px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                                     VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                                     VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent" 
                                     Visible="true" VRadioN1visible="false"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="VII_InfoHD15601" runat="server" Height="20px" Width="316px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="Aptitude au management et à la conduite de projet"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 1px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="VII_RadioHD03" runat="server" V_Groupe="Critere4"
                                     V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                                     VRadioN1Width="55px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="55px" VRadioN5Width="55px" VRadioN6Width="54px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 5px; margin-top: 0px;" VRadioN5Style="margin-left: 5px; margin-top: 0px;" VRadioN6Style="margin-left: 5px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                                     VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                                     VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>   
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VII_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="VII_LabelTitreNumero3" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="7.3 Appréciation littérale"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VII_InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="400px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="False" DonBorderColor="Black"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align: left; text-indent: 1px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VII_EtiLabelDateEvaluateur" runat="server" Height="20px" Width="100px"
                           BackColor="Transparent" BorderStyle="None" Text="Date :"
                           ForeColor="Black" Font-Italic="True"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VII_InfoHA15003" runat="server" Height="20px" Width="150px"
                           BackColor="Transparent" BorderStyle="None" Text=""
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style:  normal; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="VII_LabelSignatureEvaluateur" runat="server" Height="20px" Width="300px"
                           BackColor="Transparent" BorderStyle="None" Text="Signature du supérieur hiérarchique direct"
                           ForeColor="Black" Font-Italic="True"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="120px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage15" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="15 / 16" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>
