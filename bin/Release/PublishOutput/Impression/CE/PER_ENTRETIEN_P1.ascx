﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P1.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P1" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="I_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;  ">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="I_CadreTitreResultats" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVolet" runat="server" Text="1. RESULTATS PROFESSIONNELS" Height="25px" Width="746px"
                            BackColor="Transparent"  BorderStyle="None"
                            ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVoletBis" runat="server" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                            Text="Il s'agit ici de réaliser en commun un bilan factuel de l'aisance de l'agent à évoluer dans ses fonctions."
                            ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: italic; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="I_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelTitreNumero1" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"
                           Text="1.1 Evènements survenus au cours de la période écoulée"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 5px; margin-left: 0px; margin-bottom: 3px;
                           font-style: normal; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="60px" DonTabIndex="1" 
                           Etitext="(il peut s'agir d'évènements professionnels de toute nature, susceptibles d'avoir affecté l'activité 
                           de l'agent (nouvelle organisation, nouvelles méthodes de travail, redéfinition ou évolution des missions du poste, 
                           formations suivies, nouveaux outils...) :"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align: left; text-indent: 1px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVI03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="2" 
                           Etitext="Evolution ou redéfinition du périmètre du poste :"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align: left; text-indent: 1px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage2" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="2 / 16" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="I_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Label ID="I_LabelTitreNumero2" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="1.2 Bilan d'activité de la période écoulée au regard des objectifs assignés"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Label ID="I_LabelTitreInfos2" runat="server" Height="60px" Width="748px"
                           BackColor="Transparent" BorderStyle="None"
                           Text="(ce bilan met en rapport les objectifs fixés à l'occasion du précédent entretien 
                           (ou à l'occasion de la prise de fonction de l'agent) et les résultats obtenus. 
                           L'agent peut rédiger une présentation synthétique de ses activités au cours de la période écoulée, 
                           qui - s'il le souhaite - sera annexée au compte-rendu de l'entretien) :"
                           ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif1" runat="server" Height="20px" Width="241px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Rappel de l'objectif"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif2" runat="server" Height="20px" Width="241px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="(si objectif collectif, précisez)"
                                   ForeColor="Black" Font-Italic="true"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"  
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: italic; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteDifficultes" runat="server" CellPadding="0" CellSpacing="0" Width="310px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteDifficultes" runat="server" Height="44px" Width="300px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Difficultés rencontrées"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation" runat="server" CellPadding="0" CellSpacing="0" Width="180px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                <asp:Label ID="I_LabelEnteteRealisation" runat="server" Height="20px" Width="170px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Réalisation"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteComplete" runat="server" Height="20px" Width="59px"
                                   BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="complète"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEntetePartielle" runat="server" Height="20px" Width="59px"
                                   BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="partielle"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                                   style="margin-top: 0px; margin-left: 1px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteInsuffisante" runat="server" Height="20px" Width="59px"
                                   BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="insuff."
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                                   style="margin-top: 0px; margin-left: 1px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="244px" DonHeight="140px" DonTabIndex="3"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableDifficultes1" runat="server" CellPadding="0" CellSpacing="0" Width="310px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="303px" DonHeight="140px" DonTabIndex="4"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="186px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="147px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VTrioHorizontalRadio ID="I_RadioHC04" runat="server" V_Groupe="Realisation1"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                   RadioGaucheWidth="50px" RadioCentreWidth="50px" RadioDroiteWidth="50px"  
                                   RadioGaucheText="" RadioCentreText="" RadioDroiteText=""
                                   RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                                   RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                                   RadioDroiteBorderStyle="None" RadioDroiteBackColor="Transparent"
                                   RadioGaucheHeight="25px" RadioCentreHeight="25px" RadioDroiteHeight="25px"
                                   RadioGaucheStyle="text-align:  center;" RadioCentreStyle="text-align:  center;" RadioDroiteStyle="text-align:  center;"
                                   Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_InfoHC15105" runat="server" Height="20px" Width="151px"
                                   BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                   Text=""
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 8px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="244px" DonHeight="140px" DonTabIndex="6"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableDifficultes2" runat="server" CellPadding="0" CellSpacing="0" Width="310px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   EtiVisible="false" DonWidth="303px" DonHeight="140px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="186px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="147px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VTrioHorizontalRadio ID="I_RadioHD04" runat="server" V_Groupe="Realisation2"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                   RadioGaucheWidth="50px" RadioCentreWidth="50px" RadioDroiteWidth="50px"  
                                   RadioGaucheText="" RadioCentreText="" RadioDroiteText=""
                                   RadioGaucheHeight="25px" RadioCentreHeight="25px" RadioDroiteHeight="25px"
                                   RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                                   RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                                   RadioDroiteBorderStyle="None" RadioDroiteBackColor="Transparent"
                                   RadioGaucheStyle="text-align:  center;" RadioCentreStyle="text-align:  center;" RadioDroiteStyle="text-align:  center;"
                                   Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_InfoHD15105" runat="server" Height="20px" Width="151px"
                                   BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                   Text=""
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 8px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="244px" DonHeight="140px" DonTabIndex="9"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableDifficultes3" runat="server" CellPadding="0" CellSpacing="0" Width="310px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="303px" DonHeight="140px" DonTabIndex="10"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="186px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="147px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VTrioHorizontalRadio ID="I_RadioHE04" runat="server" V_Groupe="Realisation3"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                   RadioGaucheWidth="50px" RadioCentreWidth="50px" RadioDroiteWidth="50px"  
                                   RadioGaucheText="" RadioCentreText="" RadioDroiteText=""
                                   RadioGaucheHeight="25px" RadioCentreHeight="25px" RadioDroiteHeight="25px"
                                   RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                                   RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                                   RadioDroiteBorderStyle="None" RadioDroiteBackColor="Transparent"
                                   RadioGaucheStyle="text-align:  center;" RadioCentreStyle="text-align:  center;" RadioDroiteStyle="text-align:  center;"
                                   Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_InfoHE15105" runat="server" Height="20px" Width="151px"
                                   BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                   Text=""
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 8px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="244px" DonHeight="140px" DonTabIndex="12"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableDifficultes4" runat="server" CellPadding="0" CellSpacing="0" Width="310px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="303px" DonHeight="140px" DonTabIndex="13"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="186px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="147px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VTrioHorizontalRadio ID="I_RadioHF04" runat="server" V_Groupe="Realisation4"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                   RadioGaucheWidth="50px" RadioCentreWidth="50px" RadioDroiteWidth="50px"  
                                   RadioGaucheText="" RadioCentreText="" RadioDroiteText=""
                                   RadioGaucheHeight="25px" RadioCentreHeight="25px" RadioDroiteHeight="25px"
                                   RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                                   RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                                   RadioDroiteBorderStyle="None" RadioDroiteBackColor="Transparent"
                                   RadioGaucheStyle="text-align:  center;" RadioCentreStyle="text-align:  center;" RadioDroiteStyle="text-align:  center;"
                                   Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_InfoHF15105" runat="server" Height="20px" Width="151px"
                                   BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                   Text=""
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 8px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="244px" DonHeight="140px" DonTabIndex="15"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableDifficultes5" runat="server" CellPadding="0" CellSpacing="0" Width="310px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="303px" DonHeight="140px" DonTabIndex="16"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation5" runat="server" CellPadding="0" CellSpacing="0" Width="186px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="147px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VTrioHorizontalRadio ID="I_RadioHG04" runat="server" V_Groupe="Realisation5"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                   RadioGaucheWidth="50px" RadioCentreWidth="50px" RadioDroiteWidth="50px"  
                                   RadioGaucheText="" RadioCentreText="" RadioDroiteText=""
                                   RadioGaucheHeight="25px" RadioCentreHeight="25px" RadioDroiteHeight="25px"
                                   RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                                   RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                                   RadioDroiteBorderStyle="None" RadioDroiteBackColor="Transparent"
                                   RadioGaucheStyle="text-align:  center;" RadioCentreStyle="text-align:  center;" RadioDroiteStyle="text-align:  center;"
                                   Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_InfoHG15105" runat="server" Height="20px" Width="151px"
                                   BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                   Text=""
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 8px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVJ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="60px" DonTabIndex="18"
                           EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                           Etitext="Autres réalisations sur lesquelles l'agent s'est investi au cours de la période écoulée (sont précisés 
                           la nature de la réalisation (prise en charge d'un dossier, participation à un groupe de travail, intérim d'un poste...), 
                           les résultats obtenus et, le cas échéant, les difficultés rencontrées) :"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size: smaller"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage3" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="3 / 16" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>