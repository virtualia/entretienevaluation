﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P6.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P6" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioVerticalRadio.ascx" tagname="VTrioVerticalRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="VI_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreTitreFormation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVolet" runat="server" Text="6. BESOINS DE FORMATION" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderStyle="None"
                            ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreNumero1" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="6.1 Bilan de la période écoulée"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 5px;
                           font-style: normal; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitre11" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderStyle="None" Text="6.1.1 Formations suivies"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px;
                           font-style: normal; text-indent: 20px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="VI_LabelFormation1" runat="server" Height="24px" Width="580px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                            BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                            font-style:  oblique; text-indent: 20px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="VI_LabelFormation2" runat="server" Height="24px" Width="580px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                            BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                            font-style:  oblique; text-indent: 20px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="VI_LabelFormation3" runat="server" Height="24px" Width="580px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                            BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                            font-style:  oblique; text-indent: 20px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitre12" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderStyle="None" Text="6.1.2 Demandes de formation non satisfaites"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 20px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVU03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="746px" DonHeight="140px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="false" DonBorderColor="Black"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align: left; text-indent: 1px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitre13" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderStyle="None" Text="6.1.3 Actions conduites en tant que formateur"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 20px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVV03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="746px" DonHeight="140px" EtiHeight="20px" DonTabIndex="2" 
                           Etivisible="false" DonBorderColor="Black"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 1px; text-align: left; text-indent: 1px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage12" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="12 / 16" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="left">
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="6.2 Formations proposées pour la nouvelle période"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitre21" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderStyle="None" Text="6.2.1 Expression des besoins en formation"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 7px;
                           font-style: normal; text-indent: 20px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                      <asp:Table ID="VI_TableBesoin1" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <asp:Label ID="VI_LabelTypeBesoin1" runat="server" Height="30px" Width="740px"
                               BackColor="Transparent" BorderColor="Black" BorderStyle="None" Text="Formations d'adaptation au poste de travail"
                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteStage1" runat="server" Height="50px" Width="220px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                               Text="Intitulé du stage"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEntetePeriode1" runat="server" Height="50px" Width="120px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Période souhaitée"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteObjectif1" runat="server" Height="50px" Width="240px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteAvis1" runat="server" Height="50px" Width="145px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVA03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVA04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVA05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVA06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis1" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif11" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHA15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVB03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVB04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVB05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVB06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis1" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif12" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHB15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVC03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVC04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVC05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVC06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis1" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif13" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHC15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                      <asp:Table ID="VI_TableBesoin2" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <asp:Label ID="VI_LabelTypeBesoin2" runat="server" Height="30px" Width="740px"
                               BackColor="Transparent" BorderColor="Black" BorderStyle="None" Text="Formations à l'évolution des métiers"
                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteStage2" runat="server" Height="50px" Width="220px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                               Text="Intitulé du stage"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEntetePeriode2" runat="server" Height="50px" Width="120px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Période souhaitée"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteObjectif2" runat="server" Height="50px" Width="240px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteAvis2" runat="server" Height="50px" Width="145px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVD03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVD04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVD05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVD06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis1" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif21" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHD15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVE03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVE04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVE05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVE06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis1" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif22" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHE15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVF03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVF04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVF05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVF06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis1" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif23" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHF15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                      <asp:Table ID="VI_TableBesoin3" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <asp:Label ID="VI_LabelTypeBesoin3" runat="server" Height="30px" Width="740px"
                               BackColor="Transparent" BorderColor="Black" BorderStyle="None" Text="Formations de développement ou d'acquisition de qualifications nouvelles"
                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteStage3" runat="server" Height="50px" Width="220px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                               Text="Intitulé du stage"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEntetePeriode3" runat="server" Height="50px" Width="120px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Période souhaitée"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteObjectif3" runat="server" Height="50px" Width="240px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteAvis3" runat="server" Height="50px" Width="145px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVG03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVG04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVG05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVG06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis1" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif31" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHG15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVH03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVH04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVH05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVH06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis1" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif32" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHH15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVI03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVI04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVI05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVI06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis1" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif33" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHI15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableFooterRow>
                    <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
                        <asp:Label ID="NumeroPage13" runat="server" Height="15px" Width="150px"
                             BackColor="Transparent" BorderStyle="None"
                             Text="13 / 16" ForeColor="Black" Font-Italic="True"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                             style="text-align:left" >
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell >
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitre22" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderStyle="None" Text="6.2.2 Expression des autres besoins"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 7px;
                           font-style: normal; text-indent: 20px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                      <asp:Table ID="VI_TableBesoin4" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <asp:Label ID="VI_LabelTypeBesoin4" runat="server" Height="30px" Width="740px"
                               BackColor="Transparent" BorderColor="Black" BorderStyle="None" Text="Préparation aux concours ou examen professionnel"
                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteStage4" runat="server" Height="50px" Width="220px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                               Text="Intitulé du concours ou de l'examen professionnel"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEntetePeriode4" runat="server" Height="50px" Width="120px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Période souhaitée"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteObjectif4" runat="server" Height="50px" Width="240px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteAvis4" runat="server" Height="50px" Width="145px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVJ03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVJ04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVJ05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVJ06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis1" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif41" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHJ15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVK03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVK04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVK05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVK06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis1" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif42" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHK15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVL03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVL04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVL05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black"
                               DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVL06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis1" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif43" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHL15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                      <asp:Table ID="VI_TableBesoin5" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <asp:Label ID="VI_LabelTypeBesoin5" runat="server" Height="30px" Width="740px"
                               BackColor="Transparent" BorderColor="Black" BorderStyle="None" Text="Validation des acquis de l'expérience"
                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteStage5" runat="server" Height="50px" Width="220px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                               Text="Qualification recherchée"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEntetePeriode5" runat="server" Height="50px" Width="120px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Période souhaitée"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteObjectif5" runat="server" Height="50px" Width="240px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteAvis5" runat="server" Height="50px" Width="145px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVM03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black" 
                               DonWidth="218px" DonHeight="60px" DonTabIndex="19" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVM04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black" 
                               DonWidth="118px" DonHeight="60px" DonTabIndex="20" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVM05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black" 
                               DonWidth="238px" DonHeight="60px" DonTabIndex="21" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVM06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis5" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif5" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHM15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>   
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                      <asp:Table ID="VI_TableBesoin6" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                            <asp:Label ID="VI_LabelTypeBesoin6" runat="server" Height="30px" Width="740px"
                               BackColor="Transparent" BorderColor="Black" BorderStyle="None" Text="Bilan de compétences"
                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEntetePeriode6" runat="server" Height="50px" Width="120px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Période souhaitée"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteObjectif6" runat="server" Height="50px" Width="464px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteAvis6" runat="server" Height="50px" Width="145px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVN04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black" 
                               DonWidth="118px" DonHeight="60px" DonTabIndex="23" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVN05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black" 
                               DonWidth="462px" DonHeight="60px" DonTabIndex="24" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVN06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis6" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="1">
                            <asp:Label ID="VI_EtiLabelMotif6" runat="server" Height="18px" Width="105px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHN15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>   
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                      <asp:Table ID="VI_TableBesoin7" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="5">
                            <asp:Label ID="VI_LabelTypeBesoin7" runat="server" Height="30px" Width="740px"
                               BackColor="Transparent" BorderColor="Black" BorderStyle="None" Text="Période de professionnalisation"
                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteStage7" runat="server" Height="50px" Width="145px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                               Text="Emploi envisagé"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEntetePeriode7" runat="server" Height="50px" Width="120px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Durée et période souhaitée"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteObjectif7" runat="server" Height="50px" Width="195px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteEcheance7" runat="server" Height="50px" Width="120px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Echéance souhaitée pour le projet"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteAvis7" runat="server" Height="50px" Width="142px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVO03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black" 
                               DonWidth="143px" DonHeight="60px" DonTabIndex="26" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVO04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black" 
                               DonWidth="118px" DonHeight="60px" DonTabIndex="27" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVO05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black" 
                               DonWidth="193px" DonHeight="60px" DonTabIndex="28" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVO08" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black" 
                               DonWidth="118px" DonHeight="60px" DonTabIndex="29" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVO06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis7" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif7" runat="server" Height="18px" Width="255px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                            <asp:Label ID="VI_InfoHO15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>   
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                      <asp:Table ID="VI_TableBesoin8" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <asp:Label ID="VI_LabelTypeBesoin8" runat="server" Height="30px" Width="740px"
                               BackColor="Transparent" BorderColor="Black" BorderStyle="None" Text="Congé de formation professionnelle"
                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteStage8" runat="server" Height="50px" Width="220px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                               Text="Formation envisagée"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEntetePeriode8" runat="server" Height="50px" Width="120px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Durée et période souhaitée"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteObjectif8" runat="server" Height="50px" Width="240px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteAvis8" runat="server" Height="50px" Width="145px"
                               BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVP03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black" 
                               DonWidth="218px" DonHeight="60px" DonTabIndex="31" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVP04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black" 
                               DonWidth="118px" DonHeight="60px" DonTabIndex="32" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVP05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                               Etivisible="False" DonBorderColor="Black" 
                               DonWidth="238px" DonHeight="60px" DonTabIndex="33" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="VI_RadioVP06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvis8" RadioDroiteVisible="false"
                               RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                               RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_EtiLabelMotif8" runat="server" Height="18px" Width="330px"
                                BackColor="Transparent" BorderStyle="None" Text="en cas d'avis défavorable, en préciser le motif :"
                                ForeColor="Black" Font-Italic="True"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: italic; text-indent: 2px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="VI_InfoHP15507" runat="server" Height="18px" Width="390px"
                                BackColor="Transparent" BorderStyle="Notset" Text=""
                                ForeColor="Black" Font-Italic="False" BorderColor="Black"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style:  normal; text-indent: 1px; text-align: left;">
                            </asp:Label>          
                          </asp:TableCell>   
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage14" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="14 / 16" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>