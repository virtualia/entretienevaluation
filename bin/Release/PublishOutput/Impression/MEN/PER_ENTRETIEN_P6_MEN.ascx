﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P6_MEN.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P6_MEN" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="VI_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreTitreFormation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVoletEntete" runat="server" Height="20px" Width="1px" Text="" 
                            CssClass="EP_Imp_LargerBold" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVolet" runat="server" Height="20px" Width="746px"
                            Text="COMPTE RENDU D'ENTRETIEN DE FORMATION" 
                            CssClass="EP_Imp_LargerBold" style="margin-top: 1px; text-indent: 1px;text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVoletSuite" runat="server" Text="" Height="20px" Width="1px"
                            CssClass="EP_Imp_SmallItalic" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Label ID="VI_LabelTitreCommentaires" runat="server" Height="20px" Width="746px"
                           Text="(partie détachable à transmettre au service formation)"
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="VI_LabelAnneeEntretien" runat="server" Height="20px" Width="100px" Text=""
                           CssClass="EP_Imp_MediumBold" style="margin-top: 5px; margin-left: 1px; margin-bottom: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow> 
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreEnteteCompteRendu" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="VI_CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="VI_LabelTitreAgent" runat="server" Height="20px" Width="150px" Text="Entre l'agent"
                                    CssClass="EP_Imp_MediumNormal" 
                                    style="margin-top: 5px; margin-left: 1px; text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelIdentite" runat="server" Height="20px" Width="150px" Text="Nom d'usage :"
                                     CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_LabelIdentite" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_MediumNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelPatronyme" runat="server" Height="20px" Width="150px" Text="Nom de famille :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_LabelPatronyme" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_MediumNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelNaissance" runat="server" Height="20px" Width="150px" Text="Date de naissance :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_LabelNaissance" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelStatut" runat="server" Height="20px" Width="150px" Text="Statut :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_LabelStatut" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelGrade" runat="server" Height="20px" Width="150px" Text="Corps - grade :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_LabelGrade" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="VI_LabelTitreManager" runat="server" Height="20px" Width="280px" Text="Et son supérieur hiérarchique direct"
                                    CssClass="EP_Imp_MediumNormal" 
                                    style="margin-top: 12px; margin-left: 1px; margin-bottom: 5px; text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelManager" runat="server" Height="20px" Width="150px" Text="Nom et prénom :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_InfoH15001" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelGradeManager" runat="server" Height="20px" Width="150px" Text="Corps - grade :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_LabelGradeManager" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelFonctionManager" runat="server" Height="20px" Width="150px" Text="Intitulé de la fonction :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_LabelFonctionManager" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_EtiLabelDirectionManager" runat="server" Height="20px" Width="150px" Text="Structure :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="VI_InfoH15002" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>  
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="VI_CadreEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="VI_EtiLabelDateEntretien" runat="server" Height="20px" Width="375px"
                                    Text="Date de l'entretien de formation :"
                                    CssClass="EP_Imp_MediumNormal" style="text-indent: 1px; text-align: right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="VI_InfoH15021" runat="server" Height="20px" Width="375px" Text=""
                                    CssClass="EP_Imp_MediumNormal" style="text-indent: 3px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreBilanFormation" runat="server" Height="25px" Width="746px"
                           Text="Bilan des formations et besoins de formation"
                           CssClass="EP_Imp_LargerBoldSouligne" style="margin-top: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreNumero11" runat="server" Height="20px" Width="746px"
                           Text="1. Bilan des formations suivies sur la période écoulée"
                           CssClass="EP_Imp_MediumBold" style="text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="VI_ListeFormationsSuivies" runat="server" CadreWidth="746px" SiColonneSelect="false"
                             SiAppliquerCharte="false"
                             BackColorCaption="White" ForeColorCaption="Black" BackColorRow="White" ForeColorRow="Black"/>
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreNumero12" runat="server" Height="20px" Width="746px"
                           Text="Commentaire sur les formations suivies et leur mise en oeuvre dans le poste"
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVR03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="746px" DonHeight="190px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="false"
                           Donstyle="margin-left: 1px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage11" runat="server" Height="15px" Width="250px"
                BackColor="Transparent" BorderStyle="None"
                Text="11 / 13" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:right" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell Height="15px">
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VI_CadreEnteteDePage2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelEnteteLigne21" runat="server" Height="20px" Width="748px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelEnteteLigne22" runat="server" Height="20px" Width="748px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreNumero21" runat="server" Height="20px" Width="746px"
                           Text="2. Compétences à acquérir ou développer pour tenir le poste"
                           CssClass="EP_Imp_MediumBold" style="text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="VI_TableBesoin102" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="Transparent" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteStage102" runat="server" Height="20px" Width="390px" Text="Intitulé"
                               CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEntetePeriode102" runat="server" Height="20px" Width="350px" Text="Périodes souhaitées"
                               CssClass="EP_Imp_SmallNormal" style="text-indent: 1px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVA01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" 
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVA04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="345px" DonHeight="36px"
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>        
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVB01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" 
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">        
                              <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVB04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="345px" DonHeight="36px"
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>               
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVC01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" 
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">        
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVC04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" 
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>  
                          </asp:TableCell>                           
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVD01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" 
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVD04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" 
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVE01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px"
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVE04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="345px" DonHeight="36px"
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <asp:Label ID="VI_LabelSiFormationUrgente" runat="server" Height="20px" Width="385px"
                                Text="Une action de formation permettant d'acquérir ou de développer ces compétences doit-elle être suivie rapidement ?"
                                CssClass="EP_Imp_SmallNormal" style="margin-left: 1px;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left">        
                            <Virtualia:VSixBoutonRadio ID="VI_RadioH29" runat="server" V_Groupe="SiFormationUrgente"
                                V_PointdeVue="1" V_Objet="150" V_Information="29" V_SiDonneeDico="false"
                                VRadioN1Width="80px" VRadioN2Width="80px" VRadioN3Width="0px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px" VRadioN2Style="margin-left: 0px; margin-top: 0px" VRadioN3Style="margin-left: 0px; margin-top: 0px"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px" VRadioN5Style="margin-left: 0px; margin-top: 0px" VRadioN6Style="margin-left: 0px; margin-top: 0px" 
                                VRadioN1Text="Non" VRadioN2Text="Oui"
 				                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           	    VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                                VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell> 
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreNumero22" runat="server" Height="20px" Width="746px"
                           Text="3. Compétences à acquérir ou développer en vue d'une évolution professionnelle"
                           CssClass="EP_Imp_MediumBold"                           
                           style="margin-top: 10px; margin-bottom: 2px; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreNumero23" runat="server" Height="20px" Width="746px"
                           Text="(à compléter en fonction des perspectives d'évolution professionnelle)"
                           CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="VI_TableBesoin103" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="Transparent" BorderWidth="1px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="VI_LabelEnteteStage103" runat="server" Height="20px" Width="390px" Text="Intitulé"
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="VI_LabelEntetePeriode103" runat="server" Height="20px" Width="350px" Text="Echéances envisagées"
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVF01" runat="server" DonTextMode="true"
                                    V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="False"
                                    Etivisible="False" DonWidth="385px" DonHeight="56px" 
                                    Donstyle="margin-left: 1px; margin-bottom: 0px;"
                                    DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                            <asp:TableCell  HorizontalAlign="Center">        
                                <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVF04" runat="server" DonTextMode="true"
                                    V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="False"
                                    Etivisible="False" DonWidth="345px" DonHeight="36px"
                                    Donstyle="margin-left: 1px; margin-bottom: 0px;"
                                    DonBorderWidth="1px" DonBorderColor="Black"/> 
                            </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVG01" runat="server" DonTextMode="true"
                                    V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                                    Etivisible="False" DonWidth="385px" DonHeight="56px" 
                                    Donstyle="margin-left: 1px; margin-bottom: 0px;"
                                    DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                            <asp:TableCell  HorizontalAlign="Center">        
                                <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVG04" runat="server" DonTextMode="true"
                                    V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                                    Etivisible="False" DonWidth="345px" DonHeight="36px"
                                    Donstyle="margin-left: 1px; margin-bottom: 0px;"
                                    DonBorderWidth="1px" DonBorderColor="Black"/> 
                            </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVH01" runat="server" DonTextMode="true"
                                    V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                                    Etivisible="False" DonWidth="385px" DonHeight="56px"
                                    Donstyle="margin-left: 1px; margin-bottom: 0px;"
                                    DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                            <asp:TableCell  HorizontalAlign="Center">        
                                <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVH04" runat="server" DonTextMode="true"
                                    V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                                    Etivisible="False" DonWidth="345px" DonHeight="36px" 
                                    Donstyle="margin-left: 1px; margin-bottom: 0px;"
                                    DonBorderWidth="1px" DonBorderColor="Black"/> 
                            </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVI01" runat="server" DonTextMode="true"
                                    V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                                    Etivisible="False" DonWidth="385px" DonHeight="56px" 
                                    Donstyle="margin-left: 1px; margin-bottom: 0px;"
                                    DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                            <asp:TableCell  HorizontalAlign="Center">        
                                <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVI04" runat="server" DonTextMode="true"
                                    V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                                    Etivisible="False" DonWidth="345px" DonHeight="36px"
                                    Donstyle="margin-left: 1px; margin-bottom: 0px;"
                                    DonBorderWidth="1px" DonBorderColor="Black"/> 
                            </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVJ01" runat="server" DonTextMode="true"
                                    V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                                    Etivisible="False" DonWidth="385px" DonHeight="56px" 
                                    Donstyle="margin-left: 1px; margin-bottom: 0px;"
                                    DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                            <asp:TableCell  HorizontalAlign="Center">        
                                <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVJ04" runat="server" DonTextMode="true"
                                    V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                                    Etivisible="False" DonWidth="345px" DonHeight="36px"
                                    Donstyle="margin-left: 1px; margin-bottom: 0px;"
                                    DonBorderWidth="1px" DonBorderColor="Black"/> 
                            </asp:TableCell>                            
                        </asp:TableRow>
                     </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableFooterRow>
                    <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
                        <asp:Label ID="NumeroPage12" runat="server" Height="15px" Width="250px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="12 / 13" ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                            style="text-align:right" >
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px">
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="VI_CadreEnteteDePage3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_LabelEnteteLigne31" runat="server" Height="20px" Width="748px" Text=""
                                        CssClass="EP_Imp_SmallNormal" style="text-indent: 4px;">
                                    </asp:Label>          
                                </asp:TableCell>           
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_LabelEnteteLigne32" runat="server" Height="20px" Width="748px" Text=""
                                        CssClass="EP_Imp_SmallNormal" style="text-indent: 4px;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreNumero24" runat="server" Height="20px" Width="746px"
                           Text="4. Autres perspectives de formation"
                           CssClass="EP_Imp_MediumBold" style="margin-bottom: 2px; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="VI_TableBesoin104" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="Transparent" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEnteteStage104" runat="server" Height="20px" Width="390px" Text="Intitulé"
                               CssClass="EP_Imp_SmallNormal" style="text-indent: 1px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="VI_LabelEntetePeriode104" runat="server" Height="20px" Width="350px"
                               Text="Echéances envisagées et durée prévue"
                               CssClass="EP_Imp_SmallNormal" style="text-indent: 1px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVK01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" 
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVK04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="345px" DonHeight="36px"
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVL01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px"
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVL04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" 
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVM01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px"
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVM04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" 
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVN01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px"
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVN04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" 
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVO01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px"
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVO04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" 
                               Donstyle="margin-left: 1px; margin-bottom: 0px;"
                               DonBorderWidth="1px" DonBorderColor="Black"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>                
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_TableDIF" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="VI_LabelTitre105" runat="server" Height="20px" Width="746px"
                           Text="5. Utilisation du droit individuel à la formation (DIF)"
                           CssClass="EP_Imp_MediumBold" style="margin-bottom: 3px; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreDIF" runat="server" Height="20px" Width="340px"
                            Text="Solde du DIF au 1er janvier de l'année en cours :"
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelSoldeDIF" runat="server" Height="20px" Width="400px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 2px;">
                        </asp:Label>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="VI_LabelSiUtilisationDIF" runat="server" Height="20px" Width="340px"
                            Text="L'agent envisage-t-il de mobiliser son DIF cette année ? :"
                             CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                         </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                       <Virtualia:VSixBoutonRadio ID="VI_RadioH19" runat="server" V_Groupe="SiUtilisationDIF"
                           V_PointdeVue="1" V_Objet="150" V_Information="19" V_SiDonneeDico="false"
                           VRadioN1Width="80px" VRadioN2Width="80px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px" VRadioN2Style="margin-left: 0px; margin-top: 0px" VRadioN3Style="margin-left: 0px; margin-top: 0px"
                           VRadioN4Style="margin-left: 0px; margin-top: 0px" VRadioN5Style="margin-left: 0px; margin-top: 0px" VRadioN6Style="margin-left: 0px; margin-top: 0px" 
                           VRadioN1Text="Non" VRadioN2Text="Oui" 
                           VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"
                           VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                           Visible="true"/>
                    </asp:TableCell> 
                </asp:TableRow>         
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreSignaturesBilanFormation" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="4"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignatureNomAgent" runat="server" Height="20px" Width="250px" Text="NOM de l'agent"
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignatureFonctionAgent" runat="server" Height="20px" Width="250px" Text=""
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignatureAgent" runat="server" Height="20px" Width="150px" Text="Signature"
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignatureDateAgent" runat="server" Height="20px" Width="98px" Text="Date"
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="4"></asp:TableCell>
                </asp:TableRow>
          	    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_SignatureNomAgent" runat="server" Height="20px" Width="250px" Text=""
                           CssClass="EP_Imp_SmallBold" style="margin-bottom: 1px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_SignatureFonctionAgent" runat="server" Height="20px" Width="250px" Text=""
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_SignatureAgent" runat="server" Height="20px" Width="150px" Text=""
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
			            <asp:Label ID="VI_InfoH15020" runat="server" Height="20px" Width="98px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell Height="30px" ColumnSpan="4"></asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignatureNomManager" runat="server" Height="20px" Width="250px"
                           Text="NOM du supérieur hiérarchique"
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignatureFonctionManager" runat="server" Height="20px" Width="250px" 
                           Text="Fonction"
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignatureManager" runat="server" Height="20px" Width="150px"
                           Text="Signature"
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignatureDateManager" runat="server" Height="20px" Width="98px" Text="Date"
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="4"></asp:TableCell>
                </asp:TableRow>
          	    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_SignatureNomManager" runat="server" Height="20px" Width="250px" Text=""
                           CssClass="EP_Imp_SmallBold" style="margin-bottom: 1px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_SignatureFonctionManager" runat="server" Height="20px" Width="250px" Text=""
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_SignatureManager" runat="server" Height="20px" Width="150px" Text=""
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
			            <asp:Label ID="VI_Label15021" runat="server" Height="20px" Width="98px" Text=""
                           CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="4"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage14" runat="server" Height="15px" Width="250px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="13 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:right">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>