﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P6_CNED.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P6_CNED" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="VI_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VI_CadreEnteteDePage1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelEnteteLigne11" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 4px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelEnteteLigne12" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 4px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreTitreFormation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVoletEntete" runat="server" Text="10 - " 
                            Height="20px" Width="45px"
                            BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger" Font-Underline="false"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVolet" runat="server" 
                            Text="Bilan des formations et besoins de formation" 
                            Height="20px" Width="666px"
                            BackColor="Transparent"  BorderStyle="None"
                            ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger" Font-Underline="true"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVoletSuite" runat="server" Text="" Height="20px" Width="35px"
                            BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: oblique; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Label ID="VI_LabelTitreCommentaires" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="(partie détachable à transmettre au service formation)"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow> 
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreNumero11" runat="server" Height="40px" Width="746px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="None" 
                           Text="10.1 Bilan des formations suivies sur les quatre dernières années"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="VI_ListeFormationsSuivies" runat="server" CadreWidth="746px" SiColonneSelect="false"
                             SiAppliquerCharte="false"
                             BackColorCaption="White" ForeColorCaption="Black" BackColorRow="White" ForeColorRow="Black"/>
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreNumero12" runat="server" Height="30px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" 
                           Text="Commentaire sur les formations suivies et leur mise en oeuvre dans le poste"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVW03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="746px" DonHeight="190px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="false"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage11" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="11 / 13" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell Height="15px">
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VI_CadreEnteteDePage2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelEnteteLigne21" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 4px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelEnteteLigne22" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 4px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreNumero2" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="None" 
                           Text="10.2 Compétence à acquérir ou développer pour tenir le poste"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                      <asp:Table ID="VI_TableBesoin102" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="Black" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <asp:Label ID="VI_LabelEnteteStage102" runat="server" Height="20px" Width="390px"
                               BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                               Text=""
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Left">
                            <asp:Label ID="VI_LabelEntetePeriode102" runat="server" Height="20px" Width="350px"
                               BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px" 
                               Text="Echéances envisagées"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVA01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="2" 
                               Donstyle="margin-left: 1px;"
                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="VI_RadioHA08" runat="server" V_Groupe="EcheanceA"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"
                                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"   
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVB01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="3" 
                               Donstyle="margin-left: 1px;"
                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="VI_RadioHB08" runat="server" V_Groupe="EcheanceB"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"
                                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"      
                                Visible="true"/>
                          </asp:TableCell>               
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVC01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="4" 
                               Donstyle="margin-left: 1px;"
                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="VI_RadioHC08" runat="server" V_Groupe="EcheanceC"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"
                                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                                Visible="true"/>
                          </asp:TableCell>                           
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVD01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="5" 
                               Donstyle="margin-left: 1px;"
                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="VI_RadioHD08" runat="server" V_Groupe="EcheanceD"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"
                                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                      <asp:Table ID="VI_TableBesoin103" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="Black" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <asp:Label ID="VI_LabelTypeBesoin103" runat="server" Height="20px" Width="746px"
                               BackColor="Transparent" BorderColor="Black" BorderStyle="None" 
                               Text="10.3 Compétences à acquérir ou développer en vue d'une évolution professionnelle"
                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 2px; text-align:  left;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <asp:Label ID="VI_LabelTypeBesoin103Bis" runat="server" Height="20px" Width="746px"
                               BackColor="Transparent" BorderColor="Black" BorderStyle="None" 
                               Text="(à compléter en fonction des perspectives d'évolution professionnelle)"
                               BorderWidth="1px" ForeColor="Black" 
                               Font-Names="Trebuchet MS" Font-Size= "80%" Font-Italic="True" Font-Bold="False" 
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: italic; text-indent: 3px; text-align:  left;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <asp:Label ID="VI_LabelEnteteStage103" runat="server" Height="20px" Width="390px"
                               BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                               Text=""
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Left">
                            <asp:Label ID="VI_LabelEntetePeriode103" runat="server" Height="20px" Width="350px"
                               BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px" 
                               Text="Echéances envisagées"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVE01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="6" 
                               Donstyle="margin-left: 1px;"
                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="VI_RadioHE08" runat="server" V_Groupe="EcheanceE"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"
                                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVF01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="7" 
                               Donstyle="margin-left: 1px;"
                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="VI_RadioHF08" runat="server" V_Groupe="EcheanceF"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"
                                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                                Visible="true"/>
                          </asp:TableCell>               
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVG01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="8" 
                               Donstyle="margin-left: 1px;"
                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="VI_RadioHG08" runat="server" V_Groupe="EcheanceG"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"
                                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                                Visible="true"/>
                          </asp:TableCell>                           
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVH01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="9" 
                               Donstyle="margin-left: 1px;"
                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="VI_RadioHH08" runat="server" V_Groupe="EcheanceH"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"
                                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableFooterRow>
                    <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
                        <asp:Label ID="NumeroPage12" runat="server" Height="15px" Width="150px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="12 / 13" ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                            style="text-align:left" >
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px">
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="VI_CadreEnteteDePage3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_LabelEnteteLigne31" runat="server" Height="20px" Width="748px"
                                        BackColor="Transparent" BorderStyle="None" Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>           
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_LabelEnteteLigne32" runat="server" Height="20px" Width="748px"
                                        BackColor="Transparent" BorderStyle="None" Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                      <asp:Table ID="VI_TableBesoin104" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="Black" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <asp:Label ID="VI_LabelTypeBesoin104" runat="server" Height="20px" Width="746px"
                               BackColor="Transparent" BorderColor="Black" BorderStyle="None" Text="10.4 Autres perspectives de formation"
                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 2px; text-align:  left;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <asp:Label ID="VI_LabelEnteteStage104" runat="server" Height="20px" Width="390px"
                               BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                               Text=""
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Left">
                            <asp:Label ID="VI_LabelEntetePeriode104" runat="server" Height="20px" Width="350px"
                               BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px" 
                               Text="Echéances envisagées"
                               ForeColor="Black" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVI01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="10" 
                               Donstyle="margin-left: 1px;"
                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="VI_RadioHI08" runat="server" V_Groupe="EcheanceI"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"
                                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVJ01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="11" 
                               Donstyle="margin-left: 1px;"
                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="VI_RadioHJ08" runat="server" V_Groupe="EcheanceJ"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"
                                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                                Visible="true"/>
                          </asp:TableCell>               
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVK01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="12" 
                               Donstyle="margin-left: 1px;"
                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="VI_RadioHK08" runat="server" V_Groupe="EcheanceK"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"
                                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                                Visible="true"/>
                          </asp:TableCell>                           
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVL01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="false"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="13" 
                               Donstyle="margin-left: 1px;"
                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                               DonBorderWidth="1px" DonBorderColor="Black"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="VI_RadioHL08" runat="server" V_Groupe="EcheanceL"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="false"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 70%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 70%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"
                                VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                                VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_TableDIF" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="VI_LabelTitre105" runat="server" Height="30px" Width="746px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="None" 
                           Text="10.5 Utilisation du droit individuel à la formation (DIF)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 10px;
                           font-style: normal; text-indent: 2px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelTitreDIF" runat="server" Height="30px" Width="340px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Solde du DIF au 1er janvier de l'année en cours :"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelSoldeDIF" runat="server" Height="30px" Width="400px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px" 
                            Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="VI_LabelSiUtilisationDIF" runat="server" Height="25px" Width="340px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" 
                            Text="L'agent envisage-t-il de mobiliser son DIF cette année ?"
                            BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                       <Virtualia:VSixBoutonRadio ID="VI_RadioHM19" runat="server" V_Groupe="SiUtilisationDIF"
                           V_PointdeVue="1" V_Objet="150" V_Information="19" V_SiDonneeDico="false"
                           VRadioN1Width="80px" VRadioN2Width="80px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                           VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px" VRadioN2Style="margin-left: 0px; margin-top: 0px" VRadioN3Style="margin-left: 0px; margin-top: 0px"
                           VRadioN4Style="margin-left: 0px; margin-top: 0px" VRadioN5Style="margin-left: 0px; margin-top: 0px" VRadioN6Style="margin-left: 0px; margin-top: 0px" 
                           VRadioN1Text="Non" VRadioN2Text="Oui" 
                           VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"
                           VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"         
                           Visible="true"/>
                    </asp:TableCell> 
                </asp:TableRow>         
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage14" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="13 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>