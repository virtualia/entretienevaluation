﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P4_AFB.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P4_AFB" %>

<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>

<div style="page-break-before: always;"></div>

<asp:Table ID="IV_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="IV_CadreTitreEvolution" runat="server" Height="30px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelVolet" runat="server" Text="4 - Projet professionnel de l'agent" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"
                            BorderWidth="1px" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            Style="margin-top: 1px; margin-left: 1px; margin-bottom: 10px; font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table6" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="Transparent" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow BackColor="#DFF2FF">
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="Label6" runat="server" Height="20px" Width="730px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Souhaits de l'agent"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign ="top">
                        <asp:Label ID="Label7" runat="server" Height="115px" Width="230px"
                            BackColor="Transparent" BorderStyle="Solid" BorderColor="Black" BorderWidth="1px"
                            Text=" </br>  </br>  </br>  Évolution professionnelle envisagée"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 5px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVQ03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="110px" DonTabIndex="17"
                            DonStyle="margin-left: 1px;" EtiWidth ="0px"  
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" verticalAlign ="top">
                        <asp:Label ID="Label9" runat="server" Height="115px" Width="230px"
                            BackColor="Transparent" BorderStyle="Solid" BorderColor ="Black" BorderWidth ="1px"
                            Text=" </br>  </br>  </br>  Mobilité fonctionnelle ou géographique"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%" 
                            Style="margin-top: 0px; margin-left: 5px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                            </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVR03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="110px" DonTabIndex="19"
                            DonStyle="margin-left: 1px;" EtiWidth ="0px"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left"  VerticalAlign ="top">
                        <asp:Label ID="Label10" runat="server" Height="115px" Width="230px"
                            BackColor="Transparent" BorderStyle="Solid" BorderColor ="Black" BorderWidth ="1px"
                            Text=" </br>  </br>  </br>  Mobilité interne ou externe"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%" 
                            Style="margin-top: 0px; margin-left: 5px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                            </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVS03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="110px" DonTabIndex="21"
                            DonStyle="margin-left: 1px;" EtiWidth ="0px"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left"  VerticalAlign ="top">
                        <asp:Label ID="Label11" runat="server" Height="115px" Width="230px"
                            BackColor="Transparent" BorderStyle="Solid" BorderColor ="Black" BorderWidth ="1px"
                            Text=" </br>  </br> Souhait d'un entretien de carrière </br> <smaller>(après 5 ans d'ancienneté)</smaller>"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%" 
                            Style="margin-top: 0px; margin-left: 5px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                            </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVK03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="110px" DonTabIndex="23"
                            DonStyle="margin-left: 1px;" EtiWidth ="0px"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left"  VerticalAlign ="top">
                        <asp:Label ID="Label12" runat="server" Height="115px" Width="230px"
                            BackColor="Transparent" BorderStyle="Solid" BorderColor ="Black" BorderWidth ="1px"
                            Text=" </br>  </br> Souhait d'un bilan de carrière </br> <smaller>(après 15 ans d'ancienneté)</smaller>"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%" 
                            Style="margin-top: 0px; margin-left: 5px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                            </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVL03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="110px" DonTabIndex="23"
                            DonStyle="margin-left: 1px;" EtiWidth ="0px"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left"  VerticalAlign ="top">
                        <asp:Label ID="Label13" runat="server" Height="115px" Width="230px"
                            BackColor="Transparent" BorderStyle="Solid" BorderColor ="Black" BorderWidth ="1px"
                            Text=" </br>  </br>  </br> Autres"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%" 
                            Style="margin-top: 0px; margin-left: 5px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                            </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVM03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="110px" DonTabIndex="23"
                            DonStyle="margin-left: 1px;" EtiWidth ="0px"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

        <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="III_CadreNumero2_1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelContexte" runat="server" Height="18px" Width="748px"
                            BackColor="#DFF2FF" BorderStyle="None"
                            Text="Avis du responsable hiérarchique direct"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVN03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="743px" DonHeight="90px" DonTabIndex="7"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Transparent" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="Label8" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="12 / 18" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                Style="text-align: left">
            </asp:Label>
        </asp:TableCell>
    </asp:TableFooterRow>

</asp:Table>
