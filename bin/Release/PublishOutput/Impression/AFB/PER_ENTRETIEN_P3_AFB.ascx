﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P3_AFB.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P3_AFB" %>

<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>

<div style="page-break-before: always;"></div>

<asp:Table ID="III_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="III_CadreTitreObjectifs" runat="server" Height="30px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelVolet" runat="server" Text="3 - DEFINITION DES OBJECTIFS" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"
                            BorderWidth="1px" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            Style="margin-top: 1px; margin-left: 1px; margin-bottom: 10px; font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="III_CadreNumero2_1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="I_LabelContexte" runat="server" Height="18px" Width="748px"
                            BackColor="#DFF2FF" BorderStyle="None"
                            Text="CONTEXTE PRÉVISIBLE DE L'ANNÉE"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                        <asp:Label ID="I_LabelContexte_2" runat="server" Height="18px" Width="748px"
                            BackColor="#DFF2FF" BorderStyle="None"
                            Text="(Politique, environnement, réorganisation, moyens, objectifs du service dans lesquels s'inscrivent ceux de l'agent...)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="50%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVD03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="743px" DonHeight="100px" DonTabIndex="1"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Transparent" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
          </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="III_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="III_LabelNumerobjectif1" runat="server" Height="30px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°1"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteObjectifs1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteObjectif1" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVB02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="120px" DonTabIndex="2"
                            EtiBorderStyle="None" EtiBackColor="Transparent"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller;"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="0px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteIndicateur1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteIndicateur1" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Résultats attendus et critères d'atteinte des objectifs"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVB05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="3"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="III_LabelNumerobjectif2" runat="server" Height="30px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°2"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteObjectifs2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteObjectif2" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVC02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="120px" DonTabIndex="4"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="0px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteIndicateur2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteIndicateur2" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Résultats attendus et critères d'atteinte des objectifs"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVC05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="5"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
                        <asp:Label ID="NumeroPage6" runat="server" Height="15px" Width="150px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="8 / 18" ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                            Style="text-align: left">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                         <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="III_LabelNumerobjectif3" runat="server" Height="30px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°3"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteObjectifs3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteObjectif3" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVD02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="6"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="0px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteIndicateur3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteIndicateur3" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Résultats attendus et critères d'atteinte des objectifs"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVD05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="100px" DonTabIndex="7"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="III_LabelNumerobjectif4" runat="server" Height="30px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°4"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteObjectifs4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteObjectif4" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVE02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="8"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="0px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteIndicateur4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteIndicateur4" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Résultats attendus et critères d'atteinte des objectifs"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVE05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="100px" DonTabIndex="9"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="III_LabelNumerobjectif5" runat="server" Height="30px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°5"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteObjectifs5" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteObjectif5" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVF02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="10"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="0px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteIndicateur5" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteIndicateur5" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Résultats attendus et critères d'atteinte des objectifs"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVF05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="100px" DonTabIndex="11"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
                        <asp:Label ID="NumeroPage7" runat="server" Height="15px" Width="150px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="9 / 18" ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                            Style="text-align: left">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                         <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="III_LabelNumerobjectif6" runat="server" Height="30px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°6"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteObjectifs6" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteObjectif6" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVG02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="130px" DonTabIndex="12"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="0px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteIndicateur6" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteIndicateur6" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Résultats attendus et critères d'atteinte des objectifs"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVG05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="120px" DonTabIndex="13"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="III_LabelNumerobjectif7" runat="server" Height="30px" Width="748px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Objectif n°7"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteObjectifs7" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="III_LabelEnteteObjectif7" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVH02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="130px" DonTabIndex="14"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="0px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="III_TableEnteteIndicateur7" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label3" runat="server" Height="20px" Width="746px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Résultats attendus et critères d'atteinte des objectifs"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVH05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="744px" DonHeight="120px" DonTabIndex="15"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>

            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage8" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="10 / 18" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                Style="text-align: left">
            </asp:Label>
        </asp:TableCell>
    </asp:TableRow>


    <asp:TableRow>
        <asp:TableCell>
                         <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="Transparent" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow  BackColor="#DFF2FF">
                    <asp:TableCell HorizontalAlign="Center" >
                        <asp:Label ID="Label1" runat="server" Height="20px" Width="230px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="OBJECTIFS MANAGERIAUX"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Label2" runat="server" Height="20px" Width="500px"
                            BackColor="#DFF2FF" BorderStyle="None"
                            Text="Résultats attendus et critères d'atteinte des objectifs"
                            ForeColor="Black" Font-Italic="False"
                          Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVI02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="230px" DonHeight="130px" DonTabIndex="16"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVI05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="130px" DonTabIndex="17"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVJ02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="230px" DonHeight="130px" DonTabIndex="18"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVJ05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="130px" DonTabIndex="19"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVK02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="230px" DonHeight="130px" DonTabIndex="20"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVK05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="130px" DonTabIndex="21"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVL02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="230px" DonHeight="130px" DonTabIndex="22"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVL05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="500px" DonHeight="130px" DonTabIndex="23"
                            DonStyle="margin-left: 1px; margin-bottom: -12px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
                    <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
        <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="Label4" runat="server" Height="18px" Width="748px"
                            BackColor="#DFF2FF" BorderStyle="None"
                            Text="OBSERVATIONS ET COMMENTAIRES"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                        <asp:Label ID="Label5" runat="server" Height="18px" Width="748px"
                            BackColor="#DFF2FF" BorderStyle="None"
                            Text="(conditions de réussite: moyens, délais...)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="50%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVE03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="743px" DonHeight="100px" DonTabIndex="24"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Transparent" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
          </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
        <asp:TableRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage9" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="11 / 18" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                Style="text-align: left">
            </asp:Label>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
