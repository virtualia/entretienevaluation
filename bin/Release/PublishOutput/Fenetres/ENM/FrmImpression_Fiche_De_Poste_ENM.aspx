﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmImpression_Fiche_De_Poste_ENM.aspx.vb" Inherits="Virtualia.Net.FrmImpression_Fiche_De_Poste_ENM"  UICulture="Fr" %>

<%@ Register src="~/Impression/ENM/PER_FICHE_POSTE_P0_ENM.ascx" tagname="FP_P0" tagprefix="Virtualia" %>
<%@ Register src="~/Impression/ENM/PER_FICHE_POSTE_P1_ENM.ascx" tagname="FP_P1" tagprefix="Virtualia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Formulaire Entretien</title>
</head>
<body>
    <form id="FrmFiche_De_Poste" runat="server" style="max-width: 800px">
    <div style="margin-left : 10px">
        <asp:ImageButton ID="CommandePDF" runat="server" ImageUrl="~/Images/General/PDF_on.gif"
                     ToolTip="Editer le formulaire au format PDF" />
    </div>
    <div id="Div1" runat="server" style="width: 750px;">
        <asp:Table runat="server" ID="CadreApercu" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:FP_P0 ID="FICHE_DE_POSTE_0" runat="server"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:FP_P1 ID="FICHE_DE_POSTE_1" runat="server" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
    </div>
    </form>
</body>
</html>
