﻿<%@ Page Language="vb" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeBehind="FrmInitialisation.aspx.vb"
     Inherits="Virtualia.Net.FrmInitialisation" UICulture="fr" %>

<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="CadreValidation" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanelValide" runat="server">
        <ContentTemplate>
            <asp:Timer ID="HorlogeLogin" runat="server" Interval="1000">
            </asp:Timer>
            <asp:Table ID="CadreAttente" runat="server" Height="600px" Width="1150px" HorizontalAlign="Center"
                                BackColor="#124545" style="margin-top: 0px; margin-bottom: 0px; border-width: 4px; border-style: ridge;
                                border-color: #B0E0D7" >
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ForeColor="White" Height="60px">
                        <asp:UpdateProgress ID="UpdateAttente" runat="server">
                                <ProgressTemplate>
                                    <img alt="" src="../Images/General/Loading.gif" 
                                    style="vertical-align:top; text-align:center; font-family: 'Trebuchet MS';" 
                                    height="30px" /> 
                                    Chargement de l'application - Traitement en cours ... 
                                </ProgressTemplate>
                        </asp:UpdateProgress>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="200px" HorizontalAlign="Center" VerticalAlign="Top">
                        <div style="margin-top: 10px; background-color: transparent; text-align: center; 
                                font-style: oblique; color: White; width: 1000px;">
                            <h1>
                                Virtualia - Préparation
                            </h1>
                            <h1>
                                Extractions des données des formulaires d'entretien
                            </h1>
                            <h1>
                                veuillez patienter ...
                            </h1>
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                        <asp:Label ID="EtiTraitement" runat="server" Text="Traitement en cours..."
                                        Height="30px" Width="1000px" BackColor="Transparent" ForeColor="#D7FAF3"
                                        BorderColor="#B6C7E2"  BorderStyle="None" BorderWidth="1px"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Large" Font-Italic="true">
                            </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:HiddenField ID="HNumPage" runat="server" Value="0" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>     
</asp:Content>  