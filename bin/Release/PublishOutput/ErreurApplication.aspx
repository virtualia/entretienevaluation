﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ErreurApplication.aspx.vb" Inherits="Virtualia.Net.ErreurApplication" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Virtualia - Erreurs</title>
</head>
<body>
    <form id="VirtualiaErreur" runat="server">
    <div>
         <asp:Table ID="CadreDateJour" runat="server" Width="400px" Height="100px" HorizontalAlign="Center"
                    BorderColor="AppWorkspace" BorderStyle="Ridge">
             <asp:TableRow>
                <asp:TableCell> 
                    <asp:Label ID="EtiDateJour" runat="server" BackColor="Transparent" Text=""
                                Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#124545"
                                Height="30px" Width="200px" Font-Bold="False" Font-Italic="true"
                                style="text-align:center">
                    </asp:Label>
                </asp:TableCell>
             </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left"> 
                    <asp:Label ID="EtiMessage" runat="server" BackColor="Transparent" Text=""
                                Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#124545" 
                                Height="50px" Width="300px" Font-Bold="False" Font-Italic="true"
                                style="text-align:center">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
    </form>
</body>
</html>