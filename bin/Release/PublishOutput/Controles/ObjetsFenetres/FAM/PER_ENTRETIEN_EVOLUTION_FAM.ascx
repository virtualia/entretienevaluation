﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_EVOLUTION_FAM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_EVOLUTION_FAM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreEvolution" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="V. PERSPECTIVES D'EVOLUTION PROFESSIONNELLE DE L'AGENT" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations" runat="server" Height="50px" Width="746px"
                           BackColor="#98D4CA" BorderColor="#B0E0D7" BorderStyle="none"
                           Text="Souhaits d'évolution professionnelle de l'agent (précisez l'échéance)"
                           BorderWidth="1px" ForeColor="#0F2F30"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero1" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Evolution sur le poste actuel"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="210px" EtiHeight="50px" DonTabIndex="1" 
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="LabelSiAdaptation" runat="server" Height="24px" Width="348px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="Modification éventuelle de la fiche de poste"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell Width="400px" HorizontalAlign="Left">        
                       <Virtualia:VTrioHorizontalRadio ID="RadioH22" runat="server" V_Groupe="SiEncadrement"
                           V_PointdeVue="1" V_Objet="150" V_Information="22" V_SiDonneeDico="true"
                           RadioGaucheWidth="80px" RadioCentreWidth="80px" RadioDroiteWidth="0px"  
                           RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
                           RadioGaucheHeight="25px" RadioCentreHeight="25px"
                           RadioCentreStyle="text-align:  center;"  
                           RadioGaucheStyle="margin-left: 3px; text-align:  center;" Visible="true"/>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Prise de responsabilités plus importantes"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVR03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="210px" EtiHeight="60px" DonTabIndex="2"
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreProjet2" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Projet professionnel"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>               
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVS03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="210px" EtiHeight="20px" DonTabIndex="3"
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align:  left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero3" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Observations éventuelles du supérieur hiérarchique direct"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVT03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="210px" EtiHeight="30px" DonTabIndex="4"
                           Etitext="sur ces perspectives d'évolution professionnelle"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align:  left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                       <asp:Label ID="LabelTitreNumero4" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Commentaires éventuels de l'agent"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                       </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVU03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="210px" EtiHeight="30px" DonTabIndex="5"
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align:  left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>  
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow> 
 </asp:Table>