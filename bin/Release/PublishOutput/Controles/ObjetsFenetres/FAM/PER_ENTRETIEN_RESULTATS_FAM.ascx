﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_RESULTATS_FAM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_RESULTATS_FAM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderColor="#B0E0D7" 
    BorderStyle="None" BorderWidth="2px" HorizontalAlign="Center" 
    style="margin-top: 1px;" Visible="true" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreResultats" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="I. BILAN DE L'ANNEE ECOULEE" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero1" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Contexte de l'année écoulée"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="30px" DonTabIndex="1" 
                           Etitext="Environnement de travail de l'agent et objectifs du service sur l'année écoulée"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" 
                           Text="Objectifs"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectif" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteObjectif1" runat="server" Height="60px" Width="241px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Rappel de l'objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteRealisation" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                                <asp:Label ID="LabelEnteteRealisation" runat="server" Height="29px" Width="220px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Réalisation" 
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteComplete" runat="server" Height="30px" Width="49px"
                                   BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="objectif atteint"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px; 
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEntetePartielle" runat="server" Height="30px" Width="64px"
                                   BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="partiellement atteint"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px; 
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteInsuffisante" runat="server" Height="30px" Width="49px"
                                   BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="non atteint"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteSansObjet" runat="server" Height="30px" Width="53px"
                                   BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="devenu sans objet"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteDifficultes" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteDifficultes" runat="server" Height="60px" Width="265px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Analyse des résultats et enseignements tirés"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif1" runat="server" Height="20px" Width="251px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif n°1"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="246px" DonHeight="140px" DonTabIndex="2"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHC04" runat="server" V_Groupe="Realisation1"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHC05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="170px" DonTabIndex="3" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 8px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes1" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="270px" DonHeight="140px" DonTabIndex="4"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif2" runat="server" Height="20px" Width="251px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif n°2"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="246px" DonHeight="140px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHD04" runat="server" V_Groupe="Realisation2"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHD05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="170px" DonTabIndex="6" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 8px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes2" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="270px" DonHeight="140px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif3" runat="server" Height="20px" Width="251px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif n°3"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="246px" DonHeight="140px" DonTabIndex="8"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHE04" runat="server" V_Groupe="Realisation3"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHE05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="170px" DonTabIndex="9" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 8px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes3" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="270px" DonHeight="140px" DonTabIndex="10"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="30px" DonTabIndex="11" 
                           Etitext="Ce qui a facilité ou freiné les résultats"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero3" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Autres dossiers ou travaux sur lesquels l'agent s'est investi en cours d'année"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="30px" DonTabIndex="12" 
                           Etitext="Nature des dossiers ou travaux"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="30px" DonTabIndex="13" 
                           Etitext="Les résultats obtenus par l'agent"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero4" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Appréciation globale sur l'activité et la réalisation des objectifs de l'année écoulée"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero4Evaluateur" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Evaluateur"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="30px" DonTabIndex="14" 
                           Etitext="Appréciation de l'évaluateur"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="30px" DonTabIndex="15" 
                           Etitext="Eléments particuliers à prendre en compte"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                <asp:TableCell>
                    <asp:Table ID="CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="LabelConflit1" runat="server" Height="24px" Width="550px"
                                    BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="La situation de l'agent vis-à-vis du risque de conflit d'intérêt a été abordé lors de l'entretien"
                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  oblique; text-indent: 10px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell Width="220px" HorizontalAlign="Left">        
                               <Virtualia:VTrioHorizontalRadio ID="RadioHK04" runat="server" V_Groupe="SiConflitAborde"
                                   V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                                   RadioGaucheWidth="80px" RadioCentreWidth="80px" RadioDroiteWidth="0px"  
                                   RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
                                   RadioGaucheHeight="25px" RadioCentreHeight="25px"
                                   RadioCentreStyle="text-align:  center;"  
                                   RadioGaucheStyle="margin-left: 3px; text-align:  center;" Visible="true"/>
                            </asp:TableCell> 
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero4Evalue" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Evalué"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="30px" DonTabIndex="16" 
                           Etitext="Observations éventuelles de l'agent sur son activité et la réalisation des objectifs"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
</asp:Table>

