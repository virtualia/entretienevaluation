﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_EMPLOI_FAM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_EMPLOI_FAM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderColor="#B0E0D7" 
    BorderStyle="None" BorderWidth="2px" HorizontalAlign="Center" 
    style="margin-top: 1px;" Visible="true" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreEmploi" runat="server" Height="170px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="LE POSTE OCCUPE" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                         <asp:Label ID="LabelEmploiExerce" runat="server" Height="24px" Width="746px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                         <asp:Label ID="LabelCotationPoste" runat="server" Height="24px" Width="746px"
                            BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                         <asp:Label ID="LabelQuotiteTravail" runat="server" Height="24px" Width="746px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                       <asp:Label ID="LabelTitreNumero5" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Conflit d'intérêt"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                       </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="LabelConflit1" runat="server" Height="24px" Width="420px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="Etes-vous en situation potentielle de conflit d'intérêt ?"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell Width="330px" HorizontalAlign="Left">        
                       <Virtualia:VTrioHorizontalRadio ID="RadioHA04" runat="server" V_Groupe="SiConflit"
                           V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                           RadioGaucheWidth="80px" RadioCentreWidth="80px" RadioDroiteWidth="0px"  
                           RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
                           RadioGaucheHeight="25px" RadioCentreHeight="25px"
                           RadioCentreStyle="text-align:  center;"  
                           RadioGaucheStyle="margin-left: 3px; text-align:  center;" Visible="true"/>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="LabelConflit2" runat="server" Height="24px" Width="420px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="Si oui, votre déclaration de conflit d'intérêt est-elle en règle ?"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell Width="330px" HorizontalAlign="Left">        
                       <Virtualia:VTrioHorizontalRadio ID="RadioHB04" runat="server" V_Groupe="SiEnRegle"
                           V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                           RadioGaucheWidth="80px" RadioCentreWidth="80px" RadioDroiteWidth="0px"  
                           RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
                           RadioGaucheHeight="25px" RadioCentreHeight="25px"
                           RadioCentreStyle="text-align:  center;"  
                           RadioGaucheStyle="margin-left: 3px; text-align:  center;" Visible="true"/>
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                       <asp:Label ID="LabelTitreNumero4" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Votre fiche de poste"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                       </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="LabelSiAdaptation" runat="server" Height="24px" Width="248px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="- Est-elle adaptée au poste occupé ?"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell Width="230px" HorizontalAlign="Left">        
                       <Virtualia:VTrioHorizontalRadio ID="RadioH15" runat="server" V_Groupe="SiEncadrement"
                           V_PointdeVue="1" V_Objet="150" V_Information="15" V_SiDonneeDico="true"
                           RadioGaucheWidth="80px" RadioCentreWidth="80px" RadioDroiteWidth="0px"  
                           RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
                           RadioGaucheHeight="25px" RadioCentreHeight="25px"
                           RadioCentreStyle="text-align:  center;"  
                           RadioGaucheStyle="margin-left: 3px; text-align:  center;" Visible="true"/>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV16" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="16" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="110px" EtiHeight="40px" DonTabIndex="1" 
                           Etitext="- Sinon, sur quels points doit-elle être actualisée ?"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV17" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="17" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="110px" EtiHeight="40px" DonTabIndex="2" 
                           Etitext="Le cas échéant, donnez votre appréciation sur votre poste (attraits et contraintes)"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
</asp:Table>

