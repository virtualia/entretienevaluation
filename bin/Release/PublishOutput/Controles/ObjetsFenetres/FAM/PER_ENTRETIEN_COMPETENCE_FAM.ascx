﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_COMPETENCE_FAM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE_FAM" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreCompetence" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="III. EXPERIENCE PROFESSIONNELLE - EVALUATION DES ACQUIS" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre" runat="server" Text="Les compétences requises sur le poste / Les compétences mises en oeuvre par l'agent" 
                            Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations" runat="server" Height="40px" Width="746px"
                            BackColor="#98D4CA" BorderColor="#B0E0D7" BorderStyle="none"
                            Text="LEGENDE pour compléter les colonnes NIVEAU REQUIS et NIVEAU de l'AGENT sachant
                            que le niveau d'exigence requis doit tenir compte du statut de l'agent :"
                            BorderWidth="1px" ForeColor="#0F2F30"  
                            Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                            font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelNiveau4" runat="server" Height="50px" Width="746px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" 
                            Text="4 - Expert : domine le sujet, voire est capable de le faire évoluer - capacité à former et/ou à être tuteur.
			                La notion d'expert est ici distincte des certifications ou agréments attribués par les ministères dans
			                l'exercice de certaines fonctions spécifiques"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 3px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelNiveau3" runat="server" Height="37px" Width="746px"
                            BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet"
                            Text="3 - Maîtrise : connaissances approfondies - capacité à traiter de façon autonome les situations complexes ou inhabituelles"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 3px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow><asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelNiveau2" runat="server" Height="20px" Width="746px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" 
                            Text="2 - Pratique : connaissances générales - capacité à traiter de façon autonome les situations courantes"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 3px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelNiveau1" runat="server" Height="20px" Width="746px"
                            BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet"
                            Text="1 - Initié : connaissances élémentaires, notions - capacité à faire mais en étant tutoré"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 3px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelNiveau0" runat="server" Height="20px" Width="746px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" 
                            Text="0 - Non requis"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 3px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille1" runat="server" Height="35px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="COMPETENCES"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille1" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="Transparent" BorderStyle="NotSet" Text="à compléter à partir de la fiche de poste"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence1" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text="Compétence"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau1" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels1" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote11" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote12" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote13" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote14" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote15" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHA01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace11" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHA09" runat="server" V_Groupe="CritereRequis1"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace12" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent1" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHA07" runat="server" V_Groupe="CritereAgent1"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace13" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation1" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHA11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="2"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur1" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence2" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text="Compétence"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau2" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote21" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote22" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote23" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote24" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote25" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHB01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="3"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace21" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHB09" runat="server" V_Groupe="CritereRequis2"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace22" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent2" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHB07" runat="server" V_Groupe="CritereAgent2"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace23" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation2" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHB11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="4"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur2" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence3" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text="Compétence"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau3" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels3" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote31" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote32" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote33" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote34" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote35" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHC01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="5"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace31" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHC09" runat="server" V_Groupe="CritereRequis3"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace32" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent3" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHC07" runat="server" V_Groupe="CritereAgent3"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace33" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation3" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHC11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="6"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur3" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence4" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text="Compétence"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau4" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels4" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote41" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote42" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote43" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote44" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote45" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHD01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="7"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace41" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHD09" runat="server" V_Groupe="CritereRequis4"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace42" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent4" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHD07" runat="server" V_Groupe="CritereAgent4"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace43" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation4" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHD11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="8"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur4" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence5" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text="Compétence"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau5" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels5" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote51" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote52" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote53" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote54" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote55" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHE01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="9"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace51" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHE09" runat="server" V_Groupe="CritereRequis5"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace52" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent5" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHE07" runat="server" V_Groupe="CritereAgent5"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace53" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation5" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHE11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="10"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille2" runat="server" Height="35px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="SAVOIR-FAIRE"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreSavoirFaire6" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreSavoirFaire6" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau6" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels6" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote61" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote62" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote63" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote64" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote65" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHF01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="11"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Travail en équipe" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace61" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHF09" runat="server" V_Groupe="CritereRequis6"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace62" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent6" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHF07" runat="server" V_Groupe="CritereAgent6"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace63" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation6" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHF11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="12"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur6" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreSavoirFaire7" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreSavoirFaire7" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau7" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels7" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote71" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote72" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote73" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote74" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote75" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHG01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="13"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Capacité de synthèse" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace71" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHG09" runat="server" V_Groupe="CritereRequis7"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace72" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent7" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHG07" runat="server" V_Groupe="CritereAgent7"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace73" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation7" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHG11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="14"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur7" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreSavoirFaire8" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreSavoirFaire8" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau8" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels8" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote81" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote82" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote83" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote84" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote85" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHH01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="15"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Capacité d'analyse" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace81" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHH09" runat="server" V_Groupe="CritereRequis8"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace82" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent8" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHH07" runat="server" V_Groupe="CritereAgent8"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace83" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation8" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHH11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="16"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur8" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreSavoirFaire9" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreSavoirFaire9" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau9" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels9" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote91" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote92" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote93" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote94" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote95" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHI01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="17"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Animation d'équipe" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace91" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHI09" runat="server" V_Groupe="CritereRequis9"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace92" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent9" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHI07" runat="server" V_Groupe="CritereAgent9"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace93" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation9" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHI11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="18"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur9" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreSavoirFaire10" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreSavoirFaire10" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau10" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels10" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote101" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote102" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote103" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote104" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote105" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHJ01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="19"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Expression écrite" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace101" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHJ09" runat="server" V_Groupe="CritereRequis10"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace102" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent10" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHJ07" runat="server" V_Groupe="CritereAgent10"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace103" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation10" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHJ11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="20"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur10" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreSavoirFaire11" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreSavoirFaire11" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau11" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels11" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote111" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote112" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote113" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote114" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote115" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHK01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="21"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Expression orale" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace111" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHK09" runat="server" V_Groupe="CritereRequis11"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace112" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent11" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHK07" runat="server" V_Groupe="CritereAgent11"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace113" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation11" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHK11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="22"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur11" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreSavoirFaire12" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreSavoirFaire12" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau12" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels12" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote121" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote122" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote123" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote124" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote125" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHL01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="23"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Techniques spécifiques" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace121" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHL09" runat="server" V_Groupe="CritereRequis12"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace122" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent12" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHL07" runat="server" V_Groupe="CritereAgent12"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace123" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation12" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHL11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="24"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur12" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreSavoirFaire13" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreSavoirFaire13" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau13" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels13" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote131" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote132" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote133" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote134" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote135" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHM01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="25"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Autres" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace131" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHM09" runat="server" V_Groupe="CritereRequis13"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace132" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent13" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHM07" runat="server" V_Groupe="CritereAgent13"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace133" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation13" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHM11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="26"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>    
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille3" runat="server" Height="35px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="QUALITES RELATIONNELLES"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreQualites14" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreQualites14" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau14" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels14" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote141" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote142" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote143" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote144" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote145" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHN01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="27"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Sens des relations humaines" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace141" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHN09" runat="server" V_Groupe="CritereRequis14"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace142" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent14" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHN07" runat="server" V_Groupe="CritereAgent14"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace143" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation14" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHN11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="28"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur14" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreQualites15" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreQualites15" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau15" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels15" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote151" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote152" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote153" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote154" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote155" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHO01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="29"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Capacité d'adaptation" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace151" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHO09" runat="server" V_Groupe="CritereRequis15"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace152" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent15" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHO07" runat="server" V_Groupe="CritereAgent15"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace153" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation15" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHO11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="30"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur15" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreQualites16" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreQualites16" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau16" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels16" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote161" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote162" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote163" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote164" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote165" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHP01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="31"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Autonomie" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace161" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHP09" runat="server" V_Groupe="CritereRequis16"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace162" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent16" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHP07" runat="server" V_Groupe="CritereAgent16"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace163" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation16" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHP11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="32"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur16" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreQualites17" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreQualites17" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau17" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels17" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote171" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote172" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote173" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote174" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote175" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHQ01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="33"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Rigueur dans l'exécution des tâches" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace171" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHQ09" runat="server" V_Groupe="CritereRequis17"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace172" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent17" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHQ07" runat="server" V_Groupe="CritereAgent17"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace173" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation17" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHQ11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="34"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur17" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreQualites18" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreQualites18" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau18" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels18" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote181" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote182" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote183" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote184" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote185" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHR01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="35"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Capacité d'initiative" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace181" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHR09" runat="server" V_Groupe="CritereRequis18"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace182" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent18" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHR07" runat="server" V_Groupe="CritereAgent18"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace183" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation18" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHR11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="36"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur18" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreQualites19" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreQualites19" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau19" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels19" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote191" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote192" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote193" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote194" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote195" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHS01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="37"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Réactivité" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace191" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHS09" runat="server" V_Groupe="CritereRequis19"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace192" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent19" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHS07" runat="server" V_Groupe="CritereAgent19"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace193" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation19" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHS11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="38"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur19" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreQualites20" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreQualites20" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau20" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels20" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote201" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="0 - Non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote202" runat="server" Height="20px" Width="69px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="1 - Initié"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote203" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="2 - Pratique"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote204" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="3 - Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote205" runat="server" Height="20px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="4 - Expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHT01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="39"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Autres" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace201" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU REQUIS"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHT09" runat="server" V_Groupe="CritereRequis20"
                                     V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace202" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent20" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHT07" runat="server" V_Groupe="CritereAgent20"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="67px" VRadioN2Width="66px" VRadioN3Width="67px"
                                     VRadioN4Width="65px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace203" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation20" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHT11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="40"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille4" runat="server" Height="30px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="LES APTITUDES AU MANAGEMENT"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille41" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text="(pour les agents en situation d'encadrement)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreEncadrement" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server"
                                       V_PointdeVue="1" V_Objet="150" V_Information="9" V_SiDonneeDico="true"
                                       EtiWidth="300px" EtiHeight="30px" DonHeight="26px" DonWidth="50px" DonTabIndex="41" 
                                       EtiText="Nombre d'agents encadrés"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="7px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG03" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="80px" EtiHeight="20px" DonTabIndex="42" 
                                       Etitext="Commentaire sur les effectifs" 
                                       Donstyle="margin-left: 3px;"
                                       Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                                       DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px"></asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreManagement21" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreManagement21" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau21" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels21" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote211" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote212" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote213" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote214" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHV01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="43"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Capacité à déléguer" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent21" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHV07" runat="server" V_Groupe="CritereAgent21"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace213" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation21" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHV11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="44"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur21" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreManagement22" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreManagement22" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau22" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels22" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote221" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote222" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote223" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote224" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHW01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="45"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Capacité à mobiliser et valoriser les compétences" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent22" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHW07" runat="server" V_Groupe="CritereAgent22"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace223" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation22" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHW11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="46"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur22" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreManagement23" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreManagement23" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau23" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels23" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote231" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote232" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote233" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote234" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHX01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="47"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Capacité d'organisation, de pilotage" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent23" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHX07" runat="server" V_Groupe="CritereAgent23"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace233" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation23" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHX11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="48"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur23" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreManagement24" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreManagement24" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau24" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels24" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote241" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote242" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote243" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote244" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHY01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="49"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Attention portée au développement professionnel des collaborateurs" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent24" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHY07" runat="server" V_Groupe="CritereAgent24"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace243" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation24" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHY11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="50"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur24" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreManagement25" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreManagement25" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau25" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels25" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote251" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote252" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote253" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote254" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHZ01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="51"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Aptitude à prévenir, arbitrer et gérer les conflits" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent25" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHZ07" runat="server" V_Groupe="CritereAgent25"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace253" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation25" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHZ11" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="52"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur25" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreManagement26" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreManagement26" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau26" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels26" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote261" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote262" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote263" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote264" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoH101" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="53"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Aptitude à la prise de décision" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent26" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioH107" runat="server" V_Groupe="CritereAgent26"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace263" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation26" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoH111" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="54"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                   <asp:Label ID="LabelSeparateur26" runat="server" Height="2px" Width="740px"
                                     BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                                     BorderWidth="1px" ForeColor="White" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style: normal; text-indent: 0px; text-align:  center;">
                                   </asp:Label>
                              </asp:TableCell>
                           </asp:TableRow>      
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreManagement27" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreManagement27" runat="server" Height="20px" Width="260px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveau27" runat="server" Height="20px" Width="95px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels27" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote271" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A acquérir"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote272" runat="server" Height="20px" Width="89px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A développer"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote273" runat="server" Height="20px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote274" runat="server" Height="20px" Width="100px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Excellente maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoH201" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="55"
                                     Etivisible="False" DonWidth="256px" DonHeight="20px" 
                                     DonText="Capacité à fixer des objectifs cohérents" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: -1px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreNiveauAgent27" runat="server" Height="23px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="NIVEAU DE L'AGENT"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioH207" runat="server" V_Groupe="CritereAgent27"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="78px" VRadioN2Width="86px" VRadioN3Width="75px"
                                     VRadioN4Width="94px" VRadioN5Width="67px" VRadioN6Width="70px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreEspace273" runat="server" Height="20px" Width="256px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreAppreciation27" runat="server" Height="28px" Width="117px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="APPRECIATION (le cas échéant)"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                   <Virtualia:VCoupleEtiDonnee ID="InfoH211" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true" DonTabIndex="56"
                                     Etivisible="False" DonWidth="353px" DonHeight="20px" 
                                     DonText="" V_SiEnLectureSeule="false"
                                     DonStyle="margin-left: -2px; margin-top: 0px; text-indent: 2px; text-align: left"/> 
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>