﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_FORMATION_FAM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_FORMATION_FAM" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioVerticalRadio.ascx" tagname="VTrioVerticalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreFormation" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="IV. LES BESOINS DE FORMATION" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="40px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Formations suivies (années N et N-1)"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="ListeFormationsSuivies" runat="server" CadreWidth="746px" SiColonneSelect="false"/>
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero13" runat="server" Height="30px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Commentaires sur les formations suivies (appréciation, bilan, suites)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 4px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="false"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="40px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Formations demandées (années N et N-1)"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero14" runat="server" Height="30px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Commentaires sur les formations demandées"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 4px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="2" 
                           Etivisible="false"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="40px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Recueil des demandes de formation"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT11" runat="server" Height="20px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(1)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT12" runat="server" Height="20px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T1 : Formations liées à l'adaptation immédiate au poste de travail"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT21" runat="server" Height="20px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT22" runat="server" Height="20px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T2 : Formations liées à l'évolution des métiers"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT31" runat="server" Height="20px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT32" runat="server" Height="20px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T3 : Formations liées au développement des qualifications ou l'acquisition de nouvelles qualifications"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeDIF11" runat="server" Height="25px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(2)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeDIF12" runat="server" Height="25px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="L'agent indique s'il souhaite mobiliser son DIF pour la formation envisagée."
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeDIF21" runat="server" Height="25px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeDIF22" runat="server" Height="25px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Les formations liées à une adaptation immédiate au poste de travail ne peuvent être imputées sur le DIF."
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreEnteteDemandes1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left"  ColumnSpan="6">
                        <asp:Label ID="LabelTitreFormations1" runat="server" Height="35px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Demandes de formation"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteExpressionDemande1" runat="server" Height="50px" Width="362px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Expression des demandes"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteTypologie1" runat="server" Height="50px" Width="60px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Typologie (1)"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteInitiative1" runat="server" Height="50px" Width="100px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Demande <br/> d'initiative"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteAvisResponsable1" runat="server" Height="50px" Width="90px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Avis du responsable hiérarchique"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteRecoursDIF1" runat="server" Height="50px" Width="60px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Recours <br/> au DIF <br/> (2)"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteEcheance1" runat="server" Height="50px" Width="70px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Echéance <br/> envisagée"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN1" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN1" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VListeCombo ID="ListCA01" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="155" V_Information="1" V_SiDonneeDico="true" V_NomTable="Thème"
                                      LstWidth="351px" LstHeight="19px" LstBorderWidth="1px" 
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionDemandeN1" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisez (si nécessaire) :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHA03" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="4" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifDemandeN1" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="5" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVA02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123A"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVA10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeA" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 5px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 5px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVA06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisA"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVA11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifA" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHA04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="7" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN2" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN2" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN2" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VListeCombo ID="ListCB01" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="155" V_Information="1" V_SiDonneeDico="true" V_NomTable="Thème"
                                      LstWidth="351px" LstHeight="19px" LstBorderWidth="1px" 
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionDemandeN2" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisez (si nécessaire) :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHB03" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="9" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifDemandeN2" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="10" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVB02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123B"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVB10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeB" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 5px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 5px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVB06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisB"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVB11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifB" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHB04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="12" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN3" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN3" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN3" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VListeCombo ID="ListCC01" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="155" V_Information="1" V_SiDonneeDico="true" V_NomTable="Thème"
                                      LstWidth="351px" LstHeight="19px" LstBorderWidth="1px" 
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionDemandeN3" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisez (si nécessaire) :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHC03" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="14" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifDemandeN3" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="15" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVC02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123C"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVC10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeC" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 5px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 5px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVC06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisC"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVC11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifC" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHC04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="17" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN4" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN4" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN4" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN4" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VListeCombo ID="ListCD01" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="155" V_Information="1" V_SiDonneeDico="true" V_NomTable="Thème"
                                      LstWidth="351px" LstHeight="19px" LstBorderWidth="1px" 
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionDemandeN4" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisez (si nécessaire) :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHD03" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="19" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifDemandeN4" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="20" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVD02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123D"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVD10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeD" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 5px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 5px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVD06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisD"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVD11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifD" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHD04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="22" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN5" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN5" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN5" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN5" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VListeCombo ID="ListCE01" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="155" V_Information="1" V_SiDonneeDico="true" V_NomTable="Thème"
                                      LstWidth="351px" LstHeight="19px" LstBorderWidth="1px" 
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionDemandeN5" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisez (si nécessaire) :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHE03" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="24" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifDemandeN5" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="25" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVE02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123E"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVE10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeE" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 5px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 5px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVE06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisE"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVE11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifE" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHE04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="27" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN6" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN6" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN6" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN6" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VListeCombo ID="ListCF01" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="155" V_Information="1" V_SiDonneeDico="true" V_NomTable="Thème"
                                      LstWidth="351px" LstHeight="19px" LstBorderWidth="1px" 
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionDemandeN6" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisez (si nécessaire) :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHF03" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="29" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifDemandeN6" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="30" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVF02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123F"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVF10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeF" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 5px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 5px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVF06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisF"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVF11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifF" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHF04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="32" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN7" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN7" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN7" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN7" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VListeCombo ID="ListCG01" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="155" V_Information="1" V_SiDonneeDico="true" V_NomTable="Thème"
                                      LstWidth="351px" LstHeight="19px" LstBorderWidth="1px" 
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionDemandeN7" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisez (si nécessaire) :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHG03" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="34" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifDemandeN7" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="35" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVG02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123G"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVG10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeG" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 5px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 5px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVG06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisG"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVG11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifG" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHG04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="37" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN8" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN8" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN8" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN8" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VListeCombo ID="ListCH01" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="155" V_Information="1" V_SiDonneeDico="true" V_NomTable="Thème"
                                      LstWidth="351px" LstHeight="19px" LstBorderWidth="1px" 
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionDemandeN8" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisez (si nécessaire) :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHH03" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="39" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifDemandeN8" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="40" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVH02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123H"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVH10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeH" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 5px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 5px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVH06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisH"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVH11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifH" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHH04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="42" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN9" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN9" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN9" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN9" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VListeCombo ID="ListCI01" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="155" V_Information="1" V_SiDonneeDico="true" V_NomTable="Thème"
                                      LstWidth="351px" LstHeight="19px" LstBorderWidth="1px" 
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionDemandeN9" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisez (si nécessaire) :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHI03" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="44" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifDemandeN9" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="45" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVI02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123I"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVI10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeI" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 5px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 5px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVI06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisI"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVI11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifI" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHI04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="47" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN10" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN10" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN10" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN10" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VListeCombo ID="ListCJ01" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="155" V_Information="1" V_SiDonneeDico="true" V_NomTable="Thème"
                                      LstWidth="351px" LstHeight="19px" LstBorderWidth="1px" 
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionDemandeN10" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisez (si nécessaire) :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHJ03" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="49" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifDemandeN10" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="50" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVJ02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123J"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVJ10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeJ" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 5px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 5px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVJ06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisJ"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVJ11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifJ" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHJ04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="52" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="40px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreEnteteDemandes2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left"  ColumnSpan="6">
                        <asp:Label ID="LabelTitreFormations2" runat="server" Height="35px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Préparations aux concours"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteExpressionDemande2" runat="server" Height="50px" Width="362px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text=""
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; padding-top: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteTypologie2" runat="server" Height="50px" Width="60px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text=""
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 2px; margin-left: -1px; margin-bottom: 0px; padding-top: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteInitiative2" runat="server" Height="50px" Width="100px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text=""
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 2px; margin-left: -1px; margin-bottom: 0px; padding-top: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteAvisResponsable2" runat="server" Height="50px" Width="90px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Avis du responsable hiérarchique"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteRecoursDIF2" runat="server" Height="50px" Width="60px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Recours <br/> au DIF <br/> (2)"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteEcheance2" runat="server" Height="50px" Width="70px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Echéance <br/> envisagée"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN11" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN11" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN11" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="CONCOURS envisagé"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHK01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="53" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionDemandeN11" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Commentaire :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="45px" DonTabIndex="54" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTypologieDemandeN11" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInitiativeDemandeN11" runat="server" Height="19px" Width="100px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVK06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisK"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVK11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifK" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHK04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="57" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN12" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN12" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN12" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN12" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="CONCOURS envisagé"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHL01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="58" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionDemandeN12" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Commentaire :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="45px" DonTabIndex="59" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTypologieDemandeN12" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInitiativeDemandeN12" runat="server" Height="19px" Width="100px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVL06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisL"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVL11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifL" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHL04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="61" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell Height="40px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreEnteteDemandes3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left"  ColumnSpan="6">
                        <asp:Label ID="LabelTitreFormations3" runat="server" Height="35px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Autres actions (VAE, bilan de compétence, congé de formation)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteExpressionDemande3" runat="server" Height="50px" Width="362px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text=""
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteTypologie3" runat="server" Height="50px" Width="60px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text=""
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 2px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteInitiative3" runat="server" Height="50px" Width="100px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Demande <br/> d'initiative"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteAvisResponsable3" runat="server" Height="50px" Width="90px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Avis du responsable hiérarchique"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteRecoursDIF3" runat="server" Height="50px" Width="60px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Recours <br/> au DIF <br/> (2)"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteEcheance3" runat="server" Height="50px" Width="70px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Echéance <br/> envisagée"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN13" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN13" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN13" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="ACTION IDENTIFIEE"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHM01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="62" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifDemandeN13" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="349px" DonHeight="45px" DonTabIndex="64" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTypologieDemandeN13" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVM10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeM" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 5px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 5px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVM06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisM"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVM11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifM" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHM04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="66" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN14" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN14" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN14" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelThematiqueDemandeN14" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="ACTION IDENTIFIEE"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHN01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="350px" DonHeight="19px" DonTabIndex="67" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifDemandeN14" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="349px" DonHeight="45px" DonTabIndex="68" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTypologieDemandeN14" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVN10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeN" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 5px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 5px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVN06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisN"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheStyle="text-align: left; Font-size: 85%" RadioCentreStyle="text-align: left; Font-size: 85%" RadioDroiteStyle="text-align: left; Font-size: 85%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVN11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeDifN" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 85%; margin-left: 1px" 
                            RadioCentreStyle="text-align: left; Font-size: 85%; margin-left: 1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHN04" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="19px" DonTabIndex="70" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

 </asp:Table>