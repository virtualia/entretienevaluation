﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_FORMATION_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_FORMATION_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia"%>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia"%>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia"%>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" Visible="true"
    Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreFormation" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="V. FORMATION" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Text="" Height="30px" Width="746px"
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Text="1 - Formations suivies durant l'année écoulée" Height="38px" Width="750px"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 4px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell Width="746px" HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="ListeFormationsSuivies" runat="server" CadreWidth="740px" SiColonneSelect="false"/>
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Text="2 - Formations sollicitées et non suivies" Height="38px" Width="750px"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 4px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin102" runat="server" CellPadding="0" CellSpacing="0" Width="746px" BorderStyle="None">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage102" runat="server" Text="Intitulé" Height="25px" Width="325px"
                               CssClass="EP_EtiComplement"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteInitiative102" runat="server" Text="Formation proposée ou demandée" Height="25px" Width="410px"
                               CssClass="EP_EtiComplement"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="320px" DonHeight="56px" DonTabIndex="1" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHA10" runat="server" V_Groupe="InitiativeA"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 90%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="320px" DonHeight="56px" DonTabIndex="2" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHB10" runat="server" V_Groupe="InitiativeB"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 90%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="320px" DonHeight="56px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHC10" runat="server" V_Groupe="InitiativeC"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 90%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="320px" DonHeight="56px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHD10" runat="server" V_Groupe="InitiativeD"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 90%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="320px" DonHeight="56px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHE10" runat="server" V_Groupe="InitiativeE"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 90%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin103" runat="server" CellPadding="0" CellSpacing="0" Width="746px" BorderStyle="None">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="LabelTypeBesoin103" runat="server" Text="3 - Formations envisagées" Height="38px" Width="750px"
                               CssClass="EP_TitreParagraphe"
                               style="padding-top: 4px;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage103" runat="server" Text="Intitulé" Height="25px" Width="325px"
                               CssClass="EP_EtiComplement"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteInitiative103" runat="server" Text="Formation proposée ou demandée" Height="25px" Width="410px"
                               CssClass="EP_EtiComplement"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="320px" DonHeight="56px" DonTabIndex="6" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHH10" runat="server" V_Groupe="InitiativeH"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 90%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false" 
                                Visible="true"/>
                          </asp:TableCell>                              
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="320px" DonHeight="56px" DonTabIndex="7" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHI10" runat="server" V_Groupe="InitiativeI"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 90%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="320px" DonHeight="56px" DonTabIndex="8" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHJ10" runat="server" V_Groupe="InitiativeJ"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 90%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="320px" DonHeight="56px" DonTabIndex="9" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHK10" runat="server" V_Groupe="InitiativeK"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 90%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="320px" DonHeight="56px" DonTabIndex="10" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHL10" runat="server" V_Groupe="InitiativeL"
                                V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="true"
                                VRadioN1Width="150px" VRadioN2Width="100px" VRadioN3Width="150px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 90%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 90%" 
                                VRadioN1Text="par l'administration" VRadioN2Text="par l'agent" VRadioN3Text="par les deux parties"
                                VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero31" runat="server" Height="30px" Width="748px"
                           Text="Observations"
                           CssClass="EP_TitreChapitre"
                           style="margin-bottom: 2px; padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="744px" DonHeight="190px" EtiHeight="20px" DonTabIndex="11" 
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 1px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>