﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_COMPETENCE_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE_ENM" %>

<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VSixBoutonRadio.ascx" TagName="VSixBoutonRadio" TagPrefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR {
        background-color: #216B68;
        color: #D7FAF3;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: bold;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: initial;
        height: 25px;
        width: 746px;
    }

    .EP_TitreOnglet {
        background-color: #E2F5F1;
        color: #0E5F5C;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: bold;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_TitreChapitre {
        background-color: #CAEBE4;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiComplement {
        background-color: #E2F5F1;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-align: left;
        text-indent: 5px;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiComplementBis {
        background-color: #E9FDF9;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-align: left;
        text-indent: 5px;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiComplementTer {
        background-color: #98D4CA;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-align: left;
        text-indent: 5px;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiTableau {
        background-color: #D7FAF3;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: normal;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_TitreParagraphe {
        background-color: #8DA8A3;
        color: white;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: normal;
        text-indent: 8px;
        text-align: left;
        padding-top: 6px;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiNiveau {
        background-color: white;
        color: #124545;
        border-style: solid;
        border-width: 1px;
        border-color: #9EB0AC;
        font-family: 'Trebuchet MS';
        font-size: x-small;
        font-style: italic;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreCompetence" runat="server" Height="75px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            CssClass="EP_TitreCR">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="II. EVALUATION" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations1" runat="server" Height="40px" Width="746px"
                            Text="La qualification du niveau global de l'agent tel qu'il apparaît dans la grille des critères d'appréciation et d'évaluation doit être en parfaite harmonie avec l'appréciation littérale de la valeur professionnelle de l'agent."
                            CssClass="EP_EtiComplement"
                            Style="text-indent: 1px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations2" runat="server" Height="25px" Width="746px"
                            Text="GRILLE D'EVALUATION : Pour chacun des thèmes, cocher le niveau estimé et la marge d'évolution"
                            CssClass="EP_EtiComplement"
                            Style="text-indent: 0px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille2" runat="server" Height="38px" Width="740px"
                            Text="COMPETENCES PROFESSIONNELLES"
                            CssClass="EP_TitreParagraphe"
                            Style="margin-left: -2px; padding-top: 7px; text-indent: 0px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences2" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelCadrage2" runat="server" Height="17px" Width="445px" Text=""
                                        CssClass="EP_EtiComplement">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="LabelTitreNiveau2" runat="server" Height="14px" Width="287px" Text="Niveau de performance"
                                        CssClass="EP_EtiNiveau"
                                        Font-Size="80%" Style="margin-left: 3px; margin-top: 2px; text-align: center; padding-top: 3px">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelTitreCompetence2" runat="server" Height="32px" Width="442px" Text="Grille à compléter en intégralité"
                                        CssClass="EP_EtiComplement"
                                        Font-Size="80%"
                                        Style="margin-top: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote21" runat="server" Height="29px" Width="50px" Text="Excellent" ToolTip="Excellent"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote22" runat="server" Height="29px" Width="40px" Text="Très bon" ToolTip="Très bon"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote23" runat="server" Height="29px" Width="35px" Text="Bon" ToolTip="Bon"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote24" runat="server" Height="29px" Width="38px" Text="Moyen" ToolTip="Moyen"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote25" runat="server" Height="29px" Width="57px" Text="Insuffisant" ToolTip="Insuffisant"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote26" runat="server" Height="29px" Width="57px" Text="Très insuffisant" ToolTip="Très insuffisant"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="1"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHA07" runat="server" V_Groupe="CritereAgent1"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="2"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHB07" runat="server" V_Groupe="CritereAgent2"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="3"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHC07" runat="server" V_Groupe="CritereAgent3"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="35px" VRadioN2Height="35px" VRadioN3Height="35px"
                                        VRadioN4Height="35px" VRadioN5Height="35px" VRadioN6Height="35px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="4"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHD07" runat="server" V_Groupe="CritereAgent4"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="5"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHE07" runat="server" V_Groupe="CritereAgent5"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="6"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHF07" runat="server" V_Groupe="CritereAgent6"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Table ID="CadreMarge2" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreMarge2" runat="server" Height="34px" Width="150px" Text="Marge d'évolution"
                                                    CssClass="EP_TitreOnglet"
                                                    Style="padding-top: 6px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left" VerticalAlign="Top">
                                                <Virtualia:VSixBoutonRadio ID="RadioHA03" runat="server" V_Groupe="CritereMarge1"
                                                    V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                                                    VRadioN1Width="120px" VRadioN2Width="120px" VRadioN3Width="120px"
                                                    VRadioN4Width="5px" VRadioN5Width="5px" VRadioN6Width="5px"
                                                    VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                                    VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                                    VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                                    VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                                    VRadioN1Text="En progrès" VRadioN2Text="Constant" VRadioN3Text="A améliorer"
                                                    VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                                    VRadioN4Visible="False" VRadioN5Visible="False" VRadioN6Visible="False"
                                                    Visible="true" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille3" runat="server" Height="38px" Width="740px"
                            Text="APTITUDES PROFESSIONNELLES ET EFFICACITE DANS L'EMPLOI"
                            CssClass="EP_TitreParagraphe"
                            Style="margin-left: -2px; padding-top: 7px; text-indent: 0px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences3" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelCadrage3" runat="server" Height="17px" Width="445px" Text=""
                                        CssClass="EP_EtiComplement">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="LabelTitreNiveau3" runat="server" Height="14px" Width="287px" Text="Niveau de performance"
                                        CssClass="EP_EtiNiveau"
                                        Font-Size="80%" Style="margin-left: 3px; margin-top: 2px; text-align: center; padding-top: 3px">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelTitreCompetence3" runat="server" Height="32px" Width="442px" Text="Grille à compléter en intégralité"
                                        CssClass="EP_EtiComplement"
                                        Font-Size="80%"
                                        Style="margin-top: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="CadreEnteteLabels3" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote31" runat="server" Height="29px" Width="50px" Text="Excellent" ToolTip="Excellent"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote32" runat="server" Height="29px" Width="40px" Text="Très bon" ToolTip="Très bon"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote33" runat="server" Height="29px" Width="35px" Text="Bon" ToolTip="Bon"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote34" runat="server" Height="29px" Width="38px" Text="Moyen" ToolTip="Moyen"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote35" runat="server" Height="29px" Width="57px" Text="Insuffisant" ToolTip="Insuffisant"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote36" runat="server" Height="29px" Width="57px" Text="Très insuffisant" ToolTip="Très insuffisant"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="7"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHI07" runat="server" V_Groupe="CritereAgent7"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="8"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHJ07" runat="server" V_Groupe="CritereAgent8"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="9"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHK07" runat="server" V_Groupe="CritereAgent9"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="10"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHL07" runat="server" V_Groupe="CritereAgent10"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="11"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHM07" runat="server" V_Groupe="CritereAgent11"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="12"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHN07" runat="server" V_Groupe="CritereAgent12"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Table ID="CadreMarge3" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreMarge3" runat="server" Height="34px" Width="150px" Text="Marge d'évolution"
                                                    CssClass="EP_TitreOnglet"
                                                    Style="padding-top: 6px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left" VerticalAlign="Top">
                                                <Virtualia:VSixBoutonRadio ID="RadioHB03" runat="server" V_Groupe="CritereMarge2"
                                                    V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                                                    VRadioN1Width="120px" VRadioN2Width="120px" VRadioN3Width="120px"
                                                    VRadioN4Width="5px" VRadioN5Width="5px" VRadioN6Width="5px"
                                                    VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                                    VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                                    VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                                    VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                                    VRadioN1Text="En progrès" VRadioN2Text="Constant" VRadioN3Text="A améliorer"
                                                    VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                                    VRadioN4Visible="False" VRadioN5Visible="False" VRadioN6Visible="False"
                                                    Visible="true" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille4" runat="server" Height="38px" Width="740px"
                            Text="QUALITES ET CAPACITES RELATIONNELLES"
                            CssClass="EP_TitreParagraphe"
                            Style="margin-left: -2px; padding-top: 7px; text-indent: 0px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences4" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelCadrage4" runat="server" Height="17px" Width="445px" Text=""
                                        CssClass="EP_EtiComplement">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="LabelTitreNiveau4" runat="server" Height="14px" Width="287px" Text="Niveau de performance"
                                        CssClass="EP_EtiNiveau"
                                        Font-Size="80%" Style="margin-left: 3px; margin-top: 2px; text-align: center; padding-top: 3px">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelTitreCompetence4" runat="server" Height="32px" Width="442px" Text="Grille à compléter en intégralité"
                                        CssClass="EP_EtiComplement"
                                        Font-Size="80%"
                                        Style="margin-top: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="CadreEnteteLabels4" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote41" runat="server" Height="29px" Width="50px" Text="Excellent" ToolTip="Excellent"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote42" runat="server" Height="29px" Width="40px" Text="Très bon" ToolTip="Très bon"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote43" runat="server" Height="29px" Width="35px" Text="Bon" ToolTip="Bon"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote44" runat="server" Height="29px" Width="38px" Text="Moyen" ToolTip="Moyen"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote45" runat="server" Height="29px" Width="57px" Text="Insuffisant" ToolTip="Insuffisant"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote46" runat="server" Height="29px" Width="57px" Text="Très insuffisant" ToolTip="Très insuffisant"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="13"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHQ07" runat="server" V_Groupe="CritereAgent13"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVR01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="14"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHR07" runat="server" V_Groupe="CritereAgent14"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Table ID="CadreMarge4" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreMarge4" runat="server" Height="34px" Width="150px" Text="Marge d'évolution"
                                                    CssClass="EP_TitreOnglet"
                                                    Style="padding-top: 6px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left" VerticalAlign="Top">
                                                <Virtualia:VSixBoutonRadio ID="RadioHC03" runat="server" V_Groupe="CritereMarge3"
                                                    V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                                                    VRadioN1Width="120px" VRadioN2Width="120px" VRadioN3Width="120px"
                                                    VRadioN4Width="5px" VRadioN5Width="5px" VRadioN6Width="5px"
                                                    VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                                    VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                                    VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                                    VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                                    VRadioN1Text="En progrès" VRadioN2Text="Constant" VRadioN3Text="A améliorer"
                                                    VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                                    VRadioN4Visible="False" VRadioN5Visible="False" VRadioN6Visible="False"
                                                    Visible="true" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille5" runat="server" Height="38px" Width="740px"
                            Text="CAPACITES D'ENCADREMENT"
                            CssClass="EP_TitreParagraphe"
                            Style="margin-left: -2px; padding-top: 7px; text-indent: 0px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Button ID="BtnRenitialisation" runat="server" Height="30px" Width="100px"
                            Text="Rénitialiser"
                            CssClass="EP_TitreParagraphe"
                            Style="margin-left: 0px; margin-top: 10px; padding-top: 2px; text-indent: 0px; text-align: center; font-size: small;"></asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences5" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelCadrage5" runat="server" Height="17px" Width="445px" Text=""
                                        CssClass="EP_EtiComplement">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="LabelTitreNiveau5" runat="server" Height="14px" Width="287px" Text="Niveau de performance"
                                        CssClass="EP_EtiNiveau"
                                        Font-Size="80%" Style="margin-left: 3px; margin-top: 2px; text-align: center; padding-top: 3px">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelTitreCompetence5" runat="server" Height="32px" Width="442px" Text="A compléter uniquement lorsque l'agent évalué exerce des fonctions d'encadrement"
                                        CssClass="EP_EtiComplement"
                                        Font-Size="80%"
                                        Style="margin-top: 1px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="CadreEnteteLabels5" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote51" runat="server" Height="29px" Width="50px" Text="Excellent" ToolTip="Excellent"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote52" runat="server" Height="29px" Width="40px" Text="Très bon" ToolTip="Très bon"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote53" runat="server" Height="29px" Width="35px" Text="Bon" ToolTip="Bon"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote54" runat="server" Height="29px" Width="38px" Text="Moyen" ToolTip="Moyen"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote55" runat="server" Height="29px" Width="57px" Text="Insuffisant" ToolTip="Insuffisant"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote56" runat="server" Height="29px" Width="57px" Text="Très insuffisant" ToolTip="Très insuffisant"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVT01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="15"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHT07" runat="server" V_Groupe="CritereAgent15"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVU01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="16"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHU07" runat="server" V_Groupe="CritereAgent16"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVV01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="17"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHV07" runat="server" V_Groupe="CritereAgent17"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVW01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="18"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHW07" runat="server" V_Groupe="CritereAgent18"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVX01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="19"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHX07" runat="server" V_Groupe="CritereAgent19"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVY01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="442px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="20"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHY07" runat="server" V_Groupe="CritereAgent20"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="47px" VRadioN2Width="36px" VRadioN3Width="32px"
                                        VRadioN4Width="34px" VRadioN5Width="53px" VRadioN6Width="53px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Table ID="CadreMarge5" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreMarge5" runat="server" Height="34px" Width="150px" Text="Marge d'évolution"
                                                    CssClass="EP_TitreOnglet"
                                                    Style="padding-top: 6px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left" VerticalAlign="Top">
                                                <Virtualia:VSixBoutonRadio ID="RadioHD03" runat="server" V_Groupe="CritereMarge4"
                                                    V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                                                    VRadioN1Width="120px" VRadioN2Width="120px" VRadioN3Width="120px"
                                                    VRadioN4Width="5px" VRadioN5Width="5px" VRadioN6Width="5px"
                                                    VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                                    VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                                    VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                                    VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                                    VRadioN1Text="En progrès" VRadioN2Text="Constant" VRadioN3Text="A améliorer"
                                                    VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                                    VRadioN4Visible="False" VRadioN5Visible="False" VRadioN6Visible="False"
                                                    Visible="true" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="Label1" runat="server" Height="38px" Width="740px"
                                        Text=""
                                        CssClass="EP_TitreParagraphe"
                                        Style="margin-left: -2px; padding-top: 7px; text-indent: 0px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero7" runat="server" CellPadding="0" CellSpacing="0" Width="550px"
                HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille6" runat="server" Height="38px" Width="500px"
                            Text="MARGE D'EVOLUTION GLOBALE"
                            CssClass="EP_TitreOnglet"
                            Style="margin-left: -2px; padding-top: 7px; text-indent: 0px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences6" runat="server" CellPadding="0" CellSpacing="0" Width="546px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Table ID="CadreMarge6" runat="server" CellPadding="0" CellSpacing="0" Width="546px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                                <Virtualia:VSixBoutonRadio ID="RadioHE03" runat="server" V_Groupe="CritereMarge5"
                                                    V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                                                    VRadioN1Width="130px" VRadioN2Width="130px" VRadioN3Width="130px"
                                                    VRadioN4Width="5px" VRadioN5Width="5px" VRadioN6Width="5px"
                                                    VRadioN1Height="30px" VRadioN2Height="30px" VRadioN3Height="30px"
                                                    VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                                    VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                                    VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                                    VRadioN1Text="En progrès" VRadioN2Text="Constant" VRadioN3Text="A améliorer"
                                                    VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                                    VRadioN4Visible="False" VRadioN5Visible="False" VRadioN6Visible="False"
                                                    Visible="true" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille7" runat="server" Height="38px" Width="500px"
                            Text="NIVEAU GLOBAL DE PERFORMANCE"
                            CssClass="EP_TitreOnglet"
                            Style="margin-left: -2px; padding-top: 7px; text-indent: 0px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences7" runat="server" CellPadding="0" CellSpacing="0" Width="546px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" Visible="False">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVZ01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="242px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="21"
                                        EtiVisible="false" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="CadreEnteteLabels7" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote71" runat="server" Height="29px" Width="60px" Text="Excellent" ToolTip="Excellent"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote72" runat="server" Height="29px" Width="60px" Text="Très bon" ToolTip="Très bon"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote73" runat="server" Height="29px" Width="60px" Text="Bon" ToolTip="Bon"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote74" runat="server" Height="29px" Width="60px" Text="Moyen" ToolTip="Moyen"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote75" runat="server" Height="29px" Width="60px" Text="Insuffisant" ToolTip="Insuffisant"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote76" runat="server" Height="29px" Width="60px" Text="Très insuffisant" ToolTip="Très insuffisant"
                                                    CssClass="EP_EtiNiveau"
                                                    Font-Size="80%"
                                                    Style="margin-top: 1px; padding-top: 5px">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                    <Virtualia:VSixBoutonRadio ID="RadioHZ07" runat="server" V_Groupe="CritereAgent21"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="57px" VRadioN2Width="56px" VRadioN3Width="56px"
                                        VRadioN4Width="56px" VRadioN5Width="56px" VRadioN6Width="57px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
</asp:Table>
