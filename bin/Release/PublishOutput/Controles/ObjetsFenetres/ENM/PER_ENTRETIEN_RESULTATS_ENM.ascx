﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_RESULTATS_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_RESULTATS_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderColor="#B0E0D7" 
    BorderStyle="None" BorderWidth="2px" HorizontalAlign="Center" 
    style="margin-top: 1px;" Visible="true" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreResultats" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="I. BILAN DES OBJECTIFS FIXES POUR L'ANNEE ECOULEE" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectif" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteObjectif1" runat="server" Height="60px" Width="241px"
                                   Text="Rappel des objectifs assignés à l'agent"
                                   CssClass="EP_EtiTableau">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteRealisation" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                                <asp:Label ID="LabelEnteteObjectif" runat="server" Height="29px" Width="220px"
                                   Text="Degré de réalisation de l'objectif"
                                   Tooltip="Au choix l'objectif sera défini comme étant atteint, partiellement atteint, non atteint, devenu sans objet"
                                   CssClass="EP_EtiTableau">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteComplete" runat="server" Height="30px" Width="49px"
                                   Text="objectif atteint"
                                   CssClass="EP_EtiTableau"
                                   BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEntetePartielle" runat="server" Height="30px" Width="64px"
                                   Text="partiellement atteint"
                                   CssClass="EP_EtiTableau"
                                   BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteInsuffisante" runat="server" Height="30px" Width="49px"
                                   Text="non atteint"
                                   CssClass="EP_EtiTableau"
                                   BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteSansObjet" runat="server" Height="30px" Width="53px"
                                   Text="devenu sans objet"
                                   CssClass="EP_EtiTableau"
                                   BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObservations" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteObservations" runat="server" Height="60px" Width="265px"
                                   Text="Observations"
                                   Tooltip="Difficultés rencontrées, nouvelles orientations, réorganisations, nouveaux outils, missions nouvelles venant expliquer un décalage par rapport aux objectifs fixés"
                                   CssClass="EP_EtiTableau">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif1" runat="server" Height="25px" Width="251px"
                                   Text="Objectif n°1"
                                   CssClass="EP_EtiComplement"
                                   Font-Size="Medium" style="margin-top: 5px; font-style: normal; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="1"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif" Etiwidth="245px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="2"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Contexte et moyens mis en oeuvre" Etiwidth="245px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHA04" runat="server" V_Groupe="Realisation1"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHA05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="170px" DonTabIndex="3" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 8px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes1" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="true"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="4"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Avis de l'agent" Etiwidth="269px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Avis du supérieur hiérarchique direct" Etiwidth="269px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" Width="746px" BackColor="#98D4CA" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif2" runat="server" Height="25px" Width="251px"
                                   Text="Objectif n°2"
                                   CssClass="EP_EtiComplement"
                                   Font-Size="Medium" style="margin-top: 5px; font-style: normal; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="6"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif" Etiwidth="245px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Contexte et moyens mis en oeuvre" Etiwidth="245px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHB04" runat="server" V_Groupe="Realisation2"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHB05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="170px" DonTabIndex="8" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 8px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes2" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="true"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="9"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Avis de l'agent" Etiwidth="269px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="10"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Avis du supérieur hiérarchique direct" Etiwidth="269px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" Width="746px" BackColor="#98D4CA" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif3" runat="server" Height="25px" Width="251px"
                                   Text="Objectif n°3"
                                   CssClass="EP_EtiComplement"
                                   Font-Size="Medium" style="margin-top: 5px; font-style: normal; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="11"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif" Etiwidth="245px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="12"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Contexte et moyens mis en oeuvre" Etiwidth="245px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHC04" runat="server" V_Groupe="Realisation3"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHC05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="170px" DonTabIndex="13" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 8px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes3" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="true"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="14"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Avis de l'agent" Etiwidth="269px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="15"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Avis du supérieur hiérarchique direct" Etiwidth="269px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" Width="746px" BackColor="#98D4CA" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif4" runat="server" Height="25px" Width="251px"
                                   Text="Objectif n°4"
                                   CssClass="EP_EtiComplement"
                                   Font-Size="Medium" style="margin-top: 5px; font-style: normal; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="16"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif" Etiwidth="245px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="17"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Contexte et moyens mis en oeuvre" Etiwidth="245px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHD04" runat="server" V_Groupe="Realisation4"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHD05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="170px" DonTabIndex="18" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 8px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes4" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="true"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="19"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Avis de l'agent" Etiwidth="269px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="20"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Avis du supérieur hiérarchique direct" Etiwidth="269px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" Width="746px" BackColor="#98D4CA" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif5" runat="server" Height="25px" Width="251px"
                                   Text="Objectif n°5"
                                   CssClass="EP_EtiComplement"
                                   Font-Size="Medium" style="margin-top: 5px; font-style: normal; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="21"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif" Etiwidth="245px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="120px" DonTabIndex="22"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Contexte et moyens mis en oeuvre" Etiwidth="245px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation5" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHE04" runat="server" V_Groupe="Realisation5"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHE05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="170px" DonTabIndex="23" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 8px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes5" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="true"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="24"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Avis de l'agent" Etiwidth="269px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   DonWidth="267px" DonHeight="120px" DonTabIndex="25"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Avis du supérieur hiérarchique direct" Etiwidth="269px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                           Text="SYNTHESE GENERALE DU BILAN DES OBJECTIFS"
                           CssClass="EP_TitreChapitre"
                           style=" margin-bottom: 3px; padding-top: 7px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="150px" EtiHeight="30px" DonTabIndex="26" 
                           Etitext="Avis du supérieur hiérarchique direct"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 4px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="150px" EtiHeight="30px" DonTabIndex="27" 
                           Etitext="Avis de l'agent"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 4px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
</asp:Table>