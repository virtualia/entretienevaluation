﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_OBJECTIFS_CESE.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_OBJECTIFS_CESE" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreObjectifs" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="OBJECTIFS POUR L'ANNEE A VENIR" Height="20px" Width="746px"
                            BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
			               Text="Mentionner les objectifs et priorités de travail ou points sur lesquels
                           doit porter l'effort, assignés pour l'année suivante"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                           font-style: oblique; text-indent: 3px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Rappel des objectifs collectifs assignés au service"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; padding-top: 7px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="744px" DonHeight="170px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 1px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelContexteAgent" runat="server" Height="33px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Objectifs de l'agent"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableEnteteObjectifs" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelEnteteObjectif1" runat="server" Height="20px" Width="313px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                   style="margin-top: 0px; margin-left: 10px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelEnteteObjectif2" runat="server" Height="28px" Width="313px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="" ForeColor="#124545" Font-Italic="true"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"  
                                   style="margin-top: 0px; margin-left: 10px; margin-bottom: 0px;
                                   font-style: italic; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableEnteteSeparateur" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelEnteteSeparateur1" runat="server" Height="20px" Width="70px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="" ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelEnteteSeparateur2" runat="server" Height="28px" Width="70px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="" ForeColor="#124545" Font-Italic="true"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"  
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: italic; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableEnteteConditions" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelEnteteConditions1" runat="server" Height="20px" Width="320px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Conditions de réussite"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelEnteteConditions2" runat="server" Height="28px" Width="320px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="(moyens nécessaires, actions à conduire, formation, <br/> résultats attendus, délai de réalisation…)"
                                   ForeColor="#124545" Font-Italic="true"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"  
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: italic; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="2"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableSeparateur1" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelSeparateur1" runat="server" Height="90px" Width="75px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif <br/> n°1" ForeColor="#124545" Font-Italic="false"
                                   Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"  
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 20px;
                                   font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableConditions1" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="3"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="4"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableSeparateur2" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelSeparateur2" runat="server" Height="90px" Width="75px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif <br/> n°2" ForeColor="#124545" Font-Italic="false"
                                   Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"  
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 20px;
                                   font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableConditions2" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="6"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableSeparateur3" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelSeparateur3" runat="server" Height="90px" Width="75px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif <br/> n°3" ForeColor="#124545" Font-Italic="false"
                                   Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"  
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 20px;
                                   font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableConditions3" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="8"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableSeparateur4" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelSeparateur4" runat="server" Height="90px" Width="75px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif <br/> n°4" ForeColor="#124545" Font-Italic="false"
                                   Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"  
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 20px;
                                   font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableConditions4" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="9"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>             
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>