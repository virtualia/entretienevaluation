﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_COMPETENCE_CESE.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE_CESE" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreCompetence" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="EVALUATION" Height="20px" Width="746px"
                            BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations" runat="server" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="none"
                            Text="Pour chacun des thèmes, cocher le niveau estimé."
                            BorderWidth="1px" ForeColor="#124545"  
                            Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Font-Bold="False" 
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                            font-style: italic; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille1" runat="server" Height="25px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="CONNAISSANCE DU DOMAINE D'EXERCICE DE LA FONCTION"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                           font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille1" runat="server" Height="20px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="A compléter en fonction des compétences attendues par la fiche de poste actualisée"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence1" runat="server" Height="32px" Width="246px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text="Compétence"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels1" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote11" runat="server" Height="27px" Width="55px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expertise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote12" runat="server" Height="27px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote13" runat="server" Height="29px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A <br/> développer"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote14" runat="server" Height="29px" Width="55px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A <br/> acquérir"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreObserv1" runat="server" Height="32px" Width="236px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text="Observations éventuelles"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>       
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="1" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHA07" runat="server" V_Groupe="CritereAgent1"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="2" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="3" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHB07" runat="server" V_Groupe="CritereAgent2"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="4" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="5" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHC07" runat="server" V_Groupe="CritereAgent3"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="6" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>  
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille2" runat="server" Height="45px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="QUALITE DU TRAVAIL ET QUALITES RELATIONNELLES"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                           font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence2" runat="server" Height="32px" Width="246px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text="Qualité"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote21" runat="server" Height="27px" Width="55px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Excellent"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote22" runat="server" Height="27px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Satisfaisant"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote23" runat="server" Height="29px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A <br/> développer"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote24" runat="server" Height="29px" Width="55px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A <br/> acquérir"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreObserv2" runat="server" Height="32px" Width="236px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text="Observations éventuelles"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>       
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="7" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHH07" runat="server" V_Groupe="CritereAgent4"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="8" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="9" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHI07" runat="server" V_Groupe="CritereAgent5"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="10" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="11" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHJ07" runat="server" V_Groupe="CritereAgent6"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="12" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="13" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHK07" runat="server" V_Groupe="CritereAgent7"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="14" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="15" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHL07" runat="server" V_Groupe="CritereAgent8"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="16" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="17" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHM07" runat="server" V_Groupe="CritereAgent9"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="18" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="19" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHN07" runat="server" V_Groupe="CritereAgent10"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="20" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="21" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHO07" runat="server" V_Groupe="CritereAgent11"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="22" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="23" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHP07" runat="server" V_Groupe="CritereAgent12"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="24" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="25" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHQ07" runat="server" V_Groupe="CritereAgent13"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="26" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille3" runat="server" Height="25px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="APTITUDES AU MANAGEMENT"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                           font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille3" runat="server" Height="20px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="(uniquement pour les agents en situation d'encadrement)"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: oblique; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence3" runat="server" Height="32px" Width="246px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text="Aptitude"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels3" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote31" runat="server" Height="27px" Width="55px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Excellent"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote32" runat="server" Height="27px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Satisfaisant"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote33" runat="server" Height="29px" Width="70px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A <br/> développer"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote34" runat="server" Height="29px" Width="55px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A <br/> acquérir"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreObserv3" runat="server" Height="32px" Width="236px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text="Observations éventuelles"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>       
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVU01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="27" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHU07" runat="server" V_Groupe="CritereAgent14"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVU11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="28" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVV01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="29" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHV07" runat="server" V_Groupe="CritereAgent15"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVV11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="30" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVW01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="31" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHW07" runat="server" V_Groupe="CritereAgent16"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVW11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="32" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVX01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="33" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHX07" runat="server" V_Groupe="CritereAgent17"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVX11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="34" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>