﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_COMPETENCE_ONISEP.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE_ONISEP" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderColor="#B0E0D7" 
    BorderStyle="None" BorderWidth="2px" HorizontalAlign="Center" 
    style="margin-top: 1px;" Visible="true" Width="750px">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreCompetence" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="3. VALEUR PROFESSIONNELLE ET MANIERE DE SERVIR" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero31" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitre31" runat="server" Height="40px" Width="748px"
                           Text="3.1 Critères d'appréciation"
                           CssClass="EP_TitreParagraphe" style="margin-bottom: 4px; padding-top: 9px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations31" runat="server" Height="60px" Width="748px"
                           Text="L'évaluateur retient, pour apprécier la valeur professionnelle des agents au cours de l'entretien
			               professionnel, les critères annexés à l'arrêté ministériel et qui sont adaptés à la nature des tâches
			               qui leur sont confiées, au niveau de leurs responsabilités et au contexte professionnel"
                           CssClass="EP_EtiComplementTer" style="margin-bottom: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero311" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille311" runat="server" Height="35px" Width="748px"
                           Text="1. Les compétences professionnelles et technicité"
                           CssClass="EP_TitreChapitre" style="margin-bottom: 4px; padding-top: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations311" runat="server" Height="40px" Width="746px"
                           Text="Maîtrise technique ou expertise scientifique du domaine d'activité, connaissance de l'environnement professionnel
                           et capacité à s'y situer, qualité d'expression écrite, qualité d'expression orale, etc."
                           CssClass="EP_EtiComplement" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero312" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille312" runat="server" Height="35px" Width="748px"
                           Text="2. La contribution à l'activité du service"
                           CssClass="EP_TitreChapitre" style="margin-bottom: 4px; padding-top: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations312" runat="server" Height="40px" Width="746px"
                           Text="Capacité à partager l'information, à transférer les connaissances et à rendre compte,
			               capacité à s'investir dans des projets, sens du service public et conscience professionnelle,
			               capacité à respecter l'organisation collective du travail, etc."
                           CssClass="EP_EtiComplement" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="2" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
        <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero313" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille313" runat="server" Height="35px" Width="748px"
                           Text="3. Les capacités professionnelles et relationnelles"
                           CssClass="EP_TitreChapitre" style="margin-bottom: 4px; padding-top: 5px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations313" runat="server" Height="40px" Width="746px"
                           Text="Autonomie, discernement et sens des initiatives dans l'exercice de ses attributions,
			               capacité d'adaptation, capacité à travailler en équipe, etc."
                           CssClass="EP_EtiComplement" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="3" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
        <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero314" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille314" runat="server" Height="35px" Width="748px"
                           Text="4. Aptitude à l'encadrement et/ou à la conduite de projets"
                           Tooltip="Le cas échéant"
                           CssClass="EP_TitreChapitre" style="margin-bottom: 4px; padding-top: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations314" runat="server" Height="40px" Width="746px"
                           Text="Capacité d'organisation et de pilotage, aptitude à la conduite de projets, capacité à déléguer,
			               aptitude au dialogue, à la communication et à la négociation, etc."
                           CssClass="EP_EtiComplement" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="4" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero321" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitre321" runat="server" Height="50px" Width="748px"
                           Text="3.2 Appréciation générale sur la valeur professionnelle, la manière de servir <br/> et la réalisation des objectifs"
                           CssClass="EP_TitreParagraphe" style="margin-bottom: 4px; padding-top: 9px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille321" runat="server" Height="40px" Width="748px"
                           Text="Merci d'apporter un soin particulier à cette appréciation qui constitue un critère
			               pour l'avancement de grade des agents et pourra être repris dans les rapports liés à la promotion de grade"
                           CssClass="EP_EtiComplementTer" style="margin-bottom: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreAppreciation" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreAppreciation" runat="server" Height="44px" Width="371px" Text=""
                            CssClass="EP_EtiComplement" style="margin-top: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreEnteteLabels" runat="server" CellPadding="0" CellSpacing="0">
                            <asp:TableRow> 
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTitreNote1" runat="server" Height="44px" Width="65px"
                                    Text="sans <br/> objet"
                                    CssClass="EP_EtiNiveau" Font-Size="Small" style="margin-top: 1px;">
                                </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTitreNote2" runat="server" Height="44px" Width="80px"
                                    Text="à <br/> acquérir"
                                    CssClass="EP_EtiNiveau" Font-Size="Small" style="margin-top: 1px;">
                                </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTitreNote3" runat="server" Height="44px" Width="95px"
                                    Text="à <br/> développer"
                                    CssClass="EP_EtiNiveau" Font-Size="Small" style="margin-top: 1px;">
                                </asp:Label>         
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTitreNote4" runat="server" Height="44px" Width="60px"
                                    Text="<br/> Maîtrise"
                                    CssClass="EP_EtiNiveau" Font-Size="Small" style="margin-top: 1px;">
                                </asp:Label>           
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTitreNote5" runat="server" Height="44px" Width="62px"
                                    Text="<br/> Expert"
                                    CssClass="EP_EtiNiveau" Font-Size="Small" style="margin-top: 1px;">
                                </asp:Label>          
                            </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>      
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"
                    BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHA01" runat="server"
                            V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="5"
                            Etivisible="False" DonWidth="370px" DonHeight="20px" 
                            DonText="Compétences professionnelles et technicité" V_SiEnLectureSeule="true"
                            DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Center"
                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                        <Virtualia:VSixBoutonRadio ID="RadioHA03" runat="server" V_Groupe="Critere1"
                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                            VRadioN1Width="57px" VRadioN2Width="78px" VRadioN3Width="90px"
                            VRadioN4Width="57px" VRadioN5Width="55px" VRadioN6Width="54px"
                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                            VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 60px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                            Visible="true" VRadioN1visible="false" VRadioN6visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"
                    BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHB01" runat="server"
                            V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="6"
                            Etivisible="False" DonWidth="370px" DonHeight="20px" 
                            DonText="Contribution à l'activité du service" V_SiEnLectureSeule="true"
                            DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Center"
                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                        <Virtualia:VSixBoutonRadio ID="RadioHB03" runat="server" V_Groupe="Critere2"
                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                            VRadioN1Width="57px" VRadioN2Width="78px" VRadioN3Width="90px"
                            VRadioN4Width="57px" VRadioN5Width="55px" VRadioN6Width="54px"
                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                            VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 60px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                            Visible="true" VRadioN1visible="false" VRadioN6visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"
                    BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHC01" runat="server"
                            V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="7"
                            Etivisible="False" DonWidth="370px" DonHeight="20px" 
                            DonText="Capacités professionnelles et relationnelles" V_SiEnLectureSeule="true"
                            DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Center"
                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                        <Virtualia:VSixBoutonRadio ID="RadioHC03" runat="server" V_Groupe="Critere3"
                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                            VRadioN1Width="57px" VRadioN2Width="78px" VRadioN3Width="90px"
                            VRadioN4Width="57px" VRadioN5Width="55px" VRadioN6Width="54px"
                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                            VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 60px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                            Visible="true" VRadioN1visible="false" VRadioN6visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"
                    BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHD01" runat="server"
                            V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="8"
                            Etivisible="False" DonWidth="370px" DonHeight="20px" 
                            DonText="Aptitude à l'encadrement et/ou à la conduite de projets" V_SiEnLectureSeule="true"
                            DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Center"
                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                        <Virtualia:VSixBoutonRadio ID="RadioHD03" runat="server" V_Groupe="Critere4"
                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                            VRadioN1Width="57px" VRadioN2Width="78px" VRadioN3Width="90px"
                            VRadioN4Width="57px" VRadioN5Width="55px" VRadioN6Width="54px"
                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                            VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                            Visible="true" VRadioN6visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero322" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille322" runat="server" Height="35px" Width="748px"
                           Text="Réalisation des objectifs de l'année écoulée"
                           CssClass="EP_TitreChapitre" style="margin-bottom: 4px; padding-top: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="9" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero323" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille323" runat="server" Height="35px" Width="748px"
                           Text="Appréciation littérale"
                           CssClass="EP_TitreChapitre" style="margin-bottom: 4px; padding-top: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="250px" EtiHeight="20px" DonTabIndex="10" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>