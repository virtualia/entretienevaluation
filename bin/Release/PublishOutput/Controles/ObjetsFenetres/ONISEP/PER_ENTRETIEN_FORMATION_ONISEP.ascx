﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_FORMATION_ONISEP.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_FORMATION_ONISEP" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreFormation" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN DE FORMATION" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="BILAN DES FORMATIONS ET BESOINS DE FORMATION" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="38px" Width="746px"
                           Text="1. Bilan des formations suivies sur la période écoulée"
                           CssClass="EP_TitreParagraphe" style="padding-top: 4px; text-indent: 10px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="ListeFormationsSuivies" runat="server" CadreWidth="746px" SiColonneSelect="false"/>
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="30px" Width="746px"
                           Text="Commentaire sur les formations suivies et leur mise en oeuvre dans le poste"
                           CssClass="EP_EtiTableau" Font-Size="Medium"
                           style="margin-bottom: 1px; padding-top: 4px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVR03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="746px" DonHeight="190px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="false"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="38px" Width="746px"
                           Text="2. Compétence à acquérir ou développer pour tenir le poste"
                           CssClass="EP_TitreParagraphe" style="padding-top: 4px; text-indent: 10px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin102" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage102" runat="server" Height="25px" Width="390px" Text="Intitulé"
                               CssClass="EP_EtiComplement" style="font-style: normal; text-indent: 1px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode102" runat="server" Height="25px" Width="350px"
                               Text="Périodes souhaitées"
                               CssClass="EP_EtiComplement" style="font-style: normal; text-indent: 1px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="2" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>        
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                              <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>               
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="6" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="7" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>  
                          </asp:TableCell>                           
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="8" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="9" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="10" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="11" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <asp:Label ID="LabelSiFormationUrgente" runat="server" Height="35px" Width="385px"
                                Text="Une action de formation permettant d'acquérir ou de développer ces compétences doit-elle être suivie rapidement ?"
                                CssClass="EP_EtiComplement" style="font-style: normal; padding-top: 5px; text-indent: 0px;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left">        
                            <Virtualia:VSixBoutonRadio ID="RadioH29" runat="server" V_Groupe="SiFormationUrgente"
                                V_PointdeVue="1" V_Objet="150" V_Information="29" V_SiDonneeDico="true"
                                VRadioN1Width="80px" VRadioN2Width="80px" VRadioN3Width="0px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px" VRadioN2Style="margin-left: 0px; margin-top: 0px" VRadioN3Style="margin-left: 0px; margin-top: 0px"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px" VRadioN5Style="margin-left: 0px; margin-top: 0px" VRadioN6Style="margin-left: 0px; margin-top: 0px" 
                                VRadioN1Text="Non" VRadioN2Text="Oui" 
                                VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell> 
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin103" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="LabelTypeBesoin103" runat="server" Height="38px" Width="746px"
                               Text="3. Compétences à acquérir ou développer en vue d'une évolution professionnelle"
                               CssClass="EP_TitreParagraphe" style="padding-top: 4px; text-indent: 10px;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                          <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="LabelTypeBesoin103Bis" runat="server" Height="20px" Width="746px"
                               Text="(à compléter en fonction des perspectives d'évolution professionnelle)"
                               CssClass="EP_EtiComplement" style="margin-bottom: 10px; font-style: oblique; text-indent: 3px;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage103" runat="server" Height="25px" Width="390px" Text="Intitulé"
                               CssClass="EP_EtiComplement" style="font-style: normal; text-indent: 1px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode103" runat="server" Height="25px" Width="350px" Text="Echéances envisagées"
                               CssClass="EP_EtiComplement" style="font-style: normal; text-indent: 1px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="12" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="13" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="14" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="15" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="16" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="17" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="18" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="19" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="20" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="21" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin104" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="LabelTypeBesoin104" runat="server" Height="38px" Width="746px"
                               Text="4. Autres perspectives de formation"
                               CssClass="EP_TitreParagraphe" style="padding-top: 4px; text-indent: 10px;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage104" runat="server" Height="25px" Width="390px" Text="Intitulé"
                               CssClass="EP_EtiComplement" style="font-style: normal; text-indent: 1px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode104" runat="server" Height="25px" Width="350px" Text="Echéances envisagées et durée prévue"
                               CssClass="EP_EtiComplement" style="font-style: normal; text-indent: 1px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="22" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="23" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="24" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="25" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="26" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="27" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="28" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="29" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="30" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center">        
                             <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="345px" DonHeight="36px" DonTabIndex="31" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/> 
                          </asp:TableCell>                            
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="TableDIF" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitre105" runat="server" Height="38px" Width="746px"
                           Text="5. Utilisation du droit individuel à la formation (DIF)"
                           CssClass="EP_TitreParagraphe" style="margin-bottom: 10px; padding-top: 4px; text-indent: 10px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelTitreDIF" runat="server" Height="30px" Width="340px"
                            Text="Solde du DIF au 1er janvier de l'année en cours :"
                            CssClass="EP_EtiComplement" style="font-style: normal; text-indent: 10px;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelSoldeDIF" runat="server" Height="30px" Width="400px" Text=""
                            CssClass="EP_EtiComplement" style="font-style: normal; text-indent: 2px;">
                        </asp:Label>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="LabelSiUtilisationDIF" runat="server" Height="25px" Width="340px"
                            Text="L'agent envisage-t-il de mobiliser son DIF cette année ?"
                            CssClass="EP_EtiComplement" style="font-style: normal; text-indent: 10px;">
                         </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                       <Virtualia:VSixBoutonRadio ID="RadioH19" runat="server" V_Groupe="SiUtilisationDIF"
                           V_PointdeVue="1" V_Objet="150" V_Information="19" V_SiDonneeDico="true"
                           VRadioN1Width="80px" VRadioN2Width="80px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                           VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px" VRadioN2Style="margin-left: 0px; margin-top: 0px" VRadioN3Style="margin-left: 0px; margin-top: 0px"
                           VRadioN4Style="margin-left: 0px; margin-top: 0px" VRadioN5Style="margin-left: 0px; margin-top: 0px" VRadioN6Style="margin-left: 0px; margin-top: 0px" 
                           VRadioN1Text="Non" VRadioN2Text="Oui" 
                           VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell> 
                </asp:TableRow>         
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreSignatures" runat="server" Height="35px" Width="746px"
                           Text="Compte rendu d'entretien de formation - Signatures"
                           CssClass="EP_TitreParagraphe" style="margin-bottom: 10px; padding-top: 7px;text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelObservationsAgent" runat="server" Height="20px" Width="748px" Text="L'agent :"
                           CssClass="EP_EtiComplement" style="font-style: normal; padding-top: 4px; text-indent: 10px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
          	    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH20" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="20" V_SiDonneeDico="true"
                           EtiWidth="300px" DonWidth="80px" DonTabIndex="55"
                           Etistyle="margin-left: 0px;" 
	                       EtiText="Date de signature de l'agent"/>
                    </asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelObservationsEvaluateur" runat="server" Height="20px" Width="748px" Text="Le supérieur hiérarchique :"
                           CssClass="EP_EtiComplement" style="font-style: normal; padding-top: 4px; text-indent: 10px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
          	    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH21" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="21" V_SiDonneeDico="true"
                           Etistyle="margin-left: 0px;" 
                           EtiWidth="300px" DonWidth="80px" DonTabIndex="57" EtiText="Date de signature du supérieur hiérarchique"/>
                    </asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>