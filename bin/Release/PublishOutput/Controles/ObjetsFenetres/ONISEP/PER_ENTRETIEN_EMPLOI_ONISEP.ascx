﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_EMPLOI_ONISEP.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_EMPLOI_ONISEP" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderColor="#B0E0D7" 
    BorderStyle="None" BorderWidth="2px" HorizontalAlign="Center" 
    style="margin-top: 1px;" Visible="true" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreEmploi" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="1. DESCRIPTION DU POSTE OCCUPE" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreStructure" runat="server" Height="22px" Width="750px"
                           Text="Structure :"
                           CssClass="EP_EtiComplement" style="margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreAffectation" runat="server" Height="40px" Width="750px"
                           Text="Positionnement du poste : "
                           CssClass="EP_EtiComplement" style="margin-bottom: 8px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="5" V_SiDonneeDico="true"
                           EtiWidth="120px" EtiHeight="26px" DonWidth="612px" DonHeight="20px" DonTabIndex="1" 
                           Etitext="Intitulé du poste" EtiVisible="true" Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="6" V_SiDonneeDico="true"
                           EtiWidth="150px" EtiHeight="40px" DonWidth="582px" DonHeight="20px" DonTabIndex="2"
                           Etitext="Emploi type <br/> (cf. REME ou REFERENS)" EtiVisible="true" Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align:  left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero3" runat="server" Height="30px" Width="742px"
                           Text="Missions du poste"
                           CssClass="EP_TitreChapitre" style="padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV07" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="7" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" DonTabIndex="3"
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 3px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="7"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="7">
                        <asp:Label ID="LabelTitreNumero4" runat="server" Height="30px" Width="748px"
                           Text="Fonctions d'encadrement ou de conduite de projet"
                           CssClass="EP_TitreChapitre" style="margin-bottom: 8px; padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="LabelSiConduiteProjet" runat="server" Height="24px" Width="423px"
                            Text="- l'agent assume-t-il des fonctions de conduite de projet ?"
                            CssClass="EP_EtiComplement" Font-Size="Medium" style="font-style: oblique;">
                         </asp:Label>          
                    </asp:TableCell>
                   <asp:TableCell HorizontalAlign="Left" ColumnSpan="5">        
                       <Virtualia:VTrioHorizontalRadio ID="RadioH25" runat="server" V_Groupe="SiConduiteProjet"
                           V_PointdeVue="1" V_Objet="150" V_Information="25" V_SiDonneeDico="true"
                           RadioGaucheWidth="60px" RadioCentreWidth="60px" RadioDroiteWidth="0px"  
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteVisible="false"
                           RadioGaucheHeight="25px" RadioCentreHeight="25px"
                           RadioCentreStyle="text-align:  center;"  
                           RadioGaucheStyle="text-align:  center;" Visible="true"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelCadrage2" runat="server" Height="22px" Width="47px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none" Text="">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="25px" ColumnSpan="7"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="LabelSiEncadrement" runat="server" Height="24px" Width="423px"
                            Text="- l'agent assume-t-il des fonctions d'encadrement ?"
                            CssClass="EP_EtiComplement" Font-Size="Medium" style="font-style: oblique;">
                         </asp:Label>          
                    </asp:TableCell>
                   <asp:TableCell HorizontalAlign="Left" ColumnSpan="5">        
                       <Virtualia:VTrioHorizontalRadio ID="RadioH08" runat="server" V_Groupe="SiEncadrement"
                           V_PointdeVue="1" V_Objet="150" V_Information="8" V_SiDonneeDico="true"
                           RadioGaucheWidth="60px" RadioCentreWidth="60px" RadioDroiteWidth="0px"  
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteVisible="false"
                           RadioGaucheHeight="25px" RadioCentreHeight="25px"
                           RadioCentreStyle="text-align:  center;"  
                           RadioGaucheStyle="text-align:  center;" Visible="true"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelCadrage1" runat="server" Height="22px" Width="47px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none" Text="">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="7">
                       <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="9" V_SiDonneeDico="true"
                           EtiWidth="325px" EtiHeight="22px" DonHeight="20px" DonWidth="50px" DonTabIndex="4" 
                           EtiText="Si oui, préciser le nombre d'agents encadrés"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                       <Virtualia:VCoupleEtiDonnee ID="InfoH26" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="26" V_SiDonneeDico="true"
                           EtiWidth="383px" EtiHeight="22px" DonHeight="20px" DonWidth="30px" DonTabIndex="5" 
                           EtiText="et leur répartition par catégorie"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelCategorieA" runat="server" Height="24px" Width="24px" Text="A"
                           CssClass="EP_TitreChapitre" style="margin-top: 0px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                       <Virtualia:VCoupleEtiDonnee ID="InfoH27" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="27" V_SiDonneeDico="true"
                           Etivisible="false" DonHeight="20px" DonWidth="30px" DonTabIndex="6"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelCategorieB" runat="server" Height="24px" Width="24px" Text="B"
                           CssClass="EP_TitreChapitre" style="margin-top: 0px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                       <Virtualia:VCoupleEtiDonnee ID="InfoH28" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="28" V_SiDonneeDico="true"
                           Etivisible="false" DonHeight="20px" DonWidth="30px" DonTabIndex="7"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelCategorieC" runat="server" Height="24px" Width="24px" Text="C"
                           CssClass="EP_TitreChapitre" style="margin-top: 0px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelCadrage3" runat="server" Height="22px" Width="157px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none" Text="">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
</asp:Table>