﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_FORMATION_HONDA.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_FORMATION_HONDA" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioVerticalRadio.ascx" tagname="VTrioVerticalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreEnteteComportement" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#2FA49B">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                        <asp:Label ID="Etiquette" runat="server" Text="BESOINS EN FORMATION - 90 KI" Height="30px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre1" runat="server" Text="" Height="25px" Width="75px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#0E5F5C"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre2" runat="server" Text="Nom" Height="25px" Width="200px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre3" runat="server" Text="Département" Height="25px" Width="235px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre4" runat="server" Text="Fonction" Height="25px" Width="230px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletCollaborateur" runat="server" Text="Collaborateur" Height="30px" Width="75px"
                            BackColor="Transparent"  BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 15px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteCollaborateur" runat="server" Text="" Height="35px" Width="200px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDepartementCollaborateur" runat="server" Text="" Height="35px" Width="235px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFonctionCollaborateur" runat="server" Text="" Height="35px" Width="230px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletEvaluateur" runat="server" Text="Evaluateur" Height="30px" Width="75px"
                            BackColor="Transparent"  BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 15px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteEvaluateur" runat="server" Text="" Height="35px" Width="200px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDepartementEvaluateur" runat="server" Text="" Height="35px" Width="235px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFonctionEvaluateur" runat="server" Text="" Height="35px" Width="230px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletPeriodeKI" runat="server" Text="Période KI" Height="19px" Width="75px"
                            BackColor="Transparent"  BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelPeriodeKI" runat="server" Text="" Height="19px" Width="200px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: center; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletRevision" runat="server" Text="Date de révision" Height="19px" Width="235px"
                            BackColor="Transparent"  BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: right; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDateRevision" runat="server" Text="" Height="19px" Width="230px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="4"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreFormationsSuivies" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="38px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Formations suivies"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Left"> 
                        <Virtualia:VListeGrid ID="ListeFormationsSuivies" runat="server" CadreWidth="750px" SiColonneSelect="false"/>
                    </asp:TableCell>
                </asp:TableRow>                     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero11" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left" Height="25px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="38px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Besoins en formation"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left" Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" BackColor="#124545" BorderColor="#8DA8A3" BorderStyle="NotSet">
                        <asp:Label ID="EnteteBesoinN1" runat="server" Height="36px" Width="254px"
                            BackColor="#124545" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Demande de formation N°1"
                            BorderWidth="1px" ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                            font-style: normal; text-indent: 5px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Table ID="TableLiensSitesWeb1" runat="server" CellPadding="0" CellSpacing="0" Width="494px"  HorizontalAlign="Left"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN11" runat="server" NavigateUrl="http://www.asmfp.com" Text="ASMFP" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                      style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="100px">
                                   </asp:HyperLink> 
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN12" runat="server" NavigateUrl="http://www.cegos.fr" Text="CEGOS" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="150px">
                                   </asp:HyperLink>
                                </asp:TableCell>
				                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN13" runat="server" NavigateUrl="http://www.formation-anglais-professionnelle.com" Text="Anglais individuel ou collectif" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="200px">
                                   </asp:HyperLink>     
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN14" runat="server" NavigateUrl="http://www.ism.fr" Text="ISM" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="100px">
                                   </asp:HyperLink>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN15" runat="server" NavigateUrl="http://www.telelangue.com" Text="Anglais Téléphone" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="150px">
                                   </asp:HyperLink>
                                </asp:TableCell>
				                 <asp:TableCell>
                                   <asp:HyperLink ID="SiteN16" runat="server" NavigateUrl="http://www.woospeak.com" Text="WOOSPEAK" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="200px">
                                   </asp:HyperLink>
                                </asp:TableCell>
                            </asp:TableRow>        
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left" Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero12" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelLegendeN1T11" runat="server" Height="15px" Width="28px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(*)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelLegendeN1T12" runat="server" Height="15px" Width="720px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T1 : adaptation au poste de travail"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelLegendeN1T21" runat="server" Height="15px" Width="28px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelLegendeN1T22" runat="server" Height="15px" Width="720px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T2 : développement des compétences"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left" Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreEnteteDemandeN1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteExpressionDemande1" runat="server" Height="40px" Width="427px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Formation envisagée"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteCout1" runat="server" Height="40px" Width="70px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Coût HT <br/> (€)"   
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteDureeFormation1" runat="server" Height="40px" Width="70px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Durée"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEntetePeriode1" runat="server" Height="40px" Width="122px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Période <br/> Dates choisies"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteType1" runat="server" Height="40px" Width="55px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Type (*)"   
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDemandeN1" runat="server" CellPadding="0" CellSpacing="0" Width="412px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="TableActionFormation1" runat="server" CellPadding="0" CellSpacing="0" Width="412px"
                                    BackColor="Transparent" BorderStyle="None">
                                      <asp:TableRow>
				                        <asp:TableCell>
                                           <asp:Label ID="LabelTitreAction1" runat="server" Height="20px" Width="252px"
                                                BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                                Text="Intitulé"
                                                ForeColor="#0E5F5C" Font-Italic="False"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                                font-style: normal; text-indent: 4px; text-align:  left;">
                                           </asp:Label>
                                        </asp:TableCell>
				                        <asp:TableCell>
                                           <asp:Label ID="LabelTitreReference1" runat="server" Height="20px" Width="60px"
                                                BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                                Text="Référence"
                                                ForeColor="#0E5F5C" Font-Italic="False"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                                font-style: normal; text-indent: 0px; text-align: center;">
                                           </asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <Virtualia:VCoupleEtiDonnee ID="InfoHA02" runat="server"
                                               V_PointdeVue="1" V_Objet="153" V_Information="2" V_SiDonneeDico="true"
                                               DonWidth="100px" DonHeight="20px" DonTabIndex="1" 
                                               Etivisible="false" Donstyle="margin-left: 0px; text-align: center;"
                                               DonBorderWidth="1px"/>
                                        </asp:TableCell>
                                      </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="412px" DonHeight="35px" DonTabIndex="2" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VListeCombo ID="ListCA03" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="153" V_Information="3" V_SiDonneeDico="true" V_NomTable="Domaine de formation"
                                      LstWidth="416px" LstHeight="22px" LstBorderWidth="0px"  
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionsN1" runat="server" Height="20px" Width="412px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisions sur le contenu"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA04" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="4" V_SiDonneeDico="true"
                                        DonWidth="412px" DonHeight="55px" DonTabIndex="3" 
                                        Etivisible="false" Donstyle="margin-left: 2px; Font-Names: Trebuchet MS; Font-Size: 90%"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjFormationN1" runat="server" Height="20px" Width="412px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs de la formation"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA09" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                        DonWidth="412px" DonHeight="55px" DonTabIndex="4" 
                                        Etivisible="false" Donstyle="margin-left: 2px; Font-Names: Trebuchet MS; Font-Size: 90%"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHA08" runat="server"
                           V_PointdeVue="1" V_Objet="153" V_Information="8" V_SiDonneeDico="true"
                           DonWidth="68px" DonHeight="20px" DonTabIndex="5" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDureeJours1" runat="server" CellPadding="0" CellSpacing="0" Width="70px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHA06" runat="server"
                                       V_PointdeVue="1" V_Objet="153" V_Information="6" V_SiDonneeDico="true"
                                       DonWidth="48px" DonHeight="20px" DonTabIndex="6" 
                                       Etivisible="false" Donstyle="margin-left: 0px;"
                                       DonBorderWidth="1px"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:Label ID="LabelDureeJours1" runat="server" Height="20px" Width="6px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="j."
                                        ForeColor="#2FA49B" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 0px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHA07" runat="server"
                                       V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                       DonWidth="48px" DonHeight="20px" DonTabIndex="7" 
                                       Etivisible="false" Donstyle="margin-left: 0px;"
                                       DonBorderWidth="1px"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:Label ID="LabelDureeHeures1" runat="server" Height="20px" Width="6px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text=""
                                        ForeColor="#2FA49B" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: -2px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 0px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="153" V_Information="5" V_SiDonneeDico="true"
                            DonWidth="114px" DonHeight="80px" DonTabIndex="8" 
                            Etivisible="false" Donstyle="margin-left: 2px; Font-Names: Trebuchet MS; Font-Size: 90%"
                            DonBorderWidth="1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VTrioVerticalRadio ID="RadioVA17" runat="server"  
                            V_PointdeVue="1" V_Objet="153" V_Information="17" V_SiDonneeDico="true" 
                            RadioGaucheText="T1" RadioCentreText="T2"
                            V_Groupe="GroupeT12A" RadioDroiteVisible="False"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" 
                            RadioDroiteStyle="text-align: left; Font-size: 90%" />
                    </asp:TableCell>    
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="3px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreValidation1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BorderStyle="Notset" BorderWidth="1px" BorderColor="#A8BBB8">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteValidManager1" runat="server" Height="25px" Width="149px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Evaluateur"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteValidNPlus11" runat="server" Height="25px" Width="409px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Direction"   
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteValidDRH1" runat="server" Height="25px" Width="186px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="DRH"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHA10" runat="server"
                            V_PointdeVue="1" V_Objet="153" V_Information="10" V_SiDonneeDico="true"
                            DonWidth="100px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="9" 
                            Donstyle="margin-left: 0px; text-align: center;" 
                            Etivisible="True" EtiWidth="33px" EtiBackColor="Transparent" EtiBorderStyle="None" EtiForeColor="#2FA49B" EtiText="Date"  
                            Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHA13" runat="server"
                            V_PointdeVue="1" V_Objet="153" V_Information="13" V_SiDonneeDico="true"
                            DonWidth="100px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="10" 
                            Donstyle="margin-left: 0px; text-align: center;"
                            Etivisible="True" EtiWidth="33px" EtiBackColor="Transparent" EtiBorderStyle="None" EtiForeColor="#2FA49B" EtiText="Date" 
                            Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHA15" runat="server"
                            V_PointdeVue="1" V_Objet="153" V_Information="15" V_SiDonneeDico="true"
                            DonWidth="100px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="11" 
                            Donstyle="margin-left: 0px; text-align: center;"
                            Etivisible="True" EtiWidth="33px" EtiBackColor="Transparent" EtiBorderStyle="None" EtiForeColor="#2FA49B" EtiText="Date" 
                            Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelValidManager1" runat="server" Height="25px" Width="29px"
                           BackColor="Transparent" BorderStyle="None" BorderWidth="1px"
                           Text="" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioHorizontalRadio ID="RadioHA12" runat="server" V_Groupe="ValidationN11"
                           V_PointdeVue="1" V_Objet="153" V_Information="12" V_SiDonneeDico="true"
                           RadioGaucheWidth="85px" RadioCentreWidth="160px" RadioDroiteWidth="85px"  
                           RadioGaucheText="Accord" RadioCentreText="Refus avec report" RadioDroiteText="Refus" 
                           RadioGaucheHeight="20px" RadioCentreHeight="20px" RadioDroiteHeight="20px"
                           RadioCentreStyle="text-align: left;" RadioGaucheStyle="text-align: left;" RadioDroiteStyle="text-align: left;" 
			               Visible="true"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioHorizontalRadio ID="RadioHA14" runat="server" V_Groupe="ValidationDRH1"
                           V_PointdeVue="1" V_Objet="153" V_Information="14" V_SiDonneeDico="true"
                           RadioGaucheWidth="70px" RadioCentreWidth="70px" RadioDroiteWidth="0px"  
                           RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
			               RadioGaucheHeight="20px" RadioCentreHeight="20px"
                           RadioCentreStyle="text-align: left;" RadioGaucheStyle="text-align: left;"
			               Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA16" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="153" V_Information="16" V_SiDonneeDico="true"
                           DonWidth="744px" DonHeight="45px" DonTabIndex="12" 
                           Etivisible="false" Donstyle="margin-left: 0px; Font-Names: Trebuchet MS; Font-Size: 90%"
                           DonBorderWidth="1px" DonToolTip="Commentaire"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN1" runat="server" Height="35px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero21" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left" Height="1px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" BackColor="#124545" BorderColor="#8DA8A3" BorderStyle="NotSet">
                        <asp:Label ID="EnteteBesoinN2" runat="server" Height="36px" Width="254px"
                            BackColor="#124545" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Demande de formation N°2"
                            BorderWidth="1px" ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                            font-style: normal; text-indent: 5px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Table ID="TableLiensSitesWeb2" runat="server" CellPadding="0" CellSpacing="0" Width="494px"  HorizontalAlign="Left"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN21" runat="server" NavigateUrl="http://www.asmfp.com" Text="ASMFP" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                      style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="100px">
                                   </asp:HyperLink> 
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN22" runat="server" NavigateUrl="http://www.cegos.fr" Text="CEGOS" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="150px">
                                   </asp:HyperLink>
                                </asp:TableCell>
				                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN23" runat="server" NavigateUrl="http://www.formation-anglais-professionnelle.com" Text="Anglais individuel ou collectif" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="200px">
                                   </asp:HyperLink>     
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN24" runat="server" NavigateUrl="http://www.ism.fr" Text="ISM" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="100px">
                                   </asp:HyperLink>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN25" runat="server" NavigateUrl="http://www.telelangue.com" Text="Anglais Téléphone" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="150px">
                                   </asp:HyperLink>
                                </asp:TableCell>
				                 <asp:TableCell>
                                   <asp:HyperLink ID="SiteN26" runat="server" NavigateUrl="http://www.woospeak.com" Text="WOOSPEAK" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="200px">
                                   </asp:HyperLink>
                                </asp:TableCell>
                            </asp:TableRow>        
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left" Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero22" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelLegendeN2T11" runat="server" Height="15px" Width="28px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(*)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelLegendeN2T12" runat="server" Height="15px" Width="720px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T1 : adaptation au poste de travail"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelLegendeN2T21" runat="server" Height="15px" Width="28px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelLegendeN2T22" runat="server" Height="15px" Width="720px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T2 : développement des compétences"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left" Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreEnteteDemandeN2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteExpressionDemande2" runat="server" Height="40px" Width="427px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Formation envisagée"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteCout2" runat="server" Height="40px" Width="70px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Coût HT <br/> (€)"   
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteDureeFormation2" runat="server" Height="40px" Width="70px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Durée"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEntetePeriode2" runat="server" Height="40px" Width="122px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Période <br/> Dates choisies"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteType2" runat="server" Height="40px" Width="55px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Type (*)"   
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDemandeN2" runat="server" CellPadding="0" CellSpacing="0" Width="412px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="TableActionFormation2" runat="server" CellPadding="0" CellSpacing="0" Width="412px"
                                    BackColor="Transparent" BorderStyle="None">
                                      <asp:TableRow>
				                        <asp:TableCell>
                                           <asp:Label ID="LabelTitreAction2" runat="server" Height="20px" Width="252px"
                                                BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                                Text="Intitulé"
                                                ForeColor="#0E5F5C" Font-Italic="False"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                                font-style: normal; text-indent: 4px; text-align:  left;">
                                           </asp:Label>
                                        </asp:TableCell>
				                        <asp:TableCell>
                                           <asp:Label ID="LabelTitreReference2" runat="server" Height="20px" Width="60px"
                                                BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                                Text="Référence"
                                                ForeColor="#0E5F5C" Font-Italic="False"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                                font-style: normal; text-indent: 0px; text-align: center;">
                                           </asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <Virtualia:VCoupleEtiDonnee ID="InfoHB02" runat="server"
                                               V_PointdeVue="1" V_Objet="153" V_Information="2" V_SiDonneeDico="true"
                                               DonWidth="100px" DonHeight="20px" DonTabIndex="13" 
                                               Etivisible="false" Donstyle="margin-left: 0px; text-align: center;"
                                               DonBorderWidth="1px"/>
                                        </asp:TableCell>
                                      </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="412px" DonHeight="35px" DonTabIndex="14" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VListeCombo ID="ListCB03" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="153" V_Information="3" V_SiDonneeDico="true" V_NomTable="Domaine de formation"
                                      LstWidth="416px" LstHeight="22px" LstBorderWidth="0px"  
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionsN2" runat="server" Height="20px" Width="412px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisions sur le contenu"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB04" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="4" V_SiDonneeDico="true"
                                        DonWidth="412px" DonHeight="55px" DonTabIndex="15" 
                                        Etivisible="false" Donstyle="margin-left: 2px; Font-Names: Trebuchet MS; Font-Size: 90%"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjFormationN2" runat="server" Height="20px" Width="412px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs de la formation"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB09" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                        DonWidth="412px" DonHeight="55px" DonTabIndex="16" 
                                        Etivisible="false" Donstyle="margin-left: 2px; Font-Names: Trebuchet MS; Font-Size: 90%"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHB08" runat="server"
                           V_PointdeVue="1" V_Objet="153" V_Information="8" V_SiDonneeDico="true"
                           DonWidth="68px" DonHeight="20px" DonTabIndex="17" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDureeJours2" runat="server" CellPadding="0" CellSpacing="0" Width="70px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHB06" runat="server"
                                       V_PointdeVue="1" V_Objet="153" V_Information="6" V_SiDonneeDico="true"
                                       DonWidth="48px" DonHeight="20px" DonTabIndex="18" 
                                       Etivisible="false" Donstyle="margin-left: 0px;"
                                       DonBorderWidth="1px"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:Label ID="LabelDureeJours2" runat="server" Height="20px" Width="6px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="j."
                                        ForeColor="#2FA49B" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 0px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHB07" runat="server"
                                       V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                       DonWidth="48px" DonHeight="20px" DonTabIndex="19" 
                                       Etivisible="false" Donstyle="margin-left: 0px;"
                                       DonBorderWidth="1px"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:Label ID="LabelDureeHeures2" runat="server" Height="20px" Width="6px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text=""
                                        ForeColor="#2FA49B" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: -2px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 0px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="153" V_Information="5" V_SiDonneeDico="true"
                            DonWidth="114px" DonHeight="80px" DonTabIndex="20" 
                            Etivisible="false" Donstyle="margin-left: 2px; Font-Names: Trebuchet MS; Font-Size: 90%"
                            DonBorderWidth="1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VTrioVerticalRadio ID="RadioVB17" runat="server"  
                            V_PointdeVue="1" V_Objet="153" V_Information="17" V_SiDonneeDico="true" 
                            RadioGaucheText="T1" RadioCentreText="T2"
                            V_Groupe="GroupeT12B" RadioDroiteVisible="False"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" 
                            RadioDroiteStyle="text-align: left; Font-size: 90%" />
                    </asp:TableCell>    
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="3px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreValidation2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BorderStyle="Notset" BorderWidth="1px" BorderColor="#A8BBB8">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteValidManager2" runat="server" Height="25px" Width="149px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Evaluateur"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteValidNPlus12" runat="server" Height="25px" Width="409px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Direction"   
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteValidDRH2" runat="server" Height="25px" Width="186px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="DRH"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHB10" runat="server"
                            V_PointdeVue="1" V_Objet="153" V_Information="10" V_SiDonneeDico="true"
                            DonWidth="100px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="21" 
                            Donstyle="margin-left: 0px; text-align: center;" 
                            Etivisible="True" EtiWidth="33px" EtiBackColor="Transparent" EtiBorderStyle="None" EtiForeColor="#2FA49B" EtiText="Date"  
                            Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHB13" runat="server"
                            V_PointdeVue="1" V_Objet="153" V_Information="13" V_SiDonneeDico="true"
                            DonWidth="100px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="22" 
                            Donstyle="margin-left: 0px; text-align: center;"
                            Etivisible="True" EtiWidth="33px" EtiBackColor="Transparent" EtiBorderStyle="None" EtiForeColor="#2FA49B" EtiText="Date" 
                            Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHB15" runat="server"
                            V_PointdeVue="1" V_Objet="153" V_Information="15" V_SiDonneeDico="true"
                            DonWidth="100px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="23" 
                            Donstyle="margin-left: 0px; text-align: center;"
                            Etivisible="True" EtiWidth="33px" EtiBackColor="Transparent" EtiBorderStyle="None" EtiForeColor="#2FA49B" EtiText="Date" 
                            Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelValidManager2" runat="server" Height="25px" Width="29px"
                           BackColor="Transparent" BorderStyle="None" BorderWidth="1px"
                           Text="" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioHorizontalRadio ID="RadioHB12" runat="server" V_Groupe="ValidationN21"
                           V_PointdeVue="1" V_Objet="153" V_Information="12" V_SiDonneeDico="true"
                           RadioGaucheWidth="85px" RadioCentreWidth="160px" RadioDroiteWidth="85px"  
                           RadioGaucheText="Accord" RadioCentreText="Refus avec report" RadioDroiteText="Refus" 
                           RadioGaucheHeight="20px" RadioCentreHeight="20px" RadioDroiteHeight="20px"
                           RadioCentreStyle="text-align: left;" RadioGaucheStyle="text-align: left;" RadioDroiteStyle="text-align: left;" 
			               Visible="true"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioHorizontalRadio ID="RadioHB14" runat="server" V_Groupe="ValidationDRH2"
                           V_PointdeVue="1" V_Objet="153" V_Information="14" V_SiDonneeDico="true"
                           RadioGaucheWidth="70px" RadioCentreWidth="70px" RadioDroiteWidth="0px"  
                           RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
			               RadioGaucheHeight="20px" RadioCentreHeight="20px"
                           RadioCentreStyle="text-align: left;" RadioGaucheStyle="text-align: left;"
			               Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB16" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="153" V_Information="16" V_SiDonneeDico="true"
                           DonWidth="744px" DonHeight="45px" DonTabIndex="24" 
                           Etivisible="false" Donstyle="margin-left: 0px; Font-Names: Trebuchet MS; Font-Size: 90%"
                           DonBorderWidth="1px" DonToolTip="Commentaire"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN2" runat="server" Height="35px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero31" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left" Height="1px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" BackColor="#124545" BorderColor="#8DA8A3" BorderStyle="NotSet">
                        <asp:Label ID="EnteteBesoinN3" runat="server" Height="36px" Width="254px"
                            BackColor="#124545" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Demande de formation N°3"
                            BorderWidth="1px" ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                            font-style: normal; text-indent: 5px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Table ID="TableLiensSitesWeb3" runat="server" CellPadding="0" CellSpacing="0" Width="494px"  HorizontalAlign="Left"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN31" runat="server" NavigateUrl="http://www.asmfp.com" Text="ASMFP" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                      style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="100px">
                                   </asp:HyperLink> 
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN32" runat="server" NavigateUrl="http://www.cegos.fr" Text="CEGOS" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="150px">
                                   </asp:HyperLink>
                                </asp:TableCell>
				                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN33" runat="server" NavigateUrl="http://www.formation-anglais-professionnelle.com" Text="Anglais individuel ou collectif" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="200px">
                                   </asp:HyperLink>     
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN34" runat="server" NavigateUrl="http://www.ism.fr" Text="ISM" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="100px">
                                   </asp:HyperLink>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:HyperLink ID="SiteN35" runat="server" NavigateUrl="http://www.telelangue.com" Text="Anglais Téléphone" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="150px">
                                   </asp:HyperLink>
                                </asp:TableCell>
				                 <asp:TableCell>
                                   <asp:HyperLink ID="SiteN36" runat="server" NavigateUrl="http://www.woospeak.com" Text="WOOSPEAK" Target="_blank"
                                      BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                                      ForeColor="#2F5FBD" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: left;"
                                      Height="17px" Width="200px">
                                   </asp:HyperLink>
                                </asp:TableCell>
                            </asp:TableRow>        
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left" Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero32" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelLegendeN3T11" runat="server" Height="15px" Width="28px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(*)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelLegendeN3T12" runat="server" Height="15px" Width="720px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T1 : adaptation au poste de travail"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelLegendeN3T21" runat="server" Height="15px" Width="28px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelLegendeN3T22" runat="server" Height="15px" Width="720px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T2 : développement des compétences"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left" Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreEnteteDemandeN3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteExpressionDemande3" runat="server" Height="40px" Width="427px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Formation envisagée"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteCout3" runat="server" Height="40px" Width="70px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Coût HT <br/> (€)"   
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteDureeFormation3" runat="server" Height="40px" Width="70px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Durée"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEntetePeriode3" runat="server" Height="40px" Width="122px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Période <br/> Dates choisies"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteType3" runat="server" Height="40px" Width="55px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Type (*)"   
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDemandeN3" runat="server" CellPadding="0" CellSpacing="0" Width="412px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="TableActionFormation3" runat="server" CellPadding="0" CellSpacing="0" Width="412px"
                                    BackColor="Transparent" BorderStyle="None">
                                      <asp:TableRow>
				                        <asp:TableCell>
                                           <asp:Label ID="LabelTitreAction3" runat="server" Height="20px" Width="252px"
                                                BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                                Text="Intitulé"
                                                ForeColor="#0E5F5C" Font-Italic="False"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                                font-style: normal; text-indent: 4px; text-align:  left;">
                                           </asp:Label>
                                        </asp:TableCell>
				                        <asp:TableCell>
                                           <asp:Label ID="LabelTitreReference3" runat="server" Height="20px" Width="60px"
                                                BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                                Text="Référence"
                                                ForeColor="#0E5F5C" Font-Italic="False"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                                font-style: normal; text-indent: 0px; text-align: center;">
                                           </asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <Virtualia:VCoupleEtiDonnee ID="InfoHC02" runat="server"
                                               V_PointdeVue="1" V_Objet="153" V_Information="2" V_SiDonneeDico="true"
                                               DonWidth="100px" DonHeight="20px" DonTabIndex="25" 
                                               Etivisible="false" Donstyle="margin-left: 0px; text-align: center;"
                                               DonBorderWidth="1px"/>
                                        </asp:TableCell>
                                      </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC01" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="412px" DonHeight="35px" DonTabIndex="26" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VListeCombo ID="ListCC03" runat="server" SiLigneBlanche="true"
                                      V_PointdeVue="2" V_Objet="153" V_Information="3" V_SiDonneeDico="true" V_NomTable="Domaine de formation"
                                      LstWidth="416px" LstHeight="22px" LstBorderWidth="0px"  
                                      EtiVisible="false" LstStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelPrecisionsN3" runat="server" Height="20px" Width="412px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Précisions sur le contenu"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC04" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="4" V_SiDonneeDico="true"
                                        DonWidth="412px" DonHeight="55px" DonTabIndex="27" 
                                        Etivisible="false" Donstyle="margin-left: 2px; Font-Names: Trebuchet MS; Font-Size: 90%"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjFormationN3" runat="server" Height="20px" Width="412px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs de la formation"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC09" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="9" V_SiDonneeDico="true"
                                        DonWidth="412px" DonHeight="55px" DonTabIndex="28" 
                                        Etivisible="false" Donstyle="margin-left: 2px; Font-Names: Trebuchet MS; Font-Size: 90%"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHC08" runat="server"
                           V_PointdeVue="1" V_Objet="153" V_Information="8" V_SiDonneeDico="true"
                           DonWidth="68px" DonHeight="20px" DonTabIndex="29" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDureeJours3" runat="server" CellPadding="0" CellSpacing="0" Width="70px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHC06" runat="server"
                                       V_PointdeVue="1" V_Objet="153" V_Information="6" V_SiDonneeDico="true"
                                       DonWidth="48px" DonHeight="20px" DonTabIndex="30" 
                                       Etivisible="false" Donstyle="margin-left: 0px;"
                                       DonBorderWidth="1px"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:Label ID="LabelDureeJours3" runat="server" Height="20px" Width="6px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="j."
                                        ForeColor="#2FA49B" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 0px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHC07" runat="server"
                                       V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                       DonWidth="48px" DonHeight="20px" DonTabIndex="31" 
                                       Etivisible="false" Donstyle="margin-left: 0px;"
                                       DonBorderWidth="1px"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:Label ID="LabelDureeHeures3" runat="server" Height="20px" Width="6px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text=""
                                        ForeColor="#2FA49B" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: -2px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 0px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="153" V_Information="5" V_SiDonneeDico="true"
                            DonWidth="114px" DonHeight="80px" DonTabIndex="32" 
                            Etivisible="false" Donstyle="margin-left: 2px; Font-Names: Trebuchet MS; Font-Size: 90%"
                            DonBorderWidth="1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VTrioVerticalRadio ID="RadioVC17" runat="server"  
                            V_PointdeVue="1" V_Objet="153" V_Information="17" V_SiDonneeDico="true" 
                            RadioGaucheText="T1" RadioCentreText="T2"
                            V_Groupe="GroupeT12C" RadioDroiteVisible="False"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" 
                            RadioDroiteStyle="text-align: left; Font-size: 90%" />
                    </asp:TableCell>    
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="3px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreValidation3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BorderStyle="Notset" BorderWidth="1px" BorderColor="#A8BBB8">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteValidManager3" runat="server" Height="25px" Width="149px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Evaluateur"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteValidNPlus13" runat="server" Height="25px" Width="409px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Direction"   
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelEnteteValidDRH3" runat="server" Height="25px" Width="186px"
                            BackColor="#CAEBE4" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="DRH"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; padding-top: 5px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHC10" runat="server"
                            V_PointdeVue="1" V_Objet="153" V_Information="10" V_SiDonneeDico="true"
                            DonWidth="100px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="33" 
                            Donstyle="margin-left: 0px; text-align: center;" 
                            Etivisible="True" EtiWidth="33px" EtiBackColor="Transparent" EtiBorderStyle="None" EtiForeColor="#2FA49B" EtiText="Date"  
                            Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHC13" runat="server"
                            V_PointdeVue="1" V_Objet="153" V_Information="13" V_SiDonneeDico="true"
                            DonWidth="100px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="34" 
                            Donstyle="margin-left: 0px; text-align: center;"
                            Etivisible="True" EtiWidth="33px" EtiBackColor="Transparent" EtiBorderStyle="None" EtiForeColor="#2FA49B" EtiText="Date" 
                            Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHC15" runat="server"
                            V_PointdeVue="1" V_Objet="153" V_Information="15" V_SiDonneeDico="true"
                            DonWidth="100px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="35" 
                            Donstyle="margin-left: 0px; text-align: center;"
                            Etivisible="True" EtiWidth="33px" EtiBackColor="Transparent" EtiBorderStyle="None" EtiForeColor="#2FA49B" EtiText="Date" 
                            Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelValidManager3" runat="server" Height="25px" Width="29px"
                           BackColor="Transparent" BorderStyle="None" BorderWidth="1px"
                           Text="" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioHorizontalRadio ID="RadioHC12" runat="server" V_Groupe="ValidationN31"
                           V_PointdeVue="1" V_Objet="153" V_Information="12" V_SiDonneeDico="true"
                           RadioGaucheWidth="85px" RadioCentreWidth="160px" RadioDroiteWidth="85px"  
                           RadioGaucheText="Accord" RadioCentreText="Refus avec report" RadioDroiteText="Refus" 
                           RadioGaucheHeight="20px" RadioCentreHeight="20px" RadioDroiteHeight="20px"
                           RadioCentreStyle="text-align: left;" RadioGaucheStyle="text-align: left;" RadioDroiteStyle="text-align: left;" 
			               Visible="true"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioHorizontalRadio ID="RadioHC14" runat="server" V_Groupe="ValidationDRH3"
                           V_PointdeVue="1" V_Objet="153" V_Information="14" V_SiDonneeDico="true"
                           RadioGaucheWidth="70px" RadioCentreWidth="70px" RadioDroiteWidth="0px"  
                           RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
			               RadioGaucheHeight="20px" RadioCentreHeight="20px"
                           RadioCentreStyle="text-align: left;" RadioGaucheStyle="text-align: left;"
			               Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC16" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="153" V_Information="16" V_SiDonneeDico="true"
                           DonWidth="744px" DonHeight="45px" DonTabIndex="36" 
                           Etivisible="false" Donstyle="margin-left: 0px; Font-Names: Trebuchet MS; Font-Size: 90%"
                           DonBorderWidth="1px" DonToolTip="Commentaire"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

 </asp:Table>