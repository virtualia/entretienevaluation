﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_FORMATION_PNF.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_FORMATION_PNF" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioVerticalRadio.ascx" tagname="VTrioVerticalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreFormation" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN DE FORMATION" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="ANNEE" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreEntete" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                         <asp:Label ID="LabelTitreIntitulePoste" runat="server" Height="24px" Width="285px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="Intitulé du poste occupé :"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                         <asp:Label ID="LabelIntitulePoste" runat="server" Height="24px" Width="463px"
                            BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                         <asp:Label ID="LabelTitreDatePriseFonctions" runat="server" Height="24px" Width="285px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="Date de prise de fonction du poste actuel :"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                         <asp:Label ID="LabelDatePriseFonctions" runat="server" Height="24px" Width="463px"
                            BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>   
      
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                           EtiWidth="330px" DonWidth="320px" EtiHeight="20px" DonTabIndex="1" 
                           EtiText="Supérieur hiérarchique ayant conduit l'entretien"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="45px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="BILAN DE L'ANNEE"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="45px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="a) Actions de formation continue suivies"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="ListeFormationsSuivies" runat="server" CadreWidth="750px" SiColonneSelect="false"/>
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVT03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="250px" EtiHeight="20px" DonTabIndex="2" 
                           EtiText="Commentaires éventuels"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero14" runat="server" Height="45px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="b) Actions prévues lors du précédent entretien de formation et non suivies"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVU03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="4" 
                           EtiText="" Etivisible="false"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero15" runat="server" Height="45px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="c) Actions conduites en tant que formateur"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVY03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="5" 
                           EtiText="(intitulé des actions, nombre de jours, public bénéficiaire)" 
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="25px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="45px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="PERSPECTIVES DE L'ANNEE A VENIR"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero21" runat="server" Height="45px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="a) Actions de formation continue sollicitées"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT11" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(*)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT12" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Typologie basée sur l'objectif visé, dans le cas spécifique de l'agent, c'est à dire :"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT21" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT22" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T1 : adaptation immédiate au poste de travail"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT31" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT32" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T2 : adaptation à l'évolution prévisible des métiers"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT41" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT42" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T3 : développement de ses qualifications ou acquisition de nouvelles qualifications"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeDIF11" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(**)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeDIF12" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Sous réserve de remplir les conditions d'accès au DIF (vérification par le service formation)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT51" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT52" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="TS : dans le temps de service"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT61" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT62" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="HTS : hors du temps de service"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreEnteteDemandes1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteExpressionDemande1" runat="server" Height="50px" Width="427px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Liaison formation / réalisation missions, évolution de carrière"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEntetePriorite1" runat="server" Height="50px" Width="55px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Priorité <br/>&nbsp"   
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteTypologie1" runat="server" Height="50px" Width="62px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Typologie (*)"   
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteModalite1" runat="server" Height="50px" Width="140px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Modalité de formation (**)"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteDureeFormation1" runat="server" Height="50px" Width="60px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Nombre d'heures"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN1" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelActionFormationN1" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHA01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="415px" DonHeight="20px" DonTabIndex="6" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifsN1" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="7" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelCompetencesN1" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="8" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVA08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123A"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVA02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123A"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVA12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="true"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteA"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHA11" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="20px" DonTabIndex="9" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN1" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN2" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelActionFormationN2" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHB01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="415px" DonHeight="20px" DonTabIndex="10" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifsN2" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="11" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelCompetencesN2" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="12" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVB08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123B"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVB02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123B"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVB12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="true"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteB"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHB11" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="20px" DonTabIndex="13" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN2" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN3" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelActionFormationN3" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHC01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="415px" DonHeight="20px" DonTabIndex="14" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifsN3" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="15" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelCompetencesN3" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="16" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVC08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123C"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVC02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123C"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVC12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="true"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteC"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHC11" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="20px" DonTabIndex="17" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN3" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN4" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN4" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelActionFormationN4" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHD01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="415px" DonHeight="20px" DonTabIndex="18" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifsN4" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="19" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelCompetencesN4" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="20" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVD08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123D"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVD02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123D"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVD12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="true"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteD"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHD11" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="20px" DonTabIndex="21" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN4" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN5" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN5" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelActionFormationN5" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHE01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="415px" DonHeight="20px" DonTabIndex="22" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifsN5" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="23" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelCompetencesN5" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="24" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVE08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123E"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVE02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123E"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVE12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="true"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteE"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHE11" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="20px" DonTabIndex="25" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN5" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN6" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN6" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelActionFormationN6" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHF01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="415px" DonHeight="20px" DonTabIndex="26" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifsN6" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="27" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelCompetencesN6" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="28" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVF08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123F"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVF02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123F"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVF12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="true"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteF"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHF11" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="20px" DonTabIndex="29" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN6" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN7" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN7" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelActionFormationN7" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHG01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="415px" DonHeight="20px" DonTabIndex="30" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifsN7" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="31" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelCompetencesN7" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="32" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVG08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123G"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVG02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123G"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVG12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="true"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteG"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHG11" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="20px" DonTabIndex="33" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN7" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN8" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN8" runat="server" CellPadding="0" CellSpacing="0" Width="425px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelActionFormationN8" runat="server" Height="22px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Besoin de formation ou intitulé de l'action"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHH01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="415px" DonHeight="20px" DonTabIndex="34" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifsN8" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs assignés et/ou du projet professionnel de l'agent"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="35" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelCompetencesN8" runat="server" Height="20px" Width="425px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Compétences à développer (indiquer éventuellement des priorités)"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="414px" DonHeight="55px" DonTabIndex="36" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVH08" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                            RadioGaucheText="P1" RadioCentreText="P2" RadioDroiteText="P3"
                            V_Groupe="GroupeP123H"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVH02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="true"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123H"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVH12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="true"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteH"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHH11" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="20px" DonTabIndex="37" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN8" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
       <asp:TableCell>
           <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="25px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero31" runat="server" Height="45px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="b) Autres actions sollicitées, dont celles mobilisant le DIF"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeDIF31" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(**)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeDIF32" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Sous réserve de remplir les conditions d'accès au DIF (vérification par le service formation)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT71" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT72" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="TS : dans le temps de service"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT81" runat="server" Height="17px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT82" runat="server" Height="17px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="HTS : hors du temps de service"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>            
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreEnteteDemandes2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteExpressionDemande2" runat="server" Height="50px" Width="482px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Préparation examens et concours, VAE, bilan de compétences, période de professionnalisation"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: -1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteTypologie2" runat="server" Height="50px" Width="62px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text=""
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 2px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteModalite2" runat="server" Height="50px" Width="140px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Modalité de formation (**)"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEnteteDureeFormation2" runat="server" Height="50px" Width="60px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text="Nombre d'heures"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN9" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN9" runat="server" CellPadding="0" CellSpacing="0" Width="480px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="2px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelActionFormationN9" runat="server" Height="22px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Besoin ou action"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHI01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="470px" DonHeight="20px" DonTabIndex="38" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifsN9" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs poursuivis"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="39" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelCompetencesN9" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Compétences à développer"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="40" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTypologieDemandeN9" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVI12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="true"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteI"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHI11" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="20px" DonTabIndex="41" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN9" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN10" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN10" runat="server" CellPadding="0" CellSpacing="0" Width="480px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelActionFormationN10" runat="server" Height="22px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Besoin ou action"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHJ01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="470px" DonHeight="20px" DonTabIndex="42" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifsN10" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs poursuivis"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="43" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelCompetencesN10" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Compétences à développer"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="44" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTypologieDemandeN10" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVJ12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="true"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteJ"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHJ11" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="20px" DonTabIndex="45" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN10" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN11" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN11" runat="server" CellPadding="0" CellSpacing="0" Width="480px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelActionFormationN11" runat="server" Height="22px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Besoin ou action"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHK01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="470px" DonHeight="20px" DonTabIndex="46" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifsN11" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs poursuivis"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="47" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelCompetencesN11" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Compétences à développer"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="48" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTypologieDemandeN11" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVK12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="true"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteK"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHK11" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="20px" DonTabIndex="49" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN11" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreDemandeN12" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDemandeN12" runat="server" CellPadding="0" CellSpacing="0" Width="480px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelActionFormationN12" runat="server" Height="22px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Besoin ou action"
                                        ForeColor="#083A3A" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoHL01" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="470px" DonHeight="20px" DonTabIndex="50" 
                                        Etivisible="false" Donstyle="margin-left: 0px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelObjectifsN12" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Objectifs poursuivis"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="51" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="LabelCompetencesN12" runat="server" Height="20px" Width="480px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                                        Text="Compétences à développer"
                                        ForeColor="#0E5F5C" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                                        DonWidth="469px" DonHeight="55px" DonTabIndex="52" 
                                        Etivisible="false" Donstyle="margin-left: 2px;"
                                        DonBorderWidth="1px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTypologieDemandeN12" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="RadioVL12" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="12" V_SiDonneeDico="true"
                            RadioGaucheText="Sous DIF - TS" RadioCentreText="Sous DIF - HTS" RadioDroiteText="Hors DIF"
                            V_Groupe="GroupeModaliteL"
                            RadioGaucheWidth="124px" RadioCentreWidth="124px" RadioDroiteWidth="124px"
                            RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                            RadioGaucheStyle="text-align: left; Font-size: 90%" RadioCentreStyle="text-align: left; Font-size: 90%" RadioDroiteStyle="text-align: left; Font-size: 90%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHL11" runat="server"
                           V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                           DonWidth="58px" DonHeight="20px" DonTabIndex="53" 
                           Etivisible="false" Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Left">
           <asp:Label ID="LabelSeparationDemandeN12" runat="server" Height="14px" Width="748px"
              BackColor="#A8BBB8" BorderStyle="Notset" BorderColor="#A8BBB8" BorderWidth="1px" Text="">
           </asp:Label>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="25px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero4" runat="server" Height="45px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="OBSERVATIONS - COMPLEMENTS"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>          
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelObservationsAgent" runat="server" Height="45px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="L'agent :"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="220px" EtiHeight="20px" DonTabIndex="54" 
                           EtiText="" Etivisible="false"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
          	    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH20" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="20" V_SiDonneeDico="true"
                           EtiWidth="460px" DonWidth="80px" DonTabIndex="55"
                           Etistyle="margin-left: 0px;" 
	                       EtiText="Entretien de formation - Date de signature de l'agent"/>
                    </asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelObservationsEvaluateur" runat="server" Height="45px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Le responsable hiérarchique :"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="220px" EtiHeight="20px" DonTabIndex="56" 
                           EtiText="" Etivisible="false"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
          	    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH21" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="21" V_SiDonneeDico="true"
                           Etistyle="margin-left: 0px;" 
                           EtiWidth="460px" DonWidth="80px" DonTabIndex="57" EtiText="Entretien de formation - Date de signature du responsable hiérarchique"/>
                    </asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

 </asp:Table>