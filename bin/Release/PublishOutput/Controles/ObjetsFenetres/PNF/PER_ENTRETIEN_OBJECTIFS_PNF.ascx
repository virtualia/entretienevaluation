﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_OBJECTIFS_PNF.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_OBJECTIFS_PNF" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreObjectifs" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="DEFINITION DES OBJECTIFS DE L'ANNEE A VENIR" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
      
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectif1" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°1"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifs1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteObjectif1" runat="server" Height="20px" Width="746px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="1"
                            Donstyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>                        
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>     
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateur1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteIndicateur1" runat="server" Height="20px" Width="746px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Indicateurs de réussite prévus"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="2"
                            Donstyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>                          
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectif2" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°2"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifs2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteObjectif2" runat="server" Height="20px" Width="746px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="4"
                            Donstyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>                        
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>     
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateur2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteIndicateur2" runat="server" Height="20px" Width="746px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Indicateurs de réussite prévus"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="5"
                            Donstyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>                          
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectif3" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°3"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifs3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteObjectif3" runat="server" Height="20px" Width="746px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="7"
                            Donstyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>                        
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>     
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateur3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteIndicateur3" runat="server" Height="20px" Width="746px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Indicateurs de réussite prévus"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="8"
                            Donstyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>                          
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectif4" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°4"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifs4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteObjectif4" runat="server" Height="20px" Width="746px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="9"
                            Donstyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>                        
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>     
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateur4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteIndicateur4" runat="server" Height="20px" Width="746px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Indicateurs de réussite prévus"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 5px; text-align:  left;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="10"
                            Donstyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>                          
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>