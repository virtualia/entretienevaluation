﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_COMPETENCE_PNF.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE_PNF" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreCompetence" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="APPRECIATION DES COMPETENCES DE L'AGENT" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformationsNiveaux" runat="server" Height="25px" Width="746px"
                           BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="none"
                           Text="Niveaux"
                           BorderWidth="1px" ForeColor="#124545"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformationsNiveau1" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="none"
                           Text="Niveau 1 : à développer"
                           BorderWidth="1px" ForeColor="#124545"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformationsNiveau2" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="none"
                           Text="Niveau 2 : acquis"
                           BorderWidth="1px" ForeColor="#124545"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformationsNiveau3" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="none"
                           Text="Niveau 3 : maîtrise"
                           BorderWidth="1px" ForeColor="#124545"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformationsNiveau4" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="#124545" BorderStyle="none"
                           Text="Niveau 4 : expertise"
                           BorderWidth="1px" ForeColor="#0F2F30"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformationsNiveau5" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="none"
                           Text="Niveau 5 : sans objet"
                           BorderWidth="1px" ForeColor="#124545"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille1" runat="server" Height="30px" Width="750px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Compétences professionnelles"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences311" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence1" runat="server" Height="44px" Width="361px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels1" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote11" runat="server" Height="44px" Width="76px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Niveau 1"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote12" runat="server" Height="44px" Width="76px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Niveau 2"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote13" runat="server" Height="44px" Width="76px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Niveau 3"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote14" runat="server" Height="44px" Width="76px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Niveau 4"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote15" runat="server" Height="44px" Width="76px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Niveau 5"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHA01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                     Etivisible="False" DonWidth="350px" DonHeight="20px" 
                                     DonText="Connaissances du poste" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHA07" runat="server" V_Groupe="Critere111"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6visible="false"  
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHB01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="2"
                                     Etivisible="False" DonWidth="350px" DonHeight="20px" 
                                     DonText="Connaissances de l'environnement professionnel" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHB07" runat="server" V_Groupe="Critere112"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6visible="false"  
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHC01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="3"
                                     Etivisible="False" DonWidth="350px" DonHeight="20px" 
                                     DonText="Qualités rédactionnelles" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHC07" runat="server" V_Groupe="Critere113"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHD01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="4"
                                     Etivisible="False" DonWidth="350px" DonHeight="20px" 
                                     DonText="Qualités relationnelles" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHD07" runat="server" V_Groupe="Critere114"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHE01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="5"
                                     Etivisible="False" DonWidth="350px" DonHeight="20px" 
                                     DonText="Qualités d'expression orale" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHE07" runat="server" V_Groupe="Critere115"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHF01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="6"
                                     Etivisible="False" DonWidth="350px" DonHeight="20px" 
                                     DonText="Capacité d'adaptation aux évolutions techniques et professionnelles" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHF07" runat="server" V_Groupe="Critere116"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille2" runat="server" Height="30px" Width="750px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Agents en situation de management"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences321" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence2" runat="server" Height="44px" Width="361px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote21" runat="server" Height="44px" Width="76px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Niveau 1"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote22" runat="server" Height="44px" Width="76px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Niveau 2"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote23" runat="server" Height="44px" Width="76px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Niveau 3"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote24" runat="server" Height="44px" Width="76px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Niveau 4"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote25" runat="server" Height="44px" Width="76px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Niveau 5"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHG01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="7"
                                     Etivisible="False" DonWidth="350px" DonHeight="20px" 
                                     DonText="Capacité à déléguer" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHG07" runat="server" V_Groupe="Critere117"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6visible="false"  
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHH01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="8"
                                     Etivisible="False" DonWidth="350px" DonHeight="20px" 
                                     DonText="Capacité à assurer le suivi des dossiers" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHH07" runat="server" V_Groupe="Critere118"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6visible="false"  
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHI01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="9"
                                     Etivisible="False" DonWidth="350px" DonHeight="20px" 
                                     DonText="Aptitude à la prise de décision" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHI07" runat="server" V_Groupe="Critere119"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHJ01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="10"
                                     Etivisible="False" DonWidth="350px" DonHeight="20px" 
                                     DonText="Sens de l'organisation d'une équipe" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHJ07" runat="server" V_Groupe="Critere120"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                     VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 
 </asp:Table>