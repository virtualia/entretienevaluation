﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_ENTETE_CESE2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_ENTETE_CESE2" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

 <asp:Table ID="CadreListe" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0"
    HorizontalAlign="Center">
    <asp:TableRow> 
      <asp:TableCell> 
         <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="600px" SiColonneSelect="true"/>
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>
 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="CadreTitreEntretien" runat="server" Height="50px" CellPadding="0" Width="750px" 
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#124545">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                        cssclass="EP_TitreCR">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow> 
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelAnneeEntretien" runat="server" Text="Année 2011 - 2012" Height="20px" Width="200px"
                        CssClass="EP_TitreOnglet">
                     </asp:Label>          
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server"
                        V_PointdeVue="1" V_Objet="150" V_Information="0" V_SiDonneeDico="true"
                        EtiWidth="130px" DonWidth="80px" DonTabIndex="1" EtiText="Date de l'entretien"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="15px"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="500px" Text=""
                        CssClass="EP_TitreChapitre">
                     </asp:Label>          
                </asp:TableCell> 
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelEntreeCESE" runat="server" Height="24px" Width="500px" Text=""
                        CssClass="EP_EtiComplementBis">
                     </asp:Label>          
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelStatut" runat="server" Height="24px" Width="500px" Text=""
                        CssClass="EP_EtiComplementBis">
                     </asp:Label>          
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelEchelon" runat="server" Height="24px" Width="500px" Text=""
                        CssClass="EP_EtiComplementBis">
                     </asp:Label>          
                </asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelAffectation" runat="server" Height="42px" Width="500px" Text=""
                        CssClass="EP_EtiComplementBis">
                     </asp:Label>          
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelModaliteService" runat="server" Height="24px" Width="500px" Text=""
                        CssClass="EP_EtiComplementBis">
                     </asp:Label>          
                </asp:TableCell>
            </asp:TableRow>   
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelPoste" runat="server" Height="24px" Width="500px" Text=""
                        CssClass="EP_EtiComplementBis">
                     </asp:Label>          
                </asp:TableCell>
            </asp:TableRow>   
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow> 
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="CadreSignature" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                       V_PointdeVue="1" V_Objet="150" V_Information="2" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                       EtiWidth="180px" DonWidth="350px" DonTabIndex="2" Etitext="Service de l'évaluation"/> 
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                       V_PointdeVue="1" V_Objet="150" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="false"
                       EtiWidth="180px" DonWidth="350px" DonTabIndex="3" EtiText="Nom de l'évaluateur"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="422px" DonWidth="420px" DonTabIndex="4" Etitext="Fonction de l'évaluateur"
                           DonHeight="22px" />
                    </asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow>
                <asp:TableCell Height="20px"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                       V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true"
                       EtiWidth="220px" DonWidth="80px" DonTabIndex="5" EtiText="Date de signature de l'évaluateur"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="LabelSignatureEvalue" runat="server" Height="18px" Width="750px"
                       BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet"
                       Text="L'agent reconnaît avoir pris connaissance du présent compte-rendu d'entretien"
                       BorderWidth="1px" ForeColor="#2FA49B" Font-Italic="False"
                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 5px;
                       font-style:  oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                       V_PointdeVue="1" V_Objet="150" V_Information="4" V_SiDonneeDico="true"
                       EtiWidth="220px" DonWidth="80px" DonTabIndex="6" EtiText="Date de signature de l'agent"
                       DonTooltip="Attention. Vous ne pourrez plus modifier ultérieurement le formulaire d'entretien"/>
                </asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="LabelMessage" runat="server" Height="18px" Width="750px"
                       BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet"
                       Text="La date de signature de l'agent rend non modifiable le formulaire d'entretien"
                       BorderWidth="1px" ForeColor="#731E1E" Font-Italic="False"
                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 5px;
                       font-style:  oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>
            </asp:TableRow> 
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>