﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_EVOLUTION_CESE2_2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_EVOLUTION_CESE2_2" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreEvolution" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="EVOLUTION PROFESSIONNELLE DE L'AGENT" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="9px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero21" runat="server" Height="34px" Width="748px"
                           Text="1. Proposition d'attribution d'une augmentation de la part variable de la prime de rendement"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 6px; text-indent: 20px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero22" runat="server" Height="18px" Width="748px"
                           Text="Pour rappel :"
                           CssClass="EP_EtiComplement"
                           Font-Bold="True" style="text-indent: 5px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero23" runat="server" Height="40px" Width="748px"
                           Text="«le montant individualisé de la part variable lié à l'engagement professionnel de l'agent et à l'atteinte 
			               des objectifs annuels est déterminée au regard notamment du compte-rendu de l'entretien d'évaluation.
			               Par rapport à l'année N-1, la part variable ne peut diminuer ou augmenter de plus de 10 % sauf en cas de changement de corps et / ou de fonction.»"
                           CssClass="EP_EtiComplement"
                           Font-Size="90%">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero24" runat="server" Height="20px" Width="748px"
                           Text="(Référentiel de gestion des ressources humaines, arrêté n°14-06 du 5 février 2014)"
                           CssClass="EP_EtiComplement"
                           Font-Size="90%" style="text-indent: 5px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHA01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="4"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Proposition d'une augmentation de la part variable" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="CocheA04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"/>     
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero26" runat="server" Height="40px" Width="370px"
                           Text="Dernières augmentations de la part variable obtenues"
                           CssClass="EP_TitreChapitre"
                           Font-Bold="true" Font-Size="Small"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="361px" DonHeight="40px" EtiHeight="20px" DonTabIndex="4" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="LabelTitreNumero31" runat="server" Height="34px" Width="748px"
                           Text="2. Un changement d’affectation est-il :"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 6px; text-indent: 20px">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHB01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="5"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Sans objet pour le moment" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="CocheB04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHM01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="6"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Possible dans l'année ou à court terme" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="CocheM04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHC01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="7"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Souhaitable à moyen terme" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="CocheC04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHD01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="8"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="A envisager" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="CocheD04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHE01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="9"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="A été réalisé dans l'année" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="CocheE04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>              
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="LabelTitreNumero41" runat="server" Height="34px" Width="748px"
                           Text="3. Une promotion de grade au choix, sous réserve des conditions statutaires, est-elle :"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 6px; text-indent: 20px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHF01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="10"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Pas encore à l'ordre du jour" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="CocheF04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHG01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="11"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Envisageable à moyen terme" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="CocheG04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHH01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="12"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Recommandée fortement par l'évaluateur" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="CocheH04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="LabelDatePromoGrade" runat="server" Height="20px" Width="748px"
                           Text="DERNIERE PROMOTION DE GRADE le :"
                           CssClass="EP_EtiComplement"
                           Font-Italic="False" style="padding-top: 5px; font-style: normal; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="LabelModeAccesGrade" runat="server" Height="20px" Width="748px"
                           Text="Mode d'accès :"
                           CssClass="EP_EtiComplement"
                           Font-Italic="False" style="padding-top: 5px; font-style: normal; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>        
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="LabelTitreNumero51" runat="server" Height="34px" Width="748px"
                           Text="4. Une promotion de corps au choix, sous réserve des conditions statutaires, est-elle :"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 6px; text-indent: 20px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHI01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="13"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Pas encore à l'ordre du jour" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="CocheI04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHJ01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="14"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Envisageable à moyen terme" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="CocheJ04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHK01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="15"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Recommandée fortement par l'évaluateur" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="CocheK04" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true" 
                           V_Height="25px" V_Width="35px" V_Text=" " V_SiModeCaractere="false"/>     
                    </asp:TableCell>
                    <asp:TableCell Width="330px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="LabelDatePromoCorps" runat="server" Height="20px" Width="748px"
                           Text="DERNIERE PROMOTION DE CORPS le :"
                           CssClass="EP_EtiComplement"
                           Font-Italic="False" style="padding-top: 5px; font-style: normal; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="LabelModeAccesCorps" runat="server" Height="20px" Width="748px"
                           Text="Mode d'accès :"
                           CssClass="EP_EtiComplement"
                           Font-Italic="False" style="padding-top: 5px; font-style: normal; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>        
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero6" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitrePerspectiveSalariale" runat="server" Height="34px" Width="748px"
                           Text="PERSPECTIVE SALARIALE"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 6px; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHZ03" runat="server"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="350px" EtiHeight="20px" DonWidth="90px" DonHeight="20px" DonTabIndex="16"
                           Etitext="Date de la dernière revalorisation salariale"
                           EtiVisible="true" Donstyle="margin-left: 2px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHD03" runat="server"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="350px" EtiHeight="20px" DonWidth="250px" DonHeight="20px" DonTabIndex="17"
                           Etitext="Montant de la dernière revalorisation salariale"
                           EtiVisible="true" Donstyle="margin-left: 2px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitrePerspective" runat="server" Height="35px" Width="748px"
                           Text="Perspective pour l'entretien tri-annuel de revalorisation (année) en cohérence avec la durée du contrat de l'agent "
                           CssClass="EP_TitreChapitre"
                           Font-Bold="true" Font-Size="Small"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="200px" EtiHeight="20px" DonTabIndex="17" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>