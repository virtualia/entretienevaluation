﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_APPRECIATION_CESE2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_APPRECIATION_CESE2" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreAppreciation" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="APPRECIATION GENERALE DE L'EVALUATEUR" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreAppreciationEvaluateur" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreEvaluateur" runat="server" Height="30px" Width="748px"
                           Text="Préciser les qualités et aptitudes personnelles de l'agent"
                           CssClass="EP_EtiComplement">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="True"
                           EtiWidth="740px" DonWidth="740px" DonHeight="250px" EtiHeight="60px" DonTabIndex="1" 
                           Etitext="(Sens des responsabilités, fiabilité, conscience professionnelle, aptitudes managériales, 
                           aptitudes organisationnelles, esprit d'initiative, capacité d'adaptation à l'évolution du poste ou 
                           de son environnement, aptitude à l'acquisition de nouvelles connaissances, présentation générale, 
                           ponctualité et assiduité…)" 
                           Etivisible="True"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreConditionsTravail" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelConditionsTravail" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletConditionsTravail" runat="server" Text="CONDITIONS DE TRAVAIL" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteAgentBis" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreConditionsTravail" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2Bis" runat="server" Height="30px" Width="748px"
                           Text="Conditions matérielles (à remplir par l’agent)"
                           CssClass="EP_TitreChapitre"
                           Font-Size="Small" Font-Bold="true"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVV03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="2" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero3Bis" runat="server" Height="30px" Width="748px"
                           Text="Environnement de travail (à remplir par l’agent)"
                           CssClass="EP_TitreChapitre"
                           Font-Size="Small" Font-Bold="true"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVW03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="3" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero4Bis" runat="server" Height="30px" Width="748px"
                           Text="Observations du supérieur hiérarchique"
                           CssClass="EP_TitreChapitre"
                           Font-Size="Small" Font-Bold="true"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVX03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="4" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero5Bis" runat="server" Height="40px" Width="748px"
                           Text="Retour d’expérience - Observations formulées en N-1 (à remplir par l’agent)"
                           CssClass="EP_TitreChapitre"
                           Font-Size="Small" Font-Bold="true"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVY03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="5" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreExpressionAgent" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEtiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletExpression" runat="server" Text="EXPRESSION DE L'AGENT" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteAgent" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreExpressionAgent" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero1" runat="server" Height="45px" Width="748px"
                           Text="La signature du document atteste que l'agent en a pris connaissance, pas qu'il en approuve le sens. 
                           Dans un premier temps, si vous ne partagez pas l'appréciation portée par votre évaluateur, 
                           vous pouvez l'indiquer sur cette page."
                           CssClass="EP_EtiComplement">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                           Text="Souhaitez-vous apporter des précisions à la description qui a été faite de vos fonctions ? Si oui lesquelles ?"
                           CssClass="EP_TitreChapitre"
                           Font-Size="Small" Font-Bold="true"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="6" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero3" runat="server" Height="30px" Width="748px"
                           Text="Partagez-vous l'évaluation générale vous concernant ? Sinon pourquoi ?"
                           CssClass="EP_TitreChapitre"
                           Font-Size="Small" Font-Bold="true"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVR03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="7" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero4" runat="server" Height="30px" Width="748px"
                           Text="Vous sentez-vous à l'aise dans votre service ? Si non pourquoi ?"
                           CssClass="EP_TitreChapitre"
                           Font-Size="Small" Font-Bold="true"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVS03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="8" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero5" runat="server" Height="40px" Width="748px"
                           Text="Quels sont vos souhaits d'évolution professionnelle ? Evolution sur le poste, 
                           prises de responsabilités nouvelles, projet professionnel, avancement."
                           CssClass="EP_TitreChapitre"
                           Font-Size="Small" Font-Bold="true"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVT03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="9" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero6" runat="server" Height="30px" Width="748px"
                           Text="Autres observations"
                           CssClass="EP_TitreChapitre"
                           Font-Size="Small" Font-Bold="true"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVU03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="10" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>