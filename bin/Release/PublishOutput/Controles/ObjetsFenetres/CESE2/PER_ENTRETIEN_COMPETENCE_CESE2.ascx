﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_COMPETENCE_CESE2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE_CESE2" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreCompetence" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="EVALUATION" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations" runat="server" Height="25px" Width="746px"
                            Text="Pour chacun des thèmes, cocher le niveau estimé."
                            CssClass="EP_EtiComplement"
                            style="text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille1" runat="server" Height="25px" Width="748px"
                           Text="CONNAISSANCE DU DOMAINE D'EXERCICE DE LA FONCTION"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 7px; text-indent: 1px; text-align: center; margin-bottom: -1px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille1" runat="server" Height="20px" Width="748px"
                           Text="A compléter en fonction des compétences attendues par la fiche de poste actualisée"
                           CssClass="EP_TitreParagraphe"
                           Font-Italic="true" Font-Size="Small"
                           style="font-style: oblique; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence1" runat="server" Height="32px" Width="246px" Text="Compétence"
                                       CssClass="EP_EtiComplement"
                                       style="margin-top: 1px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels1" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote11" runat="server" Height="27px" Width="55px" Text="Expertise"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote12" runat="server" Height="27px" Width="70px" Text="Maîtrise"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote13" runat="server" Height="29px" Width="70px" Text="A <br/> développer"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 3px">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote14" runat="server" Height="29px" Width="55px" Text="A <br/> acquérir"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 3px">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreObserv1" runat="server" Height="32px" Width="236px" Text="Observations éventuelles"
                                       CssClass="EP_EtiComplement"
                                       style="margin-top: 1px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>       
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="1" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"
                                   DonTooltip="champs à compléter obligatoirement"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHA07" runat="server" V_Groupe="CritereAgent1"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="2" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="3" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"
                                   DonTooltip="champs à compléter obligatoirement"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHB07" runat="server" V_Groupe="CritereAgent2"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="4" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="5" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"
                                   DonTooltip="champs à compléter obligatoirement"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHC07" runat="server" V_Groupe="CritereAgent3"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="51px" VRadioN2Width="65px" VRadioN3Width="66px"
                                   VRadioN4Width="51px" VRadioN5Width="67px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false" VRadioN5Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="6" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>  
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                  
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille2" runat="server" Height="45px" Width="748px"
                           Text="QUALITE DU TRAVAIL ET QUALITES RELATIONNELLES"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 7px; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence2" runat="server" Height="32px" Width="246px" Text="Qualité"
                                       CssClass="EP_EtiComplement"
                                       style="margin-top: 1px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote21" runat="server" Height="27px" Width="51px" Text="Excellent" Tooltip="Excellent"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="80%"
                                               style="margin-top: 1px; padding-top: 5px">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote22" runat="server" Height="27px" Width="59px" Text="Satisfaisant" Tooltip="Satisfaisant"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="80%"
                                               style="margin-top: 1px; padding-top: 5px">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote23" runat="server" Height="29px" Width="57px" Text="A <br/> développer" Tooltip="A développer"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="80%"
                                               style="margin-top: 1px; padding-top: 3px">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote24" runat="server" Height="29px" Width="45px" Text="A <br/> acquérir" Tooltip="A acquérir"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="80%"
                                               style="margin-top: 1px; padding-top: 3px">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote25" runat="server" Height="29px" Width="38px" Text="Sans <br/> objet" Tooltip="Sans objet"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="80%"
                                               style="margin-top: 1px; padding-top: 3px">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreObserv2" runat="server" Height="32px" Width="236px" Text="Observations éventuelles"
                                       CssClass="EP_EtiComplement"
                                       style="margin-top: 1px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>       
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="7" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHH07" runat="server" V_Groupe="CritereAgent4"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="8" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="9" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHI07" runat="server" V_Groupe="CritereAgent5"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="10" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="11" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHJ07" runat="server" V_Groupe="CritereAgent6"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="12" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="13" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHK07" runat="server" V_Groupe="CritereAgent7"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="14" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="15" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHL07" runat="server" V_Groupe="CritereAgent8"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="16" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="17" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHM07" runat="server" V_Groupe="CritereAgent9"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="18" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="19" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHN07" runat="server" V_Groupe="CritereAgent10"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="20" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="21" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHO07" runat="server" V_Groupe="CritereAgent11"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="22" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="23" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHP07" runat="server" V_Groupe="CritereAgent12"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="24" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="25" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHQ07" runat="server" V_Groupe="CritereAgent13"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="26" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille3" runat="server" Height="25px" Width="748px"
                           Text="APTITUDES AU MANAGEMENT"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 7px; text-indent: 1px; text-align: center; margin-bottom: -1px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille3" runat="server" Height="20px" Width="748px"
                           Text="(uniquement pour les agents en situation d'encadrement)"
                           CssClass="EP_TitreParagraphe"
                           Font-Italic="true" Font-Size="Small"
                           style="margin-bottom: 1px; font-style: oblique; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence3" runat="server" Height="32px" Width="246px" Text="Aptitude"
                                       CssClass="EP_EtiComplement"
                                       style="margin-top: 1px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels3" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote31" runat="server" Height="27px" Width="51px" Text="Excellent" Tooltip="Excellent"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="80%"
                                               style="margin-top: 1px; padding-top: 5px">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote32" runat="server" Height="27px" Width="59px" Text="Satisfaisant" Tooltip="Satisfaisant"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="80%"
                                               style="margin-top: 1px; padding-top: 5px">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote33" runat="server" Height="29px" Width="57px" Text="A <br/> développer" Tooltip="A développer"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="80%"
                                               style="margin-top: 1px; padding-top: 3px">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote34" runat="server" Height="29px" Width="45px" Text="A <br/> acquérir" Tooltip="A acquérir"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="80%"
                                               style="margin-top: 1px; padding-top: 3px">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote35" runat="server" Height="29px" Width="38px" Text="Sans <br/> objet" Tooltip="Sans objet"
                                               CssClass="EP_EtiNiveau"
                                               Font-Size="80%"
                                               style="margin-top: 1px; padding-top: 3px">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreObserv3" runat="server" Height="32px" Width="236px" Text="Observations éventuelles"
                                       CssClass="EP_EtiComplement"
                                       style="margin-top: 1px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>       
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVU01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="27" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHU07" runat="server" V_Groupe="CritereAgent14"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVU11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="28" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVV01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="29" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHV07" runat="server" V_Groupe="CritereAgent15"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVV11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="30" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVW01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="31" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHW07" runat="server" V_Groupe="CritereAgent16"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVW11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="32" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVX01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="243px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="33" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHX07" runat="server" V_Groupe="CritereAgent17"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="47px" VRadioN2Width="54px" VRadioN3Width="52px"
                                   VRadioN4Width="40px" VRadioN5Width="33px" VRadioN6Width="70px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVX11" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                   DonWidth="232px" DonHeight="35px" DonBorderWidth="1px" DonTabIndex="34" 
                                   EtiVisible="false" V_SiEnLectureSeule="false"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>                     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>