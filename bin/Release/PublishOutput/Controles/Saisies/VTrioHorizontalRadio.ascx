﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaControle_VTrioHorizontalRadio" Codebehind="VTrioHorizontalRadio.ascx.vb" %>

    <asp:table ID="CadreOption" runat="server" Height="20px" CellPadding="1" CellSpacing="0">
         <asp:TableRow>
             <asp:TableCell>
                 <asp:RadioButton ID="RadioN0" runat="server" Text="Option N1" Visible="true"
                      AutoPostBack="false" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="100px"
                      BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                      Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                      style="margin-top: 0px; margin-left: 10px; font-style: oblique;
                      vertical-align: middle; text-indent: 5px; text-align: left" />
                 <asp:RadioButton ID="RadioN1" runat="server" Text="Option N2" 
                      AutoPostBack="false" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="100px"
                      BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                      Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                      style="margin-top: 0px; margin-left: 1px; font-style: oblique;
                      vertical-align: middle; text-indent: 5px; text-align: left" />
                 <asp:RadioButton ID="RadioN2" runat="server" Text="Option N3" 
                      AutoPostBack="false" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="100px"
                      BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                      Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                      style="margin-top: 0px; margin-left: 1px; font-style: oblique;
                      vertical-align: middle; text-indent: 5px; text-align: left" />
             </asp:TableCell> 
         </asp:TableRow>
     </asp:table>
   