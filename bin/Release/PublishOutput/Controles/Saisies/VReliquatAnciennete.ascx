﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaControle_VReliquatAnciennete" Codebehind="VReliquatAnciennete.ascx.vb" %>

<asp:Table ID="VAnciennete" runat="server" CellPadding="1" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Etiquette" runat="server" Height="20px" Width="170px"
                    BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left" >
            </asp:Label>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="Annees" runat="server" Height="16px" Width="30px" MaxLength="2"
                    BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset"
                    BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                    style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"
                    ToolTip="Nombre d'années de reliquat"> 
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="Mois" runat="server" Height="16px" Width="30px" MaxLength="2"
                    BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset"
                    BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                    style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"
                    ToolTip="Nombre de mois de reliquat">  
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="Jours" runat="server" Height="16px" Width="30px" MaxLength="2"
                    BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset"
                    BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                    style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"
                    ToolTip="Nombre de jours de reliquat">  
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="DateAnciennete" runat="server" Height="16px" Width="120px" MaxLength="10"
                    BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset"
                    BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                    style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
            </asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
  </asp:Table>