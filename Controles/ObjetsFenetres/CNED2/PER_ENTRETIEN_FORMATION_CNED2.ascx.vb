﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_FORMATION_CNED2
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
            End If
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1), "CNED2")
            VirControle.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirControle.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1), "CNED2")
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VSixBoutonRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VSixBoutonRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 7, 1), "CNED2")
            VirRadio.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.V_SiAutoPostBack = Not (WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1)))
            Select Case NumObjet
                Case 150
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "Non"
                            VirRadio.VRadioN1Check = True
                        Case Is = "Oui"
                            VirRadio.VRadioN2Check = True
                        Case Else
                            VirRadio.VRadioN3Check = False
                            VirRadio.VRadioN4Check = False
                            VirRadio.VRadioN5Check = False
                            VirRadio.VRadioN6Check = False
                    End Select
                Case 155
                    If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                        Select Case ValeurLue(NumObjet, NumInfo, Rang)
                            Case Is = "1er trim."
                                VirRadio.VRadioN1Check = True
                            Case Is = "2ème trim."
                                VirRadio.VRadioN2Check = True
                            Case Is = "3ème trim."
                                VirRadio.VRadioN3Check = True
                            Case Is = "4ème trim."
                                VirRadio.VRadioN4Check = True
                            Case Else
                                VirRadio.VRadioN1Check = False
                                VirRadio.VRadioN2Check = False
                                VirRadio.VRadioN3Check = False
                                VirRadio.VRadioN4Check = False
                        End Select
                    End If
            End Select
            IndiceI += 1
        Loop
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH20.ValeurChange, _
        InfoH21.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 6, 1), "CNED2")

        Select Case NumObjet
            Case 150
                If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
                    CType(sender, VirtualiaControle_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
                End If
        End Select
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoVA01.ValeurChange, _
    InfoVB01.ValeurChange, InfoVC01.ValeurChange, InfoVD01.ValeurChange, InfoVF01.ValeurChange, _
    InfoVG01.ValeurChange, InfoVH01.ValeurChange, InfoVI01.ValeurChange, _
    InfoVK01.ValeurChange, InfoVL01.ValeurChange, _
    InfoVM01.ValeurChange, InfoVN01.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1), "CNED2")
        Select Case NumObjet
            Case 154
                If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_154(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
            Case 155
                If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_155(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub RadioValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) _
        Handles RadioH29.ValeurChange, RadioH19.ValeurChange, _
        RadioHA08.ValeurChange, RadioHB08.ValeurChange, RadioHC08.ValeurChange, RadioHD08.ValeurChange, _
        RadioHF08.ValeurChange, RadioHG08.ValeurChange, RadioHH08.ValeurChange, RadioHI08.ValeurChange, _
        RadioHK08.ValeurChange, RadioHL08.ValeurChange, RadioHM08.ValeurChange, RadioHN08.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VSixBoutonRadio).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VSixBoutonRadio).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VSixBoutonRadio).ID, 7, 1), "CNED2")

        Select Case NumObjet
            Case 150
                If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
                    WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
                End If
            Case 155
                If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_155(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ListeFormationsSuivies.Centrage_Colonne(0) = 1
        ListeFormationsSuivies.Centrage_Colonne(1) = 0
        ListeFormationsSuivies.Centrage_Colonne(2) = 1
        ListeFormationsSuivies.Centrage_Colonne(3) = 0
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        Etiquette.Text = "COMPTE-RENDU D'ENTRETIEN DE FORMATION - Année " & WsDossierPER.Annee
        LabelIdentite.Text = WsDossierPER.LibelleIdentite
        LabelObservationsAgent.Text = "L'agent : " & WsDossierPER.LibelleIdentite

        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
            LabelObservationsEvaluateur.Text = "Le supérieur hiérarchique : " & WsDossierPER.Objet_150.Evaluateur
        End If

        'Liste des formations 
        V_LibelListe = "Aucune formation" & VI.Tild & "Une formation" & VI.Tild & "formations"
        V_LibelColonne = "date" & VI.Tild & "formation suivie" & VI.Tild & "Durée" & VI.Tild & "Clef"
        ListeFormationsSuivies.V_Liste(V_LibelColonne, V_LibelListe) = WsDossierPER.ListeDesFormationsSuivies(WsDossierPER.Annee, 3)

        LabelSoldeDIF.Text = WsDossierPER.SoldeDuDif(DatedEffet)

        Call LireLaFiche()
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 155
                    If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_155(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class