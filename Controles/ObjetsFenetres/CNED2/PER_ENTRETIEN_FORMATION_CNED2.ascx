﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_FORMATION_CNED2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_FORMATION_CNED2"%>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia"%>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia"%>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia"%>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia"%>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" Visible="true"
    Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreFormation" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN DE FORMATION" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="BILAN DES FORMATIONS ET BESOINS DE FORMATION" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Text="" Height="30px" Width="746px"
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Text="1. Bilan des formations suivies sur la période écoulée" Height="38px" Width="746px"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 4px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell Width="746px" HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="ListeFormationsSuivies" runat="server" CadreWidth="740px" SiColonneSelect="false"/>
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Text="2. Compétence à acquérir ou développer pour tenir le poste" Height="38px" Width="746px"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 4px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin102" runat="server" CellPadding="0" CellSpacing="0" Width="746px" BorderStyle="None">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage102" runat="server" Text="Intitulé" Height="25px" Width="390px"
                               CssClass="EP_EtiComplement"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode102" runat="server" Text="Echéances envisagées" Height="25px" Width="350px"
                               CssClass="EP_EtiComplement"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="2" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHA08" runat="server" V_Groupe="EcheanceA"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHB08" runat="server" V_Groupe="EcheanceB"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                     
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="6" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHC08" runat="server" V_Groupe="EcheanceC"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                          
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="8" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHD08" runat="server" V_Groupe="EcheanceD"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                              
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Left">
                            <asp:Label ID="LabelSiFormationUrgente" runat="server" Height="35px" Width="385px"
                                CssClass="EP_EtiComplement"
                                Text="Une action de formation permettant d'acquérir ou de développer ces compétences doit-elle être suivie rapidement ?"
                                style="padding-top: 5px; font-style: normal; text-indent: 0px;">
                            </asp:Label>          
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Left">        
                            <Virtualia:VSixBoutonRadio ID="RadioH29" runat="server" V_Groupe="SiFormationUrgente"
                                V_PointdeVue="1" V_Objet="150" V_Information="29" V_SiDonneeDico="true"
                                VRadioN1Width="80px" VRadioN2Width="80px" VRadioN3Width="0px"
                                VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px" VRadioN2Style="margin-left: 0px; margin-top: 0px" VRadioN3Style="margin-left: 0px; margin-top: 0px"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px" VRadioN5Style="margin-left: 0px; margin-top: 0px" VRadioN6Style="margin-left: 0px; margin-top: 0px" 
                                VRadioN1Text="Non" VRadioN2Text="Oui" 
                                VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell> 
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin103" runat="server" CellPadding="0" CellSpacing="0" Width="746px" BorderStyle="None">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="LabelTypeBesoin103" runat="server" Text="3. Compétences à acquérir ou développer en vue d'une évolution professionnelle" Height="38px" Width="746px"
                               CssClass="EP_TitreParagraphe"
                               style="padding-top: 4px;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                          <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="LabelTypeBesoin103Bis" runat="server" Height="20px" Width="746px"
                               CssClass="EP_EtiComplement"
                               Text="(à compléter en fonction des perspectives d'évolution professionnelle)">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage103" runat="server" Text="Intitulé" Height="25px" Width="390px"
                               CssClass="EP_EtiComplement"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode103" runat="server" Text="Echéances envisagées" Height="25px" Width="350px"
                               CssClass="EP_EtiComplement"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="12" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHF08" runat="server" V_Groupe="EcheanceF"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                              
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="14" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHG08" runat="server" V_Groupe="EcheanceG"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="16" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHH08" runat="server" V_Groupe="EcheanceH"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                                  
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="18" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHI08" runat="server" V_Groupe="EcheanceI"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin104" runat="server" CellPadding="0" CellSpacing="0" Width="746px" BorderStyle="None">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="LabelTypeBesoin104" runat="server" Text="4. Autres perspectives de formation" Height="38px" Width="746px"
                               CssClass="EP_TitreParagraphe"
                               style="padding-top: 4px;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage104" runat="server" Text="Intitulé" Height="25px" Width="390px"
                               CssClass="EP_EtiComplement"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode104" runat="server" Text="Echéances envisagées" Height="25px" Width="350px"
                               CssClass="EP_EtiComplement"
                               style="font-style: normal; text-indent: 0px; text-align: center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="22" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHK08" runat="server" V_Groupe="EcheanceK"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="24" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHL08" runat="server" V_Groupe="EcheanceL"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="26" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHM08" runat="server" V_Groupe="EcheanceM"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                             
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="28" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHN08" runat="server" V_Groupe="EcheanceN"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="TableDIF" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitre105" runat="server" Text="5. Utilisation du droit individuel à la formation (DIF)" Height="38px" Width="746px"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 4px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelTitreDIF" runat="server" Height="30px" Width="340px"
                            CssClass="EP_EtiComplement"
                            Text="Solde du DIF au 1er janvier de l'année en cours :"
                            style="font-style: normal; text-indent: 10px;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelSoldeDIF" runat="server" Text="" Height="30px" Width="400px"
                            CssClass="EP_EtiComplement"
                            style="font-style: normal;">
                        </asp:Label>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="LabelSiUtilisationDIF" runat="server" Height="25px" Width="340px"
                            CssClass="EP_EtiComplement"
                            Text="L'agent envisage-t-il de mobiliser son DIF cette année ?"
                            style="font-style: normal; text-indent: 10px;">
                         </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                       <Virtualia:VSixBoutonRadio ID="RadioH19" runat="server" V_Groupe="SiUtilisationDIF"
                           V_PointdeVue="1" V_Objet="150" V_Information="19" V_SiDonneeDico="true"
                           VRadioN1Width="80px" VRadioN2Width="80px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                           VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px" VRadioN2Style="margin-left: 0px; margin-top: 0px" VRadioN3Style="margin-left: 0px; margin-top: 0px"
                           VRadioN4Style="margin-left: 0px; margin-top: 0px" VRadioN5Style="margin-left: 0px; margin-top: 0px" VRadioN6Style="margin-left: 0px; margin-top: 0px" 
                           VRadioN1Text="Non" VRadioN2Text="Oui" 
                           VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell> 
                </asp:TableRow>         
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreSignatures" runat="server" Text="Compte rendu d'entretien de formation - Signatures" Height="35px" Width="746px"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 7px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelObservationsAgent" runat="server" Text="L'agent :" Height="20px" Width="748px"
                           CssClass="EP_EtiComplement"
                           style="padding-top: 4px; font-style: normal; text-indent: 10px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
          	    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH20" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="20" V_SiDonneeDico="true"
                           EtiWidth="300px" DonWidth="80px" DonTabIndex="55"
                           Etistyle="margin-left: 0px;" 
	                       EtiText="Date de signature de l'agent"/>
                    </asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelObservationsEvaluateur" runat="server" Text="Le supérieur hiérarchique :" Height="20px" Width="748px"
                           CssClass="EP_EtiComplement"
                           style="padding-top: 4px; font-style: normal; text-indent: 10px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
          	    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH21" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="21" V_SiDonneeDico="true"
                           Etistyle="margin-left: 0px;" 
                           EtiWidth="300px" DonWidth="80px" DonTabIndex="57" EtiText="Date de signature du supérieur hiérarchique"/>
                    </asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>