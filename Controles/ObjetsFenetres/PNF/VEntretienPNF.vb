﻿Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class VEntretienPNF
        Implements Virtualia.Net.Entretien.IGenericEntretien
        Private WsLstCompetences As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstObservations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstFormations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing

        Public ReadOnly Property LitteralCompetence(Critere As Integer, Optional SiPrecision As Boolean = False) As String Implements IGenericEntretien.LitteralCompetence
            Get
                Select Case Critere
                    Case 1
                        Return "Niveau 1"
                    Case 2
                        Return "Niveau 2"
                    Case 3
                        Return "Niveau 3"
                    Case 4
                        Return "Niveau 4"
                    Case 5
                        Return "Niveau 5"
                    Case Else
                        Return "non requis"
                End Select
            End Get
        End Property

        Public ReadOnly Property LitteralResultat(Critere As Integer) As String Implements IGenericEntretien.LitteralResultat
            Get
                Select Case Critere
                    Case 1
                        Return "insuffisant"
                    Case 2
                        Return "partiellement atteint"
                    Case 3
                        Return "atteint"
                    Case 4
                        Return "dépassé"
                    Case 5
                        Return "exceptionnel"
                    Case Else
                        Return "atteint"
                End Select
            End Get
        End Property

        Public ReadOnly Property LitteralSynthese(Critere As Integer) As String Implements IGenericEntretien.LitteralSynthese
            Get
                Return ""
            End Get
        End Property

        Public ReadOnly Property Table_Competences As List(Of ObjetTable) Implements IGenericEntretien.Table_Competences
            Get
                If WsLstCompetences IsNot Nothing Then
                    Return WsLstCompetences
                End If

                Dim IndiceFam As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstCompetences = New List(Of Virtualia.Net.Entretien.ObjetTable)

                For IndiceFam = 0 To 9
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    Select Case IndiceFam
                        Case 0 To 5
                            ItemTable.Clef_Index = IndiceFam + 1 'Numero de Tri
                            ItemTable.Rang_Famille = "3.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Compétences de l'agent"
                            ItemTable.Rang_SousFamille = "3.1"
                            ItemTable.Libelle_SousFamille = "Compétences professionnelles"
                            Select Case IndiceFam
                                Case 0
                                    ItemTable.Intitule = "Connaissances du poste"
                                    ItemTable.Clef_Origine = "A"
                                Case 1
                                    ItemTable.Intitule = "Connaissances de l'environnement professionnel"
                                    ItemTable.Clef_Origine = "B"
                                Case 2
                                    ItemTable.Intitule = "Qualités rédactionnelles"
                                    ItemTable.Clef_Origine = "C"
                                Case 3
                                    ItemTable.Intitule = "Qualités relationnelles"
                                    ItemTable.Clef_Origine = "D"
                                Case 4
                                    ItemTable.Intitule = "Qualités d'expression orale"
                                    ItemTable.Clef_Origine = "E"
                                Case 5
                                    ItemTable.Intitule = "Capacité d'adaptation aux évolutions techniques et professionnelles"
                                    ItemTable.Clef_Origine = "F"
                            End Select

                        Case 6 To 9
                            ItemTable.Clef_Index = IndiceFam + 5 'Numero de Tri
                            ItemTable.Rang_Famille = "3.2" 'Numero de famille
                            ItemTable.Libelle_Famille = "Compétences de management"
                            ItemTable.Rang_SousFamille = "3.2"
                            ItemTable.Libelle_SousFamille = "Compétences professionnelles"
                            Select Case IndiceFam
                                Case 6
                                    ItemTable.Intitule = "Capacité à déléguer"
                                    ItemTable.Clef_Origine = "G"
                                Case 7
                                    ItemTable.Intitule = "Capacité à assurer le suivi des dossiers"
                                    ItemTable.Clef_Origine = "H"
                                Case 8
                                    ItemTable.Intitule = "Aptitude à la prise de décision"
                                    ItemTable.Clef_Origine = "I"
                                Case 9
                                    ItemTable.Intitule = "Sens de l'organisation d'une équipe"
                                    ItemTable.Clef_Origine = "J"
                            End Select
                    End Select
                    WsLstCompetences.Add(ItemTable)
                Next IndiceFam
                Return WsLstCompetences
            End Get
        End Property

        Public ReadOnly Property Table_Formations As List(Of ObjetTable) Implements IGenericEntretien.Table_Formations
            Get
                If WsLstFormations IsNot Nothing Then
                    Return WsLstFormations
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstFormations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                For IndiceTri = 0 To 13
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Intitule = ""
                    ItemTable.Clef_Index = IndiceTri + 1
                    Select Case IndiceTri
                        Case 0
                            ItemTable.Clef_Origine = "A"
                        Case 1
                            ItemTable.Clef_Origine = "B"
                        Case 2
                            ItemTable.Clef_Origine = "C"
                        Case 3
                            ItemTable.Clef_Origine = "D"
                        Case 4
                            ItemTable.Clef_Origine = "E"
                        Case 5
                            ItemTable.Clef_Origine = "F"
                        Case 6
                            ItemTable.Clef_Origine = "G"
                        Case 7
                            ItemTable.Clef_Origine = "H"
                        Case 8
                            ItemTable.Clef_Origine = "I"
                        Case 9
                            ItemTable.Clef_Origine = "J"
                        Case 10
                            ItemTable.Clef_Origine = "K"
                        Case 11
                            ItemTable.Clef_Origine = "L"
                        Case 12
                            ItemTable.Clef_Origine = "M"
                        Case 13
                            ItemTable.Clef_Origine = "N"
                    End Select
                    WsLstFormations.Add(ItemTable)
                Next IndiceTri
                Return WsLstFormations
            End Get
        End Property

        Public ReadOnly Property Table_Observations As List(Of ObjetTable) Implements IGenericEntretien.Table_Observations
            Get
                If WsLstObservations IsNot Nothing Then
                    Return WsLstObservations
                End If

                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstObservations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l'agent sur l'entretien de formation"
                ItemTable.Rang_Famille = "7.5"
                ItemTable.Clef_Origine = "O"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l'évaluateur sur l'entretien de formation"
                ItemTable.Rang_Famille = "7.6"
                ItemTable.Clef_Origine = "P"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Evolution professionnelle envisagée"
                ItemTable.Rang_Famille = "5.1"
                ItemTable.Clef_Origine = "Q"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Mobilité fonctionnelle ou géographique envisagée"
                ItemTable.Rang_Famille = "5.2"
                ItemTable.Clef_Origine = "R"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Avis du supérieur hiérarchique sur le projet professionnel"
                ItemTable.Rang_Famille = "5.3"
                ItemTable.Clef_Origine = "S"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaires sur les actions de formation continue suivies"
                ItemTable.Rang_Famille = "7.1"
                ItemTable.Clef_Origine = "T"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaire sur les formations non suivies"
                ItemTable.Rang_Famille = "7.2"
                ItemTable.Clef_Origine = "U"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaires de l'agent sur le bilan"
                ItemTable.Rang_Famille = "6.1"
                ItemTable.Clef_Origine = "V"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaires de l'agent sur l'engagement année n+1"
                ItemTable.Rang_Famille = "6.2"
                ItemTable.Clef_Origine = "W"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaires de l'évaluateur sur l'engagement année n+1"
                ItemTable.Rang_Famille = "6.3"
                ItemTable.Clef_Origine = "X"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Actions conduites en tant que formateur"
                ItemTable.Rang_Famille = "7.3"
                ItemTable.Clef_Origine = "Y"
                WsLstObservations.Add(ItemTable)

                Return WsLstObservations
            End Get
        End Property

        Public ReadOnly Property Table_Syntheses As List(Of ObjetTable) Implements IGenericEntretien.Table_Syntheses
            Get
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Table_Complements As List(Of ObjetTable) Implements IGenericEntretien.Table_Complements
            Get
                Return Nothing
            End Get
        End Property
    End Class
End Namespace