﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_RESULTATS_PNF.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_RESULTATS_PNF" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixVerticalRadio.ascx" tagname="VSixVerticalRadio" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderColor="#B0E0D7" 
    BorderStyle="None" BorderWidth="2px" HorizontalAlign="Center" 
    style="margin-top: 1px;" Visible="true" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreResultats" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="BILAN DE L'ANNEE - Atteinte des objectifs et actions conduites" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>  
    
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="LabelNumerobjectif1" runat="server" Height="25px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°1"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif1" runat="server" Height="28px" Width="301px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteIndicateurs1" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteIndicateurs1" runat="server" Height="28px" Width="260px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Indicateurs <br/> de réussite prévus"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelEnteteRealisation1" runat="server" Height="28px" Width="179px"
                                        BackColor="#D7FAF3" BorderStyle="None" 
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="297px" DonHeight="140px" DonTabIndex="1"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableIndicateur1" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="256px" DonHeight="140px" DonTabIndex="2"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="RadioHC04" runat="server" V_Groupe="Realisation1"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="insuffisant" VRadioN2Text="partiellement atteint" VRadioN3Text="atteint"
                                            VRadioN4Text="dépassé" VRadioN5Text="exceptionnel" VRadioN6Text=""
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableEnteteCommentaire1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteCommentaire1" runat="server" Height="18px" Width="748px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Commentaires"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableCommentaire1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="3"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="LabelNumerobjectif2" runat="server" Height="25px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°2"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif2" runat="server" Height="28px" Width="301px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteIndicateurs2" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteIndicateurs2" runat="server" Height="28px" Width="260px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Indicateurs <br/> de réussite prévus"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelEnteteRealisation2" runat="server" Height="28px" Width="179px"
                                        BackColor="#D7FAF3" BorderStyle="None" 
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="297px" DonHeight="140px" DonTabIndex="4"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableIndicateur2" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="256px" DonHeight="140px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="RadioHD04" runat="server" V_Groupe="Realisation2"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="insuffisant" VRadioN2Text="partiellement atteint" VRadioN3Text="atteint"
                                            VRadioN4Text="dépassé" VRadioN5Text="exceptionnel" VRadioN6Text=""
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableEnteteCommentaire2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteCommentaire2" runat="server" Height="18px" Width="748px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Commentaires"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableCommentaire2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="6"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="LabelNumerobjectif3" runat="server" Height="25px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°3"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif3" runat="server" Height="28px" Width="301px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteIndicateurs3" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteIndicateurs3" runat="server" Height="28px" Width="260px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Indicateurs <br/> de réussite prévus"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelEnteteRealisation3" runat="server" Height="28px" Width="179px"
                                        BackColor="#D7FAF3" BorderStyle="None" 
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="297px" DonHeight="140px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableIndicateur3" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="256px" DonHeight="140px" DonTabIndex="8"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="RadioHE04" runat="server" V_Groupe="Realisation3"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="insuffisant" VRadioN2Text="partiellement atteint" VRadioN3Text="atteint"
                                            VRadioN4Text="dépassé" VRadioN5Text="exceptionnel" VRadioN6Text=""
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableEnteteCommentaire3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteCommentaire3" runat="server" Height="18px" Width="748px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Commentaires"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableCommentaire3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="9"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="LabelNumerobjectif4" runat="server" Height="25px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°4"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif4" runat="server" Height="28px" Width="301px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteIndicateurs4" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteIndicateurs4" runat="server" Height="28px" Width="260px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Indicateurs <br/> de réussite prévus"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelEnteteRealisation4" runat="server" Height="28px" Width="179px"
                                        BackColor="#D7FAF3" BorderStyle="None" 
                                        Text="Atteinte des objectifs et actions conduites"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="297px" DonHeight="140px" DonTabIndex="10"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableIndicateur4" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="256px" DonHeight="140px" DonTabIndex="11"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="RadioHF04" runat="server" V_Groupe="Realisation4"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="insuffisant" VRadioN2Text="partiellement atteint" VRadioN3Text="atteint"
                                            VRadioN4Text="dépassé" VRadioN5Text="exceptionnel" VRadioN6Text=""
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableEnteteCommentaire4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteCommentaire4" runat="server" Height="18px" Width="748px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Commentaires"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableCommentaire4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="12"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
</asp:Table>
