﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_FORMATION_FAM
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
            End If
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirControle.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirControle.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            If NumInfo = 2 Then 'Service évaluateur
                VirControle.V_SiEnLectureSeule = True
            End If
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirCombo As VirtualiaControle_VListeCombo
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "ListC", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirCombo = CType(Ctl, VirtualiaControle_VListeCombo)
            NumObjet = VirCombo.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirCombo.LstText = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VTrioVerticalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VTrioVerticalRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.V_SiAutoPostBack = Not (WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1)))
            Select Case NumInfo
                Case 2
                    Call RecoursDifSiEnLectureSeule(Strings.Mid(Ctl.ID, 7, 1), WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1)))
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "T1"
                            VirRadio.RadioGaucheCheck = True
                            Call RecoursDifSiEnLectureSeule(Strings.Mid(Ctl.ID, 7, 1), True)
                        Case Is = "T2"
                            VirRadio.RadioCentreCheck = True
                        Case Is = "T3"
                            VirRadio.RadioDroiteCheck = True
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
                Case 6
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "Favorable"
                            VirRadio.RadioGaucheCheck = True
                        Case Is = "Refusé"
                            VirRadio.RadioCentreCheck = True
                        Case Is = "Reporté"
                            VirRadio.RadioDroiteCheck = True
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
                Case 10
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "Agent"
                            VirRadio.RadioGaucheCheck = True
                        Case Is = "Responsable"
                            VirRadio.RadioCentreCheck = True
                            Call PositionnerAvis(Strings.Mid(Ctl.ID, 7, 1))
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
                Case 11
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "Oui"
                            VirRadio.RadioGaucheCheck = True
                        Case Is = "Non"
                            VirRadio.RadioCentreCheck = True
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
            End Select
            IndiceI += 1
        Loop
    End Sub

    Private Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles _
        InfoHA03.ValeurChange, InfoHA04.ValeurChange, _
        InfoHB03.ValeurChange, InfoHB04.ValeurChange, _
        InfoHC03.ValeurChange, InfoHC04.ValeurChange, _
        InfoHD03.ValeurChange, InfoHD04.ValeurChange, _
        InfoHE03.ValeurChange, InfoHE04.ValeurChange, _
        InfoHF03.ValeurChange, InfoHF04.ValeurChange, _
        InfoHG03.ValeurChange, InfoHG04.ValeurChange, _
        InfoHH03.ValeurChange, InfoHH04.ValeurChange, _
        InfoHI03.ValeurChange, InfoHI04.ValeurChange, _
        InfoHJ03.ValeurChange, InfoHJ04.ValeurChange, _
        InfoHK01.ValeurChange, InfoHK04.ValeurChange, _
        InfoHL01.ValeurChange, InfoHL04.ValeurChange, _
        InfoHM01.ValeurChange, InfoHM04.ValeurChange, _
        InfoHN01.ValeurChange, InfoHN04.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 6, 1))

        Select Case NumObjet
            Case 155
                If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_155(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        CType(sender, VirtualiaControle_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select

    End Sub

    Private Sub ListC_ValeurChange(sender As Object, e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListCA01.ValeurChange, ListCB01.ValeurChange, _
        ListCC01.ValeurChange, ListCD01.ValeurChange, ListCE01.ValeurChange, ListCF01.ValeurChange, ListCG01.ValeurChange, ListCH01.ValeurChange, _
        ListCI01.ValeurChange, ListCJ01.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VListeCombo).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VListeCombo).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VListeCombo).ID, 6, 1))

        Select Case NumObjet
            Case 155
                If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                    If WsDossierPER.SiReadOnly(NumObjet, NumInfo) = True Then
                        CType(sender, VirtualiaControle_VListeCombo).LstText = WsDossierPER.Objet_155(Rang).V_TableauData(NumInfo).ToString
                        Exit Sub
                    End If
                    If WsDossierPER.Objet_155(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        CType(sender, VirtualiaControle_VListeCombo).LstBackColor = V_WebFonction.CouleurMaj
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoVE03.ValeurChange, _
    InfoVF03.ValeurChange, InfoVA05.ValeurChange, InfoVB05.ValeurChange, InfoVC05.ValeurChange, InfoVD05.ValeurChange, InfoVE05.ValeurChange, _
    InfoVF05.ValeurChange, InfoVG05.ValeurChange, InfoVH05.ValeurChange, InfoVI05.ValeurChange, InfoVJ05.ValeurChange, _
    InfoVK03.ValeurChange, InfoVL03.ValeurChange, InfoVM05.ValeurChange, InfoVN05.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1))
        Select Case NumObjet
            Case 154
                If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_154(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
            Case 155
                If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_155(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub RadioValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles _
        RadioVA02.ValeurChange, RadioVA06.ValeurChange, RadioVA10.ValeurChange, _
        RadioVB02.ValeurChange, RadioVB06.ValeurChange, RadioVB10.ValeurChange, _
        RadioVC02.ValeurChange, RadioVC06.ValeurChange, RadioVC10.ValeurChange, _
        RadioVD02.ValeurChange, RadioVD06.ValeurChange, RadioVD10.ValeurChange, _
        RadioVE02.ValeurChange, RadioVE06.ValeurChange, RadioVE10.ValeurChange, _
        RadioVF02.ValeurChange, RadioVF06.ValeurChange, RadioVF10.ValeurChange, _
        RadioVG02.ValeurChange, RadioVG06.ValeurChange, RadioVG10.ValeurChange, _
        RadioVH02.ValeurChange, RadioVH06.ValeurChange, RadioVH10.ValeurChange, _
        RadioVI02.ValeurChange, RadioVI06.ValeurChange, RadioVI10.ValeurChange, _
        RadioVJ02.ValeurChange, RadioVJ06.ValeurChange, RadioVJ10.ValeurChange, _
        RadioVK06.ValeurChange, RadioVL06.ValeurChange, RadioVM06.ValeurChange, _
        RadioVM10.ValeurChange, RadioVN06.ValeurChange, RadioVN10.ValeurChange, _
        RadioVA11.ValeurChange, RadioVB11.ValeurChange, RadioVC11.ValeurChange, _
        RadioVD11.ValeurChange, RadioVE11.ValeurChange, RadioVF11.ValeurChange, _
        RadioVG11.ValeurChange, RadioVH11.ValeurChange, RadioVI11.ValeurChange, _
        RadioVJ11.ValeurChange, RadioVK11.ValeurChange, RadioVL11.ValeurChange, _
        RadioVM11.ValeurChange, RadioVN11.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VTrioVerticalRadio).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VTrioVerticalRadio).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VTrioVerticalRadio).ID, 7, 1))

        Select Case NumObjet
            Case 155
                If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_155(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                        '*******************************************************************************
                        Select Case NumInfo
                            Case 2
                                If e.Valeur = "T1" Then
                                    WsDossierPER.TableauMaj(NumObjet, 11, Rang) = "Non"
                                End If
                            Case 10
                                If e.Valeur = "Responsable" Then
                                    WsDossierPER.TableauMaj(NumObjet, 6, Rang) = "Favorable"
                                End If
                        End Select
                        '*******************************************************************************
                    End If
                End If
        End Select
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ListeFormationsSuivies.Centrage_Colonne(0) = 1
        ListeFormationsSuivies.Centrage_Colonne(1) = 0
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        Etiquette.Text = "COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL - Année " & WsDossierPER.Annee
        LabelIdentite.Text = WsDossierPER.LibelleIdentite

        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        'Liste des formations 
        V_LibelListe = "Aucune formation" & VI.Tild & "Une formation" & VI.Tild & "formations"
        V_LibelColonne = "date" & VI.Tild & "formation suivie" & VI.Tild & "Clef"
        ListeFormationsSuivies.V_Liste(V_LibelColonne, V_LibelListe) = WsDossierPER.ListeDesFormationsSuivies(WsDossierPER.Annee, 1)

        Call LireLaFiche()
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop
        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 155
                    If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_155(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property

    Private Sub RecoursDifSiEnLectureSeule(ByVal Lettre As String, ByVal SiEnLectureSeule As Boolean)
        Select Case Lettre
            Case "A"
                RadioVA11.V_SiEnLectureSeule = SiEnLectureSeule
                Select Case SiEnLectureSeule
                    Case False
                        RadioVA11.RadioGaucheVisible = True
                    Case True
                        RadioVA11.RadioGaucheCheck = False
                        RadioVA11.RadioGaucheVisible = False
                        RadioVA11.RadioCentreCheck = True
                End Select
            Case "B"
                RadioVB11.V_SiEnLectureSeule = SiEnLectureSeule
                Select Case SiEnLectureSeule
                    Case False
                        RadioVB11.RadioGaucheVisible = True
                    Case True
                        RadioVB11.RadioGaucheCheck = False
                        RadioVB11.RadioGaucheVisible = False
                        RadioVB11.RadioCentreCheck = True
                End Select
            Case "C"
                RadioVC11.V_SiEnLectureSeule = SiEnLectureSeule
                Select Case SiEnLectureSeule
                    Case False
                        RadioVC11.RadioGaucheVisible = True
                    Case True
                        RadioVC11.RadioGaucheCheck = False
                        RadioVC11.RadioGaucheVisible = False
                        RadioVC11.RadioCentreCheck = True
                End Select
            Case "D"
                RadioVD11.V_SiEnLectureSeule = SiEnLectureSeule
                Select Case SiEnLectureSeule
                    Case False
                        RadioVD11.RadioGaucheVisible = True
                    Case True
                        RadioVD11.RadioGaucheCheck = False
                        RadioVD11.RadioGaucheVisible = False
                        RadioVD11.RadioCentreCheck = True
                End Select
            Case "E"
                RadioVE11.V_SiEnLectureSeule = SiEnLectureSeule
                Select Case SiEnLectureSeule
                    Case False
                        RadioVE11.RadioGaucheVisible = True
                    Case True
                        RadioVE11.RadioGaucheCheck = False
                        RadioVE11.RadioGaucheVisible = False
                        RadioVE11.RadioCentreCheck = True
                End Select
            Case "F"
                RadioVF11.V_SiEnLectureSeule = SiEnLectureSeule
                Select Case SiEnLectureSeule
                    Case False
                        RadioVF11.RadioGaucheVisible = True
                    Case True
                        RadioVF11.RadioGaucheCheck = False
                        RadioVF11.RadioGaucheVisible = False
                        RadioVF11.RadioCentreCheck = True
                End Select
            Case "G"
                RadioVG11.V_SiEnLectureSeule = SiEnLectureSeule
                Select Case SiEnLectureSeule
                    Case False
                        RadioVG11.RadioGaucheVisible = True
                    Case True
                        RadioVG11.RadioGaucheCheck = False
                        RadioVG11.RadioGaucheVisible = False
                        RadioVG11.RadioCentreCheck = True
                End Select
            Case "H"
                RadioVH11.V_SiEnLectureSeule = SiEnLectureSeule
                Select Case SiEnLectureSeule
                    Case False
                        RadioVH11.RadioGaucheVisible = True
                    Case True
                        RadioVH11.RadioGaucheCheck = False
                        RadioVH11.RadioGaucheVisible = False
                        RadioVH11.RadioCentreCheck = True
                End Select
            Case "I"
                RadioVI11.V_SiEnLectureSeule = SiEnLectureSeule
                Select Case SiEnLectureSeule
                    Case False
                        RadioVI11.RadioGaucheVisible = True
                    Case True
                        RadioVI11.RadioGaucheCheck = False
                        RadioVI11.RadioGaucheVisible = False
                        RadioVI11.RadioCentreCheck = True
                End Select
            Case "J"
                RadioVJ11.V_SiEnLectureSeule = SiEnLectureSeule
                Select Case SiEnLectureSeule
                    Case False
                        RadioVJ11.RadioGaucheVisible = True
                    Case True
                        RadioVJ11.RadioGaucheCheck = False
                        RadioVJ11.RadioGaucheVisible = False
                        RadioVJ11.RadioCentreCheck = True
                End Select
            Case Else
                Exit Sub
        End Select
    End Sub

    Private Sub PositionnerAvis(ByVal Lettre As String)
        Select Case Lettre
            Case "A"
                RadioVA06.RadioGaucheCheck = True
            Case "B"
                RadioVB06.RadioGaucheCheck = True
            Case "C"
                RadioVC06.RadioGaucheCheck = True
            Case "D"
                RadioVD06.RadioGaucheCheck = True
            Case "E"
                RadioVE06.RadioGaucheCheck = True
            Case "F"
                RadioVF06.RadioGaucheCheck = True
            Case "G"
                RadioVG06.RadioGaucheCheck = True
            Case "H"
                RadioVH06.RadioGaucheCheck = True
            Case "I"
                RadioVI06.RadioGaucheCheck = True
            Case "J"
                RadioVJ06.RadioGaucheCheck = True
            Case "K"
                RadioVK06.RadioGaucheCheck = True
            Case "l"
                RadioVL06.RadioGaucheCheck = True
            Case "M"
                RadioVM06.RadioGaucheCheck = True
            Case "N"
                RadioVN06.RadioGaucheCheck = True
            Case Else
                Exit Sub
        End Select
    End Sub

End Class