﻿Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class VEntretienFAM
        Implements Virtualia.Net.Entretien.IGenericEntretien
        Private WsLstCompetences As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstObservations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstFormations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstSyntheses As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstComplements As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing

        Public ReadOnly Property LitteralCompetence(Critere As Integer, Optional SiPrecision As Boolean = False) As String Implements IGenericEntretien.LitteralCompetence
            Get
                Select Case SiPrecision
                    Case True
                        Select Case Critere
                            Case 1
                                Return "A acquérir"
                            Case 2
                                Return "A développer"
                            Case 3
                                Return "Maîtrise"
                            Case 4
                                Return "Excellente maîtrise"
                            Case Else
                                Return "A acquérir"
                        End Select
                    Case False
                        Select Case Critere
                            Case 1
                                Return "Non requis"
                            Case 2
                                Return "Initié"
                            Case 3
                                Return "Pratique"
                            Case 4
                                Return "Maîtrise"
                            Case 5
                                Return "Expert"
                            Case Else
                                Return "Non requis"
                        End Select
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property LitteralResultat(Critere As Integer) As String Implements IGenericEntretien.LitteralResultat
            Get
                Select Case Critere
                    Case 0
                        Return "devenu sans objet"
                    Case 1
                        Return "non atteint"
                    Case 2
                        Return "partiellement atteint"
                    Case 3
                        Return "atteint"
                    Case Else
                        Return "devenu sans objet"
                End Select
            End Get
        End Property

        Public ReadOnly Property LitteralSynthese(Critere As Integer) As String Implements IGenericEntretien.LitteralSynthese
            Get
                Select Case Critere
                    Case 1
                        Return "A développer"
                    Case 2
                        Return "Satisfaisant"
                    Case 3
                        Return "Trés bon"
                    Case 4
                        Return "Excellent"
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property Table_Competences As List(Of ObjetTable) Implements IGenericEntretien.Table_Competences
            Get
                If WsLstCompetences IsNot Nothing Then
                    Return WsLstCompetences
                End If

                Dim IndiceFam As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstCompetences = New List(Of Virtualia.Net.Entretien.ObjetTable)

                For IndiceFam = 0 To 26
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Clef_Index = IndiceFam + 1 'Numero de Tri
                    Select Case IndiceFam
                        Case 0 To 4
                            ItemTable.Intitule = ""
                            ItemTable.Rang_Famille = "3.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Compétences"
                            ItemTable.Rang_SousFamille = "3.1"
                            ItemTable.Libelle_SousFamille = "Compétences"
                            Select Case IndiceFam
                                Case 0
                                    ItemTable.Clef_Origine = "A"
                                Case 1
                                    ItemTable.Clef_Origine = "B"
                                Case 2
                                    ItemTable.Clef_Origine = "C"
                                Case 3
                                    ItemTable.Clef_Origine = "D"
                                Case 4
                                    ItemTable.Clef_Origine = "E"
                            End Select

                        Case 5 To 12
                            ItemTable.Rang_Famille = "3.2" 'Numero de famille
                            ItemTable.Libelle_Famille = "Savoir-Faire"
                            ItemTable.Rang_SousFamille = "3.2"
                            ItemTable.Libelle_SousFamille = "Savoir-Faire"
                            Select Case IndiceFam
                                Case 5
                                    ItemTable.Intitule = "Travail en équipe"
                                    ItemTable.Clef_Origine = "F"
                                Case 6
                                    ItemTable.Intitule = "Capacité de synthèse"
                                    ItemTable.Clef_Origine = "G"
                                Case 7
                                    ItemTable.Intitule = "Capacité d'analyse"
                                    ItemTable.Clef_Origine = "H"
                                Case 8
                                    ItemTable.Intitule = "Animation d'équipe"
                                    ItemTable.Clef_Origine = "I"
                                Case 9
                                    ItemTable.Intitule = "Expression écrite"
                                    ItemTable.Clef_Origine = "J"
                                Case 10
                                    ItemTable.Intitule = "Expression orale"
                                    ItemTable.Clef_Origine = "K"
                                Case 11
                                    ItemTable.Intitule = "Techniques spécifiques"
                                    ItemTable.Clef_Origine = "L"
                                Case 12
                                    ItemTable.Intitule = "Autres"
                                    ItemTable.Clef_Origine = "M"
                            End Select

                        Case 13 To 19
                            ItemTable.Rang_Famille = "3.3" 'Numero de famille
                            ItemTable.Libelle_Famille = "Qualités relationnelles"
                            ItemTable.Rang_SousFamille = "3.3"
                            ItemTable.Libelle_SousFamille = "Qualités relationnelles"
                            Select Case IndiceFam
                                Case 13
                                    ItemTable.Intitule = "Sens des relations humaines"
                                    ItemTable.Clef_Origine = "N"
                                Case 14
                                    ItemTable.Intitule = "Capacité d'adaptation"
                                    ItemTable.Clef_Origine = "O"
                                Case 15
                                    ItemTable.Intitule = "Autonomie"
                                    ItemTable.Clef_Origine = "P"
                                Case 16
                                    ItemTable.Intitule = "Rigueur dans l'exécution des tâches"
                                    ItemTable.Clef_Origine = "Q"
                                Case 17
                                    ItemTable.Intitule = "Capacité d'initiative"
                                    ItemTable.Clef_Origine = "R"
                                Case 18
                                    ItemTable.Intitule = "Réactivité"
                                    ItemTable.Clef_Origine = "S"
                                Case 19
                                    ItemTable.Intitule = "Autres"
                                    ItemTable.Clef_Origine = "T"
                            End Select

                        Case 20 To 26
                            ItemTable.Rang_Famille = "3.4" 'Numero de famille
                            ItemTable.Libelle_Famille = "Aptitudes au management"
                            ItemTable.Rang_SousFamille = "3.4"
                            ItemTable.Libelle_SousFamille = "Aptitudes au management"
                            Select Case IndiceFam
                                Case 20
                                    ItemTable.Intitule = "Capacité à déléguer"
                                    ItemTable.Clef_Origine = "V"
                                Case 21
                                    ItemTable.Intitule = "Capacité à mobiliser et valoriser les compétences"
                                    ItemTable.Clef_Origine = "W"
                                Case 22
                                    ItemTable.Intitule = "Capacité d'organisation, de pilotage"
                                    ItemTable.Clef_Origine = "X"
                                Case 23
                                    ItemTable.Intitule = "Attention portée au développement professionnel des collaborateurs"
                                    ItemTable.Clef_Origine = "Y"
                                Case 24
                                    ItemTable.Intitule = "Aptitude à prévenir, arbitrer et gérer les conflits"
                                    ItemTable.Clef_Origine = "Z"
                                Case 25
                                    ItemTable.Intitule = "Aptitude à la prise de décision"
                                    ItemTable.Clef_Origine = "1"
                                Case 26
                                    ItemTable.Intitule = "Capacité à fixer des objectifs cohérents"
                                    ItemTable.Clef_Origine = "2"
                            End Select
                    End Select
                    WsLstCompetences.Add(ItemTable)
                Next IndiceFam
                Return WsLstCompetences
            End Get
        End Property

        Public ReadOnly Property Table_Formations As List(Of ObjetTable) Implements IGenericEntretien.Table_Formations
            Get
                If WsLstFormations IsNot Nothing Then
                    Return WsLstFormations
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstFormations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                For IndiceTri = 0 To 13
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Intitule = ""
                    ItemTable.Clef_Index = IndiceTri + 1
                    Select Case IndiceTri
                        Case 0
                            ItemTable.Clef_Origine = "A"
                        Case 1
                            ItemTable.Clef_Origine = "B"
                        Case 2
                            ItemTable.Clef_Origine = "C"
                        Case 3
                            ItemTable.Clef_Origine = "D"
                        Case 4
                            ItemTable.Clef_Origine = "E"
                        Case 5
                            ItemTable.Clef_Origine = "F"
                        Case 6
                            ItemTable.Clef_Origine = "G"
                        Case 7
                            ItemTable.Clef_Origine = "H"
                        Case 8
                            ItemTable.Clef_Origine = "I"
                        Case 9
                            ItemTable.Clef_Origine = "J"
                        Case 10
                            ItemTable.Clef_Origine = "K"
                            ItemTable.Libelle_Famille = "Préparation aux concours"
                        Case 11
                            ItemTable.Clef_Origine = "L"
                            ItemTable.Libelle_Famille = "Préparation aux concours"
                        Case 12
                            ItemTable.Clef_Origine = "M"
                            ItemTable.Libelle_Famille = "Autres actions"
                        Case 13
                            ItemTable.Clef_Origine = "N"
                            ItemTable.Libelle_Famille = "Autres actions"
                    End Select
                    WsLstFormations.Add(ItemTable)
                Next IndiceTri

                Return WsLstFormations
            End Get
        End Property

        Public ReadOnly Property Table_Observations As List(Of ObjetTable) Implements IGenericEntretien.Table_Observations
            Get
                If WsLstObservations IsNot Nothing Then
                    Return WsLstObservations
                End If

                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstObservations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaire sur les formations suivies"
                ItemTable.Rang_Famille = "4.1"
                ItemTable.Clef_Origine = "E"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaire sur les formations demandées"
                ItemTable.Rang_Famille = "4.2"
                ItemTable.Clef_Origine = "F"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Effectifs encadrés"
                ItemTable.Rang_Famille = "3"
                ItemTable.Clef_Origine = "G"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Contexte de l'année écoulée"
                ItemTable.Rang_Famille = "1.1"
                ItemTable.Clef_Origine = "H"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Difficultés ou facilités rencontrées"
                ItemTable.Rang_Famille = "1.3"
                ItemTable.Clef_Origine = "I"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Nature des dossiers ou travaux"
                ItemTable.Rang_Famille = "1.4.1"
                ItemTable.Clef_Origine = "J"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Résultats obtenus sur dossiers ou travaux"
                ItemTable.Rang_Famille = "1.4.2"
                ItemTable.Clef_Origine = "K"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Appréciation évaluateur sur la réalisation des objectifs de l'année écoulée"
                ItemTable.Rang_Famille = "1.5.1"
                ItemTable.Clef_Origine = "L"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Evaluateur - Eléments particuliers à prendre en compte"
                ItemTable.Rang_Famille = "1.5.2"
                ItemTable.Clef_Origine = "M"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l'agent sur la réalisation des objectifs"
                ItemTable.Rang_Famille = "1.5.3"
                ItemTable.Clef_Origine = "N"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Objectifs du service"
                ItemTable.Rang_Famille = "2.1.1"
                ItemTable.Clef_Origine = "O"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Contexte prévisible de l'année"
                ItemTable.Rang_Famille = "2.1.2"
                ItemTable.Clef_Origine = "P"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Evolution sur le poste actuel"
                ItemTable.Rang_Famille = "5.1"
                ItemTable.Clef_Origine = "Q"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Prise de responsabilités plus importante"
                ItemTable.Rang_Famille = "5.2"
                ItemTable.Clef_Origine = "R"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Projet professionnel"
                ItemTable.Rang_Famille = "5.3"
                ItemTable.Clef_Origine = "S"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations évaluateur sur les perspectives"
                ItemTable.Rang_Famille = "5.4"
                ItemTable.Clef_Origine = "T"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaire de l'agent sur les perspectives"
                ItemTable.Rang_Famille = "5.5"
                ItemTable.Clef_Origine = "U"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l'agent sur la conduite de l'entretien"
                ItemTable.Rang_Famille = "7.1"
                ItemTable.Clef_Origine = "V"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l'agent sur les appréciations portées"
                ItemTable.Rang_Famille = "7.2"
                ItemTable.Clef_Origine = "W"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l'évaluateur sur la conduite de l'entretien"
                ItemTable.Rang_Famille = "7.3"
                ItemTable.Clef_Origine = "X"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l'évaluateur sur les appréciations portées"
                ItemTable.Rang_Famille = "7.4"
                ItemTable.Clef_Origine = "Y"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations éventuelles de l'évaluateur"
                ItemTable.Rang_Famille = "7.5"
                ItemTable.Clef_Origine = "Z"
                WsLstObservations.Add(ItemTable)

                Return WsLstObservations
            End Get
        End Property

        Public ReadOnly Property Table_Syntheses As List(Of ObjetTable) Implements IGenericEntretien.Table_Syntheses
            Get
                If WsLstSyntheses IsNot Nothing Then
                    Return WsLstSyntheses
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstSyntheses = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Qualité du travail"
                ItemTable.Rang_Famille = "6.1"
                ItemTable.Clef_Origine = "A"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Qualités relationnelles"
                ItemTable.Rang_Famille = "6.2"
                ItemTable.Clef_Origine = "B"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Implication personnelle"
                ItemTable.Rang_Famille = "6.3"
                ItemTable.Clef_Origine = "C"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Sens du service public"
                ItemTable.Rang_Famille = "6.4"
                ItemTable.Clef_Origine = "D"
                WsLstSyntheses.Add(ItemTable)

                For IndiceTri = 0 To WsLstSyntheses.Count - 1
                    WsLstSyntheses.Item(IndiceTri).Clef_Index = IndiceTri + 1
                Next IndiceTri
                Return WsLstSyntheses
            End Get
        End Property

        Public ReadOnly Property Table_Complements As List(Of ObjetTable) Implements IGenericEntretien.Table_Complements
            Get
                If WsLstComplements IsNot Nothing Then
                    Return WsLstComplements
                End If

                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstComplements = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Si en situation potentielle de conflit d'intérêt"
                ItemTable.Clef_Index = 1
                ItemTable.Clef_Origine = "A"
                ItemTable.Libelle_Famille = CStr(VI.NatureDonnee.DonneeBooleenne)
                WsLstComplements.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Si déclaration de conflit d'intérêt en règle"
                ItemTable.Clef_Index = 2
                ItemTable.Clef_Origine = "B"
                ItemTable.Libelle_Famille = CStr(VI.NatureDonnee.DonneeBooleenne)
                WsLstComplements.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "La situation de l'agent vis-à-vis du risque de conflit d'intérêt a été abordé lors de l'entretien"
                ItemTable.Clef_Index = 3
                ItemTable.Clef_Origine = "K"
                ItemTable.Libelle_Famille = CStr(VI.NatureDonnee.DonneeBooleenne)
                WsLstComplements.Add(ItemTable)

                Return WsLstComplements
            End Get
        End Property
    End Class
End Namespace