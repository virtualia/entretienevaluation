﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_APPRECIATION_FAM
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirControle.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            Select Case NumInfo
                Case Is = 1
                    VirControle.V_SiEnLectureSeule = True
                Case Else
                    VirControle.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            End Select
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VSixBoutonRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VSixBoutonRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.V_SiAutoPostBack = Not (WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1)))
            If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                Select Case ValeurLue(NumObjet, NumInfo, Rang)
                    Case Is = "4"
                        VirRadio.VRadioN1Check = True
                    Case Is = "3"
                        VirRadio.VRadioN2Check = True
                    Case Is = "2"
                        VirRadio.VRadioN3Check = True
                    Case Is = "1"
                        VirRadio.VRadioN4Check = True
                    Case Else
                        VirRadio.VRadioN1Check = False
                        VirRadio.VRadioN2Check = False
                        VirRadio.VRadioN3Check = False
                        VirRadio.VRadioN4Check = False
                End Select
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoHA01.ValeurChange, _
    InfoHB01.ValeurChange, InfoHC01.ValeurChange, InfoHD01.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 6, 1))

        Select Case NumObjet
            Case 150
                If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
                    CType(sender, VirtualiaControle_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
                End If
            Case 156
                If WsDossierPER.Objet_156(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_156(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        CType(sender, VirtualiaControle_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV13.ValeurChange, _
        InfoV18.ValeurChange, InfoVV03.ValeurChange, InfoVW03.ValeurChange, InfoVX03.ValeurChange, InfoVY03.ValeurChange, InfoVZ03.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1))

        Select Case NumObjet
            Case 150
                If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
                    WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
                End If
            Case 156
                If WsDossierPER.Objet_156(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_156(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
            Case 154
                If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_154(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub RadioValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) _
        Handles RadioHA03.ValeurChange, RadioHB03.ValeurChange, RadioHC03.ValeurChange, RadioHD03.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VSixBoutonRadio).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VSixBoutonRadio).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VSixBoutonRadio).ID, 7, 1))
        Dim ValRadio As Integer = 0

        If IsNumeric(e.Valeur) Then
            ValRadio = 4 - CInt(e.Valeur)
        End If
        Select Case NumObjet
            Case 156
                If WsDossierPER.Objet_156(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_156(Rang).V_TableauData(NumInfo).ToString <> ValRadio.ToString Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = ValRadio.ToString
                    End If
                End If
        End Select
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")
        LabelEtiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        LabelEtiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        LabelEtiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        Etiquette.Text = "COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL - Année " & WsDossierPER.Annee
        LabelIdentite.Text = WsDossierPER.LibelleIdentite
        LabelEtiquette.Text = "COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL - Année " & WsDossierPER.Annee
        LabelIdentiteAgent.Text = WsDossierPER.LibelleIdentite

        Call LireLaFiche()
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop
        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 156
                    If WsDossierPER.Objet_156(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_156(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property
End Class