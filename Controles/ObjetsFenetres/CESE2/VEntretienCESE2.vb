﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class VEntretienCESE2
        Implements Virtualia.Net.Entretien.IGenericEntretien
        Private WsLstCompetences As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstObservations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstFormations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstSyntheses As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing

        Public ReadOnly Property LitteralCompetence(Critere As Integer, Optional SiPrecision As Boolean = False) As String Implements IGenericEntretien.LitteralCompetence
            Get
                Select Case SiPrecision
                    Case True
                        Select Case Critere
                            Case 0
                                Return "Sans objet"
                            Case 1
                                Return "A acquérir"
                            Case 2
                                Return "A développer"
                            Case 3
                                Return "Satisfaisant"
                            Case 4
                                Return "Excellent"
                            Case Else
                                Return "Sans objet"
                        End Select
                    Case False
                        Select Case Critere
                            Case 1
                                Return "A acquérir"
                            Case 2
                                Return "A développer"
                            Case 3
                                Return "Maîtrise"
                            Case 4
                                Return "Expertise"
                            Case Else
                                Return "A acquérir"
                        End Select
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property LitteralResultat(Critere As Integer) As String Implements IGenericEntretien.LitteralResultat
            Get
                Select Case Critere
                    Case 0
                        Return "devenu sans objet"
                    Case 1
                        Return "non atteint"
                    Case 2
                        Return "partiellement atteint"
                    Case 3
                        Return "atteint"
                    Case Else
                        Return "devenu sans objet"
                End Select
            End Get
        End Property

        Public ReadOnly Property LitteralSynthese(Critere As Integer) As String Implements IGenericEntretien.LitteralSynthese
            Get
                Select Case Critere
                    Case 1
                        Return "Oui"
                    Case 2
                        Return "Non"
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property Table_Competences As List(Of ObjetTable) Implements IGenericEntretien.Table_Competences
            Get
                If WsLstCompetences IsNot Nothing Then
                    Return WsLstCompetences
                End If

                Dim IndiceFam As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstCompetences = New List(Of Virtualia.Net.Entretien.ObjetTable)

                For IndiceFam = 0 To 16
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Clef_Index = IndiceFam + 1 'Numero de Tri
                    Select Case IndiceFam
                        Case 0 To 2
                            ItemTable.Intitule = ""
                            ItemTable.Rang_Famille = "3.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Compétences"
                            ItemTable.Rang_SousFamille = "3.1." & IndiceFam + 1
                            ItemTable.Libelle_SousFamille = "Connaissance du domaine"
                            Select Case IndiceFam
                                Case 0
                                    ItemTable.Clef_Origine = "A"
                                Case 1
                                    ItemTable.Clef_Origine = "B"
                                Case 2
                                    ItemTable.Clef_Origine = "C"
                            End Select

                        Case 3 To 12
                            ItemTable.Rang_Famille = "3.2" 'Numero de famille
                            ItemTable.Libelle_Famille = "Savoir-Faire"
                            ItemTable.Rang_SousFamille = "3.2." & IndiceFam - 2
                            ItemTable.Libelle_SousFamille = "Qualités du travail et qualités relationnelles"
                            Select Case IndiceFam
                                Case 3
                                    ItemTable.Intitule = "Capacité d'expertise et de proposition"
                                    ItemTable.Clef_Origine = "H"
                                Case 4
                                    ItemTable.Intitule = "Capacité à maîtriser les délais et les calendriers et à planifier son travail"
                                    ItemTable.Clef_Origine = "I"
                                Case 5
                                    ItemTable.Intitule = "Soin, efficacité, rigueur dans la réalisation du travail et des tâches"
                                    ItemTable.Clef_Origine = "J"
                                Case 6
                                    ItemTable.Intitule = "Capacité à transmettre des informations"
                                    ItemTable.Clef_Origine = "K"
                                Case 7
                                    ItemTable.Intitule = "Qualité de l'expression écrite ou orale"
                                    ItemTable.Clef_Origine = "L"
                                Case 8
                                    ItemTable.Intitule = "Capacité à utiliser les outils bureautiques"
                                    ItemTable.Clef_Origine = "M"
                                Case 9
                                    ItemTable.Intitule = "Capacité à travailler en équipe"
                                    ItemTable.Clef_Origine = "N"
                                Case 10
                                    ItemTable.Intitule = "Capacité d'adaptation à l'évolution"
                                    ItemTable.Clef_Origine = "O"
                                Case 11
                                    ItemTable.Intitule = "Sens des relations humaines"
                                    ItemTable.Clef_Origine = "P"
                                Case 12
                                    ItemTable.Intitule = "Autonomie"
                                    ItemTable.Clef_Origine = "Q"
                            End Select

                        Case 13 To 16
                            ItemTable.Rang_Famille = "3.3" 'Numero de famille
                            ItemTable.Libelle_Famille = "Encadrement"
                            ItemTable.Rang_SousFamille = "3.3." & IndiceFam - 12
                            ItemTable.Libelle_SousFamille = "Aptitude au management"
                            Select Case IndiceFam
                                Case 13
                                    ItemTable.Intitule = "Capacité d'organisation et de pilotage"
                                    ItemTable.Clef_Origine = "U"
                                Case 14
                                    ItemTable.Intitule = "Capacité à déléguer"
                                    ItemTable.Clef_Origine = "V"
                                Case 15
                                    ItemTable.Intitule = "Capacité à mobiliser et à valoriser les compétences"
                                    ItemTable.Clef_Origine = "W"
                                Case 16
                                    ItemTable.Intitule = "Attention portée au développement professionnel des collaborateurs"
                                    ItemTable.Clef_Origine = "X"
                            End Select
                    End Select
                    WsLstCompetences.Add(ItemTable)
                Next IndiceFam
                Return WsLstCompetences
            End Get
        End Property

        Public ReadOnly Property Table_Formations As List(Of ObjetTable) Implements IGenericEntretien.Table_Formations
            Get
                If WsLstFormations IsNot Nothing Then
                    Return WsLstFormations
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstFormations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                For IndiceTri = 1 To 3
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Intitule = ""
                    ItemTable.Libelle_Famille = "Formation à court terme"
                    ItemTable.Rang_Famille = "Adaptation au poste de travail et à son environnement"
                    Select Case IndiceTri
                        Case 1
                            ItemTable.Clef_Origine = "A"
                        Case 2
                            ItemTable.Clef_Origine = "B"
                        Case 3
                            ItemTable.Clef_Origine = "C"
                    End Select
                    WsLstFormations.Add(ItemTable)
                Next IndiceTri

                For IndiceTri = 1 To 3
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Intitule = ""
                    ItemTable.Libelle_Famille = "Formation à moyen et long terme"
                    ItemTable.Rang_Famille = "Préparation à l'évolution du métier ou de la carrière"
                    Select Case IndiceTri
                        Case 1
                            ItemTable.Clef_Origine = "G"
                        Case 2
                            ItemTable.Clef_Origine = "H"
                        Case 3
                            ItemTable.Clef_Origine = "I"
                    End Select
                    WsLstFormations.Add(ItemTable)
                Next IndiceTri

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "VAE"
                ItemTable.Rang_Famille = "Validation des acquis de l'expérience"
                ItemTable.Clef_Origine = "M"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Evolution professionnelle"
                ItemTable.Rang_Famille = "Concours ou mobilité interne ou externe"
                ItemTable.Clef_Origine = "N"
                WsLstFormations.Add(ItemTable)

                For IndiceTri = 0 To WsLstFormations.Count - 1
                    WsLstFormations.Item(IndiceTri).Clef_Index = IndiceTri + 1
                Next IndiceTri

                Return WsLstFormations
            End Get
        End Property

        Public ReadOnly Property Table_Observations As List(Of ObjetTable) Implements IGenericEntretien.Table_Observations
            Get
                If WsLstObservations IsNot Nothing Then
                    Return WsLstObservations
                End If

                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstObservations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Bilan global des résultats atteints"
                ItemTable.Rang_Famille = "1.1"
                ItemTable.Clef_Origine = "F"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Autres travaux"
                ItemTable.Rang_Famille = "1.2"
                ItemTable.Clef_Origine = "G"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Résultats sur autres travaux"
                ItemTable.Rang_Famille = "1.3"
                ItemTable.Clef_Origine = "H"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Rappel des objectifs collectifs assignés au service"
                ItemTable.Rang_Famille = "2.1"
                ItemTable.Clef_Origine = "I"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaires sur les formations de l'année écoulée"
                ItemTable.Rang_Famille = "4.1"
                ItemTable.Clef_Origine = "P"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Evolution conduisant à un avancement normal"
                ItemTable.Rang_Famille = "5.1"
                ItemTable.Clef_Origine = "J"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Evolution conduisant à un avancement accéléré"
                ItemTable.Rang_Famille = "5.2"
                ItemTable.Clef_Origine = "K"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Dernières augmentations de la part variable obtenues"
                ItemTable.Rang_Famille = "5.4"
                ItemTable.Clef_Origine = "M"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhaitez-vous apporter des précisions à la description qui a été faite de vos fonctions ? Si oui lesquelles ?"
                ItemTable.Rang_Famille = "6.1"
                ItemTable.Clef_Origine = "Q"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Partagez-vous l'évaluation générale vous concernant ? Sinon pourquoi ?"
                ItemTable.Rang_Famille = "6.2"
                ItemTable.Clef_Origine = "R"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Vous sentez-vous à l'aise dans votre service ? Si non pourquoi ?"
                ItemTable.Rang_Famille = "6.3"
                ItemTable.Clef_Origine = "S"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhaits évolution professionnelle ? Evolution sur le poste,prises de responsabilités,projet professionnel,avancement"
                ItemTable.Rang_Famille = "6.4"
                ItemTable.Clef_Origine = "T"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Autres observations de l'agent"
                ItemTable.Rang_Famille = "6.5"
                ItemTable.Clef_Origine = "U"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Date de la dernière revalorisation salariale"
                ItemTable.Rang_Famille = "6.6"
                ItemTable.Clef_Origine = "Z"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Montant de la dernière revalorisation salariale"
                ItemTable.Rang_Famille = "6.7"
                ItemTable.Clef_Origine = "D"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Perspective pour l'entretien tri-annuel de revalorisation"
                ItemTable.Rang_Famille = "6.8"
                ItemTable.Clef_Origine = "E"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Conditions de travail - Conditions matérielles"
                ItemTable.Rang_Famille = "7.1"
                ItemTable.Clef_Origine = "V"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Conditions de travail - Environnement de travail"
                ItemTable.Rang_Famille = "7.2"
                ItemTable.Clef_Origine = "W"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Conditions de travail - Observations du supérieur hiérarchique"
                ItemTable.Rang_Famille = "7.3"
                ItemTable.Clef_Origine = "X"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Conditions de travail - Retour d’expérience - Observations formulées en N-1"
                ItemTable.Rang_Famille = "7.4"
                ItemTable.Clef_Origine = "Y"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Date de la fonction exercée"
                ItemTable.Rang_Famille = "0.1"
                ItemTable.Clef_Origine = "A"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Motif en cas d'entretien non effectué"
                ItemTable.Rang_Famille = "0.2"
                ItemTable.Clef_Origine = "B"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Fonction de l'évaluateur"
                ItemTable.Rang_Famille = "0.3"
                ItemTable.Clef_Origine = "C"
                WsLstObservations.Add(ItemTable)

                Return WsLstObservations
            End Get
        End Property

        Public ReadOnly Property Table_Syntheses As List(Of ObjetTable) Implements IGenericEntretien.Table_Syntheses
            Get
                If WsLstSyntheses IsNot Nothing Then
                    Return WsLstSyntheses
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstSyntheses = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Réductions d'ancienneté sans objet pour les Administrateurs"
                ItemTable.Rang_Famille = "5.2.0"
                ItemTable.Clef_Origine = "L"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Proposition d'une augmentation de la part variable"
                ItemTable.Rang_Famille = "5.2.1"
                ItemTable.Clef_Origine = "A"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "3.1 Sans objet pour le moment"
                ItemTable.Rang_Famille = "5.3.1"
                ItemTable.Clef_Origine = "B"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "3.2 Possible dans l'année ou à court terme"
                ItemTable.Rang_Famille = "5.3.2"
                ItemTable.Clef_Origine = "M"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "3.3 Souhaitable à moyen terme"
                ItemTable.Rang_Famille = "5.3.3"
                ItemTable.Clef_Origine = "C"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "3.4 A envisager"
                ItemTable.Rang_Famille = "5.3.4"
                ItemTable.Clef_Origine = "D"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "3.5 A été réalisé dans l’année"
                ItemTable.Rang_Famille = "5.3.5"
                ItemTable.Clef_Origine = "E"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "4.1 Pas encore à l'ordre du jour"
                ItemTable.Rang_Famille = "5.4.1"
                ItemTable.Clef_Origine = "F"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "4.2 Envisageable à moyen terme"
                ItemTable.Rang_Famille = "5.4.2"
                ItemTable.Clef_Origine = "G"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "4.3 Recommandée fortement par l'évaluateur"
                ItemTable.Rang_Famille = "5.4.3"
                ItemTable.Clef_Origine = "H"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "5.1 Pas encore à l'ordre du jour"
                ItemTable.Rang_Famille = "5.5.1"
                ItemTable.Clef_Origine = "I"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "5.2 Envisageable à moyen terme"
                ItemTable.Rang_Famille = "5.5.2"
                ItemTable.Clef_Origine = "J"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "5.3 Recommandée fortement par l'évaluateur"
                ItemTable.Rang_Famille = "5.5.3"
                ItemTable.Clef_Origine = "K"
                WsLstSyntheses.Add(ItemTable)

                For IndiceTri = 0 To WsLstSyntheses.Count - 1
                    WsLstSyntheses.Item(IndiceTri).Clef_Index = IndiceTri + 1
                Next IndiceTri
                Return WsLstSyntheses
            End Get
        End Property

        Public ReadOnly Property Table_Complements As List(Of ObjetTable) Implements IGenericEntretien.Table_Complements
            Get
                Return Nothing
            End Get
        End Property
    End Class
End Namespace
