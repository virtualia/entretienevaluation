﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_FORMATION_CESE2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_FORMATION_CESE2" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreFormation" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="FORMATION" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="34px" Width="748px"
                           Text="Bilan de l'année écoulée"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 6px; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="ListeFormationsSuivies" runat="server" CadreWidth="746px" SiColonneSelect="false"/>
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="190px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="True" EtiText="Compétences acquises, formations suivies ou non satisfaites, difficultés éventuelles rencontrées"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 3px;"
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero21" runat="server" Height="34px" Width="748px"
                           Text="Formations souhaitées par l'agent et propositions de l'évaluateur"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 6px; text-indent: 0px; text-align: center; margin-bottom: -1px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero22" runat="server" Height="18px" Width="748px"
                           Text="(à remplir pendant l'entretien)"
                           CssClass="EP_TitreParagraphe"
                           Font-Size="Small"
                           style="text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero23" runat="server" Height="20px" Width="748px" Text="A court terme"
                           CssClass="EP_TitreChapitre"
                           Font-Bold="true" Font-Size="Small"
                           style="padding-top: 4px; text-indent: 10px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero24" runat="server" Height="20px" Width="740px"
                           Text="Afin de permettre d'assurer au mieux les fonctions confiées et d'atteindre les objectifs 
                           individuels et collectifs – Formation d'adaptation au poste de travail et à son environnement"
                           CssClass="EP_EtiComplement"
                           Font-Size="90%" style="margin-left: 8px; padding-top: 4px; text-indent: 2px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin102" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="740px" DonHeight="40px" DonTabIndex="2" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>                          
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="8px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="740px" DonHeight="40px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="8px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="740px" DonHeight="40px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>             
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin103" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell Height="8px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTypeBesoin103" runat="server" Height="20px" Width="748px" Text="A moyen et long terme"
                                   CssClass="EP_TitreChapitre"
                                   Font-Bold="true" Font-Size="Small"
                                   style="padding-top: 4px; text-indent: 10px; text-align: left;">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTypeBesoin103Bis" runat="server" Height="20px" Width="740px"
                                   Text="Pour permettre l'évolution professionnelle de l'agent (évolution du métier, perspective d'un projet de service) – Formation préparant à l'évolution du métier ou de la carrière ?"
                                   CssClass="EP_EtiComplement"
                                   Font-Size="90%" style="margin-left: 8px; padding-top: 4px; text-indent: 2px">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="740px" DonHeight="40px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>             
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="8px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="740px" DonHeight="40px" DonTabIndex="6" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>             
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="8px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="740px" DonHeight="40px" DonTabIndex="7" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>             
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin104" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTypeBesoin104" runat="server" Height="20px" Width="748px"
                                   Text="Possibilité d'une Validation des Acquis de l'Expérience"
                                   CssClass="EP_TitreChapitre"
                                   Font-Bold="true" Font-Size="Small"
                                   style="padding-top: 4px; text-indent: 10px; text-align: left;">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTypeBesoin104Bis" runat="server" Height="30px" Width="740px"
                                   Text="Si oui dans quel domaine ?"
                                   CssClass="EP_EtiComplement"
                                   Font-Size="90%" style="margin-left: 8px; padding-top: 4px; text-indent: 2px">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="740px" DonHeight="80px" DonTabIndex="8" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>             
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin105" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTypeBesoin105" runat="server" Height="20px" Width="748px"
                                   Text="Evolution professionnelle"
                                   CssClass="EP_TitreChapitre"
                                   Font-Bold="true" Font-Size="Small"
                                   style="padding-top: 4px; text-indent: 10px; text-align: left;">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTypeBesoin105Bis" runat="server" Height="20px" Width="740px"
                                   Text="Préparation d'examen professionnel ou de concours – ou action de formation souhaitée dans la perspective d'une mobilité interne ou externe"
                                   CssClass="EP_EtiComplement"
                                   Font-Size="90%" style="margin-left: 8px; padding-top: 4px; text-indent: 2px">
                                </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="740px" DonHeight="80px" DonTabIndex="9" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>             
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="10px"></asp:TableCell>
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>