﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class VEntretienCE
        Implements Virtualia.Net.Entretien.IGenericEntretien
        Private WsLstCompetences As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstObservations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstFormations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstSyntheses As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing

        Public ReadOnly Property LitteralSynthese(ByVal Critere As Integer) As String Implements IGenericEntretien.LitteralSynthese
            Get
                Select Case Critere
                    Case 1
                        Return "insuffisant"
                    Case 2
                        Return "en cours d'acquisition"
                    Case 3
                        Return "acquis"
                    Case 4
                        Return "maîtrisé"
                    Case 5
                        Return "expert"
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property LitteralResultat(ByVal Critere As Integer) As String Implements IGenericEntretien.LitteralResultat
            Get
                Select Case Critere
                    Case 1
                        Return "insuffisante"
                    Case 2
                        Return "partielle"
                    Case 3
                        Return "complète"
                    Case Else
                        Return "insuffisante"
                End Select
            End Get
        End Property

        Public ReadOnly Property LitteralCompetence(ByVal Critere As Integer, Optional ByVal SiPrecision As Boolean = False) As String Implements IGenericEntretien.LitteralCompetence
            Get
                Select Case Critere
                    Case 1
                        Return "insuffisant"
                    Case 2
                        Return "en cours d'acquisition"
                    Case 3
                        Return "acquis"
                    Case 4
                        Return "maîtrisé"
                    Case 5
                        Return "expert"
                    Case Else
                        Return "non requis"
                End Select
            End Get
        End Property

        Public ReadOnly Property Table_Competences() As List(Of Virtualia.Net.Entretien.ObjetTable) Implements IGenericEntretien.Table_Competences
            Get
                If WsLstCompetences IsNot Nothing Then
                    Return WsLstCompetences
                End If

                Dim IndiceFam As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstCompetences = New List(Of Virtualia.Net.Entretien.ObjetTable)

                For IndiceFam = 0 To 29
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    Select Case IndiceFam
                        Case 0 To 11
                            ItemTable.Clef_Index = IndiceFam + 1 'Numero de Tri
                            ItemTable.Rang_Famille = "3.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Compétences professionnelles et technicité"
                            ItemTable.Rang_SousFamille = ""
                            ItemTable.Libelle_SousFamille = ""
                            Select Case IndiceFam
                                Case 0 To 7
                                    ItemTable.Rang_SousFamille = "3.1.1"
                                    ItemTable.Libelle_SousFamille = "Connaissances du domaine d'activité"
                                    Select Case IndiceFam
                                        Case 0
                                            ItemTable.Intitule = "juridiques"
                                            ItemTable.Clef_Origine = "A"
                                        Case 1
                                            ItemTable.Intitule = "administratives"
                                            ItemTable.Clef_Origine = "B"
                                        Case 2
                                            ItemTable.Intitule = "budgétaires et financières"
                                            ItemTable.Clef_Origine = "C"
                                        Case 3
                                            ItemTable.Intitule = "gestion des ressources humaines"
                                            ItemTable.Clef_Origine = "D"
                                        Case 4
                                            ItemTable.Intitule = "techniques"
                                            ItemTable.Clef_Origine = "E"
                                        Case 5
                                            ItemTable.Intitule = "nouvelles technologies"
                                            ItemTable.Clef_Origine = "F"
                                        Case 6
                                            ItemTable.Intitule = "accueil et orientation du public"
                                            ItemTable.Clef_Origine = "G"
                                        Case 7
                                            ItemTable.Intitule = "autres"
                                            ItemTable.Clef_Origine = "H"
                                    End Select
                                Case 8
                                    ItemTable.Intitule = "Souci de formation et de perfectionnement"
                                    ItemTable.Clef_Origine = "I"
                                    ItemTable.Rang_SousFamille = "3.1.2"
                                Case 9
                                    ItemTable.Intitule = "Connaissances de l'environnement professionnel et capacités à s'y situer"
                                    ItemTable.Clef_Origine = "J"
                                    ItemTable.Rang_SousFamille = "3.1.3"
                                Case 10
                                    ItemTable.Intitule = "Qualités d'expression écrite"
                                    ItemTable.Clef_Origine = "K"
                                    ItemTable.Rang_SousFamille = "3.1.4"
                                Case 11
                                    ItemTable.Intitule = "Qualités d'expression orale"
                                    ItemTable.Clef_Origine = "L"
                                    ItemTable.Rang_SousFamille = "3.1.5"
                            End Select

                        Case 12 To 17
                            ItemTable.Clef_Index = IndiceFam + 9 'Numero de Tri
                            ItemTable.Rang_Famille = "3.2" 'Numero de famille
                            ItemTable.Libelle_Famille = "Qualités personnelles et relationneles"
                            ItemTable.Rang_SousFamille = ""
                            ItemTable.Libelle_SousFamille = ""
                            Select Case IndiceFam
                                Case 12
                                    ItemTable.Intitule = "Présentation"
                                    ItemTable.Clef_Origine = "M"
                                    ItemTable.Rang_SousFamille = "3.2.1"
                                Case 13
                                    ItemTable.Intitule = "Maîtrise de soi"
                                    ItemTable.Clef_Origine = "N"
                                    ItemTable.Rang_SousFamille = "3.2.2"
                                Case 14
                                    ItemTable.Intitule = "Aptitudes relationnelles et sens des relations humaines"
                                    ItemTable.Clef_Origine = "O"
                                    ItemTable.Rang_SousFamille = "3.2.3"
                                Case 15
                                    ItemTable.Intitule = "Esprit d'initiative"
                                    ItemTable.Clef_Origine = "P"
                                    ItemTable.Rang_SousFamille = "3.2.4"
                                Case 16
                                    ItemTable.Intitule = "Sens du service public et conscience professionnelle"
                                    ItemTable.Clef_Origine = "Q"
                                    ItemTable.Rang_SousFamille = "3.2.5"
                                Case 17
                                    ItemTable.Intitule = "Capacité à respecter l'organisation collective du travail et sens du travail en équipe"
                                    ItemTable.Clef_Origine = "R"
                                    ItemTable.Rang_SousFamille = "3.2.6"
                            End Select

                        Case 18 To 23
                            ItemTable.Clef_Index = IndiceFam + 13 'Numero de Tri
                            ItemTable.Rang_Famille = "3.3" 'Numero de famille
                            ItemTable.Libelle_Famille = "Méthode et résultats"
                            ItemTable.Rang_SousFamille = ""
                            ItemTable.Libelle_SousFamille = ""
                            Select Case IndiceFam
                                Case 18
                                    ItemTable.Intitule = "Fiabilité et qualité du travail"
                                    ItemTable.Clef_Origine = "S"
                                    ItemTable.Rang_SousFamille = "3.3.1"
                                Case 19
                                    ItemTable.Intitule = "Capacités d'analyse"
                                    ItemTable.Clef_Origine = "T"
                                    ItemTable.Rang_SousFamille = "3.3.2"
                                Case 20
                                    ItemTable.Intitule = "Esprit de synthèse"
                                    ItemTable.Clef_Origine = "U"
                                    ItemTable.Rang_SousFamille = "3.3.3"
                                Case 21
                                    ItemTable.Intitule = "Disponibilité et puissance de travail"
                                    ItemTable.Clef_Origine = "V"
                                    ItemTable.Rang_SousFamille = "3.3.4"
                                Case 22
                                    ItemTable.Intitule = "Sens de l'organisation"
                                    ItemTable.Clef_Origine = "W"
                                    ItemTable.Rang_SousFamille = "3.3.5"
                                Case 23
                                    ItemTable.Intitule = "Efficacité et respect des délais"
                                    ItemTable.Clef_Origine = "X"
                                    ItemTable.Rang_SousFamille = "3.3.6"
                            End Select

                        Case 24 To 29
                            ItemTable.Clef_Index = IndiceFam + 17 'Numero de Tri
                            ItemTable.Rang_Famille = "3.4" 'Numero de famille
                            ItemTable.Libelle_Famille = "Aptitude au management et à la conduite de projet"
                            ItemTable.Rang_SousFamille = ""
                            ItemTable.Libelle_SousFamille = ""
                            Select Case IndiceFam
                                Case 24
                                    ItemTable.Intitule = "Aptitude à l'encadrement et à l'animation d'équipe"
                                    ItemTable.Clef_Origine = "Y"
                                    ItemTable.Rang_SousFamille = "3.4.1"
                                Case 25
                                    ItemTable.Intitule = "Capacité à dialoguer et à communiquer avec ses collaborateurs"
                                    ItemTable.Clef_Origine = "Z"
                                    ItemTable.Rang_SousFamille = "3.4.2"
                                Case 26
                                    ItemTable.Intitule = "Capacité de décision et d'exercice des responsabilités"
                                    ItemTable.Clef_Origine = "1"
                                    ItemTable.Rang_SousFamille = "3.4.3"
                                Case 27
                                    ItemTable.Intitule = "Capacité à conduire des projets"
                                    ItemTable.Clef_Origine = "2"
                                    ItemTable.Rang_SousFamille = "3.4.4"
                                Case 28
                                    ItemTable.Intitule = "Délégation, contrôle et suivi des dossiers"
                                    ItemTable.Clef_Origine = "3"
                                    ItemTable.Rang_SousFamille = "3.4.5"
                                Case 29
                                    ItemTable.Intitule = "Aptitude à former des collaborateurs et à valoriser leurs compétences"
                                    ItemTable.Clef_Origine = "4"
                                    ItemTable.Rang_SousFamille = "3.4.6"
                            End Select
                    End Select
                    WsLstCompetences.Add(ItemTable)
                Next IndiceFam
                Return WsLstCompetences
            End Get
        End Property

        Public ReadOnly Property Table_Observations() As List(Of Virtualia.Net.Entretien.ObjetTable) Implements IGenericEntretien.Table_Observations
            Get
                If WsLstObservations IsNot Nothing Then
                    Return WsLstObservations
                End If

                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstObservations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Evènements survenus au cours de la période écoulée"
                ItemTable.Rang_Famille = "1.1.1"
                ItemTable.Clef_Origine = "H"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Evolution ou redéfinition du périmètre du poste"
                ItemTable.Rang_Famille = "1.1.2"
                ItemTable.Clef_Origine = "I"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Bilan d'activité - autres réalisations"
                ItemTable.Rang_Famille = "1.2"
                ItemTable.Clef_Origine = "J"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Objectifs du service"
                ItemTable.Rang_Famille = "2.1"
                ItemTable.Clef_Origine = "K"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Démarche envisagée et moyens à prévoir pour faciliter l'atteinte des objectifs"
                ItemTable.Rang_Famille = "2.2"
                ItemTable.Clef_Origine = "L"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Compétences professionnelles et technicité"
                ItemTable.Rang_Famille = "3.1"
                ItemTable.Clef_Origine = "M"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Qualités personnelles et relationnelles"
                ItemTable.Rang_Famille = "3.2"
                ItemTable.Clef_Origine = "N"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Méthode et résultats"
                ItemTable.Rang_Famille = "3.3"
                ItemTable.Clef_Origine = "O"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Aptitude au management et à la conduite de projet"
                ItemTable.Rang_Famille = "3.4"
                ItemTable.Clef_Origine = "P"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhait d'évolution des activités du poste occupé"
                ItemTable.Rang_Famille = "5.1"
                ItemTable.Clef_Origine = "Q"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhait de mobilité"
                ItemTable.Rang_Famille = "5.2.1"
                ItemTable.Clef_Origine = "R"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhait de stage dans un service"
                ItemTable.Rang_Famille = "5.2.2"
                ItemTable.Clef_Origine = "S"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhait d'évolution de carrière"
                ItemTable.Rang_Famille = "5.3"
                ItemTable.Clef_Origine = "T"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Demandes de formation non satisfaites"
                ItemTable.Rang_Famille = "6.1.2"
                ItemTable.Clef_Origine = "U"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Actions conduites en tant que formateur"
                ItemTable.Rang_Famille = "6.1.3"
                ItemTable.Clef_Origine = "V"
                WsLstObservations.Add(ItemTable)

                Return WsLstObservations

            End Get
        End Property

        Public ReadOnly Property Table_Formations() As List(Of Virtualia.Net.Entretien.ObjetTable) Implements IGenericEntretien.Table_Formations
            Get
                If WsLstFormations IsNot Nothing Then
                    Return WsLstFormations
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstFormations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Formations d'adaptation au poste de travail - 1"
                ItemTable.Rang_Famille = "6.2.1 Formation"
                ItemTable.Clef_Origine = "A"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Formations d'adaptation au poste de travail - 2"
                ItemTable.Rang_Famille = "6.2.1 Formation"
                ItemTable.Clef_Origine = "B"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Formations d'adaptation au poste de travail - 3"
                ItemTable.Rang_Famille = "6.2.1 Formation"
                ItemTable.Clef_Origine = "C"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Formations à l'évolution des métiers - 1"
                ItemTable.Rang_Famille = "6.2.1 Formation"
                ItemTable.Clef_Origine = "D"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Formations à l'évolution des métiers - 2"
                ItemTable.Rang_Famille = "6.2.1 Formation"
                ItemTable.Clef_Origine = "E"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Formations à l'évolution des métiers - 3"
                ItemTable.Rang_Famille = "6.2.1 Formation"
                ItemTable.Clef_Origine = "F"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Formations de developpement ou d'acquisition de qualifications nouvelles - 1"
                ItemTable.Rang_Famille = "6.2.1 Formation"
                ItemTable.Clef_Origine = "G"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Formations de developpement ou d'acquisition de qualifications nouvelles - 2"
                ItemTable.Rang_Famille = "6.2.1 Formation"
                ItemTable.Clef_Origine = "H"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Formations de developpement ou d'acquisition de qualifications nouvelles - 3"
                ItemTable.Rang_Famille = "6.2.1 Formation"
                ItemTable.Clef_Origine = "I"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Préparation aux concours ou examen professionnel - 1"
                ItemTable.Rang_Famille = "6.2.2 Autres besoins"
                ItemTable.Clef_Origine = "J"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Préparation aux concours ou examen professionnel - 2"
                ItemTable.Rang_Famille = "6.2.2 Autres besoins"
                ItemTable.Clef_Origine = "K"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Préparation aux concours ou examen professionnel - 3"
                ItemTable.Rang_Famille = "6.2.2 Autres besoins"
                ItemTable.Clef_Origine = "L"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Validation des acquis de l'expérience"
                ItemTable.Rang_Famille = "6.2.2 Autres besoins"
                ItemTable.Clef_Origine = "M"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Bilan de compétences"
                ItemTable.Rang_Famille = "6.2.2 Autres besoins"
                ItemTable.Clef_Origine = "N"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Période de professionnalisation"
                ItemTable.Rang_Famille = "6.2.2 Autres besoins"
                ItemTable.Clef_Origine = "O"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Congé de formation professionnelle"
                ItemTable.Rang_Famille = "6.2.2 Autres besoins"
                ItemTable.Clef_Origine = "P"
                WsLstFormations.Add(ItemTable)

                For IndiceTri = 0 To WsLstFormations.Count - 1
                    WsLstFormations.Item(IndiceTri).Clef_Index = IndiceTri + 1
                Next IndiceTri

                Return WsLstFormations
            End Get
        End Property

        Public ReadOnly Property Table_Syntheses() As List(Of Virtualia.Net.Entretien.ObjetTable) Implements IGenericEntretien.Table_Syntheses
            Get
                If WsLstSyntheses IsNot Nothing Then
                    Return WsLstSyntheses
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstSyntheses = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Compétences professionnelles et technicité"
                ItemTable.Rang_Famille = "7.2.1"
                ItemTable.Clef_Origine = "A"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Qualités personnelles et relationnelles"
                ItemTable.Rang_Famille = "7.2.2"
                ItemTable.Clef_Origine = "B"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Méthode et résultats"
                ItemTable.Rang_Famille = "7.2.3"
                ItemTable.Clef_Origine = "C"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Aptitude au management et à la conduite de projet"
                ItemTable.Rang_Famille = "7.2.4"
                ItemTable.Clef_Origine = "D"
                WsLstSyntheses.Add(ItemTable)

                For IndiceTri = 0 To WsLstSyntheses.Count - 1
                    WsLstSyntheses.Item(IndiceTri).Clef_Index = IndiceTri + 1
                Next IndiceTri

                Return WsLstSyntheses

            End Get
        End Property

        Public ReadOnly Property Table_Complements As List(Of ObjetTable) Implements IGenericEntretien.Table_Complements
            Get
                Return Nothing
            End Get
        End Property
    End Class
End Namespace