﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_RESULTATS_ONISEP
    
    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreTitreResultats.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitreResultats As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVolet As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelIdentite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelTitreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableEnteteObjectif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableEnteteObjectif As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelEnteteObjectif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteObjectif As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableEnteteDifficultes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableEnteteDifficultes As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelEnteteDifficultes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteDifficultes As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableNumerobjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableNumerobjectif1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelNumerobjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNumerobjectif1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableObjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableObjectif1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVA02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableDifficultes1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDifficultes1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVA03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableNumerobjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableNumerobjectif2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelNumerobjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNumerobjectif2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableObjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableObjectif2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVB02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableDifficultes2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDifficultes2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVB03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableNumerobjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableNumerobjectif3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelNumerobjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNumerobjectif3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableObjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableObjectif3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVC02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableDifficultes3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDifficultes3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVC03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableNumerobjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableNumerobjectif4 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelNumerobjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNumerobjectif4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableObjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableObjectif4 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVD02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVD02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableDifficultes4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDifficultes4 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVD03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVD03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableNumerobjectif5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableNumerobjectif5 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelNumerobjectif5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNumerobjectif5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableObjectif5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableObjectif5 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVE02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVE02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableDifficultes5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDifficultes5 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVE03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVE03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelTitreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoVG03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVG03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
End Class
