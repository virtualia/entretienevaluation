﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_EMPLOI_CNED.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_EMPLOI_CNED" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderColor="#B0E0D7" 
    BorderStyle="None" BorderWidth="2px" HorizontalAlign="Center" 
    style="margin-top: 1px;" Visible="true" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreEmploi" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="1. DESCRIPTION DU POSTE OCCUPE" Height="25px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="40px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero1" runat="server" Height="40px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Intitulé de la fonction"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV06" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="6" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="110px" EtiHeight="30px" DonTabIndex="1" 
                           Etitext="" EtiVisible="false" Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="40px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Positionnement du poste dans la structure"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV05" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="5" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="110px" EtiHeight="30px" DonTabIndex="2"
                           Etitext="" EtiVisible="false" Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align:  left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                       <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="9" V_SiDonneeDico="true"
                           EtiWidth="300px" EtiHeight="30px" DonHeight="26px" DonWidth="50px" DonTabIndex="3" 
                           EtiText="Nombre d'agents encadrés (le cas échéant)"/>
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero3" runat="server" Height="40px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Mission(s) du poste - Activités"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 8px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV07" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="7" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="30px" DonTabIndex="4"
                           Etitext="Activités principales pour chaque mission"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align:  left; text-indent: 5px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
</asp:Table>

