﻿Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class VEntretienCNED
        Implements Virtualia.Net.Entretien.IGenericEntretien
        Private WsLstObservations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstFormations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing

        Public ReadOnly Property LitteralCompetence(Critere As Integer, Optional SiPrecision As Boolean = False) As String Implements IGenericEntretien.LitteralCompetence
            Get
                Return ""
            End Get
        End Property

        Public ReadOnly Property LitteralResultat(Critere As Integer) As String Implements IGenericEntretien.LitteralResultat
            Get
                Select Case Critere
                    Case 0
                        Return "sans objet"
                    Case 1
                        Return "insuffisamment atteint"
                    Case 2
                        Return "partiellement atteint"
                    Case 3
                        Return "atteint"
                    Case 4
                        Return "dépassé"
                    Case Else
                        Return "sans objet"
                End Select
            End Get
        End Property

        Public ReadOnly Property LitteralSynthese(Critere As Integer) As String Implements IGenericEntretien.LitteralSynthese
            Get
                Return ""
            End Get
        End Property

        Public ReadOnly Property Table_Competences As List(Of ObjetTable) Implements IGenericEntretien.Table_Competences
            Get
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Table_Formations As List(Of ObjetTable) Implements IGenericEntretien.Table_Formations
            Get
                If WsLstFormations IsNot Nothing Then
                    Return WsLstFormations
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstFormations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Compétence pour tenir le poste"
                ItemTable.Rang_Famille = "10.2 Compétence à acquérir ou développer"
                ItemTable.Clef_Origine = "A"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Compétence pour tenir le poste"
                ItemTable.Rang_Famille = "10.2 Compétence à acquérir ou développer"
                ItemTable.Clef_Origine = "B"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Compétence pour tenir le poste"
                ItemTable.Rang_Famille = "10.2 Compétence à acquérir ou développer"
                ItemTable.Clef_Origine = "C"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Compétence pour tenir le poste"
                ItemTable.Rang_Famille = "10.2 Compétence à acquérir ou développer"
                ItemTable.Clef_Origine = "D"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Compétence en vue d'une évolution professionnelle"
                ItemTable.Rang_Famille = "10.3 Compétence à acquérir ou développer"
                ItemTable.Clef_Origine = "E"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Compétence en vue d'une évolution professionnelle"
                ItemTable.Rang_Famille = "10.3 Compétence à acquérir ou développer"
                ItemTable.Clef_Origine = "F"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Compétence en vue d'une évolution professionnelle"
                ItemTable.Rang_Famille = "10.3 Compétence à acquérir ou développer"
                ItemTable.Clef_Origine = "G"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Compétence en vue d'une évolution professionnelle"
                ItemTable.Rang_Famille = "10.3 Compétence à acquérir ou développer"
                ItemTable.Clef_Origine = "H"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Autres perspectives de formation"
                ItemTable.Rang_Famille = "10.4 Formation"
                ItemTable.Clef_Origine = "I"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Autres perspectives de formation"
                ItemTable.Rang_Famille = "10.4 Formation"
                ItemTable.Clef_Origine = "J"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Autres perspectives de formation"
                ItemTable.Rang_Famille = "10.4 Formation"
                ItemTable.Clef_Origine = "K"
                WsLstFormations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = ""
                ItemTable.Libelle_Famille = "Autres perspectives de formation"
                ItemTable.Rang_Famille = "10.4 Formation"
                ItemTable.Clef_Origine = "L"
                WsLstFormations.Add(ItemTable)

                For IndiceTri = 0 To WsLstFormations.Count - 1
                    WsLstFormations.Item(IndiceTri).Clef_Index = IndiceTri + 1
                Next IndiceTri

                Return WsLstFormations
            End Get
        End Property

        Public ReadOnly Property Table_Observations As List(Of ObjetTable) Implements IGenericEntretien.Table_Observations
            Get
                If WsLstObservations IsNot Nothing Then
                    Return WsLstObservations
                End If

                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstObservations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Evènements survenus au cours de la période écoulée"
                ItemTable.Rang_Famille = "2.1"
                ItemTable.Clef_Origine = "I"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Réalisations professionnelles particulières"
                ItemTable.Rang_Famille = "2.3"
                ItemTable.Clef_Origine = "J"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Compétences professionnelles et technicité"
                ItemTable.Rang_Famille = "4.1"
                ItemTable.Clef_Origine = "L"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Contribution à l'activité du service"
                ItemTable.Rang_Famille = "4.2"
                ItemTable.Clef_Origine = "M"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Qualités personnelles et relationnelles"
                ItemTable.Rang_Famille = "4.3"
                ItemTable.Clef_Origine = "N"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Aptitude au management et/ou à la conduite de projet"
                ItemTable.Rang_Famille = "4.4"
                ItemTable.Clef_Origine = "O"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Appréciation générale exprimant la valeur professionnelle de l'agent"
                ItemTable.Rang_Famille = "4.5"
                ItemTable.Clef_Origine = "P"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Evolution des activités"
                ItemTable.Rang_Famille = "3.2"
                ItemTable.Clef_Origine = "Q"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Mobilité - métier - structure"
                ItemTable.Rang_Famille = "3.3"
                ItemTable.Clef_Origine = "R"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Promotion"
                ItemTable.Rang_Famille = "3.4"
                ItemTable.Clef_Origine = "S"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations sur les perspectives d'évolution"
                ItemTable.Rang_Famille = "3.5"
                ItemTable.Clef_Origine = "T"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l'agent sur la conduite de l'entretien"
                ItemTable.Rang_Famille = "6.1"
                ItemTable.Clef_Origine = "U"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Mobilité - métier - structure"
                ItemTable.Rang_Famille = "6.2"
                ItemTable.Clef_Origine = "V"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaire sur les formations suivies"
                ItemTable.Rang_Famille = "10.1"
                ItemTable.Clef_Origine = "W"
                WsLstObservations.Add(ItemTable)

                Return WsLstObservations
            End Get
        End Property

        Public ReadOnly Property Table_Syntheses As List(Of ObjetTable) Implements IGenericEntretien.Table_Syntheses
            Get
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Table_Complements As List(Of ObjetTable) Implements IGenericEntretien.Table_Complements
            Get
                Return Nothing
            End Get
        End Property
    End Class
End Namespace