﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_FORMATION_CNED

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreTitreFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitreFormation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVolet As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelIdentite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreNumero11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle ListeFormationsSuivies.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeFormationsSuivies As Global.Virtualia.Net.VirtualiaControle_VListeGrid

    '''<summary>
    '''Contrôle LabelTitreNumero12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVW03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVW03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableBesoin102.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableBesoin102 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelEnteteStage102.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteStage102 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEntetePeriode102.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEntetePeriode102 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVA01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioHA08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHA08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle InfoVB01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioHB08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHB08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle InfoVC01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioHC08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHC08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle InfoVD01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVD01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioHD08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHD08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle TableBesoin103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableBesoin103 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTypeBesoin103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTypeBesoin103 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTypeBesoin103Bis.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTypeBesoin103Bis As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteStage103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteStage103 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEntetePeriode103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEntetePeriode103 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVE01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVE01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioHE08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHE08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle InfoVF01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVF01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioHF08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHF08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle InfoVG01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVG01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioHG08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHG08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle InfoVH01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVH01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioHH08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHH08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle TableBesoin104.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableBesoin104 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTypeBesoin104.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTypeBesoin104 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteStage104.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteStage104 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEntetePeriode104.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEntetePeriode104 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVI01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVI01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioHI08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHI08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle InfoVJ01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVJ01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioHJ08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHJ08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle InfoVK01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVK01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioHK08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHK08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle InfoVL01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVL01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioHL08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHL08 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio

    '''<summary>
    '''Contrôle TableDIF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDIF As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitre105.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitre105 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreDIF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreDIF As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelSoldeDIF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSoldeDIF As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelSiUtilisationDIF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSiUtilisationDIF As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle RadioHM19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHM19 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
End Class
