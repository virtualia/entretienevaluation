﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_FORMATION_CNED.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_FORMATION_CNED" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreFormation" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="10. BILAN DES FORMATIONS ET BESOINS DE FORMATION" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="40px" Width="746px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="10.1 Bilan des formations suivies sur les quatre dernières années"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="ListeFormationsSuivies" runat="server" CadreWidth="746px" SiColonneSelect="false"/>
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="50px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="30px" Width="746px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Commentaire sur les formations suivies et leur mise en oeuvre dans le poste"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVW03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="746px" DonHeight="190px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="false"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="40px" Width="746px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3"  BorderStyle="NotSet" Text="10.2 Compétence à acquérir ou développer pour tenir le poste"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin102" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage102" runat="server" Height="25px" Width="390px"
                               BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                               Text=""
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode102" runat="server" Height="25px" Width="350px"
                               BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Echéances envisagées"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="2" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHA08" runat="server" V_Groupe="EcheanceA"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHB08" runat="server" V_Groupe="EcheanceB"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>               
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHC08" runat="server" V_Groupe="EcheanceC"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                           
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHD08" runat="server" V_Groupe="EcheanceD"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin103" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="LabelTypeBesoin103" runat="server" Height="40px" Width="746px"
                               BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="10.3 Compétences à acquérir ou développer en vue d'une évolution professionnelle"
                               BorderWidth="1px" ForeColor="White" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 2px; text-align:  left;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                          <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="LabelTypeBesoin103Bis" runat="server" Height="20px" Width="746px"
                               BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(à compléter en fonction des perspectives d'évolution professionnelle)"
                               BorderWidth="1px" ForeColor="#124545" 
                               Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic="False" Font-Bold="False" 
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: oblique; text-indent: 3px; text-align:  left;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage103" runat="server" Height="25px" Width="390px"
                               BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                               Text=""
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode103" runat="server" Height="25px" Width="350px"
                               BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Echéances envisagées"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="6" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHE08" runat="server" V_Groupe="EcheanceE"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="7" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHF08" runat="server" V_Groupe="EcheanceF"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>               
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="8" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHG08" runat="server" V_Groupe="EcheanceG"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                           
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="9" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHH08" runat="server" V_Groupe="EcheanceH"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin104" runat="server" CellPadding="0" CellSpacing="0" Width="746px"
                      BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="LabelTypeBesoin104" runat="server" Height="40px" Width="746px"
                               BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="10.4 Autres perspectives de formation"
                               BorderWidth="1px" ForeColor="White" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 2px; text-align:  left;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage104" runat="server" Height="25px" Width="390px"
                               BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                               Text=""
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode104" runat="server" Height="25px" Width="350px"
                               BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Echéances envisagées"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>                       
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="10" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHI08" runat="server" V_Groupe="EcheanceI"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="11" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHJ08" runat="server" V_Groupe="EcheanceJ"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>               
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="12" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHK08" runat="server" V_Groupe="EcheanceK"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                           
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL01" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="385px" DonHeight="56px" DonTabIndex="13" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                              <Virtualia:VSixBoutonRadio ID="RadioHL08" runat="server" V_Groupe="EcheanceL"
                                V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                                VRadioN1Width="78px" VRadioN2Width="85px" VRadioN3Width="85px"
                                VRadioN4Width="85px" VRadioN5Width="0px" VRadioN6Width="0px"
                                VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                VRadioN1Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN2Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN3Style="margin-left: 0px; margin-top: 0px; Font-size: 85%"
                                VRadioN4Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN5Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" VRadioN6Style="margin-left: 0px; margin-top: 0px; Font-size: 85%" 
                                VRadioN1Text="1er trim." VRadioN2Text="2ème trim." VRadioN3Text="3ème trim."
                                VRadioN4Text="4ème trim." VRadioN5Text="" VRadioN6Text=""
                                VRadioN5Visible="false" VRadioN6Visible="false"   
                                Visible="true"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="TableDIF" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitre105" runat="server" Height="40px" Width="746px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3"  BorderStyle="NotSet" Text="10.5 Utilisation du droit individuel à la formation (DIF)"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 10px;
                           font-style: normal; text-indent: 2px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelTitreDIF" runat="server" Height="30px" Width="340px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Solde du DIF au 1er janvier de l'année en cours :"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelSoldeDIF" runat="server" Height="30px" Width="400px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="#8DA8A3" BorderWidth="1px" 
                            Text=""
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Label ID="LabelSiUtilisationDIF" runat="server" Height="25px" Width="340px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" 
                            Text="L'agent envisage-t-il de mobiliser son DIF cette année ?"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                       <Virtualia:VSixBoutonRadio ID="RadioHM19" runat="server" V_Groupe="SiUtilisationDIF"
                           V_PointdeVue="1" V_Objet="150" V_Information="19" V_SiDonneeDico="true"
                           VRadioN1Width="80px" VRadioN2Width="80px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                           VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px" VRadioN2Style="margin-left: 0px; margin-top: 0px" VRadioN3Style="margin-left: 0px; margin-top: 0px"
                           VRadioN4Style="margin-left: 0px; margin-top: 0px" VRadioN5Style="margin-left: 0px; margin-top: 0px" VRadioN6Style="margin-left: 0px; margin-top: 0px" 
                           VRadioN1Text="Non" VRadioN2Text="Oui" 
                           VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell> 
                </asp:TableRow>         
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>