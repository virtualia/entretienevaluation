﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE_CESE
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1), "CESE")
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            Select Case NumInfo
                Case 1
                    Select Case Strings.Mid(Ctl.ID, 6, 1)
                        Case "A", "B", "C"
                            VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
                        Case Else
                            VirVertical.V_SiEnLectureSeule = True
                    End Select
                Case Else
                    VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            End Select
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VSixBoutonRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VSixBoutonRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 7, 1), "CESE")
            VirRadio.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.V_SiAutoPostBack = Not (WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1)))
            If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                Select Case ValeurLue(NumObjet, NumInfo, Rang)
                    Case Is = "4"
                        VirRadio.VRadioN1Check = True
                    Case Is = "3"
                        VirRadio.VRadioN2Check = True
                    Case Is = "2"
                        VirRadio.VRadioN3Check = True
                    Case Is = "1"
                        VirRadio.VRadioN4Check = True
                    Case Else
                        VirRadio.VRadioN4Check = True
                End Select
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) _
    Handles InfoVA01.ValeurChange, InfoVA11.ValeurChange, InfoVB01.ValeurChange, InfoVB11.ValeurChange, _
            InfoVC01.ValeurChange, InfoVC11.ValeurChange, InfoVH01.ValeurChange, InfoVH11.ValeurChange, _
            InfoVI01.ValeurChange, InfoVI11.ValeurChange, InfoVJ01.ValeurChange, InfoVJ11.ValeurChange, _
            InfoVK01.ValeurChange, InfoVK11.ValeurChange, InfoVL01.ValeurChange, InfoVL11.ValeurChange, _
            InfoVM01.ValeurChange, InfoVM11.ValeurChange, InfoVN01.ValeurChange, InfoVN11.ValeurChange, _
            InfoVO01.ValeurChange, InfoVO11.ValeurChange, InfoVP01.ValeurChange, InfoVP11.ValeurChange, _
            InfoVQ01.ValeurChange, InfoVQ11.ValeurChange, InfoVU01.ValeurChange, InfoVU11.ValeurChange, _
            InfoVV01.ValeurChange, InfoVV11.ValeurChange, InfoVW01.ValeurChange, InfoVW11.ValeurChange, _
            InfoVX01.ValeurChange, InfoVX11.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1), "CESE")

        Select Case NumObjet
            Case 153
                If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub RadioValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) _
        Handles RadioHA07.ValeurChange, RadioHB07.ValeurChange, RadioHC07.ValeurChange, _
                RadioHH07.ValeurChange, RadioHI07.ValeurChange, RadioHJ07.ValeurChange, _
                RadioHK07.ValeurChange, RadioHL07.ValeurChange, RadioHM07.ValeurChange, _
                RadioHN07.ValeurChange, RadioHO07.ValeurChange, RadioHP07.ValeurChange, _
                RadioHQ07.ValeurChange, RadioHU07.ValeurChange, RadioHV07.ValeurChange, _
                RadioHW07.ValeurChange, RadioHX07.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VSixBoutonRadio).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VSixBoutonRadio).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VSixBoutonRadio).ID, 7, 1), "CESE")
        Dim ValeurRadio As String = "0"

        If IsNumeric(e.Valeur) Then
            ValeurRadio = CStr(4 - CInt(e.Valeur))
        ElseIf IsNumeric(e.Parametre) Then
            ValeurRadio = CStr(4 - CInt(e.Parametre))
        End If

        Select Case NumObjet
            Case 153
                If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> ValeurRadio Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = ValeurRadio
                    End If
                End If
        End Select
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        Etiquette.Text = "COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL - Année " & WsDossierPER.Annee
        LabelIdentite.Text = WsDossierPER.LibelleIdentite

        Call LireLaFiche()
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 153
                    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_153(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property

End Class