﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_COMPETENCE_AFB.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE_AFB" %>

<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VSixBoutonRadio.ascx" TagName="VSixBoutonRadio" TagPrefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreCompetence" runat="server" Height="95px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="APPRECIATION DES COMPETENCES DE L'AGENT" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformationsNiveaux" runat="server" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="none"
                            Text="Niveaux"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Font-Bold="False"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: italic; text-indent: 5px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformationsNiveau1" runat="server" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="none"
                            Text="<B>Initié: </B>  <small> Connaissance élémentaires, notions. Capacité à faire mais en étant tutoré. </small>"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Font-Bold="False"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformationsNiveau2" runat="server" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="none"
                            Text="<B>Pratique : </B>  <small> Connaissances générales - Capacité à traiter de façon autonome les situations courantes.</small>"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Font-Bold="False"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformationsNiveau3" runat="server" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="none"
                            Text="<B>Maîtrise : </B>  <small> Connaissances approfondies - Capacité à traiter de façon autonome les situations complexes ou inhabituelles.</small>"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Font-Bold="False"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">

                        <asp:Table ID="Table1" runat="server" Height="30px" CellPadding="0" Width="750px"
                            CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">

                                    <asp:Label ID="II_LabelInformationsNiveau4" runat="server" Height="40px" Width="100px"
                                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                                        Text="<B>Expert : </B>"
                                        BorderWidth="1px" ForeColor="#124545"
                                        Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Font-Bold="False"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: italic; text-indent: 50px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="Label1" runat="server" Height="40px" Width="646px"
                                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                                        Text="<small> Au sens de 'Fait référence dans le domaine'. 'Est capable de le faire évoluer', 'Capacité à former et/ou à être tuteur' : ne renvoie pas aux certifications attribuées par les ministère dans l'exercice de certaines fonctions spécifiques, notammment par les commités de domaine du Ministère.</small>"
                                        BorderWidth="1px" ForeColor="#124545"
                                        Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Font-Bold="False"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: italic; text-indent: 15px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformationsNiveau5" runat="server" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                            Text="<B>Sans objet</B>"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Font-Bold="False"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille1" runat="server" Height="30px" Width="750px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Compétences professionnelles"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences311" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelTitreCompetence1" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="CadreEnteteLabels1" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote11" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote12" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote13" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote14" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote15" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHA01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="Connaissances du poste" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHA07" runat="server" V_Groupe="Critere111"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="TableEnteteCommentaireComptence1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="LabelCommentaireComptence1" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="TableCommentaireComptence1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label2" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label3" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label4" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label5" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label6" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label83" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHB01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="2"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="Connaissances de l'environnement professionnel" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHB07" runat="server" V_Groupe="Critere112"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table8" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label38" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table9" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label8" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table3" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label9" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label10" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label11" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label12" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label81" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHC01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="3"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="Qualités rédactionnelles" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHC07" runat="server" V_Groupe="Critere113"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table10" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label39" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table11" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label14" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table4" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label15" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label16" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label17" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label18" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label84" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHD01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="4"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="Qualités relationnelles" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHD07" runat="server" V_Groupe="Critere114"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table12" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label40" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table13" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label20" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table5" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label21" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label22" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label23" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label24" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label85" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHE01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="5"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="Qualités d'expression orale" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHE07" runat="server" V_Groupe="Critere115"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table14" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label41" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table15" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label26" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table6" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label27" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label28" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label29" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label30" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label86" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHF01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="6"
                                        EtiVisible="False" DonWidth="410px" DonHeight="20px"
                                        DonText="Capacité d'adaptation aux évolutions techniques et professionnelles" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHF07" runat="server" V_Groupe="Critere116"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table16" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label42" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table17" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label32" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table7" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label33" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label34" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label35" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label36" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:tablecell horizontalalign="center">
                                                <asp:label id="label37" runat="server" height="20px" width="76px"
                                                    backcolor="white" bordercolor="#9eb0ac" borderstyle="notset" text="sans objet"
                                                    borderwidth="1px" forecolor="#124545" font-italic="false"
                                                    font-bold="false" font-names="trebuchet ms" font-size="small"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:label>
                                            </asp:tablecell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHG01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="6"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="Capacité à assurer le suivi des dossiers" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHG07" runat="server" V_Groupe="Critere116"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table18" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label43" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table19" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>







                                                        <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label7" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table34" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label13" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label19" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label25" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label31" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label87" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHM01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="10"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="" V_SiEnLectureSeule="False"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHM07" runat="server" V_Groupe="Critere120"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table35" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label96" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table36" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
							                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label88" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table37" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label89" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label90" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label91" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label73" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label92" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHN01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="10"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                      DonText="" V_SiEnLectureSeule="False"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHN07" runat="server" V_Groupe="Critere120"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table38" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label74" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table39" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
							                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label75" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table40" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label76" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label77" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label78" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label79" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label93" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHO01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="10"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="" V_SiEnLectureSeule="False"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHO07" runat="server" V_Groupe="Critere120"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table41" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label82" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table42" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>











                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille2" runat="server" Height="30px" Width="750px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Agents en situation de management"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences321" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelTitreCompetence2" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote21" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote22" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote23" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote24" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelTitreNote25" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHH01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="7"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="Capacité à déléguer" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHH07" runat="server" V_Groupe="Critere117"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table24" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label68" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table25" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label44" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table20" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label45" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label46" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label47" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label48" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label49" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHI01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="8"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="Capacité à assurer le suivi des  dossiers" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHI07" runat="server" V_Groupe="Critere118"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table26" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label69" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table27" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label50" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table21" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label51" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label52" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label53" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label54" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label55" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHJ01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="8"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="Aptitude à former des collaborateurs" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHJ07" runat="server" V_Groupe="Critere118"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table28" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label70" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table29" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label56" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table22" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label57" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label58" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label59" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label60" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label61" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHK01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="9"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="Aptitude à la prise de décision" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHK07" runat="server" V_Groupe="Critere119"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table30" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label71" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table31" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label62" runat="server" Height="20px" Width="361px"
                                        BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="Compétence"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table23" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label63" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Initié"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label64" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Pratique "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label65" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Maîtrise "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label66" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Expert "
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="Label95" runat="server" Height="20px" Width="76px"
                                                    BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="Sans objet"
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHL01" runat="server"
                                        V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="10"
                                        EtiVisible="False" DonWidth="350px" DonHeight="20px"
                                        DonText="Sens de l'organisation d'une équipe" V_SiEnLectureSeule="true"
                                        DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center"
                                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                    <Virtualia:VSixBoutonRadio ID="RadioHL07" runat="server" V_Groupe="Critere120"
                                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                        VRadioN1Width="71px" VRadioN2Width="71px" VRadioN3Width="72px"
                                        VRadioN4Width="72px" VRadioN5Width="72px" VRadioN6Width="72px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                         VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table32" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label72" runat="server" Height="18px" Width="748px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Commentaire"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: left;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <asp:Table ID="Table33" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL11" runat="server" DonTextMode="true"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="true"
                                                    EtiVisible="false" DonWidth="744px" DonHeight="40px" DonTabIndex="24"
                                                    DonStyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>

                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
