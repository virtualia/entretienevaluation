﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_FORMATION_AFB
    
    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreTitreFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitreFormation As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVolet As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelIdentite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEntete As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelRappel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelRappel As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle RappelL1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RappelL1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle RappelL2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RappelL2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle RappelL3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RappelL3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelPortail.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPortail As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LienPortail.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LienPortail As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''Contrôle AnoterL1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents AnoterL1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle AnoterL2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents AnoterL2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle MailFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MailFormation As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''Contrôle AnoterL3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents AnoterL3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle MailFormationTech.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MailFormationTech As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''Contrôle CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelTitreNumero11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelTitreNumero12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoVFA03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFA03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFB03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFB03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFC03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFC03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFD03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFD03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFE03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFE03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFF03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFF03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFG03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFG03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFH03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFH03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle LabelTitreNumero14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero14 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoVFI03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFI03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle LabelTitreNumero15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero15 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoVFJ03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFJ03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle Label1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoVFK03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFK03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelTitreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelTitreNumero21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero21 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelLegendeT11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeT11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelLegendeT12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeT12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelLegendeDIF11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeDIF11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelLegendeDIF12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeDIF12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelLegendeT51.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeT51 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelLegendeT52.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeT52 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Table1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Table1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Label2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Label2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Label4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Label4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoVA01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVA03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVB01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVB03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVC01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVC03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVD01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVD01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVD03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVD03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVE01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVE01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVE03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVE03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVF01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVF01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVF03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVF03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle Table2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Table2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Label5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Label5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Label6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Label6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoVG01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVG01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVG03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVG03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVH01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVH01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVH03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVH03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle CadreNumero5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero5 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelObservationsEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelObservationsEvaluateur As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoVFL03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFL03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFM03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFN03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFN03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFO03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFO03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFP03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFP03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFQ03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFQ03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFR03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFR03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVFS03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVFS03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
End Class
