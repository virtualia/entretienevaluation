﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_APPRECIATION_AFB
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    ''**** AKR LOG
    Private Const SysFicLog As String = "WebSelfSalarie.log"
    Private SysCodeIso As System.Text.Encoding = System.Text.Encoding.UTF8

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            ' Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            If Ctl.ID.Length > 8 Then
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 2))
                VirControle.DonText = ValeurLue(NumObjet, NumInfo, Rang)
                VirControle.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 2))
            Else
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
                VirControle.DonText = ValeurLue(NumObjet, NumInfo, Rang)
                VirControle.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            End If

            If NumInfo = 2 Then 'Service évaluateur
                VirControle.V_SiEnLectureSeule = True
            End If
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirControleVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControleVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirControleVertical.V_Objet
            ' Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            If Ctl.ID.Length > 8 Then
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 2))
                VirControleVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
                VirControleVertical.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 2))
            Else
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
                VirControleVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
                VirControleVertical.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            End If

            If NumInfo = 2 Then 'Service évaluateur
                VirControleVertical.V_SiEnLectureSeule = True
            End If
            VirControleVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        InfoHF04.DonText = WsDossierPER.QualitePrenomNomManagerN2
        InfoHG04.DonText = WsDossierPER.FonctionManagerN2
        InfoH0399.V_SiEnLectureSeule = True

        '' Etape de validation
        If InfoH04.DonText = "" Then
            If InfoH23.DonText = "" Then
                If InfoHB04.DonText = "" Then
                    If InfoH03.DonText = "" Then
                        Cadre_Validation_Manager.BackColor = V_WebFonction.ConvertCouleur("#92D6C8")
                        Cadre_Observations_Agent.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
                        Cadre_Validation_Autorite.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
                        Cadre_Validation_Agent.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
                    Else
                        Cadre_Validation_Manager.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
                        Cadre_Observations_Agent.BackColor = V_WebFonction.ConvertCouleur("#92D6C8")
                        Cadre_Validation_Autorite.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
                        Cadre_Validation_Agent.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
                    End If
                Else
                    Cadre_Validation_Manager.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
                    Cadre_Observations_Agent.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
                    Cadre_Validation_Autorite.BackColor = V_WebFonction.ConvertCouleur("#92D6C8")
                    Cadre_Validation_Agent.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
                End If
            Else
                Cadre_Validation_Manager.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
                Cadre_Observations_Agent.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
                Cadre_Validation_Autorite.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
                Cadre_Validation_Agent.BackColor = V_WebFonction.ConvertCouleur("#92D6C8")
            End If
        Else
            Cadre_Validation_Manager.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
            Cadre_Observations_Agent.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
            Cadre_Validation_Autorite.BackColor = V_WebFonction.ConvertCouleur("#CAEBE4")
            Cadre_Validation_Agent.BackColor = V_WebFonction.ConvertCouleur("#92D6C8")
        End If
        Call EcrireLog("AKR Chargement page APPRECIATION OK")
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoVBA03.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet
        Dim Rang As String
        'Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1))

        If CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID.Length > 8 Then
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 2))
        Else
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1))
        End If

        Select Case NumObjet
            Case 150
                If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
                    WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
                End If
            Case 154
                If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_154(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles _
    InfoHA04.ValeurChange, InfoHB04.ValeurChange, InfoHC04.ValeurChange,
    InfoH03.ValeurChange, InfoH04.ValeurChange,
    InfoHBB03.ValeurChange, InfoHBC03.ValeurChange,
    InfoH23.ValeurChange, InfoHI04.ValeurChange, InfoHJ04.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleEtiDonnee).V_Objet
        Dim Rang As String
        ' Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 6, 1))

        If CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID.Length > 8 Then
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 6, 2))
        Else
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 6, 1))
        End If

        Select Case NumObjet
            Case 150
                If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
                    CType(sender, VirtualiaControle_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
                End If
            Case 154
                If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_154(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
            Case 157
                If WsDossierPER.Objet_157(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_157(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        Etiquette.Text = "COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL - Année " & WsDossierPER.Annee
        LabelIdentite.Text = WsDossierPER.LibelleIdentite
        Call LireLaFiche()
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop


        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    If NoInfo = 99 Then
                        NoInfo = 3
                    End If
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 157
                    If WsDossierPER.Objet_157(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_157(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property

    '*** AKR LOG
    Private Shared ReadOnly LogSync As New Object()
    Private Sub EcrireLog(ByVal Msg As String)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim NomLog As String
        SyncLock LogSync
            NomLog = VI.DossierVirtualiaService("Logs") & SysFicLog
            FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
            FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
            Try
                FicWriter.WriteLine(Format(System.DateTime.Now, "g") & Space(1) & Msg)
                FicWriter.Flush()
            Finally
                FicWriter.Close()
            End Try
        End SyncLock
    End Sub

    ''*** FIN AKR LOG
End Class