﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_APPRECIATION_AFB.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_APPRECIATION_AFB" %>

<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreAppreciation" runat="server" Height="95px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="APPRECIATION GLOBALE" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreAppreciationGlobale" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreAppreciation" runat="server" Height="35px" Width="748px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Appréciation générale du supérieur hiérarchique"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVBA03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="220px" EtiHeight="20px" DonTabIndex="56"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>



    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table1" runat="server" Height="30px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label1" runat="server" Text="Circuit de signature <small> (en 4 étapes chronologiques) </small> </BR> </BR> " Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Cadre_Validation_Manager" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="#CAEBE4" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow >
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label6" runat="server" Height="30px" Width="730px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Etape 1 - Signature du responsable hiérarchique direct ayant conduit l'entretien (N+1)"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                            V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="80px" DonTabIndex="6" EtiText="Date "
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                            V_PointdeVue="1" V_Objet="150" V_Information="1" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="350px" DonTabIndex="6" EtiText="Nom, Prénom du résponsable hiérarchique direct (N+1) "
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHA04" runat="server"
                            V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="350px" DonTabIndex="6" EtiText="Signature"
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Bottom">
                        <asp:Label ID="Label11" runat="server" Height="40px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="</BR> Après avoir signé le compte rendu d'entretien, le supérieur hiérarchique direct (N+1) le transmet sans délai à l'agent. </BR>"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="left" ColumnSpan="2">
            <asp:Table ID="Table5" runat="server" Height="30px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left" Width="350px">
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="left">
                        <asp:Table ID="CadreLogoAFB" runat="server" BackImageUrl="~/Images/General/arrow_down.png" Width="50px"
                            BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell Height="50px" BackColor="Transparent" Width="50px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Cadre_Observations_Agent" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="#CAEBE4" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow >
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label2" runat="server" Height="30px" Width="730px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Etape 2 - Observations de l'agent"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label22" runat="server" Height="50px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="A compter de la date de communication du compte rendu d'entretien à l'agent, celu-ci dispose d'un délai de 10 jours ouvrés pour formuler, le cas échéant, des observations. Les observations portées par l'agent n'ont pas valeur de recours. Tout recours doit être rédiger sur un document distinct."
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH0399" runat="server"
                            V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true" V_SiEnLectureSeule ="true"
                            EtiWidth="380px" DonWidth="80px" DonTabIndex="6" EtiText="Date de remise du compte-rendu à l'agent"
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHBB03" runat="server"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="350px" EtiHeight="50px" DonHeight="50px" DonTabIndex="6" EtiText="Observations éventuelles de l'agent "
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHB04" runat="server"
                            V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="80px" DonTabIndex="6" EtiText="Date "
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHC04" runat="server"
                            V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="350px" DonTabIndex="6" EtiText="Visa de l'agent "
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow >
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label8" runat="server" Height="50px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="</BR>Après avoir visé le compte rendu d'entretien, l'agent le rend au supérieur hiérarchique direct (N+1) qui le communique à l'autorité hiérarchique (N+2) </BR>"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="left" ColumnSpan="2">
            <asp:Table ID="Table7" runat="server" Height="30px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left" Width="350px">
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="left">
                        <asp:Table ID="Table8" runat="server" BackImageUrl="~/Images/General/arrow_down.png" Width="50px"
                            BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell Height="50px" BackColor="Transparent" Width="50px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Cadre_Validation_Autorite" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="#CAEBE4" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow >
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label12" runat="server" Height="30px" Width="730px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Etape 3 - Visa de l'autorité hiérarchique (N+2)"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHF04" runat="server"
                            V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true" V_SiEnLectureSeule ="true"
                            EtiWidth="380px" DonWidth="350px" DonTabIndex="6" EtiText="Nom, Prénom de l'autorité hiérarchique (N+2) "
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHG04" runat="server"
                            V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="350px" DonTabIndex="6" EtiText="Fonction exercée "
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>

                <%--                <asp:TableRow BackColor="#CAEBE4">
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee6" runat="server"
                            V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="350px" EtiHeight="50px" DonHeight="50px" DonTabIndex="6" EtiText="Observations éventuelles"
                            DonTooltip="" DonVisible="true" EtiStyle="margin-top: 0px;" />
                    </asp:TableCell>
                    <asp:TableCell C HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VCoupleVerticalEtiDonnee" runat="server"
                            V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="350px" EtiHeight="50px" DonHeight="50px" DonTabIndex="6" EtiText="test"
                            DonTooltip="" EtiVisible="true" DonStyle="margin-top: 20px; marigin-bottom : -10;"  />
                    </asp:TableCell>
                </asp:TableRow>--%>

                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHBC03" runat="server"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="350px" EtiHeight="50px" DonHeight="50px" DonTabIndex="6" EtiText="Observations éventuelles"
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>

                               <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH23" runat="server"
                            V_PointdeVue="1" V_Objet="150" V_Information="23" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="80px" DonTabIndex="6" EtiText="Date "
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="right">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHI04" runat="server" 
                            V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="350px" DonTabIndex="6" EtiText="Visa de l'autorité hiérarchique (N+2)  "
                            DonTooltip=""  />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label16" runat="server" Height="50px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="</BR>Après avoir visé le compte rendu d'entretien, l'autorité hiérarchique (N+2) redonne le compte rendu d'entretien au supérieur hiérarchique direct (N+1) qui le transmet à l'agent pour notification. </BR>"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="left" ColumnSpan="2">
            <asp:Table ID="Table9" runat="server" Height="30px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left" Width="350px">
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="left">
                        <asp:Table ID="Table10" runat="server" BackImageUrl="~/Images/General/arrow_down.png" Width="50px"
                            BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell Height="50px" BackColor="Transparent" Width="50px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Cadre_Validation_Agent" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="#CAEBE4" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow >
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label17" runat="server" Height="30px" Width="730px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Etape 4 - Notification à l'agent"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label25" runat="server" Height="50px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="La signature de l'agent atteste qu'il a pris connaissance du document et ne vaut pas nécessairement approbation de son contenu. le refus de notification par l'agent ne constitue pas une procédure de contestation"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                            V_PointdeVue="1" V_Objet="150" V_Information="4" V_SiDonneeDico="true"
                            EtiWidth="380px" DonWidth="80px" DonTabIndex="6" EtiText="Date  "
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHJ04" runat="server"
                            V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true" V_SiEnLectureSeule ="true"
                            EtiWidth="380px" DonWidth="350px" DonTabIndex="6" EtiText="Nom, Prénom et signature de l'agent "
                            DonTooltip="" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow >
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label21" runat="server" Height="50px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="</BR>Après avoir signé le compte rendu d'entretien, l'agent le remet à sa hiérarchie qui le transmet au Département des ressources humaines </BR>"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>
</asp:Table>
