﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_FORMATION_AFB
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien
    Private WsLienPortail As String = System.Configuration.ConfigurationManager.AppSettings("LienPortail")
    Private WsMailFormation As String = System.Configuration.ConfigurationManager.AppSettings("MailFormation")
    Private WsMailFormationTechnique As String = System.Configuration.ConfigurationManager.AppSettings("MailFormationTechnique")
    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
            End If
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirControle.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirControle.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            If NumInfo = 2 Then 'Service évaluateur
                VirControle.V_SiEnLectureSeule = True
            End If
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            ' Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))

            If Ctl.ID.Length > 8 Then
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 2))
            Else
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            End If

            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VTrioVerticalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VTrioVerticalRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.V_SiAutoPostBack = Not (WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1)))
            Select Case NumInfo
                Case 2
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "T1"
                            VirRadio.RadioGaucheCheck = True
                        Case Is = "T2"
                            VirRadio.RadioCentreCheck = True
                        Case Is = "T3"
                            VirRadio.RadioDroiteCheck = True
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
                Case 8
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "P1"
                            VirRadio.RadioGaucheCheck = True
                        Case Is = "P2"
                            VirRadio.RadioCentreCheck = True
                        Case Is = "P3"
                            VirRadio.RadioDroiteCheck = True
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
                Case 12
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "Sous DIF - TS"
                            VirRadio.RadioGaucheCheck = True
                        Case Is = "Sous DIF - HTS"
                            VirRadio.RadioCentreCheck = True
                        Case Is = "Hors DIF"
                            VirRadio.RadioDroiteCheck = True
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
            End Select
            IndiceI += 1
        Loop
    End Sub

    'Private Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoHI11.ValeurChange
    '    'InfoH20.ValeurChange, InfoH21.ValeurChange,
    '    'InfoHA01.ValeurChange, InfoHA11.ValeurChange,
    '    'InfoHB01.ValeurChange, InfoHB11.ValeurChange,
    '    'InfoHC01.ValeurChange, InfoHC11.ValeurChange,
    '    'InfoHD01.ValeurChange, InfoHD11.ValeurChange,
    '    'InfoHE01.ValeurChange, InfoHE11.ValeurChange,
    '    'InfoHF01.ValeurChange, InfoHF11.ValeurChange,
    '    'InfoHG01.ValeurChange, InfoHG11.ValeurChange,
    '    'InfoHH01.ValeurChange, InfoHH11.ValeurChange,
    '    'InfoHI01.ValeurChange, InfoHI11.ValeurChange,
    '    'InfoHJ01.ValeurChange, InfoHJ11.ValeurChange,
    '    'InfoHK01.ValeurChange, InfoHK11.ValeurChange,
    '    'InfoHL01.ValeurChange, InfoHL11.ValeurChange
    '    'InfoH01.ValeurChange,

    '    WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
    '    If WsDossierPER Is Nothing Then
    '        Exit Sub
    '    End If
    '    Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 2))
    '    Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleEtiDonnee).V_Objet
    '    Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 6, 1))
    '    Select Case NumObjet
    '        Case 150
    '            If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
    '                CType(sender, VirtualiaControle_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
    '                WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
    '            End If
    '        Case 155
    '            If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
    '                If WsDossierPER.Objet_155(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
    '                    CType(sender, VirtualiaControle_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
    '                    WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
    '                End If
    '            End If
    '    End Select

    'End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles _
    InfoVFA03.ValeurChange, InfoVFB03.ValeurChange,
    InfoVFC03.ValeurChange, InfoVFD03.ValeurChange,
    InfoVFE03.ValeurChange, InfoVFF03.ValeurChange,
    InfoVFG03.ValeurChange, InfoVFH03.ValeurChange,
    InfoVFI03.ValeurChange, InfoVFJ03.ValeurChange,
    InfoVFK03.ValeurChange, InfoVFL03.ValeurChange,
    InfoVFM03.ValeurChange, InfoVFN03.ValeurChange,
    InfoVFO03.ValeurChange, InfoVFP03.ValeurChange,
    InfoVFQ03.ValeurChange, InfoVFR03.ValeurChange, InfoVFS03.ValeurChange,
    InfoVA01.ValeurChange, InfoVA03.ValeurChange,
    InfoVB01.ValeurChange, InfoVB03.ValeurChange,
    InfoVC01.ValeurChange, InfoVC03.ValeurChange,
    InfoVD01.ValeurChange, InfoVD03.ValeurChange,
    InfoVE01.ValeurChange, InfoVE03.ValeurChange,
    InfoVF01.ValeurChange, InfoVF03.ValeurChange,
    InfoVG01.ValeurChange, InfoVG03.ValeurChange,
    InfoVH01.ValeurChange, InfoVH03.ValeurChange
        'InfoVO03.ValeurChange, InfoVP03.ValeurChange,

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet
        Dim Rang As String
        'Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1))

        If CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID.Length > 8 Then
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 2))
        Else
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1))
        End If

        Select Case NumObjet
            Case 154
                If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_154(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
            Case 155
                If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_155(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    'Protected Sub RadioValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles _
    '    RadioVA02.ValeurChange, RadioVA12.ValeurChange, RadioVA08.ValeurChange,
    '    RadioVB02.ValeurChange, RadioVB12.ValeurChange, RadioVB08.ValeurChange,
    '    RadioVC02.ValeurChange, RadioVC12.ValeurChange, RadioVC08.ValeurChange,
    '    RadioVD02.ValeurChange, RadioVD12.ValeurChange, RadioVD08.ValeurChange,
    '    RadioVE02.ValeurChange, RadioVE12.ValeurChange, RadioVE08.ValeurChange,
    '    RadioVF02.ValeurChange, RadioVF12.ValeurChange, RadioVF08.ValeurChange,
    '    RadioVG02.ValeurChange, RadioVG12.ValeurChange, RadioVG08.ValeurChange,
    '    RadioVH02.ValeurChange, RadioVH12.ValeurChange, RadioVH08.ValeurChange,
    '    RadioVI12.ValeurChange, RadioVJ12.ValeurChange, RadioVK12.ValeurChange, RadioVL12.ValeurChange

    '    WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
    '    If WsDossierPER Is Nothing Then
    '        Exit Sub
    '    End If
    '    Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VTrioVerticalRadio).ID, 2))
    '    Dim NumObjet As Integer = CType(sender, VirtualiaControle_VTrioVerticalRadio).V_Objet
    '    Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VTrioVerticalRadio).ID, 7, 1))

    '    Select Case NumObjet
    '        Case 155
    '            If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
    '                If WsDossierPER.Objet_155(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
    '                    WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
    '                End If
    '            End If
    '    End Select
    'End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'ListeFormationsSuivies.Centrage_Colonne(0) = 1
        'ListeFormationsSuivies.Centrage_Colonne(1) = 0
        'ListeFormationsSuivies.Centrage_Colonne(2) = 0
        'ListeFormationsSuivies.Centrage_Colonne(3) = 0
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        Etiquette.Text = "COMPTE-RENDU D'ENTRETIEN DE FORMATION"
        LabelIdentite.Text = WsDossierPER.LibelleIdentite
        LabelVolet.Text = "ANNEE " & WsDossierPER.Annee
        LabelTitreNumero11.Text = "BILAN DE L'ANNEE " & WsDossierPER.AnneePrecedente

        LienPortail.Text = "<B>" & WsLienPortail & "</B>"
        LienPortail.NavigateUrl = WsLienPortail
        MailFormation.Text = WsMailFormation
        MailFormation.NavigateUrl = "mailto: " & WsMailFormation
        MailFormationTech.Text = WsMailFormationTechnique
        MailFormationTech.NavigateUrl = "mailto: " & WsMailFormationTechnique


        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        'Liste des formations 
        V_LibelListe = "Aucune formation" & VI.Tild & "Une formation" & VI.Tild & "formations"
        V_LibelColonne = "date" & VI.Tild & "formation suivie" & VI.Tild & "Durée" & VI.Tild & "Si DIF" & VI.Tild & "Clef"
        ' ListeFormationsSuivies.V_Liste(V_LibelColonne, V_LibelListe) = WsDossierPER.ListeDesFormationsSuivies(WsDossierPER.Annee, 1)

        'If WsDossierPER.FonctionExercee(DatedEffet, False) <> "" Then
        '    LabelIntitulePoste.Text = WsDossierPER.FonctionExercee(DatedEffet, False)
        '    LabelDatePriseFonctions.Text = WsDossierPER.FonctionExercee(DatedEffet, True)
        'End If

        Call LireLaFiche()
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop
        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 155
                    If WsDossierPER.Objet_155(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_155(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property


End Class