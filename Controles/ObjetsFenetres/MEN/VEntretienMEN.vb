﻿Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class VEntretienMEN
        Implements Virtualia.Net.Entretien.IGenericEntretien
        Private WsLstObservations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstFormations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstSyntheses As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing

        Public ReadOnly Property LitteralCompetence(Critere As Integer, Optional SiPrecision As Boolean = False) As String Implements IGenericEntretien.LitteralCompetence
            Get
                Return ""
            End Get
        End Property

        Public ReadOnly Property LitteralResultat(Critere As Integer) As String Implements IGenericEntretien.LitteralResultat
            Get
                Return ""
            End Get
        End Property

        Public ReadOnly Property LitteralSynthese(Critere As Integer) As String Implements IGenericEntretien.LitteralSynthese
            Get
                Return ""
            End Get
        End Property

        Public ReadOnly Property Table_Competences As List(Of ObjetTable) Implements IGenericEntretien.Table_Competences
            Get
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Table_Formations As List(Of ObjetTable) Implements IGenericEntretien.Table_Formations
            Get
                If WsLstFormations IsNot Nothing Then
                    Return WsLstFormations
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstFormations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                For IndiceTri = 1 To 5
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Intitule = ""
                    ItemTable.Libelle_Famille = "Compétence pour tenir le poste"
                    ItemTable.Rang_Famille = "2. Compétence à acquérir ou développer"
                    Select Case IndiceTri
                        Case 1
                            ItemTable.Clef_Origine = "A"
                        Case 2
                            ItemTable.Clef_Origine = "B"
                        Case 3
                            ItemTable.Clef_Origine = "C"
                        Case 4
                            ItemTable.Clef_Origine = "D"
                        Case 5
                            ItemTable.Clef_Origine = "E"
                    End Select
                    WsLstFormations.Add(ItemTable)
                Next IndiceTri

                For IndiceTri = 1 To 5
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Intitule = ""
                    ItemTable.Libelle_Famille = "Compétence en vue d'une évolution professionnelle"
                    ItemTable.Rang_Famille = "3. Compétence à acquérir ou développer"
                    Select Case IndiceTri
                        Case 1
                            ItemTable.Clef_Origine = "F"
                        Case 2
                            ItemTable.Clef_Origine = "G"
                        Case 3
                            ItemTable.Clef_Origine = "H"
                        Case 4
                            ItemTable.Clef_Origine = "I"
                        Case 5
                            ItemTable.Clef_Origine = "J"
                    End Select
                    WsLstFormations.Add(ItemTable)
                Next IndiceTri

                For IndiceTri = 1 To 5
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Intitule = ""
                    ItemTable.Libelle_Famille = "Autres perspectives de formation"
                    ItemTable.Rang_Famille = "4. Formation"
                    Select Case IndiceTri
                        Case 1
                            ItemTable.Clef_Origine = "K"
                        Case 2
                            ItemTable.Clef_Origine = "L"
                        Case 3
                            ItemTable.Clef_Origine = "M"
                        Case 4
                            ItemTable.Clef_Origine = "N"
                        Case 5
                            ItemTable.Clef_Origine = "O"
                    End Select
                    WsLstFormations.Add(ItemTable)
                Next IndiceTri

                For IndiceTri = 0 To WsLstFormations.Count - 1
                    WsLstFormations.Item(IndiceTri).Clef_Index = IndiceTri + 1
                Next IndiceTri

                Return WsLstFormations
            End Get
        End Property

        Public ReadOnly Property Table_Observations As List(Of ObjetTable) Implements IGenericEntretien.Table_Observations
            Get
                If WsLstObservations IsNot Nothing Then
                    Return WsLstObservations
                End If

                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstObservations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Evènements survenus au cours de la période écoulée"
                ItemTable.Rang_Famille = "2.2"
                ItemTable.Clef_Origine = "G"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Compétences professionnelles et technicité"
                ItemTable.Rang_Famille = "3.1.1"
                ItemTable.Clef_Origine = "H"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Contribution à l’activité du service"
                ItemTable.Rang_Famille = "3.1.2"
                ItemTable.Clef_Origine = "I"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Capacités professionnelles et relationnelles"
                ItemTable.Rang_Famille = "3.1.3"
                ItemTable.Clef_Origine = "J"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Aptitude au management et/ou à la conduite de projets"
                ItemTable.Rang_Famille = "3.1.4"
                ItemTable.Clef_Origine = "K"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Réalisation des objectifs de l’année écoulée"
                ItemTable.Rang_Famille = "3.2"
                ItemTable.Clef_Origine = "L"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Démarche envisagée et moyens à prévoir dont la formation"
                ItemTable.Rang_Famille = "5.2"
                ItemTable.Clef_Origine = "M"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Perspectives - Evolution des activités"
                ItemTable.Rang_Famille = "6.1"
                ItemTable.Clef_Origine = "N"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Perspectives - Evolution de carrière"
                ItemTable.Rang_Famille = "6.2"
                ItemTable.Clef_Origine = "O"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l’agent sur l’entretien"
                ItemTable.Rang_Famille = "8.1"
                ItemTable.Clef_Origine = "P"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l’agent sur les perspectives de carrière et de mobilité"
                ItemTable.Rang_Famille = "8.2"
                ItemTable.Clef_Origine = "Q"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaires sur les actions de formation suivies"
                ItemTable.Rang_Famille = "11.1"
                ItemTable.Clef_Origine = "R"
                WsLstObservations.Add(ItemTable)

                Return WsLstObservations
            End Get
        End Property

        Public ReadOnly Property Table_Syntheses As List(Of ObjetTable) Implements IGenericEntretien.Table_Syntheses
            Get
                If WsLstSyntheses IsNot Nothing Then
                    Return WsLstSyntheses
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstSyntheses = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Compétences professionnelles et technicité"
                ItemTable.Rang_Famille = "3.2.1"
                ItemTable.Clef_Origine = "A"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Contribution à l'activité du service"
                ItemTable.Rang_Famille = "3.2.2"
                ItemTable.Clef_Origine = "B"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Capacités professionnelles et relationnelles"
                ItemTable.Rang_Famille = "3.2.3"
                ItemTable.Clef_Origine = "C"
                WsLstSyntheses.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Aptitude à l'encadrement et/ou à la conduite de projets"
                ItemTable.Rang_Famille = "3.2.4"
                ItemTable.Clef_Origine = "D"
                WsLstSyntheses.Add(ItemTable)

                For IndiceTri = 0 To WsLstSyntheses.Count - 1
                    WsLstSyntheses.Item(IndiceTri).Clef_Index = IndiceTri + 1
                Next IndiceTri
                Return WsLstSyntheses
            End Get
        End Property

        Public ReadOnly Property Table_Complements As List(Of ObjetTable) Implements IGenericEntretien.Table_Complements
            Get
                Return Nothing
            End Get
        End Property
    End Class
End Namespace