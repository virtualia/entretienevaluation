﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_EMPLOI_CNM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_EMPLOI_CNM" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia"%>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderColor="#B0E0D7" 
    BorderStyle="None" BorderWidth="2px" HorizontalAlign="Center" 
    style="margin-top: 1px;" Visible="true" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreEmploi" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU ENTRETIEN ANNUEL D'EVALUATION" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="INFORMATIONS RELATIVES A L'AGENT ET A SON POSTE" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreStructure" runat="server" Height="25px" Width="750px"
                           Text="Collectivité :"
                           CssClass="EP_EtiComplement" style="margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelAffectationAnnuaire" runat="server" Height="40px" Width="750px" Text=""
                           CssClass="EP_EtiComplement" style="margin-bottom: 8px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="05" V_SiDonneeDico="true"
                           EtiWidth="120px" EtiHeight="26px" DonWidth="608px" DonHeight="20px" DonTabIndex="1" 
                           Etitext="Intitulé du poste" EtiVisible="true" Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                           V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                           EtiWidth="400px" EtiHeight="26px" DonWidth="328px" DonHeight="20px" DonTabIndex="2" 
                           Etitext="Ancienneté dans le poste au 31 décembre de l'année en cours" EtiVisible="true" Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:HyperLink ID="LienFichePoste" runat="server" NavigateUrl="Fiche de poste" Text="Fiche de poste de l'agent" Target="_blank"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-left: 0px; font-style: oblique; text-indent: 15px; text-align: center;"
                            Height="30px" Width="742px">
                        </asp:HyperLink> 
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="30px" Width="746px"
                           Text="Descriptif du poste occupé (cf. fiche de poste)"
                           CssClass="EP_TitreChapitre" style="padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV07" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="7" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" DonTabIndex="3"
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 3px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero31" runat="server" Height="30px" Width="746px"
                           Text="Bilan de l'exercice des missions dans le poste au cours de l'année écoulée"
                           CssClass="EP_TitreChapitre"
                           style="padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server" Width="746px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelTitreNumero32" runat="server" Height="25px" Width="480px"
                                       Text="Y a-t-il eu des modifications dans le poste au cours de l'année écoulée ?"
                                       CssClass="EP_EtiComplement"
                                       style="padding-top: 4px;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">        
                                   <Virtualia:VTrioHorizontalRadio ID="RadioH15" runat="server" V_Groupe="SiModificationsPoste"
                                       V_PointdeVue="1" V_Objet="150" V_Information="15" V_SiDonneeDico="true"
                                       RadioGaucheWidth="60px" RadioCentreWidth="60px" RadioDroiteWidth="0px"  
                                       RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteVisible="false"
                                       RadioGaucheHeight="25px" RadioCentreHeight="25px"
                                       RadioCentreStyle="text-align: center;"  
                                       RadioGaucheStyle="text-align: center;" Visible="true"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV16" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="16" V_SiDonneeDico="true"
                           DonWidth="742px" DonHeight="100px" DonTabIndex="4"
                           EtiVisible="true" EtiWidth ="742px" EtiHeight="26px"  
                           EtiText="Si oui, lesquelles (ex : changement de mission, de grade, d'affectation...) ?"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           Donstyle="margin-left: 3px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV17" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="17" V_SiDonneeDico="true"
                           DonWidth="742px" DonHeight="100px" DonTabIndex="5"
                           EtiVisible="true" EtiWidth ="742px" EtiHeight="26px"  
                           EtiText="Commentaires et précisions"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           Donstyle="margin-left: 3px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow> 
</asp:Table>