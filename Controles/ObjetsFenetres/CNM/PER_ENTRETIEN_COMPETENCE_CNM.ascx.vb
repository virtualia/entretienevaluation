﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE_CNM
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String = ""
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim Categorie As String
        Dim DatedEffet As String

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Select Case NumObjet
                Case 153
                    Select Case WsDossierPER.Categorie(DatedEffet)
                        Case "A", "B", "C"
                            Categorie = WsDossierPER.Categorie(DatedEffet) & Strings.Mid(Ctl.ID, 6, 1)
                        Case Else
                            Categorie = "C" & Strings.Mid(Ctl.ID, 6, 1)
                    End Select
                    Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Categorie)
                Case 154
                    Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            End Select
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            Select Case NumObjet
                Case 153
                    VirVertical.DonTooltip = ValeurLue(NumObjet, NumInfo, Rang)
                    VirVertical.V_SiEnLectureSeule = True
                Case 154
                    VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            End Select
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VSixBoutonRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VSixBoutonRadio)
            NumObjet = VirRadio.V_Objet
            Select Case NumObjet
                Case 153
                    Select Case WsDossierPER.Categorie(DatedEffet)
                        Case "A", "B", "C"
                            Categorie = WsDossierPER.Categorie(DatedEffet) & Strings.Mid(Ctl.ID, 7, 1)
                        Case Else
                            Categorie = "C" & Strings.Mid(Ctl.ID, 7, 1)
                    End Select
                    Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Categorie)
                Case 154
                    Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 7, 1))
            End Select
            VirRadio.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.V_SiAutoPostBack = Not (WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1)))
            Select Case NumObjet
                Case 153
                    If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                        Select Case ValeurLue(NumObjet, NumInfo, Rang)
                            Case Is = "4"
                                VirRadio.VRadioN1Check = True
                            Case Is = "3"
                                VirRadio.VRadioN2Check = True
                            Case Is = "2"
                                VirRadio.VRadioN3Check = True
                            Case Is = "1"
                                VirRadio.VRadioN4Check = True
                            Case Is = "0"
                                VirRadio.VRadioN5Check = True
                            Case Else
                                VirRadio.VRadioN5Check = True
                        End Select
                    End If
            End Select
            IndiceI += 1
        Loop
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoVE03.ValeurChange,
        InfoVF03.ValeurChange, InfoVG03.ValeurChange, InfoVA01.ValeurChange, InfoVB01.ValeurChange, InfoVC01.ValeurChange,
        InfoVD01.ValeurChange, InfoVE01.ValeurChange, InfoVF01.ValeurChange, InfoVG01.ValeurChange, InfoVH01.ValeurChange,
        InfoVI01.ValeurChange, InfoVJ01.ValeurChange, InfoVK01.ValeurChange, InfoVL01.ValeurChange, InfoVM01.ValeurChange,
        InfoVN01.ValeurChange, InfoVO01.ValeurChange, InfoVP01.ValeurChange, InfoVQ01.ValeurChange, InfoVR01.ValeurChange,
        InfoVS01.ValeurChange, InfoVT01.ValeurChange, InfoVU01.ValeurChange, InfoVV01.ValeurChange, InfoVW01.ValeurChange,
        InfoVX01.ValeurChange, InfoVY01.ValeurChange, InfoVZ01.ValeurChange, InfoV001.ValeurChange, InfoV101.ValeurChange,
        InfoV201.ValeurChange, InfoV301.ValeurChange, InfoV401.ValeurChange, InfoV501.ValeurChange, InfoV601.ValeurChange,
        InfoV701.ValeurChange, InfoV801.ValeurChange

        Dim Categorie As String
        Dim DatedEffet As String

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet
        Dim Rang As String = ""

        Select Case NumObjet
            Case 153
                Select Case WsDossierPER.Categorie(DatedEffet)
                    Case "A", "B", "C"
                        Categorie = WsDossierPER.Categorie(DatedEffet) & Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1)
                    Case Else
                        Categorie = "C" & Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1)
                End Select
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Categorie)
            Case 154
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1))
        End Select

        Select Case NumObjet
            Case 153
                If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
            Case 154
                If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_154(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub RadioValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles RadioHA07.ValeurChange,
        RadioHB07.ValeurChange, RadioHC07.ValeurChange, RadioHD07.ValeurChange, RadioHE07.ValeurChange, RadioHF07.ValeurChange,
        RadioHG07.ValeurChange, RadioHH07.ValeurChange, RadioHI07.ValeurChange, RadioHJ07.ValeurChange, RadioHK07.ValeurChange,
        RadioHL07.ValeurChange, RadioHM07.ValeurChange, RadioHN07.ValeurChange, RadioHO07.ValeurChange, RadioHP07.ValeurChange,
        RadioHQ07.ValeurChange, RadioHR07.ValeurChange, RadioHS07.ValeurChange, RadioHT07.ValeurChange, RadioHU07.ValeurChange,
        RadioHV07.ValeurChange, RadioHW07.ValeurChange, RadioHX07.ValeurChange, RadioHY07.ValeurChange, RadioHZ07.ValeurChange,
        RadioH007.ValeurChange, RadioH107.ValeurChange, RadioH207.ValeurChange, RadioH307.ValeurChange, RadioH407.ValeurChange,
        RadioH507.ValeurChange, RadioH607.ValeurChange, RadioH707.ValeurChange, RadioH807.ValeurChange

        Dim Categorie As String
        Dim DatedEffet As String
        Dim Eval As Integer = 0

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VSixBoutonRadio).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VSixBoutonRadio).V_Objet

        Dim Rang As String = ""

        Select Case NumObjet
            Case 153
                Select Case WsDossierPER.Categorie(DatedEffet)
                    Case "A", "B", "C"
                        Categorie = WsDossierPER.Categorie(DatedEffet) & Strings.Mid(CType(sender, VirtualiaControle_VSixBoutonRadio).ID, 7, 1)
                    Case Else
                        Categorie = "C" & Strings.Mid(CType(sender, VirtualiaControle_VSixBoutonRadio).ID, 7, 1)
                End Select
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Categorie)
            Case 154
                Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VSixBoutonRadio).ID, 7, 1))
        End Select

        Select Case NumObjet
            Case 153
                If IsNumeric(e.Valeur) Then
                    Eval = 4 - CInt(e.Valeur)
                End If
                If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                    If IsNumeric(e.Valeur) Then
                        If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> CStr(Eval) Then
                            WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = CStr(Eval)
                        End If
                    End If
                End If
        End Select
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        Etiquette.Text = "COMPTE-RENDU ENTRETIEN ANNUEL D'EVALUATION - Année " & WsDossierPER.Annee
        LabelIdentite.Text = WsDossierPER.LibelleIdentite
        LabelVolet.Text = "GRILLE DES CRITERES D'APPRECIATION - CATEGORIE " & WsDossierPER.Categorie(DatedEffet)

        Call LireLaFiche()

    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 153
                    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_153(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property

End Class