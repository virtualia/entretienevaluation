﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class VEntretienCNM
        Implements Virtualia.Net.Entretien.IGenericEntretien
        Private WsLstCompetences As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstObservations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstFormations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstSyntheses As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstComplements As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing

        Public ReadOnly Property LitteralCompetence(Critere As Integer, Optional SiPrecision As Boolean = False) As String Implements IGenericEntretien.LitteralCompetence
            Get
                Select Case Critere
                    Case 0
                        Return "SO - Sans objet"
                    Case 1
                        Return "PNS - Point non satisfaisant"
                    Case 2
                        Return "PAM - Point à améliorer"
                    Case 3
                        Return "PM - Point maîtrisé"
                    Case 4
                        Return "PF - Point fort"
                    Case Else
                        Return "Sans objet"
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property LitteralResultat(Critere As Integer) As String Implements IGenericEntretien.LitteralResultat
            Get
                Select Case Critere
                    Case 1
                        Return "Non atteint"
                    Case 2
                        Return "Partiellement atteint"
                    Case 3
                        Return "Atteint"
                    Case Else
                        Return "Non atteint"
                End Select
            End Get
        End Property

        Public ReadOnly Property LitteralSynthese(Critere As Integer) As String Implements IGenericEntretien.LitteralSynthese
            Get
                Return ""
            End Get
        End Property

        Public ReadOnly Property Table_Competences As List(Of ObjetTable) Implements IGenericEntretien.Table_Competences
            Get
                If WsLstCompetences IsNot Nothing Then
                    Return WsLstCompetences
                End If

                Dim IndiceFam As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstCompetences = New List(Of Virtualia.Net.Entretien.ObjetTable)

                For IndiceFam = 0 To 104
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Clef_Index = IndiceFam + 1 'Numero de Tri
                    Select Case IndiceFam
                        Case 0 To 9   'Compétences manière de servir - Grille de catégorie A
                            ItemTable.Rang_Famille = "2.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Savoir-Faire"
                            ItemTable.Rang_SousFamille = "2.1.1" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Manière de servir et qualités relationnelles"
                            Select Case IndiceFam
                                Case 0
                                    ItemTable.Intitule = "Implication au sein des projets de la collectivité"
                                    ItemTable.Clef_Origine = "AA"
                                Case 1
                                    ItemTable.Intitule = "Aptitudes relationnelles"
                                    ItemTable.Clef_Origine = "AB"
                                Case 2
                                    ItemTable.Intitule = "Sens du service public"
                                    ItemTable.Clef_Origine = "AC"
                                Case 3
                                    ItemTable.Intitule = "Réserve, discrétion et secret professionnels"
                                    ItemTable.Clef_Origine = "AD"
                                Case 4
                                    ItemTable.Intitule = "Capacité à travailler en équipe, en transversalité"
                                    ItemTable.Clef_Origine = "AE"
                                Case 5
                                    ItemTable.Intitule = "Adaptabilité et ouverture au changement"
                                    ItemTable.Clef_Origine = "AF"
                                Case 6
                                    ItemTable.Intitule = "Capacité à transférer ses connaissances"
                                    ItemTable.Clef_Origine = "AG"
                                Case 7
                                    ItemTable.Intitule = "Disponibilité"
                                    ItemTable.Clef_Origine = "AH"
                                Case 8
                                    ItemTable.Intitule = "Esprit d'innovation et créatif"
                                    ItemTable.Clef_Origine = "AI"
                                Case 9
                                    ItemTable.Intitule = ""
                                    ItemTable.Clef_Origine = "AJ"
                            End Select
                        Case 10 To 17 'Compétences capacités d'encadrement - Grille de catégorie A
                            ItemTable.Rang_Famille = "2.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Savoir-Faire"
                            ItemTable.Rang_SousFamille = "2.1.2" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Appréciation des capacités d'encadrement"
                            Select Case IndiceFam
                                Case 10
                                    ItemTable.Intitule = "Capacité à piloter, animer et organiser une équipe"
                                    ItemTable.Clef_Origine = "AK"
                                Case 11
                                    ItemTable.Intitule = "Capacité à maintenir la cohésion d'équipe"
                                    ItemTable.Clef_Origine = "AL"
                                Case 12
                                    ItemTable.Intitule = "Capacité à définir et négocier les missions et objectifs"
                                    ItemTable.Clef_Origine = "AM"
                                Case 13
                                    ItemTable.Intitule = "Capacité à superviser, déléguer et évaluer"
                                    ItemTable.Clef_Origine = "AN"
                                Case 14
                                    ItemTable.Intitule = "Capacité à mobiliser et valoriser les compétences individuelles et collectives"
                                    ItemTable.Clef_Origine = "AO"
                                Case 15
                                    ItemTable.Intitule = "Sens de l'écoute et attention portée aux collaborateurs"
                                    ItemTable.Clef_Origine = "AP"
                                Case 16
                                    ItemTable.Intitule = "Capacité à prévenir, à résoudre les conflits et à la médiation"
                                    ItemTable.Clef_Origine = "AQ"
                                Case 17
                                    ItemTable.Intitule = "Capacité au dialogue, à la communication et à la négociation"
                                    ItemTable.Clef_Origine = "AR"
                            End Select
                        Case 18 To 23 'Compétences techniques - Grille de catégorie A
                            ItemTable.Rang_Famille = "2.2" 'Numero de famille
                            ItemTable.Libelle_Famille = "Compétences techniques et professionnelles"
                            ItemTable.Rang_SousFamille = "2.2.1" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Compétences techniques"
                            Select Case IndiceFam
                                Case 18
                                    ItemTable.Intitule = "Maîtrise du cadre réglementaire et expertise du domaine d'activité"
                                    ItemTable.Clef_Origine = "AS"
                                Case 19
                                    ItemTable.Intitule = "Connaissance des instances et procédures décisionnelles de la collectivité"
                                    ItemTable.Clef_Origine = "AT"
                                Case 20
                                    ItemTable.Intitule = "Connaissance de l'environnement professionnel, des publics et des partenaires extérieurs"
                                    ItemTable.Clef_Origine = "AU"
                                Case 21
                                    ItemTable.Intitule = "Maîtrise des méthodes de gestion et d'évaluation de l'activité (élaboration, conception, utilisation de tableaux de bord, indicateurs..)"
                                    ItemTable.Clef_Origine = "AV"
                                Case 22
                                    ItemTable.Intitule = "Maîtrise des outils, logiciels, techniques nécessaires au poste ou au domaine d'activité"
                                    ItemTable.Clef_Origine = "AW"
                                Case 23
                                    ItemTable.Intitule = "Maîtrise des techniques d'information, de négociation et de communication"
                                    ItemTable.Clef_Origine = "AX"
                            End Select
                        Case 24 To 34 'Compétences professionnelles - Grille de catégorie A
                            ItemTable.Rang_Famille = "2.2" 'Numero de famille
                            ItemTable.Libelle_Famille = "Compétences techniques et professionnelles"
                            ItemTable.Rang_SousFamille = "2.2.2" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Compétences professionnelles"
                            Select Case IndiceFam
                                Case 24
                                    ItemTable.Intitule = "Conseiller, assister et alerter les élus sur les risques (juridiques, techniques, financiers, sanitaires..)"
                                    ItemTable.Clef_Origine = "AY"
                                Case 25
                                    ItemTable.Intitule = "Opérer des choix techniques et traduire les orientations stratégiques en projets et actions"
                                    ItemTable.Clef_Origine = "AZ"
                                Case 26
                                    ItemTable.Intitule = "Prendre des initiatives, des responsabilités et être force de propositions"
                                    ItemTable.Clef_Origine = "A0"
                                Case 27
                                    ItemTable.Intitule = "Anticiper les évolutions (en terme d'organisation, de ressources..)"
                                    ItemTable.Clef_Origine = "A1"
                                Case 28
                                    ItemTable.Intitule = "Identifier et hiérarchiser les priorités"
                                    ItemTable.Clef_Origine = "A2"
                                Case 29
                                    ItemTable.Intitule = "Identifier et mobiliser les partenaires stratégiques"
                                    ItemTable.Clef_Origine = "A3"
                                Case 30
                                    ItemTable.Intitule = "Suivre, contrôler et évaluer l'activité / les projets"
                                    ItemTable.Clef_Origine = "A4"
                                Case 31
                                    ItemTable.Intitule = "Synthétiser les informations et les analyser (élaboration d'argumentaires)"
                                    ItemTable.Clef_Origine = "A5"
                                Case 32
                                    ItemTable.Intitule = "Informer / communiquer sur les enjeux, les projets et les résultats"
                                    ItemTable.Clef_Origine = "A6"
                                Case 33
                                    ItemTable.Intitule = "Animer et conduire des réunions"
                                    ItemTable.Clef_Origine = "A7"
                                Case 34
                                    ItemTable.Intitule = "Qualité de l'expression écrite et orale"
                                    ItemTable.Clef_Origine = "A8"
                            End Select
                        Case 35 To 44   'Compétences manière de servir - Grille de catégorie B
                            ItemTable.Rang_Famille = "2.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Savoir-Faire"
                            ItemTable.Rang_SousFamille = "2.1.1" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Manière de servir et qualités relationnelles"
                            Select Case IndiceFam
                                Case 35
                                    ItemTable.Intitule = "Implication au sein du service"
                                    ItemTable.Clef_Origine = "BA"
                                Case 36
                                    ItemTable.Intitule = "Aptitudes relationnelles"
                                    ItemTable.Clef_Origine = "BB"
                                Case 37
                                    ItemTable.Intitule = "Sens du service public"
                                    ItemTable.Clef_Origine = "BC"
                                Case 38
                                    ItemTable.Intitule = "Réserve, discrétion et secret professionnels"
                                    ItemTable.Clef_Origine = "BD"
                                Case 39
                                    ItemTable.Intitule = "Capacité à travailler en équipe, en transversalité"
                                    ItemTable.Clef_Origine = "BE"
                                Case 40
                                    ItemTable.Intitule = "Ponctualité et assiduité"
                                    ItemTable.Clef_Origine = "BF"
                                Case 41
                                    ItemTable.Intitule = "Adaptabilité et ouverture au changement"
                                    ItemTable.Clef_Origine = "BG"
                                Case 42
                                    ItemTable.Intitule = "Capacité à transférer ses connaissances"
                                    ItemTable.Clef_Origine = "BH"
                                Case 43
                                    ItemTable.Intitule = "Respect des délais"
                                    ItemTable.Clef_Origine = "BI"
                                Case 44
                                    ItemTable.Intitule = "Fiabilité du travail"
                                    ItemTable.Clef_Origine = "BJ"
                            End Select
                        Case 45 To 52 'Compétences capacités d'encadrement - Grille de catégorie B
                            ItemTable.Rang_Famille = "2.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Savoir-Faire"
                            ItemTable.Rang_SousFamille = "2.1.2" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Appréciation des capacités d'encadrement"
                            Select Case IndiceFam
                                Case 45
                                    ItemTable.Intitule = "Capacité à piloter, animer et organiser une équipe"
                                    ItemTable.Clef_Origine = "BK"
                                Case 46
                                    ItemTable.Intitule = "Capacité à maintenir la cohésion d'équipe"
                                    ItemTable.Clef_Origine = "BL"
                                Case 47
                                    ItemTable.Intitule = "Capacité à définir et négocier les missions et objectifs"
                                    ItemTable.Clef_Origine = "BM"
                                Case 48
                                    ItemTable.Intitule = "Capacité à superviser, déléguer et évaluer"
                                    ItemTable.Clef_Origine = "BN"
                                Case 49
                                    ItemTable.Intitule = "Capacité à mobiliser et valoriser les compétences individuelles et collectives"
                                    ItemTable.Clef_Origine = "BO"
                                Case 50
                                    ItemTable.Intitule = "Sens de l'écoute et attention portée aux collaborateurs"
                                    ItemTable.Clef_Origine = "BP"
                                Case 51
                                    ItemTable.Intitule = "Capacité à prévenir, à résoudre les conflits et à la médiation"
                                    ItemTable.Clef_Origine = "BQ"
                                Case 52
                                    ItemTable.Intitule = ""
                                    ItemTable.Clef_Origine = "BR"
                            End Select
                        Case 53 To 58 'Compétences techniques - Grille de catégorie B
                            ItemTable.Rang_Famille = "2.2" 'Numero de famille
                            ItemTable.Libelle_Famille = "Compétences techniques et professionnelles"
                            ItemTable.Rang_SousFamille = "2.2.1" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Compétences techniques"
                            Select Case IndiceFam
                                Case 53
                                    ItemTable.Intitule = "Maîtrise du cadre réglementaire et des techniques propres au domaine d'activité"
                                    ItemTable.Clef_Origine = "BS"
                                Case 54
                                    ItemTable.Intitule = "Connaissance des instances et procédures décisionnelles de la collectivité"
                                    ItemTable.Clef_Origine = "BT"
                                Case 55
                                    ItemTable.Intitule = "Connaissance de l'environnement professionnel, des publics et des partenaires extérieurs"
                                    ItemTable.Clef_Origine = "BU"
                                Case 56
                                    ItemTable.Intitule = "Maîtrise des techniques de recueil et de traitement de l'information"
                                    ItemTable.Clef_Origine = "BV"
                                Case 57
                                    ItemTable.Intitule = "Maîtrise des outils, logiciels, techniques nécessaires au poste ou au domaine d'activité"
                                    ItemTable.Clef_Origine = "BW"
                                Case 58
                                    ItemTable.Intitule = ""
                                    ItemTable.Clef_Origine = "BX"
                            End Select
                        Case 59 To 69 'Compétences professionnelles - Grille de catégorie B
                            ItemTable.Rang_Famille = "2.2" 'Numero de famille
                            ItemTable.Libelle_Famille = "Compétences techniques et professionnelles"
                            ItemTable.Rang_SousFamille = "2.2.2" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Compétences professionnelles"
                            Select Case IndiceFam
                                Case 59
                                    ItemTable.Intitule = "Traduire en actions les objectifs du service et mettre en oeuvre les projets"
                                    ItemTable.Clef_Origine = "BY"
                                Case 60
                                    ItemTable.Intitule = "Opérer des choix techniques adaptés"
                                    ItemTable.Clef_Origine = "BZ"
                                Case 61
                                    ItemTable.Intitule = "Prendre des initiatives et des responsabilités"
                                    ItemTable.Clef_Origine = "B0"
                                Case 62
                                    ItemTable.Intitule = "Emettre des propositions et des solutions"
                                    ItemTable.Clef_Origine = "B1"
                                Case 63
                                    ItemTable.Intitule = "Identifier et hiérarchiser les priorités"
                                    ItemTable.Clef_Origine = "B2"
                                Case 64
                                    ItemTable.Intitule = "Synthétiser les informations et les analyser"
                                    ItemTable.Clef_Origine = "B3"
                                Case 65
                                    ItemTable.Intitule = "Conseiller, assister et alerter les élus sur les risques (juridiques, techniques, financiers, sanitaires..)"
                                    ItemTable.Clef_Origine = "B4"
                                Case 66
                                    ItemTable.Intitule = "Restituer l'information et rendre compte"
                                    ItemTable.Clef_Origine = "B5"
                                Case 67
                                    ItemTable.Intitule = "Animer et conduire des réunions"
                                    ItemTable.Clef_Origine = "B6"
                                Case 68
                                    ItemTable.Intitule = "Qualité de l'expression écrite et orale"
                                    ItemTable.Clef_Origine = "B7"
                                Case 69
                                    ItemTable.Intitule = ""
                                    ItemTable.Clef_Origine = "B8"
                            End Select
                        Case 70 To 79   'Compétences manière de servir - Grille de catégorie C
                            ItemTable.Rang_Famille = "2.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Savoir-Faire"
                            ItemTable.Rang_SousFamille = "2.1.1" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Manière de servir et qualités relationnelles"
                            Select Case IndiceFam
                                Case 70
                                    ItemTable.Intitule = "Implication au sein du service"
                                    ItemTable.Clef_Origine = "CA"
                                Case 71
                                    ItemTable.Intitule = "Aptitudes relationnelles"
                                    ItemTable.Clef_Origine = "CB"
                                Case 72
                                    ItemTable.Intitule = "Sens du service public"
                                    ItemTable.Clef_Origine = "CC"
                                Case 73
                                    ItemTable.Intitule = "Réserve, discrétion et secret professionnels"
                                    ItemTable.Clef_Origine = "CD"
                                Case 74
                                    ItemTable.Intitule = "Capacité à travailler en équipe, en transversalité"
                                    ItemTable.Clef_Origine = "CE"
                                Case 75
                                    ItemTable.Intitule = "Ponctualité et assiduité"
                                    ItemTable.Clef_Origine = "CF"
                                Case 76
                                    ItemTable.Intitule = "Respect des moyens matériels"
                                    ItemTable.Clef_Origine = "CG"
                                Case 77
                                    ItemTable.Intitule = "Travailler en autonomie"
                                    ItemTable.Clef_Origine = "CH"
                                Case 78
                                    ItemTable.Intitule = "Rigueur et fiabilité du travail effectué"
                                    ItemTable.Clef_Origine = "CI"
                                Case 79
                                    ItemTable.Intitule = "Réactivité face à une situation d'urgence"
                                    ItemTable.Clef_Origine = "CJ"
                            End Select
                        Case 80 To 87 'Compétences capacités d'encadrement - Grille de catégorie C
                            ItemTable.Rang_Famille = "2.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Savoir-Faire"
                            ItemTable.Rang_SousFamille = "2.1.2" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Appréciation des capacités d'encadrement"
                            Select Case IndiceFam
                                Case 80
                                    ItemTable.Intitule = "Coordonner et évaluer les interventions d'une équipe"
                                    ItemTable.Clef_Origine = "CK"
                                Case 81
                                    ItemTable.Intitule = "Capacité à maintenir la cohésion d'équipe"
                                    ItemTable.Clef_Origine = "CL"
                                Case 82
                                    ItemTable.Intitule = "Expliquer les consignes et les faire respecter"
                                    ItemTable.Clef_Origine = "CM"
                                Case 83
                                    ItemTable.Intitule = "Capacité au dialogue et à la communication"
                                    ItemTable.Clef_Origine = "CN"
                                Case 84
                                    ItemTable.Intitule = "Capacité à prévenir et à résoudre les conflits"
                                    ItemTable.Clef_Origine = "CO"
                                Case 85
                                    ItemTable.Intitule = ""
                                    ItemTable.Clef_Origine = "CP"
                                Case 86
                                    ItemTable.Intitule = ""
                                    ItemTable.Clef_Origine = "CQ"
                                Case 87
                                    ItemTable.Intitule = ""
                                    ItemTable.Clef_Origine = "CR"
                            End Select
                        Case 88 To 93 'Compétences techniques - Grille de catégorie C
                            ItemTable.Rang_Famille = "2.2" 'Numero de famille
                            ItemTable.Libelle_Famille = "Compétences techniques et professionnelles"
                            ItemTable.Rang_SousFamille = "2.2.1" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Compétences techniques"
                            Select Case IndiceFam
                                Case 88
                                    ItemTable.Intitule = "Connaissance des procédures et techniques propres au domaine d'activité"
                                    ItemTable.Clef_Origine = "CS"
                                Case 89
                                    ItemTable.Intitule = "Connaissance des règles d'hygiène et de sécurité"
                                    ItemTable.Clef_Origine = "CT"
                                Case 90
                                    ItemTable.Intitule = "Connaissance de l'environnement professionnel"
                                    ItemTable.Clef_Origine = "CU"
                                Case 91
                                    ItemTable.Intitule = "Maîtrise des outils, logiciels, techniques nécessaires au poste ou au domaine d'activité"
                                    ItemTable.Clef_Origine = "CV"
                                Case 92
                                    ItemTable.Intitule = ""
                                    ItemTable.Clef_Origine = "CW"
                                Case 93
                                    ItemTable.Intitule = ""
                                    ItemTable.Clef_Origine = "CX"
                            End Select
                        Case 94 To 104 'Compétences professionnelles - Grille de catégorie C
                            ItemTable.Rang_Famille = "2.2" 'Numero de famille
                            ItemTable.Libelle_Famille = "Compétences techniques et professionnelles"
                            ItemTable.Rang_SousFamille = "2.2.2" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Compétences professionnelles"
                            Select Case IndiceFam
                                Case 94
                                    ItemTable.Intitule = "Organiser, planifier son travail et mettre en oeuvre les instructions"
                                    ItemTable.Clef_Origine = "CY"
                                Case 95
                                    ItemTable.Intitule = "Respecter les règles et directives dans le domaine d'activités"
                                    ItemTable.Clef_Origine = "CZ"
                                Case 96
                                    ItemTable.Intitule = "Savoir utiliser et manipuler les moyens matériels"
                                    ItemTable.Clef_Origine = "C0"
                                Case 97
                                    ItemTable.Intitule = "Rendre compte de ses activités"
                                    ItemTable.Clef_Origine = "C1"
                                Case 98
                                    ItemTable.Intitule = "Respecter les délais et exécuter les consignes avec efficacité"
                                    ItemTable.Clef_Origine = "C2"
                                Case 99
                                    ItemTable.Intitule = "Prendre des initiatives"
                                    ItemTable.Clef_Origine = "C3"
                                Case 100
                                    ItemTable.Intitule = "Savoir traiter les informations recueillies"
                                    ItemTable.Clef_Origine = "C4"
                                Case 101
                                    ItemTable.Intitule = "Compétences professionnelles propres au domaine d'activité"
                                    ItemTable.Clef_Origine = "C5"
                                Case 102
                                    ItemTable.Intitule = ""
                                    ItemTable.Clef_Origine = "C6"
                                Case 103
                                    ItemTable.Intitule = ""
                                    ItemTable.Clef_Origine = "C7"
                                Case 104
                                    ItemTable.Intitule = ""
                                    ItemTable.Clef_Origine = "C8"
                            End Select
                    End Select
                    WsLstCompetences.Add(ItemTable)
                Next IndiceFam
                Return WsLstCompetences
            End Get
        End Property

        Public ReadOnly Property Table_Formations As List(Of ObjetTable) Implements IGenericEntretien.Table_Formations
            Get
                If WsLstFormations IsNot Nothing Then
                    Return WsLstFormations
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstFormations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                For IndiceTri = 1 To 4
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Intitule = ""
                    ItemTable.Libelle_Famille = "Besoins en formation DIF"
                    ItemTable.Rang_Famille = "1 - Formations DIF demandées par l'évalué"
                    Select Case IndiceTri
                        Case 1
                            ItemTable.Clef_Origine = "A"
                        Case 2
                            ItemTable.Clef_Origine = "B"
                        Case 3
                            ItemTable.Clef_Origine = "C"
                        Case 4
                            ItemTable.Clef_Origine = "D"
                    End Select
                    WsLstFormations.Add(ItemTable)
                Next IndiceTri

                For IndiceTri = 1 To 4
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Intitule = ""
                    ItemTable.Libelle_Famille = "Besoins en formation prescrits par l'évaluateur"
                    ItemTable.Rang_Famille = "2 - Formations prescrites par l'évaluateur"
                    Select Case IndiceTri
                        Case 1
                            ItemTable.Clef_Origine = "E"
                        Case 2
                            ItemTable.Clef_Origine = "F"
                        Case 3
                            ItemTable.Clef_Origine = "G"
                        Case 4
                            ItemTable.Clef_Origine = "H"
                    End Select
                    WsLstFormations.Add(ItemTable)
                Next IndiceTri

                For IndiceTri = 0 To WsLstFormations.Count - 1
                    WsLstFormations.Item(IndiceTri).Clef_Index = IndiceTri + 1
                Next IndiceTri

                Return WsLstFormations
            End Get
        End Property

        Public ReadOnly Property Table_Observations As List(Of ObjetTable) Implements IGenericEntretien.Table_Observations
            Get
                If WsLstObservations IsNot Nothing Then
                    Return WsLstObservations
                End If

                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstObservations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Bilan des missions dans le poste"
                ItemTable.Rang_Famille = "1.1"
                ItemTable.Clef_Origine = "A"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Bilan - Points forts"
                ItemTable.Rang_Famille = "1.2"
                ItemTable.Clef_Origine = "B"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Bilan - Points à améliorer"
                ItemTable.Rang_Famille = "1.3"
                ItemTable.Clef_Origine = "C"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Bilan - Objectifs du service"
                ItemTable.Rang_Famille = "1.4"
                ItemTable.Clef_Origine = "D"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaires de l'évaluateur - Manière de servir et qualités relationnelles"
                ItemTable.Rang_Famille = "2.1"
                ItemTable.Clef_Origine = "E"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaires de l'évaluateur - Capacités d'encadrement"
                ItemTable.Rang_Famille = "2.2"
                ItemTable.Clef_Origine = "F"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Commentaires de l'évaluateur - Compétences techniques et professionnelles"
                ItemTable.Rang_Famille = "2.3"
                ItemTable.Clef_Origine = "G"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Propositions - Objectifs individuels"
                ItemTable.Rang_Famille = "3.1"
                ItemTable.Clef_Origine = "H"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Appréciation de la formation et des compétences acquises"
                ItemTable.Rang_Famille = "3.2"
                ItemTable.Clef_Origine = "I"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Autres besoins matériels et autres"
                ItemTable.Rang_Famille = "3.3"
                ItemTable.Clef_Origine = "J"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhait d'évolution des missions dans le poste"
                ItemTable.Rang_Famille = "4.1"
                ItemTable.Clef_Origine = "K"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhait d'évolution professionnelle - Mobilité vers un autre poste"
                ItemTable.Rang_Famille = "4.2"
                ItemTable.Clef_Origine = "L"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhait d'évolution professionnelle - Mobilité géographique"
                ItemTable.Rang_Famille = "4.3"
                ItemTable.Clef_Origine = "M"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhait d'évolution professionnelle - Avancement de carrière"
                ItemTable.Rang_Famille = "4.4"
                ItemTable.Clef_Origine = "N"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhait d'évolution personnelle - Retraite"
                ItemTable.Rang_Famille = "4.5"
                ItemTable.Clef_Origine = "O"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhait d'évolution personnelle - Congés formation"
                ItemTable.Rang_Famille = "4.6"
                ItemTable.Clef_Origine = "P"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Souhait d'évolution personnelle - Autres"
                ItemTable.Rang_Famille = "4.7"
                ItemTable.Clef_Origine = "Q"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Rappel des objectifs de service"
                ItemTable.Rang_Famille = "5.1"
                ItemTable.Clef_Origine = "R"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Conclusion de l'entretien - Observations de l'agent"
                ItemTable.Rang_Famille = "6.1"
                ItemTable.Clef_Origine = "S"
                WsLstObservations.Add(ItemTable)

                Return WsLstObservations
            End Get
        End Property

        Public ReadOnly Property Table_Syntheses As List(Of ObjetTable) Implements IGenericEntretien.Table_Syntheses
            Get
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Table_Complements As List(Of ObjetTable) Implements IGenericEntretien.Table_Complements
            Get
                If WsLstComplements IsNot Nothing Then
                    Return WsLstComplements
                End If

                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstComplements = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Ancienneté dans le poste"
                ItemTable.Clef_Index = 1
                ItemTable.Clef_Origine = "A"
                ItemTable.Libelle_Famille = CStr(VI.NatureDonnee.DonneeTexte)
                WsLstComplements.Add(ItemTable)

                Return WsLstComplements
            End Get
        End Property
    End Class
End Namespace