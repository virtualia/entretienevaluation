﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_APPRECIATION_GIP.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_APPRECIATION_GIP" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VSixBoutonRadio.ascx" TagName="VSixBoutonRadio" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR {
        background-color: #216B68;
        color: #D7FAF3;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: bold;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: initial;
        height: 25px;
        width: 746px;
    }

    .EP_TitreOnglet {
        background-color: #E2F5F1;
        color: #0E5F5C;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: bold;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_TitreChapitre {
        background-color: #CAEBE4;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiComplement {
        background-color: #E2F5F1;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-align: left;
        text-indent: 5px;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiComplementBis {
        background-color: #E9FDF9;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-align: left;
        text-indent: 5px;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiComplementTer {
        background-color: #98D4CA;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-align: left;
        text-indent: 5px;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiTableau {
        background-color: #D7FAF3;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: normal;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_TitreParagraphe {
        background-color: #8DA8A3;
        color: white;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: normal;
        text-indent: 8px;
        text-align: left;
        padding-top: 6px;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiNiveau {
        background-color: white;
        color: #124545;
        border-style: solid;
        border-width: 1px;
        border-color: #9EB0AC;
        font-family: 'Trebuchet MS';
        font-size: x-small;
        font-style: italic;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreAppreciation" runat="server" Height="75px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            CssClass="EP_TitreCR">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="VII. APPRÉCIATION GLOBALE DE LA VALEUR PROFESSIONNELLE DE L’AGENT" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            CssClass="EP_TitreChapitre">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="70px" Width="183px"
                            Text="• Qualité du travail : </br>• Qualités relationnelles : </br> • Implication personnelle : </br>• Sens du service public :"
                            CssClass="EP_EtiNiveau" Style="text-align: left; margin-top: -8px;">
                        </asp:Label>
                        <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" Width="180px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCocheSimple ID="CocheA10" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="true" V_Height="30px"
                                        V_Text="   Exceptionnelle" V_Width="180px" V_SiModeCaractere="true"
                                        V_Style="margin-left: 0px; font-style: normal; text-indent: 5px; text-align: left; Font-size: 90%" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCocheSimple ID="CocheB10" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="true" V_Height="30px"
                                        V_Text="  Supérieure aux attentes" V_Width="180px" V_SiModeCaractere="true"
                                        V_Style="margin-left: 0px; font-style: normal; text-indent: 5px; text-align: left; Font-size: 90%" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCocheSimple ID="CocheC10" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="true" V_Height="30px"
                                        V_Text="  Conforme aux attentes" V_Width="180px" V_SiModeCaractere="true"
                                        V_Style="margin-left: 0px; font-style: normal; text-indent: 5px; text-align: left; Font-size: 90%" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCocheSimple ID="CocheD10" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="true" V_Height="30px"
                                        V_Text="  En deçà des attentes" V_Width="180px" V_SiModeCaractere="true"
                                        V_Style="margin-left: 0px; font-style: normal; text-indent: 5px; text-align: left; Font-size: 90%" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VCoupleVerticalEtiDonnee1" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            DonWidth="560px" DonHeight="210px" DonTabIndex="1"
                            DonStyle="margin-left: 0px;" DonBorderWidth="1px"
                            EtiVisible="False" EtiText="" EtiWidth="740px" EtiHeight="20px" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="Label7" runat="server" Text="IX. COMMENTAIRE GÉNÉRAL SUR LA TENUE DE L’ENTRETIEN" Height="20px" Width="746px"
                CssClass="EP_TitreOnglet">
            </asp:Label>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Label1" runat="server" Height="35px" Width="370px"
                            BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Commentaires de l'agent"
                            BorderWidth="1px" ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="35px" Width="370px"
                            BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Commentaires du supérieur hiérarchique direct "
                            BorderWidth="1px" ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVV03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="True"
                            EtiWidth="370px" DonWidth="368px" DonHeight="280px" EtiHeight="20px" DonTabIndex="1"
                            EtiText="" EtiVisible="False"
                            DonStyle="margin-left: 0px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VCoupleVerticalEtiDonnee2" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="True"
                            EtiWidth="370px" DonWidth="368px" DonHeight="280px" EtiHeight="20px" DonTabIndex="2"
                            EtiText="" EtiVisible="False"
                            DonStyle="margin-left: 0px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>











    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero21" runat="server" Height="20px" Width="746px"
                            Text="INFORMATIONS / Notification du compte rendu & Recours éventuel de l’Agent"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Label2" runat="server" Height="20px" Width="746px"
                            Text="Notification du compte rendu d’entretien et envoi à l’agent dans les 5 jours francs (remise en main propre ou recommandé pour les sites distants)"
                            CssClass="EP_EtiNiveau" BackColor="#E2F5F1" Style="text-align: left" BorderWidth="0">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Label3" runat="server" Height="30px" Width="746px"
                            Text="Le compte rendu de l’entretien d’évaluation sert de base à la proposition d’évolution professionnelle et changement de groupe ou d’indice. L’agent peut demander la révision du compte rendu de l’entretien."
                            CssClass="EP_EtiNiveau" BackColor="#E2F5F1" Style="text-align: left" BorderWidth="0">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Label4" runat="server" Height="40px" Width="746px"
                            Text="<u>En premier lieu </u>, l’agent doit exercer un recours auprès de son supérieur hiérarchique dans les 15 jours francs suivant la date de notification du compte rendu ; L’autorité hiérarchique notifie sa réponse dans un délai de quinze jours francs à compter de la date de réception de la demande de révision du compte rendu de l’entretien professionnel."
                            CssClass="EP_EtiNiveau" BackColor="#E2F5F1" Style="text-align: left" BorderWidth="0">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Label5" runat="server" Height="50px" Width="746px"
                            Text="<u>En second lieu </u>, l’agent peut saisir la commission consultative paritaire dans un délai d’un mois à compter de la date de notification de la réponse formulée par l’autorité hiérarchique dans le cadre du recours. L’autorité hiérarchique après avoir reçu l’avis de la commission, communique à l’agent, qui en accuse réception, le compte rendu définitif de l’entretien professionnel. Si l’agent n’est pas satisfait de la réponse de l’autorité hiérarchique, il lui est loisible de saisir le tribunal administratif dans le délai de deux mois suivant la réception du compte rendu définitif."
                            CssClass="EP_EtiNiveau" BackColor="#E2F5F1" Style="text-align: left" BorderWidth="0">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Label6" runat="server" Height="20px" Width="746px"
                            Text="Sources : Article 1-4 du décret n° 83-86 du 17 janvier 1986 "
                            CssClass="EP_EtiNiveau" BackColor="#E2F5F1" Style="text-align: left" BorderWidth="0">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>


    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table6" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="#CAEBE4" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Label ID="Label9" runat="server" Height="20px" Width="230px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Signature de l’agent,"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                            Style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                        <asp:Label ID="Label10" runat="server" Height="20px" Width="230px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="(Nom et prénom)"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>

                        <asp:Table ID="Table3" runat="server" CellPadding="0" CellSpacing="0" Width="230px"
                            BackColor="#CAEBE4" >
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                    <asp:Label ID="Label16" runat="server" Height="20px" Width="20px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="À,"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee2" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true" EtiVisible="false"
                                        EtiWidth="230px" DonWidth="120px" DonTabIndex="6" EtiText="Date "
                                        DonTooltip="" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                    <asp:Label ID="Label18" runat="server" Height="20px" Width="20px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Le : "
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee1" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true" EtiVisible="false"
                                        EtiWidth="230px" DonWidth="80px" DonTabIndex="6" EtiText="Date "
                                        DonTooltip="" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <asp:Table ID="Table5" runat="server" CellPadding="0" CellSpacing="0" Width="230px"
                            BackColor="#CAEBE4" Style="margin-left: -3px;">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="left" VerticalAlign="Middle">
                                    <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee5" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true" EtiVisible="false"
                                        EtiWidth="230px" DonWidth="255px" DonHeight="50px" DonTabIndex="6" EtiText="Date "
                                        DonTooltip="" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>



                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Label ID="Label12" runat="server" Height="20px" Width="230px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Signature du supérieur hiérarchique direct,"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                            Style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                        <asp:Label ID="Label13" runat="server" Height="20px" Width="230px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="(Nom et prénom)"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                        <asp:Table ID="Table4" runat="server" CellPadding="0" CellSpacing="0" Width="230px"
                            BackColor="#CAEBE4" >
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                    <asp:Label ID="Label19" runat="server" Height="20px" Width="20px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="À,"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee3" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true" EtiVisible="false"
                                        EtiWidth="230px" DonWidth="120px" DonTabIndex="6" EtiText="Date "
                                        DonTooltip="" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                    <asp:Label ID="Label20" runat="server" Height="20px" Width="20px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Le : "
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee4" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true" EtiVisible="false"
                                        EtiWidth="230px" DonWidth="80px" DonTabIndex="6" EtiText="Date "
                                        DonTooltip="" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <asp:Table ID="Table7" runat="server" CellPadding="0" CellSpacing="0" Width="230px"
                            BackColor="#CAEBE4" Style="margin-left: -3px;">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="left" VerticalAlign="Middle">
                                    <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee6" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true" EtiVisible="false"
                                        EtiWidth="230px" DonWidth="255px" DonHeight="50px" DonTabIndex="6" EtiText="Date "
                                        DonTooltip="" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>

                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Label ID="Label14" runat="server" Height="20px" Width="230px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Le Directeur du GIP FCIP"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                            Style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                        <asp:Label ID="Label15" runat="server" Height="20px" Width="230px"
                            BackColor="Transparent" BorderStyle="None"
                            Text=""
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                        <asp:Label ID="Label22" runat="server" Height="20px" Width="230px"
                            BackColor="Transparent" BorderStyle="None"
                            Text=""
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                        <asp:Table ID="Table8" runat="server" CellPadding="0" CellSpacing="0" Width="230px"
                            BackColor="#CAEBE4" Style="margin-left: -3px; margin-top: 6px;">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="left" VerticalAlign="Middle">
                                    <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee7" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true" EtiVisible="false"
                                        EtiWidth="230px" DonWidth="255px" DonHeight="50px" DonTabIndex="6" EtiText="Date "
                                        DonTooltip="" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>

                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

        <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table9" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                        <asp:Label ID="Label8" runat="server" Height="20px" Width="746px"
                            Text="Notification de l’entretien à l’agent"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>
                                                <asp:Label ID="Label11" runat="server" Height="20px" Width="746px"
                            Text="Partie à remplir après signature du Directeur"
                            CssClass="EP_TitreOnglet" Style=" font-size:x-small; font-style: italic; font-weight: normal;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                    <asp:Label ID="Label17" runat="server" Height="20px" Width="120px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Notifié à l'agent le : "
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" Width="250px">
                                    <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee8" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true" EtiVisible="false"
                                        EtiWidth="230px" DonWidth="80px" DonTabIndex="6" EtiText="Date "
                                        DonTooltip="" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                    <asp:Label ID="Label21" runat="server" Height="20px" Width="120px"
                                        BackColor="Transparent" BorderStyle="None"
                                        Text="Signature de l'agent : "
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="x-small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee9" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true" EtiVisible="false"
                                        EtiWidth="230px" DonWidth="250px" DonTabIndex="6" EtiText="Date "
                                        DonTooltip="" />
                                </asp:TableCell>
                            </asp:TableRow>
                                                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
