﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_RESULTATS_GIP.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_RESULTATS_GIP" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixVerticalRadio.ascx" tagname="VSixVerticalRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderColor="#B0E0D7" 
    BorderStyle="None" BorderWidth="2px" HorizontalAlign="Center" 
    style="margin-top: 1px;" Visible="true" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreResultats" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="I. BILAN DES OBJECTIFS FIXES POUR L'ANNEE ECOULEE" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>


       <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="LabelNumerobjectif1" runat="server" Height="25px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°1"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif1" runat="server" Height="35px" Width="301px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteIndicateurs1" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteIndicateurs1" runat="server" Height="35px" Width="260px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Analyse des résultats / Commentaires"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelEnteteRealisation1" runat="server" Height="35px" Width="179px"
                                        BackColor="#D7FAF3" BorderStyle="None" 
                                        Text="Réalisation"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="297px" DonHeight="140px" DonTabIndex="2"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableIndicateur1" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="256px" DonHeight="140px" DonTabIndex="3"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="RadioHC04" runat="server" V_Groupe="Realisation1"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="Atteint" VRadioN2Text="Partiellement atteint" VRadioN3Text="Non atteint"
                                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                             VRadioN4Visible="false"   VRadioN5Visible="false"  VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="LabelNumerobjectif2" runat="server" Height="25px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°2"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif2" runat="server" Height="35px" Width="301px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteIndicateurs2" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteIndicateurs2" runat="server" Height="35px" Width="260px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Analyse des résultats / Commentaires"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelEnteteRealisation2" runat="server" Height="35px" Width="179px"
                                        BackColor="#D7FAF3" BorderStyle="None" 
                                        Text="Réalisation"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="297px" DonHeight="140px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableIndicateur2" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="256px" DonHeight="140px" DonTabIndex="6"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="RadioHD04" runat="server" V_Groupe="Realisation1"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="Atteint" VRadioN2Text="Partiellement atteint" VRadioN3Text="Non atteint"
                                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                             VRadioN4Visible="false"   VRadioN5Visible="false"  VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="3">
                        <asp:Label ID="LabelNumerobjectif3" runat="server" Height="25px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°3"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif3" runat="server" Height="35px" Width="301px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteIndicateurs3" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteIndicateurs3" runat="server" Height="35px" Width="260px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Analyse des résultats / Commentaires"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelEnteteRealisation3" runat="server" Height="35px" Width="179px"
                                        BackColor="#D7FAF3" BorderStyle="None" 
                                        Text="Réalisation"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "80%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="301px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="297px" DonHeight="140px" DonTabIndex="8"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableIndicateur3" runat="server" CellPadding="0" CellSpacing="0" Width="260px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="256px" DonHeight="140px" DonTabIndex="9"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="RadioHE04" runat="server" V_Groupe="Realisation1"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="Atteint" VRadioN2Text="Partiellement atteint" VRadioN3Text="Non atteint"
                                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                             VRadioN4Visible="false"   VRadioN5Visible="false"  VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>





<%--  ****************************--%>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left" ColumnSpan="3">
                        <asp:Label ID="Label2" runat="server" Height="25px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Autre réalisation accomplie durant l'année écoulée"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="150px" EtiHeight="30px" DonTabIndex="26" 
                           Etitext="Autre réalisation accomplie durant l'année écoulée"
                           Donstyle="margin-left: 3px;" EtiForeColor ="White"  EtiVisible ="false"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 4px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                   <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left" ColumnSpan="3">
                        <asp:Label ID="Label1" runat="server" Height="25px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="APPRÉCIATION DE L’ÉVALUATEUR"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                 <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="RadioHA03" runat="server" V_Groupe="Realisation1"
                                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="Supérieure aux attentes" VRadioN2Text="Conforme aux attentes" VRadioN3Text="En-deçà des attentes"
                                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                             VRadioN4Visible="false"   VRadioN5Visible="false"  VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
</asp:Table>