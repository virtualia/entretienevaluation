﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_ENTETE_GIP.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_ENTETE_GIP" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VListeGrid.ascx" TagName="VListeGrid" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VSixVerticalRadio.ascx" TagName="VSixVerticalRadio" TagPrefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR {
        background-color: #216B68;
        color: #D7FAF3;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: bold;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: initial;
        height: 25px;
        width: 746px;
    }

    .EP_TitreOnglet {
        background-color: #E2F5F1;
        color: #0E5F5C;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: bold;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_TitreChapitre {
        background-color: #CAEBE4;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiComplement {
        background-color: #E2F5F1;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-align: left;
        text-indent: 5px;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiComplementBis {
        background-color: #E9FDF9;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-align: left;
        text-indent: 5px;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiComplementTer {
        background-color: #98D4CA;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-align: left;
        text-indent: 5px;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiTableau {
        background-color: #D7FAF3;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: normal;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_TitreParagraphe {
        background-color: #8DA8A3;
        color: white;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: normal;
        text-indent: 8px;
        text-align: left;
        padding-top: 6px;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiNiveau {
        background-color: white;
        color: #124545;
        border-style: solid;
        border-width: 1px;
        border-color: #9EB0AC;
        font-family: 'Trebuchet MS';
        font-size: x-small;
        font-style: italic;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 744px;
    }
</style>

<asp:Table ID="CadreListe" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0"
    HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="600px" SiColonneSelect="true" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreEntretien" runat="server" Height="50px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            CssClass="EP_TitreCR">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="LabelAnneeEntretien" runat="server" Text="Année 2011 - 2012" Height="20px" Width="200px"
                            CssClass="EP_TitreOnglet"
                            Style="margin-top: 10px; margin-bottom: 10px">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server"
                            V_PointdeVue="1" V_Objet="150" V_Information="0" V_SiDonneeDico="true"
                            EtiWidth="130px" DonWidth="80px" DonTabIndex="1" EtiText="Date de l'entretien" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>


                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left" BackColor ="#E9FDF9">
                        <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" Width="370px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelAgent" runat="server" Height="30px" Width="370px" Text="AGENT"
                                        CssClass="EP_TitreChapitre">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table3" runat="server" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label10" runat="server" Height="24px" Width="60px" Text="Prénom : "
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="LabelPrenom" runat="server" Height="24px" Width="310px" Text=""
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                               <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table4" runat="server" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label11" runat="server" Height="24px" Width="50px" Text="Nom : "
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>

                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelNom" runat="server" Height="24px" Width="320px" Text=""
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>


                                        </asp:TableRow>
                                    </asp:Table>
                               </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table5" runat="server" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label12" runat="server" Height="24px" Width="60px" Text="Groupe : "
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelGroupe" runat="server" Height="24px" Width="210px" Text=""
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                                                                        <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label16" runat="server" Height="24px" Width="50px" Text="Indice : "
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelIndice" runat="server" Height="24px" Width="50px" Text=""
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table6" runat="server" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label13" runat="server" Height="24px" Width="130px" Text="Service / Direction : "
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabeldirectionService" runat="server" Height="40px" Width="240px" Text=""
                                                    CssClass="EP_EtiComplementBis" Style ="text-indent: 0px;" >
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table7" runat="server" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label14" runat="server" Height="24px" Width="100px" Text="Poste occupé : "
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelPoste" runat="server" Height="24px" Width="270px" Text=""
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table8" runat="server" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label15" runat="server" Height="24px" Width="70px" Text="Depuis le : "
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelDepuisle" runat="server" Height="24px" Width="300px" Text=""
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" >
                        <asp:Label ID="Label8" runat="server" Height="40px" Width="5px" Text="">
                        </asp:Label>
                    </asp:TableCell>

                    <asp:TableCell HorizontalAlign="left" BackColor ="#E9FDF9">
                        <asp:Table ID="Table2" runat="server" CellPadding="0" CellSpacing="0" Width="370px" >
                            <asp:TableRow >
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label1" runat="server" Height="30px" Width="370px" Text="SUPÉRIEUR HIÉRARCHIQUE DIRECT"
                                        CssClass="EP_TitreChapitre">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table9" runat="server" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label9" runat="server" Height="24px" Width="60px" Text="Prénom : "
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="LabelPrenomChefService" runat="server" Height="24px" Width="310px" Text=""
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table10" runat="server" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label3" runat="server" Height="24px" Width="60px" Text="Nom : "
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="LabelNomChefService" runat="server" Height="24px" Width="310px" Text=""
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table11" runat="server" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label4" runat="server" Height="24px" Width="60px" Text="Statut : "
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="LabelStatutChefService" runat="server" Height="24px" Width="310px" Text=""
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Table ID="Table12" runat="server" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label5" runat="server" Height="24px" Width="120px" Text="Fonction exercée : "
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="LabelFocntionChefService" runat="server" Height="24px" Width="250px" Text=""
                                                    CssClass="EP_EtiComplementBis">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label6" runat="server" Height="40px" Width="370px" Text=""
                                        CssClass="EP_EtiComplementBis">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label7" runat="server" Height="24px" Width="370px" Text=""
                                        CssClass="EP_EtiComplementBis">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>





            </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreSignature" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                          <%--  <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="2" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                                        EtiWidth="180px" DonWidth="350px" DonTabIndex="2" EtiText="Service de l'évaluation" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                                        EtiWidth="180px" DonWidth="350px" DonTabIndex="3" EtiText="Nom de l'évaluateur" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true"
                                        EtiWidth="220px" DonWidth="80px" DonTabIndex="4" EtiText="Date de signature de l'évaluateur" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="3px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="LabelSignatureEvalue" runat="server" Height="18px" Width="750px"
                                        BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet"
                                        Text="L'agent reconnaît avoir pris connaissance du présent compte-rendu d'entretien"
                                        BorderWidth="1px" ForeColor="#2FA49B" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: 0px; margin-bottom: 5px; font-style: oblique; text-indent: 5px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                                        V_PointdeVue="1" V_Objet="150" V_Information="4" V_SiDonneeDico="true"
                                        EtiWidth="220px" DonWidth="80px" DonTabIndex="5" EtiText="Date de signature de l'agent"
                                        DonTooltip="Attention. Vous ne pourrez plus modifier ultérieurement le formulaire d'entretien" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="LabelMessage" runat="server" Height="18px" Width="750px"
                                        BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet"
                                        Text="La date de signature de l'agent rend non modifiable le formulaire d'entretien"
                                        BorderWidth="1px" ForeColor="#731E1E" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: 0px; margin-bottom: 5px; font-style: oblique; text-indent: 5px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>--%>

                            <asp:TableRow>
                                <asp:TableCell Height="10px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label2" runat="server" Height="18px" Width="650px"
                                        BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet"
                                        Text="En cas de double rattachement hiérarchique et fonctionnel, je suis d'accord pour être évalué(e) par mon supérieur fonctionnel contre signé par mon supérieur hiérarchique."
                                        BorderWidth="1px" ForeColor="#731E1E" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 1px; margin-left: 0px; margin-bottom: 5px; font-style: oblique; text-indent: 5px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="RadioHA04" runat="server" V_Groupe="Realisation1"
                                        V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                                        VRadioN1Width="100px" VRadioN2Width="100px" VRadioN3Width="100px"
                                        VRadioN4Width="100px" VRadioN5Width="100px" VRadioN6Width="100px"
                                        VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                        VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                        VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                        VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                                        VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                                        VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                        VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"
                                        Visible="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>



<%--                    </asp:TableCell>
        </asp:TableRow>
      </asp:Table>--%>