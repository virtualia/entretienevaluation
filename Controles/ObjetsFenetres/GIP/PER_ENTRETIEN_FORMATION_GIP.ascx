﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_FORMATION_GIP.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_FORMATION_GIP" %>

<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VSixBoutonRadio.ascx" TagName="VSixBoutonRadio" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VListeGrid.ascx" TagName="VListeGrid" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR {
        background-color: #216B68;
        color: #D7FAF3;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: bold;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 25px;
        width: 746px;
    }

    .EP_TitreOnglet {
        background-color: #E2F5F1;
        color: #0E5F5C;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: bold;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_TitreChapitre {
        background-color: #CAEBE4;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiComplement {
        background-color: #E2F5F1;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-align: left;
        text-indent: 5px;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiComplementBis {
        background-color: #E9FDF9;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-align: left;
        text-indent: 5px;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiComplementTer {
        background-color: #98D4CA;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-align: left;
        text-indent: 5px;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiTableau {
        background-color: #D7FAF3;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: normal;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_TitreParagraphe {
        background-color: #8DA8A3;
        color: white;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: normal;
        text-indent: 8px;
        text-align: left;
        padding-top: 6px;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .EP_EtiNiveau {
        background-color: white;
        color: #124545;
        border-style: solid;
        border-width: 1px;
        border-color: #9EB0AC;
        font-family: 'Trebuchet MS';
        font-size: x-small;
        font-style: italic;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 0px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" Visible="true"
    Width="750px" HorizontalAlign="Center" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreFormation" runat="server" Height="75px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            CssClass="EP_TitreCR">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <%--                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="V. FORMATION" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>--%>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Text="" Height="30px" Width="746px"
                            CssClass="EP_TitreChapitre">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Text="IV. Formations suivies par l'agent" Height="38px" Width="750px"
                            CssClass="EP_TitreParagraphe"
                            Style="padding-top: 4px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="746px" HorizontalAlign="Center">
                        <Virtualia:VListeGrid ID="ListeFormationsSuivies" runat="server" CadreWidth="740px" SiColonneSelect="false" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Text="V. Besoins de formation pour l'année" Height="38px" Width="750px"
                            CssClass="EP_TitreParagraphe"
                            Style="padding-top: 4px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>



                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreFormation" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                            BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table4" runat="server" CellPadding="0" CellSpacing="0" Width="232px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label3" runat="server" Height="35px" Width="232px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Libellés des formations </br> Formations liées à l'adaptation au poste"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="TableEnteteObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label1" runat="server" Height="35px" Width="55px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Demandé par l'agent"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="TableEnteteIndicateurs1" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="LabelEnteteIndicateurs1" runat="server" Height="35px" Width="55px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Demandé par le SHD"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="TableEnteteRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="103px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="LabelEnteteRealisation1" runat="server" Height="35px" Width="103px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Echéance"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table2" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label2" runat="server" Height="35px" Width="55px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Utilisation CPF"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table3" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label4" runat="server" Height="35px" Width="55px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Priorité 1,2,3"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table6" runat="server" CellPadding="0" CellSpacing="0" Width="192px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label5" runat="server" Height="35px" Width="192px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Attendus de la formation"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                 <%--          ************************ Données****************************** --%>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                  <asp:Table ID="Table5" runat="server" CellPadding="0" CellSpacing="0" Width="230px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                      <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="left"
                                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee1" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="228px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" Width="55px" 
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" >
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="TableIndicateur1" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table7" runat="server" CellPadding="0" CellSpacing="0" Width="100px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center"
                                                BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHA01" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="98px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table8" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table9" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:DropDownList runat="server" Height="25px" Width="50px">
                                                                               <asp:ListItem Selected="True" Value="">  </asp:ListItem>
                                       <asp:ListItem Value="1"> 1 </asp:ListItem>
                                      <asp:ListItem Value="2"> 2 </asp:ListItem>
                                      <asp:ListItem Value="3"> 3 </asp:ListItem>
                                  </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table10" runat="server" CellPadding="0" CellSpacing="0" Width="190px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center"
                                                BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee2" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="188px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                                                        <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                  <asp:Table ID="Table11" runat="server" CellPadding="0" CellSpacing="0" Width="230px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                      <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="left"
                                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee3" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="228px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table12" runat="server" CellPadding="0" CellSpacing="0" Width="55px" 
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" >
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table13" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table14" runat="server" CellPadding="0" CellSpacing="0" Width="100px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center"
                                                BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee4" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="98px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table15" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table16" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:DropDownList runat="server" Height="25px" Width="50px">
                                                                               <asp:ListItem Selected="True" Value="">  </asp:ListItem>
                                                                             <asp:ListItem Value="1"> 1 </asp:ListItem>
                                      <asp:ListItem Value="2"> 2 </asp:ListItem>
                                      <asp:ListItem Value="3"> 3 </asp:ListItem>
                                  </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table17" runat="server" CellPadding="0" CellSpacing="0" Width="190px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center"
                                                BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee5" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="188px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                                              <asp:TableRow>
                                <asp:TableCell Height="10px" BackColor="#8DA8A3" ></asp:TableCell>
                            </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Table18" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                            BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table19" runat="server" CellPadding="0" CellSpacing="0" Width="232px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label6" runat="server" Height="35px" Width="232px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Libellés des formations </br> Formations liées à l'évolution au poste"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table20" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label7" runat="server" Height="35px" Width="55px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Demandé par l'agent"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table21" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label8" runat="server" Height="35px" Width="55px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Demandé par le SHD"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table22" runat="server" CellPadding="0" CellSpacing="0" Width="103px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label9" runat="server" Height="35px" Width="103px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Echéance"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table23" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label10" runat="server" Height="35px" Width="55px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Utilisation CPF"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table24" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label11" runat="server" Height="35px" Width="55px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Priorité 1,2,3"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table25" runat="server" CellPadding="0" CellSpacing="0" Width="192px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label12" runat="server" Height="35px" Width="192px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Attendus de la formation"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                 <%--          ************************ Données****************************** --%>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                  <asp:Table ID="Table26" runat="server" CellPadding="0" CellSpacing="0" Width="230px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                      <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="left"
                                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee6" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="228px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table27" runat="server" CellPadding="0" CellSpacing="0" Width="55px" 
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" >
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table28" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table29" runat="server" CellPadding="0" CellSpacing="0" Width="100px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center"
                                                BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee7" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="98px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table30" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table31" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:DropDownList runat="server" Height="25px" Width="50px">
                                                                               <asp:ListItem Selected="True" Value="">  </asp:ListItem>
                                                                             <asp:ListItem Value="1"> 1 </asp:ListItem>
                                      <asp:ListItem Value="2"> 2 </asp:ListItem>
                                      <asp:ListItem Value="3"> 3 </asp:ListItem>
                                  </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table32" runat="server" CellPadding="0" CellSpacing="0" Width="190px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center"
                                                BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee8" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="188px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                                                        <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                  <asp:Table ID="Table33" runat="server" CellPadding="0" CellSpacing="0" Width="230px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                      <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="left"
                                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee9" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="228px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table34" runat="server" CellPadding="0" CellSpacing="0" Width="55px" 
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" >
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table35" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table36" runat="server" CellPadding="0" CellSpacing="0" Width="100px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center"
                                                BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee10" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="98px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table37" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table38" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:DropDownList runat="server" Height="25px" Width="50px">
                                         <asp:ListItem Selected="True" Value="">  </asp:ListItem>
                                       <asp:ListItem Value="1"> 1 </asp:ListItem>
                                      <asp:ListItem Value="2"> 2 </asp:ListItem>
                                      <asp:ListItem Value="3"> 3 </asp:ListItem>
                                  </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table39" runat="server" CellPadding="0" CellSpacing="0" Width="190px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center"
                                                BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee11" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="188px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                              <asp:TableRow>
                                <asp:TableCell Height="10px" BackColor="#8DA8A3" ></asp:TableCell>
                            </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Table40" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                            BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table41" runat="server" CellPadding="0" CellSpacing="0" Width="232px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label13" runat="server" Height="35px" Width="232px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Libellés des formations </br> Autres actions (VAE, bilan de compétence...)"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table42" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label14" runat="server" Height="35px" Width="55px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Demandé par l'agent"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table43" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label15" runat="server" Height="35px" Width="55px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Demandé par le SHD"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table44" runat="server" CellPadding="0" CellSpacing="0" Width="103px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label16" runat="server" Height="35px" Width="103px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Echéance"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table45" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label17" runat="server" Height="35px" Width="55px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Utilisation CPF"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table46" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label18" runat="server" Height="35px" Width="55px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Priorité 1,2,3"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table47" runat="server" CellPadding="0" CellSpacing="0" Width="192px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="Label19" runat="server" Height="35px" Width="192px"
                                                    BackColor="#D7FAF3" BorderStyle="None"
                                                    Text="Attendus de la formation"
                                                    ForeColor="#124545" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                                    Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                 <%--          ************************ Données****************************** --%>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                  <asp:Table ID="Table48" runat="server" CellPadding="0" CellSpacing="0" Width="230px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                      <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="left"
                                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee12" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="228px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 1px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table49" runat="server" CellPadding="0" CellSpacing="0" Width="55px" 
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" >
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table50" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table51" runat="server" CellPadding="0" CellSpacing="0" Width="100px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center"
                                                BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee13" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="98px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table52" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:CheckBox runat="server" Height="25px"></asp:CheckBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table53" runat="server" CellPadding="0" CellSpacing="0" Width="55px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                  <asp:DropDownList runat="server" Height="25px" Width="50px">
                                      <asp:ListItem Selected="True" Value="">  </asp:ListItem>
                                       <asp:ListItem Value="1"> 1 </asp:ListItem>
                                      <asp:ListItem Value="2"> 2 </asp:ListItem>
                                      <asp:ListItem Value="3"> 3 </asp:ListItem>
                                  </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="Table54" runat="server" CellPadding="0" CellSpacing="0" Width="190px"
                                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center"
                                                BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                                <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee14" runat="server"
                                                    V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                                    EtiVisible="False" DonWidth="188px" DonHeight="20px"
                                                    DonText="" V_SiEnLectureSeule="false"
                                                    DonStyle="margin-left: -5px; margin-top: 0px; text-indent: 2px; text-align: left" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>

            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
