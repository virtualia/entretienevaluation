﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE_GIP
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            Select Case NumInfo
                Case 1
                    VirVertical.V_SiEnLectureSeule = True
                Case Else
                    VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            End Select
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VSixBoutonRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VSixBoutonRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.V_SiAutoPostBack = Not (WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1)))

            Select Case NumObjet
                Case 153
                    If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                        Select Case ValeurLue(NumObjet, NumInfo, Rang)
                            Case Is = "6"
                                VirRadio.VRadioN1Check = True
                            Case Is = "5"
                                VirRadio.VRadioN2Check = True
                            Case Is = "4"
                                VirRadio.VRadioN3Check = True
                            Case Is = "3"
                                VirRadio.VRadioN4Check = True
                            Case Is = "2"
                                VirRadio.VRadioN5Check = True
                            Case Is = "1"
                                VirRadio.VRadioN6Check = True
                            Case Else
                                VirRadio.VRadioN1Check = False
                                VirRadio.VRadioN2Check = False
                                VirRadio.VRadioN3Check = False
                                VirRadio.VRadioN4Check = False
                                VirRadio.VRadioN5Check = False
                                VirRadio.VRadioN6Check = False
                        End Select
                    End If
                Case 156
                    If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                        Select Case ValeurLue(NumObjet, NumInfo, Rang)
                            Case Is = "3"
                                VirRadio.VRadioN1Check = True
                            Case Is = "2"
                                VirRadio.VRadioN2Check = True
                            Case Is = "1"
                                VirRadio.VRadioN3Check = True
                            Case Else
                                VirRadio.VRadioN1Check = False
                                VirRadio.VRadioN2Check = False
                                VirRadio.VRadioN3Check = False
                        End Select
                    End If
            End Select
            IndiceI += 1
        Loop
    End Sub

    'Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) _
    'Handles InfoVA01.ValeurChange, InfoVB01.ValeurChange, InfoVC01.ValeurChange, InfoVD01.ValeurChange, InfoVE01.ValeurChange, InfoVF01.ValeurChange,
    'InfoVI01.ValeurChange, InfoVJ01.ValeurChange, InfoVK01.ValeurChange, InfoVL01.ValeurChange, InfoVM01.ValeurChange, InfoVN01.ValeurChange,
    'InfoVT01.ValeurChange, InfoVU01.ValeurChange, InfoVV01.ValeurChange, InfoVW01.ValeurChange, InfoVX01.ValeurChange, InfoVY01.ValeurChange,
    'InfoVQ01.ValeurChange, InfoVR01.ValeurChange, InfoVZ01.ValeurChange

    '    WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
    '    If WsDossierPER Is Nothing Then
    '        Exit Sub
    '    End If
    '    Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
    '    Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet
    '    Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1))

    '    Select Case NumObjet
    '        Case 153
    '            If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
    '                If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
    '                    WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
    '                End If
    '            End If
    '    End Select
    'End Sub

    'Protected Sub Renitialisation(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnRenitialisation.Click
    '    WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
    '    If WsDossierPER Is Nothing Then
    '        Exit Sub
    '    End If
    '    Dim NumInfo As Integer
    '    Dim NumObjet As Integer
    '    Dim Rang As String
    '    Dim Eval As Integer = 0

    '    NumObjet = 153
    '    NumInfo = 7
    '    Rang = "Conduite et animation d'équipe"
    '    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
    '        If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> CStr(Eval) Then
    '            WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = CStr(Eval)
    '        End If
    '    End If

    '    Rang = "Capacité d'écoute et de négociations"
    '    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
    '        If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> CStr(Eval) Then
    '            WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = CStr(Eval)
    '        End If
    '    End If

    '    Rang = "Capacité à déléguer et à contrôler"
    '    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
    '        If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> CStr(Eval) Then
    '            WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = CStr(Eval)
    '        End If
    '    End If

    '    Rang = "Capacité à organiser le service"
    '    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
    '        If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> CStr(Eval) Then
    '            WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = CStr(Eval)
    '        End If
    '    End If

    '    Rang = "Aptitude à la prise de décision"
    '    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
    '        If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> CStr(Eval) Then
    '            WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = CStr(Eval)
    '        End If
    '    End If

    '    Rang = "Aptitude à former des collaborateurs"
    '    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
    '        If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> CStr(Eval) Then
    '            WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = CStr(Eval)
    '        End If
    '    End If

    '    Rang = "Aptitude à la prise de décision"
    '    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
    '        If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> CStr(Eval) Then
    '            WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = CStr(Eval)
    '        End If
    '    End If


    '    NumObjet = 156
    '    NumInfo = 3
    '    Rang = "Capacités d'encadrement"
    '    If WsDossierPER.Objet_156(Rang) IsNot Nothing Then
    '        If WsDossierPER.Objet_156(Rang).V_TableauData(NumInfo).ToString <> CStr(Eval) Then
    '            WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = CStr(Eval)
    '            WsDossierPER.TableauMaj(NumObjet, NumInfo + 1, Rang) = ""
    '        End If
    '    End If
    'End Sub
    'Protected Sub RadioValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) _
    '    Handles RadioHA07.ValeurChange, RadioHB07.ValeurChange, RadioHC07.ValeurChange, RadioHD07.ValeurChange, RadioHE07.ValeurChange, RadioHF07.ValeurChange,
    '    RadioHI07.ValeurChange, RadioHJ07.ValeurChange, RadioHK07.ValeurChange, RadioHL07.ValeurChange, RadioHM07.ValeurChange, RadioHN07.ValeurChange,
    '    RadioHQ07.ValeurChange, RadioHR07.ValeurChange,
    '    RadioHT07.ValeurChange, RadioHU07.ValeurChange, RadioHV07.ValeurChange, RadioHW07.ValeurChange, RadioHX07.ValeurChange, RadioHY07.ValeurChange, RadioHZ07.ValeurChange,
    '    RadioHA03.ValeurChange, RadioHB03.ValeurChange, RadioHC03.ValeurChange, RadioHD03.ValeurChange, RadioHE03.ValeurChange

    '    WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
    '    If WsDossierPER Is Nothing Then
    '        Exit Sub
    '    End If
    '    Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VSixBoutonRadio).ID, 2))
    '    Dim NumObjet As Integer = CType(sender, VirtualiaControle_VSixBoutonRadio).V_Objet
    '    Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VSixBoutonRadio).ID, 7, 1))
    '    Dim Eval As Integer = 0

    '    Select Case NumObjet
    '        Case 153
    '            If IsNumeric(e.Valeur) Then
    '                Eval = 6 - CInt(e.Valeur)
    '            End If
    '            If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
    '                If IsNumeric(e.Valeur) Then
    '                    If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> CStr(Eval) Then
    '                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = CStr(Eval)
    '                    End If
    '                End If
    '            End If
    '            Select Case RadioHZ07.VRadioN1Check
    '                Case True
    '                    LabelTitreNote71.BackColor = V_WebFonction.ConvertCouleur("#216B68")
    '                    LabelTitreNote71.ForeColor = Drawing.Color.White
    '                Case False
    '                    LabelTitreNote71.BackColor = Drawing.Color.White
    '                    LabelTitreNote71.ForeColor = V_WebFonction.ConvertCouleur("#124545")
    '            End Select
    '            Select Case RadioHZ07.VRadioN2Check
    '                Case True
    '                    LabelTitreNote72.BackColor = V_WebFonction.ConvertCouleur("#216B68")
    '                    LabelTitreNote72.ForeColor = Drawing.Color.White
    '                Case False
    '                    LabelTitreNote72.BackColor = Drawing.Color.White
    '                    LabelTitreNote72.ForeColor = V_WebFonction.ConvertCouleur("#124545")
    '            End Select
    '            Select Case RadioHZ07.VRadioN3Check
    '                Case True
    '                    LabelTitreNote73.BackColor = V_WebFonction.ConvertCouleur("#216B68")
    '                    LabelTitreNote73.ForeColor = Drawing.Color.White
    '                Case False
    '                    LabelTitreNote73.BackColor = Drawing.Color.White
    '                    LabelTitreNote73.ForeColor = V_WebFonction.ConvertCouleur("#124545")
    '            End Select
    '            Select Case RadioHZ07.VRadioN4Check
    '                Case True
    '                    LabelTitreNote74.BackColor = V_WebFonction.ConvertCouleur("#216B68")
    '                    LabelTitreNote74.ForeColor = Drawing.Color.White
    '                Case False
    '                    LabelTitreNote74.BackColor = Drawing.Color.White
    '                    LabelTitreNote74.ForeColor = V_WebFonction.ConvertCouleur("#124545")
    '            End Select
    '            Select Case RadioHZ07.VRadioN5Check
    '                Case True
    '                    LabelTitreNote75.BackColor = V_WebFonction.ConvertCouleur("#216B68")
    '                    LabelTitreNote75.ForeColor = Drawing.Color.White
    '                Case False
    '                    LabelTitreNote75.BackColor = Drawing.Color.White
    '                    LabelTitreNote75.ForeColor = V_WebFonction.ConvertCouleur("#124545")
    '            End Select
    '            Select Case RadioHZ07.VRadioN6Check
    '                Case True
    '                    LabelTitreNote76.BackColor = V_WebFonction.ConvertCouleur("#216B68")
    '                    LabelTitreNote76.ForeColor = Drawing.Color.White
    '                Case False
    '                    LabelTitreNote76.BackColor = Drawing.Color.White
    '                    LabelTitreNote76.ForeColor = V_WebFonction.ConvertCouleur("#124545")
    '            End Select
    '        Case 156
    '            If IsNumeric(e.Parametre) Then
    '                Eval = 3 - CInt(e.Parametre)
    '            End If
    '            If WsDossierPER.Objet_156(Rang) IsNot Nothing Then
    '                If WsDossierPER.Objet_156(Rang).V_TableauData(NumInfo).ToString <> CStr(Eval) Then
    '                    WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = CStr(Eval)
    '                    WsDossierPER.TableauMaj(NumObjet, NumInfo + 1, Rang) = e.Valeur
    '                End If
    '            End If
    '    End Select
    'End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        'Etiquette.Text = "COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL - Année " & WsDossierPER.Annee
        Etiquette.Text = "III)ÉVALUATION DES COMPÉTENCES REQUISES POUR LE POSTE (Cf. emploi-type)"
        'LabelIdentite.Text = WsDossierPER.LibelleIdentite

        Call LireLaFiche()

    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 153
                    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                        If WsDossierPER.Objet_153(Rang).Date_Valeur_ToDate = WsDossierPER.Objet_150.Date_Valeur_ToDate Then
                            Return WsDossierPER.Objet_153(Rang).V_TableauData(NoInfo).ToString
                        End If
                    End If
                Case 156
                    If WsDossierPER.Objet_156(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_156(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property

End Class