﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class VEntretienGIP
        Implements Virtualia.Net.Entretien.IGenericEntretien
        Private WsLstCompetences As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstObservations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstFormations As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstSyntheses As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing
        Private WsLstComplements As List(Of Virtualia.Net.Entretien.ObjetTable) = Nothing

        Public ReadOnly Property LitteralCompetence(Critere As Integer, Optional SiPrecision As Boolean = False) As String Implements IGenericEntretien.LitteralCompetence
            Get
                Select Case Critere
                    Case 1
                        Return "Très insuffisant"
                    Case 2
                        Return "Insuffisant"
                    Case 3
                        Return "Moyen"
                    Case 4
                        Return "Bon"
                    Case 5
                        Return "Très bon"
                    Case 6
                        Return "Excellent"
                    Case Else
                        Return "Sans objet"
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property LitteralResultat(Critere As Integer) As String Implements IGenericEntretien.LitteralResultat
            Get
                Select Case Critere
                    Case 0
                        Return "Devenu sans objet"
                    Case 1
                        Return "Non atteint"
                    Case 2
                        Return "Partiellement atteint"
                    Case 3
                        Return "Atteint"
                    Case Else
                        Return "Devenu sans objet"
                End Select
            End Get
        End Property

        Public ReadOnly Property LitteralSynthese(Critere As Integer) As String Implements IGenericEntretien.LitteralSynthese
            Get
                Select Case Critere
                    Case 1
                        Return "A améliorer"
                    Case 2
                        Return "Constant"
                    Case 3
                        Return "En progrès"
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property Table_Competences As List(Of ObjetTable) Implements IGenericEntretien.Table_Competences
            Get
                If WsLstCompetences IsNot Nothing Then
                    Return WsLstCompetences
                End If

                Dim IndiceFam As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstCompetences = New List(Of Virtualia.Net.Entretien.ObjetTable)

                For IndiceFam = 0 To 20
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Clef_Index = IndiceFam + 1 'Numero de Tri
                    Select Case IndiceFam
                        Case 0 To 5
                            ItemTable.Rang_Famille = "2.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Savoir-Faire"
                            ItemTable.Rang_SousFamille = "2.1.1" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Compétences professionnelles"
                            Select Case IndiceFam
                                Case 0
                                    ItemTable.Intitule = "Connaissances techniques spécifiques à la fonction"
                                    ItemTable.Clef_Origine = "A"
                                Case 1
                                    ItemTable.Intitule = "Connaissance de l'environnement professionnel"
                                    ItemTable.Clef_Origine = "B"
                                Case 2
                                    ItemTable.Intitule = "Aptitude à actualiser et à perfectionner ses connaissances et ses méthodes de travail"
                                    ItemTable.Clef_Origine = "C"
                                Case 3
                                    ItemTable.Intitule = "Expression écrite et orale"
                                    ItemTable.Clef_Origine = "D"
                                Case 4
                                    ItemTable.Intitule = "Maîtrise et adaptabilité aux nouvelles technologies"
                                    ItemTable.Clef_Origine = "E"
                                Case 5
                                    ItemTable.Intitule = "Qualités d'analyse et de synthèse"
                                    ItemTable.Clef_Origine = "F"
                            End Select
                        Case 6 To 11
                            ItemTable.Rang_Famille = "2.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Savoir-Faire"
                            ItemTable.Rang_SousFamille = "2.1.2" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Aptitudes professionnelles et efficacité dans l'emploi"
                            Select Case IndiceFam
                                Case 6
                                    ItemTable.Intitule = "Sens du service public"
                                    ItemTable.Clef_Origine = "I"
                                Case 7
                                    ItemTable.Intitule = "Conscience professionnelle et sens des responsabilités"
                                    ItemTable.Clef_Origine = "J"
                                Case 8
                                    ItemTable.Intitule = "Esprit d'initiative et dynamisme"
                                    ItemTable.Clef_Origine = "K"
                                Case 9
                                    ItemTable.Intitule = "Capacité d'adaptation aux changements et d'anticipation"
                                    ItemTable.Clef_Origine = "L"
                                Case 10
                                    ItemTable.Intitule = "Capacité de travail"
                                    ItemTable.Clef_Origine = "M"
                                Case 11
                                    ItemTable.Intitule = "Qualité du travail fourni"
                                    ItemTable.Clef_Origine = "N"
                            End Select
                        Case 12 To 13
                            ItemTable.Rang_Famille = "2.1" 'Numero de famille
                            ItemTable.Libelle_Famille = "Savoir-Faire"
                            ItemTable.Rang_SousFamille = "2.1.3" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Qualités et capacités relationnelles"
                            Select Case IndiceFam
                                Case 12
                                    ItemTable.Intitule = "Capacité de travail en équipe"
                                    ItemTable.Clef_Origine = "Q"
                                Case 13
                                    ItemTable.Intitule = "Sens des relations professionnelles"
                                    ItemTable.Clef_Origine = "R"
                            End Select
                        Case 14 To 19
                            ItemTable.Rang_Famille = "2.2" 'Numero de famille
                            ItemTable.Libelle_Famille = "Encadrement"
                            ItemTable.Rang_SousFamille = "2.2.1" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Capacités d'encadrement"
                            Select Case IndiceFam
                                Case 14
                                    ItemTable.Intitule = "Conduite et animation d'équipe"
                                    ItemTable.Clef_Origine = "T"
                                Case 15
                                    ItemTable.Intitule = "Capacité d'écoute et de négociations"
                                    ItemTable.Clef_Origine = "U"
                                Case 16
                                    ItemTable.Intitule = "Capacité à déléguer et à contrôler"
                                    ItemTable.Clef_Origine = "V"
                                Case 17
                                    ItemTable.Intitule = "Capacité à organiser le service"
                                    ItemTable.Clef_Origine = "W"
                                Case 18
                                    ItemTable.Intitule = "Aptitude à la prise de décision"
                                    ItemTable.Clef_Origine = "X"
                                Case 19
                                    ItemTable.Intitule = "Aptitude à former des collaborateurs"
                                    ItemTable.Clef_Origine = "Y"
                            End Select
                        Case 20
                            ItemTable.Rang_Famille = "2.3" 'Numero de famille
                            ItemTable.Libelle_Famille = "Niveau global"
                            ItemTable.Rang_SousFamille = "2.3.1" 'Numero de sous-famille
                            ItemTable.Libelle_SousFamille = "Niveau global de performance"
                            ItemTable.Intitule = "Niveau global de performance"
                            ItemTable.Clef_Origine = "Z"
                    End Select
                    WsLstCompetences.Add(ItemTable)
                Next IndiceFam
                Return WsLstCompetences
            End Get
        End Property

        Public ReadOnly Property Table_Formations As List(Of ObjetTable) Implements IGenericEntretien.Table_Formations
            Get
                If WsLstFormations IsNot Nothing Then
                    Return WsLstFormations
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstFormations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                For IndiceTri = 1 To 5
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Intitule = ""
                    ItemTable.Libelle_Famille = "Formations sollicitées et non suivies"
                    ItemTable.Rang_Famille = "2 - Formations sollicitées et non suivies"
                    Select Case IndiceTri
                        Case 1
                            ItemTable.Clef_Origine = "A"
                        Case 2
                            ItemTable.Clef_Origine = "B"
                        Case 3
                            ItemTable.Clef_Origine = "C"
                        Case 4
                            ItemTable.Clef_Origine = "D"
                        Case 5
                            ItemTable.Clef_Origine = "E"
                    End Select
                    WsLstFormations.Add(ItemTable)
                Next IndiceTri

                For IndiceTri = 1 To 5
                    ItemTable = New Virtualia.Net.Entretien.ObjetTable
                    ItemTable.Intitule = ""
                    ItemTable.Libelle_Famille = "Formations envisagées"
                    ItemTable.Rang_Famille = "3 - Formations envisagées"
                    Select Case IndiceTri
                        Case 1
                            ItemTable.Clef_Origine = "H"
                        Case 2
                            ItemTable.Clef_Origine = "I"
                        Case 3
                            ItemTable.Clef_Origine = "J"
                        Case 4
                            ItemTable.Clef_Origine = "K"
                        Case 5
                            ItemTable.Clef_Origine = "L"
                    End Select
                    WsLstFormations.Add(ItemTable)
                Next IndiceTri

                For IndiceTri = 0 To WsLstFormations.Count - 1
                    WsLstFormations.Item(IndiceTri).Clef_Index = IndiceTri + 1
                Next IndiceTri

                Return WsLstFormations
            End Get
        End Property

        Public ReadOnly Property Table_Observations As List(Of ObjetTable) Implements IGenericEntretien.Table_Observations
            Get
                If WsLstObservations IsNot Nothing Then
                    Return WsLstObservations
                End If

                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstObservations = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Motif en cas d'absence d'entretien professionnel"
                ItemTable.Rang_Famille = "0.1"
                ItemTable.Clef_Origine = "A"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Bilan des objectifs - Synthèse du supérieur hiérarchique"
                ItemTable.Rang_Famille = "1.1"
                ItemTable.Clef_Origine = "G"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Bilan des objectifs - Synthèse de l'agent"
                ItemTable.Rang_Famille = "1.2"
                ItemTable.Clef_Origine = "H"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Appréciation - Compétences professionnelles"
                ItemTable.Rang_Famille = "3.1"
                ItemTable.Clef_Origine = "I"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Appréciation - Aptitudes professionnelles et efficacité dans l'emploi"
                ItemTable.Rang_Famille = "3.2"
                ItemTable.Clef_Origine = "J"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Appréciation - Qualités et capacités relationnelles"
                ItemTable.Rang_Famille = "3.3"
                ItemTable.Clef_Origine = "K"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Appréciation - Capacités d'encadrement"
                ItemTable.Rang_Famille = "3.4"
                ItemTable.Clef_Origine = "L"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l'autorité hiérarchique"
                ItemTable.Rang_Famille = "8.3"
                ItemTable.Clef_Origine = "Y"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l'agent sur le déroulement de l'entretien et les thèmes abordés"
                ItemTable.Rang_Famille = "8.2.1"
                ItemTable.Clef_Origine = "W"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations de l'agent sur les appréciations portées"
                ItemTable.Rang_Famille = "8.2.2"
                ItemTable.Clef_Origine = "X"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Motif du recours invoqué par l'agent"
                ItemTable.Rang_Famille = "9.1"
                ItemTable.Clef_Origine = "Z"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Principaux objectifs du service pour l'année à venir"
                ItemTable.Rang_Famille = "4.1"
                ItemTable.Clef_Origine = "M"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Perspectives d'évolution des conditions d'organisation et de fonctionnement du service"
                ItemTable.Rang_Famille = "4.3"
                ItemTable.Clef_Origine = "N"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Evolution envisagée des fonctions exercées"
                ItemTable.Rang_Famille = "4.4"
                ItemTable.Clef_Origine = "O"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Observations sur les formations"
                ItemTable.Rang_Famille = "5.1"
                ItemTable.Clef_Origine = "P"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Compétences acquises - Connaissance de l'institution et de son organisation"
                ItemTable.Rang_Famille = "6.1"
                ItemTable.Clef_Origine = "Q"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Compétences acquises - Connaissances professionnelles"
                ItemTable.Rang_Famille = "6.2"
                ItemTable.Clef_Origine = "R"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Compétences acquises - Expérience à l'encadrement"
                ItemTable.Rang_Famille = "6.3"
                ItemTable.Clef_Origine = "S"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Formations liées à l'emploi suivies"
                ItemTable.Rang_Famille = "6.4"
                ItemTable.Clef_Origine = "T"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Capacité à remplir les fonctions d'un grade ou corps supérieur"
                ItemTable.Rang_Famille = "7.1"
                ItemTable.Clef_Origine = "U"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Mobilité fonctionnelle ou géographique envisagée"
                ItemTable.Rang_Famille = "7.2"
                ItemTable.Clef_Origine = "V"
                WsLstObservations.Add(ItemTable)

                ''****** AKR 31/058/2017

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Activités principales"
                ItemTable.Rang_Famille = "8.1"
                ItemTable.Clef_Origine = "B"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Compétences"
                ItemTable.Rang_Famille = "8.2"
                ItemTable.Clef_Origine = "C"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Conditions et moyens d'exercice"
                ItemTable.Rang_Famille = "8.3.1"
                ItemTable.Clef_Origine = "D"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Composition du service"
                ItemTable.Rang_Famille = "8.4"
                ItemTable.Clef_Origine = "E"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Lieu de travail"
                ItemTable.Rang_Famille = "8.5"
                ItemTable.Clef_Origine = "F"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Niveau catégoriel du poste"
                ItemTable.Rang_Famille = "8.6"
                ItemTable.Clef_Origine = "1"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Liens hiérarchiques"
                ItemTable.Rang_Famille = "8.7"
                ItemTable.Clef_Origine = "2"
                WsLstObservations.Add(ItemTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Liens fonctionnels et relationnels"
                ItemTable.Rang_Famille = "8.8"
                ItemTable.Clef_Origine = "3"
                WsLstObservations.Add(ItemTable)

                Return WsLstObservations
            End Get
        End Property

        Public ReadOnly Property Table_Syntheses As List(Of ObjetTable) Implements IGenericEntretien.Table_Syntheses
            Get
                If WsLstSyntheses IsNot Nothing Then
                    Return WsLstSyntheses
                End If

                Dim IndiceTri As Integer
                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstSyntheses = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Appréciation de l'évaluateur"
                ItemTable.Rang_Famille = "2.1"
                ItemTable.Clef_Origine = "A"
                WsLstSyntheses.Add(ItemTable)

                'ItemTable = New Virtualia.Net.Entretien.ObjetTable
                'ItemTable.Intitule = "Aptitudes professionnelles et efficacité dans l'emploi"
                'ItemTable.Rang_Famille = "2.2"
                'ItemTable.Clef_Origine = "B"
                'WsLstSyntheses.Add(ItemTable)

                'ItemTable = New Virtualia.Net.Entretien.ObjetTable
                'ItemTable.Intitule = "Qualités et capacités relationnelles"
                'ItemTable.Rang_Famille = "2.3"
                'ItemTable.Clef_Origine = "C"
                'WsLstSyntheses.Add(ItemTable)

                'ItemTable = New Virtualia.Net.Entretien.ObjetTable
                'ItemTable.Intitule = "Capacités d'encadrement"
                'ItemTable.Rang_Famille = "2.4"
                'ItemTable.Clef_Origine = "D"
                'WsLstSyntheses.Add(ItemTable)

                'ItemTable = New Virtualia.Net.Entretien.ObjetTable
                'ItemTable.Intitule = "Marge d'évolution globale"
                'ItemTable.Rang_Famille = "2.5"
                'ItemTable.Clef_Origine = "E"
                'WsLstSyntheses.Add(ItemTable)

                For IndiceTri = 0 To WsLstSyntheses.Count - 1
                    WsLstSyntheses.Item(IndiceTri).Clef_Index = IndiceTri + 1
                Next IndiceTri
                Return WsLstSyntheses
            End Get
        End Property

        Public ReadOnly Property Table_Complements As List(Of ObjetTable) Implements IGenericEntretien.Table_Complements
            Get
                If WsLstComplements IsNot Nothing Then
                    Return WsLstComplements
                End If

                Dim ItemTable As Virtualia.Net.Entretien.ObjetTable

                WsLstComplements = New List(Of Virtualia.Net.Entretien.ObjetTable)

                ItemTable = New Virtualia.Net.Entretien.ObjetTable
                ItemTable.Intitule = "Accord pour être évalué par le supérieur fonctionnel"
                ItemTable.Clef_Index = 1
                ItemTable.Clef_Origine = "A"
                ItemTable.Libelle_Famille = CStr(VI.NatureDonnee.DonneeBooleenne)
                WsLstComplements.Add(ItemTable)

                Return WsLstComplements
            End Get
        End Property

    End Class
End Namespace