﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_RESULTATS_ENM
    
    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreTitreResultats.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitreResultats As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelVolet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVolet As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelIdentite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreNumero1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle TableEnteteObjectif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableEnteteObjectif As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelEnteteObjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteObjectif1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableEnteteRealisation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableEnteteRealisation As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelEnteteObjectif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteObjectif As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelEnteteComplete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteComplete As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelEntetePartielle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEntetePartielle As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelEnteteInsuffisante.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteInsuffisante As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LabelEnteteSansObjet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteSansObjet As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableEnteteObservations.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableEnteteObservations As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelEnteteObservations.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteObservations As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableNumerobjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableNumerobjectif1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelNumerobjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNumerobjectif1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableObjectif1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableObjectif1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVA02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVA03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableRealisation1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableRealisation1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle RadioHA04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHA04 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle InfoHA05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHA05 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle TableDifficultes1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDifficultes1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVA06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA06 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVA07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableNumerobjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableNumerobjectif2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelNumerobjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNumerobjectif2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableObjectif2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableObjectif2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVB02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVB03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableRealisation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableRealisation2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle RadioHB04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHB04 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle InfoHB05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHB05 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle TableDifficultes2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDifficultes2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVB06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB06 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVB07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableNumerobjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableNumerobjectif3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelNumerobjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNumerobjectif3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableObjectif3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableObjectif3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVC02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVC03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableRealisation3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableRealisation3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle RadioHC04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHC04 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle InfoHC05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHC05 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle TableDifficultes3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDifficultes3 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVC06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC06 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVC07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableNumerobjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableNumerobjectif4 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelNumerobjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNumerobjectif4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableObjectif4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableObjectif4 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVD02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVD02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVD03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVD03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableRealisation4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableRealisation4 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle RadioHD04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHD04 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle InfoHD05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHD05 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle TableDifficultes4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDifficultes4 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVD06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVD06 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVD07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVD07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableNumerobjectif5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableNumerobjectif5 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelNumerobjectif5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNumerobjectif5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableObjectif5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableObjectif5 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVE02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVE02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVE03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVE03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle TableRealisation5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableRealisation5 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle RadioHE04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHE04 As Global.Virtualia.Net.VirtualiaControle_VSixBoutonRadio
    
    '''<summary>
    '''Contrôle InfoHE05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHE05 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle TableDifficultes5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDifficultes5 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoVE06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVE06 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVE07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVE07 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle CadreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero2 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelTitreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoVG03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVG03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle InfoVH03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVH03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
End Class
