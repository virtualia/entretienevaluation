﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_APPRECIATION_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_APPRECIATION_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreAppreciation" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="III. APPRECIATION GENERALE DE LA VALEUR PROFESSIONNELLE" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="25px" Width="748px"
                           Text="APPRECIATION LITTERALE DE LA VALEUR PROFESSIONNELLE DE L'AGENT"
                           CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="70px" Width="748px"
                           Text="L'appréciation littérale de la valeur professionnelle de l'agent 
                           doit être en parfaite harmonie avec la qualification du niveau global de l'agent
                           tel qu'il apparaît dans la grille des critères d'appréciation et d'évaluation 
                           propre au corps d'appartenance de l'agent. Elle résulte de la qualification 
                           des critères d'appréciation ci-dessous (critères à caractères généraux, 
                           critères propres aux fonctions exercées et critères relatifs aux fonctions d'encadrement exercées)."
                           CssClass="EP_EtiComplement" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero21" runat="server" Height="20px" Width="746px"
                            Text="Appréciation littérale du supérieur hiérarchique direct" 
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero22" runat="server" Height="25px" Width="748px"
                           Text="1 - Compétences professionnelles"
                           CssClass="EP_TitreChapitre" style="padding-top: 7px; text-indent: 3px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           DonWidth="745px" DonHeight="160px" DonTabIndex="1" 
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"
                           Etivisible="False" Etitext="" EtiWidth="740px" EtiHeight="20px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero23" runat="server" Height="25px" Width="748px"
                           Text="2 - Aptitudes professionnelles et efficacité dans l'emploi"
                           CssClass="EP_TitreChapitre" style="padding-top: 7px; text-indent: 3px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           DonWidth="745px" DonHeight="160px" DonTabIndex="2" 
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"
                           Etivisible="False" Etitext="" EtiWidth="740px" EtiHeight="20px"/>
                    </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero24" runat="server" Height="25px" Width="748px"
                           Text="3 - Qualités et capacités relationnelles"
                           CssClass="EP_TitreChapitre" style="padding-top: 7px; text-indent: 3px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           DonWidth="745px" DonHeight="160px" DonTabIndex="3" 
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"
                           Etivisible="False" Etitext="" EtiWidth="740px" EtiHeight="20px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero25" runat="server" Height="25px" Width="748px"
                           Text="4 - Capacités d'encadrement"
                           CssClass="EP_TitreChapitre" style="padding-top: 7px; text-indent: 3px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           DonWidth="745px" DonHeight="160px" DonTabIndex="4" 
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"
                           Etivisible="False" Etitext="" EtiWidth="740px" EtiHeight="20px"/>
                    </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>  
                 <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero26" runat="server" Height="30px" Width="748px"
                           Text="Appréciation globale"
                           CssClass="EP_TitreChapitre" style="padding-top: 7px; text-indent: 3px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="true"
                           DonWidth="745px" DonHeight="250px" DonTabIndex="5" 
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"
                           Etivisible="False" Etitext="" EtiWidth="740px" EtiHeight="20px"/>
                    </asp:TableCell>
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreCommunication" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEtiquetteCommunication" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletCommunication" runat="server" Text="VIII. COMMUNICATION ET NOTIFICATION DU COMPTE-RENDU D'ENTRETIEN" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteCommunication" runat="server" Height="30px" Width="746px"
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero41" runat="server" Height="20px" Width="746px"
                            Text="OBSERVATIONS DE L'AGENT" 
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero42" runat="server" Height="30px" Width="748px"
                           Text="A l'occasion de la communication du compte-rendu, l'agent peut apporter des observations sur la manière dont l'entretien professionnel s'est déroulé."
                           CssClass="EP_EtiComplement" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero43" runat="server" Height="25px" Width="748px"
                           Text="- Sur le déroulement de l'entretien et les thèmes abordés"
                           CssClass="EP_TitreChapitre" style="padding-top: 7px; text-indent: 3px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVW03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           DonWidth="745px" DonHeight="180px" DonTabIndex="8" 
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"
                           Etivisible="False" Etitext="" EtiWidth="740px" EtiHeight="20px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero44" runat="server" Height="25px" Width="748px"
                           Text="- Sur les appréciations portées"
                           CssClass="EP_TitreChapitre" style="padding-top: 7px; text-indent: 3px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVX03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           DonWidth="745px" DonHeight="160px" DonTabIndex="9" 
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"
                           Etivisible="False" Etitext="" EtiWidth="740px" EtiHeight="20px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px" Visible="false">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero31" runat="server" Height="20px" Width="746px"
                            Text="VISA DE L'AUTORITE HIERARCHIQUE" 
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH23" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="23" V_SiDonneeDico="true" DonTabIndex="6" 
                           EtiWidth="290px" EtiHeight="26px" DonWidth="100px" DonHeight="20px" Donstyle="margin-left: 2px;"
                           Etitext="Date de signature de l'autorité hiérarchique" EtiVisible="true" 
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero32" runat="server" Height="25px" Width="748px"
                           Text="Observations de l'autorité hiérarchique"
                           CssClass="EP_TitreChapitre" style="padding-top: 7px; text-indent: 3px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVY03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           DonWidth="745px" DonHeight="120px" DonTabIndex="7" 
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"
                           Etivisible="False" Etitext="" EtiWidth="740px" EtiHeight="20px"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreRecours" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEtiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletNotif" runat="server" Text="IX. RECOURS HIERARCHIQUE FORME CONTRE LE COMPTE-RENDU D'ENTRETIEN" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteAgent" runat="server" Height="30px" Width="746px"
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px" Visible="false">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero51" runat="server" Height="25px" Width="746px"
                            Text="Je souhaite former une demande de révision du compte-rendu de l'entretien professionnel" 
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VSixBoutonRadio ID="RadioHB04" runat="server" V_Groupe="SiRecours"
                            V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                            VRadioN1Width="80px" VRadioN2Width="80px" VRadioN3Width="80px"
                            VRadioN4Width="80px" VRadioN5Width="80px" VRadioN6Width="80px"
                            VRadioN1Height="26px" VRadioN2Height="26px" VRadioN3Height="26px"
                            VRadioN4Height="26px" VRadioN5Height="26px" VRadioN6Height="26px"
                            VRadioN1Style="margin-left: 0px; margin-top: 1px; Font-size: Medium; padding-top: 4px" VRadioN2Style="margin-left: 0px; margin-top: 1px; Font-size: Medium; padding-top: 4px" 
                            VRadioN3Style="margin-left: 0px; margin-top: 1px;" VRadioN4Style="margin-left: 0px; margin-top: 1px;" 
                            VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="Oui" 
					        VRadioN2Text="Non" 
					        VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                            VRadioN3Visible="false" VRadioN4Visible="false"
                            VRadioN5Visible="false" VRadioN6Visible="false"  
                            Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHA04" runat="server"
                           V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true" DonTabIndex="10" 
                           EtiWidth="180px" EtiHeight="26px" DonWidth="100px" DonHeight="20px" Donstyle="margin-left: 2px;"
                           Etitext="Date du recours" EtiVisible="true"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero52" runat="server" Height="25px" Width="748px"
                           Text="Motif(s) invoqué(s) par l'agent "
                           CssClass="EP_TitreChapitre" style="padding-top: 7px; text-indent: 3px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVZ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           DonWidth="745px" DonHeight="120px" DonTabIndex="11" 
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"
                           Etivisible="False" Etitext="" EtiWidth="740px" EtiHeight="20px"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreModalitesRecours" runat="server" Height="30px" CellPadding="0" Width="748px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelTexteRecours1" runat="server" Height="40px" Width="748px"
                            Text="L'autorité hiérarchique peut être saisie par le fonctionnaire d'une demande de révision
                            du compte-rendu de l'entretien professionnel. Ce recours est exercé dans un délai de 15 jours francs
                            à compter de la date de notification à l'agent du compte-rendu de l'entretien."
                            CssClass="EP_EtiComplement" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelTexteRecours2" runat="server" Height="50px" Width="748px"
                            Text="L'autorité hiérarchique notifie sa réponse, acceptation ou refus, dans un délai de 15 jours francs
                            à compter de la date de réception de la demande de révision du compte-rendu de l'entretien professionnel.
                            (Si l'autorité hiérarchique ne répond pas, le silence gardé par l'administration pendant 2 mois vaut décision
                            implicite de rejet. Le délai d'un mois pour saisir la CAP court à compter de la date de formation
                            de la décision implicite de rejet)."
                            CssClass="EP_EtiComplement" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelTexteRecours3" runat="server" Height="40px" Width="748px"
                            Text="L'exercice de ce recours est une condition préalable à la saisine du président
                            de la commission administrative paritaire (CAP) concernée, dans un délai d'un mois 
                            à compter de la date de notification de la réponse formulée par l'autorité hiérarchique
                            dans le cadre du recours."
                            CssClass="EP_EtiComplement" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelTexteRecours4" runat="server" Height="30px" Width="748px"
                            Text="L'agent saisit, par écrit et de façon motivée, le président de la commission
                            administrative paritaire (CAP) dont il relève sous couvert de la voie hiérarchique."
                            CssClass="EP_EtiComplement" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelTexteRecours5" runat="server" Height="20px" Width="748px"
                            Text="La CAP peut, après examen de la requête de l'intéressé, demander la révision
                            du compte-rendu d'entretien professionnel."
                            CssClass="EP_EtiComplement" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelTexteRecours6" runat="server" Height="20px" Width="748px"
                            Text="Autres voies de recours"
                            Font-Bold="True"
                            CssClass="EP_EtiComplement" style="font-style: normal; margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelTexteRecours7" runat="server" Height="60px" Width="748px"
                            Text="L'agent est en outre avisé qu'en application des articles R 421-1 et R 421-2 du code de justice administrative,
                            il peut présenter un recours contentieux devant le tribunal administratif dans les deux mois qui
                            suivent la communication du compte-rendu. Si l'agent forme un recours hiérarchique dans ce délai,
                            il disposera à nouveau d'un délai de deux mois pour saisir le tribunal administratif à compter 
                            de la date de réponse de l'administration implicite ou expresse."
                            CssClass="EP_EtiComplement" style="margin-left: 2px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>                
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>