﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_FICHE_POSTE_ENM
    Inherits Virtualia.Net.Controles.ObjetWebCtlEntretien
    Private WsDossierPER As Virtualia.Net.Entretien.DossierEntretien

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirControle.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirControle.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))

            If (Ctl.ID = "InfoVB03" Or Ctl.ID = "InfoVC03" Or Ctl.ID = "InfoVD03" Or Ctl.ID = "InfoV103") Then
                If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                    VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
                End If
            Else
                VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            End If


            VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 6, 1))
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoHC04.ValeurChange, InfoHD04.ValeurChange
        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 6, 1))

        Select Case NumObjet
            Case 157
                'Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, "A")
                If WsDossierPER.Objet_157(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_157(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoVB03.ValeurChange,
        InfoVC03.ValeurChange, InfoVD03.ValeurChange, InfoVE03.ValeurChange, InfoVF03.ValeurChange, InfoV103.ValeurChange, InfoV203.ValeurChange, InfoV303.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1))

        Select Case NumObjet
            Case 154
                If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_154(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub InfoVerticale_ValeurChange2(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoVB03.ValeurChange
        Debug.WriteLine("TEst")
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim Chaine1 As String = ""
        Dim Chaine2 As String = ""

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If

        Etiquette.Text = "FICHE DE POSTE "
        LabelIdentite.Text = WsDossierPER.LibelleIdentite
        LabelIntituleRefEmploi.Text = WsDossierPER.IntituleEmploi(DatedEffet) & " (" & WsDossierPER.ReferenceEmploi(WsDossierPER.IntituleEmploi(DatedEffet)) & ")"
        LabelSousFamille.Text = "Sous-famille : " & WsDossierPER.SousFamille(DatedEffet)
        LabelPoste.Text = "Poste : " & WsDossierPER.PosteFonctionnel(DatedEffet) & " (" & WsDossierPER.NumeroDePoste(DatedEffet) & ")"
        LabelCategorie.Text = "Catégorie : " & WsDossierPER.Categorie(DatedEffet)
        LabelServiceAffectation.Text = "SERVICE D'AFFECTATION : " & WsDossierPER.ServiceOuDirection(DatedEffet)
        LabelDefinitionSynthetique.Text = WsDossierPER.DefinitionSynthetique(WsDossierPER.IntituleEmploi(DatedEffet))

        InfoVB03.DonText = WsDossierPER.ActivitesPrincipales(WsDossierPER.IntituleEmploi(DatedEffet))
        InfoVC03.DonText = WsDossierPER.Aptitudes(WsDossierPER.IntituleEmploi(DatedEffet))
        InfoVD03.DonText = WsDossierPER.Conditions_Exercice(WsDossierPER.IntituleEmploi(DatedEffet))
        InfoV103.DonText = WsDossierPER.Categorie(DatedEffet)

        Call LireLaFiche()
        ''******** AKR 08/01/2018 
        Dim NumInfo As Integer
        Dim NumObjet As Integer

        NumInfo = 3
        NumObjet = 154

        If WsDossierPER.Objet_154("8.1") IsNot Nothing Then
            If InfoVB03.DonText IsNot Nothing Then
                WsDossierPER.TableauMaj(NumObjet, NumInfo, "8.1") = InfoVB03.DonText
            End If
        End If

        If WsDossierPER.Objet_154("8.2") IsNot Nothing Then
            If InfoVC03.DonText IsNot Nothing Then
                WsDossierPER.TableauMaj(NumObjet, NumInfo, "8.2") = InfoVC03.DonText
            End If
        End If

        If WsDossierPER.Objet_154("8.3.1") IsNot Nothing Then
            If InfoVD03.DonText IsNot Nothing Then
                WsDossierPER.TableauMaj(NumObjet, NumInfo, "8.3.1") = InfoVD03.DonText
            End If
        End If
        ''********
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop
        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierEvaluation
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 154
                    If WsDossierPER.Objet_154(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_154(Rang).V_TableauData(NoInfo).ToString
                    End If
                Case 157
                    If WsDossierPER.Objet_157(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_157(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property

End Class