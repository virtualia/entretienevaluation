﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_OBJECTIFS_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_OBJECTIFS_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreObjectifs" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="IV. OBJECTIFS ASSIGNES POUR L'ANNEE A VENIR" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="30px" Width="748px"
                           Text="1 - Principaux objectifs du service pour l'année à venir"
                           CssClass="EP_TitreChapitre"
                           style="margin-bottom: 2px; padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="744px" DonHeight="190px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 1px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelContexteAgent" runat="server" Height="33px" Width="748px"
                           Text="2 - Déclinaison des objectifs du service pour l'agent"
                           CssClass="EP_TitreChapitre"
                           style="padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableEnteteObjectifs" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelEnteteObjectif1" runat="server" Height="20px" Width="313px"
                                   Text="Description des objectifs"
                                   CssClass="EP_EtiTableau"
                                   Font-Bold="true" style="margin-left: 10px">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelEnteteObjectif2" runat="server" Height="28px" Width="313px" Text=""
                                   CssClass="EP_EtiTableau"
                                   Font-Italic="true" Font-Size="90%"  
                                   style="margin-left: 10px; font-style: italic">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableEnteteSeparateur" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelEnteteSeparateur1" runat="server" Height="20px" Width="70px" Text=""
                                   CssClass="EP_EtiComplement">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelEnteteSeparateur2" runat="server" Height="28px" Width="70px" Text=""
                                   CssClass="EP_EtiComplement">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableEnteteConditions" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelEnteteConditions1" runat="server" Height="20px" Width="320px"
                                   Text="Conditions de mise en oeuvre"
                                   CssClass="EP_EtiTableau"
                                   Font-Bold="true">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelEnteteConditions2" runat="server" Height="28px" Width="320px"
                                   Text="(délais et moyens nécessaires)"
                                   CssClass="EP_EtiTableau"
                                   Font-Italic="true" Font-Size="90%"  
                                   style="font-style: italic">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="2"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableSeparateur1" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelSeparateur1" runat="server" Height="90px" Width="75px"
                                   Text="Objectif <br/> n°1"
                                   CssClass="EP_EtiTableau"
                                   Font-Size="Medium" style="margin-top: 1px; padding-top: 20px;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableConditions1" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="3"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="4"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableSeparateur2" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelSeparateur2" runat="server" Height="90px" Width="75px"
                                   Text="Objectif <br/> n°2"
                                   CssClass="EP_EtiTableau"
                                   Font-Size="Medium" style="margin-top: 1px; padding-top: 20px;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableConditions2" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="6"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableSeparateur3" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelSeparateur3" runat="server" Height="90px" Width="75px"
                                   Text="Objectif <br/> n°3"
                                   CssClass="EP_EtiTableau"
                                   Font-Size="Medium" style="margin-top: 1px; padding-top: 20px;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableConditions3" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="8"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableSeparateur4" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelSeparateur4" runat="server" Height="90px" Width="75px"
                                   Text="Objectif <br/> n°4"
                                   CssClass="EP_EtiTableau"
                                   Font-Size="Medium" style="margin-top: 1px; padding-top: 20px;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableConditions4" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="9"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="323px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="318px" DonHeight="110px" DonTabIndex="10"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableSeparateur5" runat="server" CellPadding="0" CellSpacing="0" Width="75px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="Notset" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <asp:Label ID="LabelSeparateur5" runat="server" Height="90px" Width="75px"
                                   Text="Objectif <br/> n°5"
                                   CssClass="EP_EtiTableau"
                                   Font-Size="Medium" style="margin-top: 1px; padding-top: 20px;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Table ID="TableConditions5" runat="server" CellPadding="0" CellSpacing="0" Width="330px"
                        BackColor="Transparent" BorderColor="#98D4CA" BorderStyle="None" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="325px" DonHeight="110px" DonTabIndex="11"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>             
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero31" runat="server" Height="30px" Width="748px"
                           Text="3 - Perspectives d'évolution des conditions d'organisation et de fonctionnement du service"
                           CssClass="EP_TitreChapitre"
                           style="margin-bottom: 2px; padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="744px" DonHeight="190px" EtiHeight="20px" DonTabIndex="12" 
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 1px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero32" runat="server" Height="30px" Width="748px"
                           Text="4 - Evolution envisagée des fonctions exercées"
                           CssClass="EP_TitreChapitre"
                           style="margin-bottom: 2px; padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="744px" DonHeight="190px" EtiHeight="20px" DonTabIndex="13" 
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 1px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>