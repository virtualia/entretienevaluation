﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_COMPORTEMENT_HONDA
    Inherits Virtualia.Net.Controles.ObjetWebCtlHonda
    Private WsDossierPER As Virtualia.Net.Entretien.DossierHonda

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirControle.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirControle.Visible = WsDossierPER.SiVisible(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            If VirControle.Visible = True Then
                VirControle.V_SiEnLectureSeule = WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0, ""))
                If VirControle.V_SiEnLectureSeule = True Then
                    VirControle.DonBackColor = Drawing.Color.LightGray
                Else
                    VirControle.DonBackColor = Drawing.Color.White
                End If
            End If
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.Visible = WsDossierPER.SiVisible(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            If VirVertical.Visible = True Then
                Select Case NumInfo
                    Case 1
                        VirVertical.V_SiEnLectureSeule = True
                        VirVertical.V_SiAutoPostBack = False
                    Case Else
                        VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0, ""))
                        VirVertical.V_SiAutoPostBack = False
                End Select
                If VirVertical.V_SiEnLectureSeule = True Then
                    VirVertical.DonBackColor = Drawing.Color.LightGray
                Else
                    VirVertical.DonBackColor = Drawing.Color.White
                End If
            End If
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VTrioHorizontalRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.Visible = WsDossierPER.SiVisible(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            If VirRadio.Visible = True Then
                VirRadio.V_SiEnLectureSeule = WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            End If
            VirRadio.V_SiAutoPostBack = Not VirRadio.V_SiEnLectureSeule
            If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                Select Case ValeurLue(NumObjet, NumInfo, Rang)
                    Case Is = "Oui"
                        VirRadio.RadioCentreCheck = True
                    Case Else
                        VirRadio.RadioGaucheCheck = True
                End Select
            End If
            IndiceI += 1
        Loop

        Dim VirTextCommande As VirtualiaControle_VTrioEtiTextCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoC", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirTextCommande = CType(Ctl, VirtualiaControle_VTrioEtiTextCommande)
            NumObjet = VirTextCommande.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirTextCommande.Visible = WsDossierPER.SiVisible(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            If VirTextCommande.Visible = True Then
                VirTextCommande.CmdVisible = Not (WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0, "")))
            End If
            VirTextCommande.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirCommentaire As System.Web.UI.WebControls.Label
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "labelInfosElement", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirCommentaire = CType(Ctl, System.Web.UI.WebControls.Label)
            Rang = Strings.Right(Ctl.ID, 2)
            VirCommentaire.Text = WsDossierPER.Evaluation_Commentaire(Rang)
            IndiceI += 1
        Loop

        If WsDossierPER.SiDRH = False Then
            Select Case WsDossierPER.PeriodeKI(ValeurLue(150, 0, ""))
                Case "DEBUT KI", "MILIEU KI", "FIN KI - SELF"
                    EnteteElementEvaluationBas.Visible = False
                    CadreEvaluationComportement.Visible = False
                    CadreEvaluationGlobale.Visible = False
                    LabelEvaluationFinale.Visible = False
                    CadreLegendeNotation.Visible = False
                    CadreEvaluationFinale.Visible = False
                    CadreNotationFinale.Visible = False
                    CadreEnteteSignatureEvaluation.Visible = False
                    CadreSignatureDebutKI.Visible = False
                    CadreSignatureMilieuKI.Visible = False
                    CadreSignatureFinKI.Visible = False
                    CadreRefusSignature.Visible = False
                Case "FIN KI - MANAGER"
                    CadreRefusSignature.Visible = False
            End Select
        End If
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV22.ValeurChange, _
        InfoV27.ValeurChange, InfoV28.ValeurChange, InfoV29.ValeurChange, _
        InfoVK01.ValeurChange, InfoVL01.ValeurChange, InfoVM01.ValeurChange, _
        InfoVN01.ValeurChange, InfoVO01.ValeurChange, InfoVP01.ValeurChange, _
        InfoVQ01.ValeurChange, InfoVR01.ValeurChange, InfoVS01.ValeurChange, InfoVT01.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1))

        Select Case NumObjet
            Case 150
                If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
                    WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
                End If
            Case 152
                If WsDossierPER.Objet_152(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_152(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub RadioValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles RadioH10.ValeurChange, _
        RadioH15.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VTrioHorizontalRadio).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VTrioHorizontalRadio).V_Objet

        Select Case NumObjet
            Case 150
                If WsDossierPER.Objet_150 IsNot Nothing Then
                    If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Private Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH04.ValeurChange, _
        InfoH05.ValeurChange, InfoH06.ValeurChange, _
        InfoH07.ValeurChange, InfoH08.ValeurChange, InfoH09.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 6, 1))
        Select Case NumObjet
            Case 150
                If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
                    CType(sender, VirtualiaControle_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
                End If
        End Select
    End Sub

    Protected Sub InfoC_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoCK03.ValeurChange, _
        InfoCL03.ValeurChange, InfoCM03.ValeurChange, _
        InfoCN03.ValeurChange, InfoCO03.ValeurChange, InfoCP03.ValeurChange, _
        InfoCQ03.ValeurChange, InfoCR03.ValeurChange, InfoCS03.ValeurChange, InfoCT03.ValeurChange, _
        InfoCK04.ValeurChange, InfoCL04.ValeurChange, InfoCM04.ValeurChange, _
        InfoCN04.ValeurChange, InfoCO04.ValeurChange, InfoCP04.ValeurChange, _
        InfoCQ04.ValeurChange, InfoCR04.ValeurChange, InfoCS04.ValeurChange, InfoCT04.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VTrioEtiTextCommande).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VTrioEtiTextCommande).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VTrioEtiTextCommande).ID, 6, 1))

        Select Case NumObjet
            Case 152
                If WsDossierPER.Objet_152(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_152(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim Ctl As Control
        Dim VirTextCommande As VirtualiaControle_VTrioEtiTextCommande
        Dim IndiceI As Integer = 0
        Dim DateAffectation As String

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        Etiquette.Text = "COMPORTEMENT DU COLLABORATEUR - " & WsDossierPER.KI(ValeurLue(150, 0, "")) & " KI"
        LabelIdentiteCollaborateur.Text = WsDossierPER.LibelleIdentite
        DateAffectation = WsDossierPER.DateValeur_Infos(ValeurLue(150, 0, ""))
        If WsDossierPER.NiveauAffectation(WsDossierPER.DateValeur_Infos(DateAffectation), 2) <> "" Then
            LabelDepartementCollaborateur.Text = WsDossierPER.NiveauAffectation(DateAffectation, 2)
        Else
            LabelDepartementCollaborateur.Text = WsDossierPER.NiveauAffectation(DateAffectation, 1)
        End If
        LabelFonctionCollaborateur.Text = WsDossierPER.FonctionExercee(DateAffectation, False)
        LabelIdentiteEvaluateur.Text = WsDossierPER.Evaluateur_Nom_Prenom
        LabelDepartementEvaluateur.Text = WsDossierPER.Evaluateur_Service
        LabelFonctionEvaluateur.Text = WsDossierPER.Evaluateur_Fonction
        LabelPeriodeKI.Text = WsDossierPER.PeriodeKI(ValeurLue(150, 0, ""))

        LabelVoletRevision.Text = ValeurLue(150, 3, "")

        EvaluationComportementCollaborateur.Text = Strings.Format(WsDossierPER.Total_Comportement(True), "0.00")
        EvaluationComportementEvaluateur.Text = Strings.Format(WsDossierPER.Total_Comportement(False), "0.00")
        EvaluationGlobaleCollaborateur.Text = WsDossierPER.Note_Globale(True)
        EvaluationGlobaleEvaluateur.Text = WsDossierPER.Note_Globale(False)

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoC", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirTextCommande = CType(Ctl, VirtualiaControle_VTrioEtiTextCommande)
            VirTextCommande.V_Strict = False
            VirTextCommande.V_Suffixe = ""
            Select Case ValeurLue(150, 2, "") 'Type du formulaire
                Case "Manager"
                    VirTextCommande.DonTooltip = "Evaluation de l'évaluateur - | -1 | -0,5 | 0 | 0,5 | 1 |" & vbCrLf & "Valeur moyenne 0"
                    VirTextCommande.V_TypeCtrl = VirtualiaControle_VTrioEtiTextCommande.TypeTrioEti.ListeValeur
                    VirTextCommande.V_ListeValeurs = "-1|-0,5|0|0,5|1"
                Case Else
                    VirTextCommande.DonTooltip = "Evaluation de l'évaluateur - | 0 | 1 | 2 | 3 | 4 |" & vbCrLf & "Valeur moyenne 2"
                    VirTextCommande.V_TypeCtrl = VirtualiaControle_VTrioEtiTextCommande.TypeTrioEti.Bornee
                    VirTextCommande.V_ValMax = 4
                    VirTextCommande.V_ValMin = 0
                    VirTextCommande.V_Pas = 1
            End Select
            IndiceI += 1
        Loop

        Select Case ValeurLue(150, 2, "") 'Type du formulaire
            Case "Manager"
                Etiquette.Text = "COMPORTEMENT DU MANAGER - " & WsDossierPER.KI(ValeurLue(150, 0, "")) & " KI"

                TitreEvaluationComportement.Text = "2. Evaluation du comportement : Maximum 1 point"
                TitreEvaluationGlobale.Text = "3. Evaluation globale : Maximum 6 points"

                EvaluationPerformanceGlobale.Text = ValeurLue(150, 18, "")
                EvaluationComportementGlobale.Text = ValeurLue(150, 21, "")
                EvaluationTotaleGlobale.Text = ValeurLue(150, 24, "")
                EvaluationNoteGlobale.Text = ValeurLue(150, 25, "")

            Case Else
                TitreEvaluationComportement.Text = "2. Evaluation du comportement : Maximum 40 points"
                TitreEvaluationGlobale.Text = "3. Evaluation globale : Maximum 100 points"

        End Select

        LabelVoletRevision.Text = ValeurLue(150, 3, "")
        EvaluationNoteGlobale.Text = WsDossierPER.Note_Globale_Lettre
        EvaluationPerformanceGlobale.Text = ValeurLue(150, 18, "")
        EvaluationComportementGlobale.Text = ValeurLue(150, 21, "")
        EvaluationTotaleGlobale.Text = ValeurLue(150, 24, "")
        EvaluationNoteGlobale.ToolTip = WsDossierPER.Tooltip_NoteLettre(EvaluationNoteGlobale.Text)

        LabelLegendeNoteAPlus2.Text = WsDossierPER.Label_NoteChiffre("A*")
        LabelLegendeNoteA2.Text = WsDossierPER.Label_NoteChiffre("A")
        LabelLegendeNoteBPlus2.Text = WsDossierPER.Label_NoteChiffre("B+")
        LabelLegendeNoteB2.Text = WsDossierPER.Label_NoteChiffre("B")
        LabelLegendeNoteBMoins2.Text = WsDossierPER.Label_NoteChiffre("B-")
        LabelLegendeNoteC2.Text = WsDossierPER.Label_NoteChiffre("C")

        Call LireLaFiche()
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop

        Dim VirTextCommande As VirtualiaControle_VTrioEtiTextCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoC", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirTextCommande = CType(Ctl, VirtualiaControle_VTrioEtiTextCommande)
            VirTextCommande.DonBackColor = Drawing.Color.White
            VirTextCommande.DonText = ""
            IndiceI += 1
        Loop

    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 152
                    If WsDossierPER.Objet_152(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_152(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property

End Class