﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_PERFORMANCE_HONDA

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreEntetePerformance.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEntetePerformance As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelIdentiteCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelIdentiteCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDepartementCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDepartementCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelFonctionCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFonctionCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelIdentiteEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelIdentiteEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDepartementEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDepartementEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelFonctionEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFonctionEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletPeriodeKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletPeriodeKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelPeriodeKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPeriodeKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletRevision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletRevision As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDateRevision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateRevision As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreRole.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreRole As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreRoleOrganisation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreRoleOrganisation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelCommentaireRoleOrganisation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCommentaireRoleOrganisation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreRole.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreRole As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV11 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle LabelTitreCible.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCible As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV12 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle LabelTitreRoleCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreRoleCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelCommentaireRoleCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCommentaireRoleCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV13 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle LabelTitreExigence.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreExigence As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelCommentaireExigence.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCommentaireExigence As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV14 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle LabelEvaluationPerformance.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvaluationPerformance As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EnteteObjectifSociete1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EnteteObjectifSociete1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TitreRealisationSociete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreRealisationSociete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TexteRealisationSociete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TexteRealisationSociete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EnteteObjectifSociete2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EnteteObjectifSociete2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelObjectifSociete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelObjectifSociete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTargetSociete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTargetSociete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelRealisationSociete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelRealisationSociete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelPoidsSociete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPoidsSociete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelResultatSociete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelResultatSociete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDifficulteSociete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDifficulteSociete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEvaluationSociete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvaluationSociete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreObjectifSociete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreObjectifSociete As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneObjSociete1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneObjSociete1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVK02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVK02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVK03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVK03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVK04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVK04 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle PerformanceObjSociete1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PerformanceObjSociete1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelCollPerfObjSoc1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCollPerfObjSoc1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CollaborateurPerfObjSoc1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CollaborateurPerfObjSoc1 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCK09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCK09 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCK10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCK10 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCK11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCK11 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCK12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCK12 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LabelEvalPerfObjSoc1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvalPerfObjSoc1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluateurPerfObjSoc1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluateurPerfObjSoc1 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCK05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCK05 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCK06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCK06 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCK07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCK07 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCK08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCK08 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneObjSociete2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneObjSociete2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVL02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVL02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVL03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVL03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVL04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVL04 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle PerformanceObjSociete2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PerformanceObjSociete2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelCollPerfObjSoc2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCollPerfObjSoc2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CollaborateurPerfObjSoc2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CollaborateurPerfObjSoc2 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCL09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCL09 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCL10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCL10 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCL11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCL11 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCL12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCL12 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LabelEvalPerfObjSoc2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvalPerfObjSoc2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluateurPerfObjSoc2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluateurPerfObjSoc2 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCL05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCL05 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCL06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCL06 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCL07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCL07 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCL08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCL08 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneObjSociete3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneObjSociete3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVM02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVM02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVM03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVM04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVM04 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle PerformanceObjSociete3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PerformanceObjSociete3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelCollPerfObjSoc3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCollPerfObjSoc3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CollaborateurPerfObjSoc3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CollaborateurPerfObjSoc3 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCM09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCM09 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCM10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCM10 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCM11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCM11 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCM12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCM12 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LabelEvalPerfObjSoc3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvalPerfObjSoc3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluateurPerfObjSoc3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluateurPerfObjSoc3 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCM05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCM05 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCM06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCM06 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCM07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCM07 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCM08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCM08 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneObjSociete4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneObjSociete4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVN02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVN02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVN03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVN03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVN04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVN04 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle PerformanceObjSociete4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PerformanceObjSociete4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelCollPerfObjSoc4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCollPerfObjSoc4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CollaborateurPerfObjSoc4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CollaborateurPerfObjSoc4 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCN09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCN09 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCN10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCN10 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCN11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCN11 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCN12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCN12 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LabelEvalPerfObjSoc4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvalPerfObjSoc4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluateurPerfObjSoc4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluateurPerfObjSoc4 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCN05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCN05 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCN06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCN06 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCN07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCN07 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCN08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCN08 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle EnteteObjectifFonction1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EnteteObjectifFonction1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TitreRealisationFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreRealisationFonction As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TexteRealisationFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TexteRealisationFonction As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EnteteObjectifFonction2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EnteteObjectifFonction2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelObjectifFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelObjectifFonction As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTargetFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTargetFonction As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelRealisationFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelRealisationFonction As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelPoidsFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPoidsFonction As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelResultatFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelResultatFonction As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDifficulteFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDifficulteFonction As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEvaluationFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvaluationFonction As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreObjectifFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreObjectifFonction As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneObjFonction1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneObjFonction1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVO02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVO02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVO03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVO03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVO04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVO04 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle PerformanceObjFonction1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PerformanceObjFonction1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelCollPerfObjFonc1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCollPerfObjFonc1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CollaborateurPerfObjFonc1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CollaborateurPerfObjFonc1 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCO09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCO09 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCO10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCO10 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCO11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCO11 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCO12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCO12 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LabelEvalPerfObjFonc1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvalPerfObjFonc1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluateurPerfObjFonc1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluateurPerfObjFonc1 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCO05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCO05 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCO06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCO06 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCO07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCO07 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCO08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCO08 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneObjFonction2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneObjFonction2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVP02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVP02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVP03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVP03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVP04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVP04 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle PerformanceObjFonction2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PerformanceObjFonction2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelCollPerfObjFonc2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCollPerfObjFonc2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CollaborateurPerfObjFonc2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CollaborateurPerfObjFonc2 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCP09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCP09 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCP10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCP10 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCP11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCP11 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCP12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCP12 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LabelEvalPerfObjFonc2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvalPerfObjFonc2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluateurPerfObjFonc2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluateurPerfObjFonc2 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCP05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCP05 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCP06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCP06 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCP07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCP07 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCP08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCP08 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneObjFonction3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneObjFonction3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVQ02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVQ02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVQ03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVQ03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVQ04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVQ04 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle PerformanceObjFonction3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PerformanceObjFonction3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelCollPerfObjFonc3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCollPerfObjFonc3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CollaborateurPerfObjFonc3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CollaborateurPerfObjFonc3 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCQ09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCQ09 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCQ10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCQ10 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCQ11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCQ11 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCQ12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCQ12 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LabelEvalPerfObjFonc3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvalPerfObjFonc3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluateurPerfObjFonc3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluateurPerfObjFonc3 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCQ05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCQ05 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCQ06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCQ06 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCQ07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCQ07 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCQ08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCQ08 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneObjFonction4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneObjFonction4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVR02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVR02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVR03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVR03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVR04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVR04 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle PerformanceObjFonction4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PerformanceObjFonction4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelCollPerfObjFonc4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCollPerfObjFonc4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CollaborateurPerfObjFonc4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CollaborateurPerfObjFonc4 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCR09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCR09 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCR10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCR10 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCR11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCR11 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCR12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCR12 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LabelEvalPerfObjFonc4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvalPerfObjFonc4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluateurPerfObjFonc4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluateurPerfObjFonc4 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCR05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCR05 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCR06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCR06 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCR07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCR07 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCR08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCR08 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneObjFonction5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneObjFonction5 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVS02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVS02 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVS03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVS03 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoVS04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVS04 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle PerformanceObjFonction5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PerformanceObjFonction5 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelCollPerfObjFonc5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCollPerfObjFonc5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CollaborateurPerfObjFonc5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CollaborateurPerfObjFonc5 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCS09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCS09 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCS10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCS10 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCS11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCS11 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCS12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCS12 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LabelEvalPerfObjFonc5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvalPerfObjFonc5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluateurPerfObjFonc5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluateurPerfObjFonc5 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle InfoCS05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCS05 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCS06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCS06 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCS07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCS07 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCS08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCS08 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle CadrePerformanceCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadrePerformanceCollaborateur As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TitrePerformanceCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitrePerformanceCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle SommePoidsObjectifsCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SommePoidsObjectifsCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelResultatCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelResultatCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelPerformanceCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPerformanceCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadrePerformanceEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadrePerformanceEvaluateur As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TitrePerformanceEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitrePerformanceEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle SommePoidsObjectifsEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SommePoidsObjectifsEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelResultatEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelResultatEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelPerformanceEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPerformanceEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreDifficulte.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreDifficulte As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV19 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
End Class
