﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_FORMATION_HONDA
    Inherits Virtualia.Net.Controles.ObjetWebCtlHonda
    Private WsDossierPER As Virtualia.Net.Entretien.DossierHonda

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
            End If
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirControle.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirControle.Visible = WsDossierPER.SiVisible(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            If VirControle.Visible = True Then
                VirControle.V_SiEnLectureSeule = WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0, ""))
                If VirControle.V_SiEnLectureSeule = True Then
                    VirControle.DonBackColor = Drawing.Color.LightGray
                Else
                    VirControle.DonBackColor = Drawing.Color.White
                End If
            End If
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.Visible = WsDossierPER.SiVisible(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            If VirVertical.Visible = True Then
                VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0, ""))
                If VirVertical.V_SiEnLectureSeule = True Then
                    VirVertical.DonBackColor = Drawing.Color.LightGray
                Else
                    VirVertical.DonBackColor = Drawing.Color.White
                End If
            End If
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirCombo As VirtualiaControle_VListeCombo
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "ListC", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirCombo = CType(Ctl, VirtualiaControle_VListeCombo)
            NumObjet = VirCombo.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirCombo.LstText = ValeurLue(NumObjet, NumInfo, Rang)
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VTrioVerticalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadio = CType(Ctl, VirtualiaControle_VTrioVerticalRadio)
            NumObjet = VirRadio.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 7, 1))
            VirRadio.Visible = WsDossierPER.SiVisible(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            If VirRadio.Visible = True Then
                VirRadio.V_SiEnLectureSeule = WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            End If
            VirRadio.V_SiAutoPostBack = Not VirRadio.V_SiEnLectureSeule
            Select Case NumInfo
                Case 17
                    Select Case ValeurLue(NumObjet, NumInfo, Rang)
                        Case Is = "T1"
                            VirRadio.RadioGaucheCheck = True
                        Case Is = "T2"
                            VirRadio.RadioCentreCheck = True
                        Case Else
                            VirRadio.RadioGaucheCheck = False
                            VirRadio.RadioCentreCheck = False
                            VirRadio.RadioDroiteCheck = False
                    End Select
            End Select
            IndiceI += 1
        Loop

        Dim VirRadioH As VirtualiaControle_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirRadioH = CType(Ctl, VirtualiaControle_VTrioHorizontalRadio)
            NumObjet = VirRadioH.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 7, 1))
            VirRadioH.Visible = WsDossierPER.SiVisible(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            If VirRadioH.Visible = True Then
                VirRadioH.V_SiEnLectureSeule = WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            End If
            VirRadioH.V_SiAutoPostBack = Not VirRadioH.V_SiEnLectureSeule
            If ValeurLue(NumObjet, NumInfo, Rang) <> "" Then
                Select Case NumInfo
                    Case Is = 12
                        Select Case ValeurLue(NumObjet, NumInfo, Rang)
                            Case Is = "Accord"
                                VirRadioH.RadioGaucheCheck = True
                            Case Is = "Refus avec report"
                                VirRadioH.RadioCentreCheck = True
                            Case Is = "Refus"
                                VirRadioH.RadioDroiteCheck = True
                            Case Else
                                VirRadioH.RadioGaucheCheck = False
                                VirRadioH.RadioCentreCheck = False
                                VirRadioH.RadioDroiteCheck = False
                        End Select
                    Case Is = 14
                        Select Case ValeurLue(NumObjet, NumInfo, Rang)
                            Case Is = "Oui"
                                VirRadioH.RadioCentreCheck = True
                            Case Else
                                VirRadioH.RadioGaucheCheck = True
                        End Select
                End Select
            End If
            IndiceI += 1
        Loop

        If WsDossierPER.SiDRH = False AndAlso WsDossierPER.SiSelf = True Then
            CadreValidation1.Visible = False
            CadreValidation2.Visible = False
            CadreValidation3.Visible = False
            If WsDossierPER.Objet_153("1").SiValidation_DRH <> "Oui" Or WsDossierPER.Objet_153("1").Validation_N1 <> "Accord" Then
                CadreNumero11.Visible = False
                CadreNumero12.Visible = False
                CadreEnteteDemandeN1.Visible = False
                CadreDemandeN1.Visible = False
                LabelSeparationDemandeN1.Visible = False
            End If
            If WsDossierPER.Objet_153("2").SiValidation_DRH <> "Oui" Or WsDossierPER.Objet_153("2").Validation_N1 <> "Accord" Then
                CadreNumero21.Visible = False
                CadreNumero22.Visible = False
                CadreEnteteDemandeN2.Visible = False
                CadreDemandeN2.Visible = False
                LabelSeparationDemandeN2.Visible = False
            End If
            If WsDossierPER.Objet_153("3").SiValidation_DRH <> "Oui" Or WsDossierPER.Objet_153("3").Validation_N1 <> "Accord" Then
                CadreNumero31.Visible = False
                CadreNumero32.Visible = False
                CadreEnteteDemandeN3.Visible = False
                CadreDemandeN3.Visible = False
            End If
        End If
    End Sub

    Private Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoHA02.ValeurChange, _
        InfoHA06.ValeurChange, InfoHA10.ValeurChange, InfoHA08.ValeurChange, InfoHA07.ValeurChange, InfoHA13.ValeurChange, InfoHA15.ValeurChange, _
        InfoHB06.ValeurChange, InfoHB10.ValeurChange, InfoHB08.ValeurChange, InfoHB07.ValeurChange, InfoHB13.ValeurChange, InfoHB15.ValeurChange, InfoHB02.ValeurChange, _
        InfoHC06.ValeurChange, InfoHC10.ValeurChange, InfoHC08.ValeurChange, InfoHC07.ValeurChange, InfoHC13.ValeurChange, InfoHC15.ValeurChange, InfoHC02.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 6, 1))
        Select Case NumObjet
            Case 153
                If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        CType(sender, VirtualiaControle_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Private Sub ListC_ValeurChange(sender As Object, e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListCA03.ValeurChange, _
        ListCB03.ValeurChange, ListCC03.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VListeCombo).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VListeCombo).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VListeCombo).ID, 6, 1))

        Select Case NumObjet
            Case 153
                If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                    If WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 1, "")) = True Then
                        CType(sender, VirtualiaControle_VListeCombo).LstText = WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString
                        Exit Sub
                    End If
                    If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        CType(sender, VirtualiaControle_VListeCombo).LstBackColor = V_WebFonction.CouleurMaj
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles _
    InfoVA01.ValeurChange, InfoVA04.ValeurChange, InfoVA05.ValeurChange, InfoVA09.ValeurChange, InfoVA16.ValeurChange, _
    InfoVB01.ValeurChange, InfoVB04.ValeurChange, InfoVB05.ValeurChange, InfoVB09.ValeurChange, InfoVB16.ValeurChange, _
    InfoVC01.ValeurChange, InfoVC04.ValeurChange, InfoVC05.ValeurChange, InfoVC09.ValeurChange, InfoVC16.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1))

        Select Case NumObjet
            Case 153
                If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub RadioValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles _
        RadioVA17.ValeurChange, RadioVB17.ValeurChange, RadioVC17.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VTrioVerticalRadio).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VTrioVerticalRadio).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VTrioVerticalRadio).ID, 7, 1))

        Select Case NumObjet
            Case 153
                If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub RadioHorizValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles _
        RadioHA12.ValeurChange, RadioHA14.ValeurChange, RadioHB12.ValeurChange, RadioHB14.ValeurChange, RadioHC12.ValeurChange, RadioHC14.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VTrioHorizontalRadio).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VTrioHorizontalRadio).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VTrioHorizontalRadio).ID, 7, 1))

        Select Case NumObjet
            Case 153
                If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_153(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ListeFormationsSuivies.Centrage_Colonne(0) = 1
        ListeFormationsSuivies.Centrage_Colonne(1) = 0
        ListeFormationsSuivies.Centrage_Colonne(2) = 0
        ListeFormationsSuivies.Centrage_Colonne(3) = 0
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DateAffectation As String
        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        Etiquette.Text = "BESOINS EN FORMATION - " & WsDossierPER.KI(ValeurLue(150, 0, "")) & " KI"
        LabelIdentiteCollaborateur.Text = WsDossierPER.LibelleIdentite
        DateAffectation = WsDossierPER.DateValeur_Infos(ValeurLue(150, 0, ""))
        If WsDossierPER.NiveauAffectation(WsDossierPER.DateValeur_Infos(DateAffectation), 2) <> "" Then
            LabelDepartementCollaborateur.Text = WsDossierPER.NiveauAffectation(DateAffectation, 2)
        Else
            LabelDepartementCollaborateur.Text = WsDossierPER.NiveauAffectation(DateAffectation, 1)
        End If
        LabelFonctionCollaborateur.Text = WsDossierPER.FonctionExercee(DateAffectation, False)
        LabelIdentiteEvaluateur.Text = WsDossierPER.Evaluateur_Nom_Prenom
        LabelDepartementEvaluateur.Text = WsDossierPER.Evaluateur_Service
        LabelFonctionEvaluateur.Text = WsDossierPER.Evaluateur_Fonction
        LabelPeriodeKI.Text = WsDossierPER.PeriodeKI(ValeurLue(150, 0, ""))
        LabelVoletRevision.Text = ValeurLue(150, 3, "")

        'Liste des formations 
        V_LibelListe = "Aucune formation" & VI.Tild & "Une formation" & VI.Tild & "formations"
        V_LibelColonne = "date" & VI.Tild & "formation suivie" & VI.Tild & "Durée" & VI.Tild & "Clef"
        ListeFormationsSuivies.V_Liste(V_LibelColonne, V_LibelListe) = WsDossierPER.ListeDesFormationsSuivies(WsDossierPER.Annee, 3)

        Call LireLaFiche()
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop
        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 153
                    If WsDossierPER.Objet_153(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_153(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property

End Class