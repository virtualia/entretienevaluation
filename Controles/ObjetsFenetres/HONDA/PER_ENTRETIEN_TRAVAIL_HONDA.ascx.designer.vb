﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_TRAVAIL_HONDA

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreEnteteTravail.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEnteteTravail As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiquetteSuite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiquetteSuite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelIdentiteCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelIdentiteCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDepartementCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDepartementCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelFonctionCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFonctionCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelIdentiteEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelIdentiteEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDepartementEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDepartementEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelFonctionEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFonctionEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletPeriodeKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletPeriodeKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelPeriodeKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPeriodeKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletRevision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletRevision As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDateRevision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateRevision As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreTravailManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTravailManager As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreTravailManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreTravailManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailManager1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailManager1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailManager2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailManager2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailManager3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailManager3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailManager4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailManager4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailManager5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailManager5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailManager6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailManager6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailManager7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailManager7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailManager8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailManager8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailManager9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailManager9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreCommentaireManager.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCommentaireManager As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV31 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle CadreTravailSalarie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTravailSalarie As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreTravailSalarie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreTravailSalarie As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailSalarie1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailSalarie1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailSalarie2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailSalarie2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailSalarie3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailSalarie3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailSalarie4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailSalarie4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailSalarie5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailSalarie5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailSalarie6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailSalarie6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailSalarie7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailSalarie7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailSalarie8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailSalarie8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTravailSalarie9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTravailSalarie9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreCommentaireSalarie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCommentaireSalarie As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV30 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
End Class
