﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_FORMATION_HONDA

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreEnteteComportement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEnteteComportement As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelIdentiteCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelIdentiteCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDepartementCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDepartementCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelFonctionCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFonctionCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelIdentiteEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelIdentiteEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDepartementEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDepartementEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelFonctionEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFonctionEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletPeriodeKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletPeriodeKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelPeriodeKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPeriodeKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletRevision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletRevision As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDateRevision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateRevision As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreFormationsSuivies.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreFormationsSuivies As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreNumero11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle ListeFormationsSuivies.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeFormationsSuivies As Global.Virtualia.Net.VirtualiaControle_VListeGrid

    '''<summary>
    '''Contrôle CadreNumero11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero11 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreNumero2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNumero2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EnteteBesoinN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EnteteBesoinN1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableLiensSitesWeb1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableLiensSitesWeb1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle SiteN11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN11 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN12 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN13 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN14 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN15 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN16 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle CadreNumero12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero12 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelLegendeN1T11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeN1T11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeN1T12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeN1T12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeN1T21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeN1T21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeN1T22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeN1T22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreEnteteDemandeN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEnteteDemandeN1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelEnteteExpressionDemande1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteExpressionDemande1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteCout1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteCout1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteDureeFormation1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteDureeFormation1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEntetePeriode1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEntetePeriode1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteType1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteType1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreDemandeN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDemandeN1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TableDemandeN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDemandeN1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TableActionFormation1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableActionFormation1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreAction1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreAction1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreReference1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreReference1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoHA02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHA02 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoVA01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle ListCA03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListCA03 As Global.Virtualia.Net.VirtualiaControle_VListeCombo

    '''<summary>
    '''Contrôle LabelPrecisionsN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPrecisionsN1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVA04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA04 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle LabelObjFormationN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelObjFormationN1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVA09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA09 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoHA08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHA08 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle TableDureeJours1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDureeJours1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoHA06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHA06 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelDureeJours1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDureeJours1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoHA07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHA07 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelDureeHeures1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDureeHeures1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVA05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioVA17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioVA17 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle CadreValidation1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreValidation1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelEnteteValidManager1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteValidManager1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteValidNPlus11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteValidNPlus11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteValidDRH1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteValidDRH1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoHA10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHA10 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoHA13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHA13 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoHA15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHA15 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelValidManager1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelValidManager1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle RadioHA12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHA12 As Global.Virtualia.Net.VirtualiaControle_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle RadioHA14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHA14 As Global.Virtualia.Net.VirtualiaControle_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle InfoVA16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVA16 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle LabelSeparationDemandeN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSeparationDemandeN1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreNumero21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero21 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EnteteBesoinN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EnteteBesoinN2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableLiensSitesWeb2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableLiensSitesWeb2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle SiteN21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN21 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN22 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN23 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN24 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN25 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN26 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle CadreNumero22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero22 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelLegendeN2T11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeN2T11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeN2T12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeN2T12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeN2T21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeN2T21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeN2T22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeN2T22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreEnteteDemandeN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEnteteDemandeN2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelEnteteExpressionDemande2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteExpressionDemande2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteCout2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteCout2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteDureeFormation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteDureeFormation2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEntetePeriode2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEntetePeriode2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteType2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteType2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreDemandeN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDemandeN2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TableDemandeN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDemandeN2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TableActionFormation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableActionFormation2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreAction2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreAction2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreReference2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreReference2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoHB02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHB02 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoVB01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle ListCB03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListCB03 As Global.Virtualia.Net.VirtualiaControle_VListeCombo

    '''<summary>
    '''Contrôle LabelPrecisionsN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPrecisionsN2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVB04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB04 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle LabelObjFormationN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelObjFormationN2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVB09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB09 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoHB08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHB08 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle TableDureeJours2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDureeJours2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoHB06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHB06 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelDureeJours2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDureeJours2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoHB07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHB07 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelDureeHeures2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDureeHeures2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVB05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioVB17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioVB17 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle CadreValidation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreValidation2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelEnteteValidManager2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteValidManager2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteValidNPlus12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteValidNPlus12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteValidDRH2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteValidDRH2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoHB10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHB10 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoHB13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHB13 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoHB15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHB15 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelValidManager2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelValidManager2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle RadioHB12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHB12 As Global.Virtualia.Net.VirtualiaControle_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle RadioHB14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHB14 As Global.Virtualia.Net.VirtualiaControle_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle InfoVB16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVB16 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle LabelSeparationDemandeN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSeparationDemandeN2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreNumero31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero31 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EnteteBesoinN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EnteteBesoinN3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableLiensSitesWeb3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableLiensSitesWeb3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle SiteN31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN31 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN32 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN33 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN34 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN35.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN35 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle SiteN36.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SiteN36 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle CadreNumero32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNumero32 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelLegendeN3T11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeN3T11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeN3T12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeN3T12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeN3T21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeN3T21 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeN3T22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeN3T22 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreEnteteDemandeN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEnteteDemandeN3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelEnteteExpressionDemande3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteExpressionDemande3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteCout3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteCout3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteDureeFormation3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteDureeFormation3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEntetePeriode3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEntetePeriode3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteType3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteType3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreDemandeN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDemandeN3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TableDemandeN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDemandeN3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TableActionFormation3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableActionFormation3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreAction3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreAction3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreReference3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreReference3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoHC02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHC02 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoVC01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle ListCC03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListCC03 As Global.Virtualia.Net.VirtualiaControle_VListeCombo

    '''<summary>
    '''Contrôle LabelPrecisionsN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPrecisionsN3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVC04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC04 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle LabelObjFormationN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelObjFormationN3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVC09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC09 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoHC08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHC08 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle TableDureeJours3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDureeJours3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoHC06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHC06 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelDureeJours3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDureeJours3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoHC07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHC07 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelDureeHeures3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDureeHeures3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoVC05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC05 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle RadioVC17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioVC17 As Global.Virtualia.Net.VirtualiaControle_VTrioVerticalRadio

    '''<summary>
    '''Contrôle CadreValidation3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreValidation3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelEnteteValidManager3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteValidManager3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteValidNPlus13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteValidNPlus13 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteValidDRH3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteValidDRH3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoHC10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHC10 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoHC13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHC13 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoHC15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHC15 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelValidManager3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelValidManager3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle RadioHC12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHC12 As Global.Virtualia.Net.VirtualiaControle_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle RadioHC14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioHC14 As Global.Virtualia.Net.VirtualiaControle_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle InfoVC16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVC16 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee
End Class
