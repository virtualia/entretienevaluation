﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_TRAVAIL_HONDA
    Inherits Virtualia.Net.Controles.ObjetWebCtlHonda
    Private WsDossierPER As Virtualia.Net.Entretien.DossierHonda

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo)
            VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0))
            VirVertical.V_SiAutoPostBack = False
            If VirVertical.V_SiEnLectureSeule = True Then
                VirVertical.DonBackColor = Drawing.Color.LightGray
            Else
                VirVertical.DonBackColor = Drawing.Color.White
            End If
            IndiceI += 1
        Loop

    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV30.ValeurChange, _
    InfoV31.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet

        Select Case NumObjet
            Case 150
                If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
                    WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
                End If
        End Select
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim DateAffectation As String

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur

        Etiquette.Text = "ENTRETIEN ANNUEL RELATIF A L'APPLICATION DU FORFAIT ANNUEL EN JOURS - " & WsDossierPER.KI(DatedEffet) & " KI"
        LabelIdentiteCollaborateur.Text = WsDossierPER.LibelleIdentite
        DateAffectation = WsDossierPER.DateValeur_Infos(DatedEffet)
        If WsDossierPER.NiveauAffectation(WsDossierPER.DateValeur_Infos(DateAffectation), 2) <> "" Then
            LabelDepartementCollaborateur.Text = WsDossierPER.NiveauAffectation(DateAffectation, 2)
        Else
            LabelDepartementCollaborateur.Text = WsDossierPER.NiveauAffectation(DateAffectation, 1)
        End If
        LabelFonctionCollaborateur.Text = WsDossierPER.FonctionExercee(DateAffectation, False)
        LabelIdentiteEvaluateur.Text = WsDossierPER.Evaluateur_Nom_Prenom
        LabelDepartementEvaluateur.Text = WsDossierPER.Evaluateur_Service
        LabelFonctionEvaluateur.Text = WsDossierPER.Evaluateur_Fonction
        LabelPeriodeKI.Text = WsDossierPER.PeriodeKI(DatedEffet)

        LabelVoletRevision.Text = ValeurLue(150, 3)

        Call LireLaFiche()
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop

    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
            End Select
            Return ""
        End Get
    End Property

End Class