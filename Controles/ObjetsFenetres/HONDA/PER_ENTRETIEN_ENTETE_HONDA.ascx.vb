﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_ENTETE_HONDA
    Inherits Virtualia.Net.Controles.ObjetWebCtlHonda
    Private WsDossierPER As Virtualia.Net.Entretien.DossierHonda

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
                Call ActualiserListe()
            End If
        End Set
    End Property

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            VirControle.DonText = ValeurLue(NumObjet, NumInfo)
            VirControle.Visible = WsDossierPER.SiVisible(NumObjet, NumInfo, ValeurLue(150, 0))
            If VirControle.Visible = True Then
                VirControle.V_SiEnLectureSeule = WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0))
                VirControle.V_SiAutoPostBack = Not VirControle.V_SiEnLectureSeule
                If VirControle.V_SiEnLectureSeule = True Then
                    VirControle.DonBackColor = Drawing.Color.LightGray
                Else
                    VirControle.DonBackColor = Drawing.Color.White
                End If
            End If
            IndiceI += 1
        Loop

        Dim VirRadio As VirtualiaControle_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirRadio = CType(Ctl, VirtualiaControle_VTrioHorizontalRadio)
            NumObjet = VirRadio.V_Objet
            VirRadio.Visible = WsDossierPER.SiVisible(NumObjet, NumInfo, ValeurLue(150, 0))
            If VirRadio.Visible = True Then
                VirRadio.V_SiEnLectureSeule = WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0))
                VirRadio.V_SiAutoPostBack = Not VirRadio.V_SiEnLectureSeule
            End If
            If ValeurLue(NumObjet, NumInfo) <> "" Then
                Select Case ValeurLue(NumObjet, NumInfo)
                    Case Is = "Oui"
                        VirRadio.RadioCentreCheck = True
                    Case Else
                        VirRadio.RadioGaucheCheck = True
                End Select
            End If
            IndiceI += 1
        Loop

        If WsDossierPER.SiDRH = False Then
            Select Case WsDossierPER.PeriodeKI(ValeurLue(150, 0))
                Case "DEBUT KI", "MILIEU KI", "FIN KI - SELF"
                    CadreRefusSignature.Visible = False
                    LabelEvaluationFinale.Visible = False
                    CadreLegendeNotation.Visible = False
                    CadreEvaluationFinale.Visible = False
                    CadreNotationFinale.Visible = False
                Case "FIN KI - MANAGER"
                    CadreRefusSignature.Visible = False
            End Select
        End If
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
            End Select
            Return ""
        End Get
    End Property

    Private Sub ActualiserListe()
        V_LibelListe = "Aucun entretien" & VI.Tild & "Un entretien" & VI.Tild & "entretiens"
        V_LibelColonne = "Année fiscale" & VI.Tild & "Type de formulaire" & VI.Tild & "KI" & VI.Tild & "Clef"

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            ListeGrille.V_Liste(V_LibelColonne, V_LibelListe) = V_ListeFiches
        End If
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As VirtualiaControle_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, VirtualiaControle_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH04.ValeurChange, _
        InfoH05.ValeurChange, InfoH06.ValeurChange, InfoH03.ValeurChange, _
        InfoH07.ValeurChange, InfoH08.ValeurChange, InfoH09.ValeurChange, InfoH26.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleEtiDonnee).V_Objet
        Dim Chaine As String = e.Valeur
        Dim Msg As String = ""
        Select Case NumObjet
            Case 150
                Select Case NumInfo 'Dates de signature
                    Case 3 To 9
                        If Chaine <> "" Then
                            If CDate(e.Valeur) > CDate(V_WebFonction.ViRhDates.DateduJour) Then
                                Chaine = V_WebFonction.ViRhDates.DateduJour
                            End If
                        End If
                        '*** Controles sur Signature Manager Fin KI
                        If NumInfo = 6 Then
                            If ValeurLue(150, 3) = "" Then 'Date de l'entretien final
                                Chaine = ""
                                Msg = "La date de l'entretien final doit être renseignée avant de signer"
                            End If
                            If ValeurLue(150, 26) <> "" Then 'Note révisée différente de Note évaluée
                                If ValeurLue(150, 26) <> ValeurLue(150, 25) Then
                                    Chaine = ""
                                    Msg = "Veuillez réviser la note avant de signer"
                                End If
                            End If
                        End If
                End Select
                If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> Chaine Then
                    CType(sender, VirtualiaControle_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = Chaine
                End If
                If Msg <> "" Then
                    Call EnvoyerMessage_Informatif(Msg)
                End If
        End Select
    End Sub

    Protected Sub RadioValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles RadioH10.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VTrioHorizontalRadio).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VTrioHorizontalRadio).V_Objet

        Select Case NumObjet
            Case 150
                If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
                    WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
                End If
        End Select
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 0
        ListeGrille.Centrage_Colonne(2) = 1
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DatedEffet As String
        Dim DateAffectation As String

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Call ActualiserListe()

        If WsDossierPER.Objet_150 Is Nothing Then
            DatedEffet = V_WebFonction.ViRhDates.DateduJour
        Else
            DatedEffet = WsDossierPER.Objet_150.Date_de_Valeur
        End If
        DateAffectation = WsDossierPER.DateValeur_Infos(DatedEffet)
        Etiquette.Text = "ENTRETIEN PROFESSIONNEL - " & WsDossierPER.KI(DatedEffet) & " KI"
        LabelIdentiteCollaborateur.Text = WsDossierPER.LibelleIdentite
        If WsDossierPER.NiveauAffectation(WsDossierPER.DateValeur_Infos(DateAffectation), 2) <> "" Then
            LabelDepartementCollaborateur.Text = WsDossierPER.NiveauAffectation(DateAffectation, 2)
        Else
            LabelDepartementCollaborateur.Text = WsDossierPER.NiveauAffectation(DateAffectation, 1)
        End If
        LabelFonctionCollaborateur.Text = WsDossierPER.FonctionExercee(DateAffectation, False)
        LabelIdentiteEvaluateur.Text = WsDossierPER.Evaluateur_Nom_Prenom
        LabelDepartementEvaluateur.Text = WsDossierPER.Evaluateur_Service
        LabelFonctionEvaluateur.Text = WsDossierPER.Evaluateur_Fonction
        LabelPeriodeKI.Text = WsDossierPER.PeriodeKI(DatedEffet)

        LabelVoletRevision.Text = ValeurLue(150, 3)
        EvaluationNoteGlobale.Text = WsDossierPER.Note_Globale_Lettre
        EvaluationPerformanceGlobale.Text = ValeurLue(150, 18)
        EvaluationComportementGlobale.Text = ValeurLue(150, 21)
        EvaluationTotaleGlobale.Text = ValeurLue(150, 24)
        EvaluationNoteGlobale.ToolTip = WsDossierPER.Tooltip_NoteLettre(EvaluationNoteGlobale.Text)

        LabelLegendeNoteAPlus2.Text = WsDossierPER.Label_NoteChiffre("A*")
        LabelLegendeNoteA2.Text = WsDossierPER.Label_NoteChiffre("A")
        LabelLegendeNoteBPlus2.Text = WsDossierPER.Label_NoteChiffre("B+")
        LabelLegendeNoteB2.Text = WsDossierPER.Label_NoteChiffre("B")
        LabelLegendeNoteBMoins2.Text = WsDossierPER.Label_NoteChiffre("B-")
        LabelLegendeNoteC2.Text = WsDossierPER.Label_NoteChiffre("C")

        Call LireLaFiche()

    End Sub

    Private Sub EnvoyerMessage_Informatif(ByVal Msg As String)
        Dim BoutonOK As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        BoutonOK = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(Me.ID, "OK", Msg)
        V_MessageDialog(BoutonOK)
    End Sub
End Class