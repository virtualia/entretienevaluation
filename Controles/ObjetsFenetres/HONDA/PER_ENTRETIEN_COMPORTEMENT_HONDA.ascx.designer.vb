﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaFenetre_PER_ENTRETIEN_COMPORTEMENT_HONDA

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreEnteteComportement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEnteteComportement As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletTitre4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletTitre4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelIdentiteCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelIdentiteCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDepartementCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDepartementCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelFonctionCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFonctionCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelIdentiteEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelIdentiteEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDepartementEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDepartementEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelFonctionEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFonctionEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletPeriodeKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletPeriodeKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelPeriodeKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPeriodeKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelVoletRevision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVoletRevision As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDateRevision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateRevision As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEvaluationComportement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvaluationComportement As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelCommentaireEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCommentaireEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV22 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle EnteteElementEvaluation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EnteteElementEvaluation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TitreEnteteElementEvaluation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEnteteElementEvaluation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreEnteteVide.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEnteteVide As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreEnteteCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEnteteCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreEnteteEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEnteteEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreElementsEvaluation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreElementsEvaluation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneElementEvaluation1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneElementEvaluation1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVK01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVK01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle labelInfosElement01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents labelInfosElement01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoCK04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCK04 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCK03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCK03 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneElementEvaluation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneElementEvaluation2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVL01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVL01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle labelInfosElement02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents labelInfosElement02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoCL04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCL04 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCL03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCL03 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneElementEvaluation3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneElementEvaluation3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVM01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVM01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle labelInfosElement03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents labelInfosElement03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoCM04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCM04 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCM03 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneElementEvaluation4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneElementEvaluation4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVN01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVN01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle labelInfosElement04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents labelInfosElement04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoCN04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCN04 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCN03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCN03 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneElementEvaluation5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneElementEvaluation5 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVO01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVO01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle labelInfosElement05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents labelInfosElement05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoCO04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCO04 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCO03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCO03 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneElementEvaluation6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneElementEvaluation6 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVP01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVP01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle labelInfosElement06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents labelInfosElement06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoCP04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCP04 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCP03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCP03 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneElementEvaluation7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneElementEvaluation7 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVQ01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVQ01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle labelInfosElement07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents labelInfosElement07 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoCQ04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCQ04 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCQ03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCQ03 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneElementEvaluation8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneElementEvaluation8 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVR01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVR01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle labelInfosElement08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents labelInfosElement08 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoCR04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCR04 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCR03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCR03 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneElementEvaluation9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneElementEvaluation9 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVS01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVS01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle labelInfosElement09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents labelInfosElement09 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoCS04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCS04 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCS03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCS03 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle LigneElementEvaluation10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneElementEvaluation10 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoVT01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVT01 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle labelInfosElement10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents labelInfosElement10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoCT04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCT04 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle InfoCT03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoCT03 As Global.Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande

    '''<summary>
    '''Contrôle EnteteElementEvaluationBas.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EnteteElementEvaluationBas As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TitreEnteteElementEvaluationBas.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEnteteElementEvaluationBas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreEnteteVideBas.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEnteteVideBas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreEnteteCollaborateurBas.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEnteteCollaborateurBas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreEnteteEvaluateurBas.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEnteteEvaluateurBas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreEvaluationComportement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEvaluationComportement As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TitreEvaluationComportement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEvaluationComportement As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluationComportementCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluationComportementCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluationComportementEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluationComportementEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreEvaluationGlobale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEvaluationGlobale As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TitreEvaluationGlobale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEvaluationGlobale As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluationGlobaleCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluationGlobaleCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluationGlobaleEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluationGlobaleEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelResumeDeveloppement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelResumeDeveloppement As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV27 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle CadreBesoinFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreBesoinFormation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelBesoinFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelBesoinFormation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle RadioH15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioH15 As Global.Virtualia.Net.VirtualiaControle_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle LabelBesoinFormationDroite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelBesoinFormationDroite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelCommentaireCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCommentaireCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV28 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle LabelCommentairesEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCommentairesEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV29 As Global.Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle LabelEvaluationFinale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEvaluationFinale As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreLegendeNotation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreLegendeNotation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelLegendeGauche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeGauche As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeNoteC1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeNoteC1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeNoteC2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeNoteC2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeNoteBMoins1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeNoteBMoins1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeNoteBMoins2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeNoteBMoins2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeNoteB1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeNoteB1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeNoteB2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeNoteB2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeNoteBPlus1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeNoteBPlus1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeNoteBPlus2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeNoteBPlus2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeNoteA1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeNoteA1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeNoteA2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeNoteA2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeNoteAPlus1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeNoteAPlus1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeNoteAPlus2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeNoteAPlus2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelLegendeDroite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLegendeDroite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreEvaluationFinale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEvaluationFinale As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TitreEvaluationGlobaleGauche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEvaluationGlobaleGauche As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreEvaluationPerformanceGlobale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEvaluationPerformanceGlobale As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreEvaluationComportementGlobale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEvaluationComportementGlobale As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreEvaluationTotaleGlobale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEvaluationTotaleGlobale As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreEvaluationNoteGlobale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEvaluationNoteGlobale As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreEvaluationGlobaleDroite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreEvaluationGlobaleDroite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreNotationFinale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreNotationFinale As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EvaluationGlobaleGauche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluationGlobaleGauche As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluationPerformanceGlobale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluationPerformanceGlobale As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluationComportementGlobale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluationComportementGlobale As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluationTotaleGlobale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluationTotaleGlobale As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluationNoteGlobale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluationNoteGlobale As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EvaluationGlobaleDroite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EvaluationGlobaleDroite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreEnteteSignatureEvaluation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEnteteSignatureEvaluation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TitreSignatureGauche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreSignatureGauche As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreSignatureCollaborateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreSignatureCollaborateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreSignaturePeriodeKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreSignaturePeriodeKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreSignatureEvaluateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreSignatureEvaluateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TitreSignatureDroite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreSignatureDroite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreSignatureDebutKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSignatureDebutKI As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelSignatureGaucheDebutKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSignatureGaucheDebutKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH07 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelSignatureDebutKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSignatureDebutKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH04 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelSignatureDroiteDebutKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSignatureDroiteDebutKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreSignatureMilieuKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSignatureMilieuKI As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelSignatureGaucheMilieuKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSignatureGaucheMilieuKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH08 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelSignatureMilieuKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSignatureMilieuKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH05 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelSignatureDroiteMilieuKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSignatureDroiteMilieuKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreSignatureFinKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSignatureFinKI As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelSignatureGaucheFinKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSignatureGaucheFinKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH09 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelSignatureFinKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSignatureFinKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH06 As Global.Virtualia.Net.VirtualiaControle_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelSignatureDroiteFinKI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSignatureDroiteFinKI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreRefusSignature.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreRefusSignature As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelRefusSignature.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelRefusSignature As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle RadioH10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioH10 As Global.Virtualia.Net.VirtualiaControle_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle LabelRefusSignatureDroite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelRefusSignatureDroite As Global.System.Web.UI.WebControls.Label
End Class
