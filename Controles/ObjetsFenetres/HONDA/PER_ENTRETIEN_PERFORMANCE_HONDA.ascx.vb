﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_ENTRETIEN_PERFORMANCE_HONDA
    Inherits Virtualia.Net.Controles.ObjetWebCtlHonda
    Private WsDossierPER As Virtualia.Net.Entretien.DossierHonda

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NumObjet As Integer
        Dim Rang As String
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            VirVertical.Visible = WsDossierPER.SiVisible(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            If VirVertical.Visible = True Then
                VirVertical.V_SiEnLectureSeule = WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0, ""))
                If VirVertical.V_SiEnLectureSeule = True Then
                    VirVertical.DonBackColor = Drawing.Color.LightGray
                Else
                    VirVertical.DonBackColor = Drawing.Color.White
                End If
            End If
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirTextCommande As VirtualiaControle_VTrioEtiTextCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoC", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Rang = ""
            VirTextCommande = CType(Ctl, VirtualiaControle_VTrioEtiTextCommande)
            NumObjet = VirTextCommande.V_Objet
            Rang = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(Ctl.ID, 6, 1))
            VirTextCommande.Visible = WsDossierPER.SiVisible(NumObjet, NumInfo, ValeurLue(150, 0, ""))
            Select Case NumInfo
                Case 8, 12
                    VirTextCommande.CmdVisible = False
                    VirTextCommande.DonText = ValeurLue(NumObjet, NumInfo, Rang)
                Case Else
                    VirTextCommande.CmdVisible = Not (WsDossierPER.SiReadonly(NumObjet, NumInfo, ValeurLue(150, 0, "")))
                    VirTextCommande.DonText = ValeurLue(NumObjet, NumInfo, Rang)
            End Select
            IndiceI += 1
        Loop

        If WsDossierPER.SiDRH = False Then
            Select Case WsDossierPER.PeriodeKI(ValeurLue(150, 0, ""))
                Case "DEBUT KI", "MILIEU KI", "FIN KI - SELF"
                    CadrePerformanceCollaborateur.Visible = False
                    CadrePerformanceEvaluateur.Visible = False
                    LabelTitreDifficulte.Visible = False
            End Select
        End If
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV11.ValeurChange, _
        InfoV12.ValeurChange, InfoV13.ValeurChange, InfoV14.ValeurChange, InfoV19.ValeurChange, _
        InfoVK02.ValeurChange, InfoVK03.ValeurChange, InfoVK04.ValeurChange, _
        InfoVL02.ValeurChange, InfoVL03.ValeurChange, InfoVL04.ValeurChange, _
        InfoVM02.ValeurChange, InfoVM03.ValeurChange, InfoVM04.ValeurChange, _
        InfoVN02.ValeurChange, InfoVN03.ValeurChange, InfoVN04.ValeurChange, _
        InfoVO02.ValeurChange, InfoVO03.ValeurChange, InfoVO04.ValeurChange, _
        InfoVP02.ValeurChange, InfoVP03.ValeurChange, InfoVP04.ValeurChange, _
        InfoVQ02.ValeurChange, InfoVQ03.ValeurChange, InfoVQ04.ValeurChange, _
        InfoVR02.ValeurChange, InfoVR03.ValeurChange, InfoVR04.ValeurChange, _
        InfoVS02.ValeurChange, InfoVS03.ValeurChange, InfoVS04.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 6, 1))

        Select Case NumObjet
            Case 150
                If WsDossierPER.Objet_150.V_TableauData(NumInfo).ToString <> e.Valeur Then
                    WsDossierPER.TableauMaj(NumObjet, NumInfo, "") = e.Valeur
                End If
            Case 151
                If WsDossierPER.Objet_151(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_151(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                    End If
                End If
        End Select
    End Sub

    Protected Sub InfoC_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles _
        InfoCK05.ValeurChange, InfoCK06.ValeurChange, InfoCK07.ValeurChange, InfoCK08.ValeurChange, InfoCK09.ValeurChange, InfoCK10.ValeurChange, InfoCK11.ValeurChange, InfoCK12.ValeurChange, _
        InfoCL05.ValeurChange, InfoCL06.ValeurChange, InfoCL07.ValeurChange, InfoCL08.ValeurChange, InfoCL09.ValeurChange, InfoCL10.ValeurChange, InfoCL11.ValeurChange, InfoCL12.ValeurChange, _
        InfoCM05.ValeurChange, InfoCM06.ValeurChange, InfoCM07.ValeurChange, InfoCM08.ValeurChange, InfoCM09.ValeurChange, InfoCM10.ValeurChange, InfoCM11.ValeurChange, InfoCM12.ValeurChange, _
        InfoCN05.ValeurChange, InfoCN06.ValeurChange, InfoCN07.ValeurChange, InfoCN08.ValeurChange, InfoCN09.ValeurChange, InfoCN10.ValeurChange, InfoCN11.ValeurChange, InfoCN12.ValeurChange, _
        InfoCO05.ValeurChange, InfoCO06.ValeurChange, InfoCO07.ValeurChange, InfoCO08.ValeurChange, InfoCO09.ValeurChange, InfoCO10.ValeurChange, InfoCO11.ValeurChange, InfoCO12.ValeurChange, _
        InfoCP05.ValeurChange, InfoCP06.ValeurChange, InfoCP07.ValeurChange, InfoCP08.ValeurChange, InfoCP09.ValeurChange, InfoCP10.ValeurChange, InfoCP11.ValeurChange, InfoCP12.ValeurChange, _
        InfoCQ05.ValeurChange, InfoCQ06.ValeurChange, InfoCQ07.ValeurChange, InfoCQ08.ValeurChange, InfoCQ09.ValeurChange, InfoCQ10.ValeurChange, InfoCQ11.ValeurChange, InfoCQ12.ValeurChange, _
        InfoCR05.ValeurChange, InfoCR06.ValeurChange, InfoCR07.ValeurChange, InfoCR08.ValeurChange, InfoCR09.ValeurChange, InfoCR10.ValeurChange, InfoCR11.ValeurChange, InfoCR12.ValeurChange, _
        InfoCS05.ValeurChange, InfoCS06.ValeurChange, InfoCS07.ValeurChange, InfoCS08.ValeurChange, InfoCS09.ValeurChange, InfoCS10.ValeurChange, InfoCS11.ValeurChange, InfoCS12.ValeurChange

        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VTrioEtiTextCommande).ID, 2))
        Dim NumObjet As Integer = CType(sender, VirtualiaControle_VTrioEtiTextCommande).V_Objet
        Dim Rang As String = V_WebFonction.PointeurGlobal.VirRangTranslate(NumObjet, Strings.Mid(CType(sender, VirtualiaControle_VTrioEtiTextCommande).ID, 6, 1))

        Select Case NumObjet
            Case 151
                If WsDossierPER.Objet_151(Rang) IsNot Nothing Then
                    If WsDossierPER.Objet_151(Rang).V_TableauData(NumInfo).ToString <> e.Valeur Then
                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = e.Valeur
                        Select Case NumInfo
                            Case 5, 9
                                If IsNumeric(e.Valeur) = False Then
                                    WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = "0"
                                End If
                                If NumInfo = 5 Then
                                    If WsDossierPER.Total_Poids(False) > 100 Then
                                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = "0"
                                    End If
                                Else
                                    If WsDossierPER.Total_Poids(True) > 100 Then
                                        WsDossierPER.TableauMaj(NumObjet, NumInfo, Rang) = "0"
                                    End If
                                End If
                        End Select
                    End If
                End If
        End Select
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim DateAffectation As String
        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")


        WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
        If WsDossierPER Is Nothing Then
            Exit Sub
        End If

        Etiquette.Text = "PERFORMANCE DU COLLABORATEUR - " & WsDossierPER.KI(ValeurLue(150, 0, "")) & " KI"
        LabelIdentiteCollaborateur.Text = WsDossierPER.LibelleIdentite
        DateAffectation = WsDossierPER.DateValeur_Infos(ValeurLue(150, 0, ""))
        If WsDossierPER.NiveauAffectation(WsDossierPER.DateValeur_Infos(DateAffectation), 2) <> "" Then
            LabelDepartementCollaborateur.Text = WsDossierPER.NiveauAffectation(DateAffectation, 2)
        Else
            LabelDepartementCollaborateur.Text = WsDossierPER.NiveauAffectation(DateAffectation, 1)
        End If
        LabelFonctionCollaborateur.Text = WsDossierPER.FonctionExercee(DateAffectation, False)
        LabelIdentiteEvaluateur.Text = WsDossierPER.Evaluateur_Nom_Prenom
        LabelDepartementEvaluateur.Text = WsDossierPER.Evaluateur_Service
        LabelFonctionEvaluateur.Text = WsDossierPER.Evaluateur_Fonction

        LabelPeriodeKI.Text = WsDossierPER.PeriodeKI(ValeurLue(150, 0, ""))
        LabelVoletRevision.Text = ValeurLue(150, 3, "")

        Dim VirTextCommande As VirtualiaControle_VTrioEtiTextCommande
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim NumInfo As Integer
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoC", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            Select Case NumInfo
                Case 6, 8, 10, 12
                    VirTextCommande = CType(Ctl, VirtualiaControle_VTrioEtiTextCommande)
                    VirTextCommande.V_Strict = False
                    VirTextCommande.V_TypeCtrl = VirtualiaControle_VTrioEtiTextCommande.TypeTrioEti.Bornee
                    VirTextCommande.V_Suffixe = ""
                    Select Case ValeurLue(150, 2, "") 'Type du formulaire
                        Case "Manager"
                            VirTextCommande.DonTooltip = "Résultat atteint - | 1 | 2 | 3 | 4 | 5 |" & vbCrLf & "Valeur moyenne 3"
                            VirTextCommande.V_ValMax = 5
                            VirTextCommande.V_ValMin = 1
                            VirTextCommande.V_Pas = 1
                        Case Else
                            VirTextCommande.DonTooltip = "Résultat atteint - | 0 | 15 | 30 | 45 | 60 |" & vbCrLf & "Valeur moyenne 30"
                            VirTextCommande.V_ValMax = 60
                            VirTextCommande.V_ValMin = 0
                            VirTextCommande.V_Pas = 15
                    End Select
            End Select
            IndiceI += 1
        Loop

        SommePoidsObjectifsCollaborateur.Text = WsDossierPER.Total_Poids(True).ToString & " %"
        LabelPerformanceCollaborateur.Text = Strings.Format(WsDossierPER.Total_Performance(True), "0.00")
        SommePoidsObjectifsEvaluateur.Text = WsDossierPER.Total_Poids(False).ToString & " %"
        LabelPerformanceEvaluateur.Text = Strings.Format(WsDossierPER.Total_Performance(False), "0.00")

        Select Case ValeurLue(150, 2, "") 'Type du formulaire
            Case "Manager"
                Etiquette.Text = "PERFORMANCE DU MANAGER - " & WsDossierPER.KI(ValeurLue(150, 0, "")) & " KI"
                LabelTitreExigence.Visible = False
                LabelCommentaireExigence.Visible = False
                InfoV14.Visible = False
                EnteteObjectifSociete1.Visible = True
                EnteteObjectifSociete2.Visible = True
                CadreObjectifSociete.Visible = True
                LigneObjFonction5.Visible = False

                TitrePerformanceCollaborateur.Text = "1. Evaluation des activités (Collaborateur) : Maximum 5 points"
                TitrePerformanceEvaluateur.Text = "1. Evaluation des activités (Evaluateur) : Maximum 5 points"

            Case Else
                LabelTitreExigence.Visible = True
                LabelCommentaireExigence.Visible = True
                InfoV14.Visible = True
                EnteteObjectifSociete1.Visible = False
                EnteteObjectifSociete2.Visible = False
                CadreObjectifSociete.Visible = False
                LigneObjFonction5.Visible = True

                TitrePerformanceCollaborateur.Text = "1. Evaluation des activités (Collaborateur) : Maximum 60 points"
                TitrePerformanceEvaluateur.Text = "1. Evaluation des activités (Evaluateur) : Maximum 60 points"

        End Select

        Call LireLaFiche()
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop

        Dim VirTextCommande As VirtualiaControle_VTrioEtiTextCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoC", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirTextCommande = CType(Ctl, VirtualiaControle_VTrioEtiTextCommande)
            VirTextCommande.DonBackColor = Drawing.Color.White
            VirTextCommande.DonText = ""
            IndiceI += 1
        Loop

    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Rang As String) As String
        Get
            If WsDossierPER Is Nothing Then
                WsDossierPER = V_WebFonction.ContexteSession(Session.SessionID).DossierHonda
                If WsDossierPER Is Nothing Then
                    Return ""
                End If
            End If
            If WsDossierPER.Objet_150 Is Nothing Then
                Return ""
            End If
            Select Case NoObjet
                Case 150
                    Return WsDossierPER.Objet_150.V_TableauData(NoInfo).ToString
                Case 151
                    If WsDossierPER.Objet_151(Rang) IsNot Nothing Then
                        Return WsDossierPER.Objet_151(Rang).V_TableauData(NoInfo).ToString
                    End If
            End Select
            Return ""
        End Get
    End Property

End Class