﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class VirtualiaControle_VListeCombo
    Inherits Virtualia.Net.Controles.ObjetWebControlSaisie
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    '***********************************************************
    'Gestion
    Private WsNomTable As String
    Private WsCritereListe As String
    Private WsSiLigneBlanche As Boolean = False
    'Execution
    Private WsPageArrayList As ArrayList

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property V_NomTable() As String
        Get
            Return WsNomTable
        End Get
        Set(ByVal value As String)
            WsNomTable = value
        End Set
    End Property

    Public Property V_CritereListe() As String
        Get
            Return WsCritereListe
        End Get
        Set(ByVal value As String)
            If value = "" Then
                WsCritereListe = ""
                Exit Property
            End If
            WsCritereListe = value
            If (Me.ViewState("EtatCourant") IsNot Nothing) Then
                WsPageArrayList = CType(Me.ViewState("EtatCourant"), ArrayList)
                Me.ViewState.Remove("EtatCourant")
            Else
                WsPageArrayList = CreateArray()
            End If
            WsPageArrayList(3) = value
            Me.ViewState.Add("EtatCourant", WsPageArrayList)
        End Set
    End Property

    Public Property SiLigneBlanche() As Boolean
        Get
            Return WsSiLigneBlanche
        End Get
        Set(ByVal value As Boolean)
            WsSiLigneBlanche = value
        End Set
    End Property

    Public WriteOnly Property V_Liste(ByVal Libelles As String) As String
        Set(ByVal value As String)
            If Me.ViewState("EtatCourant") Is Nothing Then
                WsPageArrayList = CreateArray()
            Else
                WsPageArrayList = CType(Me.ViewState("EtatCourant"), ArrayList)
                Me.ViewState.Remove("EtatCourant")
            End If
            WsPageArrayList(5) = value
            If Libelles = "" Then
                Libelles = "Aucune situation" & VI.Tild & "Une situation" & VI.Tild & "situations"
            End If
            WsPageArrayList(6) = Libelles
            Me.ViewState.Add("EtatCourant", WsPageArrayList)
        End Set
    End Property

    Public Property EtiText() As String
        Get
            Return Etiquette.Text
        End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Public Property EtiHeight() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Height = value
        End Set
    End Property

    Public Property EtiWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Width = value
        End Set
    End Property

    Public Property EtiBackColor() As System.Drawing.Color
        Get
            Return Etiquette.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor() As System.Drawing.Color
        Get
            Return Etiquette.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.ForeColor = value
        End Set
    End Property

    Public Property EtiBorderColor() As System.Drawing.Color
        Get
            Return Etiquette.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BorderColor = value
        End Set
    End Property

    Public Property EtiBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.BorderWidth = value
        End Set
    End Property

    Public Property EtiBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return Etiquette.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Etiquette.BorderStyle = value
        End Set
    End Property

    Public Property EtiTooltip() As String
        Get
            Return Etiquette.ToolTip
        End Get
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property

    Public Property EtiVisible() As Boolean
        Get
            Return Etiquette.Visible
        End Get
        Set(ByVal value As Boolean)
            Etiquette.Visible = value
        End Set
    End Property

    Public WriteOnly Property EtiStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Etiquette.Style.Remove(Strings.Trim(TableauW(0)))
                Etiquette.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property LstStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                VComboBox.Style.Remove(Strings.Trim(TableauW(0)))
                VComboBox.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property LstText() As String
        Get
            Return VComboBox.Text
        End Get
        Set(ByVal value As String)
            If (Me.ViewState("EtatCourant") IsNot Nothing) Then
                WsPageArrayList = CType(Me.ViewState("EtatCourant"), ArrayList)
                Me.ViewState.Remove("EtatCourant")
            Else
                WsPageArrayList = CreateArray()
            End If
            WsPageArrayList(4) = value
            Me.ViewState.Add("EtatCourant", WsPageArrayList)
            Try
                VComboBox.Items.FindByText(value).Selected = True
                VComboBox.Text = value
            Catch ex As Exception
                Exit Property
            End Try
        End Set
    End Property

    Public Property LstHeight() As System.Web.UI.WebControls.Unit
        Get
            Return VComboBox.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            VComboBox.Height = value
        End Set
    End Property

    Public Property LstWidth() As System.Web.UI.WebControls.Unit
        Get
            Return VComboBox.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            VComboBox.Width = value
        End Set
    End Property

    Public Property LstBackColor() As System.Drawing.Color
        Get
            Return VComboBox.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VComboBox.BackColor = value
        End Set
    End Property

    Public Property LstForeColor() As System.Drawing.Color
        Get
            Return VComboBox.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VComboBox.ForeColor = value
        End Set
    End Property

    Public Property LstBorderColor() As System.Drawing.Color
        Get
            Return VComboBox.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VComboBox.BorderColor = value
        End Set
    End Property

    Public Property LstBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return VComboBox.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            VComboBox.BorderWidth = value
        End Set
    End Property

    Public Property LstBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return VComboBox.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            VComboBox.BorderStyle = value
        End Set
    End Property

    Public Property LstVisible() As Boolean
        Get
            Return VComboBox.Visible
        End Get
        Set(ByVal value As Boolean)
            VComboBox.Visible = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If V_SiDonneeDico = True Then
            Etiquette.BackColor = V_EtiBackcolor
            Etiquette.ForeColor = V_EtiForecolor
            Etiquette.BorderColor = V_EtiBordercolor
            VComboBox.BorderColor = V_DonBordercolor

            Etiquette.Font.Name = V_FontName
            Etiquette.Font.Size = V_FontTaille
            Etiquette.Font.Bold = V_FontBold
            Etiquette.Font.Italic = V_FontItalic
            VComboBox.Font.Name = V_FontName
        End If
        Call FaireListe()
    End Sub

    Private Sub FaireListe()
        Dim IndiceI As Integer
        Dim TableauData(0) As String

        If (Me.ViewState("EtatCourant") IsNot Nothing) Then
            WsPageArrayList = CType(Me.ViewState("EtatCourant"), ArrayList)
        Else
            WsPageArrayList = CreateArray()
            Me.ViewState.Add("EtatCourant", WsPageArrayList)
        End If

        V_PointdeVue = CShort(WsPageArrayList(0))
        V_Objet = CShort(WsPageArrayList(1))
        If WsPageArrayList(2) IsNot Nothing Then
            WsNomTable = WsPageArrayList(2).ToString
        End If
        If WsPageArrayList(3) IsNot Nothing Then
            WsCritereListe = WsPageArrayList(3).ToString
        End If

        VComboBox.Items.Clear()
        If WsPageArrayList(5) IsNot Nothing Then
            TableauData = Strings.Split(WsPageArrayList(5).ToString, VI.Tild, -1)
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    If SiLigneBlanche = False Then
                        Exit For
                    End If
                End If
                VComboBox.Items.Add(TableauData(IndiceI))
            Next IndiceI
        End If
        If EtiText = "" Then
            TableauData = Strings.Split(WsPageArrayList(6).ToString, VI.Tild, -1)
            Select Case VComboBox.Items.Count
                Case Is = 0
                    Etiquette.Text = "Aucune valeur"
                Case Is = 1
                    Etiquette.Text = "Une valeur"
                Case Else
                    Etiquette.Text = VComboBox.Items.Count.ToString & Space(1) & "valeurs"
            End Select
        End If
        If VComboBox.Items.Count > 0 Then
            If WsPageArrayList(4).ToString = "" Then
                VComboBox.SelectedIndex = VComboBox.Items.Count - 1
            Else
                Try
                    VComboBox.Items.FindByText(WsPageArrayList(4).ToString).Selected = True
                    VComboBox.Text = WsPageArrayList(4).ToString
                Catch ex As Exception
                    Exit Try
                End Try
            End If
        End If
    End Sub

    Private Function CreateArray() As ArrayList
        Dim VCache As ArrayList
        VCache = New ArrayList(6)

        VCache.Add(V_PointdeVue.ToString) '0
        VCache.Add(V_Objet.ToString) '1
        VCache.Add(V_NomTable) '2
        VCache.Add(V_CritereListe) '3
        VCache.Add(LstText) '4

        Select Case V_NomTable '5
            Case Is = "PointdeVue"
                VCache.Add(ListePointdevue)
            Case Is = "ListeObjets"
                VCache.Add(ListeObjets)
            Case Is = "ListeInfos"
                VCache.Add(ListeInfosPer)
            Case Is = "TypeSgbdr"
                VCache.Add(ListeTypeSgbd)
            Case Else
                If V_NomTable <> "" Then
                    VCache.Add(ListeSysReference)
                Else
                    VCache.Add("")
                End If
        End Select
        VCache.Add("")
        Return VCache
    End Function

    Private Function ListeSysReference() As String
        Dim Chaine As New System.Text.StringBuilder
        Dim TableauObjet(0) As String
        Dim TableauData(0) As String
        Dim IndiceK As Integer

        Select Case V_PointdeVue
            Case Is = 2
                TableauObjet = Strings.Split(V_WebFonction.PointeurUtilisateur.LireTableGeneraleSimple(V_NomTable), VI.SigneBarre, -1)
            Case Else
                TableauObjet = Strings.Split(V_WebFonction.PointeurUtilisateur.LireTableSysReference(V_PointdeVue), VI.SigneBarre, -1)
        End Select
        For IndiceK = 0 To TableauObjet.Count - 1
            If TableauObjet(IndiceK) = "" Then
                Exit For
            End If
            TableauData = Strings.Split(TableauObjet(IndiceK), VI.Tild, -1)
            Chaine.Append(TableauData(1) & VI.Tild)
        Next IndiceK
        Return Chaine.ToString
    End Function

    Private Function ListeTypeSgbd() As String
        Dim Chaine As New System.Text.StringBuilder

        Chaine.Append(VI.TypeSgbdrNumeric.SgbdrAccess & " - " & VI.XmlSgbdrAccess & VI.Tild)
        Chaine.Append(VI.TypeSgbdrNumeric.SgbdrSqlServer & " - " & VI.XmlSgbdrSqlServer & VI.Tild)
        Chaine.Append(VI.TypeSgbdrNumeric.SgbdrOracle & " - " & VI.XmlSgbdrOracle & VI.Tild)
        Chaine.Append(VI.TypeSgbdrNumeric.SgbdrDb2 & " - " & VI.XmlSgbdrDb2 & VI.Tild)
        Chaine.Append(VI.TypeSgbdrNumeric.SgbdrMySql & " - " & VI.XmlSgbdrMySql & VI.Tild)
        Return Chaine.ToString
    End Function

    Private Function ListePointdevue() As String
        Dim IndiceA As Integer
        Dim Chaine As New System.Text.StringBuilder
        Dim AppObjetGlobal As Virtualia.Net.WebAppli.ObjetGlobal

        AppObjetGlobal = CType(Application.Item("VirGlobales"), Virtualia.Net.WebAppli.ObjetGlobal)

        Chaine.Append("(Tous)" & VI.Tild)
        For IndiceA = 0 To AppObjetGlobal.VirModele.NombredePointdeVue - 1
            Chaine.Append(AppObjetGlobal.VirModele.Item(IndiceA).Intitule & VI.Tild)
        Next IndiceA
        Chaine.Append("Uniquement le système de référence")
        Return Chaine.ToString
    End Function

    Private Function ListeObjets() As String
        Dim IndiceP As Integer
        Dim IndiceO As Integer
        Dim Chaine As New System.Text.StringBuilder
        Dim AppObjetGlobal As Virtualia.Net.WebAppli.ObjetGlobal
        Dim Tableaudata(0) As String

        AppObjetGlobal = CType(Application.Item("VirGlobales"), Virtualia.Net.WebAppli.ObjetGlobal)

        Chaine.Append("(Tous)" & VI.Tild)
        Select Case V_CritereListe
            Case Is = "", "(Tous)", "Uniquement le système de référence"
                Return Chaine.ToString
                Exit Function
        End Select
        If V_CritereListe = "1" Then
            Try
                For IndiceP = 0 To AppObjetGlobal.VirModele.NombredePointdeVue - 1
                    Select Case AppObjetGlobal.VirModele.Item(IndiceP).Numero
                        Case Is = VI.PointdeVue.PVueApplicatif
                            For IndiceO = 0 To AppObjetGlobal.VirModele.Item(IndiceP).NombredObjets - 1
                                If AppObjetGlobal.VirModele.Item(IndiceP).Item(IndiceO).SiAuMoinsuneInfodeReference = True Then
                                    Chaine.Append(AppObjetGlobal.VirModele.Item(IndiceP).Item(IndiceO).Numero & " - " & AppObjetGlobal.VirModele.Item(IndiceP).Item(IndiceO).Intitule & VI.Tild)
                                End If
                            Next IndiceO
                            Exit For
                    End Select
                Next IndiceP
            Catch ex As Exception
                Return Chaine.ToString
                Exit Function
            End Try
        Else
            Tableaudata = Strings.Split(V_CritereListe, " - ", -1)
            Try
                For IndiceP = 0 To AppObjetGlobal.VirModele.NombredePointdeVue - 1
                    Select Case AppObjetGlobal.VirModele.Item(IndiceP).Numero
                        Case Is = CShort(Tableaudata(0))
                            For IndiceO = 0 To AppObjetGlobal.VirModele.Item(IndiceP).NombredObjets - 1
                                Chaine.Append(AppObjetGlobal.VirModele.Item(IndiceP).Item(IndiceO).Numero & " - " & AppObjetGlobal.VirModele.Item(IndiceP).Item(IndiceO).Intitule & VI.Tild)
                            Next IndiceO
                            Exit For
                    End Select
                Next IndiceP
            Catch ex As Exception
                Return Chaine.ToString
                Exit Function
            End Try
        End If
        Return Chaine.ToString
    End Function

    Private Function ListeInfosPer() As String
        Dim IndiceP As Integer
        Dim IndiceO As Integer
        Dim IndiceI As Integer
        Dim Chaine As New System.Text.StringBuilder
        Dim AppObjetGlobal As Virtualia.Net.WebAppli.ObjetGlobal
        Dim Tableaudata(0) As String

        AppObjetGlobal = CType(Application.Item("VirGlobales"), Virtualia.Net.WebAppli.ObjetGlobal)

        Select Case V_CritereListe
            Case Is = ""
                Return Chaine.ToString
                Exit Function
        End Select
        Tableaudata = Strings.Split(V_CritereListe, " - ", -1)
        Try
            For IndiceP = 0 To AppObjetGlobal.VirModele.NombredePointdeVue - 1
                Select Case AppObjetGlobal.VirModele.Item(IndiceP).Numero
                    Case Is = VI.PointdeVue.PVueApplicatif
                        For IndiceO = 0 To AppObjetGlobal.VirModele.Item(IndiceP).NombredObjets - 1
                            If AppObjetGlobal.VirModele.Item(IndiceP).Item(IndiceO).Numero = _
                                CShort(Tableaudata(0)) Then
                                For IndiceI = 0 To AppObjetGlobal.VirModele.Item(IndiceP).Item(IndiceO).NombredInformations - 1
                                    If AppObjetGlobal.VirModele.Item(IndiceP).Item(IndiceO).Item(IndiceI).PointdeVueInverse > 0 Then
                                        Chaine.Append(AppObjetGlobal.VirModele.Item(IndiceP).Item(IndiceO).Item(IndiceI).Numero _
                                            & " - " & AppObjetGlobal.VirModele.Item(IndiceP).Item(IndiceO).Item(IndiceI).Intitule & VI.Tild)
                                    End If
                                Next IndiceI
                                Exit For
                            End If
                        Next IndiceO
                End Select
            Next IndiceP
        Catch ex As Exception
            Return Chaine.ToString
            Exit Function
        End Try
        Return Chaine.ToString
    End Function

    Protected Sub VComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VComboBox.SelectedIndexChanged
        If Me.ViewState("EtatCourant") Is Nothing Then
            Exit Sub
        End If
        WsPageArrayList = CType(Me.ViewState("EtatCourant"), ArrayList)
        WsPageArrayList(4) = VComboBox.SelectedItem.Text
        Me.ViewState.Remove("EtatCourant")
        Me.ViewState.Add("EtatCourant", WsPageArrayList)
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(VComboBox.SelectedValue)
        Saisie_Change(Evenement)
    End Sub

End Class