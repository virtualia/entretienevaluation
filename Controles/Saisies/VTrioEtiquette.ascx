﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaControle_VTrioEtiquette" Codebehind="VTrioEtiquette.ascx.vb" %>

<asp:Table ID="CadreLabel" runat="server" Height="20px" Width="306px" HorizontalAlign="Center" CellPadding="1" CellSpacing="0" >
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="LabelGauche" runat="server" Height="18px" Width="100px"
                BackColor="#B0E0D7" Visible="true" BorderColor="#B0E0D7" 
                BorderStyle="Inset" BorderWidth="2px" ForeColor="#142425"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                style="margin-top: 0px; margin-left: 4px; font-style: oblique;
                text-indent: 2px; text-align: center"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="LabelCentre" runat="server" Height="18px" Width="100px"
                BackColor="#B0E0D7" Visible="true" BorderColor="#B0E0D7"  
                BorderStyle="Inset" BorderWidth="2px" ForeColor="#142425"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                style="font-style: oblique; text-indent: 2px; text-align: center"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="LabelDroite" runat="server" Height="18px" Width="100px"
                BackColor="#B0E0D7" Visible="true" BorderColor="#B0E0D7"
                BorderStyle="Inset" BorderWidth="2px" ForeColor="#142425"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                style="font-style: oblique; text-indent: 2px; text-align: center"></asp:Label>
         </asp:TableCell> 
    </asp:TableRow>               
</asp:Table>      

