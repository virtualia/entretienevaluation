﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaControle_VListeGrid" Codebehind="VListeGrid.ascx.vb" %>
<asp:GridView ID="GridDonnee" runat="server" Caption="Virtualia"
    CaptionAlign="Left" CellPadding="2" BackColor="#B0E0D7" 
    ForeColor="#142425" Height="40px" HorizontalAlign="Left" Width="500px"
    AlternatingRowStyle-BorderStyle="None" BorderStyle="Inset" 
    Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="True"
    AutoGenerateColumns="False" AutoGenerateSelectButton="True">
    <RowStyle BackColor="#DFFAF3" Font-Names="Trebuchet MS" Font-Italic="false" 
              Font-Bold="false" Font-Size="Small" Height="18px"
              HorizontalAlign="Left" VerticalAlign="Middle" />
    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"/>
    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Left" />
    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
    <HeaderStyle BackColor="#B0E0D7" ForeColor="#1C5150" Font-Bold="False" Font-Italic="True" 
        Font-Names="Trebuchet MS" Font-Size="Small" HorizontalAlign="Left"
        BorderStyle="Outset" BorderWidth="1px" Height="20px" VerticalAlign="Middle" />
    <EditRowStyle BackColor="#2461BF" />
    <AlternatingRowStyle BackColor="White" />
    <Columns>
      <asp:boundfield datafield="Colonne_1" headertext="Colonne_1"/>
      <asp:boundfield datafield="Colonne_2" headertext="Colonne_2"/>
      <asp:boundfield datafield="Colonne_3" headertext="Colonne_3"/>
      <asp:boundfield datafield="Colonne_4" headertext="Colonne_4"/>
      <asp:boundfield datafield="Colonne_5" headertext="Colonne_5"/>
      <asp:boundfield datafield="Colonne_6" headertext="Colonne_6"/>
      <asp:boundfield datafield="Colonne_7" headertext="Colonne_7"/>
      <asp:boundfield datafield="Colonne_8" headertext="Colonne_8"/>
      <asp:boundfield datafield="Colonne_9" headertext="Colonne_9"/>
      <asp:boundfield datafield="Colonne_10" headertext="Colonne_10"/>
      <asp:boundfield datafield="Colonne_11" headertext="Colonne_11"/>
      <asp:boundfield datafield="Colonne_12" headertext="Colonne_12"/>
      <asp:boundfield datafield="Colonne_13" headertext="Colonne_13"/>
      <asp:boundfield datafield="Colonne_14" headertext="Colonne_14"/>
      <asp:boundfield datafield="Colonne_15" headertext="Colonne_15"/>
      <asp:boundfield datafield="Colonne_16" headertext="Colonne_16"/>
      <asp:boundfield datafield="Colonne_17" headertext="Colonne_17"/>
      <asp:boundfield datafield="Colonne_18" headertext="Colonne_18"/>
      <asp:boundfield datafield="Colonne_19" headertext="Colonne_19"/>
      <asp:boundfield datafield="Colonne_20" headertext="Colonne_20"/>
      <asp:boundfield datafield="Colonne_21" headertext="Colonne_21"/>
      <asp:boundfield datafield="Colonne_22" headertext="Colonne_22"/>
      <asp:boundfield datafield="Colonne_23" headertext="Colonne_23"/>
      <asp:boundfield datafield="Colonne_24" headertext="Colonne_24"/>
      <asp:boundfield datafield="Colonne_25" headertext="Colonne_25"/>
      <asp:boundfield datafield="Colonne_26" headertext="Colonne_26"/>
      <asp:boundfield datafield="Colonne_27" headertext="Colonne_27"/>
      <asp:boundfield datafield="Colonne_28" headertext="Colonne_28"/>
      <asp:boundfield datafield="Colonne_29" headertext="Colonne_29"/>
      <asp:boundfield datafield="Colonne_30" headertext="Colonne_30"/>
      <asp:boundfield datafield="Colonne_31" headertext="Colonne_31"/>
      <asp:boundfield datafield="Colonne_32" headertext="Colonne_32"/>
      <asp:boundfield datafield="Colonne_33" headertext="Colonne_33"/>
      <asp:boundfield datafield="Colonne_34" headertext="Colonne_34"/>
      <asp:boundfield datafield="Colonne_35" headertext="Colonne_35"/>
      <asp:boundfield datafield="Colonne_36" headertext="Colonne_36"/>
      <asp:boundfield datafield="Colonne_37" headertext="Colonne_37"/>
      <asp:boundfield datafield="Colonne_38" headertext="Colonne_38"/>
      <asp:boundfield datafield="Colonne_39" headertext="Colonne_39"/>
      <asp:boundfield datafield="Colonne_40" headertext="Colonne_40"/>
      <asp:boundfield datafield="Colonne_41" headertext="Colonne_41"/>
      <asp:boundfield datafield="Colonne_42" headertext="Colonne_42"/>
      <asp:boundfield datafield="Colonne_43" headertext="Colonne_43"/>
      <asp:boundfield datafield="Colonne_44" headertext="Colonne_44"/>
      <asp:boundfield datafield="Colonne_45" headertext="Colonne_45"/>
      <asp:boundfield datafield="Colonne_46" headertext="Colonne_46"/>
      <asp:boundfield datafield="Colonne_47" headertext="Colonne_47"/>
      <asp:boundfield datafield="Colonne_48" headertext="Colonne_48"/>
      <asp:boundfield datafield="Colonne_49" headertext="Colonne_49"/>
      <asp:boundfield datafield="Colonne_50" headertext="Colonne_50"/>
      <asp:boundfield datafield="Colonne_51" headertext="Colonne_51"/>
      <asp:boundfield datafield="Colonne_52" headertext="Colonne_52"/>
      <asp:boundfield datafield="Colonne_53" headertext="Colonne_53"/>
      <asp:boundfield datafield="Colonne_54" headertext="Colonne_54"/>
      <asp:boundfield datafield="Colonne_55" headertext="Colonne_55"/>
      <asp:boundfield datafield="Colonne_56" headertext="Colonne_56"/>
      <asp:boundfield datafield="Colonne_57" headertext="Colonne_57"/>
      <asp:boundfield datafield="Colonne_58" headertext="Colonne_58"/>
      <asp:boundfield datafield="Colonne_59" headertext="Colonne_59"/>
      <asp:boundfield datafield="Colonne_60" headertext="Colonne_60"/>
      <asp:boundfield datafield="Colonne_61" headertext="Colonne_61"/>
      <asp:boundfield datafield="Colonne_62" headertext="Colonne_62"/>
      <asp:boundfield datafield="Colonne_63" headertext="Colonne_63"/>
      <asp:boundfield datafield="Colonne_64" headertext="Colonne_64"/>
      <asp:boundfield datafield="Colonne_65" headertext="Colonne_65"/>
      <asp:boundfield datafield="Colonne_66" headertext="Colonne_66"/>
      <asp:boundfield datafield="Colonne_67" headertext="Colonne_67"/>
      <asp:boundfield datafield="Colonne_68" headertext="Colonne_68"/>
      <asp:boundfield datafield="Colonne_69" headertext="Colonne_69"/>
      <asp:boundfield datafield="Colonne_70" headertext="Colonne_70"/>
      <asp:boundfield datafield="Colonne_71" headertext="Colonne_71"/>
      <asp:boundfield datafield="Colonne_72" headertext="Colonne_72"/>
      <asp:boundfield datafield="Colonne_73" headertext="Colonne_73"/>
      <asp:boundfield datafield="Colonne_74" headertext="Colonne_74"/>
      <asp:boundfield datafield="Colonne_75" headertext="Colonne_75"/>
      <asp:boundfield datafield="Colonne_76" headertext="Colonne_76"/>
      <asp:boundfield datafield="Colonne_77" headertext="Colonne_77"/>
      <asp:boundfield datafield="Colonne_78" headertext="Colonne_78"/>
      <asp:boundfield datafield="Colonne_79" headertext="Colonne_79"/>
      <asp:boundfield datafield="Colonne_80" headertext="Colonne_80"/>
      <asp:boundfield datafield="Colonne_81" headertext="Colonne_81"/>
      <asp:boundfield datafield="Colonne_82" headertext="Colonne_82"/>
      <asp:boundfield datafield="Colonne_83" headertext="Colonne_83"/>
      <asp:boundfield datafield="Colonne_84" headertext="Colonne_84"/>
      <asp:boundfield datafield="Colonne_85" headertext="Colonne_85"/>
      <asp:boundfield datafield="Colonne_86" headertext="Colonne_86"/>
      <asp:boundfield datafield="Colonne_87" headertext="Colonne_87"/>
      <asp:boundfield datafield="Colonne_88" headertext="Colonne_88"/>
      <asp:boundfield datafield="Colonne_89" headertext="Colonne_89"/>
      <asp:boundfield datafield="Colonne_90" headertext="Colonne_90"/>
      <asp:boundfield datafield="Colonne_91" headertext="Colonne_91"/>
      <asp:boundfield datafield="Colonne_92" headertext="Colonne_92"/>
      <asp:boundfield datafield="Colonne_93" headertext="Colonne_93"/>
      <asp:boundfield datafield="Colonne_94" headertext="Colonne_94"/>
      <asp:boundfield datafield="Colonne_95" headertext="Colonne_95"/>
      <asp:boundfield datafield="Colonne_96" headertext="Colonne_96"/>
      <asp:boundfield datafield="Colonne_97" headertext="Colonne_97"/>
      <asp:boundfield datafield="Colonne_98" headertext="Colonne_98"/>
      <asp:boundfield datafield="Colonne_99" headertext="Colonne_99"/>
      <asp:boundfield datafield="Colonne_100" headertext="Colonne_100"/>
    </Columns>
</asp:GridView>



