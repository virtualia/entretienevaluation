﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaControle_VSixBoutonRadio" Codebehind="VSixBoutonRadio.ascx.vb" %>

<asp:table ID="CadreOption" runat="server" height="20px" CellPadding="1" CellSpacing="0">
     <asp:TableRow>
         <asp:TableCell>
             <asp:RadioButton ID="VRadioN1" runat="server" Text="Option N1" Visible="true"
                  AutoPostBack="true" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                  text-indent: 5px; text-align: center; vertical-align: middle" />
         </asp:TableCell>
         <asp:TableCell>
             <asp:RadioButton ID="VRadioN2" runat="server" Text="Option N2" Visible="true"
                  AutoPostBack="true" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                  text-indent: 5px; text-align: center; vertical-align: middle" />
         </asp:TableCell>
         <asp:TableCell>
             <asp:RadioButton ID="VRadioN3" runat="server" Text="Option N3" Visible="true"
                  AutoPostBack="true" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                  text-indent: 5px; text-align: center; vertical-align: middle" />
         </asp:TableCell>
         <asp:TableCell>
             <asp:RadioButton ID="VRadioN4" runat="server" Text="Option N4" Visible="true"
                  AutoPostBack="true" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                  text-indent: 5px; text-align: center; vertical-align: middle" />
         </asp:TableCell>
         <asp:TableCell>
             <asp:RadioButton ID="VRadioN5" runat="server" Text="Option N5" Visible="true"
                  AutoPostBack="true" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                  text-indent: 5px; text-align: center; vertical-align: middle" />
         </asp:TableCell>
         <asp:TableCell>
             <asp:RadioButton ID="VRadioN6" runat="server" Text="Option N6" Visible="true"
                  AutoPostBack="true" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                  text-indent: 5px; text-align: center; vertical-align: middle" />
         </asp:TableCell>
     </asp:TableRow>
</asp:table>