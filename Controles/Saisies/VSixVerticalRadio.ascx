﻿<%@ Control Language="vb" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaControle_VSixVerticalRadio" CodeBehind="VSixVerticalRadio.ascx.vb"  %>

<asp:Table ID="CadreOption" runat="server" CellPadding="1" CellSpacing="0" 
    height="20px">
    <asp:TableRow>
        <asp:TableCell>
             <asp:RadioButton ID="VRadioN1" runat="server" Text="Option N1" Visible="true"
                  AutoPostBack="true" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 2px; font-style: oblique;
                  text-indent: 2px; text-align: left; vertical-align: middle"/>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:RadioButton ID="VRadioN2" runat="server" Text="Option N2" Visible="true"
                  AutoPostBack="true" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 2px; font-style: oblique;
                  text-indent: 2px; text-align: left; vertical-align: middle"/>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:RadioButton ID="VRadioN3" runat="server" Text="Option N3" Visible="true"
                  AutoPostBack="true" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 2px; font-style: oblique;
                  text-indent: 2px; text-align: left; vertical-align: middle"/>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:RadioButton ID="VRadioN4" runat="server" Text="Option N4" Visible="true"
                  AutoPostBack="true" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 2px; font-style: oblique;
                  text-indent: 2px; text-align: left; vertical-align: middle"/>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:RadioButton ID="VRadioN5" runat="server" Text="Option N5" Visible="true"
                  AutoPostBack="true" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 2px; font-style: oblique;
                  text-indent: 2px; text-align: left; vertical-align: middle"/>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:RadioButton ID="VRadioN6" runat="server" Text="Option N6" Visible="true"
                  AutoPostBack="true" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 2px; font-style: oblique;
                  text-indent: 2px; text-align: left; vertical-align: middle"/>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>