﻿Option Strict On
Option Explicit On
Option Compare Text

Imports System.Web.UI.WebControls
Imports System.Drawing
Imports System

Public Class VirtualiaControle_VTrioEtiTextCommande

    Inherits Virtualia.Net.Controles.ObjetWebControlSaisie
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    '***********************************************************

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Enum TypeTrioEti
        ListeValeur
        Booleen
        Bornee
    End Enum

    <Serializable>
    Public Class CacheTrioEti
        Public Const KeyState As String = "CacheTrioEti"

        Public Property EstStrict As Boolean
        Public Property ValMax As Integer
        Public Property ValMin As Integer
        Public Property Suffixe As String
        Public Property TypeCtrl As TypeTrioEti
        Public Property Valeurs As List(Of String)
        Public Property Pas As Integer

        Public Sub New()
            TypeCtrl = TypeTrioEti.Bornee

            ValMax = 100
            ValMin = 0
            Pas = 5
            Suffixe = ""
            Valeurs = New List(Of String)()
        End Sub

        Public Function Move(ByVal Valeur As String, ByVal SiSuivant As Boolean) As String

            If TypeCtrl = TypeTrioEti.Bornee Then
                Return MoveBorne(Valeur, SiSuivant)
            End If
            Dim idx As Integer = Valeurs.IndexOf(Valeur)

            If SiSuivant = True Then
                If idx = Valeurs.Count - 1 Then
                    Return Valeurs(0)
                End If
                Return Valeurs(idx + 1)
            End If
            If idx = 0 Then
                Return Valeurs(Valeurs.Count - 1)
            End If
            Return Valeurs(idx - 1)
        End Function

        Private Function MoveBorne(ByVal Valeur As String, ByVal SiSuivant As Boolean) As String

            Dim ret As String = ""
            If Valeur = "" Then
                If SiSuivant = True Then
                    ret = ValMin.ToString("G").Trim()
                Else
                    ret = ValMax.ToString("G").Trim()
                End If
                Return ret
            End If

            Dim valnouv As Integer = GetValeurDansListe(Integer.Parse(ValeurSansSuffixe(Valeur)), SiSuivant)
            Dim valsaisie As Integer = Integer.Parse(ValeurSansSuffixe(Valeur))
            Dim idx As Integer

            Dim tmp As Integer = 0
            If valnouv = valsaisie Then
                idx = Valeurs.IndexOf(valsaisie.ToString("G"))

                If SiSuivant = True Then
                    If idx = (Valeurs.Count - 1) Then
                        Return Valeur
                    End If
                    ret = Valeurs(idx + 1)
                Else
                    If idx <= 0 Then
                        Return Valeur
                    End If
                    ret = Valeurs(idx - 1)
                End If
                Return ret
            End If

            ret = valnouv.ToString("G").Trim()
            Return ret
        End Function

        Public Function VerifieValeur(ByVal Valeur As String) As String
            If TypeCtrl <> TypeTrioEti.Bornee Then
                Return Valeur
            End If
            Dim ret As String = GetValeur(Valeur)
            Return ret
        End Function

        Public Function GetValeur(ByVal Valeur As String) As String
            If TypeCtrl <> TypeTrioEti.Bornee Then
                Return Valeur
            End If
            If Valeur.Trim() = "" Then
                Return ""
            End If
            Dim tmp As String = ValeurSansSuffixe(Valeur)
            Dim int As Integer = 0
            If Not Integer.TryParse(tmp, int) Then
                Return ValMin.ToString("G")
            End If
            If int > ValMax Then
                Return ValMax.ToString("G")
            End If
            If int < ValMin Then
                Return ValMin.ToString("G")
            End If
            If Not EstStrict Then
                Return int.ToString("G")
            End If
            int = GetValeurDansListe(int, True)
            Return int.ToString("G")

        End Function

        Private Function GetValeurDansListe(ByVal int As Integer, ByVal SiSuivant As Boolean) As Integer
            If Valeurs.Contains(int.ToString("G")) Then
                Return int
            End If

            Dim pred As Func(Of Integer, Boolean) = Function(v)
                                                        If SiSuivant Then
                                                            Return v > int
                                                        End If
                                                        Return v < int
                                                    End Function

            Dim ret As Integer = 0
            If SiSuivant Then
                ret = (From v In Valeurs Where Integer.Parse(v) > int Order By Integer.Parse(v) Select Integer.Parse(v)).First()
            Else
                Try
                    ret = (From v In Valeurs Where Integer.Parse(v) < int Order By Integer.Parse(v) Descending Select Integer.Parse(v)).First()
                Catch ex As Exception
                    ret = 0
                End Try
            End If

            Return ret

        End Function

        Private Function ValeurSansSuffixe(ByVal Valeur As String) As String
            Dim tmp As String = Valeur
            If Suffixe <> "" Then
                If tmp.EndsWith(Suffixe) Then
                    tmp = tmp.Substring(0, (tmp.Length - (Suffixe.Length + 1)))
                End If
            End If
            Return tmp
        End Function
    End Class

    Public WriteOnly Property DonStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Donnee.Style.Remove(Strings.Trim(TableauW(0)))
                Donnee.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property DonText() As String
        Get
            Return ValeurSuffixee(V_Cache.GetValeur(Donnee.Text), False)
        End Get
        Set(ByVal value As String)
            Donnee.Text = ValeurSuffixee(value, True)
        End Set
    End Property

    Public Property DonHeight() As Unit
        Get
            Return Donnee.Height
        End Get
        Set(ByVal value As Unit)
            Donnee.Height = value
        End Set
    End Property

    Public Property DonBackColor() As Color
        Get
            Return Donnee.BackColor
        End Get
        Set(ByVal value As Color)
            Donnee.BackColor = value
        End Set
    End Property

    Public Property DonForeColor() As Color
        Get
            Return Donnee.ForeColor
        End Get
        Set(ByVal value As Color)
            Donnee.ForeColor = value
        End Set
    End Property

    Public Property DonBorderColor() As Color
        Get
            Return Donnee.BorderColor
        End Get
        Set(ByVal value As Color)
            Donnee.BorderColor = value
        End Set
    End Property

    Public Property DonBorderWidth() As Unit
        Get
            Return Donnee.BorderWidth
        End Get
        Set(ByVal value As Unit)
            Donnee.BorderWidth = value
        End Set
    End Property

    Public Property DonBorderStyle() As BorderStyle
        Get
            Return Donnee.BorderStyle
        End Get
        Set(ByVal value As BorderStyle)
            Donnee.BorderStyle = value
        End Set
    End Property

    Public Property DonTooltip() As String
        Get
            Return Donnee.ToolTip
        End Get
        Set(ByVal value As String)
            Donnee.ToolTip = value
        End Set
    End Property

    Public Property DonTabIndex() As Integer
        Get
            Return Donnee.TabIndex
        End Get
        Set(ByVal value As Integer)
            Donnee.TabIndex = CShort(value)

        End Set
    End Property

    Public WriteOnly Property EtiStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Etiquette.Style.Remove(Strings.Trim(TableauW(0)))
                Etiquette.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property EtiText() As String
        Get
            Return Etiquette.Text
        End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Public Property EtiHeight() As Unit
        Get
            Return Etiquette.Height
        End Get
        Set(ByVal value As Unit)
            Etiquette.Height = value
        End Set
    End Property

    Public Property EtiBackColor() As Color
        Get
            Return Etiquette.BackColor
        End Get
        Set(ByVal value As Color)
            Etiquette.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor() As Color
        Get
            Return Etiquette.ForeColor
        End Get
        Set(ByVal value As Color)
            Etiquette.ForeColor = value
        End Set
    End Property

    Public Property EtiBorderColor() As Color
        Get
            Return Etiquette.BorderColor
        End Get
        Set(ByVal value As Color)
            Etiquette.BorderColor = value
        End Set
    End Property

    Public Property EtiBorderWidth() As Unit
        Get
            Return Etiquette.BorderWidth
        End Get
        Set(ByVal value As Unit)
            Etiquette.BorderWidth = value
        End Set
    End Property

    Public Property EtiBorderStyle() As BorderStyle
        Get
            Return Etiquette.BorderStyle
        End Get
        Set(ByVal value As BorderStyle)
            Etiquette.BorderStyle = value
        End Set
    End Property

    Public Property EtiTooltip() As String
        Get
            Return Etiquette.ToolTip
        End Get
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property

    Public Property EtiVisible() As Boolean
        Get
            Return CellEti.Visible
        End Get
        Set(ByVal value As Boolean)
            CellEti.Visible = value
        End Set
    End Property

    Public Property CmdVisible As Boolean
        Get
            Return TableCmd.Visible
        End Get
        Set(value As Boolean)
            TableCmd.Visible = value
        End Set
    End Property

    Public Property Width As Unit
        Get
            Return Etiquette.Width
        End Get
        Set(ByVal value As Unit)

            Dim w As Unit = value

            If (value.IsEmpty OrElse w.Type <> UnitType.Pixel OrElse w.Value < 48.0R) Then
                w = New Unit(48, UnitType.Pixel)
            Else
                w = value
            End If

            Etiquette.Width = w
            Donnee.Width = New Unit((w.Value - 4), UnitType.Pixel)
        End Set
    End Property

    Private Function ValeurSuffixee(ByVal Valeur As String, ByVal SiAvec As Boolean) As String
        Dim Data As String = ""
        If V_Suffixe = "" Then
            Return Valeur
        End If
        If SiAvec = False Then
            If Valeur.EndsWith(V_Suffixe) Then
                Data = Valeur.Substring(0, (Valeur.Length - (V_Suffixe.Length + 1)))
            End If
        Else
            Data = Valeur & " " & V_Suffixe
        End If
        Return Data
    End Function

    Private Property V_Cache As CacheTrioEti
        Get
            Dim trouve As Boolean = False
            For Each cle As String In ViewState.Keys
                If (cle = CacheTrioEti.KeyState) Then
                    trouve = True
                    Exit For
                End If
            Next

            If Not (trouve) Then
                ViewState.Add(CacheTrioEti.KeyState, New CacheTrioEti())
            End If

            Return DirectCast(ViewState(CacheTrioEti.KeyState), CacheTrioEti)

        End Get
        Set(value As CacheTrioEti)
            Dim trouve As Boolean = False
            For Each cle As String In ViewState.Keys
                If (cle = CacheTrioEti.KeyState) Then
                    trouve = True
                    Exit For
                End If
            Next

            If Not (trouve) Then
                ViewState.Add(CacheTrioEti.KeyState, value)
                Return
            End If

            ViewState(CacheTrioEti.KeyState) = value
        End Set
    End Property

    Public Property V_ListeValeurs As String
        Set(value As String)
            AjouteDansViewState("V_ListeValeurs", value)
        End Set
        Private Get
            Dim obj As Object = LireValeurDeViewState("V_ListeValeurs")

            If (obj Is Nothing) Then
                Return ""
            End If

            Return obj.ToString()

        End Get
    End Property

    Public Property V_ValMax As Integer
        Set(value As Integer)
            AjouteDansViewState("V_ValMax", value)
        End Set
        Private Get
            Dim obj As Object = LireValeurDeViewState("V_ValMax")

            If obj Is Nothing Then
                Return 100
            End If

            Return DirectCast(obj, Integer)

        End Get
    End Property

    Public Property V_ValMin As Integer
        Set(value As Integer)
            AjouteDansViewState("V_ValMin", value)
        End Set
        Private Get
            Dim obj As Object = LireValeurDeViewState("V_ValMin")

            If (obj Is Nothing) Then
                Return 0
            End If

            Return DirectCast(obj, Integer)
        End Get
    End Property

    Public Property V_Pas As Integer
        Set(value As Integer)
            AjouteDansViewState("V_Pas", value)
        End Set
        Private Get
            Dim obj As Object = LireValeurDeViewState("V_Pas")

            If (obj Is Nothing) Then
                Return 5
            End If

            Return DirectCast(obj, Integer)
        End Get
    End Property

    Public Property V_Suffixe As String
        Set(value As String)
            AjouteDansViewState("V_Suffixe", value)
        End Set
        Private Get
            Dim obj As Object = LireValeurDeViewState("V_Suffixe")
            If obj Is Nothing Then
                Return ""
            End If
            Return obj.ToString()
        End Get
    End Property

    Public Property V_Strict As Boolean
        Set(value As Boolean)
            AjouteDansViewState("V_Strict", value)
        End Set
        Private Get
            Dim obj As Object = LireValeurDeViewState("V_Strict")

            If (obj Is Nothing) Then
                Return True
            End If

            Return DirectCast(obj, Boolean)
        End Get
    End Property

    Public Property V_TypeCtrl As TypeTrioEti
        Set(value As TypeTrioEti)
            AjouteDansViewState("V_TypeCtrl", value)
        End Set
        Private Get
            Dim obj As Object = LireValeurDeViewState("V_TypeCtrl")

            If (obj Is Nothing) Then
                Return TypeTrioEti.Bornee
            End If

            Return DirectCast(obj, TypeTrioEti)
        End Get
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)
        Dim tmpcache As CacheTrioEti = New CacheTrioEti()

        tmpcache.Valeurs.Clear()

        Select Case V_TypeCtrl
            Case TypeTrioEti.Booleen
                tmpcache.Valeurs.Add("Oui")
                tmpcache.Valeurs.Add("Non")
            Case TypeTrioEti.ListeValeur
                For Each s As String In V_ListeValeurs.Split("|"c)
                    tmpcache.Valeurs.Add(s)
                Next
            Case TypeTrioEti.Bornee
                tmpcache.Pas = V_Pas
                If (V_ValMax > V_ValMin) Then
                    tmpcache.ValMax = V_ValMax
                    tmpcache.ValMin = V_ValMin
                End If

                For it = tmpcache.ValMin To tmpcache.ValMax Step tmpcache.Pas
                    tmpcache.Valeurs.Add(it.ToString("G"))
                Next

                If Not (tmpcache.Valeurs.Contains(tmpcache.ValMax.ToString("G"))) Then
                    tmpcache.Valeurs.Add(tmpcache.ValMax.ToString("G"))
                End If

                tmpcache.EstStrict = V_Strict
                tmpcache.Suffixe = V_Suffixe
        End Select

        tmpcache.TypeCtrl = V_TypeCtrl
        V_Cache = tmpcache

    End Sub

    Private Sub AjouteDansViewState(ByVal Clef As String, ByVal Valeur As Object)
        Dim Sitrouve As Boolean = False
        For Each c As String In ViewState.Keys
            If c = Clef Then
                Sitrouve = True
                Exit For
            End If
        Next

        If Sitrouve Then
            ViewState(Clef) = Valeur
            Return
        End If

        ViewState.Add(Clef, Valeur)

    End Sub

    Private Function LireValeurDeViewState(ByVal Clef As String) As Object
        Dim SiTrouve As Boolean = False
        For Each c As String In ViewState.Keys
            If c = Clef Then
                SiTrouve = True
                Exit For
            End If
        Next

        If SiTrouve Then
            Return ViewState(Clef)
        End If

        Return Nothing

    End Function

    Private Sub Donnee_TextChanged(sender As Object, e As EventArgs) Handles Donnee.TextChanged
        Donnee.Text = V_Cache.VerifieValeur(Donnee.Text)
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Donnee.Text)
        Saisie_Change(Evenement)
    End Sub

    Private Sub Cmd_Click(sender As Object, e As EventArgs) Handles CmdPrecedent.Click, CmdSuivant.Click
        Dim SiSuivant As Boolean = False
        If CType(sender, System.Web.UI.WebControls.Button).ID = "CmdSuivant" Then
            SiSuivant = True
        End If
        Donnee.Text = V_Cache.Move(Donnee.Text.Trim(), SiSuivant)
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Donnee.Text)
        Saisie_Change(Evenement)
    End Sub

End Class