﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaControle_VOccurenceCouleur" Codebehind="VOccurenceCouleur.ascx.vb" %>

<asp:Table ID="CouleurVirtualia" runat="server" CellPadding="1" CellSpacing="0" > 
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="VEtiNumero" runat="server" Height="20px" Width="20px"
                    BackColor="#FFE5B7" BorderColor="#FFEBC8"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#3D3421"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; font-style: oblique; text-align: center"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="VEtiIntitule" runat="server" Height="20px" Width="200px"
                    BackColor="#FFE5B7" BorderColor="#FFEBC8"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#3D3421"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                    text-indent: 5px; text-align: left"></asp:Label>
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">
            <asp:Table ID="CadreChoix" runat="server" Height="20px" CellPadding="0" 
                 CellSpacing="0" BackImageUrl="~/Images/Boutons/Palette_Sysref.bmp" Visible="true"
                 BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                 Width="76px" HorizontalAlign="Right" style="margin-top: 1px; margin-right:3px" >
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                         <asp:Button ID="CommandeChoix" runat="server" Text="Palette" Width="65px" Height="18px"
                             BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                             BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                         </asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:DropDownList ID="VComboSelection" runat="server" Height="20px" Width="400px" BackColor="#FFE5B7"
                AutoPostBack="true" ForeColor="#3D3421"
                style="margin-left: 4px; border-color: #FFEBC8; border-width: 2px; border-style: inset;
                display: table-cell; font-style: oblique; " >
            </asp:DropDownList>
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">
            <asp:Table ID="CadreSelection" runat="server" Height="20px" CellPadding="0" 
                 CellSpacing="0" BackImageUrl="~/Images/Boutons/NewSupp_Sysref.bmp" Visible="true"
                 BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                 Width="140px" HorizontalAlign="Right" style="margin-top: 1px; margin-right:3px" >
                <asp:TableRow>
                    <asp:TableCell>
                         <asp:Button ID="CommandeAjouter" runat="server" Text="Ajouter" Width="70px" Height="18px"
                             BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                             BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                         </asp:Button>
                    </asp:TableCell>
                    <asp:TableCell>
                         <asp:Button ID="CommandeEffacer" runat="server" Text="Supprimer" Width="75px" Height="18px"
                             BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                             BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                         </asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>