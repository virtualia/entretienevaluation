﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class VirtualiaControle_VListBoxEtCoches
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    '***********************************************************
    'Gestion
    Private WsPointdeVue As Integer = 0
    Private WsNomTable As String
    'Execution
    Private WsNomState As String = "EtatCourant"
    Private WsPageArrayList As ArrayList

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property V_PointdeVue() As Integer
        Get
            Return WsPointdeVue
        End Get
        Set(ByVal value As Integer)
            WsPointdeVue = value
            If Me.ViewState(WsNomState) Is Nothing Then
                Exit Property
            End If
            WsPageArrayList = CType(Me.ViewState(WsNomState), ArrayList)
            WsPageArrayList(0) = value
            If value > 2 Then
                WsNomTable = ""
                WsPageArrayList(1) = ""
                WsPageArrayList(2) = ListeSysReference()
                WsPageArrayList(3) = ""
                Me.ViewState.Remove(WsNomState)
                Me.ViewState.Add(WsNomState, WsPageArrayList)
            End If
        End Set
    End Property

    Public Property V_NomTable() As String
        Get
            Return WsNomTable
        End Get
        Set(ByVal value As String)
            If value = "" Then
                Exit Property
            End If
            WsNomTable = value
            If Me.ViewState(WsNomState) Is Nothing Then
                Exit Property
            End If
            WsPageArrayList = CType(Me.ViewState(WsNomState), ArrayList)
            WsPointdeVue = CShort(WsPageArrayList(0))
            WsPageArrayList(1) = value
            WsPageArrayList(2) = ListeSysReference()
            WsPageArrayList(3) = ""
            Me.ViewState.Remove(WsNomState)
            Me.ViewState.Add(WsNomState, WsPageArrayList)
        End Set
    End Property

    Public ReadOnly Property V_ListeCochee() As String
        Get
            If Me.ViewState(WsNomState) Is Nothing Then
                Return ""
                Exit Property
            End If
            Dim TableauValeur(0) As String
            Dim TableauCoche(0) As String

            WsPageArrayList = CType(Me.ViewState(WsNomState), ArrayList)
            If WsPageArrayList(2) IsNot Nothing Then
                TableauValeur = Strings.Split(WsPageArrayList(2).ToString, VI.Tild, -1)
            Else
                Return ""
            End If
            If WsPageArrayList(3) IsNot Nothing Then
                TableauCoche = Strings.Split(WsPageArrayList(3).ToString, VI.Tild, -1)
            Else
                Return ""
            End If
            Dim Chaine As New System.Text.StringBuilder
            For IndiceI = 0 To TableauCoche.Count - 1
                If TableauCoche(IndiceI) = "1" Then
                    Chaine.Append(TableauValeur(IndiceI) & VI.Tild)
                End If
            Next IndiceI
            Return Chaine.ToString
        End Get
    End Property

    Public WriteOnly Property V_Preselection() As ArrayList
        Set(ByVal value As ArrayList)
            If Me.ViewState(WsNomState) Is Nothing Then
                Exit Property
            End If
            Dim IndiceI As Integer
            Dim SelItem As System.Web.UI.WebControls.ListItem
            Dim Chaine As New System.Text.StringBuilder

            WsPageArrayList = CType(Me.ViewState(WsNomState), ArrayList)
            For IndiceI = 0 To VCheckListBox.Items.Count - 1
                VCheckListBox.Items.Item(IndiceI).Selected = False
            Next IndiceI
            For IndiceI = 0 To value.Count - 1
                SelItem = VCheckListBox.Items.FindByValue(value(IndiceI).ToString)
                If SelItem IsNot Nothing Then
                    SelItem.Selected = True
                End If
            Next IndiceI

            For IndiceI = 0 To VCheckListBox.Items.Count - 1
                Select Case VCheckListBox.Items.Item(IndiceI).Selected
                    Case True
                        Chaine.Append("1")
                    Case False
                        Chaine.Append("0")
                End Select
                Chaine.Append(VI.Tild)
            Next IndiceI
            WsPageArrayList(3) = Chaine.ToString

            Me.ViewState.Remove(WsNomState)
            Me.ViewState.Add(WsNomState, WsPageArrayList)
        End Set
    End Property

    Public Property EtiText() As String
        Get
            Return Etiquette.Text
        End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Public Property EtiHeight() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Height = value
        End Set
    End Property

    Public Property EtiWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Width = value
        End Set
    End Property

    Public Property EtiBackColor() As System.Drawing.Color
        Get
            Return Etiquette.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor() As System.Drawing.Color
        Get
            Return Etiquette.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.ForeColor = value
        End Set
    End Property

    Public Property EtiBorderColor() As System.Drawing.Color
        Get
            Return Etiquette.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BorderColor = value
        End Set
    End Property

    Public Property EtiBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.BorderWidth = value
        End Set
    End Property

    Public Property EtiBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return Etiquette.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Etiquette.BorderStyle = value
        End Set
    End Property

    Public Property EtiTooltip() As String
        Get
            Return Etiquette.ToolTip
        End Get
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property

    Public Property EtiVisible() As Boolean
        Get
            Return Etiquette.Visible
        End Get
        Set(ByVal value As Boolean)
            Etiquette.Visible = value
        End Set
    End Property

    Public WriteOnly Property EtiStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Etiquette.Style.Remove(Strings.Trim(TableauW(0)))
                Etiquette.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property LstStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                VCheckListBox.Style.Remove(Strings.Trim(TableauW(0)))
                VCheckListBox.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property LstText() As String
        Get
            Return VCheckListBox.Text
        End Get
        Set(ByVal value As String)
            Try
                VCheckListBox.Text = value
            Catch ex As Exception
                Exit Property
            End Try
        End Set
    End Property

    Public Property LstHeight() As System.Web.UI.WebControls.Unit
        Get
            Return VCheckListBox.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            VCheckListBox.Height = value
        End Set
    End Property

    Public Property LstWidth() As System.Web.UI.WebControls.Unit
        Get
            Return VCheckListBox.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            VCheckListBox.Width = value
        End Set
    End Property

    Public Property LstBackColor() As System.Drawing.Color
        Get
            Return VCheckListBox.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VCheckListBox.BackColor = value
        End Set
    End Property

    Public Property LstForeColor() As System.Drawing.Color
        Get
            Return VCheckListBox.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VCheckListBox.ForeColor = value
        End Set
    End Property

    Public Property LstBorderColor() As System.Drawing.Color
        Get
            Return VCheckListBox.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VCheckListBox.BorderColor = value
        End Set
    End Property

    Public Property LstBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return VCheckListBox.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            VCheckListBox.BorderWidth = value
        End Set
    End Property

    Public Property LstBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return VCheckListBox.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            VCheckListBox.BorderStyle = value
        End Set
    End Property

    Public Property LstVisible() As Boolean
        Get
            Return VCheckListBox.Visible
        End Get
        Set(ByVal value As Boolean)
            VCheckListBox.Visible = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim IndiceI As Integer
        Dim TableauData(0) As String

        VCheckListBox.Items.Clear()
        VCheckListBox.Font.Name = WebFct.PoliceCharte

        If Me.ViewState(WsNomState) IsNot Nothing Then
            WsPageArrayList = CType(Me.ViewState(WsNomState), ArrayList)
        Else
            WsPageArrayList = CreateArray()
        End If
        WsPointdeVue = Convert.ToInt16(WsPageArrayList(0))
        If WsPageArrayList(1) IsNot Nothing Then
            WsNomTable = WsPageArrayList(1).ToString
        End If
        If WsPageArrayList(2) IsNot Nothing Then
            TableauData = Strings.Split(WsPageArrayList(2).ToString, VI.Tild, -1)
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) <> "" Then
                    VCheckListBox.Items.Add(TableauData(IndiceI))
                End If
            Next IndiceI
        End If
        If WsPageArrayList(3).ToString <> "" Then
            TableauData = Strings.Split(WsPageArrayList(3).ToString, VI.Tild, -1)
            For IndiceI = 0 To TableauData.Count - 1
                If IndiceI > VCheckListBox.Items.Count - 1 Then
                    Exit For
                End If
                Select Case TableauData(IndiceI)
                    Case Is = "1"
                        VCheckListBox.Items.Item(IndiceI).Selected = True
                    Case Else
                        VCheckListBox.Items.Item(IndiceI).Selected = False
                End Select
            Next IndiceI
        End If
        Dim Chaine As New System.Text.StringBuilder
        For IndiceI = 0 To VCheckListBox.Items.Count - 1
            Select Case VCheckListBox.Items.Item(IndiceI).Selected
                Case True
                    Chaine.Append("1")
                Case False
                    Chaine.Append("0")
            End Select
            Chaine.Append(VI.Tild)
        Next IndiceI
        WsPageArrayList(3) = Chaine.ToString

        If Me.ViewState(WsNomState) IsNot Nothing Then
            Me.ViewState.Remove(WsNomState)
        End If
        Me.ViewState.Add(WsNomState, WsPageArrayList)

    End Sub

    Private Function CreateArray() As ArrayList
        Dim VCache As ArrayList
        VCache = New ArrayList

        VCache.Add(V_PointdeVue.ToString) '0
        VCache.Add(V_NomTable) '1
        VCache.Add(ListeSysReference) '2
        VCache.Add("")
        Return VCache
    End Function

    Private Function ListeSysReference() As String
        Dim Chaine As New System.Text.StringBuilder
        Dim TableauObjet(0) As String
        Dim TableauData(0) As String
        Dim IndiceK As Integer

        Select Case V_PointdeVue
            Case Is = 2
                TableauObjet = Strings.Split(WebFct.PointeurUtilisateur.LireTableGeneraleSimple(V_NomTable), VI.SigneBarre, -1)
            Case Is > 2
                TableauObjet = Strings.Split(WebFct.PointeurUtilisateur.LireTableSysReference(V_PointdeVue), VI.SigneBarre, -1)
        End Select
        For IndiceK = 0 To TableauObjet.Count - 1
            If TableauObjet(IndiceK) = "" Then
                Exit For
            End If
            TableauData = Strings.Split(TableauObjet(IndiceK), VI.Tild, -1)
            Chaine.Append(TableauData(1) & VI.Tild)
        Next IndiceK
        Return Chaine.ToString
    End Function

    Protected Sub VCheckListBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VCheckListBox.SelectedIndexChanged
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        WsPageArrayList = CType(Me.ViewState(WsNomState), ArrayList)
        Me.ViewState.Remove(WsNomState)

        Dim Chaine As New System.Text.StringBuilder
        Dim Valeurs As New ArrayList
        Dim IndiceI As Integer
        For IndiceI = 0 To VCheckListBox.Items.Count - 1
            Select Case VCheckListBox.Items.Item(IndiceI).Selected
                Case True
                    Chaine.Append("1")
                    Valeurs.Add(VCheckListBox.Items.Item(IndiceI).Text)
                Case False
                    Chaine.Append("0")
            End Select
            Chaine.Append(VI.Tild)
        Next IndiceI
        WsPageArrayList(3) = Chaine.ToString

        Me.ViewState.Add(WsNomState, WsPageArrayList)

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Valeurs)
        Saisie_Change(Evenement)
    End Sub

    
End Class