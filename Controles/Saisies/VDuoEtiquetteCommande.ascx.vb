﻿Option Strict On
Option Explicit On
Option Compare Text
Partial Class VirtualiaControle_VDuoEtiquetteCommande
    Inherits Virtualia.Net.Controles.ObjetWebControlSaisie
    Public Delegate Sub AppelTable_EventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
    Public Event AppelTable As AppelTable_EventHandler
    '
    Private WsNomdelaTable As String = ""
    Private WsSiEnLectureSeule As Boolean = False

    Protected Overridable Sub Appel_Table(ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
        RaiseEvent AppelTable(Me, e)
    End Sub

    Public Property V_NomTable() As String
        Get
            Return WsNomdelaTable
        End Get
        Set(ByVal value As String)
            WsNomdelaTable = value
        End Set
    End Property

    Public Property V_SiEnLectureSeule() As Boolean
        Get
            Return WsSiEnLectureSeule
        End Get
        Set(ByVal value As Boolean)
            WsSiEnLectureSeule = value
        End Set
    End Property

    Public WriteOnly Property DonStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                DonneeTable.Style.Remove(Strings.Trim(TableauW(0)))
                DonneeTable.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property DonText() As String
        Get
            Return DonneeTable.Text
        End Get
        Set(ByVal value As String)
            DonneeTable.Text = value
        End Set
    End Property

    Public Property DonHeight() As System.Web.UI.WebControls.Unit
        Get
            Return DonneeTable.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            DonneeTable.Height = value
        End Set
    End Property

    Public Property DonWidth() As System.Web.UI.WebControls.Unit
        Get
            Return DonneeTable.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            DonneeTable.Width = value
        End Set
    End Property

    Public Property DonBackColor() As System.Drawing.Color
        Get
            Return DonneeTable.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            DonneeTable.BackColor = value
        End Set
    End Property

    Public Property DonForeColor() As System.Drawing.Color
        Get
            Return DonneeTable.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            DonneeTable.ForeColor = value
        End Set
    End Property

    Public Property DonBorderColor() As System.Drawing.Color
        Get
            Return DonneeTable.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            DonneeTable.BorderColor = value
        End Set
    End Property

    Public Property DonBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return DonneeTable.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            DonneeTable.BorderWidth = value
        End Set
    End Property

    Public Property DonBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return DonneeTable.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            DonneeTable.BorderStyle = value
        End Set
    End Property

    Public Property DonTooltip() As String
        Get
            Return DonneeTable.ToolTip
        End Get
        Set(ByVal value As String)
            DonneeTable.ToolTip = value
        End Set
    End Property

    Public Property DonVisible() As Boolean
        Get
            Return DonneeTable.Visible
        End Get
        Set(ByVal value As Boolean)
            DonneeTable.Visible = value
        End Set
    End Property

    Public Property DonTabIndex() As Integer
        Get
            Return DonneeTable.TabIndex
        End Get
        Set(ByVal value As Integer)
            DonneeTable.TabIndex = CShort(value)
            MyBase.V_TabIndex = value
        End Set
    End Property

    Public WriteOnly Property EtiStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Etiquette.Style.Remove(Strings.Trim(TableauW(0)))
                Etiquette.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property EtiText() As String
        Get
            Return Etiquette.Text
        End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Public Property EtiHeight() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Height = value
        End Set
    End Property

    Public Property EtiWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Width = value
        End Set
    End Property

    Public Property EtiBackColor() As System.Drawing.Color
        Get
            Return Etiquette.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor() As System.Drawing.Color
        Get
            Return Etiquette.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.ForeColor = value
        End Set
    End Property

    Public Property EtiBorderColor() As System.Drawing.Color
        Get
            Return Etiquette.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BorderColor = value
        End Set
    End Property

    Public Property EtiBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.BorderWidth = value
        End Set
    End Property

    Public Property EtiBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return Etiquette.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Etiquette.BorderStyle = value
        End Set
    End Property

    Public Property EtiTooltip() As String
        Get
            Return Etiquette.ToolTip
        End Get
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property

    Public Property EtiVisible() As Boolean
        Get
            Return Etiquette.Visible
        End Get
        Set(ByVal value As Boolean)
            Etiquette.Visible = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If V_SiDonneeDico = True And Etiquette.Text = "" Then
            Etiquette.Text = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).Etiquette
        End If
        If V_SiDonneeDico = True Then
            If V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).TabledeReference <> "" Then
                DonneeTable.ToolTip = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).TabledeReference
            End If
        End If
        If V_SiDonneeDico = True Then
            Etiquette.BackColor = V_EtiBackcolor
            Etiquette.ForeColor = V_EtiForecolor
            Etiquette.BorderColor = V_EtiBordercolor
            DonneeTable.BorderColor = V_DonBordercolor

            DonneeTable.Font.Name = V_FontName
            DonneeTable.Font.Size = V_FontTaille
            Etiquette.Font.Name = V_FontName
            Etiquette.Font.Size = V_FontTaille
            Etiquette.Font.Bold = V_FontBold
            Etiquette.Font.Italic = V_FontItalic
        End If
    End Sub

    Protected Sub DonneeTable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DonneeTable.Click
        If V_SiEnLectureSeule = True Then
            Exit Sub
        End If
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        If V_SiDonneeDico = True Then
            Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(Me.ID, V_Objet, _
                V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).PointdeVueInverse, _
                V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).TabledeReference)
        Else
            Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(Me.ID, V_Objet, V_PointdeVue, V_NomTable)
        End If
        Appel_Table(Evenement)
    End Sub

End Class
