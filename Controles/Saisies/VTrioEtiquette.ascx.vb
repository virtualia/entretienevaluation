﻿Option Strict On
Option Explicit On
Option Compare Text
Partial Class VirtualiaControle_VTrioEtiquette
    Inherits Virtualia.Net.Controles.ObjetWebControlSaisie

    Public Property CadreHeight() As System.Web.UI.WebControls.Unit
        Get
            Return CadreLabel.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            CadreLabel.Height = value
        End Set
    End Property

    Public Property CadreWidth() As System.Web.UI.WebControls.Unit
        Get
            Return CadreLabel.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            CadreLabel.Width = value
        End Set
    End Property

    Public Property CadreBorderColor() As System.Drawing.Color
        Get
            Return CadreLabel.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            CadreLabel.BorderColor = value
        End Set
    End Property

    Public Property CadreBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return CadreLabel.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            CadreLabel.BorderWidth = value
        End Set
    End Property

    Public Property CadreBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return CadreLabel.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            CadreLabel.BorderStyle = value
        End Set
    End Property

    Public Property CadreHorizontalAlign() As System.Web.UI.WebControls.HorizontalAlign
        Get
            Return CadreLabel.HorizontalAlign
        End Get
        Set(ByVal value As System.Web.UI.WebControls.HorizontalAlign)
            CadreLabel.HorizontalAlign = value
        End Set
    End Property

    Public Property EtiGaucheText() As String
        Get
            Return LabelGauche.Text
        End Get
        Set(ByVal value As String)
            LabelGauche.Text = value
        End Set
    End Property

    Public Property EtiGaucheHeight() As System.Web.UI.WebControls.Unit
        Get
            Return LabelGauche.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            LabelGauche.Height = value
        End Set
    End Property

    Public Property EtiGaucheWidth() As System.Web.UI.WebControls.Unit
        Get
            Return LabelGauche.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            LabelGauche.Width = value
        End Set
    End Property

    Public Property EtiGaucheBackColor() As System.Drawing.Color
        Get
            Return LabelGauche.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            LabelGauche.BackColor = value
        End Set
    End Property

    Public Property EtiGaucheForeColor() As System.Drawing.Color
        Get
            Return LabelGauche.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            LabelGauche.ForeColor = value
        End Set
    End Property

    Public Property EtiGaucheBorderColor() As System.Drawing.Color
        Get
            Return LabelGauche.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            LabelGauche.BorderColor = value
        End Set
    End Property

    Public Property EtiGaucheBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return LabelGauche.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            LabelGauche.BorderWidth = value
        End Set
    End Property

    Public Property EtiGaucheBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return LabelGauche.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            LabelGauche.BorderStyle = value
        End Set
    End Property

    Public Property EtiGaucheTooltip() As String
        Get
            Return LabelGauche.ToolTip
        End Get
        Set(ByVal value As String)
            LabelGauche.ToolTip = value
        End Set
    End Property

    Public Property EtiGaucheVisible() As Boolean
        Get
            Return LabelGauche.Visible
        End Get
        Set(ByVal value As Boolean)
            LabelGauche.Visible = value
        End Set
    End Property

    Public Property EtiCentreText() As String
        Get
            Return LabelCentre.Text
        End Get
        Set(ByVal value As String)
            LabelCentre.Text = value
        End Set
    End Property

    Public Property EtiCentreHeight() As System.Web.UI.WebControls.Unit
        Get
            Return LabelCentre.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            LabelCentre.Height = value
        End Set
    End Property

    Public Property EtiCentreWidth() As System.Web.UI.WebControls.Unit
        Get
            Return LabelCentre.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            LabelCentre.Width = value
        End Set
    End Property

    Public Property EtiCentreBackColor() As System.Drawing.Color
        Get
            Return LabelCentre.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            LabelCentre.BackColor = value
        End Set
    End Property

    Public Property EtiCentreForeColor() As System.Drawing.Color
        Get
            Return LabelCentre.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            LabelCentre.ForeColor = value
        End Set
    End Property

    Public Property EtiCentreBorderColor() As System.Drawing.Color
        Get
            Return LabelCentre.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            LabelCentre.BorderColor = value
        End Set
    End Property

    Public Property EtiCentreBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return LabelCentre.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            LabelCentre.BorderWidth = value
        End Set
    End Property

    Public Property EtiCentreBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return LabelCentre.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            LabelCentre.BorderStyle = value
        End Set
    End Property

    Public Property EtiCentreTooltip() As String
        Get
            Return LabelCentre.ToolTip
        End Get
        Set(ByVal value As String)
            LabelCentre.ToolTip = value
        End Set
    End Property

    Public Property EtiCentreVisible() As Boolean
        Get
            Return LabelCentre.Visible
        End Get
        Set(ByVal value As Boolean)
            LabelCentre.Visible = value
        End Set
    End Property

    Public Property EtiDroiteText() As String
        Get
            Return LabelDroite.Text
        End Get
        Set(ByVal value As String)
            LabelDroite.Text = value
        End Set
    End Property

    Public Property EtiDroiteHeight() As System.Web.UI.WebControls.Unit
        Get
            Return LabelDroite.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            LabelDroite.Height = value
        End Set
    End Property

    Public Property EtiDroiteWidth() As System.Web.UI.WebControls.Unit
        Get
            Return LabelDroite.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            LabelDroite.Width = value
        End Set
    End Property

    Public Property EtiDroiteBackColor() As System.Drawing.Color
        Get
            Return LabelDroite.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            LabelDroite.BackColor = value
        End Set
    End Property

    Public Property EtiDroiteForeColor() As System.Drawing.Color
        Get
            Return LabelDroite.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            LabelDroite.ForeColor = value
        End Set
    End Property

    Public Property EtiDroiteBorderColor() As System.Drawing.Color
        Get
            Return LabelDroite.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            LabelDroite.BorderColor = value
        End Set
    End Property

    Public Property EtiDroiteBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return LabelDroite.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            LabelDroite.BorderWidth = value
        End Set
    End Property

    Public Property EtiDroiteBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return LabelDroite.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            LabelDroite.BorderStyle = value
        End Set
    End Property

    Public Property EtiDroiteTooltip() As String
        Get
            Return LabelDroite.ToolTip
        End Get
        Set(ByVal value As String)
            LabelDroite.ToolTip = value
        End Set
    End Property

    Public Property EtiDroiteVisible() As Boolean
        Get
            Return LabelDroite.Visible
        End Get
        Set(ByVal value As Boolean)
            LabelDroite.Visible = value
        End Set
    End Property

    Public WriteOnly Property EtiGaucheStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                LabelGauche.Style.Remove(Strings.Trim(TableauW(0)))
                LabelGauche.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property EtiCentreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                LabelCentre.Style.Remove(Strings.Trim(TableauW(0)))
                LabelCentre.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property EtiDroiteStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                LabelDroite.Style.Remove(Strings.Trim(TableauW(0)))
                LabelDroite.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

End Class
