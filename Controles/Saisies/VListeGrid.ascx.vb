﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class VirtualiaControle_VListeGrid
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsSiAppliquerCharte As Boolean = True
    Private WsNiveauCharte As Integer = 1 ' 0 = PERBIS 1 = PER 2 = REF
    Private WsSiCaptionVisible As Boolean = True
    '
    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property SiAppliquerCharte() As Boolean
        Get
            Return WsSiAppliquerCharte
        End Get
        Set(ByVal value As Boolean)
            WsSiAppliquerCharte = value
        End Set
    End Property

    Public Property V_NiveauCharte() As Integer
        Get
            Return WsNiveauCharte
        End Get
        Set(ByVal value As Integer)
            WsNiveauCharte = value
        End Set
    End Property

    Public Property SiCaptionVisible() As Boolean
        Get
            Return WsSiCaptionVisible
        End Get
        Set(ByVal value As Boolean)
            WsSiCaptionVisible = value
        End Set
    End Property

    Public Property SiColonneSelect() As Boolean
        Get
            Return GridDonnee.AutoGenerateSelectButton
        End Get
        Set(ByVal value As Boolean)
            GridDonnee.AutoGenerateSelectButton = value
        End Set
    End Property

    Public Property CadreWidth() As System.Web.UI.WebControls.Unit
        Get
            Return GridDonnee.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            GridDonnee.Width = value
        End Set
    End Property

    Public Property SiPagination() As Boolean
        Get
            Return GridDonnee.AllowPaging
        End Get
        Set(ByVal value As Boolean)
            GridDonnee.AllowPaging = value
            If value = True Then
                GridDonnee.PagerSettings.Mode = PagerButtons.Numeric
                GridDonnee.PagerSettings.Position = PagerPosition.TopAndBottom
                If WebFct Is Nothing Then
                    WebFct = New Virtualia.Net.WebFonctions(Me, 0)
                End If
                GridDonnee.PagerStyle.BackColor = WebFct.ConvertCouleur("#OE5F5C")
                GridDonnee.PagerStyle.ForeColor = WebFct.ConvertCouleur("#D7FAF3")
            End If
        End Set
    End Property

    Public Property BackColorCaption() As System.Drawing.Color
        Get
            Return GridDonnee.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            GridDonnee.BackColor = value
            GridDonnee.HeaderStyle.BackColor = value
        End Set
    End Property

    Public Property ForeColorCaption() As System.Drawing.Color
        Get
            Return GridDonnee.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            GridDonnee.ForeColor = value
            GridDonnee.HeaderStyle.ForeColor = value
        End Set
    End Property

    Public Property BackColorRow() As System.Drawing.Color
        Get
            Return GridDonnee.RowStyle.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            GridDonnee.RowStyle.BackColor = value
        End Set
    End Property

    Public Property ForeColorRow() As System.Drawing.Color
        Get
            Return GridDonnee.RowStyle.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            GridDonnee.RowStyle.ForeColor = value
        End Set
    End Property

    Public Property TaillePage() As Integer
        Get
            Return GridDonnee.PageSize
        End Get
        Set(ByVal value As Integer)
            GridDonnee.PageSize = value
        End Set
    End Property

    Public ReadOnly Property TotalLignes() As Integer
        Get
            If Me.ViewState("EtatCourant") Is Nothing Then
                Return 0
                Exit Property
            End If
            Dim VCache As ArrayList
            Dim TableauData(0) As String
            Try
                VCache = CType(Me.ViewState("EtatCourant"), ArrayList)
                If VCache(2).ToString <> "" Then
                    TableauData = Strings.Split(VCache(2).ToString, VI.SigneBarre, -1)
                    Return TableauData.Count - 1
                End If
            Catch ex As Exception
                Return 0
            End Try
            Return 0
        End Get
    End Property

    Public Property Centrage_Colonne(ByVal Index As Integer) As Integer
        Get
            If Index > GridDonnee.Columns.Count Then
                Return 0
                Exit Property
            End If
            Select Case GridDonnee.Columns.Item(Index).ItemStyle.HorizontalAlign
                Case Is = HorizontalAlign.Left
                    Return 0
                Case Is = HorizontalAlign.Center
                    Return 1
                Case Is = HorizontalAlign.Right
                    Return 2
                Case Else
                    Return 0
            End Select
        End Get
        Set(ByVal value As Integer)
            If Index > GridDonnee.Columns.Count Then
                Exit Property
            End If
            Select Case value
                Case 0 'Left
                    GridDonnee.Columns.Item(Index).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                Case 1 'Centré
                    GridDonnee.Columns.Item(Index).ItemStyle.HorizontalAlign = HorizontalAlign.Center
                Case 2 'Right
                    GridDonnee.Columns.Item(Index).ItemStyle.HorizontalAlign = HorizontalAlign.Right
            End Select
        End Set
    End Property

    Public Property V_Liste(ByVal Colonnes As String, ByVal Libelles As String) As String
        Get
            If Me.ViewState("EtatCourant") Is Nothing Then
                Return ""
                Exit Property
            End If
            Dim VCache As ArrayList
            VCache = CType(Me.ViewState("EtatCourant"), ArrayList)
            Select Case Colonnes
                Case Is = ""
                    Return VCache(2).ToString
                Case Else
                    Return VCache(1).ToString & VCache(2).ToString
            End Select
        End Get
        Set(ByVal value As String)

            Dim VCache As ArrayList
            If Me.ViewState("EtatCourant") IsNot Nothing Then
                Me.ViewState.Remove("EtatCourant")
            End If
            VCache = New ArrayList
            If Libelles = "" Then
                Libelles = "Aucune situation" & VI.Tild & "Une situation" & VI.Tild & "situations"
            End If
            VCache.Add(Libelles)
            VCache.Add(Colonnes)
            VCache.Add(value)
            VCache.Add("")
            Me.ViewState.Add("EtatCourant", VCache)

        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        If GridDonnee.AllowPaging = True And GridDonnee.PageIndex > 0 Then
            Call FaireListe(GridDonnee.PageIndex)
        Else
            Call FaireListe(0)
        End If

        If WsSiAppliquerCharte = True Then
            GridDonnee.BackColor = WebFct.CouleurCharte(WsNiveauCharte, "Bordure")
            GridDonnee.ForeColor = WebFct.CouleurCharte(WsNiveauCharte, "Police_Fonce")

            GridDonnee.RowStyle.BackColor = WebFct.CouleurCharte(WsNiveauCharte, "Police_Claire")
            GridDonnee.RowStyle.ForeColor = WebFct.CouleurCharte(WsNiveauCharte, "Selection")

            GridDonnee.HeaderStyle.BackColor = WebFct.CouleurCharte(WsNiveauCharte, "Bordure")
            GridDonnee.HeaderStyle.ForeColor = WebFct.CouleurCharte(WsNiveauCharte, "Selection")

            GridDonnee.SelectedRowStyle.BackColor = WebFct.CouleurCharte(WsNiveauCharte, "Selection")
            GridDonnee.SelectedRowStyle.ForeColor = WebFct.CouleurCharte(WsNiveauCharte, "Police_Claire")
            GridDonnee.EditRowStyle.BackColor = WebFct.CouleurCharte(WsNiveauCharte, "Selection")
            GridDonnee.EditRowStyle.ForeColor = WebFct.CouleurCharte(WsNiveauCharte, "Police_Claire")

            GridDonnee.Font.Name = WebFct.PoliceCharte
        End If
    End Sub

    Protected Sub GridDonnee_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridDonnee.PageIndexChanging
        If e.NewPageIndex >= 0 And e.NewPageIndex < GridDonnee.PageCount Then
            Call FaireListe(e.NewPageIndex)
        End If
    End Sub

    Protected Sub GridDonnee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridDonnee.SelectedIndexChanged
        If Me.ViewState("EtatCourant") Is Nothing Then
            Exit Sub
        End If
        Dim VCache As ArrayList
        Try
            VCache = CType(Me.ViewState("EtatCourant"), ArrayList)
            Me.ViewState.Remove("EtatCourant")
            VCache(3) = GridDonnee.SelectedDataKey.Item(0).ToString
            Me.ViewState.Add("EtatCourant", VCache)

            Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
            Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(GridDonnee.SelectedDataKey.Item(0).ToString)
            Saisie_Change(Evenement)
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub FaireListe(ByVal NoPage As Integer)
        Dim VCache As ArrayList
        Dim TableauData(0) As String
        Dim TableauCol(0) As String
        Dim IndiceI As Integer
        Dim IndiceA As Integer
        Dim VClasseGrid As Virtualia.Systeme.Datas.ObjetIlistGrid
        Dim Datas As ArrayList
        Dim Cpt As Integer = 0

        If Me.ViewState("EtatCourant") Is Nothing Then
            Exit Sub
        End If
        VCache = CType(Me.ViewState("EtatCourant"), ArrayList)

        'Colonnes
        IndiceI = 1
        For IndiceA = 0 To GridDonnee.Columns.Count - 1
            GridDonnee.Columns.Item(IndiceA).HeaderText = "Colonne_" & IndiceI.ToString
            IndiceI += 1
        Next IndiceA

        If VCache(1).ToString <> "" Then
            TableauData = Strings.Split(VCache(1).ToString, VI.Tild, -1)
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                If TableauData(IndiceI) = "Clef" Then
                    TableauCol(0) = GridDonnee.Columns.Item(IndiceI).ToString
                    GridDonnee.DataKeyNames = TableauCol
                    GridDonnee.Columns.Item(IndiceI).Visible = False
                Else
                    GridDonnee.Columns.Item(IndiceI).Visible = True
                    GridDonnee.Columns.Item(IndiceI).HeaderText = TableauData(IndiceI)
                End If
            Next
            If IndiceI < GridDonnee.Columns.Count Then
                For IndiceA = IndiceI To GridDonnee.Columns.Count - 1
                    GridDonnee.Columns.Item(IndiceA).Visible = False
                Next IndiceA
            End If
        End If

        Datas = New ArrayList
        If VCache(2) IsNot Nothing Then
            If VCache(2).ToString <> "" Then
                TableauData = Strings.Split(VCache(2).ToString, VI.SigneBarre, -1)
                For IndiceI = 0 To TableauData.Count - 1
                    If TableauData(IndiceI) = "" Then
                        Exit For
                    End If
                    TableauCol = Strings.Split(TableauData(IndiceI), VI.Tild, -1)
                    VClasseGrid = New Virtualia.Systeme.Datas.ObjetIlistGrid
                    For IndiceA = 0 To TableauCol.Count - 1
                        VClasseGrid.Valeur(IndiceA) = TableauCol(IndiceA)
                    Next IndiceA
                    Datas.Add(VClasseGrid)
                    Cpt += 1
                Next IndiceI
            Else
                VClasseGrid = New Virtualia.Systeme.Datas.ObjetIlistGrid
                Datas.Add(VClasseGrid)
            End If
        End If
        GridDonnee.DataSource = Datas
        GridDonnee.DataBind()
        If SiPagination = True Then
            GridDonnee.PageIndex = NoPage
        End If

        If SiCaptionVisible = True Then
            TableauData = Strings.Split(VCache(0).ToString, VI.Tild, -1)
            Select Case Cpt
                Case Is = 0
                    GridDonnee.Caption = TableauData(0)
                    GridDonnee.AutoGenerateSelectButton = False
                Case Is = 1
                    GridDonnee.Caption = TableauData(1)
                Case Else
                    GridDonnee.Caption = GridDonnee.Rows.Count.ToString & Space(1) & TableauData(2)
            End Select
        Else
            GridDonnee.Caption = ""
        End If
    End Sub

End Class
