﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class VirtualiaControle_VOccurenceCouleur
    Inherits System.Web.UI.UserControl
    Public Delegate Sub AppelTable_EventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
    Public Event AppelTable As AppelTable_EventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsPointdeVue As Integer = 0
    Private WsNomTable As String
    'Execution
    Private WsNomState As String
    Private WsPageArrayList As ArrayList

    Protected Overridable Sub Appel_Table(ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
        RaiseEvent AppelTable(Me, e)
    End Sub

    Public Property V_ListeSelection() As ArrayList
        Get
            If Me.ViewState(WsNomState) Is Nothing Then
                Return Nothing
                Exit Property
            End If
            Return CType(Me.ViewState(WsNomState), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Dim IndiceI As Integer

            If Me.ViewState(WsNomState) IsNot Nothing Then
                Me.ViewState.Remove(WsNomState)
            End If
            VComboSelection.Items.Clear()
            WsPageArrayList = value
            For IndiceI = 0 To WsPageArrayList.Count - 1
                If WsPageArrayList(IndiceI).ToString <> "" Then
                    VComboSelection.Items.Add(WsPageArrayList(IndiceI).ToString)
                End If
            Next IndiceI
            Me.ViewState.Add(WsNomState, WsPageArrayList)
        End Set
    End Property

    Public Property V_PointdeVue() As Integer
        Get
            Return WsPointdeVue
        End Get
        Set(ByVal value As Integer)
            WsPointdeVue = value
        End Set
    End Property

    Public Property V_NomTable() As String
        Get
            Return WsNomTable
        End Get
        Set(ByVal value As String)
            WsNomTable = value
        End Set
    End Property

    Public Property V_Numero() As String
        Get
            Return VEtiNumero.Text
        End Get
        Set(ByVal value As String)
            VEtiNumero.Text = value
        End Set
    End Property

    Public Property V_Intitule() As String
        Get
            Return VEtiIntitule.Text
        End Get
        Set(ByVal value As String)
            VEtiIntitule.Text = value
        End Set
    End Property

    Public Property SiSelectionVisible() As Boolean
        Get
            Return VComboSelection.Visible
        End Get
        Set(ByVal value As Boolean)
            VComboSelection.Visible = value
            CadreSelection.Visible = value
        End Set
    End Property

    Public Property EtiBackColor() As System.Drawing.Color
        Get
            Return VEtiNumero.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VEtiNumero.BackColor = value
            VEtiIntitule.BackColor = value
            VComboSelection.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor() As System.Drawing.Color
        Get
            Return VEtiNumero.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VEtiNumero.ForeColor = value
            VEtiIntitule.ForeColor = value
            VComboSelection.ForeColor = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
        WsNomState = "EtatCourant"
    End Sub

    Protected Sub CommandeChoix_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeChoix.Click
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(Me.ID, 24, _
                    VI.PointdeVue.PVuePaie, "PAI_COULEURS")
        Appel_Table(Evenement)
    End Sub

    Protected Sub CommandeAjouter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeAjouter.Click
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(Me.ID, 24, _
                    V_PointdeVue, V_NomTable)
        Appel_Table(Evenement)
    End Sub

    Protected Sub CommandeEffacer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeEffacer.Click
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        WsPageArrayList = CType(Me.ViewState(WsNomState), ArrayList)
        For IndiceI = 0 To WsPageArrayList.Count - 1
            If VComboSelection.Items.Item(IndiceI).Selected = True Then
                WsPageArrayList.RemoveAt(IndiceI)
                Exit For
            End If
        Next IndiceI
        Me.ViewState.Remove(WsNomState)
        Me.ViewState.Add(WsNomState, WsPageArrayList)
    End Sub
End Class
