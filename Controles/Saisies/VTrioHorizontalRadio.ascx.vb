﻿Option Strict On
Option Explicit On
Option Compare Text
Partial Class VirtualiaControle_VTrioHorizontalRadio
    Inherits Virtualia.Net.Controles.ObjetWebControlSaisie
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WsSiEnLectureSeule As Boolean = False

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property V_SiAutoPostBack() As Boolean
        Get
            Return RadioN0.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            RadioN0.AutoPostBack = value
            RadioN1.AutoPostBack = value
            RadioN2.AutoPostBack = value
        End Set
    End Property

    Public Property V_SiEnLectureSeule() As Boolean
        Get
            Return WsSiEnLectureSeule
        End Get
        Set(ByVal value As Boolean)
            WsSiEnLectureSeule = value
            If value = True Then
                V_SiAutoPostBack = False
                RadioN0.Enabled = False
                RadioN1.Enabled = False
                RadioN2.Enabled = False
            Else
                RadioN0.Enabled = True
                RadioN1.Enabled = True
                RadioN2.Enabled = True
            End If
        End Set
    End Property

    Public Property V_Groupe() As String
        Get
            Return RadioN0.GroupName
        End Get
        Set(ByVal value As String)
            RadioN0.GroupName = value
            RadioN1.GroupName = value
            RadioN2.GroupName = value
        End Set
    End Property

    Public Property RadioGaucheText() As String
        Get
            Return RadioN0.Text
        End Get
        Set(ByVal value As String)
            RadioN0.Text = value
        End Set
    End Property

    Public Property RadioGaucheCheck() As Boolean
        Get
            Return RadioN0.Checked
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                RadioN0.Checked = True
                RadioN1.Checked = False
                RadioN2.Checked = False
                RadioN0.Font.Bold = True
                RadioN1.Font.Bold = False
                RadioN2.Font.Bold = False
            End If
        End Set
    End Property

    Public Property RadioGaucheHeight() As System.Web.UI.WebControls.Unit
        Get
            Return RadioN0.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioN0.Height = value
        End Set
    End Property

    Public Property RadioGaucheWidth() As System.Web.UI.WebControls.Unit
        Get
            Return RadioN0.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioN0.Width = value
        End Set
    End Property

    Public Property RadioGaucheBackColor() As System.Drawing.Color
        Get
            Return RadioN0.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioN0.BackColor = value
        End Set
    End Property

    Public Property RadioGaucheForeColor() As System.Drawing.Color
        Get
            Return RadioN0.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioN0.ForeColor = value
        End Set
    End Property

    Public Property RadioGaucheBorderColor() As System.Drawing.Color
        Get
            Return RadioN0.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioN0.BorderColor = value
        End Set
    End Property

    Public Property RadioGaucheBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return RadioN0.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioN0.BorderWidth = value
        End Set
    End Property

    Public Property RadioGaucheBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return RadioN0.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            RadioN0.BorderStyle = value
        End Set
    End Property

    Public Property RadioGaucheVisible() As Boolean
        Get
            Return RadioN0.Visible
        End Get
        Set(ByVal value As Boolean)
            RadioN0.Visible = value
        End Set
    End Property

    Public Property RadioCentreText() As String
        Get
            Return RadioN1.Text
        End Get
        Set(ByVal value As String)
            RadioN1.Text = value
        End Set
    End Property

    Public Property RadioCentreCheck() As Boolean
        Get
            Return RadioN1.Checked
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                RadioN0.Checked = False
                RadioN1.Checked = True
                RadioN2.Checked = False
                RadioN0.Font.Bold = False
                RadioN1.Font.Bold = True
                RadioN2.Font.Bold = False
            End If
        End Set
    End Property

    Public Property RadioCentreHeight() As System.Web.UI.WebControls.Unit
        Get
            Return RadioN1.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioN1.Height = value
        End Set
    End Property

    Public Property RadioCentreWidth() As System.Web.UI.WebControls.Unit
        Get
            Return RadioN1.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioN1.Width = value
        End Set
    End Property

    Public Property RadioCentreBackColor() As System.Drawing.Color
        Get
            Return RadioN1.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioN1.BackColor = value
        End Set
    End Property

    Public Property RadioCentreForeColor() As System.Drawing.Color
        Get
            Return RadioN1.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioN1.ForeColor = value
        End Set
    End Property

    Public Property RadioCentreBorderColor() As System.Drawing.Color
        Get
            Return RadioN1.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioN1.BorderColor = value
        End Set
    End Property

    Public Property RadioCentreBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return RadioN1.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioN1.BorderWidth = value
        End Set
    End Property

    Public Property RadioCentreBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return RadioN1.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            RadioN1.BorderStyle = value
        End Set
    End Property

    Public Property RadioCentreVisible() As Boolean
        Get
            Return RadioN1.Visible
        End Get
        Set(ByVal value As Boolean)
            RadioN1.Visible = value
        End Set
    End Property

    Public Property RadioDroiteText() As String
        Get
            Return RadioN2.Text
        End Get
        Set(ByVal value As String)
            RadioN2.Text = value
        End Set
    End Property

    Public Property RadioDroiteCheck() As Boolean
        Get
            Return RadioN2.Checked
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                RadioN0.Checked = False
                RadioN1.Checked = False
                RadioN2.Checked = True
                RadioN0.Font.Bold = False
                RadioN1.Font.Bold = False
                RadioN2.Font.Bold = True
            End If
        End Set
    End Property

    Public Property RadioDroiteHeight() As System.Web.UI.WebControls.Unit
        Get
            Return RadioN2.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioN2.Height = value
        End Set
    End Property

    Public Property RadioDroiteWidth() As System.Web.UI.WebControls.Unit
        Get
            Return RadioN2.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioN2.Width = value
        End Set
    End Property

    Public Property RadioDroiteBackColor() As System.Drawing.Color
        Get
            Return RadioN2.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioN2.BackColor = value
        End Set
    End Property

    Public Property RadioDroiteForeColor() As System.Drawing.Color
        Get
            Return RadioN2.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioN2.ForeColor = value
        End Set
    End Property

    Public Property RadioDroiteBorderColor() As System.Drawing.Color
        Get
            Return RadioN2.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioN2.BorderColor = value
        End Set
    End Property

    Public Property RadioDroiteBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return RadioN2.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioN2.BorderWidth = value
        End Set
    End Property

    Public Property RadioDroiteBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return RadioN2.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            RadioN2.BorderStyle = value
        End Set
    End Property

    Public Property RadioDroiteVisible() As Boolean
        Get
            Return RadioN2.Visible
        End Get
        Set(ByVal value As Boolean)
            RadioN2.Visible = value
        End Set
    End Property

    Public WriteOnly Property RadioGaucheStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                RadioN0.Style.Remove(Strings.Trim(TableauW(0)))
                RadioN0.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property RadioCentreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                RadioN1.Style.Remove(Strings.Trim(TableauW(0)))
                RadioN1.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property RadioDroiteStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                RadioN2.Style.Remove(Strings.Trim(TableauW(0)))
                RadioN2.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Protected Sub RadioNirtualia_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioN0.CheckedChanged, RadioN1.CheckedChanged, RadioN2.CheckedChanged
        If V_SiEnLectureSeule = True Then
            Exit Sub
        End If
        RadioN0.Font.Bold = False
        RadioN1.Font.Bold = False
        RadioN2.Font.Bold = False
        CType(sender, System.Web.UI.WebControls.RadioButton).Font.Bold = True

        Dim Valeur As String
        Dim ValIndex As Integer
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(CType(sender, System.Web.UI.WebControls.RadioButton).Text)
        Valeur = CType(sender, System.Web.UI.WebControls.RadioButton).Text
        ValIndex = CInt(Strings.Right(CType(sender, System.Web.UI.WebControls.RadioButton).ID, 1)) + 1
        If Valeur = "" Then
            Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(ValIndex.ToString, ValIndex.ToString)
        Else
            Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(ValIndex.ToString, Valeur)
        End If
        Saisie_Change(Evenement)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If V_SiDonneeDico = True Then
            Select Case RadioN0.Checked
                Case True
                    RadioN0.BackColor = V_InverseRadio
                Case False
                    RadioN0.BackColor = V_EtiBackcolor
            End Select
            RadioN0.ForeColor = V_EtiForecolor
            RadioN0.BorderColor = V_EtiBordercolor
            Select Case RadioN1.Checked
                Case True
                    RadioN1.BackColor = V_InverseRadio
                Case False
                    RadioN1.BackColor = V_EtiBackcolor
            End Select
            RadioN1.ForeColor = V_EtiForecolor
            RadioN1.BorderColor = V_EtiBordercolor
            Select Case RadioN2.Checked
                Case True
                    RadioN2.BackColor = V_InverseRadio
                Case False
                    RadioN2.BackColor = V_EtiBackcolor
            End Select
            RadioN2.ForeColor = V_EtiForecolor
            RadioN2.BorderColor = V_EtiBordercolor

            RadioN0.Font.Name = V_FontName
            RadioN0.Font.Size = V_FontTaille
            RadioN0.Font.Italic = V_FontItalic
            RadioN1.Font.Name = V_FontName
            RadioN1.Font.Size = V_FontTaille
            RadioN1.Font.Italic = V_FontItalic
            RadioN2.Font.Name = V_FontName
            RadioN2.Font.Size = V_FontTaille
            RadioN2.Font.Italic = V_FontItalic
        Else
            RadioN0.Font.Bold = RadioN0.Checked
            RadioN1.Font.Bold = RadioN1.Checked
            RadioN2.Font.Bold = RadioN2.Checked
        End If
    End Sub
End Class