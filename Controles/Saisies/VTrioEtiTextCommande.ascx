﻿<%@ Control Language="vb" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaControle_VTrioEtiTextCommande" CodeBehind="VTrioEtiTextCommande.ascx.vb"  %>

<asp:Table ID="VStandard" runat="server" CellPadding="1" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell ID="CellEti" HorizontalAlign="Center">
            <asp:Label ID="Etiquette" runat="server" Height="12px" Width="48px" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="1px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" Style="margin-bottom: 2px; margin-left: 0px; text-indent: 0px; text-align: center; padding-bottom: 4px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:UpdatePanel ID="upd" runat="server">
                <ContentTemplate>
                    <asp:TextBox ID="Donnee" runat="server" Height="40px" Width="44px" BackColor="White" BorderColor="#124545" BorderStyle="Notset" BorderWidth="1px" ForeColor="Black" Visible="true" ReadOnly="true" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="margin-top: 0px; text-indent: 0px; text-align: center" TextMode="SingleLine" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="CmdPrecedent" />
                    <asp:AsyncPostBackTrigger ControlID="CmdSuivant" />
                    <asp:PostBackTrigger ControlID="Donnee" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" Height="20px">
            <asp:Table ID="TableCmd" runat="server">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Width="22px">
                        <asp:Button ID="CmdPrecedent" runat="server" Height="18px" Width="21px" 
                                    BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="none" BorderWidth="1px" ForeColor="White" 
                                    Visible="true" Text="<" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="80%" Font-Italic="false" 
                                    Style="padding-left: 3px; margin-top: 0px; margin-bottom: 0px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Width="22px">
                        <asp:Button ID="CmdSuivant" runat="server" Height="18px" Width="21px" 
                                    BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="none" BorderWidth="1px" ForeColor="White" 
                                    Visible="true" Text=">" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="80%" Font-Italic="false" 
                                    Style="padding-left: 3px; margin-top: 0px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
