﻿Option Strict On
Option Explicit On
Option Compare Text
Partial Class VirtualiaControle_VCoupleVerticalEtiDonnee
    Inherits Virtualia.Net.Controles.ObjetWebControlSaisie
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    '***********************************************************

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property V_SiAutoPostBack() As Boolean
        Get
            Return Donnee.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            If V_SiEnLectureSeule = True Then
                Donnee.AutoPostBack = False
                Exit Property
            End If
            Donnee.AutoPostBack = value
        End Set
    End Property

    Public Property V_SiEnLectureSeule() As Boolean
        Get
            Return Donnee.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Donnee.ReadOnly = value
            If value = True Then
                V_SiAutoPostBack = False
            End If
        End Set
    End Property

    Public Property CadreWidth() As System.Web.UI.WebControls.Unit
        Get
            Return CadreEtiData.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            CadreEtiData.Width = value
        End Set
    End Property

    Public WriteOnly Property DonStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Donnee.Style.Remove(Strings.Trim(TableauW(0)))
                Donnee.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property DonText() As String
        Get
            Return Donnee.Text
        End Get
        Set(ByVal value As String)
            Donnee.Text = value
        End Set
    End Property

    Public Property DonHeight() As System.Web.UI.WebControls.Unit
        Get
            Return Donnee.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Donnee.Height = value
        End Set
    End Property

    Public Property DonWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Donnee.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Donnee.Width = value
        End Set
    End Property

    Public Property DonBackColor() As System.Drawing.Color
        Get
            Return Donnee.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Donnee.BackColor = value
        End Set
    End Property

    Public Property DonForeColor() As System.Drawing.Color
        Get
            Return Donnee.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Donnee.ForeColor = value
        End Set
    End Property

    Public Property DonBorderColor() As System.Drawing.Color
        Get
            Return Donnee.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Donnee.BorderColor = value
        End Set
    End Property

    Public Property DonBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Donnee.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Donnee.BorderWidth = value
        End Set
    End Property

    Public Property DonBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return Donnee.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Donnee.BorderStyle = value
        End Set
    End Property

    Public Property DonTooltip() As String
        Get
            Return Donnee.ToolTip
        End Get
        Set(ByVal value As String)
            Donnee.ToolTip = value
        End Set
    End Property

    Public Property DonVisible() As Boolean
        Get
            Return Donnee.Visible
        End Get
        Set(ByVal value As Boolean)
            Donnee.Visible = value
        End Set
    End Property

    Public Property DonMaxLength() As Integer
        Get
            Return Donnee.MaxLength
        End Get
        Set(ByVal value As Integer)
            Donnee.MaxLength = value
        End Set
    End Property

    Public Property DonTabIndex() As Integer
        Get
            Return Donnee.TabIndex
        End Get
        Set(ByVal value As Integer)
            Donnee.TabIndex = CShort(value)
            MyBase.V_TabIndex = value
        End Set
    End Property

    Public Property DonTextMode() As Boolean
        Get
            If Donnee.TextMode = TextBoxMode.SingleLine Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            If value = False Then
                Donnee.TextMode = TextBoxMode.SingleLine
            Else
                Donnee.TextMode = TextBoxMode.MultiLine
            End If
        End Set
    End Property

    Public WriteOnly Property EtiStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Etiquette.Style.Remove(Strings.Trim(TableauW(0)))
                Etiquette.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property EtiText() As String
        Get
            Return Etiquette.Text
        End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Public Property EtiHeight() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Height = value
        End Set
    End Property

    Public Property EtiWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Width = value
        End Set
    End Property

    Public Property EtiBackColor() As System.Drawing.Color
        Get
            Return Etiquette.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor() As System.Drawing.Color
        Get
            Return Etiquette.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.ForeColor = value
        End Set
    End Property

    Public Property EtiBorderColor() As System.Drawing.Color
        Get
            Return Etiquette.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BorderColor = value
        End Set
    End Property

    Public Property EtiBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.BorderWidth = value
        End Set
    End Property

    Public Property EtiBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return Etiquette.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Etiquette.BorderStyle = value
        End Set
    End Property

    Public Property EtiTooltip() As String
        Get
            Return Etiquette.ToolTip
        End Get
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property

    Public Property EtiVisible() As Boolean
        Get
            Return Etiquette.Visible
        End Get
        Set(ByVal value As Boolean)
            Etiquette.Visible = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If V_SiDonneeDico = True And Etiquette.Text = "" Then
            Etiquette.Text = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).Etiquette
        End If
        If V_SiDonneeDico = True And Donnee.MaxLength = 0 Then
            Donnee.MaxLength = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).LongueurMaxi
            '' AKR 27/02/2018
            Donnee.Attributes.Add("onkeyup", "reste(this.value, this.id, " & Donnee.MaxLength & ") ;")
            ''*** 

        End If
        If Donnee.TextMode = TextBoxMode.MultiLine Then
            If Donnee.MaxLength > 0 Then
                RegExpArea.ValidationExpression = "[\s\S]{0," & Donnee.MaxLength & "}"
                RegExpArea.ErrorMessage = "Longueur maximum de " & Donnee.MaxLength & " caractères atteinte"
                RegExpArea.Enabled = True
            End If
        End If
        If V_SiDonneeDico = True Then
            Donnee.BorderColor = V_DonBordercolor
            Etiquette.BackColor = V_EtiBackcolor
            Etiquette.ForeColor = V_EtiForecolor
            Etiquette.BorderColor = V_EtiBordercolor

            Donnee.Font.Name = V_FontName
            Donnee.Font.Size = V_FontTaille
            Etiquette.Font.Name = V_FontName
            Etiquette.Font.Size = V_FontTaille
            Etiquette.Font.Bold = V_FontBold
            Etiquette.Font.Italic = V_FontItalic
        End If
    End Sub

    Protected Sub Donnee_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Donnee.TextChanged
        If V_SiEnLectureSeule = True Then
            Exit Sub
        End If
        If Donnee.MaxLength > 0 Then
            Dim Chaine As String = Donnee.Text
            If Chaine.Length > Donnee.MaxLength Then
                Donnee.Text = Strings.Left(Chaine, Donnee.MaxLength)
            End If
        End If

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Donnee.Text)
        Saisie_Change(Evenement)
    End Sub
    Friend Class TextChanged
    End Class
End Class
