﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Datas
    Public Class ObjetNavigation
        Private WsParent As Virtualia.Net.Datas.ObjetUtilisateur
        Private WsParentAno As Virtualia.Net.Datas.ObjetUtiAnonyme
        'Contexte Fenêtres spécifiques
        Private WsDossierEvaluation As Virtualia.Net.Entretien.DossierEntretien
        Private WsDossierHonda As Virtualia.Net.Entretien.DossierHonda
        Private WsIdentifiantCourant As Integer = 0

        'Système de référence
        Private WsSysRefPvueInverse As Integer
        Private WsSysRefNomTable As String
        Private WsSysRefLettre As String = ""
        Private WsSysRefIDAppelant As String 'ID du contrôle appelant le SysRef
        Private WsSysRefObjetAppelant As Integer 'Objet origine appelant le SysRef
        Private WsSysRefIDVueRetour As String
        Private WsSysRefDuoPvueInverse(1) As Integer
        Private WsSysRefDuoNomTable(1) As String
        'Fenetres
        Private WsFenActiveVue(12) As Integer
        Private WsFenVuePrecedente(12) As Integer
        Private WsPointdeVueActif As Integer
        Private WsOutilActif As Integer
        Private WsTsTampon As ArrayList
        Private WsTypeFrmCourant As String = ""
        '
        Private WsSiMailAFaire As Boolean = False
        '
        Public ReadOnly Property VParent() As Virtualia.Net.Datas.ObjetUtilisateur
            Get
                Return WsParent
            End Get
        End Property

        Public ReadOnly Property VParentAno() As Virtualia.Net.Datas.ObjetUtiAnonyme
            Get
                Return WsParentAno
            End Get
        End Property

        Public Property SiMailAfaire As Boolean
            Get
                Return WsSiMailAFaire
            End Get
            Set(value As Boolean)
                WsSiMailAFaire = value
            End Set
        End Property

        Public Property DossierEvaluation As Virtualia.Net.Entretien.DossierEntretien
            Get
                If WsDossierEvaluation IsNot Nothing And WsTypeFrmCourant <> "" Then
                    WsDossierEvaluation.Referentiel_TypeEntretien = WsTypeFrmCourant
                End If
                Return WsDossierEvaluation
            End Get
            Set(ByVal value As Virtualia.Net.Entretien.DossierEntretien)
                WsDossierEvaluation = value
            End Set
        End Property

        Public Property DossierHonda As Virtualia.Net.Entretien.DossierHonda
            Get
                Return WsDossierHonda
            End Get
            Set(ByVal value As Virtualia.Net.Entretien.DossierHonda)
                WsDossierHonda = value
            End Set
        End Property

        Public Property Identifiant_Courant() As Integer
            Get
                Return WsIdentifiantCourant
            End Get
            Set(ByVal value As Integer)
                WsIdentifiantCourant = value
            End Set
        End Property

        Public Property TypeFrmCourant As String
            Get
                Return WsTypeFrmCourant
            End Get
            Set(value As String)
                WsTypeFrmCourant = value
                If WsDossierEvaluation IsNot Nothing Then
                    WsDossierEvaluation.Referentiel_TypeEntretien = value
                End If
            End Set
        End Property

        Public Property SysRef_Lettre() As String
            Get
                Return WsSysRefLettre
            End Get
            Set(ByVal value As String)
                WsSysRefLettre = value
            End Set
        End Property

        Public Property SysRef_IDAppelant() As String
            Get
                Return WsSysRefIDAppelant
            End Get
            Set(ByVal value As String)
                WsSysRefIDAppelant = value
            End Set
        End Property

        Public Property SysRef_ObjetAppelant() As Integer
            Get
                Return WsSysRefObjetAppelant
            End Get
            Set(ByVal value As Integer)
                WsSysRefObjetAppelant = value
            End Set
        End Property

        Public Property SysRef_PointdeVueInverse() As Integer
            Get
                Return WsSysRefPvueInverse
            End Get
            Set(ByVal value As Integer)
                WsSysRefPvueInverse = value
            End Set
        End Property

        Public Property SysRef_NomTable() As String
            Get
                Return WsSysRefNomTable
            End Get
            Set(ByVal value As String)
                WsSysRefNomTable = value
            End Set
        End Property

        Public Property SysRef_IDVueRetour() As String
            Get
                Return WsSysRefIDVueRetour
            End Get
            Set(ByVal value As String)
                WsSysRefIDVueRetour = value
            End Set
        End Property

        Public Property SysRef_Duo_PointdeVueInverse(ByVal Index As Integer) As Integer
            Get
                Select Case Index
                    Case 0, 1
                        Return WsSysRefDuoPvueInverse(Index)
                End Select
                Return 0
            End Get
            Set(ByVal value As Integer)
                Select Case Index
                    Case 0, 1
                        WsSysRefDuoPvueInverse(Index) = value
                End Select
            End Set
        End Property

        Public Property SysRef_Duo_NomTable(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0, 1
                        Return WsSysRefDuoNomTable(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0, 1
                        WsSysRefDuoNomTable(Index) = value
                End Select
            End Set
        End Property

        Public Sub InitialiserFenetres()
            Dim IndiceI As Integer
            For IndiceI = 0 To WsFenActiveVue.Count - 1
                WsFenActiveVue(IndiceI) = 0
            Next IndiceI
            For IndiceI = 0 To WsFenVuePrecedente.Count - 1
                WsFenVuePrecedente(IndiceI) = 0
            Next IndiceI
        End Sub

        Public Property Fenetre_VueActive(ByVal Index As Integer) As Integer
            Get
                If Index > WsFenActiveVue.Count - 1 Then
                    Return -1
                    Exit Property
                End If
                Return WsFenActiveVue(Index)
            End Get
            Set(ByVal value As Integer)
                If Index > WsFenActiveVue.Count - 1 Then
                    Exit Property
                End If
                WsFenVuePrecedente(Index) = WsFenActiveVue(Index)
                WsFenActiveVue(Index) = value
            End Set
        End Property

        Public ReadOnly Property Fenetre_VuePrecedente(ByVal Index As Integer) As Integer
            Get
                If Index > WsFenVuePrecedente.Count - 1 Then
                    Return -1
                    Exit Property
                End If
                Return WsFenVuePrecedente(Index)
            End Get
        End Property

        Public Property Fenetre_PointdeVue() As Integer
            Get
                Return WsPointdeVueActif
            End Get
            Set(ByVal value As Integer)
                WsPointdeVueActif = value
            End Set
        End Property

        Public Property Fenetre_Outil() As Integer
            Get
                Return WsOutilActif
            End Get
            Set(ByVal value As Integer)
                WsOutilActif = value
            End Set
        End Property

        Public Property TsTampon() As ArrayList
            Get
                Return WsTsTampon
            End Get
            Set(ByVal value As ArrayList)
                WsTsTampon = value
            End Set
        End Property

        Public Sub New(ByVal Host As Virtualia.Net.Datas.ObjetUtiAnonyme)
            WsParentAno = Host
        End Sub

        Public Sub New(ByVal Host As Virtualia.Net.Datas.ObjetUtilisateur)
            WsParent = Host
        End Sub
    End Class
End Namespace