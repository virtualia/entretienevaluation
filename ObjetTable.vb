﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class ObjetTable
        Private WsIntitule As String = ""
        Private WsClefIndex As Integer
        Private WsRangFamille As String = ""
        Private WsLibelFamille As String = ""
        Private WsRangSousFamille As String = ""
        Private WsLibelSousFamille As String = ""
        Private WsClefOrigine As String = ""

        Public Property Intitule As String
            Get
                Return WsIntitule
            End Get
            Set(value As String)
                WsIntitule = value
            End Set
        End Property

        Public Property Clef_Index As Integer
            Get
                Return WsClefIndex
            End Get
            Set(value As Integer)
                WsClefIndex = value
            End Set
        End Property

        Public Property Rang_Famille As String
            Get
                Return WsRangFamille
            End Get
            Set(value As String)
                WsRangFamille = value
            End Set
        End Property

        Public Property Libelle_Famille As String
            Get
                Return WsLibelFamille
            End Get
            Set(value As String)
                WsLibelFamille = value
            End Set
        End Property

        Public Property Rang_SousFamille As String
            Get
                Return WsRangSousFamille
            End Get
            Set(value As String)
                WsRangSousFamille = value
            End Set
        End Property

        Public Property Libelle_SousFamille As String
            Get
                Return WsLibelSousFamille
            End Get
            Set(value As String)
                WsLibelSousFamille = value
            End Set
        End Property

        Public Property Clef_Origine As String
            Get
                Return WsClefOrigine
            End Get
            Set(value As String)
                WsClefOrigine = value
            End Set
        End Property

    End Class
End Namespace