﻿Imports Microsoft.VisualBasic
Namespace Entretien
    Public Interface IGenericEntretien
        ReadOnly Property LitteralSynthese(ByVal Critere As Integer) As String

        ReadOnly Property LitteralResultat(ByVal Critere As Integer) As String

        ReadOnly Property LitteralCompetence(ByVal Critere As Integer, Optional ByVal SiPrecision As Boolean = False) As String

        ReadOnly Property Table_Competences() As List(Of Virtualia.Net.Entretien.ObjetTable)

        ReadOnly Property Table_Observations() As List(Of Virtualia.Net.Entretien.ObjetTable)

        ReadOnly Property Table_Formations() As List(Of Virtualia.Net.Entretien.ObjetTable)

        ReadOnly Property Table_Syntheses() As List(Of Virtualia.Net.Entretien.ObjetTable)


        ReadOnly Property Table_Complements() As List(Of Virtualia.Net.Entretien.ObjetTable)

    End Interface
End Namespace