﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_APPRECIATION_PNF.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_APPRECIATION_PNF" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreAppreciation" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="APPRECIATION GLOBALE" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow> 
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreAppreciationGlobale" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreAppreciation" runat="server" Height="35px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="L'appréciation globale est :"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="CocheA10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="true" V_Height="30px"
                           V_Text="  Insuffisante" V_Width="250px" V_SiModeCaractere="true"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left; Font-size: 120%"/>               
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="CocheB10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="true" V_Height="30px"
                           V_Text="  Partielle" V_Width="250px" V_SiModeCaractere="true"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left; Font-size: 120%"/>               
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="CocheC10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="true" V_Height="30px"
                           V_Text="  Satisfaisante" V_Width="250px" V_SiModeCaractere="true"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left; Font-size: 120%"/>               
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="CocheD10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="true" V_Height="30px"
                           V_Text="  Trés satisfaisante" V_Width="250px" V_SiModeCaractere="true"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left; Font-size: 120%"/>               
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="CocheE10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="true" V_Height="30px"
                           V_Text="  Exceptionnelle" V_Width="250px" V_SiModeCaractere="true"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left; Font-size: 120%"/>               
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"  ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero10" runat="server" Height="35px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Bilan de l'année"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="35px" Width="370px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Commentaires de l'agent sur le bilan"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="35px" Width="370px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Commentaires du manager sur le bilan"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVV03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="True"
                           EtiWidth="370px" DonWidth="368px" DonHeight="280px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="True"
                           EtiWidth="370px" DonWidth="368px" DonHeight="280px" EtiHeight="20px" DonTabIndex="2" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 0px;"                     
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="25px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"  ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero13" runat="server" Height="35px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Engagement sur l'année n+1"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero14" runat="server" Height="35px" Width="370px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Commentaires de l'agent sur l'engagement année n+1"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero15" runat="server" Height="35px" Width="370px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Commentaires du manager sur l'engagement année n+1"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVW03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="True"
                           EtiWidth="370px" DonWidth="368px" DonHeight="280px" EtiHeight="20px" DonTabIndex="3" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVX03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="True"
                           EtiWidth="370px" DonWidth="368px" DonHeight="280px" EtiHeight="20px" DonTabIndex="4" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 0px;"                     
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>
 </asp:Table>