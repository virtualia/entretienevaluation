﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_ENTETE_PNF.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_ENTETE_PNF" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

 <asp:Table ID="CadreListe" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0"
    HorizontalAlign="Center">
    <asp:TableRow> 
      <asp:TableCell> 
         <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="600px" SiColonneSelect="true"/>
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>
 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="CadreTitreEntretien" runat="server" Height="50px" CellPadding="0" Width="750px" 
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#124545">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                        BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                        BorderWidth="2px" ForeColor="#0E5F5C"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                        style="margin-top: 1px; margin-left: 1px; margin-bottom: 0px;
                        font-style: normal; text-indent: 1px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow> 
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelAnneeEntretien" runat="server" Text="Année 2012" Height="20px" Width="200px"
                        BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                        BorderWidth="2px" ForeColor="#0E5F5C"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                        style="margin-top: 10px; margin-left: 1px; margin-bottom: 10px;
                        font-style:  normal; text-indent: 2px; text-align: center;">
                     </asp:Label>          
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server"
                        V_PointdeVue="1" V_Objet="150" V_Information="0" V_SiDonneeDico="true"
                        EtiWidth="250px" DonWidth="80px" DonTabIndex="1" EtiText="Date de réalisation de l'entretien"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="15px"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="500px"
                        BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                        font-style: normal; text-indent: 1px; text-align: center;">
                     </asp:Label>          
                </asp:TableCell> 
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelPoste" runat="server" Height="24px" Width="500px"
                        BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                        font-style:  oblique; text-indent: 10px; text-align: left;">
                     </asp:Label>          
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelStatut" runat="server" Height="24px" Width="500px"
                        BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                        font-style:  oblique; text-indent: 10px; text-align: left;">
                     </asp:Label>          
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelEchelon" runat="server" Height="24px" Width="500px"
                        BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                        font-style:  oblique; text-indent: 10px; text-align: left;">
                     </asp:Label>          
                </asp:TableCell>
            </asp:TableRow> 
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow> 
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="CadreSignature" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                       V_PointdeVue="1" V_Objet="150" V_Information="2" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                       EtiWidth="180px" DonWidth="350px" DonTabIndex="2" Etitext="Service de l'évalué"/> 
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                       V_PointdeVue="1" V_Objet="150" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                       EtiWidth="180px" DonWidth="350px" DonTabIndex="3" EtiText="Nom de l'évaluateur"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="20px"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                       V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true"
                       EtiWidth="280px" DonWidth="80px" DonTabIndex="4" EtiText="Date de signature de l'évaluateur"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="LabelSignatureEvalue" runat="server" Height="18px" Width="750px"
                       BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet"
                       Text="L'agent reconnaît avoir pris connaissance du présent compte-rendu d'entretien"
                       BorderWidth="1px" ForeColor="#2FA49B" Font-Italic="False"
                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 5px;
                       font-style:  oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                       V_PointdeVue="1" V_Objet="150" V_Information="4" V_SiDonneeDico="true"
                       EtiWidth="280px" DonWidth="80px" DonTabIndex="5" EtiText="Date de signature de l'agent (notification)"
                       DonTooltip="Attention. Vous ne pourrez plus modifier ultérieurement le formulaire d'entretien"/>
                </asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="LabelMessage" runat="server" Height="18px" Width="750px"
                       BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet"
                       Text="La date de signature de l'agent rend non modifiable le formulaire d'entretien"
                       BorderWidth="1px" ForeColor="#731E1E" Font-Italic="False"
                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                       style="margin-top: 1px; margin-left: 0px; margin-bottom: 5px;
                       font-style:  oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>
            </asp:TableRow> 
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>