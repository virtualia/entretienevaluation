﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_COMPETENCE.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreCompetence" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL ET DE FORMATION" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="3. MANIERE DE SERVIR" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations" runat="server" Height="80px" Width="746px"
                           BackColor="#98D4CA" BorderColor="#B0E0D7" BorderStyle="none"
                           Text="Les quatre rubriques exprimant la valeur professionnelle de l'agent sont renseignées 
                           à l'aide des grilles d'analyse qui suivent. Les critères énumérés pour chacune d'entre elles 
                           sont utilisés au regard de la spécificité du métier exercé par l'agent et des exigences du poste 
                           qu'il occupe. Certains de ces critères pourront donc apparaître plus ou moins pertinents. 
                           Pour l'essentiel il s'agit des critères qui étaient précédemment utilisés dans le cadre de l'exercice de notation."
                           BorderWidth="1px" ForeColor="#0F2F30"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille1" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="3.1 Compétences professionnelles et technicité"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille11" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="Transparent" BorderStyle="NotSet" Text=".  Connaissances du domaine d'activité"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille11" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="Transparent" BorderStyle="NotSet" Text="évaluation des connaissances nécessaires pour l'exercice des fonctions"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences311" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence" runat="server" Height="44px" Width="260px"
                                       BackColor="#DBF5EF" BorderColor="#9EB0AC" BorderStyle="Notset" Text="composantes"
                                       BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote1" runat="server" Height="44px" Width="75px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="non requis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote2" runat="server" Height="44px" Width="80px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="insuffisant"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote3" runat="server" Height="44px" Width="95px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="en cours d'acquisition"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote4" runat="server" Height="44px" Width="75px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="acquis"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote5" runat="server" Height="44px" Width="75px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="maîtrisé"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote6" runat="server" Height="44px" Width="75px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="expert"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHA01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                     Etivisible="False" DonWidth="250px" DonHeight="20px" 
                                     DonText="juridiques" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHA07" runat="server" V_Groupe="Critere111"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHB01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="2"
                                     Etivisible="False" DonWidth="250px" DonHeight="20px" 
                                     DonText="administratives" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHB07" runat="server" V_Groupe="Critere112"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHC01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="3"
                                     Etivisible="False" DonWidth="250px" DonHeight="20px" 
                                     DonText="budgétaires et financières" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHC07" runat="server" V_Groupe="Critere113"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHD01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="4"
                                     Etivisible="False" DonWidth="250px" DonHeight="20px" 
                                     DonText="gestion des ressources humaines" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHD07" runat="server" V_Groupe="Critere114"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHE01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="5"
                                     Etivisible="False" DonWidth="250px" DonHeight="20px" 
                                     DonText="techniques" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHE07" runat="server" V_Groupe="Critere115"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHF01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="6"
                                     Etivisible="False" DonWidth="250px" DonHeight="20px" 
                                     DonText="nouvelles technologies" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHF07" runat="server" V_Groupe="Critere116"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHG01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="7"
                                     Etivisible="False" DonWidth="250px" DonHeight="20px" 
                                     DonText="accueil et orientation du public" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHG07" runat="server" V_Groupe="Critere117"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHH01" runat="server"
                                     V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true" DonTabIndex="8"
                                     Etivisible="False" DonWidth="250px" DonHeight="20px" DonText=""
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHH07" runat="server" V_Groupe="Critere118"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille12" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Souci de formation et de perfectionnement"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille12" runat="server" Height="38px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="aptitude à acquérir et à développer les connaissances nécessaires pour l'exercice des fonctions; rapidité d'acquisition; souci d'approfondissement et étendue des connaissances"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHI07" runat="server" V_Groupe="Critere312"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille13" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Connaissances de l'environnement professionnel et capacités à s'y situer"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille13" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="connaissance de l'organisation et du fonctionnement de l'administration et de la juridiction administrative"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHJ07" runat="server" V_Groupe="Critere313"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille14" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Qualités d'expression écrite"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille14" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="aptitude à rédiger des textes construits et écrits correctement"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHK07" runat="server" V_Groupe="Critere314"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille15" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Qualités d'expression orale"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille15" runat="server" Height="38px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="capacité à s'exprimer clairement et avec précision, à exposer un sujet en se mettant à la portée de son interlocuteur; aptitude à transmettre sa pensée; capacité à convaincre"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHL07" runat="server" V_Groupe="Critere315"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="Observations littérales, le cas échéant"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille2" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="3.2 Qualités personnelles et relationnelles"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille21" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Présentation"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille21" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="image que l'agent donne de lui-même et du service"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHM07" runat="server" V_Groupe="Critere321"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille22" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Maîtrise de soi"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille22" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="aptitude à faire face sereinement aux situations difficiles ou inattendues"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHN07" runat="server" V_Groupe="Critere322"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille23" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Aptitudes relationnelles et sens des relations humaines"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille23" runat="server" Height="57px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="aptitude à entretenir des relations ouvertes avec ses supérieurs, ses collaborateurs et ses interlocuteurs 
                           ; aptitude à coopérer avec ses collègues et à apporter à ses supérieurs une collaboration efficace dans la réception, l'interprétation et l'application de leurs directives"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHO07" runat="server" V_Groupe="Critere323"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille24" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Esprit d'initiative"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille24" runat="server" Height="38px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="capacité à prendre les initiatives opportunes en fonction des situations et des objectifs; 
                           souci d'amélioration du service rendu et du fonctionnement du service, aptitude à accompagner le changement"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHP07" runat="server" V_Groupe="Critere324"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille25" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Sens du service public et conscience professionnelle"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille25" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="sens de l'intérêt général; souci du service rendu aux usagers (internes ou externes)"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHQ07" runat="server" V_Groupe="Critere325"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille26" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Capacité à respecter l'organisation collective du travail et sens du travail en équipe"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille26" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text=""
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHR07" runat="server" V_Groupe="Critere326"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="2" 
                           Etitext="Observations littérales, le cas échéant"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille3" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="3.3 Méthode et résultats"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille31" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Fiabilité et qualité du travail"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille31" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="capacité à produire un travail régulier, fiable et de qualité"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHS07" runat="server" V_Groupe="Critere331"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille32" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Capacités d'analyse"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille32" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text=""
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHT07" runat="server" V_Groupe="Critere332"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille33" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Esprit de synthèse"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille33" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text=""
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHU07" runat="server" V_Groupe="Critere333"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille34" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Disponibilité et puissance de travail"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille34" runat="server" Height="38px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="implication personnelle dans le service, réactivité; capacité à produire rapidement un travail 
                           difficile ou à mener de front avec fiabilité plusieurs dossiers importants"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHV07" runat="server" V_Groupe="Critere334"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille35" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Sens de l'organisation"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille35" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="aptitude à planifier le travail et à dégager des priorités"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHW07" runat="server" V_Groupe="Critere335"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille36" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Efficacité et respect des délais"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille36" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="aptitude à obtenir dans les meilleurs délais possibles les résultats attendus"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHX07" runat="server" V_Groupe="Critere336"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="3" 
                           Etitext="Observations littérales, le cas échéant"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>    
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille4" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="3.4 Aptitude au management et à la conduite de projet"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille41" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Aptitude à l'encadrement et à l'animation d'équipe"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille41" runat="server" Height="38px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="aptitude à instaurer ou à maintenir la cohésion d'un groupe, à orienter 
                           et motiver ses collaborateurs, à leur fixer des objectifs réalistes"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHY07" runat="server" V_Groupe="Critere341"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="161px"
                        VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="non requis" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille42" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Capacité à dialoguer et à communiquer avec ses collaborateurs"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille42" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="sens de l'écoute; aptitude à prévenir, à gérer et à arbitrer les conflits"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioHZ07" runat="server" V_Groupe="Critere342"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="161px"
                        VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="non requis" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille43" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Capacité de décision et d'exercice des responsabilités"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille43" runat="server" Height="57px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="capacité à prendre, dans les délais nécessaires au bon fonctionnement du service, 
                           les décisions relevant de son niveau de responsabilités et adaptées à la situation; capacité à assumer pleinement la responsabilité de ses actions, 
                           voire celles de ses collaborateurs"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioH107" runat="server" V_Groupe="Critere343"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="161px"
                        VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="non requis" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille44" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Capacité à conduire des projets"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille44" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="initiative, capacité à faire face à toutes les situations dans le but d'atteindre les objectifs fixés"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioH207" runat="server" V_Groupe="Critere344"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="161px"
                        VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="non requis" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille45" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Délégation, contrôle et suivi des dossiers"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille45" runat="server" Height="38px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="aptitude à confier à un subordonné la réalisation d'une action tout en conservant 
                           la responsabilité; aptitude à exercer un contrôle et à se faire rendre compte"
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioH307" runat="server" V_Groupe="Critere345"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="161px"
                        VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="non requis" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelSousFamille46" runat="server" Height="30px" Width="748px"
                           BackColor="#DBF5EF" BorderColor="#DBF5EF" BorderStyle="NotSet" Text=".  Aptitude à former des collaborateurs et à valoriser leurs compétences"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoSousFamille46" runat="server" Height="19px" Width="748px"
                           BackColor="#9EB0AC" BorderColor="#9EB0AC" BorderStyle="NotSet" Text=""
                           BorderWidth="1px" ForeColor="White" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="center"
                       BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="#DBF5EF">        
                       <Virtualia:VSixBoutonRadio ID="RadioH407" runat="server" V_Groupe="Critere346"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="161px"
                        VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="non requis" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="4" 
                           Etitext="Observations littérales, le cas échéant"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 
 </asp:Table>
