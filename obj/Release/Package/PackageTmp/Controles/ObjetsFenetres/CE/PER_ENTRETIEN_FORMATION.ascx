﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_FORMATION.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_FORMATION" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioVerticalRadio.ascx" tagname="VTrioVerticalRadio" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreFormation" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL ET DE FORMATION" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="6. BESOINS DE FORMATION" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Jean DUPONT"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero1" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="6.1 Bilan de la période écoulée"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitre11" runat="server" Height="30px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="6.1.1 Formations suivies"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px;
                           font-style: normal; text-indent: 20px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                         <asp:Label ID="LabelFormation1" runat="server" Height="24px" Width="580px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                         <asp:Label ID="LabelFormation2" runat="server" Height="24px" Width="580px"
                            BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                         <asp:Label ID="LabelFormation3" runat="server" Height="24px" Width="580px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  oblique; text-indent: 10px; text-align: left;">
                         </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitre12" runat="server" Height="30px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="6.1.2 Demandes de formation non satisfaites"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 20px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVU03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="746px" DonHeight="140px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="false"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitre13" runat="server" Height="30px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="6.1.3 Actions conduites en tant que formateur"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 20px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVV03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="746px" DonHeight="140px" EtiHeight="20px" DonTabIndex="2" 
                           Etivisible="false"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="6.2 Formations proposées pour la nouvelle période"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitre21" runat="server" Height="30px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="6.2.1 Expression des besoins en formation"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 10px;
                           font-style: normal; text-indent: 20px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin1" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <asp:Label ID="LabelTypeBesoin1" runat="server" Height="30px" Width="740px"
                               BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Formations d'adaptation au poste de travail"
                               BorderWidth="1px" ForeColor="White" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage1" runat="server" Height="50px" Width="220px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                               Text="Intitulé du stage"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode1" runat="server" Height="50px" Width="120px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Période souhaitée"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteObjectif1" runat="server" Height="50px" Width="240px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteAvis1" runat="server" Height="50px" Width="145px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="3" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="4" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="5" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVA06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisA" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHA07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="6" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px" ColumnSpan="4"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="7" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="8" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="9" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVB06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisB" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHB07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="10" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px" ColumnSpan="4"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="11" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="12" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="13" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVC06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisC" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHC07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="14" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin2" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <asp:Label ID="LabelTypeBesoin2" runat="server" Height="30px" Width="740px"
                               BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Formations à l'évolution des métiers"
                               BorderWidth="1px" ForeColor="White" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage2" runat="server" Height="50px" Width="220px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                               Text="Intitulé du stage"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode2" runat="server" Height="50px" Width="120px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Période souhaitée"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteObjectif2" runat="server" Height="50px" Width="240px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteAvis2" runat="server" Height="50px" Width="145px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="15" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="16" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="17" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVD06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisD" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHD07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="18" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px" ColumnSpan="4"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="19" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="20" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="21" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVE06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisE" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHE07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="22" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px" ColumnSpan="4"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="23" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="24" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="25" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVF06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisF" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHF07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="26" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin3" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <asp:Label ID="LabelTypeBesoin3" runat="server" Height="30px" Width="740px"
                               BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Formations de développement ou d'acquisition de qualifications nouvelles"
                               BorderWidth="1px" ForeColor="White" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage3" runat="server" Height="50px" Width="220px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                               Text="Intitulé du stage"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode3" runat="server" Height="50px" Width="120px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Période souhaitée"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteObjectif3" runat="server" Height="50px" Width="240px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteAvis3" runat="server" Height="50px" Width="145px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="27" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="28" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="29" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVG06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisG" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHG07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="30" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px" ColumnSpan="4"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="31" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="32" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="33" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVH06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisH" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHH07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="34" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px" ColumnSpan="4"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="35" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="36" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="37" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVI06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisI" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHI07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="38" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitre22" runat="server" Height="30px" Width="748px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="6.2.2 Expression des autres besoins"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 10px;
                           font-style: normal; text-indent: 20px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin4" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <asp:Label ID="LabelTypeBesoin4" runat="server" Height="30px" Width="740px"
                               BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Préparation aux concours ou examen professionnel"
                               BorderWidth="1px" ForeColor="White" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage4" runat="server" Height="50px" Width="220px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                               Text="Intitulé du concours ou de l'examen professionnel"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode4" runat="server" Height="50px" Width="120px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Période souhaitée"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteObjectif4" runat="server" Height="50px" Width="240px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteAvis4" runat="server" Height="50px" Width="145px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="39" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="40" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="41" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVJ06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisJ" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHJ07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="42" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px" ColumnSpan="4"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="43" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="44" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="45" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVK06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisK" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHK07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="46" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell Height="5px" ColumnSpan="4"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="47" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="48" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="49" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVL06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisL" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHL07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="50" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow>
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin5" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <asp:Label ID="LabelTypeBesoin5" runat="server" Height="30px" Width="740px"
                               BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Validation des acquis de l'expérience"
                               BorderWidth="1px" ForeColor="White" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage5" runat="server" Height="50px" Width="220px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                               Text="Qualification recherchée"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode5" runat="server" Height="50px" Width="120px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Période souhaitée"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteObjectif5" runat="server" Height="50px" Width="240px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteAvis5" runat="server" Height="50px" Width="145px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="51" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="52" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="53" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVM06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisM" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>  
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHM07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="54" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin6" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                            <asp:Label ID="LabelTypeBesoin6" runat="server" Height="30px" Width="740px"
                               BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Bilan de compétences"
                               BorderWidth="1px" ForeColor="White" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode6" runat="server" Height="50px" Width="120px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Période souhaitée"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteObjectif6" runat="server" Height="50px" Width="464px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteAvis6" runat="server" Height="50px" Width="145px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="55" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="462px" DonHeight="60px" DonTabIndex="56" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVN06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisN" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow> 
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHN07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="57" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>  
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin7" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="5">
                            <asp:Label ID="LabelTypeBesoin7" runat="server" Height="30px" Width="740px"
                               BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Période de professionnalisation"
                               BorderWidth="1px" ForeColor="White" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage7" runat="server" Height="50px" Width="145px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                               Text="Emploi envisagé"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode7" runat="server" Height="50px" Width="120px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Durée et période souhaitée"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteObjectif7" runat="server" Height="50px" Width="195px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteEcheance7" runat="server" Height="50px" Width="120px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Echéance souhaitée pour le projet"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteAvis7" runat="server" Height="50px" Width="142px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="143px" DonHeight="60px" DonTabIndex="58" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="59" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="193px" DonHeight="60px" DonTabIndex="60" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO08" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="8" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="61" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVO06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisO" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="5">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHO07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="62" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>   
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="TableBesoin8" runat="server" CellPadding="0" CellSpacing="0" Width="740px"
                      BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px">
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <asp:Label ID="LabelTypeBesoin8" runat="server" Height="30px" Width="740px"
                               BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Congé de formation professionnelle"
                               BorderWidth="1px" ForeColor="White" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 0px; text-align:  center;">
                            </asp:Label>          
                          </asp:TableCell>    
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteStage8" runat="server" Height="50px" Width="220px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                               Text="Formation envisagée"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEntetePeriode8" runat="server" Height="50px" Width="120px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Durée et période souhaitée"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>    
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteObjectif8" runat="server" Height="50px" Width="240px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Objectif"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LabelEnteteAvis8" runat="server" Height="50px" Width="145px"
                               BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px" 
                               Text="Avis du supérieur hiérarchique conduisant l'entretien"
                               ForeColor="#124545" Font-Italic="False"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "11px"
                               style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                               font-style: normal; text-indent: 1px; text-align:  center;">
                            </asp:Label>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP03" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="218px" DonHeight="60px" DonTabIndex="63" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP04" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="4" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="118px" DonHeight="60px" DonTabIndex="64" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP05" runat="server" DonTextMode="true"
                               V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="true"
                               Etivisible="False" DonWidth="238px" DonHeight="60px" DonTabIndex="65" 
                               Donstyle="margin-left: 0px; margin-bottom: 1px;"
                               DonBorderWidth="1px"/>
                          </asp:TableCell>
                          <asp:TableCell HorizontalAlign="Center">
                            <Virtualia:VTrioVerticalRadio ID="RadioVP06" runat="server"  
                               V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="true"
                               RadioCentreText="Favorable" RadioGaucheText="Défavorable" RadioDroiteText=""
                               V_Groupe="GroupeAvisP" RadioDroiteVisible="false"
                               RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="5px"
                               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>
                          </asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                          <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                            <Virtualia:VCoupleEtiDonnee ID="InfoHP07" runat="server"
                               V_PointdeVue="1" V_Objet="155" V_Information="7" V_SiDonneeDico="true" DonTabIndex="66" 
                               Donstyle="margin-left: 0px; margin-bottom: 0px;"
                               EtiWidth="250px" DonWidth="478px" 
                               EtiText="en cas d'avis défavorable, en préciser le motif :"
                               Etistyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size: 11px; margin-bottom: 0px;"
                               DonBorderWidth="1px" EtiHeight="18px" DonHeight="18px"/>
                          </asp:TableCell>
                        </asp:TableRow> 
                      </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>