﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_COMPETENCE_CNM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE_CNM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreCompetence" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU ENTRETIEN ANNUEL D'EVALUATION" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="GRILLE DES CRITERES D'APPRECIATION" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille1" runat="server" Height="38px" Width="740px"
                           Text="Appréciation de la manière de servir et des qualités relationnelles de l'agent"
                           CssClass="EP_TitreParagraphe"
                           style="margin-left: 0px; padding-top: 7px; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegende1" runat="server" Height="25px" Width="740px"
                           Text="PF : Point fort  -  PM : Point maîtrisé  -  PAM : Point à améliorer  -  PNS : Point non satisfaisant  -  SO : Sans objet"
                           CssClass="EP_TitreChapitre"
                           style="text-indent: 0px; text-align: center;font-size:small;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences1" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                              <asp:TableCell>
                                   <asp:Label ID="LabelCadrage1" runat="server" Height="21px" Width="521px" Text=""
                                       CssClass="EP_EtiComplement">
                                   </asp:Label>  
                              </asp:TableCell>
                              <asp:TableCell>
                                   <asp:Label ID="LabelTitreNiveau1" runat="server" Height="18px" Width="210px" Text="APPRECIATION"
                                       CssClass="EP_TitreParagraphe"
                                       Font-Size="90%" style="margin-left: 3px; margin-top: 2px; text-align: center; padding-top: 3px; font-weight:bold;">
                                   </asp:Label>  
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence1" runat="server" Height="27px" Width="519px" Text=""
                                       CssClass="EP_EtiComplement"
                                       Font-Size="90%"
                                       style="margin-top: 1px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels1" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote11" runat="server" Height="22px" Width="42px" Text="PF" Tooltip="PF : Point fort"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote12" runat="server" Height="22px" Width="42px" Text="PM" Tooltip="PM : Point maîtrisé"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote13" runat="server" Height="22px" Width="42px" Text="PAM" Tooltip="PAM : Point à améliorer"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote14" runat="server" Height="22px" Width="42px" Text="PNS" Tooltip="PNS : Point non satisfaisant"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote15" runat="server" Height="22px" Width="42px" Text="SO" Tooltip="SO : Sans objet"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>          
                                        </asp:TableCell>   
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="1" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHA07" runat="server" V_Groupe="CritereAgent1"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="2" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHB07" runat="server" V_Groupe="CritereAgent2"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="3" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHC07" runat="server" V_Groupe="CritereAgent3"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="4" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHD07" runat="server" V_Groupe="CritereAgent4"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="5" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHE07" runat="server" V_Groupe="CritereAgent5"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="6" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHF07" runat="server" V_Groupe="CritereAgent6"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="7" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHG07" runat="server" V_Groupe="CritereAgent7"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="8" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHH07" runat="server" V_Groupe="CritereAgent8"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="9" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHI07" runat="server" V_Groupe="CritereAgent9"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="10" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHJ07" runat="server" V_Groupe="CritereAgent10"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           DonWidth="735px" DonHeight="80px" DonTabIndex="11"
                           EtiVisible="true" EtiWidth="735px" EtiHeight="20px"  
                           EtiText="Commentaires du supérieur hiérarchique direct"
                           Etistyle="margin-left: 5px; text-align: left; text-indent: 2px;"
                           Donstyle="margin-left: 5px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>       
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille2" runat="server" Height="45px" Width="740px"
                           Text="Appréciation des capacités d'encadrement ou le cas échéant à exercer des fonctions d'un niveau supérieur"
                           CssClass="EP_TitreParagraphe"
                           style="margin-left: 0px; padding-top: 7px; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegende2" runat="server" Height="25px" Width="740px"
                           Text="PF : Point fort  -  PM : Point maîtrisé  -  PAM : Point à améliorer  -  PNS : Point non satisfaisant  -  SO : Sans objet"
                           CssClass="EP_TitreChapitre"
                           style="text-indent: 0px; text-align: center;font-size:small;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences2" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                              <asp:TableCell>
                                   <asp:Label ID="LabelCadrage2" runat="server" Height="21px" Width="521px" Text=""
                                       CssClass="EP_EtiComplement">
                                   </asp:Label>  
                              </asp:TableCell>
                              <asp:TableCell>
                                   <asp:Label ID="LabelTitreNiveau2" runat="server" Height="18px" Width="210px" Text="APPRECIATION"
                                       CssClass="EP_TitreParagraphe"
                                       Font-Size="90%" style="margin-left: 3px; margin-top: 2px; text-align: center; padding-top: 3px; font-weight:bold;">
                                   </asp:Label>  
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence2" runat="server" Height="27px" Width="519px" Text=""
                                       CssClass="EP_EtiComplement"
                                       Font-Size="90%"
                                       style="margin-top: 1px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote21" runat="server" Height="22px" Width="42px" Text="PF" Tooltip="PF : Point fort"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote22" runat="server" Height="22px" Width="42px" Text="PM" Tooltip="PM : Point maîtrisé"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote23" runat="server" Height="22px" Width="42px" Text="PAM" Tooltip="PAM : Point à améliorer"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote24" runat="server" Height="22px" Width="42px" Text="PNS" Tooltip="PNS : Point non satisfaisant"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote25" runat="server" Height="22px" Width="42px" Text="SO" Tooltip="SO : Sans objet"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>          
                                        </asp:TableCell>   
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="12" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHK07" runat="server" V_Groupe="CritereAgent11"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="13" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHL07" runat="server" V_Groupe="CritereAgent12"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="14" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHM07" runat="server" V_Groupe="CritereAgent13"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="15" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHN07" runat="server" V_Groupe="CritereAgent14"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="16" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHO07" runat="server" V_Groupe="CritereAgent15"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="17" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHP07" runat="server" V_Groupe="CritereAgent16"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="18" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHQ07" runat="server" V_Groupe="CritereAgent17"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVR01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="19" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHR07" runat="server" V_Groupe="CritereAgent18"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           DonWidth="735px" DonHeight="80px" DonTabIndex="20"
                           EtiVisible="true" EtiWidth="735px" EtiHeight="20px"  
                           EtiText="Commentaires du supérieur hiérarchique direct"
                           Etistyle="margin-left: 5px; text-align: left; text-indent: 2px;"
                           Donstyle="margin-left: 5px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>       
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow> 

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille3" runat="server" Height="45px" Width="740px"
                           Text="Appréciation des compétences techniques et professionnelles de l'agent et des acquis de l'expérience professionnelle"
                           CssClass="EP_TitreParagraphe"
                           style="margin-left: 0px; padding-top: 7px; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegende3" runat="server" Height="25px" Width="740px"
                           Text="PF : Point fort  -  PM : Point maîtrisé  -  PAM : Point à améliorer  -  PNS : Point non satisfaisant  -  SO : Sans objet"
                           CssClass="EP_TitreChapitre"
                           style="text-indent: 0px; text-align: center;font-size:small;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences3" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                              <asp:TableCell>
                                   <asp:Label ID="LabelCadrage3" runat="server" Height="21px" Width="521px" Text=""
                                       CssClass="EP_EtiComplement">
                                   </asp:Label>  
                              </asp:TableCell>
                              <asp:TableCell>
                                   <asp:Label ID="LabelTitreNiveau3" runat="server" Height="18px" Width="210px" Text="APPRECIATION"
                                       CssClass="EP_TitreParagraphe"
                                       Font-Size="90%" style="margin-left: 3px; margin-top: 2px; text-align: center; padding-top: 3px; font-weight:bold;">
                                   </asp:Label>  
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence3" runat="server" Height="27px" Width="519px" Text="COMPETENCES TECHNIQUES"
                                       CssClass="EP_TitreOnglet" style="margin-top: 1px;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels3" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote31" runat="server" Height="22px" Width="42px" Text="PF" Tooltip="PF : Point fort"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote32" runat="server" Height="22px" Width="42px" Text="PM" Tooltip="PM : Point maîtrisé"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote33" runat="server" Height="22px" Width="42px" Text="PAM" Tooltip="PAM : Point à améliorer"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote34" runat="server" Height="22px" Width="42px" Text="PNS" Tooltip="PNS : Point non satisfaisant"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote35" runat="server" Height="22px" Width="42px" Text="SO" Tooltip="SO : Sans objet"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>          
                                        </asp:TableCell>   
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVS01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="21" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHS07" runat="server" V_Groupe="CritereAgent19"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVT01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="22" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHT07" runat="server" V_Groupe="CritereAgent20"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVU01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="23" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHU07" runat="server" V_Groupe="CritereAgent21"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVV01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="24" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHV07" runat="server" V_Groupe="CritereAgent22"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVW01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="25" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHW07" runat="server" V_Groupe="CritereAgent23"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVX01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="26" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHX07" runat="server" V_Groupe="CritereAgent24"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences4" runat="server" CellPadding="0" CellSpacing="0" Width="746px">
                            <asp:TableRow>
                              <asp:TableCell>
                                   <asp:Label ID="LabelCadrage4" runat="server" Height="21px" Width="521px" Text=""
                                       CssClass="EP_EtiComplement">
                                   </asp:Label>  
                              </asp:TableCell>
                              <asp:TableCell>
                                   <asp:Label ID="LabelTitreNiveau4" runat="server" Height="18px" Width="210px" Text="APPRECIATION"
                                       CssClass="EP_TitreParagraphe"
                                       Font-Size="90%" style="margin-left: 3px; margin-top: 2px; text-align: center; padding-top: 3px; font-weight:bold;">
                                   </asp:Label>  
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence4" runat="server" Height="27px" Width="519px" Text="COMPETENCES PROFESSIONNELLES"
                                       CssClass="EP_TitreOnglet" style="margin-top: 1px;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels4" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote41" runat="server" Height="22px" Width="42px" Text="PF" Tooltip="PF : Point fort"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote42" runat="server" Height="22px" Width="42px" Text="PM" Tooltip="PM : Point maîtrisé"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote43" runat="server" Height="22px" Width="42px" Text="PAM" Tooltip="PAM : Point à améliorer"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote44" runat="server" Height="22px" Width="42px" Text="PNS" Tooltip="PNS : Point non satisfaisant"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote45" runat="server" Height="22px" Width="42px" Text="SO" Tooltip="SO : Sans objet"
                                               CssClass="EP_TitreParagraphe"
                                               Font-Size="90%"
                                               style="margin-top: 1px; padding-top: 5px; text-align: center; font-weight:bold;">
                                           </asp:Label>          
                                        </asp:TableCell>   
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVY01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="27" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHY07" runat="server" V_Groupe="CritereAgent25"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVZ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="28" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioHZ07" runat="server" V_Groupe="CritereAgent26"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV001" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="29" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioH007" runat="server" V_Groupe="CritereAgent27"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV101" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="30" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioH107" runat="server" V_Groupe="CritereAgent28"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV201" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="31" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioH207" runat="server" V_Groupe="CritereAgent29"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV301" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="32" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioH307" runat="server" V_Groupe="CritereAgent30"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV401" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="33" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioH407" runat="server" V_Groupe="CritereAgent31"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV501" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="34" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioH507" runat="server" V_Groupe="CritereAgent32"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV601" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="35" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioH607" runat="server" V_Groupe="CritereAgent33"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV701" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="36" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioH707" runat="server" V_Groupe="CritereAgent34"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV801" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="153" V_Information="1" V_SiDonneeDico="true"
                                   DonWidth="519px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="37" 
                                   EtiVisible="false" V_SiEnLectureSeule="true"
                                   DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center" VerticalAlign="Top">        
                                <Virtualia:VSixBoutonRadio ID="RadioH807" runat="server" V_Groupe="CritereAgent35"
                                   V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="true"
                                   VRadioN1Width="36px" VRadioN2Width="36px" VRadioN3Width="36px"
                                   VRadioN4Width="36px" VRadioN5Width="36px" VRadioN6Width="36px"
                                   VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                   VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                   VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                   VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                   VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                   VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                   VRadioN6Visible="false"
                                   Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           DonWidth="735px" DonHeight="180px" DonTabIndex="38"
                           EtiVisible="true" EtiWidth="735px" EtiHeight="20px"  
                           EtiText="Commentaires du supérieur hiérarchique direct"
                           Etistyle="margin-left: 5px; text-align: left; text-indent: 2px;"
                           Donstyle="margin-left: 5px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>       
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>