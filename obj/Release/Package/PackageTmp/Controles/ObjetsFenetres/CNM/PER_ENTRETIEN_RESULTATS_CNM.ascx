﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_RESULTATS_CNM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_RESULTATS_CNM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderColor="#B0E0D7" 
    BorderStyle="None" BorderWidth="2px" HorizontalAlign="Center" 
    style="margin-top: 1px;" Visible="true" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreBilanAgent" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiquetteBilanAgent" runat="server" Text="COMPTE-RENDU ENTRETIEN ANNUEL D'EVALUATION" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletBilanAgent" runat="server" Text="BILAN DE L'ANNEE ECOULEE EXPRIME PAR L'AGENT" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteBilanAgent" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="30px" Width="748px"
                           Text="Bilan de l'exercice des missions dans le poste au cours de l'année écoulée"
                           CssClass="EP_TitreChapitre"
                           style=" margin-bottom: 3px; padding-top: 7px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="30px" DonTabIndex="1" 
                           Etitext="Effectué par l'évalué - Ex : Actions réalisées ; faits marquants ; difficultés rencontrées"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 4px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="30px" Width="748px"
                           Text="Points forts"
                           CssClass="EP_TitreChapitre"
                           style=" margin-bottom: 3px; padding-top: 7px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="150px" EtiHeight="30px" DonTabIndex="2" 
                           Etitext="Eléments portés ou exprimés par l'évalué"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 4px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero13" runat="server" Height="30px" Width="748px"
                           Text="Points à améliorer"
                           CssClass="EP_TitreChapitre"
                           style=" margin-bottom: 3px; padding-top: 7px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="150px" EtiHeight="30px" DonTabIndex="3" 
                           Etitext="Eléments portés ou exprimés par l'évalué"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 4px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="12px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreBilanEvaluateur" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiquetteBilanEvaluateur" runat="server" Text="COMPTE-RENDU ENTRETIEN ANNUEL D'EVALUATION" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletBilanEvaluateur" runat="server" Text="BILAN DE L'ANNEE ECOULEE - OBJECTIFS EFFECTUES PAR L'EVALUATEUR" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteBilanEvaluateur" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="LabelTitreNumero21" runat="server" Height="30px" Width="748px"
                           Text="Rappel des objectifs de service"
                           CssClass="EP_TitreChapitre"
                           style=" margin-bottom: 3px; padding-top: 7px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           DonWidth="740px" DonHeight="150px" DonTabIndex="4" 
                           EtiVisible="false"
                           Donstyle="margin-left: 3px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="LabelTitreNumero22" runat="server" Height="30px" Width="748px"
                           Text="Rappel des objectifs de la période écoulée - Résultats obtenus"
                           CssClass="EP_TitreChapitre"
                           style=" margin-bottom: 3px; padding-top: 7px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="LabelTitreNumero23" runat="server" Height="20px" Width="748px"
                           Text="Résultats individuels et contribution aux objectifs du service"
                           CssClass="EP_EtiComplement" style="text-align:center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectif" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteObjectif1" runat="server" Height="60px" Width="241px"
                                   Text="Objectifs"
                                   CssClass="EP_EtiTableau">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteRealisation" runat="server" CellPadding="0" CellSpacing="0" Width="169px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                <asp:Label ID="LabelEnteteObjectif" runat="server" Height="29px" Width="167px"
                                   Text="Résultats"
                                   Tooltip="Au choix l'objectif sera défini comme étant atteint, partiellement atteint, ou non atteint"
                                   CssClass="EP_EtiTableau">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteNonAtteint" runat="server" Height="30px" Width="49px"
                                   Text="Non atteint"
                                   CssClass="EP_EtiTableau"
                                   BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEntetePartielle" runat="server" Height="30px" Width="66px"
                                   Text="partiellement atteint"
                                   CssClass="EP_EtiTableau"
                                   BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteAtteint" runat="server" Height="30px" Width="49px"
                                   Text="Atteint"
                                   CssClass="EP_EtiTableau"
                                   BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" Font-Size="X-Small"
                                   style="margin-left: -1px; margin-bottom: -1px">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObservations" runat="server" CellPadding="0" CellSpacing="0" Width="298px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteObservations" runat="server" Height="60px" Width="288px"
                                   Text="Observations"
                                   CssClass="EP_EtiTableau">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif1" runat="server" Height="20px" Width="281px"
                                   Text="Objectif n°1"
                                   CssClass="EP_EtiComplement"
                                   Font-Size="Medium" style="margin-top: 3px; font-style: normal; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="273px" DonHeight="200px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif" Etiwidth="275px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="169px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHE04" runat="server" V_Groupe="Realisation1"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHE05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="154px" DonTabIndex="6" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 0px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes1" runat="server" CellPadding="0" CellSpacing="0" Width="298px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="true"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Observations de l'agent" Etiwidth="292px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="8"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Commentaires" Etiwidth="292px" EtiHeight="18px" DonTooltip="Justification par des faits précis"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" Width="746px" BackColor="#98D4CA" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif2" runat="server" Height="20px" Width="281px"
                                   Text="Objectif n°2"
                                   CssClass="EP_EtiComplement"
                                   Font-Size="Medium" style="margin-top: 3px; font-style: normal; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="273px" DonHeight="200px" DonTabIndex="9"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif" Etiwidth="275px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="169px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHF04" runat="server" V_Groupe="Realisation2"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHF05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="154px" DonTabIndex="10" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 0px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes2" runat="server" CellPadding="0" CellSpacing="0" Width="298px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="true"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="11"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Observations de l'agent" Etiwidth="292px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="12"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonTooltip="Justification par des faits précis"
                                   EtiVisible="True" EtiText="Commentaires" Etiwidth="292px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" Width="746px" BackColor="#98D4CA" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif3" runat="server" Height="20px" Width="281px"
                                   Text="Objectif n°3"
                                   CssClass="EP_EtiComplement"
                                   Font-Size="Medium" style="margin-top: 3px; font-style: normal; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="273px" DonHeight="200px" DonTabIndex="13"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif" Etiwidth="275px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="169px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHG04" runat="server" V_Groupe="Realisation3"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHG05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="154px" DonTabIndex="14" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 0px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes3" runat="server" CellPadding="0" CellSpacing="0" Width="298px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="true"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="15"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Observations de l'agent" Etiwidth="292px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="16"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonTooltip="Justification par des faits précis"
                                   EtiVisible="True" EtiText="Commentaires" Etiwidth="292px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" Width="746px" BackColor="#98D4CA" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif4" runat="server" Height="20px" Width="281px"
                                   Text="Objectif n°4"
                                   CssClass="EP_EtiComplement"
                                   Font-Size="Medium" style="margin-top: 3px; font-style: normal; text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="281px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="273px" DonHeight="200px" DonTabIndex="17"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Objectif" Etiwidth="275px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="169px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHH04" runat="server" V_Groupe="Realisation4"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHH05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="154px" DonTabIndex="18" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 0px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes4" runat="server" CellPadding="0" CellSpacing="0" Width="298px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="true"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="19"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"
                                   EtiVisible="True" EtiText="Observationss de l'agent" Etiwidth="292px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="true"
                                   DonWidth="290px" DonHeight="120px" DonTabIndex="20"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px" DonTooltip="Justification par des faits précis"
                                   EtiVisible="True" EtiText="Commentaires" Etiwidth="292px" EtiHeight="18px"
                                   Etistyle="margin-left: 0px; Font-size: 11px; text-indent:2px;" EtiBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" Width="746px" BackColor="#98D4CA" BorderStyle="None" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
</asp:Table>