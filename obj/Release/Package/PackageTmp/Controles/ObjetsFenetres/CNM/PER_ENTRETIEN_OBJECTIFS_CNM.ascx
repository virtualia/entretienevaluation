﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_OBJECTIFS_CNM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_OBJECTIFS_CNM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreObjectifs" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU ENTRETIEN ANNUEL D'EVALUATION" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="VOS OBJECTIFS" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px" Text=""
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="30px" Width="748px"
                           Text="Rappel des objectifs de service"
                           CssClass="EP_TitreChapitre"
                           style="margin-bottom: 2px; padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVR03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="744px" DonHeight="150px" DonTabIndex="1" 
                           EtiVisible="false"
                           Donstyle="margin-left: 1px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="33px" Width="748px"
                           Text="Objectifs de la période à venir"
                           CssClass="EP_TitreChapitre"
                           style="padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectifs" runat="server" CellPadding="0" CellSpacing="0" Width="383px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectifTitre" runat="server" Height="25px" Width="373px"
                                   Text="Objectifs de l'année à venir"
                                   CssClass="EP_EtiTableau" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteIndicateurs" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteIndicateurs" runat="server" Height="25px" Width="230px"
                                   Text="Indicateurs"
                                   CssClass="EP_EtiTableau" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteEcheance" runat="server" CellPadding="0" CellSpacing="0" Width="127px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteEcheance" runat="server" Height="25px" Width="117px"
                                   Text="Echéance"
                                   CssClass="EP_EtiTableau" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3" Width="746px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="LabeNumeroObjectif1" runat="server" Height="17px" Width="200px" Text="Objectif 1"
                            CssClass="EP_EtiTableau" style="text-align: left; text-indent:2px; font-weight:bold; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="383px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="376px" DonHeight="100px" DonTabIndex="2"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableIndicateurs1" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="100px" DonTabIndex="3"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEcheance1" runat="server" CellPadding="0" CellSpacing="0" Width="127px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="120px" DonHeight="100px" DonTabIndex="4"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3" Width="746px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="LabeNumeroObjectif2" runat="server" Height="17px" Width="200px" Text="Objectif 2"
                            CssClass="EP_EtiTableau" style="text-align: left; text-indent:2px; font-weight:bold; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="383px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="376px" DonHeight="100px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableIndicateurs2" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="100px" DonTabIndex="6"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEcheance2" runat="server" CellPadding="0" CellSpacing="0" Width="127px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="120px" DonHeight="100px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3" Width="746px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="LabeNumeroObjectif3" runat="server" Height="17px" Width="200px" Text="Objectif 3"
                            CssClass="EP_EtiTableau" style="text-align: left; text-indent:2px; font-weight:bold; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="383px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="376px" DonHeight="100px" DonTabIndex="8"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableIndicateurs3" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="100px" DonTabIndex="9"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEcheance3" runat="server" CellPadding="0" CellSpacing="0" Width="127px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="120px" DonHeight="100px" DonTabIndex="10"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3" Width="746px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="LabeNumeroObjectif4" runat="server" Height="17px" Width="200px" Text="Objectif 4"
                            CssClass="EP_EtiTableau" style="text-align: left; text-indent:2px; font-weight:bold; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="383px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="376px" DonHeight="100px" DonTabIndex="11"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableIndicateurs4" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="100px" DonTabIndex="12"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEcheance4" runat="server" CellPadding="0" CellSpacing="0" Width="127px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="120px" DonHeight="100px" DonTabIndex="13"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero31" runat="server" Height="33px" Width="748px"
                           Text="Modalités permettant la réalisation des objectifs"
                           CssClass="EP_TitreChapitre"
                           style="padding-top: 7px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteModalitesObjectifs" runat="server" CellPadding="0" CellSpacing="0" Width="748px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTitreModaliteObjectif" runat="server" Height="25px" Width="748px"
                                   Text="à définir entre l'évaluateur et l'évalué"
                                   CssClass="EP_EtiComplement" style="text-align: center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Width="745px" 
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="LabeNumObjectif1" runat="server" Height="17px" Width="745px" Text="Objectif 1"
                            CssClass="EP_EtiTableau" style="text-align: left; text-indent:2px; font-weight:bold; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableModaliteObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="746px" DonHeight="50px" DonTabIndex="14"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Width="745px" 
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="LabeNumObjectif2" runat="server" Height="17px" Width="745px" Text="Objectif 2"
                            CssClass="EP_EtiTableau" style="text-align: left; text-indent:2px; font-weight:bold; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableModaliteObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="746px" DonHeight="50px" DonTabIndex="15"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Width="745px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="LabeNumObjectif3" runat="server" Height="17px" Width="745px" Text="Objectif 3"
                            CssClass="EP_EtiTableau" style="text-align: left; text-indent:2px; font-weight:bold; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableModaliteObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="746px" DonHeight="50px" DonTabIndex="16"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Width="745px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="LabeNumObjectif4" runat="server" Height="17px" Width="745px" Text="Objectif 4"
                            CssClass="EP_EtiTableau" style="text-align: left; text-indent:2px; font-weight:bold; margin-left:3px;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableModaliteObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="746px" DonHeight="50px" DonTabIndex="17"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>