﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_OBJECTIFS_CNED.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_OBJECTIFS_CNED" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreObjectifs" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="5. OBJECTIFS DE LA PERIODE A VENIR" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelObjectifsAgent" runat="server" Height="15px" Width="743px"
                           BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                           Text="Objectifs de la période à venir"
                           BorderWidth="2px" ForeColor="#0F2F30"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelObjectifsAgentSuite" runat="server" Height="15px" Width="743px"
                           BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                           Text="(fixés par le supérieur hiérarchique direct)"
                           BorderWidth="2px" ForeColor="#0F2F30"  
                           Font-Names="Trebuchet MS" Font-Size= "80%" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="2">
                        <asp:Label ID="LabelNumerobjectif1" runat="server" Height="25px" Width="750px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°1"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectifs1" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif1" runat="server" Height="20px" Width="498px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif d'activités attendu"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteDelais1" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteDelai1" runat="server" Height="20px" Width="230px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Echéance"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="501px" DonHeight="80px" DonTabIndex="1"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDelais1" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="80px" DonTabIndex="2"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteDemarche1" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteDemarche1" runat="server" Height="20px" Width="498px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Démarche envisagée et moyens à prévoir pour faciliter l'atteinte de l'objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteResultat1" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteResultat1" runat="server" Height="20px" Width="230px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Résultat attendu (ou indicateur)"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDemarche1" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="501px" DonHeight="80px" DonTabIndex="3"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableResultat1" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="80px" DonTabIndex="4"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="22px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="2">
                        <asp:Label ID="LabelNumerobjectif2" runat="server" Height="25px" Width="750px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°2"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectifs2" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif2" runat="server" Height="20px" Width="498px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif d'activités attendu"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteDelais2" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteDelai2" runat="server" Height="20px" Width="230px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Echéance"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="501px" DonHeight="80px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDelais2" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="80px" DonTabIndex="6"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteDemarche2" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteDemarche2" runat="server" Height="20px" Width="498px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Démarche envisagée et moyens à prévoir pour faciliter l'atteinte de l'objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteResultat2" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteResultat2" runat="server" Height="20px" Width="230px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Résultat attendu (ou indicateur)"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDemarche2" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="501px" DonHeight="80px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableResultat2" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="80px" DonTabIndex="8"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="22px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="2">
                        <asp:Label ID="LabelNumerobjectif3" runat="server" Height="25px" Width="750px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°3"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectifs3" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif3" runat="server" Height="20px" Width="498px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif d'activités attendu"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteDelais3" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteDelai3" runat="server" Height="20px" Width="230px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Echéance"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="501px" DonHeight="80px" DonTabIndex="9"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDelais3" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="80px" DonTabIndex="10"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteDemarche3" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteDemarche3" runat="server" Height="20px" Width="498px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Démarche envisagée et moyens à prévoir pour faciliter l'atteinte de l'objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteResultat3" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteResultat3" runat="server" Height="20px" Width="230px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Résultat attendu (ou indicateur)"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDemarche3" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="501px" DonHeight="80px" DonTabIndex="11"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableResultat3" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="80px" DonTabIndex="12"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="22px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="2">
                        <asp:Label ID="LabelNumerobjectif4" runat="server" Height="25px" Width="750px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°4"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectifs4" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif4" runat="server" Height="20px" Width="498px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif d'activités attendu"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteDelais4" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteDelai4" runat="server" Height="20px" Width="230px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Echéance"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="501px" DonHeight="80px" DonTabIndex="13"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDelais4" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="80px" DonTabIndex="14"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteDemarche4" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteDemarche4" runat="server" Height="20px" Width="498px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Démarche envisagée et moyens à prévoir pour faciliter l'atteinte de l'objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteResultat4" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteResultat4" runat="server" Height="20px" Width="230px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Résultat attendu (ou indicateur)"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDemarche4" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="501px" DonHeight="80px" DonTabIndex="15"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableResultat4" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="80px" DonTabIndex="16"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="22px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="2">
                        <asp:Label ID="LabelNumerobjectif5" runat="server" Height="25px" Width="750px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°5"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectifs5" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif5" runat="server" Height="20px" Width="498px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif d'activités attendu"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteDelais5" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteDelai5" runat="server" Height="20px" Width="230px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Echéance"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="501px" DonHeight="80px" DonTabIndex="17"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDelais5" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="80px" DonTabIndex="18"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteDemarche5" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteDemarche5" runat="server" Height="20px" Width="498px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Démarche envisagée et moyens à prévoir pour faciliter l'atteinte de l'objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteResultat5" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteResultat5" runat="server" Height="20px" Width="230px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Résultat attendu (ou indicateur)"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDemarche5" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="501px" DonHeight="80px" DonTabIndex="19"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableResultat5" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="80px" DonTabIndex="20"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="22px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="2">
                        <asp:Label ID="LabelNumerobjectif6" runat="server" Height="25px" Width="750px"
                            BackColor="#9EB0AC" BorderStyle="None" 
                            Text="Objectif n°6"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteObjectifs6" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteObjectif6" runat="server" Height="20px" Width="498px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Objectif d'activités attendu"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteDelais6" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteDelai6" runat="server" Height="20px" Width="230px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Echéance"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableObjectif6" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="501px" DonHeight="80px" DonTabIndex="21"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDelais6" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="80px" DonTabIndex="22"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteDemarche6" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteDemarche6" runat="server" Height="20px" Width="498px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Démarche envisagée et moyens à prévoir pour faciliter l'atteinte de l'objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableEnteteResultat6" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="LabelEnteteResultat6" runat="server" Height="20px" Width="230px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Résultat attendu (ou indicateur)"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableDemarche6" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="4" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="501px" DonHeight="80px" DonTabIndex="23"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="TableResultat6" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG05" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="233px" DonHeight="80px" DonTabIndex="24"
                                   Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>
