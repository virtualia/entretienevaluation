﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_FORMATION_AFB.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_FORMATION_AFB" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VTrioVerticalRadio.ascx" TagName="VTrioVerticalRadio" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VListeGrid.ascx" TagName="VListeGrid" TagPrefix="Virtualia" %>


<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreFormation" runat="server" Height="95px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN DE FORMATION" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="ANNEE" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

   <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreEntete" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" >
                        <asp:Label ID="LabelRappel" runat="server" Height="15px" Width="746px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="RAPPEL:"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" >
                        <asp:Label ID="RappelL1" runat="server" Height="35px" Width="746px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="Toute demande de formation doit faire l’objet d’une inscription sur le portail de formation dédié (onglet «public interne») via lequel l’agent peut :"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" >
                        <asp:Label ID="RappelL2" runat="server" Height="15px" Width="746px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text=" • s’inscrire à une formation collective proposée par l’AFB"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" >
                        <asp:Label ID="RappelL3" runat="server" Height="15px" Width="746px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text=" • formuler une demande de formation individuelle"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" Width="746px" BackColor="#E9FDF9"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left"  Width="746px" BackColor="#E9FDF9">
                       <asp:Label ID="LabelPortail" runat="server" Height="15px" 
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="<B> Portail : </B>"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;">
                        </asp:Label>
                        <asp:HyperLink ID="LienPortail" runat="server" Height="15px" 
                           BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" 
                            BorderWidth="1px" ForeColor="blue" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                        </asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" Width="746px" BackColor="#E9FDF9"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="AnoterL1" runat="server" Height="15px" Width="746px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="À noter :"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="746px" BackColor="#E9FDF9">
                        <asp:Label ID="AnoterL2" runat="server" Height="15px" 
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text=" • Pour tout renseignement concernant la formation :"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;">
                        </asp:Label>
                        <asp:HyperLink ID="MailFormation" runat="server" Height="15px" 
                           BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" 
                            BorderWidth="1px" ForeColor="blue" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                        </asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="746px" BackColor="#E9FDF9">
                        <asp:Label ID="AnoterL3" runat="server" Height="20px" 
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text=" • Pour toutes questions ou difficultés techniques sur le portail des formations :"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;">
                        </asp:Label>
                        <asp:HyperLink ID="MailFormationTech" runat="server" Height="20px" 
                           BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" 
                            BorderWidth="1px" ForeColor="blue" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                        </asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

<%--    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreEntete" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreIntitulePoste" runat="server" Height="24px" Width="285px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="Intitulé du poste occupé :"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIntitulePoste" runat="server" Height="24px" Width="463px"
                            BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreDatePriseFonctions" runat="server" Height="24px" Width="285px"
                            BackColor="#E9FDF9" BorderColor="Transparent" BorderStyle="NotSet" Text="Date de prise de fonction du poste actuel :"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDatePriseFonctions" runat="server" Height="24px" Width="463px"
                            BackColor="#E2FDF7" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>--%>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
<%--                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                            V_PointdeVue="1" V_Objet="150" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                            EtiWidth="330px" DonWidth="320px" EtiHeight="20px" DonTabIndex="1"
                            EtiText="Supérieur hiérarchique ayant conduit l'entretien" />
                    </asp:TableCell>
                </asp:TableRow>--%>
                <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="45px" Width="748px"
                            BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="BILAN DE L'ANNEE"
                            BorderWidth="1px" ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="45px" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="a) Actions de formation suivies au titre de la formation continue : thématique et durée"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 3px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFA03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="2"
                            EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFB03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="2"
                            EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFC03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="2"
                            EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFD03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="2"
                            EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFE03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="2"
                            EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFF03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="2"
                            EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFG03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="2"
                            EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFH03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="2"
                            EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero14" runat="server" Height="45px" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="b)  Actions de formation suivies au titre de la PEC (préparation aux examens concours) : thématique et durée"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 3px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFI03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="3"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero15" runat="server" Height="45px" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="c) Autres actions suivies (VAE-validation des acquis de l'expérience, CEP-congé de formation professionnelle, bilan de carrière, etc.) : thématiques et durée"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 3px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFJ03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="4"
                            EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Label1" runat="server" Height="45px" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="d) Actions de formation conduites en tant que formateur interne : thématique et durée "
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 3px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFK03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="5"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
<%--                <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="CadreNombreHeures" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="left">
                                    <asp:Label ID="Label3" runat="server" Height="45px" Width="680px"
                                        BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Nombre d'heures de DIF (droit individuel à la formation) mobilisées au cours de l'année"
                                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 3px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHI11" runat="server"
                                        V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="true"
                                        DonWidth="58px" DonHeight="45px" DonTabIndex="6"
                                        EtiVisible="false" DonStyle="margin-left: 0px;"
                                        DonBorderWidth="1px" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>--%>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="25px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="45px" Width="748px"
                            BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="PERSPECTIVES DE L'ANNEE A VENIR"
                            BorderWidth="1px" ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero21" runat="server" Height="45px" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="a) Actions de formation continue sollicitées au regard des Compétences attendues sur le poste"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 3px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT11" runat="server" Height="17px" Width="48px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(*)"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 0px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT12" runat="server" Height="17px" Width="700px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="action de formation, inscription dans un parcours de professionnalisation thématique, tutorat, compagnonnage, etc"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 0px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeDIF11" runat="server" Height="17px" Width="48px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(**)"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 0px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeDIF12" runat="server" Height="17px" Width="700px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="la compétence individuelle doit être appréciee dans toutes ses composantes : savoir être, savoir faire, connaissance métiers"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 0px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT51" runat="server" Height="17px" Width="48px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 0px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelLegendeT52" runat="server" Height="17px" Width="700px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="cette compétence individuelle s'apprécie au regard de compétences collectives présentes dans le service"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 0px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left">
                        <asp:Label ID="Label2" runat="server" Height="50px" Width="347px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Actions de formation continue*"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="right">
                        <asp:Label ID="Label4" runat="server" Height="50px" Width="400px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Compétences** individuelles concernées&nbsp"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            Style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="340px" DonHeight="100px" EtiHeight="20px" DonTabIndex="7"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="395px" DonHeight="100px" EtiHeight="20px" DonTabIndex="8"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left">
                           <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="340px" DonHeight="100px" EtiHeight="20px" DonTabIndex="9"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="395px" DonHeight="100px" EtiHeight="20px" DonTabIndex="10"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left">
                           <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="340px" DonHeight="100px" EtiHeight="20px" DonTabIndex="11"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="395px" DonHeight="100px" EtiHeight="20px" DonTabIndex="12"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left">
                           <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="340px" DonHeight="100px" EtiHeight="20px" DonTabIndex="13"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="395px" DonHeight="100px" EtiHeight="20px" DonTabIndex="14"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left">
                           <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="340px" DonHeight="100px" EtiHeight="20px" DonTabIndex="15"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="395px" DonHeight="100px" EtiHeight="20px" DonTabIndex="16"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left">
                           <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="340px" DonHeight="100px" EtiHeight="20px" DonTabIndex="17"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="395px" DonHeight="100px" EtiHeight="20px" DonTabIndex="18"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>


            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>

                    <asp:TableCell HorizontalAlign="left">
                        <asp:Label ID="Label5" runat="server" Height="50px" Width="347px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Concours / examens visés"
                            ForeColor="#0E5F5C" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="right">
                        <asp:Label ID="Label6" runat="server" Height="50px" Width="400px"
                            BackColor="#D3F0EA" BorderStyle="Notset" BorderColor="#8DA8A3" BorderWidth="1px"
                            Text="Actions de formation PEC (préparation aux examens et concours)"
                            ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            Style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px; font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left">
                           <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="340px" DonHeight="100px" EtiHeight="20px" DonTabIndex="19"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="395px" DonHeight="100px" EtiHeight="20px" DonTabIndex="20"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="left">
                           <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH01" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="1" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="340px" DonHeight="100px" EtiHeight="20px" DonTabIndex="21"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="395px" DonHeight="100px" EtiHeight="20px" DonTabIndex="22"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelObservationsEvaluateur" runat="server" Height="45px" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Autres actions sollicitées <small> (validation des acquis de l'expérience, bilan de compétences, période de professionnalisation, etc...) Préciser le(s) motif(s) </small>"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 3px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFL03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="23"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFM03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="23"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFN03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="23"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFO03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="23"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFP03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="23"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFQ03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="23"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFR03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="23"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVFS03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="746px" DonWidth="746px" DonHeight="30px" EtiHeight="0px" DonTabIndex="23"
                            EtiText="" EtiVisible="false"
                            DonStyle="margin-left: 0px;"
                            EtiStyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                            DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
