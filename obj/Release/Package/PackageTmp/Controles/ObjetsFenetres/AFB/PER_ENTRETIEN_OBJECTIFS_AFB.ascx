﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_OBJECTIFS_AFB.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_OBJECTIFS_AFB" %>

<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreObjectifs" runat="server" Height="95px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="DEFINITION DES OBJECTIFS DE L'ANNEE A VENIR" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelContexte" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="CONTEXTE PRÉVISIBLE DE L'ANNÉE"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteContexte" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteContexte" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="(Politique, environnement, réorganisation, moyens, objectifs du service dans lesquels s'inscrivent ceux de l'agent...)"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="1"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectif1" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="Objectif n°1"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifs1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteObjectif1" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="2"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateur1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteIndicateur1" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Résultats attendus et critères d’atteinte des objectifs"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="3"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectif2" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="Objectif n°2"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifs2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteObjectif2" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="4"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateur2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteIndicateur2" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Indicateurs de réussite prévus"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="5"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectif3" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="Objectif n°3"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifs3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteObjectif3" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="6"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateur3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteIndicateur3" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Indicateurs de réussite prévus"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="7"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectif4" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="Objectif n°4"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifs4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteObjectif4" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="8"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateur4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteIndicateur4" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Indicateurs de réussite prévus"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="9"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectif5" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="Objectif n°5"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifs5" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteObjectif5" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="10"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateur5" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteIndicateur5" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Indicateurs de réussite prévus"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="11"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectif6" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="Objectif n°6"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifs6" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteObjectif6" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="12"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateur6" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteIndicateur6" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Indicateurs de réussite prévus"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="13"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectif7" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="Objectif n°7"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifs7" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteObjectif7" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="14"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateur7" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteIndicateur7" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Indicateurs de réussite prévus"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="15"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletObjectifManageriaux" runat="server" Text="Objectifs manageriaux pour les agents en situation d’encadrement" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectifManagerial1" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="Objectif managérial n°1"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifManagerial1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteObjectifManagerial1" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="16"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateurManagerial1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label1" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Indicateurs de réussite prévus"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="17"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectifManagerial2" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="Objectif managérial n°2"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifManagerial2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteObjectifManagerial2" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="18"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateurManagerial2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label2" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Indicateurs de réussite prévus"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="19"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectifManagerial3" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="Objectif managérial n°3"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifManagerial3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteObjectifManagerial3" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="20"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateurManagerial3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label3" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Indicateurs de réussite prévus"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="21"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelNumerobjectifManagerial4" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="Objectif managérial n°4"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectifManagerial4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteObjectifManagerial4" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Objectif"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL02" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="22"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteIndicateurManagerial4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Label4" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="Indicateurs de réussite prévus"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="110px" DonTabIndex="23"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="30px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Label ID="LabelObservations" runat="server" Height="35px" Width="748px"
                            BackColor="#9EB0AC" BorderStyle="None"
                            Text="OBSERVATIONS ET COMMENTAIRES"
                            ForeColor="White" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 2px; margin-left: 0px; margin-bottom: 1px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObservations" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelEnteteObservations" runat="server" Height="20px" Width="746px"
                                        BackColor="#D7FAF3" BorderStyle="None"
                                        Text="(conditions de réussite: moyens, délais...)"
                                        ForeColor="#124545" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVE03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="744px" DonHeight="80px" DonTabIndex="24"
                            DonStyle="margin-top: 0px; margin-left: 0px;" DonBorderWidth="1px" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
