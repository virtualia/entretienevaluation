﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_COMPORTEMENT_HONDA.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_COMPORTEMENT_HONDA" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioEtiTextCommande.ascx" tagname="VTrioEtiTextCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 0px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreEnteteComportement" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#2FA49B">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPORTEMENT DU COLLABORATEUR - 90 KI" Height="30px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre1" runat="server" Text="" Height="25px" Width="75px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#0E5F5C"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre2" runat="server" Text="Nom" Height="25px" Width="200px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre3" runat="server" Text="Département" Height="25px" Width="235px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre4" runat="server" Text="Fonction" Height="25px" Width="230px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletCollaborateur" runat="server" Text="Collaborateur" Height="30px" Width="75px"
                            BackColor="Transparent"  BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 15px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteCollaborateur" runat="server" Text="" Height="35px" Width="200px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDepartementCollaborateur" runat="server" Text="" Height="35px" Width="235px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFonctionCollaborateur" runat="server" Text="" Height="35px" Width="230px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletEvaluateur" runat="server" Text="Evaluateur" Height="30px" Width="75px"
                            BackColor="Transparent"  BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 15px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteEvaluateur" runat="server" Text="" Height="35px" Width="200px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDepartementEvaluateur" runat="server" Text="" Height="35px" Width="235px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFonctionEvaluateur" runat="server" Text="" Height="35px" Width="230px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletPeriodeKI" runat="server" Text="Période KI" Height="19px" Width="75px"
                            BackColor="Transparent"  BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelPeriodeKI" runat="server" Text="" Height="19px" Width="200px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: center; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletRevision" runat="server" Text="Date de révision" Height="19px" Width="235px"
                            BackColor="Transparent"  BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: right; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDateRevision" runat="server" Text="" Height="19px" Width="230px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="4"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="8px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
         <asp:Label ID="LabelEvaluationComportement" runat="server" Height="30px" Width="748px"
            BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Evaluation des éléments comportementaux"
            BorderWidth="1px" ForeColor="White" Font-Italic="False"
            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
            font-style: normal; text-indent: 0px; text-align:  center;">
         </asp:Label>          
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="7px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
        <asp:Label ID="LabelCommentaireEvaluateur" runat="server" Height="25px" Width="747px"
            BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Commentaires de l'évaluateur sur les aspects du comportement"
            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
            style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; padding-top: 8px;
            font-style: normal; text-indent: 3px; text-align:  left;">
        </asp:Label>          
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV22" runat="server" DonTextMode="true"
            V_PointdeVue="1" V_Objet="150" V_Information="22" V_SiDonneeDico="true"
            EtiWidth="741px" DonWidth="745px" DonHeight="80px" DonTabIndex="1"
            DonTooltip="Observations concrètes concernant les aspects du comportement" 
            Etivisible="False"
            Donstyle="margin-left: 0px; margin-top: -2px"
            DonBorderWidth="1px"/>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="EnteteElementEvaluation" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
             <asp:TableCell>
                <asp:Label ID="TitreEnteteElementEvaluation" runat="server" Height="28px" Width="254px"
                    BackColor="#124545" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Eléments d'évaluation"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 5px; text-align:  left;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreEnteteVide" runat="server" Height="35px" Width="344px"
                    BackColor="Transparent" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreEnteteCollaborateur" runat="server" Height="28px" Width="73px"
                    BackColor="#E9FDF9" BorderStyle="None" Text="Collaborateur"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreEnteteEvaluateur" runat="server" Height="28px" Width="70px"
                    BackColor="#CAEBE4" BorderStyle="None" Text="Evaluateur"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreElementsEvaluation" runat="server" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneElementEvaluation1" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                                   DonWidth="250px" DonHeight="64px" DonTabIndex="2" DonTooltip="Elément d'évaluation" 
                                   EtiText="" Etivisible="false" EtiWidth="0px"
                                   DonText=""
                                   Donstyle="margin-left: 1px; Font-Bold: True; Font-Size: Medium; Text-indent: 3px" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="labelInfosElement01" runat="server" Height="64px" Width="347px"
                                   BackColor="#F9F9F9" BorderStyle="Notset" BorderColor="#8DA8A3" Text=""
                                   BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                   style="margin-top: 3px; margin-left: 1px; margin-bottom: 0px; padding-top: 2px;
                                   font-style: normal; text-indent: 2px; text-align: left;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCK04" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="4"
                                   Etivisible="false" Width="70px"
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="4" DonTooltip="Evaluation du collaborateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCK03" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="3"
                                   Etivisible="false" Width="70px" 
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="5" DonTooltip="Evaluation de l'évaluateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: True; Font-Size: Medium"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="White"/>
                            </asp:TableCell>
                        </asp:TableRow>
	                </asp:Table>
	              </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneElementEvaluation2" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                                   DonWidth="250px" DonHeight="64px" DonTabIndex="6" DonTooltip="Elément d'évaluation" 
                                   EtiText="" Etivisible="false" EtiWidth="0px"
                                   DonText=""
                                   Donstyle="margin-left: 1px; Font-Bold: True; Font-Size: Medium; Text-indent: 3px" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="labelInfosElement02" runat="server" Height="64px" Width="347px"
                                   BackColor="#F9F9F9" BorderStyle="Notset" BorderColor="#8DA8A3" Text=""
                                   BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                   style="margin-top: 3px; margin-left: 1px; margin-bottom: 0px; padding-top: 2px;
                                   font-style: normal; text-indent: 2px; text-align: left;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCL04" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="4"
                                   Etivisible="false" Width="70px"
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="8" DonTooltip="Evaluation du collaborateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCL03" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="3"
                                   Etivisible="false" Width="70px" 
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="9" DonTooltip="Evaluation de l'évaluateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: True; Font-Size: Medium"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="White"/>
                            </asp:TableCell>
                        </asp:TableRow>
	                </asp:Table>
	              </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneElementEvaluation3" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                                   DonWidth="250px" DonHeight="64px" DonTabIndex="10" DonTooltip="Elément d'évaluation" 
                                   EtiText="" Etivisible="false" EtiWidth="0px"
                                   DonText=""
                                   Donstyle="margin-left: 1px; Font-Bold: True; Font-Size: Medium; Text-indent: 3px" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="labelInfosElement03" runat="server" Height="64px" Width="347px"
                                   BackColor="#F9F9F9" BorderStyle="Notset" BorderColor="#8DA8A3" Text=""
                                   BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                   style="margin-top: 3px; margin-left: 1px; margin-bottom: 0px; padding-top: 2px;
                                   font-style: normal; text-indent: 2px; text-align: left;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCM04" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="4"
                                   Etivisible="false" Width="70px"
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="12" DonTooltip="Evaluation du collaborateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCM03" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="3"
                                   Etivisible="false" Width="70px" 
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="13" DonTooltip="Evaluation de l'évaluateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: True; Font-Size: Medium"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="White"/>
                            </asp:TableCell>
                        </asp:TableRow>
	                </asp:Table>
	              </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneElementEvaluation4" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                                   DonWidth="250px" DonHeight="64px" DonTabIndex="14" DonTooltip="Elément d'évaluation" 
                                   EtiText="" Etivisible="false" EtiWidth="0px"
                                   DonText=""
                                   Donstyle="margin-left: 1px; Font-Bold: True; Font-Size: Medium; Text-indent: 3px" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="labelInfosElement04" runat="server" Height="64px" Width="347px"
                                   BackColor="#F9F9F9" BorderStyle="Notset" BorderColor="#8DA8A3" Text=""
                                   BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                   style="margin-top: 3px; margin-left: 1px; margin-bottom: 0px; padding-top: 2px;
                                   font-style: normal; text-indent: 2px; text-align: left;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCN04" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="4"
                                   Etivisible="false" Width="70px"
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="16" DonTooltip="Evaluation du collaborateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCN03" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="3"
                                   Etivisible="false" Width="70px" 
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="17" DonTooltip="Evaluation de l'évaluateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: True; Font-Size: Medium"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="White"/>
                            </asp:TableCell>
                        </asp:TableRow>
	                </asp:Table>
	              </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneElementEvaluation5" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                                   DonWidth="250px" DonHeight="64px" DonTabIndex="18" DonTooltip="Elément d'évaluation" 
                                   EtiText="" Etivisible="false" EtiWidth="0px"
                                   DonText=""
                                   Donstyle="margin-left: 1px; Font-Bold: True; Font-Size: Medium; Text-indent: 3px" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="labelInfosElement05" runat="server" Height="64px" Width="347px"
                                   BackColor="#F9F9F9" BorderStyle="Notset" BorderColor="#8DA8A3" Text=""
                                   BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                   style="margin-top: 3px; margin-left: 1px; margin-bottom: 0px; padding-top: 2px;
                                   font-style: normal; text-indent: 2px; text-align: left;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCO04" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="4"
                                   Etivisible="false" Width="70px"
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="20" DonTooltip="Evaluation du collaborateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCO03" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="3"
                                   Etivisible="false" Width="70px" 
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="21" DonTooltip="Evaluation de l'évaluateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: True; Font-Size: Medium"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="White"/>
                            </asp:TableCell>
                        </asp:TableRow>
	                </asp:Table>
	              </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneElementEvaluation6" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                                   DonWidth="250px" DonHeight="64px" DonTabIndex="22" DonTooltip="Elément d'évaluation" 
                                   EtiText="" Etivisible="false" EtiWidth="0px"
                                   DonText=""
                                   Donstyle="margin-left: 1px; Font-Bold: True; Font-Size: Medium; Text-indent: 3px" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="labelInfosElement06" runat="server" Height="64px" Width="347px"
                                   BackColor="#F9F9F9" BorderStyle="Notset" BorderColor="#8DA8A3" Text=""
                                   BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                   style="margin-top: 4px; margin-left: 1px; margin-bottom: 0px; padding-top: 2px;
                                   font-style: normal; text-indent: 2px; text-align: left;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCP04" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="4"
                                   Etivisible="false" Width="70px"
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="24" DonTooltip="Evaluation du collaborateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCP03" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="3"
                                   Etivisible="false" Width="70px" 
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="25" DonTooltip="Evaluation de l'évaluateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: True; Font-Size: Medium"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="White"/>
                            </asp:TableCell>
                        </asp:TableRow>
	                </asp:Table>
	              </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneElementEvaluation7" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                                   DonWidth="250px" DonHeight="64px" DonTabIndex="26" DonTooltip="Elément d'évaluation" 
                                   EtiText="" Etivisible="false" EtiWidth="0px"
                                   DonText=""
                                   Donstyle="margin-left: 1px; Font-Bold: True; Font-Size: Medium; Text-indent: 3px" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="labelInfosElement07" runat="server" Height="64px" Width="347px"
                                   BackColor="#F9F9F9" BorderStyle="Notset" BorderColor="#8DA8A3" Text=""
                                   BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                   style="margin-top: 4px; margin-left: 1px; margin-bottom: 0px; padding-top: 2px;
                                   font-style: normal; text-indent: 2px; text-align: left;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCQ04" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="4"
                                   Etivisible="false" Width="70px"
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="28" DonTooltip="Evaluation du collaborateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCQ03" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="3"
                                   Etivisible="false" Width="70px" 
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="29" DonTooltip="Evaluation de l'évaluateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: True; Font-Size: Medium"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="White"/>
                            </asp:TableCell>
                        </asp:TableRow>
	                </asp:Table>
	              </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneElementEvaluation8" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVR01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                                   DonWidth="250px" DonHeight="64px" DonTabIndex="30" DonTooltip="Elément d'évaluation" 
                                   EtiText="" Etivisible="false" EtiWidth="0px"
                                   DonText=""
                                   Donstyle="margin-left: 1px; Font-Bold: True; Font-Size: Medium; Text-indent: 3px" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="labelInfosElement08" runat="server" Height="64px" Width="347px"
                                   BackColor="#F9F9F9" BorderStyle="Notset" BorderColor="#8DA8A3" Text=""
                                   BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                   style="margin-top: 4px; margin-left: 1px; margin-bottom: 0px; padding-top: 2px;
                                   font-style: normal; text-indent: 2px; text-align: left;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCR04" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="4"
                                   Etivisible="false" Width="70px"
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="32" DonTooltip="Evaluation du collaborateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCR03" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="3"
                                   Etivisible="false" Width="70px" 
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="33" DonTooltip="Evaluation de l'évaluateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: True; Font-Size: Medium"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="White"/>
                            </asp:TableCell>
                        </asp:TableRow>
	                </asp:Table>
	              </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneElementEvaluation9" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVS01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                                   DonWidth="250px" DonHeight="64px" DonTabIndex="34" DonTooltip="Elément d'évaluation" 
                                   EtiText="" Etivisible="false" EtiWidth="0px"
                                   DonText=""
                                   Donstyle="margin-left: 1px; Font-Bold: True; Font-Size: Medium; Text-indent: 3px" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="labelInfosElement09" runat="server" Height="64px" Width="347px"
                                   BackColor="#F9F9F9" BorderStyle="Notset" BorderColor="#8DA8A3" Text=""
                                   BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                   style="margin-top: 4px; margin-left: 1px; margin-bottom: 0px; padding-top: 2px;
                                   font-style: normal; text-indent: 2px; text-align: left;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCS04" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="4"
                                   Etivisible="false" Width="70px"
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="36" DonTooltip="Evaluation du collaborateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCS03" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="3"
                                   Etivisible="false" Width="70px" 
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="37" DonTooltip="Evaluation de l'évaluateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: True; Font-Size: Medium"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="White"/>
                            </asp:TableCell>
                        </asp:TableRow>
	                </asp:Table>
	              </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneElementEvaluation10" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVT01" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                                   DonWidth="250px" DonHeight="64px" DonTabIndex="38" DonTooltip="Elément d'évaluation" 
                                   EtiText="" Etivisible="false" EtiWidth="0px"
                                   DonText=""
                                   Donstyle="margin-left: 1px; Font-Bold: True; Font-Size: Medium; Text-indent: 3px" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="labelInfosElement10" runat="server" Height="64px" Width="347px"
                                   BackColor="#F9F9F9" BorderStyle="Notset" BorderColor="#8DA8A3" Text=""
                                   BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                   style="margin-top: 4px; margin-left: 1px; margin-bottom: 0px; padding-top: 2px;
                                   font-style: normal; text-indent: 2px; text-align: left;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCT04" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="4"
                                   Etivisible="false" Width="70px"
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="40" DonTooltip="Evaluation du collaborateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VTrioEtiTextCommande ID="InfoCT03" runat="server" 
                                   V_PointdeVue="1" V_Objet="152" V_Information="3"
                                   Etivisible="false" Width="70px" 
                                   DonWidth="70px" DonHeight="41px" DonTabIndex="41" DonTooltip="Evaluation de l'évaluateur - | 0 | 1 | 2 | 3 | 4 |"
                                   Donstyle="margin-left: 0px; text-align: center; Font-Bold: True; Font-Size: Medium"
					               V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="4" V_ValMin="0" V_Pas="1"
                                   DonBorderWidth="1px" DonBackColor="White"/>
                            </asp:TableCell>
                        </asp:TableRow>
	                </asp:Table>
	              </asp:TableCell>
                </asp:TableRow>
            </asp:Table>        
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="EnteteElementEvaluationBas" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
             <asp:TableCell>
                <asp:Label ID="TitreEnteteElementEvaluationBas" runat="server" Height="20px" Width="254px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 5px; text-align:  left;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreEnteteVideBas" runat="server" Height="20px" Width="344px"
                    BackColor="Transparent" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreEnteteCollaborateurBas" runat="server" Height="20px" Width="73px"
                    BackColor="#E9FDF9" BorderStyle="None" Text="Collaborateur"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreEnteteEvaluateurBas" runat="server" Height="20px" Width="70px"
                    BackColor="#CAEBE4" BorderStyle="None" Text="Evaluateur"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreEvaluationComportement" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
             <asp:TableCell>
                <asp:Label ID="TitreEvaluationComportement" runat="server" Height="25px" Width="596px"
                    BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text="2. Evaluation du comportement : Maximum 40 points"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="EvaluationComportementCollaborateur" runat="server" Height="25px" Width="70px"
                    BackColor="#E9FDF9" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="EvaluationComportementEvaluateur" runat="server" Height="25px" Width="70px"
                    BackColor="White" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreEvaluationGlobale" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
             <asp:TableCell>
                <asp:Label ID="TitreEvaluationGlobale" runat="server" Height="25px" Width="596px"
                    BackColor="#124545" BorderColor="#124545" BorderStyle="Notset" Text="3. Evaluation globale : Maximum 100 points"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="EvaluationGlobaleCollaborateur" runat="server" Height="25px" Width="70px"
                    BackColor="#124545" BorderColor="#124545" BorderStyle="Notset" Text=""
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="EvaluationGlobaleEvaluateur" runat="server" Height="25px" Width="70px"
                    BackColor="#124545" BorderColor="#124545" BorderStyle="Notset" Text=""
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
        <asp:Label ID="LabelResumeDeveloppement" runat="server" Height="25px" Width="747px"
            BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Résumé développement et formation (Evaluateur)"
            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
            style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; padding-top: 8px;
            font-style: normal; text-indent: 3px; text-align:  left;">
        </asp:Label>          
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV27" runat="server" DonTextMode="true"
            V_PointdeVue="1" V_Objet="150" V_Information="27" V_SiDonneeDico="true"
            EtiWidth="741px" DonWidth="745px" DonHeight="80px" DonTabIndex="42" DonTooltip=""
            Etivisible="False"
            Donstyle="margin-left: 0px; margin-top: -2px"
            DonBorderWidth="1px"/>
       </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreBesoinFormation" runat="server" CellPadding="0" Width="750px"> 
           <asp:TableRow>
	         <asp:TableCell HorizontalAlign="left">
                <asp:Label ID="LabelBesoinFormation" runat="server" Height="25px" Width="140px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="Besoins en formation"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align: right;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell HorizontalAlign="Left">
                <Virtualia:VTrioHorizontalRadio ID="RadioH15" runat="server" V_Groupe="Formation"
                    V_PointdeVue="1" V_Objet="150" V_Information="15" V_SiDonneeDico="true"
                    RadioGaucheWidth="70px" RadioCentreWidth="70px" RadioDroiteWidth="0px"
                    RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
			        RadioGaucheHeight="20px" RadioCentreHeight="20px"
                    RadioCentreStyle="text-align: left;" RadioGaucheStyle="text-align: left;"
			        Visible="true"/>          
		     </asp:TableCell>
             <asp:TableCell HorizontalAlign="left">
                <asp:Label ID="LabelBesoinFormationDroite" runat="server" Height="25px" Width="430px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
        </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
        <asp:Label ID="LabelCommentaireCollaborateur" runat="server" Height="25px" Width="747px"
            BackColor="#E9FDF9" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Commentaires du collaborateur"
            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
            style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; padding-top: 8px;
            font-style: normal; text-indent: 3px; text-align:  left;">
        </asp:Label>          
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV28" runat="server" DonTextMode="true"
            V_PointdeVue="1" V_Objet="150" V_Information="28" V_SiDonneeDico="true"
            EtiWidth="741px" DonWidth="745px" DonHeight="160px" DonTabIndex="43" DonTooltip=""
            Etivisible="False"
            Donstyle="margin-left: 0px; margin-top: -2px"
            DonBorderWidth="1px"/>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
        <asp:Label ID="LabelCommentairesEvaluateur" runat="server" Height="25px" Width="747px"
            BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Commentaires de l'évaluateur : MOTIVATIONS PRINCIPALES DE L'EVALUATION"
            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
            style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; padding-top: 8px;
            font-style: normal; text-indent: 3px; text-align:  left;">
        </asp:Label>          
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV29" runat="server" DonTextMode="true"
            V_PointdeVue="1" V_Objet="150" V_Information="29" V_SiDonneeDico="true"
            EtiWidth="741px" DonWidth="745px" DonHeight="160px" DonTabIndex="43" DonTooltip=""
            Etivisible="False"
            Donstyle="margin-left: 0px; margin-top: -2px"
            DonBorderWidth="1px"/>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="8px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
         <asp:Label ID="LabelEvaluationFinale" runat="server" Height="30px" Width="748px"
            BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Evaluation finale"
            BorderWidth="1px" ForeColor="White" Font-Italic="False"
            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
            font-style: normal; text-indent: 0px; text-align:  center;">
         </asp:Label>          
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreLegendeNotation" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeGauche" runat="server" Height="17px" Width="94px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                 </asp:Label>          
             </asp:TableCell>
	         <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteC1" runat="server" Height="17px" Width="30px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="C"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  right;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteC2" runat="server" Height="17px" Width="60px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(0-20)"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 3px; text-align: left;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteBMoins1" runat="server" Height="17px" Width="30px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="B-"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  right;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteBMoins2" runat="server" Height="17px" Width="60px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(21-40)"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 3px; text-align:  left;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteB1" runat="server" Height="17px" Width="30px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="B"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  right;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteB2" runat="server" Height="17px" Width="60px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(41-60)"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 3px; text-align:  left;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteBPlus1" runat="server" Height="17px" Width="30px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="B+"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  right;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteBPlus2" runat="server" Height="17px" Width="60px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(61-80)"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 3px; text-align:  left;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteA1" runat="server" Height="17px" Width="30px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="A"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  right;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteA2" runat="server" Height="17px" Width="60px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(81-90)"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 3px; text-align:  left;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteAPlus1" runat="server" Height="17px" Width="30px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="A*"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  right;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteAPlus2" runat="server" Height="17px" Width="60px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(91-100)"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 3px; text-align:  left;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeDroite" runat="server" Height="17px" Width="94px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                 </asp:Label>          
             </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreEvaluationFinale" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
	         <asp:TableCell>
                <asp:Label ID="TitreEvaluationGlobaleGauche" runat="server" Height="32px" Width="150px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreEvaluationPerformanceGlobale" runat="server" Height="25px" Width="120px"
                    BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text="Performance"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreEvaluationComportementGlobale" runat="server" Height="25px" Width="120px"
                    BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text="Comportement"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreEvaluationTotaleGlobale" runat="server" Height="25px" Width="80px"
                    BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text="Global"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="TitreEvaluationNoteGlobale" runat="server" Height="25px" Width="80px"
                    BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text="NOTE"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="TitreEvaluationGlobaleDroite" runat="server" Height="32px" Width="150px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreNotationFinale" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
	         <asp:TableCell>
                <asp:Label ID="EvaluationGlobaleGauche" runat="server" Height="32px" Width="150px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="EvaluationPerformanceGlobale" runat="server" Height="25px" Width="120px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="Notset" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="EvaluationComportementGlobale" runat="server" Height="25px" Width="120px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="Notset" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="EvaluationTotaleGlobale" runat="server" Height="25px" Width="80px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="Notset" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="EvaluationNoteGlobale" runat="server" Height="25px" Width="80px"
                    BackColor="#124545" BorderColor="#8DA8A3" BorderStyle="Notset" Text=""
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Large"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="EvaluationGlobaleDroite" runat="server" Height="32px" Width="150px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreEnteteSignatureEvaluation" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
	         <asp:TableCell>
                <asp:Label ID="TitreSignatureGauche" runat="server" Height="42px" Width="130px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="TitreSignatureCollaborateur" runat="server" Height="35px" Width="170px"
                    BackColor="#CAEBE4" BorderColor="#8DA8A3" BorderStyle="Notset" Text="Date de signature <br/> Collaborateur"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreSignaturePeriodeKI" runat="server" Height="35px" Width="130px"
                    BackColor="#CAEBE4" BorderColor="#8DA8A3" BorderStyle="Notset" Text="Période KI <br/>"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreSignatureEvaluateur" runat="server" Height="35px" Width="170px"
                    BackColor="#CAEBE4" BorderColor="#8DA8A3" BorderStyle="Notset" Text="Date de signature <br/> Evaluateur"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="TitreSignatureDroite" runat="server" Height="42px" Width="130px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreSignatureDebutKI" runat="server" CellPadding="0" Width="750px" Height="26px"> 
           <asp:TableRow>
	         <asp:TableCell>
                <asp:Label ID="LabelSignatureGaucheDebutKI" runat="server" Height="24px" Width="195px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="7" V_SiDonneeDico="true"
                    Etivisible="false" DonWidth="100px" DonHeight="22px" DonTabIndex="44"
                    Etitext="" Donstyle="margin-left: -2px;"
                    DonBorderWidth="1px"/>           
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelSignatureDebutKI" runat="server" Height="21px" Width="130px"
                    BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="Notset" Text="DEBUT"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 4px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
		        <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="4" V_SiDonneeDico="true"
                    Etivisible="false" DonWidth="100px" DonHeight="22px" DonTabIndex="45"
                    Etitext="" Donstyle="margin-left: -2px;"
                    DonBorderWidth="1px"/>                
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="LabelSignatureDroiteDebutKI" runat="server" Height="24px" Width="198px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
        </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreSignatureMilieuKI" runat="server" CellPadding="0" Width="750px" Height="26px"> 
           <asp:TableRow>
	         <asp:TableCell>
                <asp:Label ID="LabelSignatureGaucheMilieuKI" runat="server" Height="24px" Width="195px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="8" V_SiDonneeDico="true"
                    Etivisible="false" DonWidth="100px" DonHeight="22px" DonTabIndex="46"
                    Etitext="" Donstyle="margin-left: -2px;"
                    DonBorderWidth="1px"/>           
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelSignatureMilieuKI" runat="server" Height="21px" Width="130px"
                    BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="Notset" Text="INTERMEDIAIRE"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 4px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
		        <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="5" V_SiDonneeDico="true"
                    Etivisible="false" DonWidth="100px" DonHeight="22px" DonTabIndex="47"
                    Etitext="" Donstyle="margin-left: -2px;"
                    DonBorderWidth="1px"/>                
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="LabelSignatureDroiteMilieuKI" runat="server" Height="24px" Width="198px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
        </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreSignatureFinKI" runat="server" CellPadding="0" Width="750px" Height="26px"> 
           <asp:TableRow>
	         <asp:TableCell>
                <asp:Label ID="LabelSignatureGaucheFinKI" runat="server" Height="24px" Width="195px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="9" V_SiDonneeDico="true"
                    Etivisible="false" DonWidth="100px" DonHeight="22px" DonTabIndex="48"
                    Etitext="" Donstyle="margin-left: -2px;"
                    DonBorderWidth="1px"/>           
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelSignatureFinKI" runat="server" Height="21px" Width="130px"
                    BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="Notset" Text="FIN"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 4px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
		        <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="6" V_SiDonneeDico="true"
                    Etivisible="false" DonWidth="100px" DonHeight="22px" DonTabIndex="49"
                    Etitext="" Donstyle="margin-left: -2px;"
                    DonBorderWidth="1px"/>                
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="LabelSignatureDroiteFinKI" runat="server" Height="24px" Width="198px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
        </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreRefusSignature" runat="server" CellPadding="0" Width="750px"> 
           <asp:TableRow>
	         <asp:TableCell HorizontalAlign="left">
                <asp:Label ID="LabelRefusSignature" runat="server" Height="25px" Width="220px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="Refus de signature du collaborateur"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align: right;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell HorizontalAlign="Left">
                <Virtualia:VTrioHorizontalRadio ID="RadioH10" runat="server" V_Groupe="Refus"
                    V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="true"
                    RadioGaucheWidth="70px" RadioCentreWidth="70px" RadioDroiteWidth="0px"
                    RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
			        RadioGaucheHeight="20px" RadioCentreHeight="20px"
                    RadioCentreStyle="text-align: left;" RadioGaucheStyle="text-align: left;"
			        Visible="true"/>          
		     </asp:TableCell>
             <asp:TableCell HorizontalAlign="left">
                <asp:Label ID="LabelRefusSignatureDroite" runat="server" Height="25px" Width="350px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
        </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>

</asp:Table>