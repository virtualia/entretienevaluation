﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_TRAVAIL_HONDA.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_TRAVAIL_HONDA" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 0px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreEnteteTravail" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#2FA49B">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                        <asp:Label ID="Etiquette" runat="server" Text="ENTRETIEN ANNUEL RELATIF A L'APPLICATION DU FORFAIT ANNUEL EN JOURS - 91 KI" Height="30px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                        <asp:Label ID="EtiquetteSuite" runat="server" Text="( prévu à l'article L.3121-46 du Code du travail )" Height="30px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre1" runat="server" Text="" Height="25px" Width="75px"
                            BackColor= "transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#0E5F5C"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre2" runat="server" Text="Nom" Height="25px" Width="200px"
                            BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre3" runat="server" Text="Département" Height="25px" Width="235px"
                            BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre4" runat="server" Text="Fonction" Height="25px" Width="230px"
                            BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletCollaborateur" runat="server" Text="Collaborateur" Height="30px" Width="75px"
                            BackColor= "Transparent" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 15px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteCollaborateur" runat="server" Text="" Height="45px" Width="200px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDepartementCollaborateur" runat="server" Text="" Height="45px" Width="235px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFonctionCollaborateur" runat="server" Text="" Height="45px" Width="230px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletEvaluateur" runat="server" Text="Evaluateur" Height="30px" Width="75px"
                            BackColor= "Transparent" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 15px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteEvaluateur" runat="server" Text="" Height="45px" Width="200px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDepartementEvaluateur" runat="server" Text="" Height="45px" Width="235px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFonctionEvaluateur" runat="server" Text="" Height="45px" Width="230px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletPeriodeKI" runat="server" Text="Période KI" Height="19px" Width="75px"
                            BackColor= "Transparent" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelPeriodeKI" runat="server" Text="" Height="19px" Width="200px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: center; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletRevision" runat="server" Text="" Height="19px" Width="235px"
                            BackColor= "Transparent" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: right; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDateRevision" runat="server" Text="" Height="19px" Width="230px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="4"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="8px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTravailManager" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#2FA49B">
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTitreTravailManager" runat="server" Height="30px" Width="744px"
                            BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="1 - OBSERVATIONS DE L'EVALUATEUR ET VERIFICATION DE L'ORGANISATION DU TRAVAIL"
                            BorderWidth="1px" ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailManager1" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Suivi des jours travaillés sur l'année (Virtualia)"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 7px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailManager2" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Volume de la charge de travail et répartition dans le temps"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailManager3" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Organisation du travail du salarié par rapport à celle du service"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailManager4" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Respect d'une amplitude journalière 'raisonnable' (maximum de 13 heures / jour)"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailManager5" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Possibilité pour le salarié de prendre ses jours de repos (jours RTT, repos hebdomadaires, jours de récupération, congés payés)"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailManager6" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Articulation entre l'activité professionnelle et la vie personnelle"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailManager7" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Rapport entre la rémunération perçue par le salarié et les sujétions qui lui sont imposées dans le cadre du forfait jour"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailManager8" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="ainsi que le respect des minima conventionnels de l'année"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailManager9" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Autres observations"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreCommentaireManager" runat="server" Height="25px" Width="744px"
                           BackColor="#D7FAF3" BorderColor="#8DA8A3" BorderStyle="NotSet" BorderWidth="1px" 
                           Text="Commentaires de l'évaluateur"
                           Font-Bold="True" Forecolor="#124545" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px; padding-top: 8px;
                           font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV31" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="31" V_SiDonneeDico="true"
                           EtiWidth="741px" DonWidth="741px" DonHeight="200px" EtiHeight="20px" DonTabIndex="1" 
                           EtiText="" Etivisible="false"
                           Donstyle="margin-left: 1px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
          </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="8px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTravailSalarie" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#2FA49B">
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTitreTravailSalarie" runat="server" Height="30px" Width="744px"
                            BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="2 - OBSERVATIONS DU COLLABORATEUR"
                            BorderWidth="1px" ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailSalarie1" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Suivi des jours travaillés sur l'année (Virtualia)"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 7px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailSalarie2" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Volume de la charge de travail et répartition dans le temps"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailSalarie3" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Organisation du travail du salarié par rapport à celle du service"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailSalarie4" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Respect d'une amplitude journalière 'raisonnable' (maximum de 13 heures / jour)"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailSalarie5" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Possibilité pour le salarié de prendre ses jours de repos (jours RTT, repos hebdomadaires, jours de récupération, congés payés)"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailSalarie6" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Articulation entre l'activité professionnelle et la vie personnelle"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailSalarie7" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Rapport entre la rémunération perçue par le salarié et les sujétions qui lui sont imposées dans le cadre du forfait jour"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailSalarie8" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="ainsi que le respect des minima conventionnels de l'année"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTravailSalarie9" runat="server" Height="24px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="- Autres observations"
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="95%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreCommentaireSalarie" runat="server" Height="25px" Width="744px"
                           BackColor="#D7FAF3" BorderColor="#8DA8A3" BorderStyle="NotSet" BorderWidth="1px" 
                           Text="Commentaires du collaborateur"
                           Font-Bold="True" Forecolor="#124545" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px; padding-top: 8px;
                           font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV30" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="30" V_SiDonneeDico="true"
                           EtiWidth="741px" DonWidth="741px" DonHeight="200px" EtiHeight="20px" DonTabIndex="2" 
                           EtiText="" Etivisible="false"
                           Donstyle="margin-left: 1px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
          </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
</asp:Table>