﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_PERFORMANCE_HONDA.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_PERFORMANCE_HONDA" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioEtiTextCommande.ascx" tagname="VTrioEtiTextCommande" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 0px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreEntetePerformance" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#2FA49B">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                        <asp:Label ID="Etiquette" runat="server" Text="PERFORMANCE DU COLLABORATEUR - 90 KI" Height="30px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre1" runat="server" Text="" Height="25px" Width="75px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#0E5F5C"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre2" runat="server" Text="Nom" Height="25px" Width="200px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre3" runat="server" Text="Département" Height="25px" Width="235px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre4" runat="server" Text="Fonction" Height="25px" Width="230px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletCollaborateur" runat="server" Text="Collaborateur" Height="30px" Width="75px"
                            BackColor="Transparent"  BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 15px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteCollaborateur" runat="server" Text="" Height="35px" Width="200px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDepartementCollaborateur" runat="server" Text="" Height="35px" Width="235px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFonctionCollaborateur" runat="server" Text="" Height="35px" Width="230px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletEvaluateur" runat="server" Text="Evaluateur" Height="30px" Width="75px"
                            BackColor="Transparent"  BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 15px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteEvaluateur" runat="server" Text="" Height="35px" Width="200px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDepartementEvaluateur" runat="server" Text="" Height="35px" Width="235px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFonctionEvaluateur" runat="server" Text="" Height="35px" Width="230px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletPeriodeKI" runat="server" Text="Période KI" Height="19px" Width="75px"
                            BackColor="Transparent"  BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelPeriodeKI" runat="server" Text="" Height="19px" Width="200px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: center; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletRevision" runat="server" Text="Date de révision" Height="19px" Width="235px"
                            BackColor="Transparent"  BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: right; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDateRevision" runat="server" Text="" Height="19px" Width="230px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="4"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="8px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreRole" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#2FA49B">
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTitreRoleOrganisation" runat="server" Height="30px" Width="744px"
                            BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Rôle dans l'organisation et cible"
                            BorderWidth="1px" ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelCommentaireRoleOrganisation" runat="server" Height="20px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="Le rôle de l'organisation dans l'entreprise et la cible principale du KI"
                            BorderWidth="1px" ForeColor="#2FA49B" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: oblique; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreRole" runat="server" Height="17px" Width="744px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="ROLE"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV11" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="11" V_SiDonneeDico="true"
                           EtiWidth="741px" DonWidth="741px" DonHeight="100px" EtiHeight="20px" DonTabIndex="1" 
                           EtiText="" Etivisible="false"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreCible" runat="server" Height="17px" Width="744px"
                           BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="CIBLE"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 3px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV12" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="12" V_SiDonneeDico="true"
                           EtiWidth="741px" DonWidth="741px" DonHeight="100px" EtiHeight="20px" DonTabIndex="2" 
                           EtiText="" Etivisible="false"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTitreRoleCollaborateur" runat="server" Height="30px" Width="744px"
                            BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Rôle du collaborateur"
                            BorderWidth="1px" ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelCommentaireRoleCollaborateur" runat="server" Height="20px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="Rôle individuel du collaborateur"
                            BorderWidth="1px" ForeColor="#2FA49B" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: oblique; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="true"
                           EtiWidth="741px" DonWidth="741px" DonHeight="100px" EtiHeight="20px" DonTabIndex="3" 
                           EtiText="" Etivisible="false"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelTitreExigence" runat="server" Height="30px" Width="744px"
                            BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Exigence de comportement"
                            BorderWidth="1px" ForeColor="White" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelCommentaireExigence" runat="server" Height="20px" Width="744px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="Objectif principal d'un comportement collaborateur basé sur la philosophie HONDA"
                            BorderWidth="1px" ForeColor="#2FA49B" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: oblique; text-indent: 5px; text-align:  Left;">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV14" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="14" V_SiDonneeDico="true"
                           EtiWidth="741px" DonWidth="741px" DonHeight="100px" EtiHeight="20px" DonTabIndex="4" 
                           EtiText="" Etivisible="false"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
          </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="8px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
         <asp:Label ID="LabelEvaluationPerformance" runat="server" Height="30px" Width="748px"
            BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Evaluation de la performance"
            BorderWidth="1px" ForeColor="White" Font-Italic="False"
            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
            font-style: normal; text-indent: 0px; text-align:  center;">
         </asp:Label>          
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="7px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="EnteteObjectifSociete1" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
             <asp:TableCell>
                <asp:Label ID="TitreRealisationSociete" runat="server" Height="28px" Width="300px"
                    BackColor="#124545" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Réalisation des objectifs de la Société"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 5px; text-align:  left;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TexteRealisationSociete" runat="server" Height="35px" Width="440px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="Réalisation des thèmes fixés par la Direction ou le Département"
                    BorderWidth="1px" ForeColor="#2FA49B" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: oblique; text-indent: 1px; text-align:  left;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="EnteteObjectifSociete2" runat="server" CellPadding="0" Width="810px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
             <asp:TableCell>
                <asp:Label ID="LabelObjectifSociete" runat="server" Height="35px" Width="174px"
                    BackColor="Transparent" BorderColor="GrayText" BorderStyle="None" Text="<br/> Objectif"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelTargetSociete" runat="server" Height="35px" Width="205px"
                    BackColor="Transparent" BorderColor="GrayText" BorderStyle="None" Text="<br/> Control Item (Target)"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelRealisationSociete" runat="server" Height="35px" Width="205px"
                    BackColor="Transparent" BorderColor="GrayText" BorderStyle="None" Text="<br/> Réalisation effective"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelPoidsSociete" runat="server" Height="35px" Width="54px"
                    BackColor="#9EB0AC" BorderColor="LightGray" BorderStyle="Notset" Text="Poids de l'objectif"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelResultatSociete" runat="server" Height="35px" Width="52px"
                    BackColor="#9EB0AC" BorderColor="LightGray" BorderStyle="Notset" Text="Résultat atteint"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelDifficulteSociete" runat="server" Height="35px" Width="55px"
                    BackColor="#9EB0AC" BorderColor="LightGray" BorderStyle="Notset" Text="Difficulté <br/>"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelEvaluationSociete" runat="server" Height="35px" Width="58px"
                    BackColor="#9EB0AC" BorderColor="LightGray" BorderStyle="Notset" Text="Evaluation finale"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreObjectifSociete" runat="server" CellPadding="0" Width="810px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneObjSociete1" runat="server" Height="100px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" BorderColor="LightGrey" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="157px" DonHeight="130px" DonTabIndex="5" DonTooltip="Objectif" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="6" DonTooltip="Control Item (Target)" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="7" DonTooltip="Réalisation effective" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table ID="PerformanceObjSociete1" runat="server" CellPadding="0" Width="225px" 
                                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
                                    <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelCollPerfObjSoc1" runat="server" Text="Collaborateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell>
                                   </asp:TableRow>
                                    <asp:TableRow ID="CollaborateurPerfObjSoc1">
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCK09" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="9" 
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="8" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCK10" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="10"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="9" DonTooltip="Résultat atteint - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCK11" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="11"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="10" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCK12" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="12"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="11" DonTooltip="Evaluation finale - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                   <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelEvalPerfObjSoc1" runat="server" Text="Evaluateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell> 
                                   </asp:TableRow>
		                           <asp:TableRow ID="EvaluateurPerfObjSoc1">
                                     <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCK05" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="5"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="12" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCK06" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="6"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="13" DonTooltip="Résultat atteint - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCK07" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="7"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="14" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCK08" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="8"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="15" DonTooltip="Evaluation finale - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                </asp:Table>            
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>       
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneObjSociete2" runat="server" Height="100px" CellPadding="0" Width="810px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" BorderColor="LightGrey" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="157px" DonHeight="130px" DonTabIndex="16" DonTooltip="Objectif" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="17" DonTooltip="Control Item (Target)" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="18" DonTooltip="Réalisation effective" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table ID="PerformanceObjSociete2" runat="server" CellPadding="0" Width="225px" 
                                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
                                    <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelCollPerfObjSoc2" runat="server" Text="Collaborateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell>
                                   </asp:TableRow>
                                    <asp:TableRow ID="CollaborateurPerfObjSoc2">
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCL09" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="9"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="19" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCL10" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="10"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="20" DonTooltip="Résultat atteint - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCL11" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="11"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="21" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCL12" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="12"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="22" DonTooltip="Evaluation finale - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                   <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelEvalPerfObjSoc2" runat="server" Text="Evaluateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell> 
                                   </asp:TableRow>
		                           <asp:TableRow ID="EvaluateurPerfObjSoc2">
                                     <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCL05" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="5"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="23" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCL06" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="6"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="24" DonTooltip="Résultat atteint - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCL07" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="7"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="25" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCL08" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="8"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="26" DonTooltip="Evaluation finale - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                </asp:Table>            
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>       
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneObjSociete3" runat="server" Height="100px" CellPadding="0" Width="810px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" BorderColor="LightGrey" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="157px" DonHeight="130px" DonTabIndex="27" DonTooltip="Objectif" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="28" DonTooltip="Control Item (Target)" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="29" DonTooltip="Réalisation effective" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table ID="PerformanceObjSociete3" runat="server" CellPadding="0" Width="225px" 
                                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
                                    <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelCollPerfObjSoc3" runat="server" Text="Collaborateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell>
                                   </asp:TableRow>
                                    <asp:TableRow ID="CollaborateurPerfObjSoc3">
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCM09" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="9"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="30" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCM10" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="10"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="31" DonTooltip="Résultat atteint - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCM11" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="11"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="32" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCM12" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="12"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="33" DonTooltip="Evaluation finale - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                   <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelEvalPerfObjSoc3" runat="server" Text="Evaluateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell> 
                                   </asp:TableRow>
		                           <asp:TableRow ID="EvaluateurPerfObjSoc3">
                                     <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCM05" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="5"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="34" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCM06" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="6"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="35" DonTooltip="Résultat atteint - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCM07" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="7"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="36" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCM08" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="8"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="37" DonTooltip="Evaluation finale - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                </asp:Table>            
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>       
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneObjSociete4" runat="server" Height="100px" CellPadding="0" Width="810px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" BorderColor="LightGrey" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="157px" DonHeight="130px" DonTabIndex="38" DonTooltip="Objectif" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="39" DonTooltip="Control Item (Target)" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVN04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="40" DonTooltip="Réalisation effective" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table ID="PerformanceObjSociete4" runat="server" CellPadding="0" Width="225px" 
                                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
                                    <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelCollPerfObjSoc4" runat="server" Text="Collaborateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell>
                                   </asp:TableRow>
                                    <asp:TableRow ID="CollaborateurPerfObjSoc4">
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCN09" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="9"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="41" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCN10" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="10"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="42" DonTooltip="Résultat atteint - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCN11" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="11"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="43" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCN12" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="12"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="44" DonTooltip="Evaluation finale - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                   <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelEvalPerfObjSoc4" runat="server" Text="Evaluateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell> 
                                   </asp:TableRow>
		                           <asp:TableRow ID="EvaluateurPerfObjSoc4">
                                     <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCN05" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="5"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="45" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCN06" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="6"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="46" DonTooltip="Résultat atteint - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCN07" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="7"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="47" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCN08" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="8"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="48" DonTooltip="Evaluation finale - | 1 | 2 | 3 | 4 | 5 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                </asp:Table>            
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>       
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="EnteteObjectifFonction1" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
             <asp:TableCell>
                <asp:Label ID="TitreRealisationFonction" runat="server" Height="28px" Width="300px"
                    BackColor="#124545" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Réalisation des objectifs de la fonction"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 5px; text-align:  left;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TexteRealisationFonction" runat="server" Height="35px" Width="440px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="La réalisation de son objectif dérivé de la responsabilité individuelle en milieu de travail"
                    BorderWidth="1px" ForeColor="#2FA49B" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: oblique; text-indent: 1px; text-align:  left;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="EnteteObjectifFonction2" runat="server" CellPadding="0" Width="810px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
             <asp:TableCell>
                <asp:Label ID="LabelObjectifFonction" runat="server" Height="35px" Width="174px"
                    BackColor="Transparent" BorderColor="GrayText" BorderStyle="None" Text="<br/> Objectif"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelTargetFonction" runat="server" Height="35px" Width="205px"
                    BackColor="Transparent" BorderColor="GrayText" BorderStyle="None" Text="<br/> Control Item (Target)"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelRealisationFonction" runat="server" Height="35px" Width="205px"
                    BackColor="Transparent" BorderColor="GrayText" BorderStyle="None" Text="<br/> Réalisation effective"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelPoidsFonction" runat="server" Height="35px" Width="54px"
                    BackColor="#9EB0AC" BorderColor="LightGray" BorderStyle="Notset" Text="Poids de l'objectif"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelResultatFonction" runat="server" Height="35px" Width="52px"
                    BackColor="#9EB0AC" BorderColor="LightGray" BorderStyle="Notset" Text="Résultat atteint"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelDifficulteFonction" runat="server" Height="35px" Width="55px"
                    BackColor="#9EB0AC" BorderColor="LightGray" BorderStyle="Notset" Text="Difficulté <br/>"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelEvaluationFonction" runat="server" Height="35px" Width="58px"
                    BackColor="#9EB0AC" BorderColor="LightGray" BorderStyle="Notset" Text="Evaluation finale"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreObjectifFonction" runat="server" Height="75px" CellPadding="0" Width="810px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#2FA49B">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneObjFonction1" runat="server" Height="100px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" BorderColor="LightGrey" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="157px" DonHeight="130px" DonTabIndex="49" DonTooltip="Objectif" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="50" DonTooltip="Control Item (Target)" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVO04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="51" DonTooltip="Réalisation effective" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table ID="PerformanceObjFonction1" runat="server" CellPadding="0" Width="225px" 
                                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
                                    <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelCollPerfObjFonc1" runat="server" Text="Collaborateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell>
                                   </asp:TableRow>
                                    <asp:TableRow ID="CollaborateurPerfObjFonc1">
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCO09" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="9"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="52" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCO10" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="10"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="53" DonTooltip="Résultat atteint - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCO11" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="11"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="54" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCO12" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="12"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="55" DonTooltip="Evaluation finale - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                   <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelEvalPerfObjFonc1" runat="server" Text="Evaluateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell> 
                                   </asp:TableRow>
		                           <asp:TableRow ID="EvaluateurPerfObjFonc1">
                                     <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCO05" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="5"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="56" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCO06" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="6"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="57" DonTooltip="Résultat atteint - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCO07" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="7"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="58" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCO08" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="8"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="59" DonTooltip="Evaluation finale - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                </asp:Table>            
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>       
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneObjFonction2" runat="server" Height="100px" CellPadding="0" Width="810px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" BorderColor="LightGrey" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="157px" DonHeight="130px" DonTabIndex="60" DonTooltip="Objectif" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="61" DonTooltip="Control Item (Target)" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVP04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="62" DonTooltip="Réalisation effective" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table ID="PerformanceObjFonction2" runat="server" CellPadding="0" Width="225px" 
                                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
                                    <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelCollPerfObjFonc2" runat="server" Text="Collaborateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell>
                                   </asp:TableRow>
                                    <asp:TableRow ID="CollaborateurPerfObjFonc2">
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCP09" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="9"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="63" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCP10" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="10"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="64" DonTooltip="Résultat atteint - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCP11" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="11"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="65" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCP12" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="12"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="66" DonTooltip="Evaluation finale - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                   <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelEvalPerfObjFonc2" runat="server" Text="Evaluateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell> 
                                   </asp:TableRow>
		                           <asp:TableRow ID="EvaluateurPerfObjFonc2">
                                     <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCP05" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="5"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="67" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCP06" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="6"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="68" DonTooltip="Résultat atteint - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCP07" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="7"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="69" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCP08" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="8"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="70" DonTooltip="Evaluation finale - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                </asp:Table>            
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>       
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneObjFonction3" runat="server" Height="100px" CellPadding="0" Width="810px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" BorderColor="LightGrey" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="157px" DonHeight="130px" DonTabIndex="71" DonTooltip="Objectif" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="72" DonTooltip="Control Item (Target)" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="73" DonTooltip="Réalisation effective" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table ID="PerformanceObjFonction3" runat="server" CellPadding="0" Width="225px" 
                                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
                                    <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelCollPerfObjFonc3" runat="server" Text="Collaborateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell>
                                   </asp:TableRow>
                                    <asp:TableRow ID="CollaborateurPerfObjFonc3">
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCQ09" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="9"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="74" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCQ10" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="10"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="75" DonTooltip="Résultat atteint - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCQ11" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="11"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="76" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCQ12" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="12"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="77" DonTooltip="Evaluation finale - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                   <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelEvalPerfObjFonc3" runat="server" Text="Evaluateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell> 
                                   </asp:TableRow>
		                           <asp:TableRow ID="EvaluateurPerfObjFonc3">
                                     <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCQ05" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="5"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="78" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCQ06" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="6"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="79" DonTooltip="Résultat atteint - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCQ07" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="7"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="80" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCQ08" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="8"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="81" DonTooltip="Evaluation finale - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                </asp:Table>            
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>       
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneObjFonction4" runat="server" Height="100px" CellPadding="0" Width="810px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" BorderColor="LightGrey" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVR02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="157px" DonHeight="130px" DonTabIndex="82" DonTooltip="Objectif" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVR03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="83" DonTooltip="Control Item (Target)" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVR04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="84" DonTooltip="Réalisation effective" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table ID="PerformanceObjFonction4" runat="server" CellPadding="0" Width="225px" 
                                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
                                    <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelCollPerfObjFonc4" runat="server" Text="Collaborateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell>
                                   </asp:TableRow>
                                    <asp:TableRow ID="CollaborateurPerfObjFonc4">
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCR09" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="9"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="85" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCR10" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="10"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="86" DonTooltip="Résultat atteint - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCR11" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="11"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="87" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCR12" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="12"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="88" DonTooltip="Evaluation finale - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                   <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelEvalPerfObjFonc4" runat="server" Text="Evaluateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell> 
                                   </asp:TableRow>
		                           <asp:TableRow ID="EvaluateurPerfObjFonc4">
                                     <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCR05" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="5"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="89" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCR06" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="6"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="90" DonTooltip="Résultat atteint - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCR07" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="7"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="91" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCR08" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="8"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="92" DonTooltip="Evaluation finale - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                </asp:Table>            
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>       
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="LigneObjFonction5" runat="server" Height="100px" CellPadding="0" Width="810px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" BorderColor="LightGrey" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVS02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   DonWidth="157px" DonHeight="130px" DonTabIndex="93" DonTooltip="Objectif" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%" 
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVS03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="94" DonTooltip="Control Item (Target)" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVS04" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                   DonWidth="205px" DonHeight="130px" DonTabIndex="95" DonTooltip="Réalisation effective" 
                                   EtiText="" Etivisible="false"
                                   Donstyle="margin-left: 1px; Font-Bold: False; Font-Size: 90%"
                                   DonBorderWidth="1px"/>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table ID="PerformanceObjFonction5" runat="server" CellPadding="0" Width="225px" 
                                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
                                    <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelCollPerfObjFonc5" runat="server" Text="Collaborateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell>
                                   </asp:TableRow>
                                    <asp:TableRow ID="CollaborateurPerfObjFonc5">
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCS09" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="9"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="96" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCS10" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="10"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="97" DonTooltip="Résultat atteint - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCS11" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="11"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="98" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCS12" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="12"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="99" DonTooltip="Evaluation finale - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: 85%"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="#E9FDF9"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                   <asp:TableRow>
                                     <asp:TableCell ColumnSpan="4">
                                        <asp:Label ID="LabelEvalPerfObjFonc5" runat="server" Text="Evaluateur" Height="12px" Width="225px"
                                           BackColor="Transparent"  BorderStyle="None"
                                           ForeColor="#124545"
                                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="75%"
                                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                           font-style: normal; text-indent: 0px; text-align: center; padding-top: 5px">
                                        </asp:Label>
                                     </asp:TableCell> 
                                   </asp:TableRow>
		                           <asp:TableRow ID="EvaluateurPerfObjFonc5">
                                     <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCS05" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="5"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="100" DonTooltip="Poids de l'objectif - | 20% | 25% | 30% | 35% | 40% |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="%" V_ValMax="40" V_ValMin="20" V_Pas="5"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCS06" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="6"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="101" DonTooltip="Résultat atteint - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCS07" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="7"
                                            Etivisible="false" EtiHeight="0px" Width="54px"
                                            DonHeight="24px" DonTabIndex="102" DonTooltip="Difficulté - | Normal-N | Elevé-Y |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="ListeValeur" V_Suffixe="" V_ListeValeurs="N|Y"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>   
		                             <asp:TableCell>
                                        <Virtualia:VTrioEtiTextCommande ID="InfoCS08" runat="server" 
                                            V_PointdeVue="1" V_Objet="151" V_Information="8"
                                            Etivisible="false" EtiHeight="0px" Width="54px" CmdVisible="false"
                                            DonHeight="24px" DonTabIndex="103" DonTooltip="Evaluation finale - | 0 | 15 | 30 | 45 | 60 |"
                                            Donstyle="margin-left: 0px; text-align: center; Font-Bold: False; Font-Size: Small"
					                        V_Strict="false"  V_TypeCtrl="Bornee" V_Suffixe="" V_ValMax="60" V_ValMin="0" V_Pas="15"
                                            DonBorderWidth="1px" DonBackColor="White"/>
                                     </asp:TableCell>          
                                   </asp:TableRow>
                                </asp:Table>            
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>       
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadrePerformanceCollaborateur" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
             <asp:TableCell>
                <asp:Label ID="TitrePerformanceCollaborateur" runat="server" Height="35px" Width="523px"
                    BackColor="#E9FDF9" BorderColor="LightGray" BorderStyle="Notset" Text="1. Evaluation des activités (Collaborateur) : Maximum 60 points"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="SommePoidsObjectifsCollaborateur" runat="server" Height="35px" Width="54px"
                    BackColor="#E9FDF9" BorderColor="LightGray" BorderStyle="Notset" Text="."
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelResultatCollaborateur" runat="server" Height="35px" Width="110px"
                    BackColor="#E9FDF9" BorderColor="LightGray" BorderStyle="Notset" Text="Total performance"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                    font-style: oblique; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelPerformanceCollaborateur" runat="server" Height="35px" Width="55px"
                    BackColor="#E9FDF9" BorderColor="LightGray" BorderStyle="Notset" Text="."
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadrePerformanceEvaluateur" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
             <asp:TableCell>
                <asp:Label ID="TitrePerformanceEvaluateur" runat="server" Height="40px" Width="523px"
                    BackColor="#CAEBE4" BorderColor="LightGray" BorderStyle="Notset" Text="1. Evaluation des activités (Evaluateur) : Maximum 60 points"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 15px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="SommePoidsObjectifsEvaluateur" runat="server" Height="40px" Width="54px"
                    BackColor="#CAEBE4" BorderColor="LightGray" BorderStyle="Notset" Text="."
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 15px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelResultatEvaluateur" runat="server" Height="40px" Width="110px"
                    BackColor="#CAEBE4" BorderColor="LightGray" BorderStyle="Notset" Text="Total performance"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 15px;
                    font-style: oblique; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelPerformanceEvaluateur" runat="server" Height="40px" Width="55px"
                    BackColor="#CAEBE4" BorderColor="LightGray" BorderStyle="Notset" Text="."
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 15px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="5px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
        <asp:Label ID="LabelTitreDifficulte" runat="server" Height="25px" Width="747px"
            BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="NotSet" Text="Explication de difficulté (Evaluateur)"
            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
            style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; padding-top: 8px;
            font-style: normal; text-indent: 3px; text-align:  left;">
        </asp:Label>          
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV19" runat="server" DonTextMode="true"
            V_PointdeVue="1" V_Objet="150" V_Information="19" V_SiDonneeDico="true"
            EtiWidth="741px" DonWidth="745px" DonHeight="60px" EtiHeight="20px" DonTabIndex="104" 
            EtiText="" Etivisible="false"
            Donstyle="margin-left: 0px; margin-top: -2px"
            DonBorderWidth="1px"/>
       </asp:TableCell>
    </asp:TableRow>
</asp:Table>