﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_ENTETE_HONDA.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_ENTETE_HONDA" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

 <asp:Table ID="CadreListe" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0"
    HorizontalAlign="Center">
    <asp:TableRow> 
      <asp:TableCell> 
         <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="600px" SiColonneSelect="true"/>
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>
 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreEnteteComportement" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#2FA49B">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                        <asp:Label ID="Etiquette" runat="server" Text="ENTRETIEN PROFESSIONNEL - 90 KI" Height="30px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre1" runat="server" Text="" Height="25px" Width="75px"
                            BackColor= "transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#0E5F5C"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre2" runat="server" Text="Nom" Height="25px" Width="200px"
                            BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre3" runat="server" Text="Département" Height="25px" Width="235px"
                            BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletTitre4" runat="server" Text="Fonction" Height="25px" Width="230px"
                            BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletCollaborateur" runat="server" Text="Collaborateur" Height="30px" Width="75px"
                            BackColor= "Transparent" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 15px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteCollaborateur" runat="server" Text="" Height="45px" Width="200px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDepartementCollaborateur" runat="server" Text="" Height="45px" Width="235px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFonctionCollaborateur" runat="server" Text="" Height="45px" Width="230px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletEvaluateur" runat="server" Text="Evaluateur" Height="30px" Width="75px"
                            BackColor= "Transparent" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 15px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteEvaluateur" runat="server" Text="" Height="45px" Width="200px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: center; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDepartementEvaluateur" runat="server" Text="" Height="45px" Width="235px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFonctionEvaluateur" runat="server" Text="" Height="45px" Width="230px"
                            BackColor= "#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 12px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletPeriodeKI" runat="server" Text="Période KI" Height="19px" Width="75px"
                            BackColor= "Transparent" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: Left; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelPeriodeKI" runat="server" Text="" Height="19px" Width="200px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: center; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletRevision" runat="server" Text="" Height="19px" Width="235px"
                            BackColor= "Transparent" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#2FA49B"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align: right; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDateRevision" runat="server" Text="" Height="19px" Width="230px"
                            BackColor= "#E9FDF9" BorderColor="#CAEBE4" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#124545"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 2px; text-align: left; padding-top: 6px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="4"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="8px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
         <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="3" V_SiDonneeDico="true"
                    Etivisible="True" DonWidth="100px" DonHeight="22px" DonTabIndex="7"
                    Etitext="Date de l'entretien final d'évaluation" EtiWidth="280px" Donstyle="text-align: center;"
                    DonBorderWidth="1px"/>             
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreEnteteSignatureEvaluation" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
	         <asp:TableCell>
                <asp:Label ID="TitreSignatureGauche" runat="server" Height="42px" Width="130px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="TitreSignatureCollaborateur" runat="server" Height="35px" Width="170px"
                    BackColor="#CAEBE4" BorderColor="#8DA8A3" BorderStyle="Notset" Text="Date de signature <br/> Collaborateur"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreSignaturePeriodeKI" runat="server" Height="35px" Width="130px"
                    BackColor="#CAEBE4" BorderColor="#8DA8A3" BorderStyle="Notset" Text="Période KI <br/>"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreSignatureEvaluateur" runat="server" Height="35px" Width="170px"
                    BackColor="#CAEBE4" BorderColor="#8DA8A3" BorderStyle="Notset" Text="Date de signature <br/> Evaluateur"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="TitreSignatureDroite" runat="server" Height="42px" Width="130px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreSignatureDebutKI" runat="server" CellPadding="0" Width="750px" Height="26px"> 
           <asp:TableRow>
	         <asp:TableCell>
                <asp:Label ID="LabelSignatureGaucheDebutKI" runat="server" Height="24px" Width="195px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="7" V_SiDonneeDico="true"
                    Etivisible="false" DonWidth="100px" DonHeight="22px" DonTabIndex="1"
                    Etitext="" Donstyle="margin-left: -2px; text-align: center;"
                    DonBorderWidth="1px"/>           
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelSignatureDebutKI" runat="server" Height="21px" Width="130px"
                    BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="Notset" Text="DEBUT"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 4px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
		        <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="4" V_SiDonneeDico="true"
                    Etivisible="false" DonWidth="100px" DonHeight="22px" DonTabIndex="2"
                    Etitext="" Donstyle="margin-left: -2px; text-align: center;"
                    DonBorderWidth="1px"/>                
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="LabelSignatureDroiteDebutKI" runat="server" Height="24px" Width="198px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
        </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreSignatureMilieuKI" runat="server" CellPadding="0" Width="750px" Height="26px"> 
           <asp:TableRow>
	         <asp:TableCell>
                <asp:Label ID="LabelSignatureGaucheMilieuKI" runat="server" Height="24px" Width="195px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="8" V_SiDonneeDico="true"
                    Etivisible="false" DonWidth="100px" DonHeight="22px" DonTabIndex="3"
                    Etitext="" Donstyle="margin-left: -2px; text-align: center;"
                    DonBorderWidth="1px"/>           
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelSignatureMilieuKI" runat="server" Height="21px" Width="130px"
                    BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="Notset" Text="INTERMEDIAIRE"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 4px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
		        <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="5" V_SiDonneeDico="true"
                    Etivisible="false" DonWidth="100px" DonHeight="22px" DonTabIndex="4"
                    Etitext="" Donstyle="margin-left: -2px; text-align: center;"
                    DonBorderWidth="1px"/>                
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="LabelSignatureDroiteMilieuKI" runat="server" Height="24px" Width="198px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
        </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreSignatureFinKI" runat="server" CellPadding="0" Width="750px" Height="26px"> 
           <asp:TableRow>
	         <asp:TableCell>
                <asp:Label ID="LabelSignatureGaucheFinKI" runat="server" Height="24px" Width="195px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="9" V_SiDonneeDico="true"
                    Etivisible="false" DonWidth="100px" DonHeight="22px" DonTabIndex="5"
                    Etitext="" Donstyle="margin-left: -2px; text-align: center;"
                    DonBorderWidth="1px"/>           
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="LabelSignatureFinKI" runat="server" Height="21px" Width="130px"
                    BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="Notset" Text="FIN"
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 4px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
		        <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="6" V_SiDonneeDico="true"
                    Etivisible="false" DonWidth="100px" DonHeight="22px" DonTabIndex="6"
                    Etitext="" Donstyle="margin-left: -2px; text-align: center;"
                    DonBorderWidth="1px"/>                
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="LabelSignatureDroiteFinKI" runat="server" Height="24px" Width="198px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
           <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" ColumnSpan="5">
                    <asp:Label ID="EtiSignatures" runat="server" Height="32px" Width="700px"
                            BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" 
                            Text="Aprés avoir saisi la date de signature, le document est finalisé, signé et n'est plus modifiable."
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                </asp:TableCell>
          </asp:TableRow>
        </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell  HorizontalAlign="center">
        <asp:Table ID="CadreRefusSignature" runat="server" CellPadding="0" Width="750px" HorizontalAlign="center"> 
           <asp:TableRow>
               <asp:TableCell HorizontalAlign="center">
                <asp:Label ID="LabelRefusSignatureGauche" runat="server" Height="25px" Width="170px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>
             </asp:TableCell>
	         <asp:TableCell HorizontalAlign="center">
                <asp:Label ID="LabelRefusSignature" runat="server" Height="25px" Width="220px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="Refus de signature du collaborateur"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align: right;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell HorizontalAlign="center">
                <Virtualia:VTrioHorizontalRadio ID="RadioH10" runat="server" V_Groupe="Refus"
                    V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="true"
                    RadioGaucheWidth="70px" RadioCentreWidth="70px" RadioDroiteWidth="0px"
                    RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
			        RadioGaucheHeight="20px" RadioCentreHeight="20px"
                    RadioCentreStyle="text-align: left;" RadioGaucheStyle="text-align: left;"
			        Visible="true"/>          
		     </asp:TableCell>
             <asp:TableCell HorizontalAlign="center">
                <asp:Label ID="LabelRefusSignatureDroite" runat="server" Height="25px" Width="170px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
        </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
         <asp:Label ID="LabelEvaluationFinale" runat="server" Height="30px" Width="200px"
            BackColor="#124545" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Evaluation finale"
            BorderWidth="1px" ForeColor="White" Font-Italic="False"
            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
            font-style: normal; text-indent: 0px; text-align:  center;">
         </asp:Label>          
       </asp:TableCell> 
    </asp:TableRow>
     <asp:TableRow>
       <asp:TableCell Height="6px"></asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
       <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreLegendeNotation" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeGauche" runat="server" Height="17px" Width="94px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                 </asp:Label>          
             </asp:TableCell>
	         <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteC1" runat="server" Height="17px" Width="30px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="C" ToolTip="Doit s’améliorer"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  right;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteC2" runat="server" Height="17px" Width="60px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(0-20)" ToolTip="Doit s’améliorer"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 3px; text-align: left;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteBMoins1" runat="server" Height="17px" Width="30px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="B-" ToolTip="En deçà des attentes"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  right;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteBMoins2" runat="server" Height="17px" Width="60px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(21-40)" ToolTip="En deçà des attentes"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 3px; text-align:  left;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteB1" runat="server" Height="17px" Width="30px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="B" ToolTip="En ligne avec les attentes"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  right;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteB2" runat="server" Height="17px" Width="60px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(41-60)" ToolTip="En ligne avec les attentes"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 3px; text-align:  left;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteBPlus1" runat="server" Height="17px" Width="30px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="B+" ToolTip="Au delà des attentes"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  right;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteBPlus2" runat="server" Height="17px" Width="60px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(61-80)" ToolTip="Au delà des attentes"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 3px; text-align:  left;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteA1" runat="server" Height="17px" Width="30px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="A" ToolTip="Très bien"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  right;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteA2" runat="server" Height="17px" Width="60px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(81-90)" ToolTip="Très bien"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 3px; text-align:  left;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteAPlus1" runat="server" Height="17px" Width="30px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="A*" ToolTip="Excellent"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  right;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeNoteAPlus2" runat="server" Height="17px" Width="60px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text="(91-100)" ToolTip="Excellent"
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 3px; text-align:  left;">
                 </asp:Label>          
             </asp:TableCell>
             <asp:TableCell HorizontalAlign="Center">
                 <asp:Label ID="LabelLegendeDroite" runat="server" Height="17px" Width="94px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 3px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                 </asp:Label>          
             </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreEvaluationFinale" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
	         <asp:TableCell>
                <asp:Label ID="TitreEvaluationGlobaleGauche" runat="server" Height="32px" Width="150px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreEvaluationPerformanceGlobale" runat="server" Height="25px" Width="120px"
                    BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text="Performance"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreEvaluationComportementGlobale" runat="server" Height="25px" Width="120px"
                    BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text="Comportement"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="TitreEvaluationTotaleGlobale" runat="server" Height="25px" Width="80px"
                    BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text="Global"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="TitreEvaluationNoteGlobale" runat="server" Height="25px" Width="80px"
                    BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text="NOTE"
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="TitreEvaluationGlobaleDroite" runat="server" Height="32px" Width="150px"
                    BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
       <asp:TableCell>
        <asp:Table ID="CadreNotationFinale" runat="server" CellPadding="0" Width="750px" 
        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
           <asp:TableRow>
	         <asp:TableCell>
                <asp:Label ID="EvaluationGlobaleGauche" runat="server" Height="32px" Width="150px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="EvaluationPerformanceGlobale" runat="server" Height="25px" Width="120px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="Notset" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="EvaluationComportementGlobale" runat="server" Height="25px" Width="120px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="Notset" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
             <asp:TableCell>
                <asp:Label ID="EvaluationTotaleGlobale" runat="server" Height="25px" Width="80px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="Notset" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="EvaluationNoteGlobale" runat="server" Height="25px" Width="80px"
                    BackColor="#124545" BorderColor="#8DA8A3" BorderStyle="Notset" Text=""
                    BorderWidth="1px" ForeColor="White" Font-Italic="False"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Large"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 7px;
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
	         <asp:TableCell>
                <asp:Label ID="EvaluationGlobaleDroite" runat="server" Height="32px" Width="150px"
                    BackColor="Transparent" BorderColor="#8DA8A3" BorderStyle="None" Text=""
                    BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                    font-style: normal; text-indent: 0px; text-align:  center;">
                </asp:Label>
		     </asp:TableCell>
		   </asp:TableRow>
	    </asp:Table>
	   </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <Virtualia:VCoupleEtiDonnee ID="InfoH26" runat="server" 
                    V_PointdeVue="1" V_Objet="150" V_Information="26" V_SiDonneeDico="true"
                    Etivisible="True" EtiWidth="120px" EtiText="Note révisée" DonWidth="80px" DonHeight="22px" DonTabIndex="8"
                    Donstyle="text-align: center;" DonBorderWidth="1px"/>        
		</asp:TableCell>
    </asp:TableRow>
 </asp:Table>