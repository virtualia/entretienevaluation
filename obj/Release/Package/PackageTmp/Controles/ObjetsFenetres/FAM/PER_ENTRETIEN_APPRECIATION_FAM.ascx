﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_APPRECIATION_FAM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_APPRECIATION_FAM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreAppreciation" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="VI. APPRECIATIONS GENERALES SUR LA VALEUR PROFESSIONNELLE DE L'AGENT" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Evaluation globale des résultats obtenus par l'agent au regard
                            des objectifs fixés initialement ou révisés, le cas échéant, en cours d'année"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Evaluation de la manière de servir de l'agent"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCompetences" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="LabelTitreCompetence" runat="server" Height="30px" Width="320px"
                                       BackColor="Transparent" BorderColor="#9EB0AC" BorderStyle="None" Text=""
                                       BorderWidth="1px" ForeColor="#DBF5EF" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="CadreEnteteLabels" runat="server" CellPadding="0" CellSpacing="0">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote1" runat="server" Height="30px" Width="105px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="EXCELLENT"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote2" runat="server" Height="30px" Width="105px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="TRES BON"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote3" runat="server" Height="30px" Width="105px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="SATISFAISANT"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="LabelTitreNote4" runat="server" Height="30px" Width="105px"
                                               BackColor="White" BorderColor="#9EB0AC" BorderStyle="NotSet" Text="A DEVELOPPER"
                                               BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHA01" runat="server"
                                     V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="1"
                                     Etivisible="False" DonWidth="313px" DonHeight="20px" 
                                     DonText="Qualité du travail" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHA03" runat="server" V_Groupe="Critere1"
                                     V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                                     VRadioN1Width="99px" VRadioN2Width="99px" VRadioN3Width="99px"
                                     VRadioN4Width="97px" VRadioN5Width="0px" VRadioN6Width="0px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 1px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"   
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHB01" runat="server"
                                     V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="2"
                                     Etivisible="False" DonWidth="313px" DonHeight="20px" 
                                     DonText="Qualités relationnelles" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHB03" runat="server" V_Groupe="Critere2"
                                     V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                                     VRadioN1Width="99px" VRadioN2Width="99px" VRadioN3Width="99px"
                                     VRadioN4Width="97px" VRadioN5Width="0px" VRadioN6Width="0px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 1px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHC01" runat="server"
                                     V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="3"
                                     Etivisible="False" DonWidth="313px" DonHeight="20px" 
                                     DonText="Implication personnelle" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHC03" runat="server" V_Groupe="Critere3"
                                     V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                                     VRadioN1Width="99px" VRadioN2Width="99px" VRadioN3Width="99px"
                                     VRadioN4Width="97px" VRadioN5Width="0px" VRadioN6Width="0px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 1px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoHD01" runat="server"
                                     V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="4"
                                     Etivisible="False" DonWidth="313px" DonHeight="20px" 
                                     DonText="Sens du service public" V_SiEnLectureSeule="true"
                                     DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="RadioHD03" runat="server" V_Groupe="Critere4"
                                     V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                                     VRadioN1Width="99px" VRadioN2Width="99px" VRadioN3Width="99px"
                                     VRadioN4Width="97px" VRadioN5Width="0px" VRadioN6Width="0px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 2px; margin-top: 0px;" VRadioN2Style="margin-left: 1px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>   
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Aptitude à exercer des fonctions supérieures"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV18" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="18" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="5" 
                           Etitext="(à remplir par le supérieur hiérarchique direct)" Etivisible="True"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero3" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Appréciations littérales du supérieur hiérarchique direct"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="True"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="6" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: center; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreNotification" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEtiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletNotif" runat="server" Text="VII. NOTIFICATION A L'AGENT DU COMPTE-RENDU DE L'ENTRETIEN PROFESSIONNEL" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteAgent" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"  ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero40" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Observations éventuelles de l'agent"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero41" runat="server" Height="42px" Width="370px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Sur la conduite de l'entretien et les thèmes abordés"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero42" runat="server" Height="42px" Width="370px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Sur les appréciations portées"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVV03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="True"
                           EtiWidth="370px" DonWidth="368px" DonHeight="200px" EtiHeight="20px" DonTabIndex="7" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVW03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="True"
                           EtiWidth="370px" DonWidth="368px" DonHeight="200px" EtiHeight="20px" DonTabIndex="8" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 0px;"                     
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"  ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero43" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Visa et observations éventuelles de l'autorité hiérarchique"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero44" runat="server" Height="42px" Width="370px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Sur la conduite de l'entretien et les thèmes abordés"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero45" runat="server" Height="42px" Width="370px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Sur les appréciations portées"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVX03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="True"
                           EtiWidth="370px" DonWidth="368px" DonHeight="200px" EtiHeight="20px" DonTabIndex="9" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVY03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="True"
                           EtiWidth="370px" DonWidth="368px" DonHeight="200px" EtiHeight="20px" DonTabIndex="10" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 0px;"                     
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="7px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero46" runat="server" Height="42px" Width="470px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Observations éventuelles"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVZ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="True"
                           EtiWidth="470px" DonWidth="468px" DonHeight="200px" EtiHeight="20px" DonTabIndex="11" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 0px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

 </asp:Table>