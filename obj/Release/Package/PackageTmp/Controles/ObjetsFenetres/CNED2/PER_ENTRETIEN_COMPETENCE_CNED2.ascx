﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_COMPETENCE_CNED2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_COMPETENCE_CNED2"%>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia"%>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia"%>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia"%>
<%@ Register src="~/Controles/Saisies/VSixVerticalRadio.ascx" tagname="VSixVerticalRadio" tagprefix="Virtualia"%>

<style type="text/css">
    .EP_TitreCR
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:25px;
        width:746px;
    }
    .EP_TitreOnglet
    {  
        background-color:#E2F5F1;
        color:#0E5F5C;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplement
    {  
        background-color:#E2F5F1;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementBis
    {  
        background-color:#E9FDF9;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiComplementTer
    {  
        background-color:#98D4CA;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-align:left;
        text-indent:5px;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiTableau
    {  
        background-color:#D7FAF3;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_TitreParagraphe
    {  
        background-color:#8DA8A3;
        color:white;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:8px;
        text-align:left;
        padding-top:6px;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
    .EP_EtiNiveau
    {  
        background-color:white;
        color:#124545;
        border-style:solid;
        border-width:1px;
        border-color:#9EB0AC;
        font-family:'Trebuchet MS';
        font-size:x-small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:744px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" HorizontalAlign="Center" 
    style="margin-top: 1px;" Visible="true" Width="750px">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreCompetence" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="3. VALEUR PROFESSIONNELLE ET MANIERE DE SERVIR" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Text="" Height="30px" Width="746px"
                            CssClass="EP_TitreChapitre">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero31" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitre31" runat="server" Text="3.1 Critères d'appréciation" Height="40px" Width="748px"
                           CssClass="EP_TitreParagraphe"
                           style="padding-top: 9px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations31" runat="server" Height="110px" Width="748px"
                           CssClass="EP_EtiComplementTer"
                           Text="L'évaluateur retient, pour apprécier la valeur professionnelle des agents au cours de l'entretien
			               professionnel, les critères annexés à l'arrêté ministériel et qui sont adaptés à la nature des tâches
			               qui leur sont confiées, au niveau de leurs responsabilités et au contexte professionnel.
                           Pour les infirmiers et les médecins seules les parties 2, 3 et 4 doivent être renseignées en tenant compte
                           des limites légales et règlementaires en matière de secret professionnel imposées à ces professionnels.">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero311" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille311" runat="server" Text="1. Les compétences professionnelles et technicité" Height="35px" Width="748px"
                           CssClass="EP_TitreChapitre"
                           style="padding-top: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations311" runat="server" Height="40px" Width="746px"
                           CssClass="EP_EtiComplement"
                           Text="Maîtrise technique ou expertise scientifique du domaine d'activité, connaissance de l'environnement professionnel
                           et capacité à s'y situer, qualité d'expression écrite, qualité d'expression orale, etc.">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero312" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille312" runat="server" Text="2. La contribution à l'activité du service" Height="35px" Width="748px"
                           CssClass="EP_TitreChapitre"
                           style="padding-top: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations312" runat="server" Height="40px" Width="746px"
                           CssClass="EP_EtiComplement"
                           Text="Capacité à partager l'information, à transférer les connaissances et à rendre compte,
			               capacité à s'investir dans des projets, sens du service public et conscience professionnelle,
			               capacité à respecter l'organisation collective du travail, etc.">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVI03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="2" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
        <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero313" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille313" runat="server" Text="3. Les capacités professionnelles et relationnelles" Height="35px" Width="748px"
                           CssClass="EP_TitreChapitre"
                           style="padding-top: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations313" runat="server" Height="40px" Width="746px"
                           CssClass="EP_EtiComplement"
                           Text="Autonomie, discernement et sens des initiatives dans l'exercice de ses attributions,
			               capacité d'adaptation, capacité à travailler en équipe, etc.">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="3" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
        <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero314" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille314" runat="server" Text="4. Aptitude à l'encadrement et/ou à la conduite de projets" Height="35px" Width="748px"
                           CssClass="EP_TitreChapitre"
                           Tooltip="Le cas échéant"
                           style="padding-top: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInformations314" runat="server" Height="40px" Width="746px"
                           CssClass="EP_EtiComplement"
                           Text="Capacité d'organisation et de pilotage, aptitude à la conduite de projets, capacité à déléguer,
			               aptitude au dialogue, à la communication et à la négociation, etc.">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="4" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero321" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitre321" runat="server" Height="50px" Width="748px"
                           CssClass="EP_TitreParagraphe"
                           Text="3.2 Appréciation générale sur la valeur professionnelle, la manière de servir  <br/> &nbsp et la réalisation des objectifs"
                           style="padding-top: 9px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille321" runat="server" Height="40px" Width="748px"
                           CssClass="EP_EtiComplementTer"
                           Text="Merci d'apporter un soin particulier à cette appréciation qui constitue un critère
			               pour l'avancement de grade des agents et pourra être repris dans les rapports liés à la promotion de grade">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreAppreciation" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreAppreciation" runat="server" Height="44px" Width="369px"
                            BackColor="Transparent" BorderStyle="None" Text="">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreEnteteLabels" runat="server" CellPadding="0" CellSpacing="0">
                            <asp:TableRow> 
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTitreNote1" runat="server" Text="sans <br/> objet" Height="44px" Width="60px"
                                    CssClass="EP_EtiNiveau" Font-Size="Small"
                                    style="margin-top: 1px;">
                                </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTitreNote2" runat="server" Text="à <br/> acquérir" Height="44px" Width="80px"
                                    CssClass="EP_EtiNiveau" Font-Size="Small"
                                    style="margin-top: 1px;">
                                </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTitreNote3" runat="server" Text="à <br/> développer" Height="44px" Width="95px"
                                    CssClass="EP_EtiNiveau" Font-Size="Small"                                    
                                    style="margin-top: 1px;">
                                </asp:Label>         
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTitreNote4" runat="server" Text="<br/> Maîtrise" Height="44px" Width="60px"
                                    CssClass="EP_EtiNiveau" Font-Size="Small"
                                    style="margin-top: 1px;">
                                </asp:Label>           
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelTitreNote5" runat="server" Text="<br/> Expert" Height="44px" Width="62px"
                                    CssClass="EP_EtiNiveau" Font-Size="Small"                                    
                                    style="margin-top: 1px;">
                                </asp:Label>          
                            </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>      
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"
                    BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHA01" runat="server"
                            V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="5"
                            Etivisible="False" DonWidth="370px" DonHeight="20px" 
                            DonText="Compétences professionnelles et technicité" V_SiEnLectureSeule="true"
                            DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Center"
                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                        <Virtualia:VSixBoutonRadio ID="RadioHA03" runat="server" V_Groupe="Critere1"
                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                            VRadioN1Width="57px" VRadioN2Width="78px" VRadioN3Width="90px"
                            VRadioN4Width="57px" VRadioN5Width="55px" VRadioN6Width="54px"
                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                            VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 55px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                            Visible="true" VRadioN1visible="false" VRadioN6visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"
                    BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHB01" runat="server"
                            V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="6"
                            Etivisible="False" DonWidth="370px" DonHeight="20px" 
                            DonText="Contribution à l'activité du service" V_SiEnLectureSeule="true"
                            DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Center"
                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                        <Virtualia:VSixBoutonRadio ID="RadioHB03" runat="server" V_Groupe="Critere2"
                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                            VRadioN1Width="57px" VRadioN2Width="78px" VRadioN3Width="90px"
                            VRadioN4Width="57px" VRadioN5Width="55px" VRadioN6Width="54px"
                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                            VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 55px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                            Visible="true" VRadioN1visible="false" VRadioN6visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"
                    BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHC01" runat="server"
                            V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="7"
                            Etivisible="False" DonWidth="370px" DonHeight="20px" 
                            DonText="Capacités professionnelles et relationnelles" V_SiEnLectureSeule="true"
                            DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Center"
                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                        <Virtualia:VSixBoutonRadio ID="RadioHC03" runat="server" V_Groupe="Critere3"
                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                            VRadioN1Width="57px" VRadioN2Width="78px" VRadioN3Width="90px"
                            VRadioN4Width="57px" VRadioN5Width="55px" VRadioN6Width="54px"
                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                            VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 55px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                            Visible="true" VRadioN1visible="false" VRadioN6visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"
                    BorderStyle="None" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHD01" runat="server"
                            V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="8"
                            Etivisible="False" DonWidth="370px" DonHeight="20px" 
                            DonText="Aptitude à l'encadrement et/ou à la conduite de projets" V_SiEnLectureSeule="true"
                            DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Center"
                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="#9EB0AC" BackColor="Transparent">        
                        <Virtualia:VSixBoutonRadio ID="RadioHD03" runat="server" V_Groupe="Critere4"
                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="true"
                            VRadioN1Width="52px" VRadioN2Width="78px" VRadioN3Width="90px"
                            VRadioN4Width="57px" VRadioN5Width="55px" VRadioN6Width="54px"
                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                            VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text="" 
                            Visible="true" VRadioN6visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero322" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille322" runat="server" Text="Réalisation des objectifs de l'année écoulée (cf. paragraphe 2.1)" Height="35px" Width="748px"
                           CssClass="EP_TitreChapitre"
                           style="padding-top: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="9" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero323" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelFamille323" runat="server" Text="Appréciation littérale" Height="35px" Width="748px"
                           CssClass="EP_TitreChapitre"
                           style="padding-top: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="250px" EtiHeight="20px" DonTabIndex="10" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="30px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreAvancement" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiquetteAvancement" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            cssclass="EP_TitreCR">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletAvancement" runat="server" Text="PROPOSITION D'UNE REDUCTION OU MAJORATION D'ANCIENNETE D'ECHELON" Height="20px" Width="746px"
                            CssClass="EP_TitreOnglet">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteAvancement" runat="server" Text="" Height="25px" Width="746px"
                            CssClass="EP_TitreChapitre"
                            style="padding-top: 5px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelGradeEchelon" runat="server" Text="" Height="20px" Width="746px"
                            CssClass="EP_TitreChapitre" Font-Size="Small" Style="margin-top:0px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelDatePromoEchelon" runat="server" Text="" Height="20px" Width="746px"
                            CssClass="EP_TitreChapitre" Font-Size="Small" Style="margin-top:0px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadrePropositionAvancement" runat="server" Height="150px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelInfosAvancement" runat="server" Height="60px" Width="748px"
                        CssClass="EP_EtiComplement"
                        Text="<B>Rappel :</B> Il est demandé aux supérieurs hiérarchiques directs d'émettre un avis, favorable ou non, à l'octroi d’une réduction d'ancienneté d'échelon (1 ou 2 mois), sans toutefois préciser de souhait particulier quant au nombre de mois qu'il conviendrait d'octroyer à l'agent ayant bénéficié d'un entretien professionnel.">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                      <asp:Label ID="LabelAvertissement" runat="server" Height="25px" Width="748px"
                        CssClass="EP_EtiComplement"
                        Text="A renseigner uniquement pour les personnels titulaires ASS et ITRF.">
                      </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="17px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelAvisHierarchique" runat="server" Text="Avis du supérieur hiérarchique direct ayant conduit l’entretien professionnel" Height="30px" Width="748px"
                        CssClass="EP_TitreChapitre"
                        style="padding-top: 5px;">
                     </asp:Label>          
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelInfoAvis" runat="server" Height="30px" Width="746px"
                           CssClass="EP_EtiComplement"
                           Text="<B>Rappel :</B> la formulation de l'avis doit être en cohérence avec le compte rendu de l'entretien professionnel.">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VSixVerticalRadio ID="RadioVA04" runat="server" V_Groupe="AvisHierarchique"
                            V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                            VRadioN1Width="550px" VRadioN2Width="550px" VRadioN3Width="550px"
                            VRadioN4Width="550px" VRadioN5Width="550px" VRadioN6Width="550px"
                            VRadioN1Height="33px" VRadioN2Height="33px" VRadioN3Height="33px"
                            VRadioN4Height="33px" VRadioN5Height="33px" VRadioN6Height="33px"
                            VRadioN1Style="margin-left: 0px; margin-top: 1px; Font-size: Medium; padding-top: 7px" VRadioN2Style="margin-left: 0px; margin-top: 1px; Font-size: Medium; padding-top: 7px" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="Favorable à l'attribution d'une réduction d'ancienneté" 
					        VRadioN2Text="Défavorable à l'attribution d'une réduction d'ancienneté" 
					        VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                            VRadioN3Visible="false" VRadioN4Visible="false"
                            VRadioN5Visible="false" VRadioN6Visible="false"  
                            Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelAvisDefavorable" runat="server" Height="30px" Width="746px"
                           CssClass="EP_EtiComplement"
                           Text="Si votre avis est défavorable, souhaitez-vous que soit attribuée une majoration d'ancienneté :"
                           Font-Size="Medium">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VSixVerticalRadio ID="RadioVB04" runat="server" V_Groupe="SiAvisDefavorable"
                            V_PointdeVue="1" V_Objet="157" V_Information="4" V_SiDonneeDico="true"
                            VRadioN1Width="80px" VRadioN2Width="80px" VRadioN3Width="80px"
                            VRadioN4Width="80px" VRadioN5Width="80px" VRadioN6Width="80px"
                            VRadioN1Height="26px" VRadioN2Height="26px" VRadioN3Height="26px"
                            VRadioN4Height="26px" VRadioN5Height="26px" VRadioN6Height="26px"
                            VRadioN1Style="margin-left: 0px; margin-top: 1px; Font-size: Medium; padding-top: 4px" VRadioN2Style="margin-left: 0px; margin-top: 1px; Font-size: Medium; padding-top: 4px" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="Oui" 
					        VRadioN2Text="Non" 
					        VRadioN3Text="" VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                            VRadioN3Visible="false" VRadioN4Visible="false"
                            VRadioN5Visible="false" VRadioN6Visible="false"  
                            Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVS03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="140px" EtiHeight="40px" DonTabIndex="11" 
                           Etitext="<B>Rappel :</B> en cas de demande de majoration, il conviendra d'une part de motiver cette demande et d'autre part d'en informer préalablement l'agent." 
                           Etivisible="true"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>