﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_RESULTATS_CESE.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_RESULTATS_CESE" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderColor="#B0E0D7" 
    BorderStyle="None" BorderWidth="2px" HorizontalAlign="Center" 
    style="margin-top: 1px;" Visible="true" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreResultats" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="BILAN DE L'ANNEE PASSEE" Height="20px" Width="746px"
                            BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Tooltip="Les trois champs à renseigner sont illustrés par un certain nombre de critères qui ont pour objet de guider l'évaluation mais l'expression est libre"                     
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="35px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
			               Text="Evaluation des réalisations au regard des objectifs formulés et des contraintes exogènes.
                           Compétences et aptitudes de l'agent."
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 12px;
                           font-style: oblique; text-indent: 2px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="OBJECTIFS POUR L'ANNEE ECOULEE"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 3px; padding-top: 7px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVF03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="150px" EtiHeight="30px" DonTabIndex="1" 
                           Etitext="Bilan global des résultats atteints"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 4px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteObjectif" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteObjectif1" runat="server" Height="60px" Width="241px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Rappel de l'objectif"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteRealisation" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                                <asp:Label ID="LabelEnteteRealisation" runat="server" Height="29px" Width="220px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Réalisation"
                                   Tooltip="Au choix l'objectif sera défini comme étant atteint, partiellement atteint, non atteint, devenu sans objet"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteComplete" runat="server" Height="30px" Width="49px"
                                   BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="objectif atteint"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px; 
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEntetePartielle" runat="server" Height="30px" Width="64px"
                                   BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="partiellement atteint"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px; 
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteInsuffisante" runat="server" Height="30px" Width="49px"
                                   BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="non atteint"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteSansObjet" runat="server" Height="30px" Width="53px"
                                   BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px" 
                                   Text="devenu sans objet"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                                   style="margin-top: 0px; margin-left: -1px; margin-bottom: -1px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableEnteteDifficultes" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="LabelEnteteDifficultes" runat="server" Height="60px" Width="265px"
                                   BackColor="#D7FAF3" BorderStyle="None" 
                                   Text="Analyse des résultats et enseignements tirés"
                                   Tooltip="Ce qui a facilité ou freiné les résultats (difficultés rencontrées par l'agent)"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif1" runat="server" Height="25px" Width="251px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif individuel n°1"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                   style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="246px" DonHeight="140px" DonTabIndex="2"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHA04" runat="server" V_Groupe="Realisation1"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHA05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="170px" DonTabIndex="3" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 8px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes1" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVA03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="270px" DonHeight="140px" DonTabIndex="4"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif2" runat="server" Height="25px" Width="251px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif individuel n°2"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                   style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="246px" DonHeight="140px" DonTabIndex="5"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHB04" runat="server" V_Groupe="Realisation2"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHB05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="170px" DonTabIndex="6" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 8px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes2" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVB03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="270px" DonHeight="140px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif3" runat="server" Height="25px" Width="251px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif individuel n°3"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                   style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="246px" DonHeight="140px" DonTabIndex="8"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHC04" runat="server" V_Groupe="Realisation3"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHC05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="170px" DonTabIndex="9" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 8px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes3" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="270px" DonHeight="140px" DonTabIndex="10"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="TableNumerobjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="left">
                                <asp:Label ID="LabelNumerobjectif4" runat="server" Height="25px" Width="251px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Objectif individuel n°4"
                                   ForeColor="#124545" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                   style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="251px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="246px" DonHeight="140px" DonTabIndex="11"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="222px"
                        BackColor="Transparent" BorderStyle="None">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VSixBoutonRadio ID="RadioHD04" runat="server" V_Groupe="Realisation4"
                                     V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="true"
                                     VRadioN1Width="45px" VRadioN2Width="52px" VRadioN3Width="45px"
                                     VRadioN4Width="49px" VRadioN5Width="45px" VRadioN6Width="45px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 5px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN5Visible="false" VRadioN6Visible="false"  
                                     Visible="true"/>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHD05" runat="server"
                                   V_PointdeVue="1" V_Objet="151" V_Information="5" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="170px" DonTabIndex="12" V_SiEnLectureSeule="True"
                                   Donstyle="margin-left: 8px; text-align: center;"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="TableDifficultes4" runat="server" CellPadding="0" CellSpacing="0" Width="275px"
                        BackColor="#D7FAF3" BorderColor="#98D4CA" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="true"
                                   EtiVisible="false" DonWidth="270px" DonHeight="140px" DonTabIndex="13"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero3" runat="server" Height="33px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text="Autres travaux sur lesquels l'agent s'est investi en cours d'année"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px; padding-top: 7px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVG03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="170px" EtiHeight="30px" DonTabIndex="14" 
                           Etitext="" EtiVisible="false"
                           Donstyle="margin-left: 3px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
		        <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVH03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="740px" DonWidth="740px" DonHeight="150px" EtiHeight="30px" DonTabIndex="15" 
                           Etitext="Analyse des résultats et enseignements tirés"
                           Donstyle="margin-left: 3px;"
                           Etistyle="margin-left: 3px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
</asp:Table>