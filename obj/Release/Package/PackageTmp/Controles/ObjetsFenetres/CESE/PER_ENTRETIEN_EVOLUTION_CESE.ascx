﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_EVOLUTION_CESE.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_EVOLUTION_CESE" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreEvolution" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="EVOLUTION PROFESSIONNELLE DE L'AGENT" Height="20px" Width="746px"
                            BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero11" runat="server" Height="34px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="1. Proposition d'attribution d'une bonification d'ancienneté"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 6px;
                           font-style: normal; text-indent: 20px; text-align: Left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero12" runat="server" Height="18px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent"  BorderStyle="None" 
                           Text="Pour rappel :"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero13" runat="server" Height="70px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent"  BorderStyle="None" 
                           Text="Tout agent ayant obtenu une promotion par examen professionnel ou au choix ne peut obtenir
                           une réduction d'ancienneté au titre de la même année. Les agents ayant obtenu une réduction de 3 mois
                           en année N ne peuvent obtenir en N+1 une réduction identique. L'absence de réduction d'ancienneté pendant 
                           plusieurs années consécutives doit être justifiée par l'administration et par le supérieur hiérarchique 
                           auprès de son collaborateur, notamment si les appréciations dans le cadre des entretiens d'évaluations 
                           ne font pas apparaître de difficulté particulière."
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero14" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent"  BorderStyle="None" 
                           Text="Concernant les administrateurs, conformément aux dispositions réglementaires, 
                           aucune réduction d'ancienneté ne peut être attribuée (article 10 du décret du 16 novembre 1999 et 
                           articles 7 à 11 du décret n° 2010-888 du 28 juillet 2010)."
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero15" runat="server" Height="20px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Evolution conduisant à un avancement normal"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVJ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="60px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero16" runat="server" Height="20px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Evolution conduisant à un avancement accéléré"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero17" runat="server" Height="25px" Width="740px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
			               Text="L'évaluateur peut indiquer l'amplitude de la réduction d'ancienneté souhaitée, 
                           soit un mois, soit trois mois (sous réserve de l'harmonisation)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="true"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 8px; margin-bottom: 0px; padding-top: 4px;
                           font-style: oblique; text-indent: 2px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVK03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="60px" EtiHeight="20px" DonTabIndex="2" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero18" runat="server" Height="20px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Dernières réductions d'ancienneté et promotions obtenues"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVL03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="60px" EtiHeight="20px" DonTabIndex="3" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero21" runat="server" Height="34px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="2. Proposition d'attribution d'une augmentation de la part variable de la prime de rendement"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 6px;
                           font-style: normal; text-indent: 20px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero22" runat="server" Height="18px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent"  BorderStyle="None" 
                           Text="Pour rappel :"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero23" runat="server" Height="40px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent"  BorderStyle="None" 
                           Text="«le montant individualisé de la part variable lié à l'engagement professionnel de l'agent et à l'atteinte 
			               des objectifs annuels est déterminée au regard notamment du compte-rendu de l'entretien d'évaluation.
			               Par rapport à l'année N-1, la part variable ne peut diminuer ou augmenter de plus de 10 % sauf en cas de changement de corps et / ou de fonction.»"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero24" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent"  BorderStyle="None" 
                           Text="(Référentiel de gestion des ressources humaines, arrêté n°14-06 du 5 février 2014)"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="90%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHA01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="4"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Proposition d'une augmentation de la part variable" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="RadioHA04" runat="server" V_Groupe="Critere1"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero26" runat="server" Height="40px" Width="370px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Dernières augmentations de la part variable obtenues"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVM03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="361px" DonHeight="40px" EtiHeight="20px" DonTabIndex="4" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero31" runat="server" Height="34px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="3. Un changement d’affectation est-il :"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 6px;
                           font-style: normal; text-indent: 20px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHB01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="5"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Sans objet pour le moment" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="RadioHB04" runat="server" V_Groupe="Critere2"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHC01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="6"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Souhaitable à moyen terme" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="RadioHC04" runat="server" V_Groupe="Critere3"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHD01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="7"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="A envisager" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="RadioHD04" runat="server" V_Groupe="Critere4"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHE01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="8"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="A été réalisé dans l'année" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="RadioHE04" runat="server" V_Groupe="Critere5"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>              
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero41" runat="server" Height="34px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="4. Une promotion de grade au choix, sous réserve des conditions statutaires, est-elle :"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 6px;
                           font-style: normal; text-indent: 20px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHF01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="9"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Pas encore à l'ordre du jour" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="RadioHF04" runat="server" V_Groupe="Critere6"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHG01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="10"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Envisageable à moyen terme" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="RadioHG04" runat="server" V_Groupe="Critere7"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHH01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="11"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Recommandée fortement par l'évaluateur" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="RadioHH04" runat="server" V_Groupe="Critere8"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>        
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="LabelTitreNumero51" runat="server" Height="34px" Width="748px"
                           BackColor="#8DA8A3" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="5. Une promotion de corps au choix, sous réserve des conditions statutaires, est-elle :"
                           BorderWidth="1px" ForeColor="White" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 6px;
                           font-style: normal; text-indent: 20px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHI01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="12"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Pas encore à l'ordre du jour" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="RadioHI04" runat="server" V_Groupe="Critere9"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHJ01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="13"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Envisageable à moyen terme" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="RadioHJ04" runat="server" V_Groupe="Critere10"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHK01" runat="server"
                           V_PointdeVue="1" V_Objet="156" V_Information="1" V_SiDonneeDico="true" DonTabIndex="14"
                           Etivisible="False" DonWidth="368px" DonHeight="20px" DonBorderWidth="1px" 
                           DonText="Recommandée fortement par l'évaluateur" V_SiEnLectureSeule="true"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 2px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="RadioHK04" runat="server" V_Groupe="Critere11"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="true"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>        
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>