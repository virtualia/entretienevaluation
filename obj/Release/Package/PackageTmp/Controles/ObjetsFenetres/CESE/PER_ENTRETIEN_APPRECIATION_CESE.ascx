﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_APPRECIATION_CESE.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_APPRECIATION_CESE" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreAppreciation" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVolet" runat="server" Text="APPRECIATION GENERALE DE L'EVALUATEUR" Height="20px" Width="746px"
                            BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentite" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreAppreciationEvaluateur" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreEvaluateur" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" Text="Préciser les qualités et aptitudes personnelles de l'agent"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 2px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="True"
                           EtiWidth="740px" DonWidth="740px" DonHeight="250px" EtiHeight="60px" DonTabIndex="1" 
                           Etitext="(Sens des responsabilités, fiabilité, conscience professionnelle, aptitudes managériales, 
                           aptitudes organisationnelles, esprit d'initiative, capacité d'adaptation à l'évolution du poste ou 
                           de son environnement, aptitude à l'acquisition de nouvelles connaissances, présentation générale, 
                           ponctualité et assiduité…)" 
                           Etivisible="True"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreTitreExpressionAgent" runat="server" Height="95px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEtiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelVoletExpression" runat="server" Text="EXPRESSION DE L'AGENT" Height="20px" Width="746px"
                            BackColor= "Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 4px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelIdentiteAgent" runat="server" Height="30px" Width="746px"
                            BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="NotSet" Text=""
                            BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreExpressionAgent" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero1" runat="server" Height="45px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="La signature du document atteste que l'agent en a pris connaissance, pas qu'il en approuve le sens. 
                           Dans un premier temps, si vous ne partagez pas l'appréciation portée par votre évaluateur, 
                           vous pouvez l'indiquer sur cette page."
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 2px; text-align: Left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" 
                           Text="Souhaitez-vous apporter des précisions à la description qui a été faite de vos fonctions ? Si oui lesquelles ?"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVQ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="2" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero3" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" 
                           Text="Partagez-vous l'évaluation générale vous concernant ? Sinon pourquoi ?"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVR03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="3" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero4" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" 
                           Text="Vous sentez-vous à l'aise dans votre service ? Si non pourquoi ?"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVS03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="4" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero5" runat="server" Height="40px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" 
                           Text="Quels sont vos souhaits d'évolution professionnelle ? Evolution sur le poste, 
                           prises de responsabilités nouvelles, projet professionnel, avancement."
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVT03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="5" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelTitreNumero6" runat="server" Height="30px" Width="748px"
                           BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" 
                           Text="Autres observations"
                           BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVU03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="6" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
 </asp:Table>