﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaControle_VListBoxEtCoches" Codebehind="VListBoxEtCoches.ascx.vb" %>

<asp:Table ID="ListBoxVirtualia" runat="server" CellPadding="1" CellSpacing="0" > 
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Etiquette" runat="server" Height="20px" Width="380px"
                    BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#142425"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="True"
                    style="margin-top: 0px; text-indent: 5px; text-align: center"></asp:Label>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="180px" Width="380px" BackColor="#D7FAF3" BorderStyle="Inset" 
                       BorderWidth="2px" BorderColor="#7EC8BE">
             <asp:CheckBoxList ID="VCheckListBox" runat="server"
                    BackColor="Transparent" AutoPostBack="true" ForeColor="#142425" 
                    BorderStyle="None" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" 
                    TextAlign="Right" CellPadding="2" CellSpacing="2" RepeatLayout="Flow">
             </asp:CheckBoxList>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>