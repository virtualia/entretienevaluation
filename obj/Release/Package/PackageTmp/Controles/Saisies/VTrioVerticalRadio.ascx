﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaControle_VTrioVerticalRadio" Codebehind="VTrioVerticalRadio.ascx.vb" %>

    <asp:table ID="CadreOption" runat="server" height="60px" CellPadding="1" CellSpacing="0">
         <asp:TableRow>
             <asp:TableCell>
                 <asp:RadioButton ID="RadioN0" runat="server" Text="Option N1" Visible="true"
                      AutoPostBack="false" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                      BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                      Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                      style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                      text-indent: 5px; text-align: center; vertical-align: middle" />
             </asp:TableCell>
         </asp:TableRow>
         <asp:TableRow>
             <asp:TableCell>
                 <asp:RadioButton ID="RadioN1" runat="server" Text="Option N2" 
                      AutoPostBack="false" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                      BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                      Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                      style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                      text-indent: 5px; text-align: center; vertical-align: middle" />
            </asp:TableCell>
         </asp:TableRow>
         <asp:TableRow>
             <asp:TableCell>
                 <asp:RadioButton ID="RadioN2" runat="server" Text="Option N3" 
                      AutoPostBack="false" GroupName="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                      BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                      Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                      style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                      text-indent: 5px; text-align: center; vertical-align: middle" />
             </asp:TableCell>
        </asp:TableRow>
    </asp:table>