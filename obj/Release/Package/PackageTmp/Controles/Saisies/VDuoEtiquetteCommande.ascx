﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaControle_VDuoEtiquetteCommande" Codebehind="VDuoEtiquetteCommande.ascx.vb" %>

<asp:Table ID="VStandard" runat="server" CellPadding="1" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Etiquette" runat="server" Height="20px" Width="170px"
                    BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left" >
            </asp:Label>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Button ID="DonneeTable" runat="server" Height="22px" Width="300px" 
                    BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset"
                    BorderWidth="2px" ForeColor="Black" Visible="true"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                    style="padding-left: 1px; margin-top: 2px; text-align: left" > 
            </asp:Button>
        </asp:TableCell>
    </asp:TableRow>
  </asp:Table>
