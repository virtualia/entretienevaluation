﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaControle_VCoupleVerticalEtiDonnee" CodeBehind="VCoupleVerticalEtiDonnee.ascx.vb" %>

    <asp:Table ID="CadreEtiData" runat="server" CellPadding="0" CellSpacing="0">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="Etiquette" runat="server" Height="20px" Width="170px"
                    BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    Style="margin-top: 0px; text-indent: 5px; text-align: left">
                </asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:TextBox ID="Donnee" runat="server" Height="170px" Width="170px" Maxlength="0"
                    BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset"
                    BorderWidth="2px" ForeColor="Black" Visible="true"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                    Style="margin-top: 1px; text-indent: 1px; text-align: left" AutoPostBack="false"> 
                </asp:TextBox>
                <asp:Label ID="LabelCounter" runat="server" Height="20px" Width="170px"
                     ForeColor="#142425" Font-Italic="true"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="x-small"
                    Style="margin-top: 0px; text-indent: 5px; text-align: left">
                </asp:Label>
                <asp:RegularExpressionValidator ID="RegExpArea"
                    ControlToValidate="Donnee" Enabled="false"
                    ValidationExpression="[\s\S]{0,500}"
                    Display="Dynamic" ForeColor="Red"
                    ErrorMessage="Longueur maximum atteinte"
                    runat="server">
                </asp:RegularExpressionValidator>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

