﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P6_CESE2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P6_CESE2" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="VI_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreTitreAppreciation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelVolet" runat="server" Text="APPRECIATION GENERALE DE L'EVALUATEUR" Height="20px" Width="748px"
                            CssClass="EP_Imp_MediumBold"
                            style="margin-top: 1px; margin-bottom: 10px">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelVoletSuite" runat="server" Height="20px" Width="748px"
                            Text="Préciser les qualités et aptitudes personnelles de l'agent"
                            CssClass="EP_Imp_SmallItalic"
                            style="margin-top: 2px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="350px" EtiHeight="80px" DonTabIndex="1" 
                           Etitext="(Sens des responsabilités, fiabilité, conscience professionnelle, aptitudes managériales, 
                           aptitudes organisationnelles, esprit d'initiative, capacité d'adaptation à l'évolution du poste ou 
                           de son environnement, aptitude à l'acquisition de nouvelles connaissances, présentation générale, 
                           ponctualité et assiduité…)" 
                           Etivisible="True" EtiBackColor="Transparent" EtiBorderColor="Transparent" EtiBorderStyle="None"
                           Donstyle="margin-left: 0px;"
                           Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Left" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage11" runat="server" Height="15px" Width="740px"
                BackColor="Transparent" BorderStyle="None"
                Text="11 / 13" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:right">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero1bis" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelVoletAgentbis" runat="server" Text="CONDITIONS DE TRAVAIL" Height="20px" Width="748px"
                            CssClass="EP_Imp_MediumBold">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero1ter" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero11" runat="server" Height="20px" Width="748px"
                           Text="Conditions matérielles (à remplir par l'agent)"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVV03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="2" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero31" runat="server" Height="20px" Width="748px"
                           Text="Environnement de travail (à remplir par l'agent)"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVW03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="3" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero41" runat="server" Height="20px" Width="748px"
                           Text="Observations du supérieur hiérarchique"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVX03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="4" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero51" runat="server" Height="20px" Width="748px"
                           Text="Retour d'expérience - Observations formulées en N-1 (à remplir par l'agent)"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVY03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="150px" EtiHeight="20px" DonTabIndex="5" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Left" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage12" runat="server" Height="15px" Width="740px"
                BackColor="Transparent" BorderStyle="None"
                Text="12 / 13" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:right">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelVoletAgent" runat="server" Text="EXPRESSION DE L'AGENT" Height="20px" Width="748px"
                            CssClass="EP_Imp_MediumBold">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelVoletSuiteAgent" runat="server" Height="40px" Width="748px"
                            Text="La signature du document atteste que l'agent en a pris connaissance, pas qu'il en approuve le sens. 
                            Dans un premier temps, si vous ne partagez pas l'appréciation portée par votre évaluateur, 
                            vous pouvez l'indiquer sur cette page."
                            CssClass="EP_Imp_SmallItalic"
                            style="margin-top: 2px">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero2" runat="server" Height="20px" Width="748px"
                           Text="Souhaitez-vous apporter des précisions à la description qui a été faite de vos fonctions ? Si oui lesquelles ?"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVQ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="135px" EtiHeight="20px" DonTabIndex="2" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero3" runat="server" Height="20px" Width="748px"
                           Text="Partagez-vous l'évaluation générale vous concernant ? Sinon pourquoi ?"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVR03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="135px" EtiHeight="20px" DonTabIndex="3" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero4" runat="server" Height="20px" Width="748px"
                           Text="Vous sentez-vous à l'aise dans votre service ? Si non pourquoi ?"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVS03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="135px" EtiHeight="20px" DonTabIndex="4" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero5" runat="server" Height="35px" Width="748px"
                           Text="Quels sont vos souhaits d'évolution professionnelle ? Evolution sur le poste, 
                           prises de responsabilités nouvelles, projet professionnel, avancement."
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVT03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="135px" EtiHeight="20px" DonTabIndex="5" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero6" runat="server" Height="20px" Width="748px"
                           Text="Autres observations"
                           CssClass="EP_Imp_SmallBold"
                           style="margin-bottom: 4px; padding-top: 4px; text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVU03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="746px" DonHeight="130px" EtiHeight="20px" DonTabIndex="6" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreSignatures" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignature1" runat="server" Height="16px" Width="500px" Text="A"
                           CssClass="EP_Imp_SmallBold"
                           style="text-align: right">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignature2" runat="server" Height="16px" Width="248px" Text="le"
                           CssClass="EP_Imp_SmallBold"
                           style="text-align: center">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignature3" runat="server" Height="35px" Width="500px"
                           Text="Signature de l'évaluateur <br/> ayant conduit l'entretien"
                           CssClass="EP_Imp_SmallBold"
                           style="text-align: center">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelSignature4" runat="server" Height="35px" Width="248px"
                           Text="Signature de l'agent"
                           CssClass="EP_Imp_SmallBold"
                           style="text-align: center">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoH015003" runat="server" Height="17px" Width="500px" Text=""
                            CssClass="EP_Imp_SmallNormal"
                            style="text-align: center">
                        </asp:Label>          
                    </asp:TableCell>       
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_InfoH015004" runat="server" Height="17px" Width="248px" Text=""
                            CssClass="EP_Imp_SmallNormal"
                            style="text-align: center">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="VI_LabelSignature5" runat="server" Height="20px" Width="500px"
                           Text="Visa du Directeur des ressources humaines"
                           CssClass="EP_Imp_SmallBold"
                           style="text-indent: 10px">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="20px" HorizontalAlign="Left" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage13" runat="server" Height="15px" Width="740px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="13 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:right" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>