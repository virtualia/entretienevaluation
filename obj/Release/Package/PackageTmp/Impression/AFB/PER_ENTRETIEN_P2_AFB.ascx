﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P2_AFB.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P2_AFB" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="II_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreTitreCompetence" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVolet" runat="server" Text="2 - APPRECIATION DES COMPETENCES DE L'AGENT" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="Transparent" BorderStyle="None"
                            BorderWidth="1px" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            style="margin-top: 1px; margin-left: 1px; margin-bottom: 10px;
                            font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformationsNiveaux" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                           Text="Niveaux"
                           BorderWidth="1px" ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 2px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformationsNiveau1" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                           Text="<B>Initié: </B>  <small> Connaissance élémentaires, notions. Capacité à faire mais en étant tutoré. </small>"
                           BorderWidth="1px" ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformationsNiveau2" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                           Text="<B>Pratique : </B>  <small> Connaissances générales - Capacité à traiter de façon autonome les situations courantes.</small>"
                           BorderWidth="1px" ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformationsNiveau3" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                           Text="<B>Maîtrise : </B>  <small> Connaissances approfondies - Capacité à traiter de façon autonome les situations complexes ou inhabituelles.</small>"
                           BorderWidth="1px" ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformationsNiveau4" runat="server" Height="40px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                           Text="<B>Expert : </B>  <small> Au sens de 'Fait référence dans le domaine'. 'Est capable de le faire évoluer', 'Capacité à former et/ou à être tuteur' : ne renvoie pas aux certifications attribuées par les ministère dans l'exercice de certaines fonctions spécifiques, notammment par les commités de domaine du Ministère.</small>"
                           BorderWidth="1px" ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
<%--                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformationsNiveau5" runat="server" Height="20px" Width="746px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="none"
                           Text="Niveau 5 : sans objet"
                           BorderWidth="1px" ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 50px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>--%>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelFamille1" runat="server" Height="30px" Width="750px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Compétences professionnelles"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences311" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="II_LabelTitreCompetence1" runat="server" Height="20px" Width="437px"
                                       BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels1" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote11" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote12" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote13" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote14" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHA15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Connaissances du poste"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHA07" runat="server" V_Groupe="Critere111"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="65px" VRadioN2Width="65px" VRadioN3Width="65px"
                                     VRadioN4Width="65px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN5visible="false" VRadioN6visible="false"  
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="II_TableCommentaire1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVA11" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="35px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                                                       <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="Label21" runat="server" Height="20px" Width="437px"
                                       BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="Table5" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label22" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label23" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label24" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label25" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHB15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Connaissances de l'environnement professionnel"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHB07" runat="server" V_Groupe="Critere112"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="65px" VRadioN2Width="65px" VRadioN3Width="65px"
                                     VRadioN4Width="65px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN5visible="false" VRadioN6visible="false"  
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                                            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table11" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVB11" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="35px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                                                       <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="Label26" runat="server" Height="20px" Width="437px"
                                       BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="Table6" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label27" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label28" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label29" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label30" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHC15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Qualités rédactionnelles"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHC07" runat="server" V_Groupe="Critere113"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="65px" VRadioN2Width="65px" VRadioN3Width="65px"
                                     VRadioN4Width="65px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN5visible="false" VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                                            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table12" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVC11" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="35px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                                                       <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="Label31" runat="server" Height="20px" Width="437px"
                                       BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="Table7" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label32" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label33" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label34" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label35" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHD15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Qualités relationnelles"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHD07" runat="server" V_Groupe="Critere114"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="65px" VRadioN2Width="65px" VRadioN3Width="65px"
                                     VRadioN4Width="65px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN5visible="false" VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                                            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table13" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVD11" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="35px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                                                       <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="Label36" runat="server" Height="20px" Width="437px"
                                       BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="Table8" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label37" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label38" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label39" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label40" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHE15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Qualités d'expression orale"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHE07" runat="server" V_Groupe="Critere115"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="65px" VRadioN2Width="65px" VRadioN3Width="65px"
                                     VRadioN4Width="65px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN5visible="false" VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                                            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table14" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVE11" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="35px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                                                       <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="Label41" runat="server" Height="20px" Width="437px"
                                       BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="Table9" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label42" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label43" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label44" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label45" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHF15301" runat="server" Height="25px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Capacité d'adaptation aux évolutions techniques et professionnelles"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHF07" runat="server" V_Groupe="Critere116"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="65px" VRadioN2Width="65px" VRadioN3Width="65px"
                                     VRadioN4Width="65px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                                     VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN5visible="false" VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                                            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table15" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVF11" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="35px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                          <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                                                       <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="Label46" runat="server" Height="20px" Width="437px"
                                       BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="Table10" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label47" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label48" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label49" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label50" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHG15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Capacité à assurer le suivi des dossiers"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHG07" runat="server" V_Groupe="Critere121"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="65px" VRadioN2Width="65px" VRadioN3Width="65px"
                                     VRadioN4Width="65px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN5visible="false" VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                                            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table16" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVG11" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="35px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="Label51" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="6 / 18" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
                    <asp:TableRow>
                    <asp:TableCell >
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelFamille2" runat="server" Height="30px" Width="750px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Agents en situation de management"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreCompetences321" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="II_LabelTitreCompetence2" runat="server" Height="20px" Width="437px"
                                       BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="II_CadreEnteteLabels2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote21" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote22" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote23" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="II_LabelTitreNote24" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHH15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Capacité à déléguer"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHH07" runat="server" V_Groupe="Critere117"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="65px" VRadioN2Width="65px" VRadioN3Width="65px"
                                     VRadioN4Width="65px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN5visible="false" VRadioN6visible="false"  
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                                            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table17" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVH11" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                                                       <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="Label1" runat="server" Height="20px" Width="437px"
                                       BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label2" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label3" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label4" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label5" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHI15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Capacité à assurer le suivi des dossiers"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHI07" runat="server" V_Groupe="Critere118"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="65px" VRadioN2Width="65px" VRadioN3Width="65px"
                                     VRadioN4Width="65px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN5visible="false" VRadioN6visible="false"  
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                                            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table18" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVI11" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                                                       <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="Label6" runat="server" Height="20px" Width="437px"
                                       BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="Table2" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label7" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label8" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label9" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label10" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHJ15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Aptitude à former des collaborateurs"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHJ07" runat="server" V_Groupe="Critere122"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="65px" VRadioN2Width="65px" VRadioN3Width="65px"
                                     VRadioN4Width="65px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN5visible="false" VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                                            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table19" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVJ11" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                             <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                                                       <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="Label11" runat="server" Height="20px" Width="437px"
                                       BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="Table3" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label12" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label13" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label14" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label15" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHK15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Aptitude à la prise de décision"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHK07" runat="server" V_Groupe="Critere119"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="65px" VRadioN2Width="65px" VRadioN3Width="65px"
                                     VRadioN4Width="65px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN5visible="false" VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                                            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table20" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVK11" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                                                       <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center">
                                   <asp:Label ID="Label16" runat="server" Height="20px" Width="437px"
                                       BackColor="#DFF2FF" BorderColor="Black" BorderStyle="Notset" Text="Compétences"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Center">
                                  <asp:Table ID="Table4" runat="server" CellPadding="0" CellSpacing="0" BorderStyle="None">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label17" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Initié"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label18" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Pratique"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label19" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Maîtrise"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">
                                           <asp:Label ID="Label20" runat="server" Height="20px" Width="76px"
                                               BackColor="#DFF2FF" BorderColor="Black" BorderStyle="NotSet" Text="Expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="II_InfoHL15301" runat="server" Height="20px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Sens de l'organisation d'une équipe"
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Center"
                              BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="II_RadioHL07" runat="server" V_Groupe="Critere120"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="65px" VRadioN2Width="65px" VRadioN3Width="65px"
                                     VRadioN4Width="65px" VRadioN5Width="72px" VRadioN6Width="72px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                     VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"  
                                     VRadioN5visible="false" VRadioN6visible="false" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                                            <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table21" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVL11" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="153" V_Information="11" V_SiDonneeDico="false"
                                        EtiVisible="false" DonWidth="743px" DonHeight="40px" DonTabIndex="3"
                                        DonStyle="margin-left: 1px;"
                                        EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Transparent" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage3" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="7 / 18" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>