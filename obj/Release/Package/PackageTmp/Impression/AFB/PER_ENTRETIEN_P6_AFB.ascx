﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P6_AFB.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P6_AFB" %>

<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>

<div style="page-break-before: always;"></div>

<asp:Table ID="VI_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VI_CadreTitreAppreciation" runat="server" Height="30px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVolet" runat="server" Text="6 - APPRECIATION GLOBALE" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"
                            BorderWidth="1px" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            Style="margin-top: 1px; margin-left: 1px; margin-bottom: 10px; font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VI_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="748px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero11" runat="server" Height="25px" Width="740px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Appréciation générale du supérieur hiérarchique"
                            BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px; font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVBA03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                            EtiWidth="370px" DonWidth="746px" DonHeight="100px" EtiHeight="20px" DonTabIndex="1"
                            EtiText="" EtiVisible="False"
                            EtiBorderStyle="None" EtiBackColor="Transparent"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderWidth="1px" DonBorderColor="Black" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table1" runat="server" Height="30px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label1" runat="server" Text="7 - Circuit de signature <small> (en 4 étapes chronologiques) </small>" Height="20px" Width="746px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"
                            BorderWidth="1px" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            Style="margin-top: 1px; margin-left: 1px; margin-bottom: 10px; font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table6" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="Transparent" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow BackColor="#DFF2FF">
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label6" runat="server" Height="20px" Width="730px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Etape 1 - Signature du responsable hiérarchique direct ayant conduit l'entretien (N+1)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label7" runat="server" Height="20px" Width="330px" 
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Date : "
                            ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
<%--                        <Virtualia:VCoupleEtiDonnee ID="VCoupleEtiDonnee2" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                            EtiVisible="false" DonWidth="400px" DonHeight="20px" DonTabIndex="17"
                            DonStyle="margin-left: 1px;"
                            EtiStyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                            DonBorderStyle="None" DonBorderWidth="1px" DonBorderColor="Black" />--%>
                            <asp:Label ID="VI_InfoH015003" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;">
                                 </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label9" runat="server" Height="20px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Nom, Prénom du résponsable hiérarchique direct (N+1) : "
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                       <asp:Label ID="VI_InfoH015001" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;">
                                 </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label10" runat="server" Height="20px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text=" Signature : "
                            ForeColor="Black" Font-Italic="False"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                       <asp:Label ID="VI_InfoHA15704" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;">
                                                           </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label11" runat="server" Height="20px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Après avoir signé le compte rendu d'entretien, le supérieur hiérarchique direct (N+1) le transmet sans délai à l'agent"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="60%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
                    <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" >
            <asp:Table ID="Table2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="Transparent" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow BackColor="#DFF2FF">
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label2" runat="server" Height="20px" Width="730px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Etape 2 - Observations de l'agent"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label22" runat="server" Height="35px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="A compter de la date de communication du compte rendu d'entretien à l'agent, celu-ci dispose d'un délai de 10 jours ouvrés pour formuler, le cas échéant, des observations. Les observations portées par l'agent n'ont pas valeur de recours. Tout recours doit être rédiger sur un document distinct."
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="60%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label3" runat="server" Height="20px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Date de remise du compte-rendu à l'agent : "
                            ForeColor="Black" Font-Italic="False"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                       <asp:Label ID="VI_InfoH115003" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;"> </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label4" runat="server" Height="40px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Observations éventuelles de l'agent : "
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                       <asp:Label ID="VI_InfoHBB15403" runat="server" Height="40px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;"> </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label5" runat="server" Height="20px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Date :"
                            ForeColor="Black" Font-Italic="False"
                          Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                       <asp:Label ID="VI_InfoHB15704" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;"> </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label27" runat="server" Height="20px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Visa de l'agent : "
                            ForeColor="Black" Font-Italic="False"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                       <asp:Label ID="VI_InfoHC15704" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;"> </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label8" runat="server" Height="30px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Après avoir visé le compte rendu d'entretien, l'agent le rend au supérieur hiérarchique direct (N+1) qui le communique à l'autorité hiérarchique (N+2)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="60%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
                    <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table3" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="Transparent" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow BackColor="#DFF2FF">
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label12" runat="server" Height="20px" Width="730px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Etape 3 - Visa de l'autorité hiérarchique (N+2)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label13" runat="server" Height="20px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Nom, Prénom de l'autorité hiérarchique (N+2) : "
                            ForeColor="Black" Font-Italic="False"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                       <asp:Label ID="VI_InfoHF15704" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;"> </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label14" runat="server" Height="20px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Fonction exercée : "
                            ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                       <asp:Label ID="VI_InfoHG15704" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;"> </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label15" runat="server" Height="40px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text=" Observations éventuelles : "
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                       <asp:Label ID="VI_InfoHBC15403" runat="server" Height="40px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;"> </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label23" runat="server" Height="20px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Date : "
                            ForeColor="Black" Font-Italic="False"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                       <asp:Label ID="VI_InfoH015023" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;"> </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label24" runat="server" Height="20px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Visa de l'autorité hiérarchique (N+2) : "
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                       <asp:Label ID="VI_InfoHI15704" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;"> </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label16" runat="server" Height="30px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Après avoir visé le compte rendu d'entretien, l'autorité hiérarchique (N+2) redonne le compte rendu d'entretien au supérieur hiérarchique direct (N+1) qui le transmet à l'agent pour notification."
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="60%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
                        <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="Transparent" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow BackColor="#DFF2FF">
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label17" runat="server" Height="20px" Width="730px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="Etape 4 - Notification à l'agent"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label25" runat="server" Height="35px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="La signature de l'agent atteste qu'il a pris connaissance du document et ne vaut pas nécessairement approbation de son contenu. </br> le refus de notification par l'agent ne constitue pas une procédure de contestation"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="60%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label18" runat="server" Height="20px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Date : "
                            ForeColor="Black" Font-Italic="False"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                       <asp:Label ID="VI_InfoH015004" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;"> </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:Label ID="Label19" runat="server" Height="20px" Width="330px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Nom, Prénom et signature de l'agent : "
                            ForeColor="Black" Font-Italic="False"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: right;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                                        <asp:Label ID="VI_InfoHJ15704" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 0px; text-align: left;"> </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label21" runat="server" Height="20px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="Après avoir signé le compte rendu d'entretien, l'agent le remet à sa hiérarchie qui le transmet au Département des ressources humaines"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="60%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="1px"></asp:TableCell>
    </asp:TableRow>
                    <asp:TableFooterRow>
                    <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
                        <asp:Label ID="NumeroPage9" runat="server" Height="15px" Width="150px"
                             BackColor="Transparent" BorderStyle="None"
                             Text="17 / 18" ForeColor="Black" Font-Italic="True"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                             style="text-align:left" >
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell >
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
                    <asp:TableRow>
                    <asp:TableCell Height="25px"></asp:TableCell>
                </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table5" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                BackColor="Transparent" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                <asp:TableRow BackColor="Transparent">
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label20" runat="server" Height="20px" Width="730px"
                            BackColor="Transparent" BorderStyle="None"
                            Text="INFORMATIONS SUR LES PROCEDURES DE RECOURS EVENTUELS"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="80%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label26" runat="server" Height="120px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="<u>Recours spécifique à l'entretien professionnel : </u> </br> Le recours spécifique de l'article 6 du décret n° 2010-888 modifié n'est pas exclusif des recours administratifs et contentieux de droit commun. </br> L'agent-e dispose donc à tout moment des voies et délais de recours de droit commun dans les conditions suivantes : </br> * recours administratifs par la voie, notammment, d'un recours gracieux adressé au supérieur hiérarchique direct dans un délai de 2 mois à compter de la notification de la décision; </br> * recours contentieux pour excès de pouvoir devant le tribunal administratif du lieu d'affectation dans un délai de deux mois à compter de la notification de la décision, conformément aux articles R. 421-1 et R. 421-5 du code de justice administrative. "
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="60%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" VerticalAlign="Middle">
                        <asp:Label ID="Label30" runat="server" Height="120px" Width="730px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text="<u>Recours de droit commun : </u> </br> L'agent peut saisir l'autorité fiérarchique (N+2) d'une demande de révision de tout ou partie du compte rendu de l'entretien professionnel, dans un délai de 15 jours frans suivant la notification du présent document. </br> L'autorité hiérarchique dispose d'un délai de 15 jours à compter de la saisie pour lui répondre. </br> L'absence de réponse de l'autorité hiérarchique dans les deux mois vaut décision implicite de rejet. </br> La CAP (ou CCP) peut être saisie dans un délai d'un mois à compter de l réponse, implicite ou explicite de l'autorité hiérarchique. L'exercice du recours hiérarchqiue est un préalable obligatoire à la saisie de la commission paritaire compétente."
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="60%"
                            Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 5px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>

            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage8" runat="server" Height="15px" Width="150px"
                BackColor="Transparent" BorderStyle="None"
                Text="18 / 18" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                Style="text-align: left">
            </asp:Label>
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>
