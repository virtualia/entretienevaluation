﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P6_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P6_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>


<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="VI_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VI_CadreTitreAcquis" runat="server" Height="50px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black" BackColor="LightGray" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVoletEnteteAcquis" runat="server" Text="VI - " Height="40px" Width="40px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVoletAcquis" runat="server" Text="LES ACQUIS DE L'EXPERIENCE PROFESSIONNELLE AU COURS DE L'ANNEE DE REFERENCE" Height="45px" Width="700px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VI_CadreTitreAcquisSuite" runat="server" Height="40px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVoletAcquisInformations" runat="server" Height="20px" Width="746px"
                            Text="Il convient de faire mention des compétences et savoir-faire acquis dans l'exercice des fonctions
                            au regard de ceux qui sont requis par le poste."
                            CssClass="EP_Imp_SmallItalic" style="text-indent: 1px; font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero11" runat="server" Height="25px" Width="748px"
                           Text="LES DIFFERENTS SECTEURS D'ACTIVITES EXERCEES :"
                           CssClass="EP_Imp_SmallBold">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoV12" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="12" V_SiDonneeDico="false"
                           DonWidth="745px" DonHeight="190px" DonTabIndex="1" 
                           DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero21" runat="server" Height="20px" Width="748px"
                            Text="LES COMPETENCES ACQUISES :" 
                            CssClass="EP_Imp_SmallBold">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="VI_CadreCompetencesAcquises" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                            BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VI_LabelTitreNumero22" runat="server" Height="25px" Width="748px"
                                       Text="Connaissance de l'institution et de son organisation :"
                                       CssClass="EP_Imp_SmallBold" style="padding-top: 7px; text-indent: 8px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVQ03" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="735px" DonHeight="150px" DonTabIndex="2" 
                                       DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                       DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VI_LabelTitreNumero23" runat="server" Height="25px" Width="748px"
                                       Text="Connaissances professionnelles :"
                                       CssClass="EP_Imp_SmallBold" style="padding-top: 7px; text-indent: 8px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVR03" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="735px" DonHeight="150px" DonTabIndex="3" 
                                       DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                       DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                                </asp:TableCell>
                            </asp:TableRow> 
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="VI_LabelTitreNumero24" runat="server" Height="25px" Width="748px"
                                       Text="Expérience à l'encadrement :"
                                       CssClass="EP_Imp_SmallBold" style="padding-top: 7px; text-indent: 8px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVS03" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="735px" DonHeight="150px" DonTabIndex="4" 
                                       DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                                       DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
        <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VI_SautPage" runat="server">
                <asp:TableFooterRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="VI_CadreBasDePage13" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                            HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_LabelBasdePageNom13" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell> 
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_LabelBasdePagePrenom13" runat="server" Height="25px" Width="150px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_LabelBasdePageCorps13" runat="server" Height="25px" Width="350px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="VI_LabelBasdePageAnnee13" runat="server" Height="25px" Width="30px" Text=""
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="NumeroPage13" runat="server" Height="25px" Width="50px"
                                        Text="p.13/16" 
                                        CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                                    </asp:Label>   
                                </asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="VI_LabelTitreNumero25" runat="server" Height="20px" Width="748px"
                Text="LES FORMATIONS LIEES A L'EMPLOI SUIVIES :" 
                CssClass="EP_Imp_SmallBold">
            </asp:Label>          
        </asp:TableCell>      
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="4px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVT03" runat="server" DonTextMode="true"
                V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                DonWidth="745px" DonHeight="160px" DonTabIndex="5" 
                DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow> 
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VI_CadreTitrePerspectives" runat="server" Height="50px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Bordercolor="Black" BackColor="LightGray" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVoletEntetePerspectives" runat="server" Text="VII - " Height="20px" Width="40px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 5px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelVoletPerspectives" runat="server" Text="PERSPECTIVES D'EVOLUTION PROFESSIONNELLE" Height="20px" Width="700px"
                            CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="VI_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero31" runat="server" Height="25px" Width="746px"
                            Text="1° Capacité à remplir les fonctions d'un grade ou corps supérieur :" 
                            CssClass="EP_Imp_SmallBold">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVU03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="745px" DonHeight="200px" DonTabIndex="6" 
                           DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="VI_LabelTitreNumero32" runat="server" Height="25px" Width="746px"
                            Text="2° Mobilité fonctionnelle ou géographique envisagée :" 
                            CssClass="EP_Imp_SmallBold">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="VI_InfoVV03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           DonWidth="745px" DonHeight="200px" DonTabIndex="7" 
                           DonBorderWidth="1px" EtiVisible="false" DonBorderColor="Black"
                           DonStyle="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: left"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="VI_CadreBasDePage14" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelBasdePageNom14" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelBasdePagePrenom14" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelBasdePageCorps14" runat="server" Height="25px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="VI_LabelBasdePageAnnee14" runat="server" Height="25px" Width="30px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage14" runat="server" Height="25px" Width="50px"
                            Text="p.14/16" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>