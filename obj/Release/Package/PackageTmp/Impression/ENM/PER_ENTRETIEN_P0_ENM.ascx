﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P0_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P0_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<asp:Table ID="PAGE_0" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="O_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreTitreEntretien" runat="server" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="Black">
                        <asp:TableRow>
                            <asp:TableCell Height="3px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="O_Etiquette" runat="server" Text="COMPTE-RENDU DE L'ENTRETIEN PROFESSIONNEL" Height="25px" Width="700px"
                                    CssClass="EP_Imp_LargerBoldSouligne" style="margin-top: 5px; text-align: center;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="O_LabelAnneeEntretien" runat="server" Text="" Height="25px" Width="400px"
                                    CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="O_LabelAnneeEntretien_bis" runat="server"  Height="20px" Width="746px"
                                    Text="Document à remettre à l'agent au moins 10 jours avant la date de l'entretien, accompagné d'un descriptif des fonctions exercées"
                                    CssClass="EP_Imp_SmallItalic" style="margin-top: 5px; text-align: center; font-size: 70%;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="3px"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelCorps" runat="server" Height="20px" Width="150px" Text="Corps :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelCorps" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow><asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGrade" runat="server" Height="20px" Width="150px" Text="Grade :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGrade" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelEchelon" runat="server" Height="20px" Width="150px" Text="Echelon :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelEchelon" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">
                                <asp:Table ID="O_CadreDateEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                 <asp:Label ID="O_EtiLabelPeriodeEntretien" runat="server" Height="20px" Width="250px" Text="Période de référence :"
                                                    CssClass="EP_Imp_MediumNormal" style="text-indent: 1px;">
                                                 </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                 <asp:Label ID="O_LabelPeriodeEntretien" runat="server" Height="20px" Width="550px" Text=""
                                                    CssClass="EP_Imp_MediumNormal">
                                                 </asp:Label>          
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                 <asp:Label ID="O_EtiLabelDateEntretien" runat="server" Height="20px" Width="250px" Text="Date de l'entretien professionnel :"
                                                    CssClass="EP_Imp_MediumNormal" style="text-indent: 1px;">
                                                 </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                 <asp:Label ID="O_InfoH15000" runat="server" Height="20px" Width="550px" Text=""
                                                    CssClass="EP_Imp_MediumNormal">
                                                 </asp:Label>          
                                            </asp:TableCell>
                                        </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelIdentite" runat="server" Height="20px" Width="150px" Text="Nom d'usage :"
                                     CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelIdentite" runat="server" Height="20px" Width="550px" Text=""
                                     CssClass="EP_Imp_MediumNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelPatronyme" runat="server" Height="20px" Width="150px" Text="Nom de famille :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelPatronyme" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_MediumNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelPrenom" runat="server" Height="20px" Width="150px" Text="Prénom :"
                                     CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelPrenom" runat="server" Height="20px" Width="550px" Text=""
                                     CssClass="EP_Imp_MediumNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelNaissance" runat="server" Height="20px" Width="150px" Text="Date de naissance :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelNaissance" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelAdresseAgent" runat="server" Height="20px" Width="150px" Text="Adresse :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelAdresseAgent" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelFonctionAgent" runat="server" Height="20px" Width="150px" Text="Intitulé de la fonction :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelFonctionAgent" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDirection" runat="server" Height="20px" Width="150px" Text="Service / Direction :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15002" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>  
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelEtablissement" runat="server" Height="20px" Width="150px" Text="Etablissement :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelEtablissement" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreManager" runat="server" Height="20px" Width="280px"
                                    Text="Evaluateur"
                                    CssClass="EP_Imp_MediumNormal"
                                    style="margin-top: 12px; margin-bottom: 5px; text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelManager" runat="server" Height="20px" Width="150px" Text="Nom et prénom :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15001" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelFonctionManager" runat="server" Height="20px" Width="150px" Text="Intitulé de la fonction :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelFonctionManager" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>  
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:Table ID="O_CadreEmploi" runat="server" BorderStyle="None" BorderWidth="1px" Visible="true"
                Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="O_LabelTitreSuite" runat="server" Height="20px" Width="750px"
                                       Text="*************"
                                       CssClass="EP_Imp_SmallNormal" style="margin-bottom: 2px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="O_LabelAnciennetePoste" runat="server" Height="20px" Width="200px"
                                       Text="Nombre d'années dans le poste : "
                                       CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; font-style: normal; text-indent: 1px;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_InfoH15016" runat="server" Height="20px" Width="550px" Text=""
                                        CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                     </asp:Label>     
                                </asp:TableCell>
                            </asp:TableRow>  
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                              <asp:TableCell Height="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="O_LabelTitreNumero2" runat="server" Height="20px" Width="742px"
                                       Text="Description des fonctions exercées : "
                                       CssClass="EP_Imp_SmallNormal">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV07" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="7" V_SiDonneeDico="false"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="200px" DonTabIndex="3"
                                       Etitext="" EtiVisible="false"
                                       EtiBorderStyle="None" EtiBackColor="Transparent"
		                               Donstyle="margin-left: 1px;"
		                               Etistyle="margin-left: 1px; text-align: left; text-indent: 1px; font-size:Smaller"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>         
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="O_LabelTitreNumero3" runat="server" Height="20px" Width="742px"
                                       Text="En cas d'absence d'entretien professionnel, préciser les motifs :"
                                       CssClass="EP_Imp_SmallItalic">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoVA03" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="740px" DonHeight="60px" DonTabIndex="4"
                                       Etitext="" EtiVisible="false"
                                       EtiBorderStyle="None" EtiBackColor="Transparent"
		                               Donstyle="margin-left: 1px;"
		                               Etistyle="margin-left: 1px; text-align: left; text-indent: 1px; font-size:Smaller"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
             </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="O_CadreBasDePage1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="O_LabelBasdePageNom1" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="O_LabelBasdePagePrenom1" runat="server" Height="25px" Width="150px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="O_LabelBasdePageCorps1" runat="server" Height="25px" Width="350px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="O_LabelBasdePageAnnee1" runat="server" Height="25px" Width="30px" Text=""
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage1" runat="server" Height="25px" Width="50px"
                            Text="p.1/16" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>