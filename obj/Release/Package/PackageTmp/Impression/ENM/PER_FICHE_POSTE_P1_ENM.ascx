﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_FICHE_POSTE_P1_ENM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_FICHE_POSTE_P1_ENM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

<asp:Table ID="I_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreTitreResultats" runat="server" Height="50px" CellPadding="0" Width="750px"               
                BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="Transparent" HorizontalAlign="Left" style="margin-top: 1px;">
                     <asp:TableRow >
                              <asp:TableCell ColumnSpan="3"  HorizontalAlign="left" Width="758px" >
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD15403" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="758px" DonHeight="60px" DonTabIndex="2"
                                       Etitext="CONDITIONS ET MOYENS D’EXERCICE" EtiVisible="true" 
		                               Donstyle="margin-left: 1px; font-size:9.0pt"
                                        EtiWidth="754px" EtiBorderColor ="Black"
		                               Etistyle="text-align: center; text-indent: 1px; font-size:small; font-weight: bold; EtiForeColor:#BFBFBF"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="15px"></asp:TableCell>
                        </asp:TableRow>
                         <asp:TableRow >
                          <asp:TableCell ColumnSpan="3"  HorizontalAlign="left" Width="758px"  >
                                  <asp:Label ID="Label13" runat="server"  Width="754px"  HorizontalAlign="center" BorderWidth="2px" BorderColor="Black" 
                                    Text="SERVICE D’AFFECTATION"
                                    CssClass="EP_Imp_SmallItalic" style="text-align: center; text-indent: 1px; font-size:small; font-weight: bold; background-color:#BFBFBF">
                                 </asp:Label>   
                                 </asp:TableCell>
                        </asp:TableRow>
                         <asp:TableRow >
                               <asp:TableCell ColumnSpan="3"  HorizontalAlign="left" Width="758px">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE15403" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="758px" DonHeight="60px" DonTabIndex="1"
                                       Etitext="Composition du service (nombre et catégorie des personnels)" EtiVisible="true" 
		                               Donstyle="margin-left: 1px; font-size:9.0pt"
                                        EtiWidth="754px" EtiBorderColor ="Black"
		                               Etistyle="text-align: left; text-indent: 1px; font-size:small; EtiForeColor:#D9D9D9"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                        </asp:TableRow>
                         <asp:TableRow >
                                  <asp:TableCell ColumnSpan="3"  HorizontalAlign="left" Width="758px">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF15403" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="758px" DonHeight="60px" DonTabIndex="2"
                                       Etitext="Lieu de travail" EtiVisible="true" 
		                               Donstyle="margin-left: 1px; font-size:9.0pt"
                                        EtiWidth="754px" EtiBorderColor ="Black"
		                               Etistyle="text-align: left; text-indent: 1px; font-size:small; EtiForeColor:#D9D9D9"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                        </asp:TableRow>
                         <asp:TableRow >
                               <asp:TableCell ColumnSpan="3"  HorizontalAlign="left" Width="758px">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoV115403" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="758px" DonHeight="60px" DonTabIndex="3"
                                       Etitext="Niveau catégoriel du poste" EtiVisible="true" 
		                               Donstyle="margin-left: 1px; font-size:9.0pt"
                                        EtiWidth="754px" EtiBorderColor ="Black"
		                               Etistyle="text-align: left; text-indent: 1px; font-size:small; EtiForeColor:#D9D9D9"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="15px"></asp:TableCell>
                        </asp:TableRow>
                         <asp:TableRow >
                          <asp:TableCell ColumnSpan="3"  HorizontalAlign="left" Width="758px"  >
                                  <asp:Label ID="Label6" runat="server"  Width="754px"  HorizontalAlign="center" BorderWidth="2px" BorderColor="Black" 
                                    Text="POSITIONNEMENT DU POSTE"
                                    CssClass="EP_Imp_SmallItalic" style="text-align: center; text-indent: 1px; font-size:small; font-weight: bold; background:#BFBFBF">
                                 </asp:Label>   
                                 </asp:TableCell>
                        </asp:TableRow>
                         <asp:TableRow >
                                 <asp:TableCell ColumnSpan="3"  HorizontalAlign="left" Width="758px">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoV215403" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="758px" DonHeight="60px" DonTabIndex="4"
                                       Etitext="Liens hiérarchiques" EtiVisible="true" 
		                               Donstyle="margin-left: 1px; font-size:9.0pt"
                                        EtiWidth="754px" EtiBorderColor ="Black"
		                               Etistyle="text-align: left; text-indent: 1px; font-size:small; EtiForeColor:#D9D9D9"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                        </asp:TableRow>
                         <asp:TableRow >
                              <asp:TableCell ColumnSpan="3"  HorizontalAlign="left" Width="758px">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoV315403" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                                       DonWidth="758px" DonHeight="60px" DonTabIndex="5"
                                       Etitext="Liens fonctionnels et relationnels" EtiVisible="true" 
		                               Donstyle="margin-left: 1px; font-size:9.0pt"
                                        EtiWidth="754px" EtiBorderColor ="Black"
		                               Etistyle="text-align: left; text-indent: 1px; font-size:small; EtiForeColor:#D9D9D9"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                        </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="I_CadreBasDePage4" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Left" BackColor="Transparent" BorderStyle="None" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="NumeroPage4" runat="server" Height="25px" Width="730px"
                            Text="Page 2/2" 
                            CssClass="EP_Imp_SmallItalic" style="font-size: 70%; text-align:right;">
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>