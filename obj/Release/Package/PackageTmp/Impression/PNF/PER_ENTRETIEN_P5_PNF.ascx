﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P5_PNF.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P5_PNF" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="V_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreTitreAppreciation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelVolet" runat="server" Text="APPRECIATION GLOBALE" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderColor="Transparent" BorderStyle="None"
                            BorderWidth="1px" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            style="margin-top: 1px; margin-left: 1px; margin-bottom: 10px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreAppreciationGlobale" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreAppreciation" runat="server" Height="35px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="L'appréciation globale est :"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Larger"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="V_CocheA10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="false" V_Height="30px"
                           V_BackColor="Transparent" V_BorderStyle="None"
                           V_Text="  Insuffisante" V_Width="250px" V_SiModeCaractere="true"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left; Font-size: 120%"/>               
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="V_CocheB10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="false" V_Height="30px"
                           V_BackColor="Transparent" V_BorderStyle="None"
                           V_Text="  Partielle" V_Width="250px" V_SiModeCaractere="true"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left; Font-size: 120%"/>               
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="V_CocheC10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="false" V_Height="30px"
                           V_BackColor="Transparent" V_BorderStyle="None"
                           V_Text="  Satisfaisante" V_Width="250px" V_SiModeCaractere="true"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left; Font-size: 120%"/>               
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="V_CocheD10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="false" V_Height="30px"
                           V_BackColor="Transparent" V_BorderStyle="None"
                           V_Text="  Très satisfaisante" V_Width="250px" V_SiModeCaractere="true"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left; Font-size: 120%"/>               
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="V_CocheE10" runat="server"
                           V_PointdeVue="1" V_Objet="150" V_Information="10" V_SiDonneeDico="false" V_Height="30px"
                           V_BackColor="Transparent" V_BorderStyle="None"
                           V_Text="  Exceptionnelle" V_Width="250px" V_SiModeCaractere="true"
                           V_Style="margin-left: 0px; font-style: normal; text-indent: 20px; text-align: left; Font-size: 120%"/>               
                    </asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero10" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Bilan de l'année"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Larger"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero11" runat="server" Height="35px" Width="370px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Commentaires de l'agent sur le bilan"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero12" runat="server" Height="35px" Width="370px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Commentaires du manager sur le bilan"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVV03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="370px" DonWidth="368px" DonHeight="250px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="" Etivisible="False"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoV013" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="false"
                           EtiWidth="370px" DonWidth="368px" DonHeight="250px" EtiHeight="20px" DonTabIndex="2" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"  ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero13" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Engagement sur l'année n+1"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Larger"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero14" runat="server" Height="40px" Width="370px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Commentaires de l'agent sur l'engagement année n+1"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero15" runat="server" Height="40px" Width="370px"
                           BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" Text="Commentaires du manager sur l'engagement année n+1"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVW03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="370px" DonWidth="368px" DonHeight="250px" EtiHeight="20px" DonTabIndex="3" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVX03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="370px" DonWidth="368px" DonHeight="250px" EtiHeight="20px" DonTabIndex="4" 
                           Etitext="" Etivisible="False"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="745px" HorizontalAlign="Center"
                BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" Visible="true">
                <asp:TableRow>
                    <asp:TableCell Height="1px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreSignature" runat="server" Height="27px" Width="745px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Date et signatures"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 3px;
                           font-style: normal; text-indent: 10px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreAgent" runat="server" Height="20px" Width="370px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="L'agent"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 10px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="V_LabelTitreManager" runat="server" Height="20px" Width="370px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Le manager"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: normal; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"  ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoH015004" runat="server" Height="40px" Width="370px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  normal; text-indent: 9px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoH015003" runat="server" Height="40px" Width="370px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  normal; text-indent: 0px; text-align: left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>  
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage8" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="8 / 14" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>