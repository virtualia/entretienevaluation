﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P2_ONISEP.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P2_ONISEP" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<div style="page-break-before:always;"></div>

 <asp:Table ID="II_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="II_CadreEnteteDePage1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelEnteteLigne11" runat="server" Height="20px" Width="748px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelEnteteLigne12" runat="server" Height="20px" Width="748px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreTitreEvolution" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVoletEntete" runat="server" Height="20px" Width="30px" Text="3 - " 
                            CssClass="EP_Imp_LargerBold" style="margin-top: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVolet" runat="server" Height="20px" Width="556px"
                            Text="Valeur professionnelle et manière de servir du fonctionnaire" 
                            CssClass="EP_Imp_LargerBoldSouligne" style="margin-top: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVoletSuite" runat="server" Text="" Height="20px" Width="160px"
                            CssClass="EP_Imp_SmallItalic" style="margin-top: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero31" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelTitre31" runat="server" Height="20px" Width="748px"
                           Text="3.1 Critères d'appréciation"
                           CssClass="EP_Imp_MediumBold" style="margin-bottom: 1px; text-indent: 1px; text-align:left">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformations31" runat="server" Height="40px" Width="748px"
                           Text="L'évaluateur retient, pour apprécier la valeur professionnelle des agents au cours de l'entretien
			               professionnel, les critères annexés à l'arrêté ministériel et qui sont adaptés à la nature des tâches
			               qui leur sont confiées, au niveau de leurs responsabilités et au contexte professionnel."
                           CssClass="EP_Imp_SmallNormal" style="margin-bottom: 1px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero311" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelFamille311" runat="server" Height="20px" Width="748px"
                           Text="1. Les compétences professionnelles et technicité"
                           CssClass="EP_Imp_MediumBold" style="margin-bottom: 1px; text-indent: 1px; text-align:left">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformations311" runat="server" Height="40px" Width="746px"
                           Text="(maîtrise technique ou expertise scientifique du domaine d'activité, connaissance de l'environnement professionnel
                           et capacité à s'y situer, qualité d'expression écrite, qualité d'expression orale, etc.)"
                           CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVH03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 0px; Font-size: 11px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero312" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelFamille312" runat="server" Height="20px" Width="748px"
                           Text="2. La contribution à l'activité du service"
                           CssClass="EP_Imp_MediumBold" style="margin-bottom: 1px; text-indent: 1px; text-align:left">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformations312" runat="server" Height="40px" Width="746px"
                           Text="(capacité à partager l'information, à transférer les connaissances et à rendre compte,
			               capacité à s'investir dans des projets, sens du service public et conscience professionnelle,
			               capacité à respecter l'organisation collective du travail, etc.)"
                           CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVI03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="2" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 0px; Font-size: 11px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero313" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelFamille313" runat="server" Height="20px" Width="748px"
                           Text="3. Les capacités professionnelles et relationnelles"
                           CssClass="EP_Imp_MediumBold" style="margin-bottom: 1px; text-indent: 1px; text-align:left">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformations313" runat="server" Height="40px" Width="746px"
                           Text="(autonomie, discernement et sens des initiatives dans l'exercice de ses attributions,
			               capacité d'adaptation, capacité à travailler en équipe, etc.)"
                           CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVJ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="3" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 0px; Font-size: 11px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero314" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelFamille314" runat="server" Height="20px" Width="748px"
                           Text="4. Le cas échéant, aptitude à l'encadrement et/ou à la conduite de projets"
                           CssClass="EP_Imp_MediumBold" style="margin-bottom: 1px; text-indent: 1px; text-align:left">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelInformations314" runat="server" Height="40px" Width="746px"
                           Text="(capacité d'organisation et de pilotage, aptitude à la conduite de projets, capacité à déléguer,
			               aptitude au dialogue, à la communication et à la négociation, etc.)"
                           CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVK03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="180px" EtiHeight="20px" DonTabIndex="4" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 0px; Font-size: 11px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
       <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage4" runat="server" Height="15px" Width="250px"
                BackColor="Transparent" BorderStyle="None"
                Text="4 / 13" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:right" >
            </asp:Label>   
       </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell>
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="3px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="II_CadreEnteteDePage2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelEnteteLigne21" runat="server" Height="20px" Width="748px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelEnteteLigne22" runat="server" Height="20px" Width="748px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="text-indent: 4px;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>   
    <asp:TableRow>
        <asp:TableCell Height="2px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero321" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelTitre321" runat="server" Height="35px" Width="748px"
                           Text="3.2 Appréciation générale sur la valeur professionnelle, la manière de servir et la réalisation des objectifs"
                           CssClass="EP_Imp_MediumBold" style="margin-bottom: 3px; text-indent: 1px; text-align:left">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="II_CadreAppreciation" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelTitreAppreciation" runat="server" Height="44px" Width="368px" Text=""
                            CssClass="EP_Imp_SmallNormal" style="margin-top: 1px; text-indent: 2px;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="II_CadreEnteteLabels" runat="server" CellPadding="0" CellSpacing="0">
                            <asp:TableRow> 
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="II_LabelTitreNote1" runat="server" Height="44px" Width="68px" Text="sans <br/> objet"
                                        CssClass="EP_Imp_SmallNormal"
                                        BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                                        style="margin-top: 1px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="II_LabelTitreNote2" runat="server" Height="44px" Width="80px" Text="à <br/> acquérir"
                                        CssClass="EP_Imp_SmallNormal"
                                        BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                                        style="margin-top: 1px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="II_LabelTitreNote3" runat="server" Height="44px" Width="95px" Text="à <br/> développer"
                                        CssClass="EP_Imp_SmallNormal"
                                        BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                                        style="margin-top: 1px; text-align: center;">
                                    </asp:Label>         
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="II_LabelTitreNote4" runat="server" Height="44px" Width="60px" Text="<br/> Maîtrise"
                                        CssClass="EP_Imp_SmallNormal"
                                        BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                                        style="margin-top: 1px; text-align: center;">
                                    </asp:Label>           
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="II_LabelTitreNote5" runat="server" Height="44px" Width="65px" Text="<br/> Expert"
                                        CssClass="EP_Imp_SmallNormal"
                                        BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" 
                                        style="margin-top: 1px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>      
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" runat="server"
                    BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
			            <asp:Label ID="II_InfoHA15601" runat="server" Height="30px" Width="367px"
                             Text="Compétences professionnelles et technicité"
                             CssClass="EP_Imp_SmallNormal" style="text-indent: 2px;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Center"
                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                        <Virtualia:VSixBoutonRadio ID="II_RadioHA03" runat="server" V_Groupe="Critere1"
                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                            VRadioN1Width="57px" VRadioN2Width="78px" VRadioN3Width="90px"
                            VRadioN4Width="57px" VRadioN5Width="55px" VRadioN6Width="54px"
                            VRadioN1Height="30px" VRadioN2Height="30px" VRadioN3Height="30px"
                            VRadioN4Height="30px" VRadioN5Height="30px" VRadioN6Height="30px"
                            VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 60px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" 
                            Visible="true" VRadioN1visible="false" VRadioN6visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" runat="server"
                    BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
			            <asp:Label ID="II_InfoHB15601" runat="server" Height="30px" Width="367px" 
                             Text="Contribution à l'activité du service"
                             CssClass="EP_Imp_SmallNormal" style="text-indent: 2px;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Center"
                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                        <Virtualia:VSixBoutonRadio ID="II_RadioHB03" runat="server" V_Groupe="Critere2"
                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                            VRadioN1Width="57px" VRadioN2Width="78px" VRadioN3Width="90px"
                            VRadioN4Width="57px" VRadioN5Width="55px" VRadioN6Width="54px"
                            VRadioN1Height="30px" VRadioN2Height="30px" VRadioN3Height="30px"
                            VRadioN4Height="30px" VRadioN5Height="30px" VRadioN6Height="30px"
                            VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 60px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" 
                            Visible="true" VRadioN1visible="false" VRadioN6visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" runat="server"
                    BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                        <asp:Label ID="II_InfoHC15601" runat="server" Height="30px" Width="367px"
                             Text="Capacités professionnelles et relationnelles"
                             CssClass="EP_Imp_SmallNormal" style="text-indent: 2px;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Center"
                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                        <Virtualia:VSixBoutonRadio ID="II_RadioHC03" runat="server" V_Groupe="Critere3"
                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                            VRadioN1Width="57px" VRadioN2Width="78px" VRadioN3Width="90px"
                            VRadioN4Width="57px" VRadioN5Width="55px" VRadioN6Width="54px"
                            VRadioN1Height="30px" VRadioN2Height="30px" VRadioN3Height="30px"
                            VRadioN4Height="30px" VRadioN5Height="30px" VRadioN6Height="30px"
                            VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 60px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" 
                            Visible="true" VRadioN1visible="false" VRadioN6visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" runat="server"
                    BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                        <asp:Label ID="II_InfoHD15601" runat="server" Height="30px" Width="367px"
                             Text="Aptitude à l'encadrement et/ou à la conduite de projets"
                             CssClass="EP_Imp_SmallNormal" style="text-indent: 2px;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Center"
                    BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                        <Virtualia:VSixBoutonRadio ID="II_RadioHD03" runat="server" V_Groupe="Critere4"
                            V_PointdeVue="1" V_Objet="156" V_Information="3" V_SiDonneeDico="false"
                            VRadioN1Width="57px" VRadioN2Width="78px" VRadioN3Width="90px"
                            VRadioN4Width="57px" VRadioN5Width="55px" VRadioN6Width="54px"
                            VRadioN1Height="30px" VRadioN2Height="30px" VRadioN3Height="30px"
                            VRadioN4Height="30px" VRadioN5Height="30px" VRadioN6Height="30px"
                            VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                            VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                            VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                            VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent" 
                            Visible="true" VRadioN6visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>   
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero322" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelFamille322" runat="server" Height="25px" Width="748px"
                           Text="Réalisation des objectifs de l'année écoulée (cf. paragraphe 2.1)"
                           BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px"
                           CssClass="EP_Imp_SmallNormal" style=" padding-top: 5px; text-indent: 6px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVL03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="9" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 0px; Font-size: 11px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero323" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="II_LabelFamille323" runat="server" Height="25px" Width="748px"
                           Text="Appréciation littérale *"
                           BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px"
                           CssClass="EP_Imp_SmallNormal" style=" padding-top: 5px; text-indent: 6px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoV13" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="150" V_Information="13" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="746px" DonHeight="250px" EtiHeight="20px" DonTabIndex="10" 
                           Etitext="" Etivisible="false"
                           Donstyle="margin-left: 0px; Font-size: 11px;"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelFamille321" runat="server" Height="35px" Width="748px"
                           Text="* Merci d'apporter un soin particulier à cette appréciation qui constitue un critère
			               pour l'avancement de grade des agents et pourra être repris dans les rapports liés à la promotion de grade"
                           CssClass="EP_Imp_SmallNormal" Font-Size="80%" style="margin-top: 2px; text-indent: 1px;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage5" runat="server" Height="15px" Width="250px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="5 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:right" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>