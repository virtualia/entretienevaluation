﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P0.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P0" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<asp:Table ID="PAGE_0" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="O_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreTitreEntretien" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#124545">
                        <asp:TableRow>
                            <asp:TableCell Height="3px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Table ID="CadreLogoCE" runat="server" BackImageUrl="~/Images/General/LogoCE.jpg" Width="106px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true">
                                    <asp:TableRow>
                                        <asp:TableCell Height="80px" BackColor="Transparent" Width="106px"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="3px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="O_Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL ET DE FORMATION" Height="20px" Width="746px"
                                    BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="1px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 1px; margin-left: 1px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                 <asp:Label ID="O_LabelAnneeEntretien" runat="server" Text="" Height="16px" Width="200px"
                                    BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="2px" ForeColor="Black"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 10px; margin-left: 1px; margin-bottom: 10px;
                                    font-style:  normal; text-indent: 2px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreAgent" runat="server" Text="Entre l'agent" Height="18px" Width="350px"
                                    BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="2px" ForeColor="Black"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 10px; margin-left: 1px; margin-bottom: 10px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelIdentite" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Nom et prénom :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelIdentite" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelStatut" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Statut :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelStatut" runat="server" Height="16px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGrade" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Corps / grade :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGrade" runat="server" Height="16px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelEchelon" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Echelon :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelEchelon" runat="server" Height="16px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDateEchelon" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Date de l'échelon :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelDateEchelon" runat="server" Height="16px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDateProchainEchelon" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Date du prochain échelon :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelDateProchainEchelon" runat="server" Height="16px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelAffectation" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Direction / service :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelAffectation" runat="server" Height="16px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelPoste" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Poste occupé :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelPoste" runat="server" Height="16px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow><asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDatePoste" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="depuis le :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 200px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelDatePoste" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreManager" runat="server" Text="et son supérieur hiérarchique direct" Height="20px" Width="350px"
                                    BackColor="Transparent"  BorderStyle="None"
                                    ForeColor="Black"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 20px; margin-left: 1px; margin-bottom: 10px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelManager" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Nom et prénom :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15001" runat="server" Height="16px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelStatutManager" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Statut :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelStatutManager" runat="server" Height="16px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGradeManager" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Corps / grade :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGradeManager" runat="server" Height="16px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelFonctionManager" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Fonctions exercées :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelFonctionManager" runat="server" Height="16px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>   
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDateEntretien" runat="server" Height="16px" Width="150px"
                                    BackColor="Transparent" BorderStyle="None" Text="Date de l'entretien :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15000" runat="server" Height="16px" Width="100px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDateEntretienPrecedent" runat="server" Height="16px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Date du précédent entretien :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelDateEntretienPrecedent" runat="server" Height="16px" Width="100px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:Table ID="O_CadreEmploi" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreTitreEmploi" runat="server" Height="23px" CellPadding="0" Width="750px" 
                            CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVolet" runat="server" Text="DESCRIPTIF DU POSTE OCCUPE" Height="18px" Width="746px"
                                        BackColor="Transparent"  BorderStyle="None"
                                        ForeColor="Black"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                                        style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>  
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                            <asp:TableRow>
                                <asp:TableCell Height="3px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero1" runat="server" Height="16px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Domaine fonctionnel"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                                       font-style: normal; text-indent: 1px; text-align:  left; ">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV05" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="5" V_SiDonneeDico="False"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="100px" EtiHeight="38px" DonTabIndex="1" 
                                       Etitext="(il correspond à une grande fonction de l'Etat et rassemble des emplois types qui y contribuent. 
                                       Les domaines fonctionnels des services du Conseil d'Etat sont présentés dans le répertoire des métiers des personnels) :"
                                       EtiBorderStyle="None" EtiBackColor="Transparent"
                                       Donstyle="margin-left: 1px;"
                                       Etistyle="margin-left: 1px; text-align: left; text-indent: 1px; font-size:smaller"
                                       DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>      
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                            <asp:TableRow>
                                <asp:TableCell Height="3px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero2" runat="server" Height="16px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Emploi-type"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                                       font-style: normal; text-indent: 1px; text-align:  left;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV06" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="6" V_SiDonneeDico="false"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="70px" EtiHeight="60px" DonTabIndex="2"
                                       Etitext="(les emplois types (ou métiers) sont référencés dans le répertoire des métiers des personnels. 
                                       L'emploi-type est un regroupement de postes de travail qui est fonction d'une proximité d'activités, 
                                       de savoir-faire et de connaissances à mettre en oeuvre (ex.: gestionnaire de personnels, assistant juridique, contrôleur de gestion...) :"
                                       EtiBorderStyle="None" EtiBackColor="Transparent"
                                       Donstyle="margin-left: 1px;"
                                       Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                       DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>      
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                            <asp:TableRow>
                                <asp:TableCell Height="3px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero3" runat="server" Height="18px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Activités principales du poste"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                                       font-style: normal; text-indent: 1px; text-align:  left; ">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV07" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="7" V_SiDonneeDico="false"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="140px" EtiHeight="60px" DonTabIndex="3"
                                       Etitext="(il s'agit d'un ensemble de tâches organisées et orientées vers un objectif précis. Les activités
                                        principales recoupent les missions confiées à l'agent dans le cadre du métier qu'il exerce. Elles seront 
                                        présentées de façon synthétique à l'exemple de celles qui sont présentées dans les avis de vacance de poste) :"
                                       EtiBorderStyle="None" EtiBackColor="Transparent"
                                       Donstyle="margin-left: 1px;"
                                       Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                       DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>      
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                            <asp:TableRow>
                                <asp:TableCell Height="3px" ColumnSpan="3"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                   <asp:Label ID="O_LabelTitreNumero4" runat="server" Height="20px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Fonctions d'encadrement"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                                       font-style: normal; text-indent: 1px; text-align:  left; ">
                                   </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="200px" HorizontalAlign="Left">        
                                   <Virtualia:VTrioHorizontalRadio ID="O_RadioH08" runat="server" V_Groupe="SiEncadrement"
                                       V_PointdeVue="1" V_Objet="150" V_Information="8" V_SiDonneeDico="false"
                                       RadioGaucheWidth="80px" RadioCentreWidth="80px" RadioDroiteWidth="0px"  
                                       RadioGaucheText="Non" RadioCentreText="Oui" RadioDroiteVisible="false"
                                       RadioGaucheHeight="23px" RadioCentreHeight="23px"
                                       RadioGaucheBorderStyle="None" RadioGaucheBackColor="Transparent"
                                       RadioCentreBorderStyle="None" RadioCentreBackColor="Transparent"
                                       RadioCentreStyle="text-align:  center;"  
                                       RadioGaucheStyle="margin-left: 1px; text-align:  center;" Visible="true"/>
                                </asp:TableCell> 
                                <asp:TableCell HorizontalAlign="Left">
                                   <asp:Label ID="O_EtiLabelNbAgents" runat="server" Height="18px" Width="180px"
                                       BackColor="Transparent" BorderStyle="None" Text="Nombre d'agents encadrés :"
                                       ForeColor="Black" Font-Italic="True"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: italic; text-indent: 1px; text-align: left;">
                                   </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                   <asp:Label ID="O_InfoH15009" runat="server" Height="18px" Width="50px"
                                       BackColor="Transparent" BorderStyle="None" Text="" 
                                       ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                       font-style:  normal; text-indent: 5px; text-align: left;">
                                   </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                   <asp:Label ID="O_LabelCommentaireBis" runat="server" Height="18px" Width="400px"
                                       BackColor="Transparent" BorderStyle="None"
                                       Text="(y compris, le cas échéant, assistants de justice et stagiaires)"
                                       ForeColor="Black" Font-Italic="True"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: 250px; margin-bottom: 0px;
                                       font-style: italic; text-indent: 1px; text-align: left;">
                                   </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                   <asp:Label ID="O_LabelCommentaire" runat="server" Height="60px" Width="740px"
                                       BackColor="Transparent" BorderStyle="None"
                                       Text="Afin d'éviter une double saisie, les rubriques figurant sur cette page auront été, dans la mesure du possible, 
                                       pré-renseignées par le département de la gestion des personnels du Conseil d'Etat et de la Cour nationale du droit 
                                       d'asile, à partir des données figurant dans Virtualia, à l'exception de celles signalées par une *, qui feront l'objet 
                                       d'une saisie manuelle."
                                       ForeColor="Black" Font-Italic="True"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                                       style="margin-top: 10px; margin-left: 0px; margin-bottom: 0px;
                                       font-style: italic; text-indent: 1px; text-align: left;">
                                   </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>
             </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="20px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage1" runat="server" Height="15px" Width="250px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="1 / 16" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>

 