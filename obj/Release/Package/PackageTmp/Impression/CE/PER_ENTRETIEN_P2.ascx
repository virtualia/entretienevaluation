﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P2.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P2" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="II_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreTitreObjectifs" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelVolet" runat="server" Text="2. OBJECTIFS ASSIGNES POUR L'ANNEE A VENIR" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderStyle="None"
                            ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelTitreNumero1" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="2.1 Objectifs d'activité attendus"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelTitreInfos1" runat="server" Height="140px" Width="743px"
                           BackColor="Transparent" BorderStyle="None"
                           Text="(leur nombre doit être restreint. Ces objectifs ne sauraient correspondre à une mission 
                           ou une activité du poste. Ils doivent en revanche refléter une volonté d'amélioration ou d'évolution, 
                           soit des tâches confiées à l'agent, soit de sa façon de les accomplir. Dans la mesure du possible, 
                           ces objectifs sont quantitatifs et qualitatifs. Lorsqu'ils sont qualitatifs, ils doivent être, dans 
                           la mesure du possible, quantifiables (ex.: réduire de moitié le délai de traitement d'un dossier; 
                           proposer des améliorations pour alléger le contenu d'un dossier administratif; réduire les coûts d'une prestation externe; 
                           créer et tenir à jour un tableau de suivi des agents récemment recrutés...). Il peut être fait mention 
                           dans cette rubrique de l'ancienneté sur le poste au regard des résultats souhaités)."
                           ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVK03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="False"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="Objectifs du service :"
                           EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align: left; text-indent: 1px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="II_LabelObjectifsAgent" runat="server" Height="20px" Width="743px"
                           BackColor="Transparent" BorderStyle="None"
                           Text="Objectifs assignés à l'agent :"
                           ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="II_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="II_TableEnteteObjectifs" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="II_LabelEnteteObjectif1" runat="server" Height="20px" Width="498px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Définition de l'objectif"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="II_LabelEnteteObjectif2" runat="server" Height="20px" Width="498px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="(si objectif collectif, précisez)"
                                   ForeColor="Black" Font-Italic="true"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"  
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: italic; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="II_TableEnteteDelais" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="II_LabelEnteteDelai1" runat="server" Height="20px" Width="230px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Délais de réalisation"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="II_LabelEnteteDelai2" runat="server" Height="20px" Width="230px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="(s'il y a lieu)"
                                   ForeColor="Black" Font-Italic="true"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"  
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: italic; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="II_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVB02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="False"
                                   EtiVisible="false" DonWidth="501px" DonHeight="100px" DonTabIndex="2"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; margin-bottom: 2px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="II_TableDelais1" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVB03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="233px" DonHeight="100px" DonTabIndex="3"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; margin-bottom: 2px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="II_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="501px" DonHeight="100px" DonTabIndex="4"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; margin-bottom: 2px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="II_TableDelais2" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="233px" DonHeight="100px" DonTabIndex="5"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; margin-bottom: 2px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="II_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="501px" DonHeight="100px" DonTabIndex="6"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; margin-bottom: 2px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="II_TableDelais3" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="233px" DonHeight="100px" DonTabIndex="7"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; margin-bottom: 2px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="II_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="501px" DonHeight="100px" DonTabIndex="8"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; margin-bottom: 2px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="II_TableDelais4" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVE03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="233px" DonHeight="100px" DonTabIndex="9"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; margin-bottom: 2px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="II_TableObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="508px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVF02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="501px" DonHeight="100px" DonTabIndex="10"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; margin-bottom: 2px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="II_TableDelais5" runat="server" CellPadding="0" CellSpacing="0" Width="240px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVF03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="152" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="233px" DonHeight="100px" DonTabIndex="11"
                                   EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                                   Donstyle="margin-left: 0px; margin-bottom: 2px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableFooterRow>
                    <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom" ColumnSpan="2" >
                        <asp:Label ID="NumeroPage4" runat="server" Height="15px" Width="150px"
                             BackColor="Transparent" BorderStyle="None"
                             Text="4 / 16" ForeColor="Black" Font-Italic="True"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                             style="text-align:left" >
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2" >
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="II_LabelDemarche" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"
                           Text="2.2 Démarche envisagée et moyens à prévoir pour faciliter l'atteinte des objectifs"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="II_InfoVL03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="100px" DonTabIndex="12"
                           Etitext="(il s'agit d'accompagner, voire de guider l'agent dans la réalisation des objectifs qui lui sont assignés. 
                           La question de la formation peut, dans ses grandes lignes, être envisagée à ce stade de l'entretien. Elle sera 
                           ensuite approfondie dans le cadre du volet de l'entretien consacré à la formation. Les moyens à prévoir peuvent 
                           correspondre également à l'acquisition de savoir-faire (maîtriser la rédaction administrative), aux outils techniques 
                           ou aux documents qu'il serait nécessaire de mettre en place pour améliorer l'efficacité du service)."
                           EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage5" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="5 / 16" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>