﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P3.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P3" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="III_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreTitreCompetence" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelVolet" runat="server" Text="3. MANIERE DE SERVIR" Height="20px" Width="746px"
                            BackColor="Transparent"  BorderStyle="None"
                            ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 6px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" BorderStyle="NotSet" BorderWidth="1px" BorderColor="Black"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInformations" runat="server" Height="80px" Width="746px"
                           BackColor="Transparent" BorderStyle="none"
                           Text="Les quatre rubriques exprimant la valeur professionnelle de l'agent sont renseignées 
                           à l'aide des grilles d'analyse qui suivent. Les critères énumérés pour chacune d'entre elles 
                           sont utilisés au regard de la spécificité du métier exercé par l'agent et des exigences du poste 
                           qu'il occupe. Certains de ces critères pourront donc apparaître plus ou moins pertinents. 
                           Pour l'essentiel il s'agit des critères qui étaient précédemment utilisés dans le cadre de l'exercice de notation."
                           ForeColor="Black"  
                           Font-Names="Trebuchet MS" Font-Size= "Small" Font-Italic= "true" Font-Bold="False" 
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                           font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>  
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelFamille1" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="3.1 Compétences professionnelles et technicité"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 10px;
                           font-style: normal; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille11" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=".  Connaissances du domaine d'activité"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille11" runat="server" Height="19px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(évaluation des connaissances nécessaires pour l'exercice des fonctions) :"
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="III_CadreCompetences311" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Left">
                                   <asp:Label ID="III_LabelTitreCompetence" runat="server" Height="44px" Width="260px"
                                       BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" Text="composantes"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                       style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 2px; text-align: center;">
                                   </asp:Label>          
                              </asp:TableCell>
                              <asp:TableCell HorizontalAlign="Left">
                                  <asp:Table ID="III_CadreEnteteLabels" runat="server" CellPadding="0" CellSpacing="0">
                                     <asp:TableRow> 
                                        <asp:TableCell HorizontalAlign="Left">
                                           <asp:Label ID="III_LabelTitreNote1" runat="server" Height="44px" Width="75px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="non requis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                           <asp:Label ID="III_LabelTitreNote2" runat="server" Height="44px" Width="80px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="insuffisant"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                           <asp:Label ID="III_LabelTitreNote3" runat="server" Height="44px" Width="95px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="en cours d'acquisition"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>         
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                           <asp:Label ID="III_LabelTitreNote4" runat="server" Height="44px" Width="75px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="acquis"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>           
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                           <asp:Label ID="III_LabelTitreNote5" runat="server" Height="44px" Width="75px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="maîtrisé"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                           <asp:Label ID="III_LabelTitreNote6" runat="server" Height="44px" Width="70px"
                                               BackColor="White" BorderColor="Black" BorderStyle="NotSet" Text="expert"
                                               BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                               style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                               font-style: oblique; text-indent: 0px; text-align:  center;">
                                           </asp:Label>          
                                        </asp:TableCell>
                                     </asp:TableRow>
                                  </asp:Table>      
                              </asp:TableCell>      
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHA01" runat="server" Height="20px" Width="267px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="juridiques"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHA07" runat="server" V_Groupe="Critere111"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 5px; margin-top: 0px;" VRadioN5Style="margin-left: 5px; margin-top: 0px;" VRadioN6Style="margin-left: 5px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                                     VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                                     VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHB01" runat="server" Height="20px" Width="267px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="administratives"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHB07" runat="server" V_Groupe="Critere112"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 5px; margin-top: 0px;" VRadioN5Style="margin-left: 5px; margin-top: 0px;" VRadioN6Style="margin-left: 5px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                                     VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                                     VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHC01" runat="server" Height="20px" Width="267px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="budgétaires et financières"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHC07" runat="server" V_Groupe="Critere113"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 5px; margin-top: 0px;" VRadioN5Style="margin-left: 5px; margin-top: 0px;" VRadioN6Style="margin-left: 5px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                                     VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                                     VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHD01" runat="server" Height="20px" Width="267px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="gestion des ressources humaines"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHD07" runat="server" V_Groupe="Critere114"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 5px; margin-top: 0px;" VRadioN5Style="margin-left: 5px; margin-top: 0px;" VRadioN6Style="margin-left: 5px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                                     VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                                     VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHE01" runat="server" Height="20px" Width="267px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="techniques"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHE07" runat="server" V_Groupe="Critere115"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 5px; margin-top: 0px;" VRadioN5Style="margin-left: 5px; margin-top: 0px;" VRadioN6Style="margin-left: 5px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                                     VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                                     VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>                        
                              <asp:TableCell HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHF01" runat="server" Height="20px" Width="267px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="nouvelles technologies"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHF07" runat="server" V_Groupe="Critere116"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 5px; margin-top: 0px;" VRadioN5Style="margin-left: 5px; margin-top: 0px;" VRadioN6Style="margin-left: 5px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                                     VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                                     VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHG01" runat="server" Height="20px" Width="267px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="accueil et orientation du public"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHG07" runat="server" V_Groupe="Critere117"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 5px; margin-top: 0px;" VRadioN5Style="margin-left: 5px; margin-top: 0px;" VRadioN6Style="margin-left: 5px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                                     VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                                     VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                              <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>                              
                              <asp:TableCell HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">
                                  <asp:Label ID="III_InfoHH01" runat="server" Height="20px" Width="267px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                                     Text="autre(s)"
                                     ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 2px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                              <asp:TableCell  HorizontalAlign="Left"
                              BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                                  <Virtualia:VSixBoutonRadio ID="III_RadioHH07" runat="server" V_Groupe="Critere118"
                                     V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                                     VRadioN1Width="70px" VRadioN2Width="78px" VRadioN3Width="90px"
                                     VRadioN4Width="70px" VRadioN5Width="70px" VRadioN6Width="69px"
                                     VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                     VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                     VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                                     VRadioN4Style="margin-left: 5px; margin-top: 0px;" VRadioN5Style="margin-left: 5px; margin-top: 0px;" VRadioN6Style="margin-left: 5px; margin-top: 0px;" 
                                     VRadioN1Text="" VRadioN2Text="" VRadioN3Text=""
                                     VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                                     VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                                     VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                                     VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent" 
                                     Visible="true"/>
                              </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell> 
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille12" runat="server" Height="30px" Width="748px"
                           Text=".  Souci de formation et de perfectionnement"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille12" runat="server" Height="38px" Width="748px"
                           Text="(aptitude à acquérir et à développer les connaissances nécessaires pour l'exercice des fonctions; rapidité d'acquisition; souci d'approfondissement et étendue des connaissances) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHI07" runat="server" V_Groupe="Critere312"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille13" runat="server" Height="30px" Width="748px"
                           Text=".  Connaissances de l'environnement professionnel et capacités à s'y situer"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille13" runat="server" Height="19px" Width="748px"
                           Text="(connaissance de l'organisation et du fonctionnement de l'administration et de la juridiction administrative) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHJ07" runat="server" V_Groupe="Critere313"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille14" runat="server" Height="30px" Width="748px"
                           Text=".  Qualités d'expression écrite"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille14" runat="server" Height="19px" Width="748px"
                           Text="(aptitude à rédiger des textes construits et écrits correctement) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHK07" runat="server" V_Groupe="Critere314"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille15" runat="server" Height="30px" Width="748px"
                           Text=".  Qualités d'expression orale"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille15" runat="server" Height="38px" Width="748px"
                           Text="(capacité à s'exprimer clairement et avec précision, à exposer un sujet en se mettant à la portée de son interlocuteur; aptitude à transmettre sa pensée; capacité à convaincre) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHL07" runat="server" V_Groupe="Critere315"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVM03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="1"
                           EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black" 
                           Etitext="Observations littérales, le cas échéant :"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align: left; text-indent: 1px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage6" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="6 / 16" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelFamille2" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="3.2 Qualités personnelles et relationnelles"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 10px;
                           font-style: normal; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille21" runat="server" Height="30px" Width="748px"
                           Text=".  Présentation"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille21" runat="server" Height="19px" Width="748px"
                           Text="(image que l'agent donne de lui-même et du service) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHM07" runat="server" V_Groupe="Critere321"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille22" runat="server" Height="30px" Width="748px"
                           Text=".  Maîtrise de soi"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille22" runat="server" Height="19px" Width="748px"
                           Text="(aptitude à faire face sereinement aux situations difficiles ou inattendues) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHN07" runat="server" V_Groupe="Critere322"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille23" runat="server" Height="30px" Width="748px"
                           Text=".  Aptitudes relationnelles et sens des relations humaines"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille23" runat="server" Height="57px" Width="748px"
                           Text="(aptitude à entretenir des relations ouvertes avec ses supérieurs, ses collaborateurs et ses interlocuteurs 
                           ; aptitude à coopérer avec ses collègues et à apporter à ses supérieurs une collaboration efficace dans la réception, l'interprétation et l'application de leurs directives) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHO07" runat="server" V_Groupe="Critere323"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille24" runat="server" Height="30px" Width="748px"
                           Text=".  Esprit d'initiative"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille24" runat="server" Height="38px" Width="748px"
                           Text="(capacité à prendre les initiatives opportunes en fonction des situations et des objectifs; 
                           souci d'amélioration du service rendu et du fonctionnement du service, aptitude à accompagner le changement) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHP07" runat="server" V_Groupe="Critere324"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="False"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille25" runat="server" Height="30px" Width="748px"
                           Text=".  Sens du service public et conscience professionnelle"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille25" runat="server" Height="19px" Width="748px"
                           Text="(sens de l'intérêt général; souci du service rendu aux usagers (internes ou externes) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHQ07" runat="server" V_Groupe="Critere325"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille26" runat="server" Height="30px" Width="748px"
                           Text=".  Capacité à respecter l'organisation collective du travail et sens du travail en équipe"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille26" runat="server" Height="2px" Width="748px"
                           Text=""
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHR07" runat="server" V_Groupe="Critere326"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVN03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="2"
                           EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black" 
                           Etitext="Observations littérales, le cas échéant :"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align: left; text-indent: 1px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage7" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="7 / 16" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelFamille3" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="3.3 Méthode et résultats"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 10px;
                           font-style: normal; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille31" runat="server" Height="30px" Width="748px"
                           Text=".  Fiabilité et qualité du travail"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille31" runat="server" Height="19px" Width="748px"
                           Text="(capacité à produire un travail régulier, fiable et de qualité) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHS07" runat="server" V_Groupe="Critere331"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille32" runat="server" Height="30px" Width="748px"
                           Text=".  Capacités d'analyse"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille32" runat="server" Height="2px" Width="748px"
                           Text=""
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHT07" runat="server" V_Groupe="Critere332"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille33" runat="server" Height="30px" Width="748px"
                           Text=".  Esprit de synthèse"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille33" runat="server" Height="2px" Width="748px"
                           Text=""
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHU07" runat="server" V_Groupe="Critere333"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille34" runat="server" Height="30px" Width="748px"
                           Text=".  Disponibilité et puissance de travail"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille34" runat="server" Height="38px" Width="748px"
                           Text="(implication personnelle dans le service, réactivité; capacité à produire rapidement un travail 
                           difficile ou à mener de front avec fiabilité plusieurs dossiers importants) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHV07" runat="server" V_Groupe="Critere334"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille35" runat="server" Height="30px" Width="748px"
                           Text=".  Sens de l'organisation"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille35" runat="server" Height="19px" Width="748px"
                           Text="(aptitude à planifier le travail et à dégager des priorités) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHW07" runat="server" V_Groupe="Critere335"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille36" runat="server" Height="30px" Width="748px"
                           Text=".  Efficacité et respect des délais"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille36" runat="server" Height="19px" Width="748px"
                           Text="(aptitude à obtenir dans les meilleurs délais possibles les résultats attendus) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHX07" runat="server" V_Groupe="Critere336"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="140px" VRadioN2Width="140px" VRadioN3Width="161px"
                        VRadioN4Width="140px" VRadioN5Width="140px" VRadioN6Width="135px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true" VRadioN1Visible="false"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVO03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="3"
                           EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black" 
                           Etitext="Observations littérales, le cas échéant :"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align: left; text-indent: 1px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>    
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage8" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="8 / 16" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="III_CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelFamille4" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="3.4 Aptitude au management et à la conduite de projet"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 10px;
                           font-style: normal; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille41" runat="server" Height="30px" Width="748px"
                           Text=".  Aptitude à l'encadrement et à l'animation d'équipe"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille41" runat="server" Height="38px" Width="748px"
                           Text="(aptitude à instaurer ou à maintenir la cohésion d'un groupe, à orienter 
                           et motiver ses collaborateurs, à leur fixer des objectifs réalistes) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHY07" runat="server" V_Groupe="Critere341"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="161px"
                        VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="non requis" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille42" runat="server" Height="30px" Width="748px"
                           Text=".  Capacité à dialoguer et à communiquer avec ses collaborateurs"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille42" runat="server" Height="19px" Width="748px"
                           Text="(sens de l'écoute; aptitude à prévenir, à gérer et à arbitrer les conflits) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioHZ07" runat="server" V_Groupe="Critere342"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="161px"
                        VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="non requis" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille43" runat="server" Height="30px" Width="748px"
                           Text=".  Capacité de décision et d'exercice des responsabilités"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille43" runat="server" Height="57px" Width="748px"
                           Text="(capacité à prendre, dans les délais nécessaires au bon fonctionnement du service, 
                           les décisions relevant de son niveau de responsabilités et adaptées à la situation; capacité à assumer pleinement la responsabilité de ses actions, 
                           voire celles de ses collaborateurs) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioH107" runat="server" V_Groupe="Critere343"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="161px"
                        VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="non requis" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille44" runat="server" Height="30px" Width="748px"
                           Text=".  Capacité à conduire des projets"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille44" runat="server" Height="19px" Width="748px"
                           Text="(initiative, capacité à faire face à toutes les situations dans le but d'atteindre les objectifs fixés) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioH207" runat="server" V_Groupe="Critere344"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="161px"
                        VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="non requis" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille45" runat="server" Height="30px" Width="748px"
                           Text=".  Délégation, contrôle et suivi des dossiers"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille45" runat="server" Height="38px" Width="748px"
                           Text="(aptitude à confier à un subordonné la réalisation d'une action tout en conservant 
                           la responsabilité; aptitude à exercer un contrôle et à se faire rendre compte) :"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioH307" runat="server" V_Groupe="Critere345"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="False"
                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="161px"
                        VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="non requis" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelSousFamille46" runat="server" Height="30px" Width="748px"
                           Text=".  Aptitude à former des collaborateurs et à valoriser leurs compétences"
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 10px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="III_LabelInfoSousFamille46" runat="server" Height="2px" Width="748px"
                           Text=""
                           BackColor="Transparent" BorderStyle="None" 
                           ForeColor="Black" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                           style="margin-top: 0px; margin-left: 9px; margin-bottom: 0px;
                           font-style: italic; text-indent: 1px; text-align:  left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell  HorizontalAlign="Left"
                       BorderStyle="None" BorderWidth="1px" BorderColor="Black" BackColor="Transparent">        
                       <Virtualia:VSixBoutonRadio ID="III_RadioH407" runat="server" V_Groupe="Critere346"
                        V_PointdeVue="1" V_Objet="153" V_Information="7" V_SiDonneeDico="false"
                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="161px"
                        VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                        VRadioN1Height="25px" VRadioN2Height="25px" VRadioN3Height="25px"
                        VRadioN4Height="25px" VRadioN5Height="25px" VRadioN6Height="25px"
                        VRadioN1BorderStyle="None" VRadioN1BackColor="Transparent" VRadioN2BorderStyle="None" VRadioN2BackColor="Transparent"
                        VRadioN3BorderStyle="None" VRadioN3BackColor="Transparent" VRadioN4BorderStyle="None" VRadioN4BackColor="Transparent"
                        VRadioN5BorderStyle="None" VRadioN5BackColor="Transparent" VRadioN6BorderStyle="None" VRadioN6BackColor="Transparent"
                        VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 0px; margin-top: 0px;" VRadioN3Style="margin-left: 0px; margin-top: 0px;"
                        VRadioN4Style="margin-left: 0px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                        VRadioN1Text="non requis" VRadioN2Text="insuffisant" VRadioN3Text="en cours d'acquisition"
                        VRadioN4Text="acquis" VRadioN5Text="maîtrisé" VRadioN6Text="expert" 
                        Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="III_InfoVP03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="4" 
                           EtiBorderStyle="None" EtiBackColor="Transparent" DonBorderColor="Black"
                           Etitext="Observations littérales, le cas échéant :"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align: left; text-indent: 1px;"
                           DonBorderWidth="1px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage9" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="9 / 16" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>