﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P5_CESE.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P5_CESE" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="V_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreTitreAppreciation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelVolet" runat="server" Text="EVOLUTION PROFESSIONNELLE DE L'AGENT" Height="20px" Width="748px"
                            BackColor= "Transparent" BorderStyle="None" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero11" runat="server" Height="34px" Width="748px"
                           BackColor="#E7E7E7" BorderColor="Transparent" BorderStyle="NotSet" 
                           Text="1. Proposition d'attribution d'une bonification d'ancienneté"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 6px;
                           font-style: normal; text-indent: 20px; text-align: Left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero12" runat="server" Height="18px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent"  BorderStyle="None" 
                           Text="Pour rappel :"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="True"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero13" runat="server" Height="110px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent"  BorderStyle="None" 
                           Text="Tout agent ayant obtenu une promotion par examen professionnel ou au choix ne peut obtenir
                           une réduction d'ancienneté au titre de la même année. Les agents ayant obtenu une réduction de 3 mois
                           en année N ne peuvent obtenir en N+1 une réduction identique. L'absence de réduction d'ancienneté pendant 
                           plusieurs années consécutives doit être justifiée par l'administration et par le supérieur hiérarchique 
                           auprès de son collaborateur, notamment si les appréciations dans le cadre des entretiens d'évaluations 
                           ne font pas apparaître de difficulté particulière."
                           BorderWidth="1px" ForeColor="Black" Font-Italic="True"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero14" runat="server" Height="45px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent"  BorderStyle="None" 
                           Text="Concernant les administrateurs, conformément aux dispositions réglementaires, 
                           aucune réduction d'ancienneté ne peut être attribuée (article 10 du décret du 16 novembre 1999 et 
                           articles 7 à 11 du décret n° 2010-888 du 28 juillet 2010)."
                           BorderWidth="1px" ForeColor="Black" Font-Italic="True"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero15" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Evolution conduisant à un avancement normal"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVJ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="744px" DonHeight="60px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero16" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Evolution conduisant à un avancement accéléré"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero17" runat="server" Height="40px" Width="740px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
			               Text="L'évaluateur peut indiquer l'amplitude de la réduction d'ancienneté souhaitée, 
                           soit un mois, soit trois mois (sous réserve de l'harmonisation)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="true"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="80%"
                           style="margin-top: 0px; margin-left: 8px; margin-bottom: 0px; padding-top: 4px;
                           font-style: oblique; text-indent: 2px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVK03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="744px" DonHeight="60px" EtiHeight="20px" DonTabIndex="2" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero18" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Dernières réductions d'ancienneté et promotions obtenues"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVL03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="744px" DonHeight="60px" EtiHeight="20px" DonTabIndex="3" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow> 
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero21" runat="server" Height="34px" Width="748px"
                           BackColor="#E7E7E7" BorderColor="Transparent" BorderStyle="NotSet" 
                           Text="2. Proposition d'attribution d'une augmentation de la part variable de la prime de rendement"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 6px;
                           font-style: normal; text-indent: 20px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero22" runat="server" Height="18px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Pour rappel :"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="True"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero23" runat="server" Height="75px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="«le montant individualisé de la part variable lié à l'engagement professionnel de l'agent et à l'atteinte 
			               des objectifs annuels est déterminée au regard notamment du compte-rendu de l'entretien d'évaluation.
			               Par rapport à l'année N-1, la part variable ne peut diminuer ou augmenter de plus de 10 % sauf en cas de changement de corps et / ou de fonction.»"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="True"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero24" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="(Référentiel de gestion des ressources humaines, arrêté n°14-06 du 5 février 2014)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="True"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 5px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHA15601" runat="server" Height="20px" Width="368px"
                           BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                           Text="Proposition d'une augmentation de la part variable"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="V_RadioHA04" runat="server" V_Groupe="Critere1"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                           VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent"  
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_LabelTitreNumero26" runat="server" Height="40px" Width="370px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Dernières augmentations de la part variable obtenues"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 4px;
                           font-style: normal; text-indent: 10px; text-align: left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="V_InfoVM03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="746px" DonWidth="361px" DonHeight="40px" EtiHeight="20px" DonTabIndex="4" 
                           Etivisible="False" EtiText=""
                           Donstyle="margin-left: 0px;" DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Center" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage9" runat="server" Height="15px" Width="748px"
                BackColor="Transparent" BorderStyle="None"
                Text="9 / 12" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:right">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero31" runat="server" Height="34px" Width="748px"
                           BackColor="#E7E7E7" BorderColor="Transparent" BorderStyle="NotSet" 
                           Text="3. Un changement d’affectation est-il :"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 6px;
                           font-style: normal; text-indent: 20px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHB15601" runat="server" Height="20px" Width="368px"
                           BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                           Text="Sans objet pour le moment"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="V_RadioHB04" runat="server" V_Groupe="Critere2"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                           VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHC15601" runat="server" Height="20px" Width="368px"
                           BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                           Text="Souhaitable à moyen terme"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="V_RadioHC04" runat="server" V_Groupe="Critere3"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                           VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHD15601" runat="server" Height="20px" Width="368px"
                           BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                           Text="A envisager"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="V_RadioHD04" runat="server" V_Groupe="Critere4"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                           VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHE15601" runat="server" Height="20px" Width="368px"
                           BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                           Text="A été réalisé dans l'année"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="V_RadioHE04" runat="server" V_Groupe="Critere5"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                           VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero41" runat="server" Height="34px" Width="748px"
                           BackColor="#E7E7E7" BorderColor="Transparent" BorderStyle="NotSet" 
                           Text="4. Une promotion de grade au choix, sous réserve des conditions statutaires, est-elle :"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 6px;
                           font-style: normal; text-indent: 20px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHF15601" runat="server" Height="20px" Width="368px"
                           BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                           Text="Pas encore à l'ordre du jour"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="V_RadioHF04" runat="server" V_Groupe="Critere6"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                           VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHG15601" runat="server" Height="20px" Width="368px"
                           BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                           Text="Envisageable à moyen terme"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="V_RadioHG04" runat="server" V_Groupe="Critere7"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                           VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHH15601" runat="server" Height="20px" Width="368px"
                           BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                           Text="Recommandée fortement par l'évaluateur"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="V_RadioHH04" runat="server" V_Groupe="Critere8"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                           VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="V_CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="V_LabelTitreNumero51" runat="server" Height="34px" Width="748px"
                           BackColor="#E7E7E7" BorderColor="Transparent" BorderStyle="NotSet" 
                           Text="5. Une promotion de corps au choix, sous réserve des conditions statutaires, est-elle :"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 6px;
                           font-style: normal; text-indent: 20px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHI15601" runat="server" Height="20px" Width="368px"
                           BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                           Text="Pas encore à l'ordre du jour"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="V_RadioHI04" runat="server" V_Groupe="Critere9"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                           VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHJ15601" runat="server" Height="20px" Width="368px"
                           BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                           Text="Envisageable à moyen terme"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell  HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="V_RadioHJ04" runat="server" V_Groupe="Critere10"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                           VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="V_InfoHK15601" runat="server" Height="20px" Width="368px"
                           BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                           Text="Recommandée fortement par l'évaluateur"
                           ForeColor="Black" Font-Italic="False"
                           Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 2px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">        
                        <Virtualia:VSixBoutonRadio ID="V_RadioHK04" runat="server" V_Groupe="Critere11"
                           V_PointdeVue="1" V_Objet="156" V_Information="4" V_SiDonneeDico="false"
                           VRadioN1Width="176px" VRadioN2Width="176px" VRadioN3Width="0px"
                           VRadioN4Width="0px" VRadioN5Width="0px" VRadioN6Width="0px"
                           VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                           VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                           VRadioN1Style="margin-left: 0px; margin-top: 0px;" VRadioN2Style="margin-left: 2px; margin-top: 0px;" VRadioN3Style="margin-left: 1px; margin-top: 0px;"
                           VRadioN4Style="margin-left: 1px; margin-top: 0px;" VRadioN5Style="margin-left: 0px; margin-top: 0px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;"
                           VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent"
                           VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" 
                           VRadioN1Text="Oui" VRadioN2Text="Non" VRadioN3Text=""
                           VRadioN4Text="" VRadioN5Text="" VRadioN6Text=""
                           VRadioN3Visible="false" VRadioN4Visible="false" VRadioN5Visible="false" VRadioN6Visible="false"   
                           Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Center" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage10" runat="server" Height="15px" Width="748px"
                BackColor="Transparent" BorderStyle="None"
                Text="10 / 12" ForeColor="Black" Font-Italic="True"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                style="text-align:right; text-indent: 0px">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>