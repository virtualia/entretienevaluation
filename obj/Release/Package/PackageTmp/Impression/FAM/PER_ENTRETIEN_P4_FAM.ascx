﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P4_FAM.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P4_FAM" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioVerticalRadio.ascx" tagname="VTrioVerticalRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="IV_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="Transparent" Width="750px" HorizontalAlign="left" style="margin-top: 1px;">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreTitreFormation" runat="server" Height="30px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="IV_LabelVolet" runat="server" Text="IV. Les besoins de formation" 
                            Height="20px" Width="746px"
                            BackColor="Transparent"  BorderStyle="None"
                            ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_EtiLabelIdentite" runat="server" Height="20px" Width="120px"
                            BackColor="Transparent" BorderStyle="None" Text="Nom et prénom :"
                            ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelIdentite" runat="server" Height="20px" Width="580px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_EtiLabelAffectation" runat="server" Height="20px" Width="200px"
                            BackColor="Transparent" BorderStyle="None" Text="Direction - service - unité :"
                            ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelAffectation" runat="server" Height="20px" Width="500px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_EtiLabelAffectationUnite" runat="server" Height="20px" Width="200px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="True"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: italic; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelAffectationUnite" runat="server" Height="20px" Width="500px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style:  normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>     
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero11" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Formations suivies (années N et N-1)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VListeGrid ID="IV_ListeFormationsSuivies" runat="server" CadreWidth="746px" SiColonneSelect="false"
                            SiAppliquerCharte="false" BackColorCaption="White" ForeColorCaption="Black" BackColorRow="White" ForeColorRow="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableFooterRow>
                    <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
                        <asp:Label ID="NumeroPage10" runat="server" Height="15px" Width="150px"
                             BackColor="Transparent" BorderStyle="None"
                             Text="10 / 18" ForeColor="Black" Font-Italic="True"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                             style="text-align:left" >
                        </asp:Label>   
                    </asp:TableCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell >
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero11Repete" runat="server" Height="40px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Formations suivies (années N et N-1)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelTitreNumero13" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Commentaires sur les formations suivies (appréciation, bilan, suites)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVE03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="1" 
                           Etivisible="false"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTitreNumero12" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Formations demandées (années N et N-1)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="IV_LabelTitreNumero14" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Commentaires sur les formations demandées"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVF03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="746px" DonHeight="180px" EtiHeight="20px" DonTabIndex="2" 
                           Etivisible="false"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>     
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage11" runat="server" Height="15px" Width="150px"
                    BackColor="Transparent" BorderStyle="None"
                    Text="11 / 18" ForeColor="Black" Font-Italic="True"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                    style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="IV_LabelTitreNumero2" runat="server" Height="25px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Recueil des demandes de formation"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Larger"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 1px; text-align:  center; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelLegendeT11" runat="server" Height="20px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(1)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelLegendeT12" runat="server" Height="20px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T1 : Formations liées à l'adaptation immédiate au poste de travail"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelLegendeT21" runat="server" Height="20px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelLegendeT22" runat="server" Height="20px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T2 : Formations liées à l'évolution des métiers"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelLegendeT31" runat="server" Height="20px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelLegendeT32" runat="server" Height="20px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="T3 : Formations liées au développement des qualifications ou l'acquisition de nouvelles qualifications"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelLegendeDIF11" runat="server" Height="25px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="(2)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelLegendeDIF12" runat="server" Height="25px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="L'agent indique s'il souhaite mobiliser son DIF pour la formation envisagée."
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelLegendeDIF21" runat="server" Height="25px" Width="48px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text=""
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
	                <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelLegendeDIF22" runat="server" Height="25px" Width="700px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="Les formations liées à une adaptation immédiate au poste de travail ne peuvent être imputées sur le DIF."
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="6px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>            
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreEnteteDemandes1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left"  ColumnSpan="6">
                        <asp:Label ID="IV_LabelTitreFormations1" runat="server" Height="35px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Demandes de formation"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteExpressionDemande1" runat="server" Height="50px" Width="362px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px"
                            Text="Expression des demandes"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteTypologie1" runat="server" Height="50px" Width="60px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Typologie (1)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteInitiative1" runat="server" Height="50px" Width="100px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Demande <br/> d'initiative"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteAvisResponsable1" runat="server" Height="50px" Width="90px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Avis du responsable hiérarchique"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteRecoursDIF1" runat="server" Height="50px" Width="60px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Recours <br/> au DIF <br/> (2)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteEcheance1" runat="server" Height="50px" Width="70px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Echéance <br/> envisagée"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN1" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN1" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="IV_InfoHA15501" runat="server" Height="19px" Width="350px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelPrecisionDemandeN1" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHA15503" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelObjectifDemandeN1" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVA05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="5" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVA02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123A"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVA10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeA" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVA06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisA"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVA11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifA" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHA15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN2" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN2" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="IV_InfoHB15501" runat="server" Height="19px" Width="350px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelPrecisionDemandeN2" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHB15503" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelObjectifDemandeN2" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVB05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="10" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVB02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123B"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVB10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeB" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVB06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisB"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVB11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifB" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHB15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN3" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN3" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="IV_InfoHC15501" runat="server" Height="19px" Width="350px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelPrecisionDemandeN3" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHC15503" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelObjectifDemandeN3" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVC05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="15" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVC02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123C"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVC10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeC" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVC06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisC"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVC11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifC" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHC15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN4" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN4" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN4" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="IV_InfoHD15501" runat="server" Height="19px" Width="350px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelPrecisionDemandeN4" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHD15503" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelObjectifDemandeN4" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVD05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="20" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVD02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123D"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVD10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeD" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVD06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisD"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVD11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifD" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHD15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN5" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN5" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN5" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="IV_InfoHE15501" runat="server" Height="19px" Width="350px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelPrecisionDemandeN5" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHE15503" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelObjectifDemandeN5" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVE05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="25" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVE02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123E"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVE10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeE" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVE06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisE"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVE11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifE" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHE15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage12" runat="server" Height="15px" Width="150px"
                    BackColor="Transparent" BorderStyle="None"
                    Text="12 / 18" ForeColor="Black" Font-Italic="True"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                    style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN6" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN6" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN6" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="IV_InfoHF15501" runat="server" Height="19px" Width="350px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelPrecisionDemandeN6" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHF15503" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelObjectifDemandeN6" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVF05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="30" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVF02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123F"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVF10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeF" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVF06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisF"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVF11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifF" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHF15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN7" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN7" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN7" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="IV_InfoHG15501" runat="server" Height="19px" Width="350px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelPrecisionDemandeN7" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHG15503" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelObjectifDemandeN7" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVG05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="35" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVG02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123G"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVG10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeG" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVG06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisG"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVG11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifG" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHG15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN8" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN8" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN8" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="IV_InfoHH15501" runat="server" Height="19px" Width="350px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelPrecisionDemandeN8" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHH15503" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelObjectifDemandeN8" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVH05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="40" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVH02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123H"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVH10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeH" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVH06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisH"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVH11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifH" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHH15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN9" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN9" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN9" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="IV_InfoHI15501" runat="server" Height="19px" Width="350px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelPrecisionDemandeN9" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHI15503" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelObjectifDemandeN9" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVI05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="45" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVI02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123I"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVI10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeI" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVI06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisI"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVI11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifI" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHI15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN10" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN10" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN10" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="THEMATIQUE souhaitée"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="IV_InfoHJ15501" runat="server" Height="19px" Width="350px"
                                        BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                        Text="" ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style : normal; text-indent: 0px; text-align:  left;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelPrecisionDemandeN10" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHJ15503" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelObjectifDemandeN10" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVJ05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="349px" DonHeight="30px" DonTabIndex="50" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVJ02" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="2" V_SiDonneeDico="false"
                            RadioGaucheText="T1" RadioCentreText="T2" RadioDroiteText="T3"
                            V_Groupe="GroupeT123J"
                            RadioGaucheWidth="45px" RadioCentreWidth="45px" RadioDroiteWidth="45px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVJ10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeJ" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVJ06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisJ"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVJ11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifJ" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHJ15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage13" runat="server" Height="15px" Width="150px"
                    BackColor="Transparent" BorderStyle="None"
                    Text="13 / 18" ForeColor="Black" Font-Italic="True"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                    style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreEnteteDemandes2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left"  ColumnSpan="6">
                        <asp:Label ID="IV_LabelTitreFormations2" runat="server" Height="35px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Préparations aux concours"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteExpressionDemande2" runat="server" Height="50px" Width="362px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; padding-top: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteTypologie2" runat="server" Height="50px" Width="60px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px" 
                            Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: -1px; margin-bottom: 0px; padding-top: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteInitiative2" runat="server" Height="50px" Width="100px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px" 
                            Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: -1px; margin-bottom: 0px; padding-top: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteAvisResponsable2" runat="server" Height="50px" Width="90px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Avis du responsable hiérarchique"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteRecoursDIF2" runat="server" Height="50px" Width="60px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Recours <br/> au DIF <br/> (2)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteEcheance2" runat="server" Height="50px" Width="70px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Echéance <br/> envisagée"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN11" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN11" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN11" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="CONCOURS envisagé"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHK15501" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelPrecisionDemandeN11" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVK03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="350px" DonHeight="45px" DonTabIndex="54" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTypologieDemandeN11" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelInitiativeDemandeN11" runat="server" Height="19px" Width="100px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVK06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisK"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVK11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifK" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHK15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN12" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN12" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN12" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="CONCOURS envisagé"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHL15501" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelPrecisionDemandeN12" runat="server" Height="15px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="85%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVL03" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="3" V_SiDonneeDico="false"
                                        DonWidth="350px" DonHeight="45px" DonTabIndex="59" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTypologieDemandeN12" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelInitiativeDemandeN12" runat="server" Height="19px" Width="100px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVL06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisL"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVL11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifL" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHL15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell Height="40px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreEnteteDemandes3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left"  ColumnSpan="6">
                        <asp:Label ID="IV_LabelTitreFormations3" runat="server" Height="35px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Autres actions (VAE, bilan de compétence, congé de formation)"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteExpressionDemande3" runat="server" Height="50px" Width="362px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                            Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteTypologie3" runat="server" Height="50px" Width="60px"
                            BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px" 
                            Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteInitiative3" runat="server" Height="50px" Width="100px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Demande <br/> d'initiative"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteAvisResponsable3" runat="server" Height="50px" Width="90px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Avis du responsable hiérarchique"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteRecoursDIF3" runat="server" Height="50px" Width="60px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Recours <br/> au DIF <br/> (2)"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelEnteteEcheance3" runat="server" Height="50px" Width="70px"
                            BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px" 
                            Text="Echéance <br/> envisagée"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: -1px; margin-left: -1px; margin-bottom: 0px;
                            font-style: normal; text-indent: 0px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>

    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN13" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN13" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN13" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="ACTION IDENTIFIEE"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHM15501" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelObjectifDemandeN13" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVM05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="349px" DonHeight="45px" DonTabIndex="64" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTypologieDemandeN13" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVM10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeM" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVM06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisM"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVM11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifM" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHM15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="IV_CadreDemandeN14" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                BackColor="Transparent" BorderStyle="Notset" BorderColor="Black" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="IV_TableDemandeN14" runat="server" CellPadding="0" CellSpacing="0" Width="360px"
                            BackColor="Transparent" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelThematiqueDemandeN14" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="ACTION IDENTIFIEE"
                                        ForeColor="GrayText" Font-Italic="False"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="75%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                  <asp:Label ID="IV_InfoHN15501" runat="server" Height="19px" Width="350px"
                                     BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" 
                                     Text="" ForeColor="Black" Font-Italic="False"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                     style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                     font-style : normal; text-indent: 0px; text-align:  left;">
                                  </asp:Label>
                              </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:Label ID="IV_LabelObjectifDemandeN14" runat="server" Height="19px" Width="360px"
                                        BackColor="Transparent" BorderStyle="None" BorderColor="Black" BorderWidth="1px"
                                        Text="Objectifs à atteindre :"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="70%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 4px; text-align:  left;">
                                   </asp:Label>
                                </asp:TableCell>
	                        </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                   <Virtualia:VCoupleVerticalEtiDonnee ID="IV_InfoVN05" runat="server" DonTextMode="true"
                                        V_PointdeVue="1" V_Objet="155" V_Information="5" V_SiDonneeDico="false"
                                        DonWidth="349px" DonHeight="45px" DonTabIndex="68" 
                                        Etivisible="false" 
                                        Donstyle="margin-left: 1px;"
                                        Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
                                        DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>                                   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_LabelTypologieDemandeN14" runat="server" Height="19px" Width="55px"
                            BackColor="Transparent" BorderStyle="None" BorderWidth="1px" Text="">
                        </asp:Label>
                    </asp:TableCell>    
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVN10" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="10" V_SiDonneeDico="false"
                            RadioGaucheText="agent" RadioCentreText="responsable" RadioDroiteText=""
                            V_Groupe="GroupeInitiativeN" RadioDroiteVisible="false"
                            RadioGaucheWidth="94px" RadioCentreWidth="94px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVN06" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="6" V_SiDonneeDico="false"
                            RadioGaucheText="Favorable" RadioCentreText="Refusé" RadioDroiteText="Reporté"
                            V_Groupe="GroupeAvisN"
                            RadioGaucheWidth="84px" RadioCentreWidth="84px" RadioDroiteWidth="84px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"
                            RadioGaucheStyle="text-align: left; Font-size: 75%" RadioCentreStyle="text-align: left; Font-size: 75%" RadioDroiteStyle="text-align: left; Font-size: 75%"/>    
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VTrioVerticalRadio ID="IV_RadioVN11" runat="server"  
                            V_PointdeVue="1" V_Objet="155" V_Information="11" V_SiDonneeDico="false"
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                            V_Groupe="GroupeRecoursDifN" RadioDroiteVisible="false"
                            RadioGaucheWidth="57px" RadioCentreWidth="57px" RadioDroiteWidth="0px"
                            RadioGaucheHeight="19px" RadioCentreHeight="19px" RadioDroiteHeight="19px"
                            RadioGaucheBackColor="Transparent" RadioCentreBackcolor="Transparent" RadioDroiteBackcolor="Transparent"
                            RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent" RadioDroiteBorderColor="Transparent"		                    
                            RadioGaucheStyle="text-align: left; Font-size: 75%; margin-left: 0px" 
                            RadioCentreStyle="text-align: left; Font-size: 75%; margin-left: 0px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="IV_InfoHN15504" runat="server" Height="19px" Width="58px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="None" BorderWidth="1px" 
                            Text="" ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style : normal; text-indent: 0px; text-align:  left;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage14" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="14 / 18" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>