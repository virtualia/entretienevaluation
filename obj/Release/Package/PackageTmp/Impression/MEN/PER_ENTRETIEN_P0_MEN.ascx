﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P0_MEN.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P0_MEN" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .EP_Imp_SmallNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_SmallItalic
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_MediumNormal
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBold
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
    .EP_Imp_LargerBoldSouligne
    {  
        background-color:transparent;
        color:Black;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:larger;
        font-style:normal;
        font-weight:bold;
        text-decoration:underline;
        text-indent:0px;
        text-align:left;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
    }
</style>

<asp:Table ID="PAGE_0" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="O_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreTitreEntretien" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#124545">
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Table ID="O_CadreLogoMEN" runat="server" BackImageUrl="~/Images/General/LogoMEN.jpg" Width="667px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true" HorizontalAlign="Left">
                                    <asp:TableRow>
                                        <asp:TableCell Height="132px" BackColor="Transparent" Width="667px" HorizontalAlign="Left"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="O_Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                                    CssClass="EP_Imp_MediumBold">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="O_LabelAnneeEntretien" runat="server" Text="" Height="20px" Width="100px"
                                    CssClass="EP_Imp_MediumNormal" style="margin-top: 5px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreAgent" runat="server" Height="20px" Width="150px" Text="Entre l'agent"
                                    CssClass="EP_Imp_MediumNormal" 
                                    style="margin-top: 5px; margin-left: 1px; text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelIdentite" runat="server" Height="20px" Width="150px" Text="Nom d'usage :"
                                     CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelIdentite" runat="server" Height="20px" Width="550px" Text=""
                                     CssClass="EP_Imp_MediumNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelPatronyme" runat="server" Height="20px" Width="150px" Text="Nom de famille :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelPatronyme" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_MediumNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelNaissance" runat="server" Height="20px" Width="150px" Text="Date de naissance :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelNaissance" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelStatut" runat="server" Height="20px" Width="150px" Text="Statut :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelStatut" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGrade" runat="server" Height="20px" Width="150px" Text="Corps - grade :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGrade" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelEchelon" runat="server" Height="20px" Width="150px" Text="Echelon :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelEchelon" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDateEchelon" runat="server" Height="20px" Width="250px" Text="Date de promotion dans l'échelon :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelDateEchelon" runat="server" Height="20px" Width="450px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreManager" runat="server" Height="20px" Width="280px"
                                    Text="Et son supérieur hiérarchique direct"
                                    CssClass="EP_Imp_MediumNormal"
                                    style="margin-top: 12px; margin-left: 1px; margin-bottom: 5px; text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelManager" runat="server" Height="20px" Width="150px" Text="Nom et prénom :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15001" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGradeManager" runat="server" Height="20px" Width="150px" Text="Corps - grade :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGradeManager" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelFonctionManager" runat="server" Height="20px" Width="150px" Text="Intitulé de la fonction :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelFonctionManager" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDirectionManager" runat="server" Height="20px" Width="150px" Text="Structure :"
                                    CssClass="EP_Imp_SmallItalic" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15002" runat="server" Height="20px" Width="550px" Text=""
                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>  
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="2px"></asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="O_EtiLabelDateEntretien" runat="server" Height="20px" Width="375px" Text="Date de l'entretien professionnel :"
                                    CssClass="EP_Imp_MediumNormal" style="text-indent: 1px; text-align: right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="O_InfoH15000" runat="server" Height="20px" Width="375px" Text=""
                                    CssClass="EP_Imp_MediumNormal" style="text-indent: 5px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:Table ID="O_CadreEmploi" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreTitreEmploi" runat="server" Height="25px" CellPadding="0" Width="750px" 
                            CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVoletEntete" runat="server" Text="1 - " Height="20px" Width="30px"
                                        CssClass="EP_Imp_LargerBold" style="margin-top: 5px; text-indent: 1px;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVolet" runat="server" Text="Description du poste occupé par l'agent" Height="20px" Width="460px"
                                        CssClass="EP_Imp_LargerBoldSouligne" style="margin-top: 5px; text-indent: 1px;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVoletSuite" runat="server" Text="" Height="20px" Width="256px"
                                        CssClass="EP_Imp_SmallItalic" style="margin-top: 10px; text-indent: 1px;">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreTitreStructure" runat="server" Height="20px" CellPadding="0" Width="750px" 
                            CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
			                <asp:TableRow>
                                <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                     <asp:Label ID="O_EtiLabelStructure" runat="server" Height="20px" Width="80px" Text="- structure : "
                                        CssClass="EP_Imp_SmallBold" style="text-indent: 1px;">
                                     </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                     <asp:Label ID="O_LabelEtablissement" runat="server" Height="20px" Width="600px" Text=""
                                        CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                     </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero1" runat="server" Height="20px" Width="125px" Text="- intitulé du poste : "
                                       CssClass="EP_Imp_SmallBold" style="text-indent: 1px;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_InfoH15005" runat="server" Height="20px" Width="610px" Text=""
                                        CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                     </asp:Label>     
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="1px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreEntreeEtablissement" runat="server" Height="20px" Width="135px" Text="- date d'affectation : "
                                       CssClass="EP_Imp_SmallBold" style="text-indent: 1px;">                            
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                     <asp:Label ID="O_LabelEntreeEtablissement" runat="server" Height="20px" Width="600px" Text=""
                                        CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                     </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow>     
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero2" runat="server" Height="20px" Width="250px" Text="- emploi type (cf. REME ou REFERENS) :"
                                       CssClass="EP_Imp_SmallBold" style="text-indent: 1px;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_InfoH15006" runat="server" Height="20px" Width="498px" Text=""
                                        CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                     </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>    
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero31" runat="server" Height="20px" Width="300px" Text="- positionnement du poste dans la structure : "
                                       CssClass="EP_Imp_SmallBold" style="text-indent: 1px;">
                                    </asp:Label>          
                                </asp:TableCell> 
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelAffectationNiveau1" runat="server" Height="20px" Width="498px" Text=""
                                       CssClass="EP_Imp_SmallNormal" style="text-indent: 3px;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero32" runat="server" Height="20px" Width="300px" Text=""
                                       CssClass="EP_Imp_SmallBold" style="text-indent: 1px;">
                                    </asp:Label>          
                                </asp:TableCell>  
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <asp:Label ID="O_LabelAffectationNiveau2" runat="server" Height="20px" Width="498px" Text=""
                                       CssClass="EP_Imp_SmallNormal" style="text-indent: 3px;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>      
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero41" runat="server" Height="20px" Width="748px" Text="- quotité d'affectation : 100"
                                       CssClass="EP_Imp_SmallBold" style="text-indent: 1px;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero42" runat="server" Height="20px" Width="748px" Text="- cotation du poste (part F) :"
                                       CssClass="EP_Imp_SmallBold" style="text-indent: 1px;">
                                    </asp:Label>  
                                </asp:TableCell>
                            </asp:TableRow>      
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero5" runat="server" Height="20px" Width="748px" Text="Missions du poste"
                                       CssClass="EP_Imp_MediumNormal" style="text-indent: 1px;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV07" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="7" V_SiDonneeDico="false"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="4"
                                       Etitext="" EtiVisible="false"
                                       EtiBorderStyle="None" EtiBackColor="Transparent"
		                               Donstyle="margin-left: 1px;"
		                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:Smaller"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>      
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero6" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero6" runat="server" Height="20px" Width="748px" 
                                       Text="le cas échéant, fonctions d'encadrement ou de conduite de projet :"
                                       CssClass="EP_Imp_SmallBold" style="text-indent: 1px; margin-bottom: 1px;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="TableSiConduiteProjet" runat="server" CellPadding="0" CellSpacing="0" Width="748px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                 <asp:Label ID="O_LabelSiConduiteProjet" runat="server" Height="20px" Width="350px"
                                                    Text="- l'agent assume-t-il des fonctions de conduite de projet ?"
                                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                                 </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">        
                                                <Virtualia:VTrioHorizontalRadio ID="O_RadioH25" runat="server" V_Groupe="SiConduiteProjet"
                                                    V_PointdeVue="1" V_Objet="150" V_Information="25" V_SiDonneeDico="false"
                                                    RadioGaucheWidth="60px" RadioCentreWidth="60px" RadioDroiteWidth="0px"  
                                                    RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteVisible="false"
                                                    RadioGaucheHeight="20px" RadioCentreHeight="20px"
                                                    RadioCentreStyle="text-align:  center;"  
                                                    RadioGaucheStyle="text-align:  center;" Visible="true"
			                                        RadioGaucheBackColor="Transparent" RadioCentreBackColor="Transparent"
			                                        RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent"/>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelCadrage2" runat="server" Height="20px" Width="248px"
                                                    BackColor="Transparent" BorderColor="Transparent" BorderStyle="none" Text="">
                                                </asp:Label>          
                                            </asp:TableCell>
                                        </asp:TableRow>      
                                    </asp:Table>
                               </asp:TableCell>  
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="1px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="TableSiEncadrement" runat="server" CellPadding="0" CellSpacing="0" Width="748px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                 <asp:Label ID="O_LabelSiEncadrement" runat="server" Height="20px" Width="350px"
                                                    Text="- l'agent assume-t-il des fonctions d'encadrement ?"
                                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                                 </asp:Label>          
                                           </asp:TableCell>
                                           <asp:TableCell HorizontalAlign="Left">        
                                               <Virtualia:VTrioHorizontalRadio ID="O_RadioH08" runat="server" V_Groupe="SiEncadrement"
                                                   V_PointdeVue="1" V_Objet="150" V_Information="8" V_SiDonneeDico="false"
                                                   RadioGaucheWidth="60px" RadioCentreWidth="60px" RadioDroiteWidth="0px"  
                                                   RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteVisible="false"
                                                   RadioGaucheHeight="20px" RadioCentreHeight="20px"
                                                   RadioCentreStyle="text-align:  center;"  
                                                   RadioGaucheStyle="text-align:  center;" Visible="true"
			                                       RadioGaucheBackColor="Transparent" RadioCentreBackColor="Transparent"
			                                       RadioGaucheBorderColor="Transparent" RadioCentreBorderColor="Transparent"/>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelCadrage1" runat="server" Height="20px" Width="248px"
                                                   BackColor="Transparent" Text="">
                                                </asp:Label>          
                                            </asp:TableCell>
                                        </asp:TableRow>      
                                    </asp:Table>
                                </asp:TableCell>     
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="TableAgentsEncadres" runat="server" CellPadding="0" CellSpacing="0" Width="748px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                 <asp:Label ID="O_LabelNbEncadres" runat="server" Height="20px" Width="225px"
                                                    Text="Si oui, préciser le nombre d'agents : "
                                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                                 </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="O_InfoH15009" runat="server" Width="35px" Height="20px" Text=""
                                                   CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                 <asp:Label ID="O_LabelRepartition" runat="server" Height="20px" Width="210px"
                                                    Text="et leur répartition par catégorie : "
                                                    CssClass="EP_Imp_SmallNormal" style="text-indent: 1px;">
                                                 </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                               <asp:Label ID="O_InfoH15026" runat="server" Width="35px" Height="20px" Text=""
                                                   CssClass="EP_Imp_SmallNormal" style="text-indent: 2px; text-align: right;">
                                               </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelCategorieA" runat="server" Height="24px" Width="20px" Text="A"
                                                   CssClass="EP_Imp_MediumNormal" style="text-indent: 1px; text-align: center;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label ID="O_InfoH15027" runat="server" Height="20px" Width="35px"
                                                   CssClass="EP_Imp_SmallNormal" style="text-indent: 1px; text-align: right;">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelCategorieB" runat="server" Height="24px" Width="20px" Text="B"
                                                   CssClass="EP_Imp_MediumNormal" style="text-align: center;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                               <asp:Label ID="O_InfoH15028" runat="server" Height="20px" Width="35px"
                                                   CssClass="EP_Imp_SmallNormal" style="text-indent: 1px; text-align: right;">
                                               </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelCategorieC" runat="server" Height="24px" Width="20px" Text="C"
                                                   CssClass="EP_Imp_MediumNormal" style="text-align: center;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="O_LabelCadrage3" runat="server" Height="20px" Width="313px"
                                                   BackColor="Transparent" BorderColor="Transparent" BorderStyle="none" Text="">
                                                </asp:Label>          
                                            </asp:TableCell>
                                        </asp:TableRow>      
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
             </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="25px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage1" runat="server" Height="15px" Width="250px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="1 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                 style="text-align:left; text-indent: 2px" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>