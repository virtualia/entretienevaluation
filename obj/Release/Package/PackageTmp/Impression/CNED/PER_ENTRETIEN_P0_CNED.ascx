﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P0_CNED.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P0_CNED" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<asp:Table ID="PAGE_0" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="O_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreTitreEntretien" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#124545">
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Table ID="CadreLogoCNED" runat="server" BackImageUrl="~/Images/General/LogoCNED.jpg" Width="180px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true">
                                    <asp:TableRow>
                                        <asp:TableCell Height="120px" BackColor="Transparent" Width="180px"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="10px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="O_Etiquette" runat="server" Text="COMPTE-RENDU D'ENTRETIEN PROFESSIONNEL" Height="25px" Width="746px"
                                    BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="1px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 1px; margin-left: 1px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEnteteEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                 <asp:Label ID="O_LabelAnneeEntretien" runat="server" Text="" Height="20px" Width="200px"
                                    BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="2px" ForeColor="Black"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 5px; margin-left: 1px; margin-bottom: 5px;
                                    font-style:  normal; text-indent: 2px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreAgent" runat="server" Text="Entre l'agent" Height="20px" Width="350px"
                                    BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="2px" ForeColor="Black"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 5px; margin-left: 1px; margin-bottom: 2px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelIdentite" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Nom et prénom :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelIdentite" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelNaissance" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelNaissance" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelStatut" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Statut :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelStatut" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGrade" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Corps / grade :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGrade" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelEchelon" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Echelon :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelEchelon" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDateEchelon" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Date de l'échelon :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelDateEchelon" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelAffectation" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Direction / service :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelAffectation" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelPoste" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Poste occupé :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelPoste" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow><asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDatePoste" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="depuis le :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 200px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelDatePoste" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                 <asp:Label ID="O_LabelTitreManager" runat="server" Text="et son supérieur hiérarchique direct" Height="30px" Width="350px"
                                    BackColor="Transparent"  BorderStyle="None"
                                    ForeColor="Black"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 20px; margin-left: 1px; margin-bottom: 10px;
                                    font-style:  normal; text-indent: 2px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelManager" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Nom et prénom :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15001" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelGradeManager" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Corps / grade :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelGradeManager" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelFonctionManager" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Fonctions exercées :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelFonctionManager" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelDirectionManager" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="CNED direction :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_InfoH15002" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_EtiLabelSiteGeo" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text="Site :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="O_LabelSiteGeo" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>   
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEntretien" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="O_EtiLabelDateEntretien" runat="server" Height="20px" Width="375px"
                                    BackColor="Transparent" BorderStyle="None" Text="Date de l'entretien :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="O_InfoH15000" runat="server" Height="20px" Width="375px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style:  normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:Table ID="O_CadreEmploi" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                   <asp:TableCell>
                        <asp:Table ID="O_CadreTitreEmploi" runat="server" Height="30px" CellPadding="0" Width="750px" 
                            CellSpacing="0" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="Black">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVoletEntete" runat="server" Text="1 - " Height="20px" Width="30px"
                                        BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger" Font-Underline="false"
                                        style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVolet" runat="server" Text="Description du poste occupé" Height="20px" Width="260px"
                                        BackColor="Transparent"  BorderStyle="None"
                                        ForeColor="Black"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger" Font-Underline="true"
                                        style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelVoletSuite" runat="server" Text="(conforme à la fiche de poste)" Height="20px" Width="456px"
                                        BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 10px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: oblique; text-indent: 1px; text-align: left;">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                        </asp:Table>
                   </asp:TableCell> 
                </asp:TableRow>  
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="3px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero1" runat="server" Height="25px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Intitulé de la fonction"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                                       font-style: normal; text-indent: 1px; text-align:  left; ">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV06" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="6" V_SiDonneeDico="false"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="110px" EtiHeight="30px" DonTabIndex="1" 
                                       Etitext="" EtiVisible="false" 
		                               EtiBorderStyle="None" EtiBackColor="Transparent"
		                               Donstyle="margin-left: 1px;"
		                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>      
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="3px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero2" runat="server" Height="25px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Positionnement du poste dans la structure"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                                       font-style: normal; text-indent: 1px; text-align:  left; ">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV05" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="5" V_SiDonneeDico="false"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="110px" EtiHeight="30px" DonTabIndex="2"
                                       Etitext="" EtiVisible="false" 
		                               EtiBorderStyle="None" EtiBackColor="Transparent"
		                               Donstyle="margin-left: 1px;"
		                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:smaller"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>      
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="3px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                     <asp:Label ID="O_LabelNbEncadres" runat="server" Height="25px" Width="280px"
                                        BackColor="Transparent" BorderStyle="None" Text="Nombre d'agents encadrés (le cas échéant) : "
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                                        font-style:  normal; text-indent: 2px; text-align: left;">
                                     </asp:Label>          
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                     <asp:Label ID="O_InfoH15009" runat="server" Height="25px" Width="470px"
                                        BackColor="Transparent" BorderStyle="None" Text=""
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 2px;
                                        font-style:  normal; text-indent: 1px; text-align: left;">
                                     </asp:Label>          
                            </asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="O_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="2px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="O_LabelTitreNumero3" runat="server" Height="25px" Width="748px"
                                       BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Mission(s) du poste - Activités"
                                       BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                       style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                                       font-style: normal; text-indent: 1px; text-align:  left; ">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="O_InfoV07" runat="server" DonTextMode="true"
                                       V_PointdeVue="1" V_Objet="150" V_Information="7" V_SiDonneeDico="false"
                                       EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="4"
                                       Etitext="Activités principales pour chaque mission"
                                       EtiBorderStyle="None" EtiBackColor="Transparent"
		                               Donstyle="margin-left: 1px;"
		                               Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:80%"
		                               DonBorderWidth="1px" DonBorderColor="Black"/>
                                </asp:TableCell>
                            </asp:TableRow>      
                        </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
             </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage1" runat="server" Height="15px" Width="250px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="1 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>