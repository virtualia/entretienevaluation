﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ENTRETIEN_P1_CNED.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_ENTRETIEN_P1_CNED" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixVerticalRadio.ascx" tagname="VSixVerticalRadio" tagprefix="Virtualia" %>

<div style="page-break-before:always;"></div>

<asp:Table ID="I_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreEnteteDePage1" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelEnteteLigne11" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 4px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelEnteteLigne12" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 4px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreTitreResultats" runat="server" Height="25px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVoletEntete" runat="server" Text="2 - " Height="20px" Width="30px"
                            BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger" Font-Underline="false"
                            style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVolet" runat="server" Text="Résultats professionnels" Height="20px" Width="260px"
                            BackColor="Transparent"  BorderStyle="None"
                            ForeColor="Black"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger" Font-Underline="true"
                            style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelVoletSuite" runat="server" Text="" Height="20px" Width="456px"
                            BackColor="Transparent"  BorderStyle="None" ForeColor="Black"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 10px; margin-left: 0px; margin-bottom: 0px;
                            font-style: oblique; text-indent: 1px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreNumero1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelTitreNumero1" runat="server" Height="20px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="2.1 Evènements survenus au cours de la période écoulée ayant entraîné un impact sur l'activité"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVI03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="20px" DonTabIndex="1" 
                           Etitext="(nouvelles orientations, réorganisations, nouvelles méthodes, nouveaux outils, etc.)"
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:80%"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelTitreNumero2" runat="server" Height="30px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="2.2 Bilan d'activité de la période écoulée"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                           font-style: normal; text-indent: 1px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="4">
                        <asp:Label ID="I_LabelNumerobjectif1" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°1"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="221px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif1" runat="server" Height="28px" Width="221px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteEcheances1" runat="server" CellPadding="0" CellSpacing="0" Width="120px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteEcheances1" runat="server" Height="28px" Width="120px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Echéances de réalisation et indicateurs"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs1" runat="server" CellPadding="0" CellSpacing="0" Width="220px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteIndicateurs1" runat="server" Height="28px" Width="220px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Suivi <br/> des indicateurs"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation1" runat="server" Height="28px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None" 
                                        Text="Bilan des réalisations professionnelles"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif1" runat="server" CellPadding="0" CellSpacing="0" Width="221px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="217px" DonHeight="140px" DonTabIndex="2"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEcheance1" runat="server" CellPadding="0" CellSpacing="0" Width="120px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="116px" DonHeight="140px" DonTabIndex="3"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur1" runat="server" CellPadding="0" CellSpacing="0" Width="220px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="216px" DonHeight="140px" DonTabIndex="4"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation1" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="I_RadioHC04" runat="server" V_Groupe="Realisation1"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="objectif dépassé" VRadioN2Text="objectif atteint" VRadioN3Text="partiellement atteint"
                                            VRadioN4Text="insuffisamment atteint" VRadioN5Text="objectif sans objet" VRadioN6Text=""
                                            VRadioN1Backcolor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                        <asp:Table ID="I_TableEnteteEvenement1" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteEvenement1" runat="server" Height="18px" Width="747px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Evènements survenus durant la période écoulée pouvant expliquer un écart avec les objectifs initiaux"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                        <asp:Table ID="I_TableEvenement1" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVC07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="745px" DonHeight="40px" DonTabIndex="5"
                                   EtiBorderStyle="None" EtiBackColor="Transparent"
                                   Donstyle="margin-left: -1px; margin-top: 0px; font-size:11px"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px"
                                   DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>     
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="4">
                        <asp:Label ID="I_LabelNumerobjectif2" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°2"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="221px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif2" runat="server" Height="28px" Width="221px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteEcheances2" runat="server" CellPadding="0" CellSpacing="0" Width="120px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteEcheances2" runat="server" Height="28px" Width="120px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Echéances de réalisation et indicateurs"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs2" runat="server" CellPadding="0" CellSpacing="0" Width="220px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteIndicateurs2" runat="server" Height="28px" Width="220px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Suivi <br/> des indicateurs"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation2" runat="server" Height="28px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None" 
                                        Text="Bilan des réalisations professionnelles"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif2" runat="server" CellPadding="0" CellSpacing="0" Width="221px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="217px" DonHeight="140px" DonTabIndex="6"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEcheance2" runat="server" CellPadding="0" CellSpacing="0" Width="120px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="116px" DonHeight="140px" DonTabIndex="7"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur2" runat="server" CellPadding="0" CellSpacing="0" Width="220px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="216px" DonHeight="140px" DonTabIndex="8"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation2" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="I_RadioHD04" runat="server" V_Groupe="Realisation2"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="objectif dépassé" VRadioN2Text="objectif atteint" VRadioN3Text="partiellement atteint"
                                            VRadioN4Text="insuffisamment atteint" VRadioN5Text="objectif sans objet" VRadioN6Text=""
                                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                        <asp:Table ID="I_TableEnteteEvenement2" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteEvenement2" runat="server" Height="18px" Width="747px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Evènements survenus durant la période écoulée pouvant expliquer un écart avec les objectifs initiaux"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                        <asp:Table ID="I_TableEvenement2" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVD07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="745px" DonHeight="40px" DonTabIndex="9"
                                   EtiBorderStyle="None" EtiBackColor="Transparent"
                                   Donstyle="margin-left: -1px; margin-top: 0px; font-size:11px"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px"
                                   DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>     
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="4">
                        <asp:Label ID="I_LabelNumerobjectif3" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°3"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="221px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif3" runat="server" Height="28px" Width="221px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteEcheances3" runat="server" CellPadding="0" CellSpacing="0" Width="120px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteEcheances3" runat="server" Height="28px" Width="120px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Echéances de réalisation et indicateurs"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs3" runat="server" CellPadding="0" CellSpacing="0" Width="220px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteIndicateurs3" runat="server" Height="28px" Width="220px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Suivi <br/> des indicateurs"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation3" runat="server" Height="28px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None" 
                                        Text="Bilan des réalisations professionnelles"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif3" runat="server" CellPadding="0" CellSpacing="0" Width="221px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="217px" DonHeight="140px" DonTabIndex="10"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEcheance3" runat="server" CellPadding="0" CellSpacing="0" Width="120px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="116px" DonHeight="140px" DonTabIndex="11"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur3" runat="server" CellPadding="0" CellSpacing="0" Width="220px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="216px" DonHeight="140px" DonTabIndex="12"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation3" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="I_RadioHE04" runat="server" V_Groupe="Realisation3"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="objectif dépassé" VRadioN2Text="objectif atteint" VRadioN3Text="partiellement atteint"
                                            VRadioN4Text="insuffisamment atteint" VRadioN5Text="objectif sans objet" VRadioN6Text=""
                                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                        <asp:Table ID="I_TableEnteteEvenement3" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteEvenement3" runat="server" Height="18px" Width="747px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Evènements survenus durant la période écoulée pouvant expliquer un écart avec les objectifs initiaux"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                        <asp:Table ID="I_TableEvenement3" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVE07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="745px" DonHeight="40px" DonTabIndex="13"
                                   EtiBorderStyle="None" EtiBackColor="Transparent"
                                   Donstyle="margin-left: -1px; margin-top: 0px; font-size:11px"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px"
                                   DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="5px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage2" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="2 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreEnteteDePage2" runat="server" CellPadding="0" CellSpacing="0" Width="748px"
                HorizontalAlign="Center" BackColor="Transparent" BorderColor="Black" BorderStyle="Notset" BorderWidth="1px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelEnteteLigne21" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 4px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>           
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="I_LabelEnteteLigne22" runat="server" Height="20px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" Text=""
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                            style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 4px; text-align: left;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>   
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="4">
                        <asp:Label ID="I_LabelNumerobjectif4" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°4"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="221px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif4" runat="server" Height="28px" Width="221px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteEcheances4" runat="server" CellPadding="0" CellSpacing="0" Width="120px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteEcheances4" runat="server" Height="28px" Width="120px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Echéances de réalisation et indicateurs"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs4" runat="server" CellPadding="0" CellSpacing="0" Width="220px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteIndicateurs4" runat="server" Height="28px" Width="220px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Suivi <br/> des indicateurs"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation4" runat="server" Height="28px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None" 
                                        Text="Bilan des réalisations professionnelles"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif4" runat="server" CellPadding="0" CellSpacing="0" Width="221px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="217px" DonHeight="140px" DonTabIndex="14"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEcheance4" runat="server" CellPadding="0" CellSpacing="0" Width="120px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="116px" DonHeight="140px" DonTabIndex="15"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur4" runat="server" CellPadding="0" CellSpacing="0" Width="220px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="216px" DonHeight="140px" DonTabIndex="16"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation4" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="I_RadioHF04" runat="server" V_Groupe="Realisation4"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="objectif dépassé" VRadioN2Text="objectif atteint" VRadioN3Text="partiellement atteint"
                                            VRadioN4Text="insuffisamment atteint" VRadioN5Text="objectif sans objet" VRadioN6Text=""
                                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                        <asp:Table ID="I_TableEnteteEvenement4" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteEvenement4" runat="server" Height="18px" Width="747px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Evènements survenus durant la période écoulée pouvant expliquer un écart avec les objectifs initiaux"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                        <asp:Table ID="I_TableEvenement4" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVF07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="745px" DonHeight="40px" DonTabIndex="17"
                                   EtiBorderStyle="None" EtiBackColor="Transparent"
                                   Donstyle="margin-left: -1px; margin-top: 0px; font-size:11px"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px"
                                   DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>     
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="4">
                        <asp:Label ID="I_LabelNumerobjectif5" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°5"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="221px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif5" runat="server" Height="28px" Width="221px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteEcheances5" runat="server" CellPadding="0" CellSpacing="0" Width="120px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteEcheances5" runat="server" Height="28px" Width="120px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Echéances de réalisation et indicateurs"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs5" runat="server" CellPadding="0" CellSpacing="0" Width="220px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteIndicateurs5" runat="server" Height="28px" Width="220px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Suivi <br/> des indicateurs"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation5" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation5" runat="server" Height="28px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None" 
                                        Text="Bilan des réalisations professionnelles"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif5" runat="server" CellPadding="0" CellSpacing="0" Width="221px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="217px" DonHeight="140px" DonTabIndex="18"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEcheance5" runat="server" CellPadding="0" CellSpacing="0" Width="120px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="116px" DonHeight="140px" DonTabIndex="19"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur5" runat="server" CellPadding="0" CellSpacing="0" Width="220px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="216px" DonHeight="140px" DonTabIndex="20"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation5" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="I_RadioHG04" runat="server" V_Groupe="Realisation5"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="objectif dépassé" VRadioN2Text="objectif atteint" VRadioN3Text="partiellement atteint"
                                            VRadioN4Text="insuffisamment atteint" VRadioN5Text="objectif sans objet" VRadioN6Text=""
                                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                        <asp:Table ID="I_TableEnteteEvenement5" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteEvenement5" runat="server" Height="18px" Width="747px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Evènements survenus durant la période écoulée pouvant expliquer un écart avec les objectifs initiaux"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                        <asp:Table ID="I_TableEvenement5" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVG07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="745px" DonHeight="40px" DonTabIndex="21"
                                   EtiBorderStyle="None" EtiBackColor="Transparent"
                                   Donstyle="margin-left: -1px; margin-top: 0px; font-size:11px"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px"
                                   DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>     
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreObjectif6" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center" ColumnSpan="4">
                        <asp:Label ID="I_LabelNumerobjectif6" runat="server" Height="25px" Width="748px"
                            BackColor="Transparent" BorderStyle="None" 
                            Text="Objectif n°6"
                            ForeColor="Black" Font-Italic="False"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top: 2px; margin-left: 0px; margin-bottom: 2px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteObjectif6" runat="server" CellPadding="0" CellSpacing="0" Width="221px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteObjectif6" runat="server" Height="28px" Width="221px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Rappel de l'objectif <br/> fixé pour l'année"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteEcheances6" runat="server" CellPadding="0" CellSpacing="0" Width="120px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteEcheances6" runat="server" Height="28px" Width="120px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Echéances de réalisation et indicateurs"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteIndicateurs6" runat="server" CellPadding="0" CellSpacing="0" Width="220px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteIndicateurs6" runat="server" Height="28px" Width="220px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Suivi <br/> des indicateurs"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteRealisation6" runat="server" CellPadding="0" CellSpacing="0" Width="179px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="I_LabelEnteteRealisation6" runat="server" Height="28px" Width="179px"
                                        BackColor="Transparent" BorderStyle="None" 
                                        Text="Bilan des réalisations professionnelles"
                                        ForeColor="Black" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableObjectif6" runat="server" CellPadding="0" CellSpacing="0" Width="221px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH02" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="2" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="217px" DonHeight="140px" DonTabIndex="22"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEcheance6" runat="server" CellPadding="0" CellSpacing="0" Width="120px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH06" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="6" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="116px" DonHeight="140px" DonTabIndex="23"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableIndicateur6" runat="server" CellPadding="0" CellSpacing="0" Width="220px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH03" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="3" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="216px" DonHeight="140px" DonTabIndex="24"
                                   Donstyle="margin-left: 0px; Font-size: 11px;" DonBorderWidth="1px"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableRealisation6" runat="server" CellPadding="0" CellSpacing="0" Width="170px"
                            BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                              <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VSixVerticalRadio ID="I_RadioHH04" runat="server" V_Groupe="Realisation6"
                                            V_PointdeVue="1" V_Objet="151" V_Information="4" V_SiDonneeDico="false"
                                            VRadioN1Width="173px" VRadioN2Width="173px" VRadioN3Width="173px"
                                            VRadioN4Width="173px" VRadioN5Width="173px" VRadioN6Width="173px"
                                            VRadioN1Height="20px" VRadioN2Height="20px" VRadioN3Height="20px"
                                            VRadioN4Height="20px" VRadioN5Height="20px" VRadioN6Height="20px"
                                            VRadioN1Style="margin-left: 0px; margin-top: 9px;" VRadioN2Style="margin-left: 0px; margin-top: 1px;" VRadioN3Style="margin-left: 0px; margin-top: 1px;"
                                            VRadioN4Style="margin-left: 0px; margin-top: 1px;" VRadioN5Style="margin-left: 0px; margin-top: 1px;" VRadioN6Style="margin-left: 0px; margin-top: 0px;" 
                                            VRadioN1Text="objectif dépassé" VRadioN2Text="objectif atteint" VRadioN3Text="partiellement atteint"
                                            VRadioN4Text="insuffisamment atteint" VRadioN5Text="objectif sans objet" VRadioN6Text=""
                                            VRadioN1BackColor="Transparent" VRadioN2Backcolor="Transparent" VRadioN3Backcolor="Transparent" VRadioN4Backcolor="Transparent" VRadioN5Backcolor="Transparent"
                                            VRadioN1BorderColor="Transparent" VRadioN2BorderColor="Transparent" VRadioN3BorderColor="Transparent" VRadioN4BorderColor="Transparent" VRadioN5BorderColor="Transparent"
                                            VRadioN6Visible="false"  
                                            Visible="true"/>
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>        
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                        <asp:Table ID="I_TableEnteteEvenement6" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteEvenement6" runat="server" Height="18px" Width="747px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Evènements survenus durant la période écoulée pouvant expliquer un écart avec les objectifs initiaux"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="60%"
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                        <asp:Table ID="I_TableEvenement6" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVH07" runat="server" DonTextMode="true"
                                   V_PointdeVue="1" V_Objet="151" V_Information="7" V_SiDonneeDico="false"
                                   EtiVisible="false" DonWidth="745px" DonHeight="40px" DonTabIndex="25"
                                   EtiBorderStyle="None" EtiBackColor="Transparent"
                                   Donstyle="margin-left: -1px; margin-top: 0px; font-size:11px"
                                   Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px"
                                   DonBorderWidth="1px" DonBorderColor="Black"/>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>   
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreNumero3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="I_LabelTitreNumero3" runat="server" Height="50px" Width="748px"
                           BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" 
                           Text="2.3 Au-delà des objectifs, quelles ont été les réalisations professionnelles particulières de l'agent au cours de l'année écoulée ?"
                           BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 4px;
                           font-style: normal; text-indent: 2px; text-align:  left; ">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="I_InfoVJ03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="154" V_Information="3" V_SiDonneeDico="false"
                           EtiWidth="740px" DonWidth="740px" DonHeight="200px" EtiHeight="25px" DonTabIndex="26" 
                           Etitext="" EtiVisible="false" 
                           EtiBorderStyle="None" EtiBackColor="Transparent"
                           Donstyle="margin-left: 1px;"
                           Etistyle="margin-left: 1px; text-align:  left; text-indent: 1px; font-size:80%"
                           DonBorderWidth="1px" DonBorderColor="Black"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px"></asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Label ID="NumeroPage3" runat="server" Height="15px" Width="150px"
                 BackColor="Transparent" BorderStyle="None"
                 Text="3 / 13" ForeColor="Black" Font-Italic="True"
                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller" 
                 style="text-align:left" >
            </asp:Label>   
        </asp:TableCell>
    </asp:TableFooterRow>
 </asp:Table>