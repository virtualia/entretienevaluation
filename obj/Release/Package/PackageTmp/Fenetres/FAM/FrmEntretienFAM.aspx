﻿<%@ Page Language="vb" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeBehind="FrmEntretienFAM.aspx.vb" 
    Inherits="Virtualia.Net.FrmEntretienFAM" UICulture="fr" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/Site.master"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Saisies/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>

<%@ Register src="~/Controles/ObjetsFenetres/FAM/PER_ENTRETIEN_APPRECIATION_FAM.ascx" tagname="EVAL_APPRECIATION" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/ObjetsFenetres/FAM/PER_ENTRETIEN_COMPETENCE_FAM.ascx" tagname="EVAL_COMPETENCES" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/ObjetsFenetres/FAM/PER_ENTRETIEN_EMPLOI_FAM.ascx" tagname="EVAL_EMPLOI" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/ObjetsFenetres/FAM/PER_ENTRETIEN_ENTETE_FAM.ascx" tagname="EVAL_ENTETE" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/ObjetsFenetres/FAM/PER_ENTRETIEN_EVOLUTION_FAM.ascx" tagname="EVAL_EVOLUTION" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/ObjetsFenetres/FAM/PER_ENTRETIEN_FORMATION_FAM.ascx" tagname="EVAL_FORMATION" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/ObjetsFenetres/FAM/PER_ENTRETIEN_OBJECTIFS_FAM.ascx" tagname="EVAL_OBJECTIFS" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/ObjetsFenetres/FAM/PER_ENTRETIEN_RESULTATS_FAM.ascx" tagname="EVAL_RESULTATS" tagprefix="Virtualia" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanelSaisie" runat="server">
        <ContentTemplate>
          <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="950px" HorizontalAlign="Center"
                     style="margin-top : 0px" >
             <asp:TableRow>
                <asp:TableCell>
                    <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="800px" HorizontalAlign="Left"
                           CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Onglets/Onglet_2bords_100.bmp"
                           style="margin-top: 10px; border-top-color: #124545;
                            border-bottom-color: #B0E0D7; border-bottom-style: groove" >
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="98px">
                            <asp:Button ID="BoutonN1" runat="server" Height="24px" Width="85px" 
                                ForeColor="White" Text="Entretien" Font-Bold="true" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="#7D9F99" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="98px">
                            <asp:Button ID="BoutonN2" runat="server" Height="33px" Width="98px" 
                                ForeColor="#142425" Text="Emploi" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="98px">
                            <asp:Button ID="BoutonN3" runat="server" Height="33px" Width="98px" 
                                ForeColor="#142425" Text="Résultats" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None"
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="98px">
                            <asp:Button ID="BoutonN4" runat="server" Height="33px" Width="98px" 
                                ForeColor="#142425" Text="Objectifs" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="98px">
                            <asp:Button ID="BoutonN5" runat="server" Height="33px" Width="98px" 
                                ForeColor="#142425" Text="Compétences" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="98px">
                            <asp:Button ID="BoutonN6" runat="server" Height="33px" Width="98px" 
                                ForeColor="#142425" Text="Formation" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="98px">
                            <asp:Button ID="BoutonN7" runat="server" Height="33px" Width="98px" 
                                ForeColor="#142425" Text="Perspectives" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center; overflow: auto" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="98px">
                            <asp:Button ID="BoutonN8" runat="server" Height="33px" Width="98px" 
                                ForeColor="#142425" Text="Appréciation" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                    </asp:TableRow>
                  </asp:Table>
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
            <asp:Table ID="CadreSaisie" runat="server" Width="950px" HorizontalAlign="Center"
                     BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" CellSpacing="2"
                     style="margin-top: 0px " >
               <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDateJour" runat="server" Width="750px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left"> 
                                    <asp:Label ID="EtiDateJour" runat="server" BackColor="Transparent" 
                                                Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#124545"
                                                ToolTip="Enregistrement automatique toutes les minutes"
                                                Text="" Height="15px" Width="200px" Font-Bold="False" Font-Italic="true">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left"> 
                                    <asp:Label ID="EtiHeure" runat="server" BackColor="Transparent" 
                                                Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#124545"
                                                ToolTip="Enregistrement automatique toutes les minutes"
                                                Text="" Height="15px" Width="400px" Font-Bold="False" Font-Italic="true"
                                                style="text-align:left" >
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Right"> 
                                    <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
                                            CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="true"
                                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                                            Width="80px" HorizontalAlign="Center" style="margin-top: 3px; margin-right:3px" >
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Bottom">
                                                    <asp:Button ID="CommandeOK" runat="server" Text="Enregistrer" Width="75px" Height="20px"
                                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                                    </asp:Button>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell Width="70px" HorizontalAlign="Right"> 
                                   <asp:Button ID="CommandeImprimer" runat="server" Width="60px" Height="23px"
                                        BackColor="#7D9F99" ForeColor="#D7FAF3" ToolTip="Aperçu complet avant édition"
                                        BorderStyle="Groove" BorderColor="#FFEBC8" Text="Aperçu" style="margin-top:2px">
                                   </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                   <asp:TableCell ID="ConteneurVues" Width="820px" VerticalAlign="Top" HorizontalAlign="Center">
                       <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                          <asp:View ID="VueEntete" runat="server">  
                              <Virtualia:EVAL_ENTETE ID="ENT_ENTETE" runat="server" 
                              CadreStyle="margin-bottom: 20px" /> 
                          </asp:View>
                          <asp:View ID="VueEmploi" runat="server">  
                              <Virtualia:EVAL_EMPLOI ID="ENT_EMPLOI" runat="server" 
                                CadreStyle="margin-bottom: 20px" />
                          </asp:View>
                          <asp:View ID="VueResultats" runat="server">  
                              <Virtualia:EVAL_RESULTATS ID="ENT_RESULTATS" runat="server" 
                                CadreStyle="margin-bottom: 20px" />
                          </asp:View>
                          <asp:View ID="VueObjectifs" runat="server">  
                              <Virtualia:EVAL_OBJECTIFS ID="ENT_OBJECTIFS" runat="server" 
                                  CadreStyle="margin-bottom: 20px" />
                          </asp:View>
                          <asp:View ID="VueCompetence" runat="server">
                             <Virtualia:EVAL_COMPETENCES ID="ENT_COMPETENCES" runat="server" 
                              CadreStyle="margin-top: 10px" />           
                          </asp:View>
                           <asp:View ID="VueFormation" runat="server">
                              <Virtualia:EVAL_FORMATION ID="ENT_FORMATION" runat="server" 
                                CadreStyle="margin-bottom: 20px" />
                          </asp:View>
                          <asp:View ID="VueEvolution" runat="server">  
                              <Virtualia:EVAL_EVOLUTION ID="ENT_EVOLUTION" runat="server" 
                                 CadreStyle="margin-bottom: 20px" />
                          </asp:View>     
                          <asp:View ID="VueAppreciation" runat="server"> 
                              <Virtualia:EVAL_APPRECIATION ID="ENT_APPRECIATION" runat="server" />
                          </asp:View>            
                      </asp:MultiView>
                  </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>
                <asp:TableCell>
                    <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
                </asp:TableCell>
              </asp:TableRow>
                <asp:TableRow>
                 <asp:TableCell ID="CellMessage" Visible="false">
                    <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
                    <asp:Panel ID="PanelMsgPopup" runat="server">
                        <Virtualia:VMessage id="MsgVirtualia" runat="server" />
                    </asp:Panel>
                    <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
                </asp:TableCell>
            </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>