﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmProposition.aspx.vb" Inherits="Virtualia.Net.FrmProposition" UICulture="Fr" %>

<%@ Register src="~/Impression/CNED2/PER_ENTRETIEN_P7_MEN.ascx" tagname="ENT_P7" tagprefix="Virtualia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Formulaire Entretien</title>
</head>
<body>
    <form id="FrmEntretien" runat="server" style="max-width: 800px">
    <div style="margin-left : 10px">
        <asp:ImageButton ID="CommandePDF" runat="server" ImageUrl="~/Images/General/PDF_on.gif"
                     ToolTip="Editer le formulaire au format PDF" />
    </div>
    <div id="Div1" runat="server" style="width: 750px;">
        <asp:Table runat="server" ID="CadreApercu" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:ENT_P7 ID="ENTRETIEN_7" runat="server"/>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
    </div>
    </form>
</body>
</html>
