﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class EnsembleExtractHonda
        Private AppObjetGlobal As Virtualia.Net.WebAppli.ObjetGlobal
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
        '
        Private WsListeAllFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Private WsListeEtatCivil As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = Nothing
        Private WsListeDossier As List(Of Virtualia.Net.Entretien.DossierHonda)
        Private WsListeResultat As List(Of Virtualia.Net.Entretien.DossierHonda)
        '
        Private WsSeparateur As String = VI.PointVirgule

        Public ReadOnly Property NomFichier(ByVal NoExtraction As Integer) As String
            Get
                Dim LibelFichier As String = "VirtualiaInfosGen" '0
                Select Case NoExtraction
                    Case 1
                        LibelFichier = "VirtualiaPerformance"
                    Case 2
                        LibelFichier = "VirtualiaComportement"
                    Case 3
                        LibelFichier = "VirtualiaFormation"
                End Select
                'Return LibelFichier & ".txt"
                Return LibelFichier & ".csv"
            End Get
        End Property

        Public ReadOnly Property Fichier_Txt_Corps(ByVal NoExtraction As Integer) As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                Chaine = New System.Text.StringBuilder

                Select Case NoExtraction
                    Case 0 ' Info Générales
                        Return ""
                    Case 1 'Performance
                        For IndiceI = 1 To 5
                            Chaine.Append(VI.DoubleQuote & "P_Intitule_Objectif" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "P_Note_Finale_Manager" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "P_Note_Finale_Evalue" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 2 'Comportement
                        For IndiceI = 1 To 5
                            Chaine.Append(VI.DoubleQuote & "C_Comportement_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "C_Note_Manager" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "C_Note_Evalue" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 3 'Formation
                        For IndiceI = 1 To 30
                            Chaine.Append(VI.DoubleQuote & "F_Intitule_Besoin" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "F_Domaine_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "F_Validation_N1" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "F_Validation_DRH" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                End Select

                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property Fichier_Txt_Corps(ByVal Dossier As Virtualia.Net.Entretien.DossierHonda, ByVal NoExtraction As Integer) As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                Chaine = New System.Text.StringBuilder

                Select Case NoExtraction
                    Case 0 'Infos Gen
                        Return ""
                    Case 1 'Performance
                        For IndiceI = 0 To Dossier.ListeObjet151.Count - 1
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet151.Item(IndiceI).Intitule_Objectif & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet151.Item(IndiceI).Note_Finale_Manager & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet151.Item(IndiceI).Note_Finale_Self & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 2 'Comportement
                        For IndiceI = 0 To Dossier.ListeObjet152.Count - 1
                            Chaine.Append(VI.DoubleQuote & Strings.Replace(Dossier.ListeObjet152.Item(IndiceI).Intitule, vbCrLf, Strings.Space(1)) & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet152.Item(IndiceI).Note_Manager & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet152.Item(IndiceI).Note_Self & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 3 'Formation
                        For IndiceI = 0 To Dossier.ListeObjet153.Count - 1
                            Chaine.Append(VI.DoubleQuote & Strings.Replace(Dossier.ListeObjet153.Item(IndiceI).Intitule_Besoin, vbCrLf, Strings.Space(1)) & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet153.Item(IndiceI).Domaine & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Strings.Replace(Dossier.ListeObjet153.Item(IndiceI).Date_Validation_N1, vbCrLf, Strings.Space(1)) & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet153.Item(IndiceI).Date_Validation_DRH & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                End Select

                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property Fichier_Txt_Commun As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder
                Chaine.Append(VI.DoubleQuote & "Identifiant" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Qualite" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Nom" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Prenom" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Niveau_1" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Niveau_2" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Niveau_3" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Niveau_4" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Fonction" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Fonction" & VI.DoubleQuote & WsSeparateur)

                Chaine.Append(VI.DoubleQuote & "KI" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Type_Formulaire" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Entretien" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Evaluateur" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Entretien" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Signature_Manager_KI1" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Signature_Evalue_KI1" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Signature_Manager_KI2" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Signature_Evalue_KI2" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Signature_Manager_KI3" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Signature_Evalue_KI3" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Note_Performance_Manager" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Note_Comportement_Manager" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Note_Globale_Manager" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Note_Lettre_Manager" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Note_Performance_Evalue" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Note_Comportement_Evalue" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Note_Globale_Evalue" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Note_Lettre_Revisee" & VI.DoubleQuote & WsSeparateur)

                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property Fichier_Txt_Commun(ByVal Dossier As Virtualia.Net.Entretien.DossierHonda) As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Dim DatedEffet As String

                DatedEffet = Dossier.Objet_150.Date_de_Valeur

                Chaine = New System.Text.StringBuilder
                Chaine.Append(VI.DoubleQuote & Dossier.V_Identifiant & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Qualite & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Nom & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Prenom & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.NiveauAffectation(DatedEffet, 1) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.NiveauAffectation(DatedEffet, 2) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.NiveauAffectation(DatedEffet, 3) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.NiveauAffectation(DatedEffet, 4) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.FonctionExercee(DatedEffet, False) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.FonctionExercee(DatedEffet, True) & VI.DoubleQuote & WsSeparateur)

                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.KI & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Type_Formulaire & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Date_Entretien & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Evaluateur_Nom_Prenom & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Date_Entretien & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Date_de_Signature_Manager_KI1 & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Date_de_Signature_Evalue_KI1 & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Date_de_Signature_Manager_KI2 & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Date_de_Signature_Evalue_KI2 & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Date_de_Signature_Manager_KI3 & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Date_de_Signature_Evalue_KI3 & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Note_Performance_Manager & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Note_Comportement_Manager & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Note_Globale_Manager & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Note_Lettre_Manager & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Note_Performance_Self & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Note_Comportement_Self & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Note_Globale_Self & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Note_Lettre_Revisee & VI.DoubleQuote & WsSeparateur)
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property ListeResultats As List(Of Virtualia.Net.Entretien.DossierHonda)
            Get
                Return WsListeResultat
            End Get
        End Property

        Public ReadOnly Property ListeDossiers(ByVal Criteres As List(Of String)) As List(Of Virtualia.Net.Entretien.DossierHonda)
            Get
                Dim IndiceA As Integer
                Dim IndiceT As Integer
                Dim IndexCritere As Integer
                Dim TableauData(0) As String
                Dim ListeTravail As List(Of Virtualia.Net.Entretien.DossierHonda) = Nothing
                Dim DateValeur As String = "31/12/" & Year(Now)
                Dim ValeurLue As String
                Dim ICompetences As Integer = -1
                Dim ISynthese As Integer = -1

                WsListeResultat = Nothing
                If Criteres IsNot Nothing Then
                    For Each It In Criteres
                        If It <> "" Then
                            TableauData = Strings.Split(It, VI.Tild, -1)
                            IndexCritere = CInt(TableauData(0))
                            If IndexCritere = 1 Then
                                DateValeur = "31/12/" & TableauData(1)
                                Exit For
                            End If
                        End If
                    Next
                End If
                WsListeResultat = (From dossier In WsListeDossier Select dossier Where dossier.Objet_150(Strings.Right(DateValeur, 4)) IsNot Nothing
                               Order By dossier.V_Identifiant Ascending).ToList

                For IndiceA = 0 To WsListeResultat.Count - 1
                    WsListeResultat.Item(IndiceA).Annee = CDate(DateValeur).Year
                Next IndiceA

                If Criteres Is Nothing Then
                    Return WsListeResultat
                End If

                For Each It In Criteres
                    If It <> "" Then
                        TableauData = Strings.Split(It, VI.Tild, -1)
                        IndexCritere = CInt(TableauData(0))
                        Select Case IndexCritere
                            Case 0 'KI
                                ListeTravail = New List(Of Virtualia.Net.Entretien.DossierHonda)
                                For Each dossier In WsListeResultat
                                    For IndiceT = 1 To TableauData.Count - 1
                                        If TableauData(IndiceT) = "" Then
                                            Exit For
                                        End If
                                        If dossier.Objet_150.KI = TableauData(IndiceT) Then
                                            ListeTravail.Add(dossier)
                                            Exit For
                                        End If
                                    Next IndiceT
                                Next
                                WsListeResultat = ListeTravail

                            Case 2 'Nom et Prénom
                                ListeTravail = New List(Of Virtualia.Net.Entretien.DossierHonda)
                                For Each dossier In WsListeResultat
                                    For IndiceT = 1 To TableauData.Count - 1
                                        If TableauData(IndiceT) = "" Then
                                            Exit For
                                        End If
                                        If dossier.Nom & Strings.Space(1) & dossier.Prenom = TableauData(IndiceT) Then
                                            ListeTravail.Add(dossier)
                                            Exit For
                                        End If
                                    Next IndiceT
                                Next
                                WsListeResultat = ListeTravail

                            Case 3 To 4
                                ListeTravail = New List(Of Virtualia.Net.Entretien.DossierHonda)
                                For Each dossier In WsListeResultat
                                    DateValeur = dossier.Objet_150.Date_de_Valeur
                                    ValeurLue = ""
                                    Select Case IndexCritere
                                        Case 3 'Type du formulaire
                                            ValeurLue = dossier.Objet_150.Type_Formulaire
                                        Case 4 'Manager
                                            ValeurLue = dossier.Evaluateur_Nom_Prenom
                                    End Select
                                    For IndiceT = 1 To TableauData.Count - 1
                                        If TableauData(IndiceT) = "" Then
                                            Exit For
                                        End If
                                        If ValeurLue = TableauData(IndiceT) Then
                                            ListeTravail.Add(dossier)
                                            Exit For
                                        End If
                                    Next IndiceT
                                Next
                                WsListeResultat = ListeTravail
                        End Select

                    End If
                    If WsListeResultat Is Nothing Then
                        Exit For
                    End If
                Next
                Return WsListeResultat
            End Get
        End Property

        Public ReadOnly Property ListeDossiers As List(Of Virtualia.Net.Entretien.DossierHonda)
            Get
                Return WsListeDossier
            End Get
        End Property

        Public ReadOnly Property ItemDossier(ByVal Ide As Integer) As Virtualia.Net.Entretien.DossierHonda
            Get
                For Each Fiche In WsListeDossier
                    Select Case Fiche.Objet_150.Ide_Dossier
                        Case Is = Ide
                            Return CType(Fiche, Virtualia.Net.Entretien.DossierHonda)
                            Exit Property
                    End Select
                Next
                Return Nothing
            End Get
        End Property

        Public Property Separateur As String
            Get
                Return WsSeparateur
            End Get
            Set(ByVal value As String)
                WsSeparateur = value
            End Set
        End Property

        Private Sub LireToutesFiches(ByVal LstObjets As List(Of Integer))
            Dim ToutUnObjet As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceI As Integer
            For IndiceI = 0 To LstObjets.Count - 1
                ToutUnObjet = ExtractionEntretiens(LstObjets.Item(IndiceI))
                WsListeAllFiches.AddRange(ToutUnObjet)
            Next IndiceI

        End Sub

        Private ReadOnly Property SiCritereOK(ByVal LstCriteres As List(Of String), ByVal Index As Integer, ByVal Valeur As String) As Boolean
            Get
                If LstCriteres Is Nothing Then
                    Return False
                End If
                Dim TableauData(0) As String
                Dim IndiceT As Integer

                TableauData = Strings.Split(LstCriteres.Item(Index).ToString, VI.Tild, -1)
                For IndiceT = 1 To TableauData.Count - 1
                    If TableauData(IndiceT) = "" Then
                        Exit For
                    End If
                    If TableauData(IndiceT) = Valeur Then
                        Return True
                    End If
                Next IndiceT
                Return False
            End Get
        End Property

        Private ReadOnly Property ListeEtatCivil As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Get
                If WsListeEtatCivil Is Nothing Then
                    WsListeEtatCivil = (From Fiche In WsListeAllFiches Where Fiche.NumeroObjet = 1 Order By Fiche.Ide_Dossier).ToList
                End If
                Return WsListeEtatCivil
            End Get
        End Property

        Public ReadOnly Property Nombre_Total_FichesEtatcivil As Integer
            Get
                Return ListeEtatCivil.Count
            End Get
        End Property

        Public Function ConstituerListeDossiersPER() As Boolean
            Dim Ide As Integer
            Dim DossierPER As Virtualia.Net.Entretien.DossierHonda
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

            If ListeEtatCivil Is Nothing Then
                Return False
            End If
            Call LireAllFichesPER()
            WsListeDossier = New List(Of Virtualia.Net.Entretien.DossierHonda)
            For Each FichePER In WsListeEtatCivil
                Ide = FichePER.Ide_Dossier
                LstFiches = (From Fiches In WsListeAllFiches Where Fiches.Ide_Dossier = Ide).ToList
                If LstFiches IsNot Nothing Then
                    DossierPER = New Virtualia.Net.Entretien.DossierHonda(AppObjetGlobal, Ide, LstFiches, "Virtualia")
                    WsListeDossier.Add(DossierPER)
                End If
            Next
            Return True
        End Function

        Public Sub LireAllFichesPER()
            Dim TabObjets As New List(Of Integer)
            Dim IndiceI As Integer
            TabObjets.Add(VI.ObjetPer.ObaSociete)
            TabObjets.Add(VI.ObjetPer.ObaOrganigramme)
            For IndiceI = 150 To 153
                TabObjets.Add(IndiceI)
            Next IndiceI
            Call LireToutesFiches(TabObjets)
        End Sub

        Public ReadOnly Property ExtractionEntretiens(ByVal NoObjet As Integer) As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim OrdreSql As String
                Dim IndiceA As Integer
                Dim DicoInfos As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
                Dim NbExtraits As Integer

                DicoInfos = (From Info In AppObjetGlobal.VirListeInfosDico Where Info.PointdeVue = VI.PointdeVue.PVueApplicatif And Info.Objet = NoObjet Order By Info.Information).ToList
                If DicoInfos Is Nothing Then
                    Return Nothing
                End If
                NbExtraits = DicoInfos.Count
                Select Case NoObjet
                    Case VI.ObjetPer.ObaGrade, VI.ObjetPer.ObaGradeDetache
                        NbExtraits -= 2
                End Select

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(AppObjetGlobal.VirModele, AppObjetGlobal.PointeurObjetUtiGlobal.InstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.NoInfoSelection(0, 150) = 0
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Inclu, False) = "01/01/2008;31/12/" & Year(Now)
                For IndiceA = 0 To NbExtraits - 1
                    Constructeur.InfoExtraite(IndiceA, NoObjet, 0) = DicoInfos.Item(IndiceA).Information
                Next IndiceA
                OrdreSql = Constructeur.OrdreSqlDynamique
                Constructeur = Nothing
                Return AppObjetGlobal.VirServiceServeur.RequeteSql_ToFiches(AppObjetGlobal.PointeurObjetUtiGlobal.Nom, VI.PointdeVue.PVueApplicatif, NoObjet, OrdreSql)
            End Get
        End Property

        Public Sub New(ByVal PointeurGlobal As Virtualia.Net.WebAppli.ObjetGlobal)
            Dim TabObjets As New List(Of Integer)
            AppObjetGlobal = PointeurGlobal
            WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
            WsListeAllFiches = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            TabObjets.Add(VI.ObjetPer.ObaCivil)
            Call LireToutesFiches(TabObjets)
            Dim SiOK As Boolean = ConstituerListeDossiersPER()
        End Sub

    End Class
End Namespace
