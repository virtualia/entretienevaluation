﻿Public Class VirtualiaMain
    Inherits System.Web.UI.MasterPage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        EtiDateJour.Text = Format(System.DateTime.Now, "D")
        EtiHeure.Text = Format(TimeOfDay, "t")
    End Sub
End Class