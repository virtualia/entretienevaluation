﻿<%@ Page Language="vb" MasterPageFile="~/Extractions/VirtualiaMain.master" AutoEventWireup="false" CodeBehind="FrmExtractHonda.aspx.vb" 
    Inherits="Virtualia.Net.FrmExtractHonda" UICulture="fr" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/Extractions/VirtualiaMain.master"  %>

<asp:Content ID="Corps" runat="server" ContentPlaceHolderID="Principal">
    <asp:ScriptManager ID="ScriptManagerOK" runat="server" EnablePartialRendering="true" AsyncPostBackTimeout="180" />
    <asp:UpdatePanel ID="UpdatePanelOK" runat="server">
        <Triggers>
                  <asp:PostBackTrigger ControlID="CommandeFichier" />
        </Triggers>
        <ContentTemplate>
            <asp:Table ID="CadreHaut" runat="server" Width="1050px" Height="700px" HorizontalAlign="Center"
                            BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" BorderStyle="Groove" BorderColor="Aqua" >
                 <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreListes" runat="server" Width="750px" Height="200px" Font-Names="Trebuchet MS"
                                Font-Size="Small" HorizontalAlign="Center" BackColor="Transparent">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="LabelLstExtraction" runat="server" Height="20px" Width="650px"
                                        BackColor="#1C5150" BorderColor="#B0E0D7" BorderStyle="Groove" Text="Choix d'un type d'extraction"
                                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                        style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>          
                                </asp:TableCell> 
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:ListBox ID="ListeExtraction" runat="server" Height="170px" 
                                        Width="370px" BackColor="#CAEBE4" ForeColor="#142425"  
                                        AppendDataBoundItems="False" AutoPostBack="True" Font-Bold="False" 
                                        Font-Size="Medium" Font-Names="Trebuchet MS">
                                        <asp:ListItem>Informations générales</asp:ListItem>
                                        <asp:ListItem>Résultats professionnels</asp:ListItem>
                                        <asp:ListItem>Comportement</asp:ListItem>
                                        <asp:ListItem>Besoins en formation</asp:ListItem>
                                    </asp:ListBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelLstSel" runat="server" Height="20px" Width="370px"
                                        BackColor="#1C5150" BorderColor="#B0E0D7" BorderStyle="Groove" Text="Critères"
                                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                        style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>
                                 <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelLstValeur" runat="server" Height="20px" Width="430px"
                                        BackColor="#1C5150" BorderColor="#B0E0D7" BorderStyle="Groove" Text="Valeurs possibles"
                                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                        style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>  
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <div style="width:430px; height: 230px; overflow: auto;" >
                                        <asp:ListBox ID="ListeSelection" runat="server" Height="230px" 
                                            Width="370px" BackColor="#CAEBE4" ForeColor="#142425" 
                                            AppendDataBoundItems="False" AutoPostBack="True" Font-Bold="False" >
                                            <asp:ListItem>KI</asp:ListItem>
                                            <asp:ListItem>Année de l'entretien</asp:ListItem>
                                            <asp:ListItem>Nom et prénom</asp:ListItem>
                                            <asp:ListItem>Type du formulaire</asp:ListItem>
                                            <asp:ListItem>Manager</asp:ListItem>
                                        </asp:ListBox>
                                      </div>          
                               </asp:TableCell>
                               <asp:TableCell HorizontalAlign="Left" BackColor="#CAEBE4">
                                    <div style="width:430px; height: 230px; overflow: auto;" >
                                        <asp:CheckBoxList ID="ListeValeur" runat="server" Height="230px" 
                                            Width="370px" BackColor="#CAEBE4" ForeColor="#142425" 
                                            RepeatLayout="Flow" AutoPostBack="True" Font-Bold="False" >
                                            <asp:ListItem>2014</asp:ListItem>
                                        </asp:CheckBoxList>
                                      </div>          
                               </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="LabelResume" runat="server" Height="20px" Width="650px"
                                        BackColor="#1C5150" BorderColor="#B0E0D7" BorderStyle="Groove" Text="Résumé de la sélection"
                                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="False"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                        style="margin-top: 8px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: normal; text-indent: 1px; text-align:  center;">
                                    </asp:Label>          
                                </asp:TableCell>
                            </asp:TableRow> 
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:TextBox ID="TxtResume" runat="server" Height="120px" Width="650px"
                                        TextMode="MultiLine"  >
                                    </asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <asp:Table ID="CadreAttente" runat="server" HorizontalAlign="Center" Width="800px" Height="40px">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2" Width="400px" HorizontalAlign="Center">
                                    <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                                        <ProgressTemplate>
                                           <img alt="" src="../Images/General/Loading.gif"  />
                                              Traitement en cours ......
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                               </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Button ID="CommandeInit" runat="server" Text="Réinitialiser la sélection" 
                                        Width="280px" ForeColor="#142425" BackColor="#B0E0D7" BorderColor="#B0E0D7">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Button ID="CommandeOK" runat="server" Text="Exécuter l'extraction" 
                                        Width="280px" ForeColor="#142425" BackColor="#B0E0D7" BorderColor="#B0E0D7">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell  HorizontalAlign="Center">
                                    <asp:Label ID="EtiNbDossiers" runat="server" BorderStyle="Ridge" Text="" Height="24px"
                                        Width="300px" BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#124545"
                                            style="text-align: center">
                                    </asp:Label> 
                                </asp:TableCell>
                                <asp:TableCell Width="400px" HorizontalAlign="Center">
                                        <asp:Button ID="CommandeFichier" runat="server" Text="" BorderStyle="None"  Height="24px"
                                            Width="380px" ForeColor="Blue" BackColor="transparent" BorderColor="#B0E0D7"
                                            Font-Italic="true" Font-Bold="true" Font-Underline="true"  style="text-align: center">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                       </asp:Table>
                    </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                   <asp:TableCell>
                      <asp:HiddenField ID="HSelExtrait" runat="server" Value="0" />
                      <asp:HiddenField ID="HSelCritere" runat="server" Value="0" />
                   </asp:TableCell>
                </asp:TableRow> 
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

