﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Entretien
    Public Class EnsembleExtraction
        Private AppObjetGlobal As Virtualia.Net.WebAppli.ObjetGlobal
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
        '
        Private WsListeAllFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Private WsListeEtatCivil As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = Nothing
        Private WsListeDossier As List(Of Virtualia.Net.Entretien.DossierEntretien)
        Private WsListeResultat As List(Of Virtualia.Net.Entretien.DossierEntretien)
        '
        Private WsSeparateur As String = VI.PointVirgule

        Public ReadOnly Property NomFichier(ByVal NoExtraction As Integer) As String
            Get
                Dim LibelFichier As String = "VirtualiaInfosGen" '0
                Select Case NoExtraction
                    Case 1
                        LibelFichier = "VirtualiaResultats"
                    Case 2
                        LibelFichier = "VirtualiaObjectifs"
                    Case 3
                        LibelFichier = "VirtualiaCompetences"
                    Case 4
                        LibelFichier = "VirtualiaPerspectives"
                    Case 5
                        LibelFichier = "VirtualiaFormation"
                    Case 6
                        LibelFichier = "VirtualiaSynthese"
                End Select
                'Return LibelFichier & ".txt"
                Return LibelFichier & ".csv"
            End Get
        End Property

        Public ReadOnly Property Fichier_Txt_Corps(ByVal NoExtraction As Integer) As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                Chaine = New System.Text.StringBuilder

                Select Case NoExtraction
                    Case 0 ' Info Générales
                        Return ""
                    Case 1 'Résultats
                        For IndiceI = 1 To 5
                            Chaine.Append(VI.DoubleQuote & "R_Resultat_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "R_Critere_Num_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "R_Critere_Alpha_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 2 'Objectifs
                        For IndiceI = 1 To 5
                            Chaine.Append(VI.DoubleQuote & "O_Objectif_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "O_Delai_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 3 'Compétences
                        For IndiceI = 1 To 30
                            Chaine.Append(VI.DoubleQuote & "C_Competence_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "C_Critere_Num_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "C_Critere_Alpha_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 4 'Perspectives
                        For IndiceI = 1 To 4
                            Chaine.Append(VI.DoubleQuote & "P_Perspective_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "P_Texte_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 5 'Formation
                        For IndiceI = 1 To 16
                            Chaine.Append(VI.DoubleQuote & "F_Formation_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "F_Type_Besoin_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "F_Expression_Besoin_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "F_Periode_Souhaitee" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "F_Objectif_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "F_Avis_Evaluateur" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "F_Echeance" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 6 'Synthese
                        For IndiceI = 1 To 4
                            Chaine.Append(VI.DoubleQuote & "S_Intitule_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "S_Critere_Num_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & "S_Critere_Alpha_" & IndiceI.ToString & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                End Select

                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property Fichier_Txt_Corps(ByVal Dossier As Virtualia.Net.Entretien.DossierEntretien, ByVal NoExtraction As Integer) As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                Chaine = New System.Text.StringBuilder

                Select Case NoExtraction
                    Case 0 'Infos Gen
                        Return ""
                    Case 1 'Résultats
                        For IndiceI = 0 To Dossier.ListeObjet151.Count - 1
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet151.Item(IndiceI).Intitule & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet151.Item(IndiceI).Critere_Numerique.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet151.Item(IndiceI).Critere_Litteral & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 2 'Objectifs
                        For IndiceI = 0 To Dossier.ListeObjet152.Count - 1
                            Chaine.Append(VI.DoubleQuote & ChaineObservation_ToCsv(Dossier.ListeObjet152.Item(IndiceI).Intitule) & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet152.Item(IndiceI).Delai & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 3 'Compétences
                        For IndiceI = 0 To Dossier.ListeObjet153.Count - 1
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet153.Item(IndiceI).Competence & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet153.Item(IndiceI).Critere_Numerique.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet153.Item(IndiceI).Critere_Litteral & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 4 'Perspectives
                        For IndiceI = 0 To Dossier.ListeObjet154.Count - 1
                            Select Case AppObjetGlobal.VirTypeEntretien
                                Case "CE"
                                    Select Case Dossier.ListeObjet154.Item(IndiceI).Numero_Famille
                                        Case "5.1", "5.2.1", "5.2.2", "5.3"
                                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet154.Item(IndiceI).Intitule_Famille & VI.DoubleQuote & WsSeparateur)
                                            Chaine.Append(VI.DoubleQuote & ChaineObservation_ToCsv(Dossier.ListeObjet154.Item(IndiceI).Observation) & VI.DoubleQuote & WsSeparateur)
                                    End Select
                                Case "CESE"
                                    Select Case Dossier.ListeObjet154.Item(IndiceI).Numero_Famille
                                        Case "5.2", "6.1", "6.2", "6.3", "6.4", "6.5"
                                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet154.Item(IndiceI).Intitule_Famille & VI.DoubleQuote & WsSeparateur)
                                            Chaine.Append(VI.DoubleQuote & ChaineObservation_ToCsv(Dossier.ListeObjet154.Item(IndiceI).Observation) & VI.DoubleQuote & WsSeparateur)
                                    End Select
                                Case Else
                                    Select Case Dossier.ListeObjet154.Item(IndiceI).Numero_Famille
                                        Case "5.1"
                                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet154.Item(IndiceI).Intitule_Famille & VI.DoubleQuote & WsSeparateur)
                                            Chaine.Append(VI.DoubleQuote & ChaineObservation_ToCsv(Dossier.ListeObjet154.Item(IndiceI).Observation) & VI.DoubleQuote & WsSeparateur)
                                    End Select
                            End Select
                        Next IndiceI

                    Case 5 'Formation
                        For IndiceI = 0 To Dossier.ListeObjet155.Count - 1
                            Chaine.Append(VI.DoubleQuote & ChaineObservation_ToCsv(Dossier.ListeObjet155.Item(IndiceI).Intitule) & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet155.Item(IndiceI).Type_du_Besoin & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & ChaineObservation_ToCsv(Dossier.ListeObjet155.Item(IndiceI).Expression_Besoin) & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet155.Item(IndiceI).Periode_Souhaitee & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & ChaineObservation_ToCsv(Dossier.ListeObjet155.Item(IndiceI).Objectif) & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet155.Item(IndiceI).Avis_Evaluateur & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet155.Item(IndiceI).Echeance_Souhaitee & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                    Case 6 'Synthese
                        For IndiceI = 0 To Dossier.ListeObjet156.Count - 1
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet156.Item(IndiceI).Intitule_Famille & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet156.Item(IndiceI).Critere_Numerique.ToString & VI.DoubleQuote & WsSeparateur)
                            Chaine.Append(VI.DoubleQuote & Dossier.ListeObjet156.Item(IndiceI).Critere_Litteral & VI.DoubleQuote & WsSeparateur)
                        Next IndiceI

                End Select

                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property Fichier_Txt_Commun As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder
                Chaine.Append(VI.DoubleQuote & "Identifiant" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Qualite" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Nom" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Prenom" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Patronyme" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Statut" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Corps" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Grade" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Echelon" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Echelon" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Niveau_1" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Niveau_2" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Niveau_3" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Niveau_4" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Fonction" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Fonction" & VI.DoubleQuote & WsSeparateur)

                Chaine.Append(VI.DoubleQuote & "Date_Entretien" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Evaluateur" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Service_Evaluation" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Emploi_Type" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "SiEncadrement" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Nombre_Encadres" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "SiSouhait_EntCarriere" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Signature_Evaluateur" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Date_Signature_Evalue" & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & "Synthese_Evaluation" & VI.DoubleQuote & WsSeparateur)

                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property Fichier_Txt_Commun(ByVal Dossier As Virtualia.Net.Entretien.DossierEntretien) As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Dim DatedEffet As String

                DatedEffet = Dossier.Objet_150.Date_de_Valeur

                Chaine = New System.Text.StringBuilder
                Chaine.Append(VI.DoubleQuote & Dossier.V_Identifiant & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Qualite & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Nom & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Prenom & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.NomPatronymique & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Statut(DatedEffet) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Corps(DatedEffet) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Grade(DatedEffet) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Echelon(DatedEffet) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Date_Echelon(DatedEffet) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.NiveauAffectation(DatedEffet, 1) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.NiveauAffectation(DatedEffet, 2) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.NiveauAffectation(DatedEffet, 3) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.NiveauAffectation(DatedEffet, 4) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.FonctionExercee(DatedEffet, False) & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.FonctionExercee(DatedEffet, True) & VI.DoubleQuote & WsSeparateur)

                Chaine.Append(DatedEffet & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Evaluateur & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Entite_Evaluation & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Emploi_Type & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.SiEncadrement & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Nombre_Encadres & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.SiSouhait_EntretienCarriere & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Date_de_Signature_Evaluateur & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Date_de_Signature_Evalue & VI.DoubleQuote & WsSeparateur)
                Chaine.Append(VI.DoubleQuote & Dossier.Objet_150.Intitule_Synthese & VI.DoubleQuote & WsSeparateur)
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property ListeResultats As List(Of Virtualia.Net.Entretien.DossierEntretien)
            Get
                Return WsListeResultat
            End Get
        End Property

        Public ReadOnly Property ListeDossiers(ByVal Criteres As List(Of String)) As List(Of Virtualia.Net.Entretien.DossierEntretien)
            Get
                Dim IndiceA As Integer
                Dim IndiceF As Integer
                Dim IndiceT As Integer
                Dim IndexCritere As Integer
                Dim TableauData(0) As String
                Dim ListeTravail As List(Of Virtualia.Net.Entretien.DossierEntretien) = Nothing
                Dim DateValeur As String = "31/12/" & Year(Now)
                Dim ValeurLue As String
                Dim ICompetences As Integer = -1
                Dim ISynthese As Integer = -1

                WsListeResultat = Nothing
                If Criteres IsNot Nothing Then
                    For Each It In Criteres
                        If It <> "" Then
                            TableauData = Strings.Split(It, VI.Tild, -1)
                            IndexCritere = CInt(TableauData(0))
                            If IndexCritere = 0 Then
                                DateValeur = "31/12/" & TableauData(1)
                                Exit For
                            End If
                        End If
                    Next
                End If
                WsListeResultat = (From dossier In WsListeDossier Select dossier Where dossier.Objet_150(Strings.Right(DateValeur, 4)) IsNot Nothing
                               Order By dossier.V_Identifiant Ascending).ToList

                For IndiceA = 0 To WsListeResultat.Count - 1
                    WsListeResultat.Item(IndiceA).Annee = CDate(DateValeur).Year
                Next IndiceA

                If Criteres Is Nothing Then
                    Return WsListeResultat
                End If

                For Each It In Criteres
                    If It <> "" Then
                        TableauData = Strings.Split(It, VI.Tild, -1)
                        IndexCritere = CInt(TableauData(0))
                        Select Case IndexCritere
                            Case 15 'Famille de compétences
                                For IndiceK = 0 To Criteres.Count - 1
                                    If Criteres.Item(IndiceK) <> "" Then
                                        TableauData = Strings.Split(Criteres.Item(IndiceK), VI.Tild, -1)
                                        Select Case CInt(TableauData(0))
                                            Case Is = 16 'Critère de compétences
                                                ICompetences = IndiceK
                                                Exit For
                                        End Select
                                    End If
                                Next IndiceK
                            Case 17 'Famille de la synthèse
                                For IndiceK = 0 To Criteres.Count - 1
                                    If Criteres.Item(IndiceK) <> "" Then
                                        TableauData = Strings.Split(Criteres.Item(IndiceK), VI.Tild, -1)
                                        Select Case CInt(TableauData(0))
                                            Case Is = 18 'Critère
                                                ISynthese = IndiceK
                                                Exit For
                                        End Select
                                    End If
                                Next IndiceK
                        End Select
                    End If
                Next

                For Each It In Criteres
                    If It <> "" Then
                        TableauData = Strings.Split(It, VI.Tild, -1)
                        IndexCritere = CInt(TableauData(0))
                        Select Case IndexCritere
                            Case 1 'Mois
                                ListeTravail = New List(Of Virtualia.Net.Entretien.DossierEntretien)
                                For Each dossier In WsListeResultat
                                    For IndiceT = 1 To TableauData.Count - 1
                                        If TableauData(IndiceT) = "" Then
                                            Exit For
                                        End If
                                        If WsRhDates.MoisEnClair(CShort(Strings.Mid(dossier.Objet_150.Date_de_Valeur, 4, 2))) = TableauData(IndiceT) Then
                                            ListeTravail.Add(dossier)
                                            Exit For
                                        End If
                                    Next IndiceT
                                Next
                                WsListeResultat = ListeTravail

                            Case 2 'Nom et Prénom
                                ListeTravail = New List(Of Virtualia.Net.Entretien.DossierEntretien)
                                For Each dossier In WsListeResultat
                                    For IndiceT = 1 To TableauData.Count - 1
                                        If TableauData(IndiceT) = "" Then
                                            Exit For
                                        End If
                                        If dossier.Nom & Strings.Space(1) & dossier.Prenom = TableauData(IndiceT) Then
                                            ListeTravail.Add(dossier)
                                            Exit For
                                        End If
                                    Next IndiceT
                                Next
                                WsListeResultat = ListeTravail

                            Case 3 To 14
                                ListeTravail = New List(Of Virtualia.Net.Entretien.DossierEntretien)
                                For Each dossier In WsListeResultat
                                    DateValeur = dossier.Objet_150.Date_de_Valeur
                                    ValeurLue = ""
                                    Select Case IndexCritere
                                        Case 3 'Statut
                                            ValeurLue = dossier.Statut(DateValeur)
                                        Case 4 'Corps
                                            ValeurLue = dossier.Corps(DateValeur)
                                        Case 5 'Grade
                                            ValeurLue = dossier.Grade(DateValeur)
                                        Case 6 To 9 'Niveaux
                                            ValeurLue = dossier.NiveauAffectation(DateValeur, IndexCritere - 5)
                                        Case 10 'Emploi-Type
                                            ValeurLue = dossier.EmploiType(DateValeur)
                                        Case 11 'Service Evaluation
                                            ValeurLue = dossier.ServiceEvaluation(DateValeur)
                                        Case 12 'Si Fonction Encadrement
                                            Select Case dossier.SiFonctionEncadrement(DateValeur)
                                                Case True
                                                    ValeurLue = "Oui"
                                                Case Else
                                                    ValeurLue = "Non"
                                            End Select
                                        Case 13 'Si souhait entretien carrière
                                            Select Case dossier.SiSouhaitEntCarriere(DateValeur)
                                                Case True
                                                    ValeurLue = "Oui"
                                                Case Else
                                                    ValeurLue = "Non"
                                            End Select
                                        Case 14 'Si formulaire complet et signé
                                            Select Case dossier.SiFormulaireSigne_Termine(DateValeur)
                                                Case True
                                                    ValeurLue = "Oui"
                                                Case Else
                                                    ValeurLue = "Non"
                                            End Select
                                    End Select
                                    For IndiceT = 1 To TableauData.Count - 1
                                        If TableauData(IndiceT) = "" Then
                                            Exit For
                                        End If
                                        If ValeurLue = TableauData(IndiceT) Then
                                            ListeTravail.Add(dossier)
                                            Exit For
                                        End If
                                    Next IndiceT
                                Next
                                WsListeResultat = ListeTravail

                            Case 15, 16 'Famille et critere de compétences
                                Select Case ICompetences
                                    Case Is < 99
                                        If ICompetences >= 0 And IndexCritere = 16 Then
                                            Exit Select
                                        End If
                                        ListeTravail = New List(Of Virtualia.Net.Entretien.DossierEntretien)
                                        For Each dossier In WsListeResultat
                                            ValeurLue = "Non"
                                            For IndiceT = 1 To TableauData.Count - 1
                                                If TableauData(IndiceT) = "" Then
                                                    Exit For
                                                End If
                                                If ICompetences >= 0 Then
                                                    For IndiceF = 0 To dossier.ListeObjet153.Count - 1
                                                        If dossier.ListeObjet153.Item(IndiceF).Competence = TableauData(IndiceT) _
                                                            And dossier.ListeObjet153.Item(IndiceF).Critere_Numerique > 0 Then
                                                            If SiCritereOK(Criteres, ICompetences, _
                                                                  dossier.ListeObjet153.Item(IndiceF).Critere_Litteral) = True Then
                                                                ListeTravail.Add(dossier)
                                                                ValeurLue = "Oui"
                                                            End If
                                                            Exit For
                                                        End If
                                                    Next IndiceF
                                                Else
                                                    Select Case IndexCritere
                                                        Case 15
                                                            For IndiceF = 0 To dossier.ListeObjet153.Count - 1
                                                                If dossier.ListeObjet153.Item(IndiceF).Competence = TableauData(IndiceT) _
                                                                    And dossier.ListeObjet153.Item(IndiceF).Critere_Numerique > 0 Then
                                                                    ListeTravail.Add(dossier)
                                                                    ValeurLue = "Oui"
                                                                    Exit For
                                                                End If
                                                            Next IndiceF
                                                        Case 16
                                                            For IndiceF = 0 To dossier.ListeObjet153.Count - 1
                                                                If dossier.ListeObjet153.Item(IndiceF).Critere_Litteral _
                                                                    = TableauData(IndiceT) _
                                                                    And dossier.ListeObjet153.Item(IndiceF).Critere_Numerique > 0 Then
                                                                    ListeTravail.Add(dossier)
                                                                    ValeurLue = "Oui"
                                                                    Exit For
                                                                End If
                                                            Next IndiceF
                                                    End Select
                                                End If
                                                If ValeurLue = "Oui" Then
                                                    Exit For
                                                End If
                                            Next IndiceT
                                        Next
                                        If ISynthese < 99 Then
                                            WsListeResultat = ListeTravail
                                        End If
                                        If ICompetences >= 0 Then
                                            ICompetences = 99
                                        End If
                                End Select

                            Case 17, 18 'Famille et critere de la synthèse
                                Select Case ISynthese
                                    Case Is < 99
                                        If ISynthese >= 0 And IndexCritere = 18 Then
                                            Exit Select
                                        End If
                                        ListeTravail = New List(Of Virtualia.Net.Entretien.DossierEntretien)
                                        For Each dossier In WsListeResultat
                                            ValeurLue = "Non"
                                            For IndiceT = 1 To TableauData.Count - 1
                                                If TableauData(IndiceT) = "" Then
                                                    Exit For
                                                End If
                                                If ISynthese >= 0 Then
                                                    For IndiceF = 0 To dossier.ListeObjet156.Count - 1
                                                        If dossier.ListeObjet156.Item(IndiceF).Intitule_Famille = TableauData(IndiceT) _
                                                            And dossier.ListeObjet156.Item(IndiceF).Critere_Numerique > 0 Then
                                                            If SiCritereOK(Criteres, ISynthese, _
                                                                  dossier.ListeObjet156.Item(IndiceF).Critere_Litteral) = True Then
                                                                ListeTravail.Add(dossier)
                                                                ValeurLue = "Oui"
                                                            End If
                                                            Exit For
                                                        End If
                                                    Next IndiceF
                                                Else
                                                    Select Case IndexCritere
                                                        Case 17
                                                            For IndiceF = 0 To dossier.ListeObjet156.Count - 1
                                                                If dossier.ListeObjet156.Item(IndiceF).Intitule_Famille = TableauData(IndiceT) _
                                                                    And dossier.ListeObjet156.Item(IndiceF).Critere_Numerique > 0 Then
                                                                    ListeTravail.Add(dossier)
                                                                    ValeurLue = "Oui"
                                                                    Exit For
                                                                End If
                                                            Next IndiceF
                                                        Case 18
                                                            For IndiceF = 0 To dossier.ListeObjet156.Count - 1
                                                                If dossier.ListeObjet156.Item(IndiceF).Critere_Litteral = TableauData(IndiceT) _
                                                                    And dossier.ListeObjet156.Item(IndiceF).Critere_Numerique > 0 Then
                                                                    ListeTravail.Add(dossier)
                                                                    ValeurLue = "Oui"
                                                                    Exit For
                                                                End If
                                                            Next IndiceF
                                                    End Select
                                                End If
                                                If ValeurLue = "Oui" Then
                                                    Exit For
                                                End If
                                            Next IndiceT
                                        Next
                                        If ISynthese < 99 Then
                                            WsListeResultat = ListeTravail
                                        End If
                                        If ISynthese >= 0 Then
                                            ISynthese = 99
                                        End If
                                End Select

                            Case 19 'Critère de résultat de synthèse
                                ListeTravail = New List(Of Virtualia.Net.Entretien.DossierEntretien)
                                For Each dossier In WsListeResultat
                                    For IndiceT = 1 To TableauData.Count - 1
                                        If TableauData(IndiceT) = "" Then
                                            Exit For
                                        End If
                                        If dossier.Objet_150.Intitule_Synthese = TableauData(IndiceT) _
                                                And dossier.Objet_150.Critere_Synthese > 0 Then
                                            ListeTravail.Add(dossier)
                                            Exit For
                                        End If
                                    Next IndiceT
                                Next
                                WsListeResultat = ListeTravail

                        End Select

                    End If
                    If WsListeResultat Is Nothing Then
                        Exit For
                    End If
                Next
                Return WsListeResultat
            End Get
        End Property

        Public ReadOnly Property ListeDossiers As List(Of Virtualia.Net.Entretien.DossierEntretien)
            Get
                Return WsListeDossier
            End Get
        End Property

        Public ReadOnly Property ItemDossier(ByVal Ide As Integer) As Virtualia.Net.Entretien.DossierEntretien
            Get
                Dim IndiceA As Integer
                For IndiceA = 0 To WsListeDossier.Count - 1
                    Select Case WsListeDossier.Item(IndiceA).Objet_150.Ide_Dossier
                        Case Is = Ide
                            Return CType(WsListeDossier.Item(IndiceA), Virtualia.Net.Entretien.DossierEntretien)
                    End Select
                Next IndiceA
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property ReferentielCompetence As Virtualia.Net.Entretien.ObjetRefCompetence
            Get
                Return AppObjetGlobal.VirRefCompetence
            End Get
        End Property

        Public Property Separateur As String
            Get
                Return WsSeparateur
            End Get
            Set(ByVal value As String)
                WsSeparateur = value
            End Set
        End Property

        Private Sub LireToutesFiches(ByVal LstObjets As List(Of Integer))
            Dim ToutUnObjet As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceI As Integer

            For IndiceI = 0 To LstObjets.Count - 1
                ToutUnObjet = ExtractionEntretiens(LstObjets.Item(IndiceI))
                If ToutUnObjet IsNot Nothing Then
                    WsListeAllFiches.AddRange(ToutUnObjet)
                End If
            Next IndiceI

        End Sub

        Private ReadOnly Property SiCritereOK(ByVal LstCriteres As List(Of String), ByVal Index As Integer, ByVal Valeur As String) As Boolean
            Get
                If LstCriteres Is Nothing Then
                    Return False
                End If
                Dim TableauData(0) As String
                Dim IndiceT As Integer

                TableauData = Strings.Split(LstCriteres.Item(Index).ToString, VI.Tild, -1)
                For IndiceT = 1 To TableauData.Count - 1
                    If TableauData(IndiceT) = "" Then
                        Exit For
                    End If
                    If TableauData(IndiceT) = Valeur Then
                        Return True
                    End If
                Next IndiceT
                Return False
            End Get
        End Property

        Private ReadOnly Property ListeEtatCivil As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Get
                If WsListeEtatCivil Is Nothing Then
                    WsListeEtatCivil = (From Fiche In WsListeAllFiches Where Fiche.NumeroObjet = 1 Order By Fiche.Ide_Dossier).ToList
                End If
                Return WsListeEtatCivil
            End Get
        End Property

        Public ReadOnly Property Nombre_Total_FichesEtatcivil As Integer
            Get
                Return ListeEtatCivil.Count
            End Get
        End Property

        Public Function ConstituerListeDossiersPER(ByVal Index As Integer) As Boolean
            Dim Ide As Integer
            Dim DossierPER As Virtualia.Net.Entretien.DossierEntretien
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceA As Integer = 0

            If WsListeEtatCivil Is Nothing Then
                Return False
            End If
            If Index = 0 Then
                WsListeDossier = New List(Of Virtualia.Net.Entretien.DossierEntretien)
            End If
            For IndiceA = Index To WsListeEtatCivil.Count - 1
                Ide = WsListeEtatCivil.Item(IndiceA).Ide_Dossier
                LstFiches = (From Fiches In WsListeAllFiches Where Fiches.Ide_Dossier = Ide).ToList
                If LstFiches IsNot Nothing Then
                    DossierPER = New Virtualia.Net.Entretien.DossierEntretien(AppObjetGlobal, Ide, LstFiches)
                    WsListeDossier.Add(DossierPER)
                End If
                If IndiceA >= Index + 499 Then
                    Exit For
                End If
            Next IndiceA
            Return True
        End Function

        Public Sub LireAllFichesPER(ByVal NoPage As Integer)
            Dim TabObjets As New List(Of Integer)
            Dim IndiceI As Integer
            Select Case NoPage
                Case 1
                    TabObjets.Add(VI.ObjetPer.ObaSociete)
                    TabObjets.Add(VI.ObjetPer.ObaStatut)
                    TabObjets.Add(VI.ObjetPer.ObaOrganigramme)
                    TabObjets.Add(VI.ObjetPer.ObaActivite)
                    TabObjets.Add(VI.ObjetPer.ObaGrade)
                Case 2
                    For IndiceI = 150 To 156
                        TabObjets.Add(IndiceI)
                    Next IndiceI
                Case 3
                    'TabObjets.Add(VI.ObjetPer.ObaFormation)
                    TabObjets.Add(VI.ObjetPer.ObaDIF)
            End Select
            Call LireToutesFiches(TabObjets)
        End Sub

        Public ReadOnly Property ExtractionEntretiens(ByVal NoObjet As Integer) As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim OrdreSql As String
                Dim IndiceA As Integer
                Dim DicoInfos As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
                Dim NbExtraits As Integer

                DicoInfos = (From Info In AppObjetGlobal.VirListeInfosDico Where Info.PointdeVue = VI.PointdeVue.PVueApplicatif And Info.Objet = NoObjet Order By Info.Information).ToList
                If DicoInfos Is Nothing Then
                    Return Nothing
                End If
                NbExtraits = DicoInfos.Count
                Select Case NoObjet
                    Case VI.ObjetPer.ObaGrade, VI.ObjetPer.ObaGradeDetache
                        NbExtraits -= 2
                End Select

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(AppObjetGlobal.VirModele, AppObjetGlobal.PointeurObjetUtiGlobal.InstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.NoInfoSelection(0, 150) = 0
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Inclu, False) = "01/01/2008;31/12/" & Year(Now)
                For IndiceA = 0 To NbExtraits - 1
                    Constructeur.InfoExtraite(IndiceA, NoObjet, 0) = DicoInfos.Item(IndiceA).Information
                Next IndiceA
                OrdreSql = Constructeur.OrdreSqlDynamique
                Constructeur = Nothing
                Return AppObjetGlobal.VirServiceServeur.RequeteSql_ToFiches(AppObjetGlobal.PointeurObjetUtiGlobal.Nom, VI.PointdeVue.PVueApplicatif, NoObjet, OrdreSql)
            End Get
        End Property

        Private Function ChaineObservation_ToCsv(ByVal Valeur As String) As String
            If Valeur = "" Then
                Return ""
            End If
            Dim Chaine As String = Valeur.Replace(vbTab, "_")
            Dim NewChaine As String = Chaine.Replace(vbCrLf, "_")
            Chaine = NewChaine.Replace(vbCr, "_")
            NewChaine = Chaine.Replace(vbLf, "_")
            Chaine = NewChaine.Replace(VI.PointVirgule, VI.Virgule)
            NewChaine = Chaine.Replace(VI.DoubleQuote, Strings.Space(1))
            Return NewChaine
        End Function

        Public Sub New(ByVal PointeurGlobal As Virtualia.Net.WebAppli.ObjetGlobal)
            Dim TabObjets As New List(Of Integer)

            AppObjetGlobal = PointeurGlobal
            WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates

            WsListeAllFiches = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            TabObjets.Add(VI.ObjetPer.ObaCivil)
            Call LireToutesFiches(TabObjets)
        End Sub

    End Class
End Namespace