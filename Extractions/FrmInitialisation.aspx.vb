﻿Option Compare Text
Public Class FrmInitialisation
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsEnsemble As Virtualia.Net.Entretien.EnsembleExtraction

    Private Sub FrmInitialisation_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        Dim NoPage As Integer = CInt(HNumPage.Value)


        Select Case NoPage
            Case 0
                NoPage += 1
                HNumPage.Value = NoPage.ToString
                EtiTraitement.Text = "... Lecture de la base de données ..."

                WsEnsemble = New Virtualia.Net.Entretien.EnsembleExtraction(WebFct.PointeurGlobal)
                WebFct.PointeurGlobal.PointeurEnsemble = WsEnsemble
                EtiTraitement.Text &= "1. Situations administratives ..."

            Case 1 To 100
                WsEnsemble = WebFct.PointeurGlobal.PointeurEnsemble
                Select Case NoPage
                    Case 1 To 3
                        Call WsEnsemble.LireAllFichesPER(NoPage)
                        NoPage += 1
                        HNumPage.Value = NoPage.ToString
                        Select Case NoPage
                            Case 2
                                EtiTraitement.Text = "... Lecture de la base de données ... 2. Entretiens professionnels ..."
                            Case 3
                                EtiTraitement.Text = "... Lecture de la base de données... 3. Formation ..."
                        End Select
                    Case Else
                        NoPage = 101
                        HNumPage.Value = NoPage.ToString
                End Select
                Exit Sub

            Case 101 To 200
                WsEnsemble = WebFct.PointeurGlobal.PointeurEnsemble
                Dim CptIde As Integer = WsEnsemble.Nombre_Total_FichesEtatcivil
                Dim IDebut As Integer = (NoPage - 101) * 500

                If WsEnsemble.ConstituerListeDossiersPER(IDebut) = True Then
                    NoPage += 1
                    HNumPage.Value = NoPage.ToString
                Else
                    Exit Sub
                End If
                If IDebut + 500 > CptIde Then
                    NoPage = 201
                    HNumPage.Value = NoPage.ToString
                    Exit Sub
                End If
                If CptIde < IDebut + 500 Then
                    EtiTraitement.Text = "... Préparation des dossiers ... " & CptIde.ToString & " / " & CptIde.ToString
                Else
                    EtiTraitement.Text = "... Préparation des dossiers ... " & (IDebut + 500).ToString & " / " & CptIde.ToString
                End If

            Case Is > 200
                EtiTraitement.Text = "... Initialisation terminée ..."
                HorlogeLogin.Enabled = False
                Response.Redirect("FrmExtractions.aspx")
        End Select
    End Sub

    'Private Async Function Preparation(ByVal Index As Integer) As System.Threading.Tasks.Task(Of Boolean)
    '    Dim FonctionExecutee As Func(Of Integer, Boolean) = Function(Idx)
    '                                                            Select Case Idx
    '                                                                Case 1 To 3
    '                                                                    Call WsEnsemble.LireAllFichesPER(Idx)
    '                                                            End Select
    '                                                        End Function
    '    Await System.Threading.Tasks.Task.fromresult
    'End Function
End Class