﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmExtractHonda
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String = "Extraction"
    Private WsPageValeurs As List(Of String)

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        If WebFct.PointeurGlobal.PointeurEnsemble Is Nothing Then
            Dim Ensemble As Virtualia.Net.Entretien.EnsembleExtraction
            Ensemble = New Virtualia.Net.Entretien.EnsembleExtraction(WebFct.PointeurGlobal)
            WebFct.PointeurGlobal.PointeurEnsemble = Ensemble
        End If
        Call FaireListeValeurs(CInt(HSelCritere.Value))
        Call AfficherResume()
        Call ActualiserCheckBoxList()
    End Sub

    Private Sub FaireListeValeurs(ByVal Index As Integer)
        Dim TabRes As List(Of String)
        Dim IndiceA As Integer
        Dim Rupture As String = ""
        Dim DateEffet As String

        ListeValeur.ClearSelection()
        ListeValeur.Items.Clear()

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        End If
        DateEffet = WebFct.ViRhDates.DateduJour

        Select Case Index
            Case 0 'KI
                Dim LstDistinct As List(Of Integer)
                LstDistinct = (From dossier In WebFct.PointeurGlobal.PointeurExtractionHonda.ListeDossiers Where dossier.KI(DateEffet) > 0
                                          Order By dossier.KI(DateEffet) Select dossier.KI(DateEffet)).Distinct.ToList
                If LstDistinct IsNot Nothing Then
                    For IndiceA = 0 To LstDistinct.Count - 1
                        ListeValeur.Items.Add(CStr(LstDistinct.Item(IndiceA)))
                    Next IndiceA
                End If

            Case 1 'Année
                TabRes = WebFct.PointeurGlobal.SelectionDistincte(VI.PointdeVue.PVueApplicatif, 150, "PER_ENTRETIEN_GENERAL", "Date_valeur")
                If TabRes Is Nothing Then
                    Exit Sub
                End If
                For Each Ligne As String In TabRes
                    If Strings.Right(Ligne.Replace(VI.Tild, ""), 4) <> Rupture Then
                        ListeValeur.Items.Add(Strings.Right(Ligne.Replace(VI.Tild, ""), 4))
                        Rupture = Strings.Right(Ligne.Replace(VI.Tild, ""), 4)
                    End If
                Next

            Case 2 'Nom et Prénom
                Dim ListeTriee As List(Of Virtualia.Net.Entretien.DossierHonda)
                ListeTriee = (From dossier In WebFct.PointeurGlobal.PointeurExtractionHonda.ListeDossiers Select dossier _
                                               Order By dossier.Nom Ascending, dossier.Prenom Ascending).ToList
                If ListeTriee Is Nothing Then
                    Exit Sub
                End If
                For Each Dossier In ListeTriee
                    ListeValeur.Items.Add(Dossier.Nom & Strings.Space(1) & Dossier.Prenom)
                Next

            Case 3 'Type du formulaire
                Dim LstDistinct As List(Of String)
                LstDistinct = (From dossier In WebFct.PointeurGlobal.PointeurExtractionHonda.ListeDossiers Where dossier.Objet_150.Type_Formulaire <> ""
                                          Order By dossier.Objet_150.Type_Formulaire Select dossier.Objet_150.Type_Formulaire).Distinct.ToList

                If LstDistinct IsNot Nothing Then
                    For IndiceA = 0 To LstDistinct.Count - 1
                        ListeValeur.Items.Add(LstDistinct.Item(IndiceA))
                    Next IndiceA
                End If

            Case 4 'Manager
                Dim LstDistinct As List(Of String)
                LstDistinct = (From dossier In WebFct.PointeurGlobal.PointeurExtractionHonda.ListeDossiers Where dossier.Evaluateur_Nom_Prenom <> ""
                                          Order By dossier.Evaluateur_Nom_Prenom Select dossier.Evaluateur_Nom_Prenom).Distinct.ToList

                If LstDistinct IsNot Nothing Then
                    For IndiceA = 0 To LstDistinct.Count - 1
                        ListeValeur.Items.Add(LstDistinct.Item(IndiceA))
                    Next IndiceA
                End If
        End Select
    End Sub

    Private Sub ListeSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListeSelection.SelectedIndexChanged
        HSelCritere.Value = ListeSelection.SelectedIndex.ToString
    End Sub

    Private Sub ListeExtraction_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListeExtraction.SelectedIndexChanged
        HSelExtrait.Value = ListeExtraction.SelectedIndex.ToString
    End Sub

    Private Sub ListeValeur_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListeValeur.SelectedIndexChanged
        Dim TableauData(0) As String
        Dim IndiceI As Integer
        Dim IndexCritere As Integer = -1
        Dim Chaine As String = ""

        If Me.ViewState(WsNomState) IsNot Nothing Then
            WsPageValeurs = CType(Me.ViewState(WsNomState), List(Of String))
            For IndiceI = 0 To WsPageValeurs.Count - 1
                TableauData = Strings.Split(WsPageValeurs(IndiceI).ToString, VI.Tild, -1)
                If TableauData(0) = HSelCritere.Value Then
                    IndexCritere = IndiceI
                    WsPageValeurs(IndexCritere) = HSelCritere.Value & VI.Tild
                    Exit For
                End If
            Next IndiceI
            Me.ViewState.Remove(WsNomState)
            If IndexCritere = -1 Then
                IndexCritere = WsPageValeurs.Count
                WsPageValeurs.Add(HSelCritere.Value & VI.Tild)
            End If
        Else
            IndexCritere = 0
            WsPageValeurs = New List(Of String)
            WsPageValeurs.Add(HSelCritere.Value & VI.Tild)
        End If

        For IndiceI = 0 To ListeValeur.Items.Count - 1
            Select Case ListeValeur.Items.Item(IndiceI).Selected
                Case True
                    Chaine &= ListeValeur.Items.Item(IndiceI).Text & VI.Tild
            End Select
        Next IndiceI
        If Chaine <> "" Then
            WsPageValeurs(IndexCritere) = WsPageValeurs(IndexCritere).ToString & Chaine
        Else
            WsPageValeurs.RemoveAt(IndexCritere)
        End If
        Me.ViewState.Add(WsNomState, WsPageValeurs)

        EtiNbDossiers.Text = ""
    End Sub

    Private Sub AfficherResume()
        Dim Chaine As String = ""
        Chaine = "EXTRACTION" & vbCrLf
        Chaine &= Strings.Space(10) & ListeExtraction.Items.Item(CInt(HSelExtrait.Value)).Text & vbCrLf
        If Me.ViewState(WsNomState) Is Nothing Then
            TxtResume.Text = Chaine
            Exit Sub
        End If

        Dim TableauData(0) As String
        Dim IndiceI As Integer
        Dim IndiceK As Integer

        Chaine &= "SELECTION" & vbCrLf

        WsPageValeurs = CType(Me.ViewState(WsNomState), List(Of String))
        For IndiceI = 0 To WsPageValeurs.Count - 1
            TableauData = Strings.Split(WsPageValeurs(IndiceI).ToString, VI.Tild, -1)
            Chaine &= Strings.Space(10) & ListeSelection.Items.Item(CInt(TableauData(0))).Text & vbCrLf
            For IndiceK = 1 To TableauData.Count - 1
                If TableauData(IndiceK) <> "" Then
                    Chaine &= Strings.Space(15) & "- " & TableauData(IndiceK) & vbCrLf
                End If
            Next IndiceK

        Next IndiceI

        TxtResume.Text = Chaine
    End Sub

    Private Sub ActualiserCheckBoxList()
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        Dim TableauData(0) As String
        Dim IndiceI As Integer
        Dim IndexCritere As Integer = -1

        WsPageValeurs = CType(Me.ViewState(WsNomState), List(Of String))
        For IndiceI = 0 To WsPageValeurs.Count - 1
            TableauData = Strings.Split(WsPageValeurs(IndiceI).ToString, VI.Tild, -1)
            If TableauData(0) = HSelCritere.Value Then
                IndexCritere = IndiceI
                Exit For
            End If
        Next IndiceI
        If IndexCritere = -1 Then
            Exit Sub
        End If

        For IndiceI = 1 To TableauData.Count - 1
            If TableauData(IndiceI) <> "" Then
                ListeValeur.Items.FindByText(TableauData(IndiceI)).Selected = True
            End If
        Next IndiceI
    End Sub

    Private Sub CommandeInit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeInit.Click
        If Me.ViewState(WsNomState) IsNot Nothing Then
            Me.ViewState.Remove(WsNomState)
        End If
        EtiNbDossiers.Text = ""
        CommandeFichier.Text = ""
    End Sub

    Private Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        CommandeFichier.Text = ""
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        End If
        Dim EnsembleW As Virtualia.Net.Entretien.EnsembleExtractHonda
        EnsembleW = WebFct.PointeurGlobal.PointeurExtractionHonda
        If EnsembleW Is Nothing Then
            Exit Sub
        End If

        '** Extraction *********************************************************
        WsPageValeurs = CType(Me.ViewState(WsNomState), List(Of String))

        Dim ResultatCMC As List(Of Virtualia.Net.Entretien.DossierHonda)

        ResultatCMC = EnsembleW.ListeDossiers(WsPageValeurs)
        EtiNbDossiers.Text = ResultatCMC.Count.ToString & " dossiers sélectionnés"

        If ResultatCMC.Count = 0 Then
            Exit Sub
        End If
        Dim FicExtrait As String
        FicExtrait = EnsembleW.NomFichier(CInt(HSelExtrait.Value))
        If FicExtrait = "" Then
            Exit Sub
        End If

        '*** Ecriture du fichier ***********************************************
        Dim IndiceI As Integer
        Dim CodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim Enreg As String

        FicStream = New System.IO.FileStream(WebFct.PointeurGlobal.PathPhysiqueFichierTxt & "\" & FicExtrait, IO.FileMode.Create, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, CodeIso)

        Enreg = EnsembleW.Fichier_Txt_Commun & EnsembleW.Fichier_Txt_Corps(CInt(HSelExtrait.Value))
        FicWriter.WriteLine(Enreg)
        For IndiceI = 0 To ResultatCMC.Count - 1
            Enreg = EnsembleW.Fichier_Txt_Commun(ResultatCMC.Item(IndiceI))
            Enreg &= EnsembleW.Fichier_Txt_Corps(ResultatCMC.Item(IndiceI), CInt(HSelExtrait.Value))
            FicWriter.WriteLine(Enreg)
        Next IndiceI

        FicWriter.Flush()
        FicWriter.Close()

        '*** Mise à disposition
        CommandeFichier.Text = FicExtrait
    End Sub

    Private Sub CommandeFichier_Click(sender As Object, e As System.EventArgs) Handles CommandeFichier.Click
        If CommandeFichier.Text = "" Then
            Exit Sub
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        End If
        Dim NomFichier As String = WebFct.PointeurGlobal.PathPhysiqueFichierTxt & "\" & CommandeFichier.Text
        Dim FluxTeleChargement As Byte()
        If My.Computer.FileSystem.FileExists(NomFichier) = False Then
            Exit Sub
        End If
        FluxTeleChargement = My.Computer.FileSystem.ReadAllBytes(NomFichier)

        If FluxTeleChargement IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & NomFichier & "; size=" & FluxTeleChargement.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxTeleChargement)
            response.Flush()
            response.End()
        End If
    End Sub

End Class